﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Cellar
{
    public class ResponseCellarAssingJson
    {
        public int id { get; set; }
        public int numberPage { get; set; }
        public DateTime? dateCreate { get; set; }
        public decimal value { get; set; }
        public int countTimbre { get; set; }
        public int countEnable { get; set; }
        public int? cashier { get; set; }
        public string nameCashier { get; set; }
        public int noTimbre { get; set; }
        public int timbre { get; set; }
        public int status { get; set; }
        public List<ListTimbresimpleJson> timbres { get; set; }
    }
}
﻿var moduleName = 'document';

(function () {
    angular.module('document')
        .controller('userProfile', ['$scope', '$rootScope', '$http', '$anchorScroll', 'CONSTANT', '$window','documentFactory',
                function ($scope, $rootScope, $http, $anchorScroll, CONSTANT, $window, documentFactory, ModalCtrl, ModalInstanceCtrl) {

            $scope.regexDate = CONSTANT.REGEX_FECHA;
            $scope.regexPass = CONSTANT.REGEX_PASS;
            $scope.regexMail = CONSTANT.REGEX_EMAIL;
            $scope.regexPhone = CONSTANT.REGEX_PHONE;
            $scope.regexPhoneState = CONSTANT.REGEX_STATEPHONE;
            $scope.regexDigit = CONSTANT.REGEX_DIGIT;

            $scope.showNotify = 'emergente1 notify-event-content';
            $scope.showNotifyLittle = 'emergente1';
            $scope.listAddress;
            $scope.listPhone;
            $scope.listMail;
            $scope.idRefereeContent = 0;
            $scope.showmsgO = false;
            $scope.showmsgN = false;
            $scope.menuAddress = false;
            $scope.menuAddressDelete = true;
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.showEditAddress = 0;
            $scope.updateIdReferee = null;
            $scope.dropDownCargando = 0;
            $scope.callBackMethodName = null;


            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.aproxNext = function (total) {
                return Math.ceil(total);
            }

            // Sin Usar
            $scope.NuevoMail = function () {
                if ($scope.mailReferee != null && $scope.mailRefereeC != null) {
                    if ($scope.mailReferee == $scope.mailRefereeC) {
                        var information = {
                            idReferee: $scope.idRefereeContent,
                            NewMail: $scope.mailReferee
                        }
                        $scope.showLoadingFunction();
                        documentFactory.actionPostFunction(CONSTANT.URL_API_CHANGEMAIL, information)
                        .then(function (response) {
                            if (response.data.Result) {
                                $scope.hideLoadingFunction();
                                $scope.mail = response.data.nMail;
                                $scope.mailReferee = null;
                                $scope.mailRefereeC = null;
                                alert(response.data.Mensaje);
                            } else {
                                $scope.hideLoadingFunction();
                                $scope.mailReferee = null;
                                $scope.mailRefereeC = null;
                                alert(response.data.Mensaje);
                            }
                        }, function (error) {
                            //error
                            $scope.hideLoadingFunction();
                            $scope.mailReferee = null;
                            $scope.mailRefereeC = null;
                            alert(response.data.Mensaje);
                        });
                    } else {
                        //error 
                        alert("Por favor verifique que ambos correos ingresados sean iguales.");
                        //alert("Deben ser iguales");
                    }
                }
            }

            //---------------------------------------SALDO--------------------------------------

            $scope.getBalanceReferee = function (idreferee) {
                $scope.showLoadingFunction();
                documentFactory.actionGetIntFunction(CONSTANT.URL_GET_COLEGIADO_BALANCE, idreferee)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        $scope.balanceCountReferee = response.data.countReferee;
                        $scope.balanceMountReferee = response.data.referee;
                        $scope.balanceCountTimbre = response.data.countTimbre;
                        $scope.balanceMountTimbre = response.data.timbre;
                        $scope.balanceCountOther = response.data.countOther;
                        $scope.balanceMountOther = response.data.other;
                        $scope.balanceMountMora = response.data.mora;

                        $scope.balanceDateReferee = response.data.dateReferee;
                        $scope.balanceDateTimbre = response.data.dateTimbre;
                        $scope.balanceDatePostumo = response.data.datePostumo;

                        $scope.balanceCountPstumo = response.data.countPostumo;
                        $scope.balanceMountPostumo = response.data.postumo;


                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            // --------------------------------INFO DEL REFEREE-------------------------------

            $scope.getReferee = function (idreferee) {
                $scope.statusUpdate = "";

                documentFactory.actionGetIntFunction(CONSTANT.URL_API_REFEREE, idreferee)
                    .then(function (response) {
                        var data = response.data;
                        if (data != null) {
                            $scope.idRefereeContent = idreferee;
                            $scope.completeNameReferee = data.PrimerNombre + " " + data.SegundoNombre + " "
                                                         + data.TercerNombre + " " + data.PrimerApellido + " " + data.SegundoApellido + " " + data.CasdaApellido;
                            $scope.firstNameReferee = data.PrimerNombre;
                            $scope.secoundNameReferee = data.SegundoNombre;
                            $scope.thirdNameReferee = data.TercerNombre;
                            $scope.fistLastNameReferee = data.PrimerApellido;
                            $scope.secoundLastNameReferee = data.SegundoApellido;
                            $scope.marriedNameReferee = data.CasdaApellido;

                            $scope.nitReferee = data.Nit;
                            $scope.noCedulaReferee = data.RegCedula;
                            $scope.extCedulaReferee = data.CedulaExtendida;
                            $scope.regCedulaReferee = data.Registro;
                            $scope.noDpiReferee = data.Dpi;
                            $scope.extDpieferee = data.DpiExtendido;
                            $scope.dateReferee = data.FechaColegiacionTexto;
                            $scope.birthdayReferee = data.FechaNacimientoTexto;
                            $scope.notifyReferee = data.Notificacion;
                            $scope.statusReferee = data.Estatus;
                            $scope.dateSweReferee = data.FechaJuramentacionTexto;
                            $scope.salaryReferee = data.Salario;
                            $scope.headquarterRefereeJu = data.nameSedeJuramentada;
                            $scope.headquarterReferee = data.nameSedeActual;
                            $scope.placeBirthReferee = data.placeBirth;
                            $scope.nacionalityReferee = data.nacionality;
                            $scope.dateJubilacionReferee = data.fechaJubilacion;
                            $scope.experenceReferee = data.experence;

                            if (data.Sexo == 0) {
                                $scope.genderReferee = "Masculino";
                            } else if (data.Sexo == 1) {
                                $scope.genderReferee = "Femenino";
                            } else {
                                $scope.genderReferee = "No definido";
                            }

                            $scope.statusReferee = data.Estatus;
                            $scope.statuscivilReferee = data.EstadoCivil;
                            $scope.remarkReferee = data.Observaciones;
                            $scope.typeReferee = data.typeReferee;
                            $scope.imageReferee = 'https://cdn.pixabay.com/photo/2017/02/25/22/04/user-icon-2098873_960_720.png';
                            // $scope.imageReferee = data.imagen === 'sinfoto.jpg' ? 'https://cdn.pixabay.com/photo/2017/02/25/22/04/user-icon-2098873_960_720.png' : '';
                            $scope.deathReferee = data.fechaFallecimiento;
                            $scope.bloodTypeReferee = data.TipoSangre;

                            $scope.mail = data.mail;

                            $scope.statusRefereeCard = data.EstadoTarjetaSeguro;
                            $scope.enableJu = ((data.esJuramentado == 1) ? true : false);
                            $scope.enablePen = ((data.verTexto == 1) ? true : false);

                            $scope.hideLoadingFunction();
                        }
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.setAuth = function (idReferee) {
                $scope.idReferee = idReferee;
                documentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN_COLEGIADO)
                .then(function (response) {
                    documentFactory.setAuthorization(response.data);
                    $scope.getReferee(idReferee);
                    $scope.getAddressreferee(idReferee);
                    $scope.getCountryReferee();
                    $scope.getBalanceReferee(idReferee);
                    $scope.getPhoneReferee(idReferee);
                    $scope.getMailReferee(idReferee);

                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }


            $scope.submit = function () {
                $scope.showLoadingFunction();
                var formdata = new FormData();
                var fileInput = document.getElementById('fileInput');
                formdata.append("idReferee", $scope.idReferee);
                formdata.append("idPerfil", $scope.idPerfil);
                for (i = 0; i < fileInput.files.length; i++) {
                    formdata.append(fileInput.files[i].name, fileInput.files[i]);
                }
                var xhr = new XMLHttpRequest();
                var res = "";
                xhr.open('POST', '/Document/UploadCustomImageColegiado');
                xhr.send(formdata);
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        res = xhr.responseText;
                        console.log("RESPUESTA:" + res);
                        if (res === "500") {
                            alert("Error al cargar imagenes.");
                        } else {
                            alert(res);
                        }
                    }
                }


                $scope.hideLoadingFunction();
            };



            //-------------------------------------------------------Address------------------------------------------------
            $scope.changeTypeAddress = function (type) {
                if (type == 1) {
                    return "Residencia";
                } else if (type == 2) {
                    return "Trabajo";
                } else if (type == 3) {
                    return "Notificación";
                } else if (type == 4) {
                    return "Correspondencia";
                } else {
                    return "No definido";
                }
            }

            //Muestra de pantalla de Crear y Editar Direccion
            $scope.showMenuAddress = function () {
                $scope.menuAddress = !$scope.menuAddress;
                $scope.updateIdReferee = null;
            }

            //Muesta el menu de eliminar para confirmar
            $scope.showMenuAddressDelete = function () {
                $scope.menuAddressDelete = !$scope.menuAddressDelete;
                $scope.updateIdReferee = null;
            }

            ///muestra para editar y definir cual es el que hay que editar
            $scope.showEditAddress = function (Address) {
                console.log(Address);
                $scope.updateIdReferee = Address.idAddress;
                $scope.addressStreet = Address.street;
                $scope.addressNumber = Address.number;
                $scope.addressZone = Address.zone;
                $scope.addressColony = Address.colony;
                $scope.addressCaserio = Address.caserio;
                $scope.addressBarrio = Address.barrio;
                $scope.addressType = $scope.changeTypeAddress(Address.typeAddress);
                $scope.addressCountry = Address.country;
                $scope.addressDep = Address.departament;
                $scope.addressMuni = Address.municipality;
                $scope.menuAddress = (($scope.menuAddress) ? $scope.menuAddress : !$scope.menuAddress);
                $scope.addressEnableNot = ((Address.notification == 1) ? true : false);
            }

            //obtiene la direccion del colegiado
            $scope.getAddressreferee = function (idReferee) {
                documentFactory.actionGetIntFunction(CONSTANT.URL_API_ADDRESS, idReferee)
                    .then(function (response) {
                        $scope.listAddress = response.data;
                    }, function (error) {
                        console.log(error);
                    });
            }

            //crear direccion por ahora sin la parte del municipio
            $scope.addAddressReferee = function () {
                var statusT = (($scope.addressEnableNot == true) ? 1 : 0);
                $scope.showLoadingFunction();
                if ($scope.formaddressreferee.$valid) {

                    var information = {
                        street: $scope.addressStreet,
                        number: $scope.addressNumber,
                        zone: $scope.addressZone,
                        colony: $scope.addressColony,
                        caserio: $scope.addressCaserio,
                        barrio: $scope.addressBarrio,
                        typeAddress: $scope.addressSelectType,
                        municipality: $scope.addressSelectMuni.id,
                        idReferee: $scope.idRefereeContent,
                        notification: statusT
                    }

                    documentFactory.actionPostFunction(CONSTANT.URL_API_ADDRESS, information)
                        .then(function (response) {
                            $scope.showMenuAddress();
                            $scope.getAddressreferee($scope.idRefereeContent);
                            $scope.hideLoadingFunction();
                            $scope.clearDataAddress();
                            $scope.showModal(null, null, 'Agregar', 'Dirección ingresada correctamente!', false, 'OK', '');
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                            $scope.showModal(null, null, 'ERROR!', 'No se pudo ingresar la dirección!', false, 'OK', '');
                        });
                }
            }

            //Actualizar la direccion de un usuario sin la parte de municipio
            $scope.updateAddressReferee = function () {
                var statusT = (($scope.addressEnableNot == true) ? 1 : 0);
                $scope.showLoadingFunction();
                var information = {
                    street: $scope.addressStreet,
                    number: $scope.addressNumber,
                    zone: $scope.addressZone,
                    colony: $scope.addressColony,
                    caserio: $scope.addressCaserio,
                    barrio: $scope.addressBarrio,
                    typeAddress: (($scope.addressSelectType == null) ? 0 : $scope.addressSelectType),
                    municipality: (($scope.addressSelectMuni == null) ? 0 : $scope.addressSelectMuni.id),
                    idReferee: $scope.idRefereeContent,
                    notification: statusT
                };

                documentFactory.actionPutFunction(CONSTANT.URL_API_ADDRESS, $scope.updateIdReferee, information)
                        .then(function (response) {
                            console.log(response);
                            $scope.hideLoadingFunction();
                            $scope.showMenuAddress();
                            $scope.getAddressreferee($scope.idRefereeContent);
                            $scope.clearDataAddress();
                            $scope.showModal(null, null, 'Actualización', 'Dirección actualizada!', false, 'OK', '');
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                            $scope.showModal(null, null, 'ERROR!', 'No se pudo actualizar la dirección!', false, 'OK', '');
                        });
            }
            
            //funcion para eliminar la direccion
            $scope.deleteAddressReferee = function (Address) {
                $scope.updateIdReferee = null;

                $scope.callBackMethodName = 'deleteAddressRefereeOK';
                $scope.showModal(
                    Address, $scope.callBackMethodName, "Eliminación", "¿Desea eliminar la dirección?", true, 'OK', 'Cancel'
                );
            }

            $scope.$on('deleteAddressRefereeOK', function (evt, data) {
                $scope.deleteAddressRefereeOK(data.datos);
                $scope.callBackMethodName = null;
            });


            //funcion para eliminar la direccion después de modal
            $scope.deleteAddressRefereeOK = function (Address) {
                $scope.showLoadingFunction();
                documentFactory.actionDeleteFunction(CONSTANT.URL_API_ADDRESS, Address.idAddress)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            $scope.getAddressreferee($scope.idRefereeContent);
                            $scope.clearDataAddress();
                            $scope.showModal(null, null, 'Eliminación', 'Dirección eliminada!', false, 'OK', '');
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                            $scope.showModal(null, null, 'ERROR!', 'No se pudo eliminar la dirección!', false, 'OK', '');
                        });
            }

            //Limpia la pantalla
            $scope.clearDataAddress = function () {
                $scope.addressStreet = "";
                $scope.addressNumber = "";
                $scope.addressZone = "";
                $scope.addressColony = "";
                $scope.addressCaserio = "";
                $scope.addressBarrio = "";
                $scope.addressType = "";
                $scope.addressCountry = "";
                $scope.addressDep = "";
                $scope.addressMuni = "";
                $scope.addressEnableNot = false;
                $scope.addressSelectType = null;
                $scope.addressSelectMuni = null;
            }


            $scope.showLoadingLittle = function (idCarga) {
                $scope.showNotifyLittle = 'emergente2';
                $scope.dropDownCargando = idCarga;
            }

            $scope.hideLoadingLittle = function () {
                $scope.showNotifyLittle = 'emergente1';
                $scope.dropDownCargando = 0;
            }

            //-----------------------------------------Country-------------------------------------------

            $scope.getCountryReferee = function () {
                documentFactory.actionGetFunction(CONSTANT.URL_API_COUNTRY)
                    .then(function (response) {
                        $scope.listCountry = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            //--------------------------------------------Departament------------------------------------

            $scope.getDepartamentReferee = function () {
                $scope.showLoadingLittle(1);
                documentFactory.actionGetIntFunction(CONSTANT.URL_API_DEPARTAMENT, $scope.addressSelectCountry.id)
                    .then(function (response) {
                        $scope.hideLoadingLittle();
                        $scope.listDepartament = response.data;
                    }, function (error) {
                        $scope.hideLoadingLittle();
                        console.log(error);
                    })
            }

            //---------------------------------------------Municipality----------------------------------

            $scope.getMunicipalityReferee = function () {
                $scope.showLoadingLittle(2);
                documentFactory.actionGetIntFunction(CONSTANT.URL_API_MUNICIPALITY, $scope.addressSelectDep.id)
                    .then(function (response) {
                        $scope.hideLoadingLittle();
                        $scope.listMunicipality = response.data;
                    }, function (error) {
                        $scope.hideLoadingLittle();
                        console.log(error);
                    })
            }



            //-------------------------------------------------------Phone------------------------------------------------

            //Agregamos telefono
            $scope.addPhone = function (phone) {
                $scope.showLoadingFunction();

                var information = {
                    number: phone.number,
                    cod: ((phone.cod == null || phone.cod == "") ? 502 : phone.cod),
                    idReferee: $scope.idRefereeContent,
                    idFamily: null,
                    type: phone.type
                }

                documentFactory.actionPostFunction(CONSTANT.URL_API_PHONE, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            // $scope.hideMenuFamilyPhone();
                            $scope.getPhoneReferee($scope.idRefereeContent);
                            $scope.showModal(null, null, 'Agregar', 'Teléfono ingresado correctamente!', false, 'OK', '');
                        }

                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                        $scope.showModal(null, null, 'ERROR!', 'No se pudo ingresar el teléfono!', false, 'OK', '');
                    });
            }

            //obtiene el listado de los telefonos
            $scope.getPhoneReferee = function (idReferee) {
                $scope.showLoadingFunction();
                documentFactory.actionGetIntFunction(CONSTANT.URL_API_PHONE, idReferee)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        $scope.listPhone = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });

            }

            //Cambio de nombre a tipo de telefono
            $scope.defineTypePhone = function (type) {
                switch (type) {
                    case 1:
                        return "Teléfono casa";
                        break;
                    case 2:
                        return "Teléfono oficina";
                        break;
                    case 3:
                        return "Fax oficina";
                        break;
                    case 4:
                        return "Celular";
                        break;
                    case 5:
                        return "Otro";
                        break;
                    default:
                        return "";

                }
            }
            
            //Eliminar del listado de telefono
            $scope.deletePhoneReferee = function (phone) {
                $scope.callBackMethodName = 'deletePhoneRefereeOK';
                $scope.showModal(
                    phone, $scope.callBackMethodName, "Eliminación", "¿Desea eliminar " + phone.state + "-" + phone.number + "?", true, 'OK', 'Cancel'
                );
            }

            $scope.$on('deletePhoneRefereeOK', function (evt, data) {
                $scope.deletePhoneRefereeOK(data.datos);
                $scope.callBackMethodName = null;
            });


            //elimina un telefono del listado
            $scope.deletePhoneRefereeOK = function (phone) {
                $scope.showLoadingFunction();
                documentFactory.actionDeleteFunction(CONSTANT.URL_API_PHONE, phone.id)
                        .then(function (response) {
                            if (response.data) {
                                $scope.getPhoneReferee($scope.idRefereeContent);
                                $scope.showModal(null, null, 'Eliminación', 'Teléfono eliminado!', false, 'OK', '');
                            }
                            $scope.hideLoadingFunction();
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                            $scope.showModal(null, null, 'ERROR!', 'No se pudo eliminar el teléfono!', false, 'OK', '');
                        });
            }



            //-------------------------------------------------------Mail------------------------------------------------

            //obtener el listado de los mail
            $scope.getMailReferee = function (idReferee) {
                documentFactory.actionGetIntFunction(CONSTANT.URL_API_MAIL, idReferee)
                    .then(function (response) {
                        $scope.listMail = response.data;
                    }, function (error) {
                        console.log(error);
                    });
            }

            //agrega en correo al sistema
            $scope.addMailReferee = function (emailito) {
                $scope.showLoadingFunction();
                var information = {
                    mail: emailito.texto,
                    idReferee: $scope.idRefereeContent
                }
                documentFactory.actionPostFunction(CONSTANT.URL_API_MAIL, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            $scope.showModal(null, null, 'Agregar', 'Correo ingresado correctamente!', false, 'OK', '');
                            $scope.getMailReferee($scope.idRefereeContent);
                            $scope.mailitem.texto = '';
                        }, function (error) {
                            console.log(error);
                            $scope.showModal(null, null, 'ERROR!', 'No se pudo ingresar el E-Mail', false, 'OK', '');
                            $scope.hideLoadingFunction();
                        });

            }
            
            //Eliminar el listado de mail
            $scope.deleteMailReferee = function (mail) {
                $scope.callBackMethodName = 'deleteMailRefereeOK';
                $scope.showModal(
                    mail, $scope.callBackMethodName, "Eliminación", "¿Desea eliminar " + mail.mail + "?", true, 'OK', 'Cancel'
                );
            }
            
            $scope.$on('deleteMailRefereeOK', function (evt, data) {
                $scope.deleteMailRefereeOK(data.datos);
                $scope.callBackMethodName = null;
            });

            $scope.deleteMailRefereeOK = function (mail) {
                $scope.showLoadingFunction();
                documentFactory.actionDeleteFunction(CONSTANT.URL_API_MAIL, mail.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getMailReferee($scope.idRefereeContent);
                                $scope.showModal(null, null, 'Eliminación', 'Correo eliminado!', false, 'OK', '');
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.showModal(null, null, 'ERROR!', 'No se pudo eliminar el registro!', false, 'OK', '');
                            $scope.hideLoadingFunction();
                        });
            }


            //----------------------------------------------------- Password ------------------------------------------------
            
            $scope.setAuthPasswordPage = function (idReferee) {
                $scope.showLoadingFunction();
                $scope.idRefereeContent = idReferee;
                documentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN_COLEGIADO)
                .then(function (response) {
                    documentFactory.setAuthorization(response.data);
                    $scope.hideLoadingFunction();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.toogleShowPassword = function () {
                var passInput = document.getElementById("nPass");
                var passCInput = document.getElementById("nPassC");
                if (passInput.type === "password") {
                    passInput.type = "text";
                    passCInput.type = "text";
                } else {
                    passInput.type = "password";
                    passCInput.type = "password";
                }
            }

            $scope.ChangePassword = function () {
                if ($scope.passReferee != null && $scope.passRefereeC != null) {
                    if ($scope.passReferee == $scope.passRefereeC) {
                        var information = {
                            idReferee: $scope.idRefereeContent,
                            newPassword: $scope.passReferee
                        };
                        $scope.showLoadingFunction();
                        documentFactory.actionPostFunction(CONSTANT.URL_API_CHANGEPASSWORD, information)
                            .then(function (response) {
                                $scope.hideLoadingFunction();
                                if (response.data.result) {
                                    $scope.passReferee = null;
                                    $scope.passRefereeC = null;
                                    $scope.showModal(null, null, 'INFORMACIÓN', 'Contraseña actualizada correctamente.!', false, 'OK', '');
                                    return;
                                } else {
                                    $scope.showModal(null, null, 'ERROR!', 'No se pudo actualizar la constraseña!', false, 'OK', '');
                                    $scope.passReferee = null;
                                    $scope.passRefereeC = null;
                                }
                            }, function (error) {
                                $scope.hideLoadingFunction();
                                $scope.passReferee = null;
                                $scope.passRefereeC = null;
                                console.log(error);
                                $scope.showModal(null, null, 'ERROR!', 'No se pudo actualizar la constraseña!', false, 'OK', '');
                            });
                    } else {
                        $scope.showModal(null, null, 'INFORMACIÓN', 'Por favor verifique que ambas contraseñas ingresadas sean iguales.', false, 'OK', '');
                    }
                }
            }

            ///////////////////////// ------------ Modal ------------- ////////////////////////////
            // Funcion para mostrar el modal
            $scope.showModal = function (datos, callBackMethodName, modalTitle, modalContent, modalCancelShow, modalOkButtonTitle, modalCancelButtonTitle) {
                $rootScope.$emit("CallModelMethod",
                    {
                        datos: datos,
                        callBackMethodName: callBackMethodName,
                        modalTitle: modalTitle,
                        modalContent: modalContent,
                        modalCancelShow: modalCancelShow != null ? modalCancelShow : true,
                        modalOkButtonTitle: modalOkButtonTitle != null ? modalOkButtonTitle : 'OK',
                        modalCancelButtonTitle: modalCancelButtonTitle != null ? modalCancelButtonTitle : 'Cancelar'
                    });
            }
            
        //---------------------------- FIN DEL CONTROLADOR -------------------------------------
        }]);

})();


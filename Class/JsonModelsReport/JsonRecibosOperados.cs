﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonRecibosOperados
    {
        public string contable { get; set; }
        public int tempContable {
            get
            {
                try
                {
                    int tem = Int32.Parse(contable);
                    return tem;
                }
                catch (Exception)
                {

                    return 0;
                }
            }
        }
        private decimal tempEfectivo { get; set; }
        public decimal efectivo {
            get
            {
                return Math.Round(tempEfectivo, 2);
            }
            set { tempEfectivo = value; }
        }
        private decimal tempCheque { get; set; }
        public decimal cheque {
            get
            {
                return Math.Round(tempCheque, 2);
            }
            set { tempCheque = value; }
        }
        private decimal tempTarjeta { get; set; }
        public decimal tarjeta {
            get
            {
                return Math.Round(tempTarjeta, 2);
            }
            set { tempTarjeta = value; }
        }
        private decimal tempTransaccion { get; set; }
        public decimal transaccion
        {
            get
            {
                return Math.Round(tempTransaccion, 2);
            }
            set { tempTransaccion = value; }
        }
        private decimal tempTotal { get; set; }
        public decimal total {
            get
            {
                return Math.Round(tempTotal, 2);
            }
            set { tempTotal = value; }
        }
        public string nombre { get; set; }
        public string banco { get; set; }
        public string documento { get; set; }
        public string rechazado { get; set; }
    }
}
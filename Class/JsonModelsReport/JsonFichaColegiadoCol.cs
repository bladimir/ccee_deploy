﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonFichaColegiadoCol
    {
        public int Col_NoColegiado { get; set; }
        public string nombre { get; set; }
        public string lugarNacimiento { get; set; }
        public DateTime? tempColegiacion { get; set; }
        public string Col_FechaColegiacion {
            get
            {
                return ((tempColegiacion == null) ? "" : tempColegiacion.Value.ToString("dd/MM/yyyy"));
            }
        }
        public DateTime? nacimiento { get; set; }
        public string Col_FechaNacimiento {
            get
            {
                return ((nacimiento == null) ? "" : nacimiento.Value.ToString("dd/MM/yyyy")) ;
            }
        }
        public string Col_Dpi { get; set; }
        public int? tempSexo { get; set; }
        public string sexo { get
            {
                switch (tempSexo)
                {
                    case 0:
                        return "Masculino";
                    case 1:
                        return "Femenino";
                    default:
                        return "";
                }
            }
        }
        public int? tempCivil { get; set; }
        public string civil {
            get
            {
                switch (tempCivil)
                {
                    case 0:
                        return "NA";
                    case 1:
                        return "Soltero/a";
                    case 2:
                        return "Casado/a";
                    case 3:
                        return "Separado/a";
                    case 4:
                        return "Viudo/a";
                    default:
                        return "";
                }
            }
        }
        public DateTime? juramentacion { get; set; }
        public string Col_FechaJuramentacion
        {
            get
            {
                return ((juramentacion == null) ? "" : juramentacion.Value.ToString("dd/MM/yyyy"));
            }
        }
        public string nacionalidad { get; set; }
        public string status { get; set; }
    }
}
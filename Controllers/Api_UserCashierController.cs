﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.Cashier;
using webcee.Class.JsonModels.UserCash;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_UserCashierController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_UserCashier
        public IEnumerable<ResponseCashierJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                var tempCashier = from caj in db.ccee_Cajero
                                  select new ResponseCashierJson()
                                  {
                                      id = caj.Cjo_NoCajero,
                                      active = caj.Cjo_Activo,
                                      initHour = caj.Cjo_HoraInicia,
                                      finalHour = caj.Cjo_HoraFin
                                  };
                return tempCashier;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_UserCashier/5
        public IEnumerable<ResponseUserCashJson> Get(int id)
        {
            string autorization = Request.Headers.Authorization.Parameter;
            if (!Autorization.enableAutorization(db, autorization)) return null;
            if (id == Constant.user_cash_get_list_no_assing)
            {
                try
                {
                    var tempCashier = from caj in db.ccee_Cajero
                                      where caj.fk_Usuario == 0 || caj.fk_Usuario == null
                                      select new ResponseUserCashJson()
                                      {
                                          id = caj.Cjo_NoCajero,
                                          active = caj.Cjo_Activo,
                                          initHour = caj.Cjo_HoraInicia,
                                          finalHour = caj.Cjo_HoraFin,
                                          name = caj.Cjo_Name
                                      };
                    Autorization.saveOperation(db, autorization, "Obteniendo usuario cajero no asignado");
                    return tempCashier;
                }
                catch (Exception)
                {
                    return null;
                }
            }
            else if(id == Constant.user_cash_get_list_assing)
            {

                try
                {
                    var tempCashier = from caj in db.ccee_Cajero
                                      join usu in db.ccee_Usuario on caj.fk_Usuario equals usu.Usu_NoUsuario
                                      where caj.fk_Usuario != 0 && caj.fk_Usuario != null
                                      select new ResponseUserCashJson()
                                      {
                                          id = caj.Cjo_NoCajero,
                                          active = caj.Cjo_Activo,
                                          initHour = caj.Cjo_HoraInicia,
                                          finalHour = caj.Cjo_HoraFin,
                                          idUser = usu.Usu_NoUsuario,
                                          name = usu.Usu_Nombre,
                                          nameCashier = caj.Cjo_Name
                                      };
                    Autorization.saveOperation(db, autorization, "Obteniendo usuario cajero asignados");
                    return tempCashier;
                }
                catch (Exception)
                {
                    return null;
                }

            }
            else
            {
                return null;
            }
        }

        // POST: api/Api_UserCashier
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_UserCashier/5
        public IHttpActionResult Put(int id, RequestUserCashJson userCash)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_Cajero tempCashier = db.ccee_Cajero.Find(id);

                if (tempCashier == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempCashier.fk_Usuario = userCash.idUser;
                tempCashier.Cjo_Name = userCash.nameUser;

                db.Entry(tempCashier).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando usuario cajero");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_UserCashier/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

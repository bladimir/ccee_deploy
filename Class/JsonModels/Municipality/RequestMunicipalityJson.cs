﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Municipality
{
    public class RequestMunicipalityJson
    {
        public string name { get; set; }
        public int idDepartament { get; set; }
    }
}
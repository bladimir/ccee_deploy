﻿(function () {
    angular.module('document')
        .controller('typeDocumentAccounting', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'documentFactory', function ($scope, $http, CONSTANT, $anchorScroll, documentFactory) {
            $scope.listData;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idUpdate = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                documentFactory.actionGetFunction(CONSTANT.URL_API_TYPEDOCUMENTACCOUNTING)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listData = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.setAuth = function () {
                documentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        documentFactory.setAuthorization(response.data);
                        $scope.init();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.get = function () {
                $scope.showLoadingFunction();
                documentFactory.actionGetFunction(CONSTANT.URL_API_TYPEDOCUMENTACCOUNTING)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listData = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.add = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.typename,
                    description: $scope.typedescription,
                    templateXML: $scope.typetemplate
                }
                console.log(information);
                documentFactory.actionPostFunction(CONSTANT.URL_API_TYPEDOCUMENTACCOUNTING, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.get();
                                $scope.cleanData();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (data) {
                $scope.idUpdate = data.id;
                $scope.typename = data.name;
                $scope.typedescription = data.description;
                $scope.typetemplate = data.templateXML;
                $anchorScroll();
            }

            $scope.update = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.typename,
                    description: $scope.typedescription,
                    templateXML: $scope.typetemplate
                }
                documentFactory.actionPutFunction(CONSTANT.URL_API_TYPEDOCUMENTACCOUNTING, $scope.idUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.get();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.delete = function (data) {
                $scope.showLoadingFunction();
                documentFactory.actionDeleteFunction(CONSTANT.URL_API_TYPEDOCUMENTACCOUNTING, data.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.get();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idUpdate = 0;
                $scope.typename = "";
                $scope.typedescription = "";
                $scope.typetemplate = "";
                $scope.typeconsumable = "";
            }



        }]);

})()
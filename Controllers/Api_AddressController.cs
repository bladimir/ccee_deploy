﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.Address;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_AddressController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Address
        public void Get()
        {
            
        }

        // GET: api/Api_Address/5
        public IEnumerable<ResponseAddressJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempAdress = from dir in db.ccee_Direccion
                                 join mun in db.ccee_Municipio on dir.fk_Municipio equals mun.Mun_NoMunicipio
                                 join dep in db.ccee_Departamento on mun.fk_Departamento equals dep.Dep_NoDepartamento
                                 join pai in db.ccee_Pais on dep.fk_Pais equals pai.Pai_NoPais
                                 where dir.fk_Colegiado == id
                                 select new ResponseAddressJson
                                 {
                                     idAddress = dir.Dir_NoDireccion,
                                     status = Constant.status_code_success,
                                     street = dir.Dir_CalleAvenida,
                                     number = dir.Dir_Numero,
                                     zone = dir.Dir_Zona,
                                     colony = dir.Dir_Colonia,
                                     caserio = dir.Dir_Caserio,
                                     barrio = dir.Dir_Barrio,
                                     typeAddress = dir.Dir_TipoDireccion,
                                     municipality = mun.Mun_NombreMun,
                                     departament = dep.Dep_NombreDep,
                                     country = pai.Pai_NombrePais,
                                     notification = dir.Dir_Notificacion
                                 };
                Autorization.saveOperation(db, autorization, "Obteniendo direcciones: " + id);
                return tempAdress;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_Address
        public IHttpActionResult Post(RequestAddressJson addressJson)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return Ok(HttpStatusCode.BadRequest);

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        if(addressJson.notification == 1)
                        {
                            List<ccee_Direccion> listAddress = (from dir in db.ccee_Direccion
                                                               where dir.fk_Colegiado == addressJson.idReferee
                                                               && dir.Dir_Notificacion == 1
                                                               select dir).ToList();

                            foreach (ccee_Direccion address in listAddress)
                            {
                                address.Dir_Notificacion = 0;
                                db.Entry(address).State = EntityState.Modified;
                            }


                        }

                        ccee_Direccion tempAddress = new ccee_Direccion()
                        {
                            Dir_Caserio = addressJson.caserio,
                            Dir_Barrio = addressJson.barrio,
                            Dir_CalleAvenida = addressJson.street,
                            Dir_Colonia = addressJson.colony,
                            Dir_Numero = addressJson.number,
                            Dir_TipoDireccion = (addressJson.typeAddress.Value == 0) ? 0 : addressJson.typeAddress.Value,
                            Dir_Zona = addressJson.zone,
                            Dir_Notificacion = addressJson.notification,
                            fk_Colegiado = addressJson.idReferee,
                            fk_Municipio = (addressJson.municipality.Value == 0) ? 100 : addressJson.municipality.Value
                        };
                        db.ccee_Direccion.Add(tempAddress);
                        Autorization.saveOperation(db, autorization, "Agregando direccion");
                        db.SaveChanges();
                        transaction.Commit();
                        return Ok(HttpStatusCode.OK);
                    }
                    catch (Exception)
                    {
                        return Ok(HttpStatusCode.BadRequest);
                    }
                }

                
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }


        }

        // PUT: api/Api_Address/5
        public IHttpActionResult Put(int id, RequestAddressJson addressJson)
        {

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    string autorization = Request.Headers.Authorization.Parameter;
                    if (!Autorization.enableAutorization(db, autorization)) return null;

                    if (addressJson.notification == 1)
                    {
                        List<ccee_Direccion> listAddress = (from dir in db.ccee_Direccion
                                                            where dir.fk_Colegiado == addressJson.idReferee
                                                            && dir.Dir_Notificacion == 1
                                                            select dir).ToList();

                        foreach (ccee_Direccion address in listAddress)
                        {
                            address.Dir_Notificacion = 0;
                            db.Entry(address).State = EntityState.Modified;
                        }


                    }

                    ccee_Direccion tempAddress = db.ccee_Direccion.Find(id);
                    if (tempAddress != null)
                    {
                        tempAddress.Dir_Barrio = addressJson.barrio;
                        tempAddress.Dir_CalleAvenida = addressJson.street;
                        tempAddress.Dir_Caserio = addressJson.caserio;
                        tempAddress.Dir_Colonia = addressJson.colony;
                        tempAddress.Dir_Numero = addressJson.number;
                        tempAddress.Dir_TipoDireccion = ((addressJson.typeAddress == 0 || addressJson.municipality == null) ? tempAddress.Dir_TipoDireccion : addressJson.typeAddress);
                        tempAddress.Dir_Zona = addressJson.zone;
                        tempAddress.Dir_Notificacion = addressJson.notification;
                        tempAddress.fk_Municipio = ((addressJson.municipality == 0 || addressJson.municipality == null) ? tempAddress.fk_Municipio : addressJson.municipality.Value);

                        db.Entry(tempAddress).State = EntityState.Modified;
                        Autorization.saveOperation(db, autorization, "Actualizando direccion");
                        db.SaveChanges();
                        transaction.Commit();
                        return Ok(Constant.status_code_success);

                    }
                    return Ok(id);
                }
                catch (Exception)
                {
                    return Ok(Constant.status_code_error);
                }
            }
            
        }

        // DELETE: api/Api_Address/5
        public IHttpActionResult Delete(int id)
        {
            string autorization = Request.Headers.Authorization.Parameter;
            if (!Autorization.enableAutorization(db, autorization)) return null;

            ccee_Direccion tempAddress = db.ccee_Direccion.Find(id);
            if (tempAddress == null)
            {
                return Ok(HttpStatusCode.NotFound);
            }

            try
            {
                db.ccee_Direccion.Remove(tempAddress);
                Autorization.saveOperation(db, autorization, "Eliminando direccion id:" + id);
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

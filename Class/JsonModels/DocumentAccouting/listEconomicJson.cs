﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.DocumentAccouting
{
    public class listEconomicJson
    {
        public int idEconomic { get; set; }
        public decimal? amount { get; set; }
        public int idTypeEconomic { get; set; }
        public decimal? total { get; set; }
    }
}
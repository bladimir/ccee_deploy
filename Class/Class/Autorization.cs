﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webcee.Models;

namespace webcee.Class.Class
{
    public class Autorization
    {

        CCEEEntities db;

        public Autorization(CCEEEntities db)
        {
            this.db = db;
        }

        public string createToken(int idUser, string nameUser)
        {
            int count = db.Autorizacion.Count();
            string token = Constant.Constant.varToken + (count + 1);
            token = GeneralFunction.MD5Encrypt(token);
            Autorizacion autorization = new Autorizacion();
            autorization.Aut_Clave = token;
            autorization.Aut_Fecha = DateTime.Today;
            db.Autorizacion.Add(autorization);
            db.SaveChanges();
            token = token + "_" + idUser + "_" + nameUser;
            token = GeneralFunction.Base64Encode(token);
            return token;
        }

        public static bool enableAutorization(CCEEEntities db, string data)
        {
            string infoBase64 = GeneralFunction.Base64Decode(data);
            string[] divid = infoBase64.Split('_');
            if (divid.Length != 3) return false;
            string key = divid[0];
            Autorizacion aut = db.Autorizacion.Where(p=>p.Aut_Clave == key).FirstOrDefault();
            if (aut == null) return false;
            if (aut.Aut_Fecha != DateTime.Today) return false;
            return true;
        }

        public static void saveOperation(CCEEEntities db, string data, string operation)
        {
            HistorialOperaciones historialOperation = new HistorialOperaciones();
            string infoBase64 = GeneralFunction.Base64Decode(data);
            string[] divid = infoBase64.Split('_');
            string idUser = divid[1];
            string nameUser = divid[2];
            historialOperation.HisOpe_IdUsuario = Convert.ToInt32(idUser);
            historialOperation.HisOpe_NombreUsuario = nameUser;
            historialOperation.HisOpe_Operacion = operation;
            historialOperation.HisOpe_Fecha = DateTime.Now;
            db.HistorialOperaciones.Add(historialOperation);
            db.SaveChanges();
        }
        


    }
}
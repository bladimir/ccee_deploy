﻿(function () {
    angular.module('generals')
        .controller('typeBeneficiary', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listTypeBeneficiary;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idTypeBeneficiaryUpdate = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }


            $scope.init = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_TYPEBENEFICIARY)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listTypeBeneficiary = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getTypeEspecialty = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_TYPEBENEFICIARY)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listTypeBeneficiary = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.addTypeBeneficiary = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.typeName,
                    description: $scope.tyepDescription
                }
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_TYPEBENEFICIARY, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.cleanData();
                                $scope.getTypeEspecialty();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (type) {
                $scope.idTypeBeneficiaryUpdate = type.id;
                $scope.typeName = type.name;
                $scope.tyepDescription = type.description;
                $anchorScroll();
            }

            $scope.updateTypeBeneficiary = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.typeName,
                    description: $scope.tyepDescription
                }
                generalactionsFactory.actionPutFunction(CONSTANT.URL_API_TYPEBENEFICIARY, $scope.idTypeBeneficiaryUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.getTypeEspecialty();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }


            $scope.deleteTypeBeneficiary = function (type) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionDeleteFunction(CONSTANT.URL_API_TYPEBENEFICIARY, type.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getTypeEspecialty();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idTypeBeneficiaryUpdate = 0;
                $scope.typeName = "";
                $scope.tyepDescription = "";
            }


        }]);

})()
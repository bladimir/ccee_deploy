﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.Cellar;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_CellarPageController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_CellarPage
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_CellarPage/5
        public string Get(int id)
        {
            return "value";
        }


        [Route("api/Api_CellarPage/{cashier}/{valueTimbre}/{date}/{page}")]
        public List<ResponseCellarAssingJson> Get(int cashier, decimal valueTimbre, string date, string page)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var pLis = db.Database.SqlQuery<ResponseCellarAssingJson>(queryData(cashier, valueTimbre, date, page)).ToList();
                for (int i = 0; i < pLis.Count; i++)
                {
                    pLis[i].nameCashier = getCashier(pLis[i].cashier);
                }

                var listTimbre = pLis
                .GroupBy(o => new
                {
                    id = o.id,
                    numberPage = o.numberPage,
                    dateCreate = o.dateCreate,
                    value = o.value,
                    cashier = o.cashier,
                    nameCashier = o.nameCashier,
                })
                .Select(g => new ResponseCellarAssingJson
                {
                    id = g.Key.id,
                    numberPage = g.Key.numberPage,
                    dateCreate = g.Key.dateCreate,
                    value = g.Key.value,
                    cashier = g.Key.cashier,
                    nameCashier = g.Key.nameCashier,
                    countTimbre = g.Count(),
                    status = g.Max(o=>o.status),
                    countEnable = g.Where(o=>o.status == 1).Count(),
                    timbres = g.Select(p => new ListTimbresimpleJson()
                                                { noTimbre = p.noTimbre,
                                                    numberTimbre = p.timbre,
                                                    statusTimbre = p.status
                                                }).ToList()
                })
                .OrderBy(a => a.numberPage)
                .ToList();
                Autorization.saveOperation(db, autorization, "Obteniendo hoja timbre asignada");
                return listTimbre;
            }
            catch (Exception)
            {
                return null;
            }
        }


        // POST: api/Api_CellarPage
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_CellarPage/5
        public IHttpActionResult Put(int id, RequestPutCellarAssingJson value)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                if (id == 1)
                {

                    ccee_HojaTimbre tempPage = db.ccee_HojaTimbre.Find(value.id);
                    if(tempPage == null)
                    {
                        return Ok(HttpStatusCode.NotFound);
                    }
                    tempPage.fk_Cajero = null;
                    db.Entry(tempPage).State = EntityState.Modified;
                    db.SaveChanges();
                    Autorization.saveOperation(db, autorization, "Actualizando asignacion hoja timbre");
                    return Ok(HttpStatusCode.OK);

                }
                else if(id == 2)
                {
                    ccee_Timbre tempTimbre = db.ccee_Timbre.Find(value.id);
                    if(tempTimbre == null)
                    {
                        return Ok(HttpStatusCode.NotFound);
                    }
                    tempTimbre.Tim_Estatus = Constant.timbre_status_out;
                    db.Entry(tempTimbre).State = EntityState.Modified;
                    db.SaveChanges();
                    Autorization.saveOperation(db, autorization, "Actualizando estatus hoja timbre");
                    return Ok(HttpStatusCode.OK);
                }
                return Ok(HttpStatusCode.BadRequest);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_CellarPage/5
        public void Delete(int id)
        {
        }


        private string queryData(int cashier, decimal valueTimbre, string date, string page)
        {
            string queryPage = ((!page.Equals("0")) ? GeneralFunction.generateQuery("[HoT_NumeroHoja]", page, Constant.text_query_is_number) : "") + " ";
            string[] divDate = date.Split('_');
            string queryDate = "";
            if (!date.Equals("0")){
                if (divDate.Count() > 1)
                {
                    queryDate += "and [HoT_FechaCreacion] >= '" + divDate[0] + "' ";
                    queryDate += "and [HoT_FechaCreacion] <= '" + divDate[1] + "' ";
                }
            }
            

            string query = "select [HoT_NoHojaTimbre] as id, [HoT_NumeroHoja] as numberPage, [HoT_FechaCreacion] as dateCreate, [Tim_Valor] as value, " +
                            "[Tim_NoTimbre] as noTimbre, [Tim_NumeroTimbre] as timbre, [fk_Cajero] as cashier, [Tim_Estatus] as status " +
                            "from[ccee_HojaTimbre], [ccee_Timbre] " +
                            "where[ccee_Timbre].[fk_HojaTimbre] = [ccee_HojaTimbre].[HoT_NoHojaTimbre] " +
                            "and [fk_Cajero] is not null " +
                                    ((cashier > 0) ? "and [fk_Cajero] = " + cashier + " " : "") +
                                    ((valueTimbre > 0m) ? "and [Tim_Valor] = " + valueTimbre + " " : "") +
                                    queryDate +
                                    queryPage ;

            return query;
        }

        private string getCashier(int? cashier)
        {
            if (cashier == null) return "";
            return db.ccee_Cajero.Find(cashier.Value).Cjo_Name;
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

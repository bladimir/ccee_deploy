﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using webcee.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.Event;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_EventController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Event
        public List<ResponseEventJson> Get()
        {
            List<ResponseEventJson> listEvent = new List<ResponseEventJson>();
            DateTime date = DateTime.Now;
            var tempEvent = db.ccee_Evento
                            .Where(p => p.Eve_Fin >= date);

            foreach ( ccee_Evento contEve in tempEvent )
            {
                string initiationDate = GeneralFunction.changeDateWithCero(contEve.Eve_Inicio.Value.Day) 
                                        + "/" + GeneralFunction.changeDateWithCero(contEve.Eve_Inicio.Value.Month )
                                        + "/" + contEve.Eve_Inicio.Value.Year;

                int t = contEve.Eve_Inicio.Value.Day;

                string finalizenDate = GeneralFunction.changeDateWithCero(contEve.Eve_Fin.Value.Day)
                                        + "/" + GeneralFunction.changeDateWithCero(contEve.Eve_Fin.Value.Month)
                                        + "/" + contEve.Eve_Fin.Value.Year;

                listEvent.Add(new ResponseEventJson() {

                    id = contEve.Eve_NoEvento,
                    name = contEve.Eve_NombreEvento,
                    description = contEve.Eve_Descripcion,
                    initation = initiationDate,
                    finalize = finalizenDate,
                    status = contEve.Eve_Estatus.Value
                    

                });
            }

            return listEvent;
        }

        // GET: api/Api_Event/5
        [ResponseType(typeof(ccee_Evento))]
        public void Get(int id)
        {


        }

        // PUT: api/Api_Event/5
        public IHttpActionResult Put(int id, RequestEventJson eventJson)
        {

            try
            {

                DateTime dateIn = GeneralFunction.changeStringofDate(eventJson.initation);
                DateTime dateFi = GeneralFunction.changeStringofDate(eventJson.finalize);

                if (dateIn.Year == 1990 || dateFi.Year == 1990) return Ok(Constant.status_code_error);

                ccee_Evento tempEvent = db.ccee_Evento.Find(id);
                if (tempEvent != null)
                {
                    tempEvent.Eve_NombreEvento = eventJson.name;
                    tempEvent.Eve_Descripcion = eventJson.description;
                    tempEvent.Eve_Inicio = dateIn;
                    tempEvent.Eve_Fin = dateFi;
                    tempEvent.Eve_Estatus = eventJson.status;

                    db.Entry(tempEvent).State = EntityState.Modified;
                    db.SaveChanges();
                    return Ok(Constant.status_code_success);

                }
                else
                {
                    return Ok(Constant.status_code_error);
                }
            }
            catch
            {
                return Ok(Constant.status_code_error);
            }


        }

        // POST: api/Api_Event
        public IHttpActionResult Post(RequestEventJson eventJson)
        {

            if (eventJson == null) return Ok(Constant.status_code_error);

            DateTime dateIn = GeneralFunction.changeStringofDate(eventJson.initation);
            DateTime dateFi = GeneralFunction.changeStringofDate(eventJson.finalize);

            if(dateIn.Year == 1990 || dateFi.Year == 1990) return Ok(Constant.status_code_error);

            ccee_Evento tempEvent = new ccee_Evento()
            {
                Eve_NombreEvento = eventJson.name,
                Eve_Descripcion = eventJson.description,
                Eve_Inicio = dateIn,
                Eve_Fin = dateFi,
                Eve_Estatus = eventJson.status

            };

            try
            {
                db.ccee_Evento.Add(tempEvent);
                db.SaveChanges();
                return Ok(Constant.status_code_success);
            }
            catch
            {
                return Ok(Constant.status_code_error);
            }
            
        }

        // DELETE: api/Api_Event/5
        [ResponseType(typeof(ccee_Evento))]
        public void Delete(int id)
        {
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ccee_EventoExists(int id)
        {
            return db.ccee_Evento.Count(e => e.Eve_NoEvento == id) > 0;
        }
    }
}
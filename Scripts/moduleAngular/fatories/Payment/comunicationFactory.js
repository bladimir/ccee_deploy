﻿(function () {
    angular.module('payment')
        .factory('comunicationFactory', ['CONSTANT', function (CONSTANT) {

            var paymentFactory = {};
            var idReferee = 0;

            paymentFactory.actionGetFunction = function (dirr) {
                return $http.get(dirr);
            };

            paymentFactory.setIdReferee = function (id) {
                console.log("Numero: " + id);
                idReferee = id;
            };

            paymentFactory.getIdReferee = function () {
                return idReferee;
            };

            return paymentFactory;
        }]);

})()
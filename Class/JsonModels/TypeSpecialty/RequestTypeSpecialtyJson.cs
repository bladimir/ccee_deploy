﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.TypeSpecialty
{
    public class RequestTypeSpecialtyJson
    {
        public string name { get; set; }
        public string degree { get; set; }
        public string abbreviation { get; set; }
    }
}
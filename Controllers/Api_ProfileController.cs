﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using webcee.Class.JsonModels;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_ProfileController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Profile
        public List<DataProfileJson> Getccee_Perfil()
        {
            var profiles = db.ccee_Perfil.ToList();
            List<DataProfileJson> listProfile = new List<DataProfileJson>();
            foreach (ccee_Perfil dataUser in profiles)
            {
                listProfile.Add(new DataProfileJson()
                {
                    id = dataUser.Per_NoPerfil,
                    nombre = dataUser.Per_Nombre
                });
            } 
            return listProfile;
        }

        // GET: api/Api_Profile/5
        [ResponseType(typeof(ccee_Perfil))]
        public IHttpActionResult Getccee_Perfil(int id)
        {
            ccee_Perfil ccee_Perfil = db.ccee_Perfil.Find(id);
            if (ccee_Perfil == null)
            {
                return NotFound();
            }

            return Ok(ccee_Perfil);
        }

        // PUT: api/Api_Profile/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putccee_Perfil(int id, ccee_Perfil ccee_Perfil)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ccee_Perfil.Per_NoPerfil)
            {
                return BadRequest();
            }

            db.Entry(ccee_Perfil).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ccee_PerfilExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Api_Profile
        [ResponseType(typeof(ccee_Perfil))]
        public IHttpActionResult Postccee_Perfil(ccee_Perfil ccee_Perfil)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ccee_Perfil.Add(ccee_Perfil);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = ccee_Perfil.Per_NoPerfil }, ccee_Perfil);
        }

        // DELETE: api/Api_Profile/5
        [ResponseType(typeof(ccee_Perfil))]
        public IHttpActionResult Deleteccee_Perfil(int id)
        {
            ccee_Perfil ccee_Perfil = db.ccee_Perfil.Find(id);
            if (ccee_Perfil == null)
            {
                return NotFound();
            }

            db.ccee_Perfil.Remove(ccee_Perfil);
            db.SaveChanges();

            return Ok(ccee_Perfil);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ccee_PerfilExists(int id)
        {
            return db.ccee_Perfil.Count(e => e.Per_NoPerfil == id) > 0;
        }
    }
}
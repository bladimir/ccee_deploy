﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using webcee.Models;

namespace webcee.Class.Class
{
    public class Payment
    {
        private CCEEEntities db;

        public class contentIdPay
        {
            int idPay;
        }

        public Payment(CCEEEntities db)
        {
            this.db = db;
        }

        public List<int?> findIdPayWithDoc(int idDoc)
        {
            List<int?> pays = new List<int?>();
            try
            {
                
                
                var idPayC = db.ccee_CargoEconomico_colegiado
                                    .Where(p => p.fk_DocContable == idDoc)
                                    .Select(o => o.fk_Pago);

                var idPayT = db.ccee_CargoEconomico_timbre
                                    .Where(p => p.fk_DocContable == idDoc)
                                    .Select(o => o.fk_Pago);

                var idPayO = db.ccee_CargoEconomico_otros
                                    .Where(p => p.fk_DocContable == idDoc)
                                    .Select(o => o.fk_Pago);

                pays.AddRange(idPayC);
                pays.AddRange(idPayT);
                pays.AddRange(idPayO);
                pays = pays.Distinct().ToList();

                return pays;
            }
            catch (Exception)
            {
                return pays;
            }
        }


        public bool reversePayment(int idPay)
        {
            
            try
            {

                var listReferre = db.ccee_CargoEconomico_colegiado
                                    .Where(p => p.fk_Pago == idPay);
                foreach (ccee_CargoEconomico_colegiado referee in listReferre)
                {
                    referee.fk_Pago = null;
                    referee.fk_DocContable = null;
                    db.Entry(referee).State = EntityState.Modified;
                }

                var listTimbre = db.ccee_CargoEconomico_timbre
                                    .Where(p => p.fk_Pago == idPay);
                foreach (ccee_CargoEconomico_timbre timbre in listTimbre)
                {
                    timbre.fk_Pago = null;
                    timbre.fk_DocContable = null;
                    db.Entry(timbre).State = EntityState.Modified;
                }

                var listOther = db.ccee_CargoEconomico_otros
                                    .Where(p => p.fk_Pago == idPay);
                foreach (ccee_CargoEconomico_otros other in listOther)
                {
                    other.fk_Pago = null;
                    other.fk_DocContable = null;
                    db.Entry(other).State = EntityState.Modified;
                }

                var listMethod = db.ccee_MedioPago_Pago_C
                                    .Where(p => p.fk_Pago == idPay);

                foreach (ccee_MedioPago_Pago_C method in listMethod)
                {
                    method.fk_Pago = null;
                    if(method.fk_MedioPago >= 46 && method.fk_MedioPago <= 48)
                    {
                        method.MPP_NoTransaccion = "ANUL_" + method.MPP_NoTransaccion;
                    }
                    db.Entry(method).State = EntityState.Modified;
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }




    }
}
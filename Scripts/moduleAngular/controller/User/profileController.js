﻿(function () {
    angular.module('user')
        .controller('profile', ['$scope', '$http', 'CONSTANTE', '$anchorScroll', 'userFactory', function ($scope, $http, CONSTANTE, $anchorScroll, userFactory) {
            
            $scope.showDelete = false;
            $scope.showNotify = CONSTANTE.NOTIFY_SHOW_CSS;
            $scope.idUser = 0;


            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANTE.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANTE.NOTIFY_HIDE_CSS;
            }

            $scope.setAuth = function (idUser) {
                userFactory.actionGetFunction(CONSTANTE.URL_GET_TOKEN)
                    .then(function (response) {
                        userFactory.setAuthorization(response.data);
                        $scope.init(idUser);
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.init = function (idUser) {
                userFactory.actionGetIntFunction(CONSTANTE.URL_API_USER, idUser)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        if (response.data != null) {
                            $scope.idUser = idUser;
                            $scope.email = response.data.correo;
                            $scope.name = response.data.nombre;
                            $scope.last = response.data.apellido;
                            $scope.tel = response.data.telefonoResidencial;
                            $scope.cel = response.data.telefonoMovil;
                            $scope.question = response.data.question;
                            $scope.answer = response.data.answer;

                        }
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.update = function () {
                var information = {
                    nombre: $scope.name,
                    apellido: $scope.last,
                    telefonoMovil: $scope.cel,
                    telefonoResidencial: $scope.tel,
                    idProfile: 0,
                    question: $scope.question,
                    answer: $scope.answer,
                    oldPass: $scope.oldPass,
                    newPass: $scope.newPass,
                    isAdmin: false
                }
                console.log(information);
                userFactory.actionPutFunction(CONSTANTE.URL_API_USER, $scope.idUser, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.error = "Información actualizada exitosamente";
                        } else if (response.data == 404) {
                            $scope.error = "Información general actualizada, contraseña actual no coincide";
                        } else {
                            $scope.error = "Información no actualizada";
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
            }



        }]);

})()
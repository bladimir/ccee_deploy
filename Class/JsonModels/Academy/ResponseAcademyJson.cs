﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Academy
{
    public class ResponseAcademyJson
    {
        public int idSpecialty { get; set; }
        public int idUniversity { get; set; }
        public string specialty { get; set; }
        public string university { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public Nullable<DateTime> date { get; set; }
        public string dateText { get; set; }
        public string evaluation { get; set; }
        public string graduationSubject { get; set; }
        public string fullNameUniversity { get; set; }
    }
}
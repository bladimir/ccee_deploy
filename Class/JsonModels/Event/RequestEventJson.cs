﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Event
{
    public class RequestEventJson
    {
        public string name { get; set; }
        public string description { get; set; }
        public string initation { get; set; }
        public string finalize { get; set; }
        public int status { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonAsamblea
    {
        public string apellido { get; set; }
        public string nombre { get; set; }
        public int colegiado { get; set; }
    }
}
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace webcee.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class dumpfull01
    {
        public int Col_NoColegiado { get; set; }
        public Nullable<decimal> totalcolegiado { get; set; }
        public Nullable<decimal> totaltimbre { get; set; }
        public Nullable<decimal> totalotros { get; set; }
        public Nullable<decimal> deudacolegiado { get; set; }
        public Nullable<decimal> deudatimbre { get; set; }
        public Nullable<decimal> deudaotros { get; set; }
        public Nullable<System.DateTime> FechaUcolegiado { get; set; }
        public Nullable<System.DateTime> FechaUtimbre { get; set; }
    }
}

﻿(function () {
    angular.module('generals')
        .controller('timbresChange', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listCashier = [];
            //$scope.list;
            //$scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.downTimbre = [];
            $scope.upTimbre = [];
            $scope.valueTotal = 0;
            $scope.disableCashier = false;
            $scope.listT = [];
            //$scope.idObject = 0;
            //$scope.regexAmount = CONSTANT.REGEX_AMOUNT;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }



            

            $scope.init = function () {
                $scope.getCashier();
                $scope.valueTimbre();
            }

            $scope.setAuth = function (idCashier) {
                $scope.inCashi = idCashier;
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }


            $scope.getCashier = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_CASHIER)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listCashier = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.valueTimbre = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_VALUETIMBRE)
                    .then(function (response) {
                        console.log(response);
                        $scope.listT = response.data;
                    }, function (error) {
                        console.log(error);
                    });
            }


            $scope.splitArray = function (noTimbre) {
                var listTimbre = noTimbre.split('-');
                for (var i = 0; i < listTimbre.length; i++) {

                }
            }


            $scope.getInfoTimbreDown = function (noTimbre, noPage) {
                var listTimbre = noTimbre.split('-');
                console.log(listTimbre);
                var fist = listTimbre[0];
                var secound = ((listTimbre.length > 1) ? listTimbre[1] : listTimbre[0]);

                $scope.showLoadingFunction();
                generalactionsFactory.actionGetGetGetGetGetGetIntFunction(CONSTANT.URL_API_TIMBRECHANGE, noPage, fist, secound, 0, false, $scope.selectValueDow.id)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    if (response.data != null) {
                        for (var i = 0; i < response.data.length; i++) {
                            $scope.downTimbre.push(response.data[i]);
                        }
                        
                        $scope.dowNoTimbre = "";
                        $scope.sumTimbre();
                    }
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.removeDown = function (index) {
                $scope.downTimbre.splice(index, 1);
                $scope.sumTimbre();
            }

            $scope.getInfoTimbreUp = function (noTimbre, noPage) {
                if ($scope.inCashi == null) return;
                var listTimbre = noTimbre.split('-');
                console.log(listTimbre);
                var fist = listTimbre[0];
                var secound = ((listTimbre.length > 1) ? listTimbre[1] : listTimbre[0]);

                $scope.showLoadingFunction();
                generalactionsFactory.actionGetGetGetGetGetGetIntFunction(CONSTANT.URL_API_TIMBRECHANGE, noPage, fist, secound, $scope.inCashi, true, $scope.selectValueUp.id)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    if (response.data != null) {
                        for (var i = 0; i < response.data.length; i++) {
                            $scope.upTimbre.push(response.data[i]);
                        }
                        $scope.upNoTimbre = "";
                        $scope.sumTimbre();
                    }
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.removeUp = function (index) {
                $scope.upTimbre.splice(index, 1);
                $scope.sumTimbre();
            }

            $scope.sumTimbre = function () {
                $scope.valueTotal = 0;
                var len = $scope.downTimbre.length;
                for (var i = 0; i < len; i++) {
                    $scope.valueTotal += $scope.downTimbre[i].valor;
                }
                var lenU = $scope.upTimbre.length;
                for (var i = 0; i < lenU; i++) {
                    $scope.valueTotal -= $scope.upTimbre[i].valor;
                }
                
                $scope.disableCashier = (lenU > 0);

            }

            $scope.changeTimbre = function () {
                $scope.showLoadingFunction();
                var information = {
                    cashier: $scope.inCashi,
                    removeTimbre: $scope.downTimbre,
                    addTimbre: $scope.upTimbre
                }
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_TIMBRECHANGE, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.message = "Timbres cambiados exitosamente";
                                $scope.clear();
                            } else {
                                $scope.message = "Problemas con los timbres";
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.clear = function () {
                $scope.downTimbre = [];
                $scope.upTimbre = [];
                $scope.valueTotal = 0;
                $scope.sumTimbre();
            }


        }]);

})()
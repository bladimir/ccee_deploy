﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.UploadNomina
{
    public class JsonDataResponse
    {
        public string text { get; set; }
        public string url { get; set; }
    }
}
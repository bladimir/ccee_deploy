﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonCorteCajaEfectivo
    {
        public string titulo { get; set; }
        public decimal tempMonto { get; set; }
        public decimal monto {
            get
            {
                return Math.Round(tempMonto, 2);
            }
            set { tempMonto = value; }
        }
        public string tipo { get; set; }
    }
}
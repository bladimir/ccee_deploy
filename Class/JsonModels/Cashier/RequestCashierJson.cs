﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Cashier
{
    public class RequestCashierJson
    {
        public int active { get; set; }
        public string initHour { get; set; }
        public string finalHour { get; set; }
        public string name { get; set; }
    }
}
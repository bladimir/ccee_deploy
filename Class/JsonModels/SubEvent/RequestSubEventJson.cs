﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.SubEvent
{
    public class RequestSubEventJson
    {
        public string description { get; set; }
        public int events { get; set; }
        public string name { get; set; }
        public int document { get; set; }
        public int statusPrint { get; set; }
    }
}
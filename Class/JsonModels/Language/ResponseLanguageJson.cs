﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Language
{
    public class ResponseLanguageJson
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
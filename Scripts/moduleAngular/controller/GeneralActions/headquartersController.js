﻿(function () {
    angular.module('generals')
        .controller('headquarters', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquarter;
            $scope.civilState;
            $scope.idMunicipality = 0;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idHeadquartersUpdate = 0;
            $scope.regexPhone = CONSTANT.REGEX_PHONE;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_COUNTRY)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listCountry = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            
            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }


            $scope.getHeadquar = function () {
                $scope.idMunicipality = $scope.selectMunicipality.id;
                $scope.getHeadquarters($scope.idMunicipality);
            }


            $scope.getDepartament = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_DEPARTAMENT, $scope.selectCountry.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listDepartament = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getMunicipality = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_MUNICIPALITY, $scope.selectDepartament.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listMunicipality = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getHeadquarters = function (id) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_HEADQUARTERS, id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listHeadquarter = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.addHeadquarters = function () {
                $scope.showLoadingFunction();
                var information = {
                    address: $scope.headquaaddress,
                    phone: $scope.headquaphone,
                    coordinates: $scope.headquacoordinated,
                    idMunicipality: $scope.idMunicipality
                }
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_HEADQUARTERS, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getHeadquarters($scope.idMunicipality);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (hea) {
                $scope.idHeadquartersUpdate = hea.id;
                $scope.headquaaddress = hea.address;
                $scope.headquaphone = hea.phone;
                $scope.headquacoordinated = hea.coordinates;
                $anchorScroll();
            }

            $scope.updateHeadquarters = function () {
                $scope.showLoadingFunction();
                var information = {
                    address: $scope.headquaaddress,
                    phone: $scope.headquaphone,
                    coordinates: $scope.headquacoordinated,
                    idMunicipality: $scope.idMunicipality
                }
                generalactionsFactory.actionPutFunction(CONSTANT.URL_API_HEADQUARTERS, $scope.idHeadquartersUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.getHeadquarters($scope.idMunicipality);
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.deleteHeadquarters = function (hea) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionDeleteFunction(CONSTANT.URL_API_HEADQUARTERS, hea.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getHeadquarters($scope.idMunicipality);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idHeadquartersUpdate = 0;
                $scope.headquaaddress = "";
                $scope.headquaphone = "";
                $scope.headquacoordinated = "";
            }

            $scope.viewMaps = function (hea) {
                window.open('/GeneralActions/HeadquartersMaps/' + hea.id, '_blank');
            }


        }]);

})()
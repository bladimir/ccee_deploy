﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.TimbreUser
{
    public class ResponseListTimbreUserJson
    {
        public int idTimbre { get; set; }
        public int idPage { get; set; }
        public decimal? value { get; set; }
        public int? noTimbre { get; set; }
        public int? noPage { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Economic
{
    public class RequestEconomicJson
    {
        public decimal amount { get; set; }
        public int idTypeEconomic { get; set; }
        public int idPay { get; set; }
        public int? idReferee { get; set; }
        public int? idNoReferee { get; set; }
        public string description { get; set; }
        public int? isDemand { get; set; }
        public int idType { get; set; }
    }
}
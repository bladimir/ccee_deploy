﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webcee.Class.JsonModels.PaymentMethod_Pay;
using webcee.Class.JsonModels.RejectedCheck;

namespace webcee.Class.JsonModels.RefundPayment
{
    public class RequestRefundPaymentJson
    {
        public ResponseRejectedCheckJson objectCheck { get; set; }
        public IEnumerable<RequestPaymentMethod_PayJson> jsonContentMethod { get; set; }
    }
}
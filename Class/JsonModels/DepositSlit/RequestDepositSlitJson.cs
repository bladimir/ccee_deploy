﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.DepositSlit
{
    public class RequestDepositSlitJson
    {
        public int idCahier { get; set; }
        public int idBank { get; set; }
        public string date { get; set; }
        public string boleta { get; set; }
    }
}
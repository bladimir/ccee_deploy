﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.DocAccountOld;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_DocAccountOldController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_DocAccountOld
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_DocAccountOld/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_DocAccountOld
        public ResponseDocAccountOldJson Post(RequestDocAccountOldJson infoDoc)
        {
            ResponseDocAccountOldJson response = new ResponseDocAccountOldJson();
            try
            {
                // revisar el idTypeDoc para que envie el id directo
                PrintFile printFile = new PrintFile(infoDoc.idPay, db);
                string referee = infoDoc.NoReferee.ToString();
                string docReferee = printFile.createDocumentReceipt2(infoDoc.idTypeDoc, referee,
                                                    infoDoc.nameReferee, infoDoc.addressReferee, infoDoc.nitReferee,
                                                    infoDoc.remarkDoc, "0",
                                                    infoDoc.nameUserCashier, infoDoc.idDocAccount);
                response.link = docReferee;
                response.status = 200;
                return response;

            }
            catch (Exception)
            {
                response.status = 400;
                return response;
            }

            

        }

        // PUT: api/Api_DocAccountOld/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_DocAccountOld/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

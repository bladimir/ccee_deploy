﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.UploadNomina
{
    public class ResponseUploadNominaJson
    {
        public string nombre { get; set; }
        public int colegiado { get; set; }
        public int mes { get; set; }
        public int anio { get; set; }
        public decimal cuotaTimbre { get; set; }
        public decimal seguroPostumo { get; set; }
        public decimal cuotaColegiado { get; set; }
        public decimal valorRealColegiado { get; set; }
        public decimal valorRealTimbre { get; set; }
        public decimal valorRealPostumo { get; set; }
        public int error { get; set; }
        public string textError { get; set; }
        public decimal? diferencia { get; set; }
        public bool valoresReales { get; set; }
        public int statusSync { get; set; }
    }
}
﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.TimbreSign;
using webcee.Class.JsonModels.UploadNomina;
using webcee.Models;
using webcee.ViewModels;

namespace webcee.Controllers
{
    public class DocumentController : Controller
    {
        // GET: Document
        public ActionResult Index()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, "") == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            return View();
        }

        public ActionResult TypeDocument()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_doc_generales) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult TypeDocumentAccounting()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_doc_contables) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult DocumentAccounting()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, "") == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            return View();
        }

        public ActionResult ValidarDocumento(string id)
        {
            string idDocument = "";
            if(id != null)
            {
                idDocument = GeneralFunction.Base64Decode(id);
            }
            ViewBag.idDocument = idDocument;
            return View();
        }

        

        public ActionResult PruebaUpload()
        {
            return View();
        }

        public ActionResult CancelDocument()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_invalidar_cert) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult ListBatch()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_carga) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult UploadNomina()
        {
            CCEEEntities db = new CCEEEntities();
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_carga) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            @ViewBag.idCahier = login.idCashier;
            @ViewBag.idCash = login.idCash;
            var openClose = (from ope in db.ccee_AperturaCierre
                             join caj in db.ccee_Cajero on ope.fk_Cajero equals caj.Cjo_NoCajero
                             where ope.ApC_NoAperturaCierre == login.idOpenClose
                             select new
                             {
                                 timeI = caj.Cjo_HoraInicia,
                                 timeF = caj.Cjo_HoraFin,
                                 document = ope.ApC_NoRecibo
                             }).FirstOrDefault();

            DateTime now = DateTime.Now;

            if (openClose == null)
            {
                return RedirectToAction("Index", "Referre");
            }

            if (openClose.timeF != null && openClose.timeI != null)
            {
                string[] parseTimeI = openClose.timeI.Split(':');
                int timeIHour;
                string[] parseTimeF = openClose.timeF.Split(':');
                int timeFHour;
                try
                {
                    timeIHour = Convert.ToInt32(parseTimeI[0]);
                    timeFHour = Convert.ToInt32(parseTimeF[0]);
                    int timeHour = now.Hour;
                    //int timeHour = 10;
                    if (!(timeIHour <= timeHour && timeFHour >= timeHour))
                    {
                        return RedirectToAction("Index", "Referre");
                    }
                }
                catch (Exception)
                {
                    return RedirectToAction("Index", "Referre");
                }

            }
            @ViewBag.receipt = openClose.document;

            if (login.idCashier == 0)
            {
                return RedirectToAction("Index", "Referre");
            }


            return View();
        }

        [HttpPost]
        public string UploadNominaData()
        {
            string result = "";
            List<ResponseUploadNominaJson> listNomina = new List<ResponseUploadNominaJson>();
            CCEEEntities db = new CCEEEntities();
            string cent = GeneralFunction.getKeyTableVariable(db, Constant.table_var_aprox_decimal);
            decimal dCent = Convert.ToDecimal(cent);
            try
            {

            
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                
                    BinaryReader b = new BinaryReader(file.InputStream);
                    byte[] binData = b.ReadBytes(file.ContentLength);
                    string line;
                    result = System.Text.Encoding.UTF8.GetString(binData);

                    StringReader strReader = new StringReader(result);
                    strReader.ReadLine();
                    ResponseUploadNominaJson colegiadoNomina;
                    Referee infoReferee;

                    while ((line = strReader.ReadLine()) != null)
                    {
                        string[] tupla = line.Split(',');
                        colegiadoNomina = new ResponseUploadNominaJson();
                        try
                        {
                            //colegiadoNomina.nombre = tupla[0];
                            colegiadoNomina.colegiado = Convert.ToInt32(tupla[1]);
                            colegiadoNomina.mes = Convert.ToInt32(tupla[2]);
                            colegiadoNomina.anio = Convert.ToInt32(tupla[3]);
                            colegiadoNomina.cuotaColegiado = ((tupla[4].Equals("")) ? 0m : Convert.ToDecimal(tupla[4]));
                            colegiadoNomina.cuotaTimbre = ((tupla[5].Equals("")) ? 0m : Convert.ToDecimal(tupla[5]));
                            colegiadoNomina.seguroPostumo = ((tupla[6].Equals("")) ? 0m : Convert.ToDecimal(tupla[6]));
                            colegiadoNomina.textError = "";

                            infoReferee = new Referee(colegiadoNomina.colegiado, db);
                            colegiadoNomina.nombre = infoReferee.getName();

                            decimal amountC = infoReferee.getMountColegiadoPay();
                            decimal amountT = infoReferee.getMountTimbrePay();
                            decimal postumo = infoReferee.getPsotumo();
                            Debug.WriteLine("colegiado: " + colegiadoNomina.colegiado);
                            Debug.WriteLine("------------------------------------------");


                            //decimal aproxT = GeneralFunction.aproxDecimal(colegiadoNomina.cuotaTimbre, dCent);
                            //colegiadoNomina.diferencia = aproxT - colegiadoNomina.cuotaTimbre;
                            //colegiadoNomina.cuotaTimbre = aproxT;


                            if (colegiadoNomina.cuotaTimbre != amountT && colegiadoNomina.cuotaTimbre != 0m)
                            {
                                colegiadoNomina.error = 1;
                                colegiadoNomina.textError += " Monto timbre no coincide ";
                                colegiadoNomina.valorRealTimbre = amountT;
                            }
                        
                            if (colegiadoNomina.cuotaColegiado != amountC && colegiadoNomina.cuotaColegiado != 0m)
                            {
                                colegiadoNomina.error = 1;
                                colegiadoNomina.textError += " Monto colegiado no coincide ";
                                colegiadoNomina.valorRealColegiado = amountC;
                            }
                        
                            if (colegiadoNomina.seguroPostumo != postumo && colegiadoNomina.seguroPostumo != 0m)
                            {
                                colegiadoNomina.error = 1;
                                colegiadoNomina.textError += " Monto postumo no coincide ";
                                colegiadoNomina.valorRealPostumo = postumo;
                            }

                            listNomina.Add(colegiadoNomina);

                        }
                        catch (Exception)
                        {
                            colegiadoNomina.error = 2;
                            colegiadoNomina.textError = "Error de estructura: " + tupla[0] + "," + tupla[1] + "," + tupla[2] + "," + tupla[3];
                            listNomina.Add(colegiadoNomina);
                        }
                    }
                }
                
                

                var jsonSerialiser = new JavaScriptSerializer();
                var json = jsonSerialiser.Serialize(listNomina);
                return json;
            }
            catch (Exception e)
            {

                throw;
            }
        }


        public ActionResult BatchInitLog(int id)
        {
            CCEEEntities db = new CCEEEntities();
            ccee_Batch tempBatch = db.ccee_Batch.Find(id);

            MemoryStream memoryStream = new MemoryStream();
            TextWriter tw = new StreamWriter(memoryStream);

            tw.WriteLine(tempBatch.Bat_ContenidoEntrada);
            tw.Flush();
            tw.Close();

            return File(memoryStream.GetBuffer(), "text/csv", "batch_"+id+"_entrada.csv");
        }

        public ActionResult BatchFinishLog(int id)
        {
            CCEEEntities db = new CCEEEntities();
            ccee_Batch tempBatch = db.ccee_Batch.Find(id);

            MemoryStream memoryStream = new MemoryStream();
            TextWriter tw = new StreamWriter(memoryStream);

            tw.WriteLine(tempBatch.Bar_ContenidoSalida);
            tw.Flush();
            tw.Close();

            return File(memoryStream.GetBuffer(), "text/csv", "batch_" + id + "_salida.csv");
        }

        public string getTextInit(List<ResponseUploadNominaJson> data)
        {
            string textTotal = "";
            foreach (ResponseUploadNominaJson item in data)
            {
                textTotal += item.colegiado + "," + item.cuotaColegiado + "," + item.cuotaTimbre + "," + item.seguroPostumo + "," + item.anio + "\n\r";
            }
            return textTotal;
        }


        //Usuario

        //Metodo
        public ActionResult UserTimbre()
        {
            if (GeneralFunction.permissionsRefereee(this, Session[Constant.session_user] as LoginRefreeModelView, "") == null) return RedirectToAction("TimbreUsuario", "Document");
            LoginRefreeModelView login = Session[Constant.session_user] as LoginRefreeModelView;
            ViewBag.idReferee = login.referee;
            return View();
        }

        public ActionResult Certificaciones()
        {
            if (GeneralFunction.permissionsRefereee(this, Session[Constant.session_user] as LoginRefreeModelView, "") == null) return RedirectToAction("TimbreUsuario", "Document");
            LoginRefreeModelView login = Session[Constant.session_user] as LoginRefreeModelView;
            ViewBag.idReferee = login.referee;
            return View();
        }

        public ActionResult InboxTimbre()
        {
            if (GeneralFunction.permissionsRefereee(this, Session[Constant.session_user] as LoginRefreeModelView, "") == null) return RedirectToAction("TimbreUsuario", "Document");
            LoginRefreeModelView login = Session[Constant.session_user] as LoginRefreeModelView;
            ViewBag.idReferee = login.referee;
            return View();
        }

        public ActionResult UserProfile()
        {
            if (GeneralFunction.permissionsRefereee(this, Session[Constant.session_user] as LoginRefreeModelView, "") == null) return RedirectToAction("TimbreUsuario", "Document");
            LoginRefreeModelView login = Session[Constant.session_user] as LoginRefreeModelView;
            ViewBag.idReferee = login.referee;
            return View();
        }

        public ActionResult UserProfile_ChangePassword()
        {
            if (GeneralFunction.permissionsRefereee(this, Session[Constant.session_user] as LoginRefreeModelView, "") == null) return RedirectToAction("TimbreUsuario", "Document");
            LoginRefreeModelView login = Session[Constant.session_user] as LoginRefreeModelView;
            ViewBag.idReferee = login.referee;
            return View();
        }

        public ActionResult TimbreUsuario()
        {
            return View();
        }

        public ActionResult DocumentTimbre(string id, string ids)
        {
            if (GeneralFunction.permissionsRefereee(this, Session[Constant.session_user] as LoginRefreeModelView, "") == null) return RedirectToAction("TimbreUsuario", "Document");
            if (id == null)
            {
                return View();
            }


            try
            {
                var tempDocument = new
                {
                    id = id
                };

                var client = new RestClient("http://159.65.44.11:4023");
                RestRequest restSend = HttpRequestRest.getRequestHttpPost(client, "api/timbre/find", tempDocument);
                var response = client.Execute(restSend);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string content = response.Content;
                    var info = new JavaScriptSerializer().Deserialize<ResponseStatusDocumentJson>(content);
                    byte[] bytes = Convert.FromBase64String(info.base64);

                    //byte[] byteArray = Convert.fromBase64String(base64);
                    return File(bytes, System.Net.Mime.MediaTypeNames.Application.Pdf, ids + ".pdf");
                    /*

                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        FileStreamResult file = File(ms, Constant.http_content_header_json);
                        ms.Close();
                        return file;
                    }*/

                }

                return View();
            }
            catch (Exception)
            {
                return View();
            }



            /*int referee = id.Value;
            int typeDocument = ids.Value;
            ccee_TipoDoc tipo = db.ccee_TipoDoc.Find(typeDocument);

            string html = tipo.TDC_XmlPlantilla;
            MemoryStream memory = GeneralFunction.GetPDFLetter(html);
            return File(memory, Constant.http_content_header_json);*/
        }


        public static MemoryStream StringToMemoryStream(Encoding encoding, string source)
        {
            var content = encoding.GetBytes(source);
            return new MemoryStream(content);
        }

        [HttpPost]
        public ActionResult TimbreUsuario(string colegiado, string password)
        {
            CCEEEntities dbConexion = new CCEEEntities();
            LoginRefreeModelView loginRefreeModelView = new LoginRefreeModelView();
            try
            {
                int NoReferee = Convert.ToInt32(colegiado);

                var tempRefree = (from col in dbConexion.ccee_Colegiado
                                  where col.Col_NoColegiado == NoReferee
                                  select new
                                  {
                                      referee = col.Col_NoColegiado,
                                      pass = col.Col_Password,
                                      estatus = col.Col_Estatus,
                                      name = "Colegiado: " + col.Col_PrimerNombre + " " + col.Col_PrimerApellido
                                  }).FirstOrDefault();

                if (tempRefree != null)
                {
                    if (tempRefree.pass != null)
                    {
                        if (tempRefree.pass.Equals(GeneralFunction.MD5Encrypt(password)))
                        {
                            
                            Autorization autorization = new Autorization(dbConexion);
                            string key = autorization.createToken(tempRefree.referee, tempRefree.name);

                            // Asignar parametros de Login
                            loginRefreeModelView.referee = tempRefree.referee;
                            loginRefreeModelView.estatus = tempRefree.estatus.GetValueOrDefault();
                            loginRefreeModelView.keyAutorization = key;
                            //loginRefreeModelView.idCashier = Constant.member_payment_Cashier;
                            loginRefreeModelView.idOpenCloseCash = abrirCerrarCaja(dbConexion);

                            // Asignar sesión
                            HttpContext.Session.Add(Constant.session_user, loginRefreeModelView);
                            HttpContext.Session.Timeout = 1080;

                            return RedirectToAction("UserProfile", "Document", null);

                        }
                        else
                        {
                            ViewBag.titulo = "La contraseña no coincide.";
                        }

                    }
                    else
                    {
                        ViewBag.titulo = "El Colegiado no tiene una contraseña registrada.";
                    }
                }
                else
                {
                    ViewBag.titulo = "No se encuentra el correo o no se encuentra activo, comunicarse con el CCEE";
                }

            }
            catch (Exception ex)
            {
                ViewBag.titulo = "No se puedo establecer conexión, intentar de nuevo";
            }
            return View();
        }

        public int abrirCerrarCaja(CCEEEntities dbConexion) {
            try
            {
                ccee_AperturaCierre tempOpenClose = new ccee_AperturaCierre()
                {
                    ApC_MontoEfectivo = 0,
                    ApC_MontoDocumento = 0,
                    ApC_MontoTransaccionElectronica = 0,
                    ApC_Total = 0,
                    ApC_AbreCierra = 0,
                    ApC_Fecha = DateTime.Now,
                    fk_Caja = Constant.member_payment_Caja,
                    fk_Cajero = Constant.member_payment_Cashier,
                    ApC_NoRecibo = 0
                };

                dbConexion.ccee_AperturaCierre.Add(tempOpenClose);
                dbConexion.SaveChanges();
                return tempOpenClose.ApC_NoAperturaCierre;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public string GetToken()
        {
            //if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_universidad) == null) return null;
            LoginRefreeModelView login = Session[Constant.session_user] as LoginRefreeModelView;
            string keyAuth = login.keyAutorization;
            return keyAuth;
        }




    }
}
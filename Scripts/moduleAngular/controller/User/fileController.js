﻿(function () {
    angular.module('user')
        .controller('files', ['$scope', '$http', 'CONSTANTE', '$anchorScroll', 'userFactory', function ($scope, $http, CONSTANTE, $anchorScroll, userFactory) {
            $scope.listFile;
            $scope.listOperations;
            $scope.listFileRequirement;
            $scope.showDelete = false;
            $scope.showDeleteRequire = false;
            $scope.showNotify = CONSTANTE.NOTIFY_SHOW_CSS;
            $scope.idFileUpdate = 0;
            $scope.idFileRequireFile = 0;
            $scope.idFileRequireFileUpdate = 0;


            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANTE.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANTE.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                userFactory.actionGetFunction(CONSTANTE.URL_API_FILE)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listFile = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.setAuth = function () {
                userFactory.actionGetFunction(CONSTANTE.URL_GET_TOKEN)
                    .then(function (response) {
                        userFactory.setAuthorization(response.data);
                        $scope.init();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            

            //userFactory.actionGetFunction(CONSTANTE.URL_API_FILE)
            //    .then(function (response) {
            //        console.log(response);
            //        $scope.listOperations = response.data;
            //    }, function (error) {
            //        console.log(error);
            //    });


            $scope.getFiles = function () {
                $scope.showLoadingFunction();
                userFactory.actionGetFunction(CONSTANTE.URL_API_FILE)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listFile = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.addFile = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.fileName
                }
                console.log(information);
                userFactory.actionPostFunction(CONSTANTE.URL_API_FILE, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getFiles();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }


            $scope.showDataUpdate = function (type) {
                $scope.idFileUpdate = type.id;
                $scope.fileName = type.name;
                $anchorScroll();
            }

            $scope.updateFile = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.fileName
                }
                userFactory.actionPutFunction(CONSTANTE.URL_API_FILE, $scope.idFileUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.getFiles();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }


            $scope.deleteFile = function (type) {
                $scope.showLoadingFunction();
                userFactory.actionDeleteFunction(CONSTANTE.URL_API_FILE, type.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getFiles();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }


            $scope.getFileRequired = function (id) {
                $scope.showLoadingFunction();
                userFactory.actionGetIntFunction(CONSTANTE.URL_API_FILEREQUIREMENT, id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listFileRequirement = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.addFileRequired = function () {
                var statusT = (($scope.requirementFileEnable == true) ? 1 : 0);
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.requirementFileName,
                    required: $scope.requirementFileSelectObligation,
                    idFile: $scope.idFileRequireFile,
                    initFile: statusT
                }

                console.log(information);
                userFactory.actionPostFunction(CONSTANTE.URL_API_FILEREQUIREMENT, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getFileRequired($scope.idFileRequireFile);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }


            $scope.showDataUpdateRequire = function (type) {
                $scope.idFileRequireFileUpdate = type.id;
                $scope.requirementFileName = type.name;
                $scope.requirementFileObligation = type.required;
                $scope.requirementFileEnable = ((type.initFile == 1) ? true : false);
                $anchorScroll();
            }

            $scope.updateFileRequired = function () {
                var statusT = (($scope.requirementFileEnable == true) ? 1 : 0);
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.requirementFileName,
                    required: $scope.requirementFileSelectObligation,
                    idFile: $scope.idFileRequireFile,
                    initFile: statusT
                }
                userFactory.actionPutFunction(CONSTANTE.URL_API_FILEREQUIREMENT, $scope.idFileRequireFileUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanDataRequire();
                            $scope.getFileRequired($scope.idFileRequireFile);
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }


            $scope.deleteFileRequired = function (type) {
                $scope.showLoadingFunction();
                userFactory.actionDeleteFunction(CONSTANTE.URL_API_FILEREQUIREMENT, type.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getFileRequired($scope.idFileRequireFile);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.changeStateRequired = function (value) {
                if(value == 1){
                    return "Obligatorio";
                }else if(value == 2){
                    return "Opcional";
                }
            }

            $scope.cleanData = function () {
                $scope.idFileUpdate = 0;
                $scope.fileName = "";
            }

            $scope.cleanDataRequire = function () {
                $scope.idFileRequireFileUpdate = 0;
                $scope.requirementFileName = null;
            }

            $scope.selectFileforRequired = function (type) {
                $scope.idFileRequireFile = type.id;
                $scope.getFileRequired(type.id);
            }


        }]);

})()
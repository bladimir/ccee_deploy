﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonDesgloseCuentas
    {
        public string nombre { get; set; }
        public decimal? tempValor { get; set; }
        public decimal? valor {
            get
            {
                return ((tempValor == null)? 0m: Math.Round(tempValor.Value, 2));
            }
            set { tempValor = value; }
        }
        public decimal sumatori { get; set; }
        public string cuenta { get; set; }
        public int tipo { get; set; }
        public int cantidad { get; set; }
        public int? posicion { get; set; }
        public int? agrupar { get; set; }
        public int id { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonCorteCajaTarjeta
    {
        public string transaccion { get {
                if(idMedio == 2)
                {
                    return "Bol. " + numeroTrans;
                }else
                {
                    return numeroTrans;
                }
            }
        }
        public int idMedio { get; set; }
        public string numeroTrans { get; set; }
        public string banco { get; set; }
        public decimal tempMonto { get; set; }
        public decimal monto {
            get
            {
                return Math.Round(tempMonto, 2);
            }
            set { tempMonto = value; }
        }
        public int? esNomina { get; set; }
    }
}
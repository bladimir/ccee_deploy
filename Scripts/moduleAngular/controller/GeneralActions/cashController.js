﻿(function () {
    angular.module('generals')
        .controller('cash', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquarter;
            $scope.listPaymentCenter;
            $scope.listCash;
            $scope.listPaymentMethodPay;
            $scope.idPaymentCenter = 0;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idCashUpdate = 0;
            $scope.objectCashUpdate;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_COUNTRY)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listCountry = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }


            $scope.getCash = function () {
                $scope.idPaymentCenter = $scope.selectPayment.id;
                $scope.getCashData($scope.idPaymentCenter);
            }


            $scope.getDepartament = function () {
                if ($scope.selectCountry == null) return null;
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_DEPARTAMENT, $scope.selectCountry.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listDepartament = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getMunicipality = function () {
                if ($scope.selectDepartament == null) return null;
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_MUNICIPALITY, $scope.selectDepartament.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listMunicipality = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getHeadquar = function () {
                if ($scope.selectMunicipality == null) return null;
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_HEADQUARTERS, $scope.selectMunicipality.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listHeadquarter = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getPayment = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_PAYMENTCENTER, $scope.selectHeadquart.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listPaymentCenter = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getCashData = function (id) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_CASH, id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listCash = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.addCash = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.cashname,
                    activo: $scope.cashselectactive,
                    fkPaymentCenter: $scope.idPaymentCenter
                }
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_CASH, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getCashData($scope.idPaymentCenter);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (pay) {
                $scope.idCashUpdate = pay.id;
                $scope.objectCashUpdate = pay;
                $scope.cashname = pay.name;
                $scope.cashactive = pay.activo;
                $anchorScroll();
            }

            $scope.updateCash = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.cashname,
                    activo: (($scope.cashselectactive == null) ? $scope.objectCashUpdate.activo : $scope.cashselectactive)
                }

                console.log(information);

                generalactionsFactory.actionPutFunction(CONSTANT.URL_API_CASH, $scope.idCashUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.getCashData($scope.idPaymentCenter);
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.deleteCash = function (pay) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionDeleteFunction(CONSTANT.URL_API_CASH, pay.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getCashData($scope.idPaymentCenter);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idCashUpdate = 0;
                $scope.cashname = "";
                $scope.cashactive = "";
            }

            $scope.selectCashActive = function (type) {
                if (type == 0) {
                    return "Inactivo";
                } else if (type == 1) {
                    return "Activo";
                } else {
                    return "No definido";
                }
            }


        }]);

})()
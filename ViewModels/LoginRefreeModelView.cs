﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.ViewModels
{
    public class LoginRefreeModelView
    {
        public int referee { get; set; }
        public int estatus { get; set; }
        public Boolean isColegiado { get; set; }
        public int idOpenCloseCash { get; set; }
        public string keyAutorization { get; set; }

    }
}
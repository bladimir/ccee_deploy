﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonDirectorio
    {
        public int NoColegiado { get; set; }
        public string grado { get; set; }
        public string colegiado { get {
                return NoColegiado + " " + grado;
            }
        }
        public string estatus { get; set; }
        public string nombre { get; set; }
        public string universidad { get; set; }
        public DateTime? tempFechaCol { get; set; }
        public string fechaColegiacion { get {
                return (tempFechaCol == null) ? "No definida" 
                    : "Fecha de Colegiación: " + tempFechaCol.Value.ToString("dd/MM/yyyy");
            }
        }
        public string direccionRes { get; set; }
        public string direccionOf { get; set; }
        public string telefonoTO { get; set; }
        public string telefonoFO { get; set; }
        public string telefonoC { get; set; }
        public string telefonoCasa { get; set; }
    }
}
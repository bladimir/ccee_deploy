﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonSing
{
    public class ResponseFindCertification
    {
        public int status { get; set; }
        public string msg { get; set; }
        public string base64 { get; set; }
    }
}
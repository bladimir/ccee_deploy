﻿(function () {
    angular.module('reports')
        .controller('documentosPendientes', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'reportFactory', function ($scope, $http, CONSTANT, $anchorScroll, reportFactory) {
            $scope.direction;
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquaters;
            $scope.regexReferee = CONSTANT.REGEX_REFEREE;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.identical = function (actual, expected) {
                return actual === parseInt(expected);
            }

            $scope.init = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        reportFactory.setAuthorization(response.data);
                        $scope.initF();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.initF = function () {
                $scope.getTypeReferee();
                $scope.getHeadquarter();
                $scope.getUniversity();
                $scope.getTypeEspeciality();
            }

            $scope.getUniversity = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_UNIVERSITY)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listUniversity = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getTypeReferee = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_TYPEREFEREE)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listTypeReferee = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getHeadquarter = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_HEADQUARTERS)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listHeadquarter = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getTypeEspeciality = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_TYPESPECIALTY)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listTypeEspeciality = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getDataSelect = function (list) {
                if (list == null) return "";
                var len = list.length;

                var listTotal = "";
                for (var i = 0; i < len; i++) {
                    listTotal += list[i].id + ((len > 0 && i < len - 1) ? "," : "");
                }
                return listTotal;
            }

            $scope.getDataSelectType = function (list) {
                if (list == null) return "";
                var len = list.length;

                var listTotal = "";
                for (var i = 0; i < len; i++) {
                    listTotal += list[i].idTypeReferee + ((len > 0 && i < len - 1) ? "," : "");
                }
                return listTotal;
            }

            $scope.generateList = function () {
                var textGetReport = "";
                console.log(($scope.selectHead != null) ? $scope.selectHead.id : "");
                textGetReport = "referee=" + $scope.inReferee +
                                "&universidad=" + $scope.getDataSelect($scope.inUniversity) +
                                "&type=" + $scope.getDataSelectType($scope.inTypeReferee) +
                                "&head=" + $scope.getDataSelect($scope.inHead) +
                                "&special=" + $scope.getDataSelect($scope.inSpecial) +
                                "&active=" + $scope.inActive;
                console.log(textGetReport);
                $scope.direction = "../../Reports/aspxReports/webDocumentosPendientes.aspx?" + textGetReport;
            }

        }]);

})()
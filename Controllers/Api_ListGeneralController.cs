﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.ListGeneral;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_ListGeneralController : ApiController
    {

        public class temPhone
        {
            public int referee { get; set; }
            public string number { get; set; }
            public int? type { get; set; }
            public string numberType { get {

                    switch (type)
                    {
                        case 1:
                            return "Teléfono casa: " + number;
                        case 2:
                            return "Teléfono oficina: " + number;
                        case 3:
                            return "Fax oficina: " + number;
                        case 4:
                            return "Celular: " + number;
                        case 5:
                            return "Otro: " + number;
                        default:
                            return "Sin tipo: " + number;
                    }
                } }

        }

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_ListGeneral
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_ListGeneral/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_ListGeneral
        public List<ResponseListGeneralJson> Post(RequestListGeneralJson value)
        {
            return getListToBD(value);
            //try
            //{
            //    string selectAddress = @",CASE ccee_Direccion.Dir_TipoDireccion 
            //                    WHEN 1 THEN 'Residencia' WHEN 2 THEN 'Trabajo' WHEN 3 THEN 'Notificación' WHEN 4 THEN 'Correspondencia' ELSE '--' END AS Dir_TipoDireccion 
            //                    ,CONCAT(ccee_Direccion.Dir_CalleAvenida, ' ', ccee_Direccion.Dir_Colonia, ccee_Direccion.Dir_Zona, ' ', ccee_Direccion.Dir_Numero) AS direccion 
            //                    ";
            //    string fromAddress = ", ccee_Direccion, ccee_Municipio, ccee_Departamento ";
            //    string whereAddress = @"AND ccee_Colegiado.Col_NoColegiado = ccee_Direccion.fk_Colegiado 
            //                            AND ccee_Direccion.fk_Municipio = ccee_Municipio.Mun_NoMunicipio 
            //                            AND ccee_Municipio.fk_Departamento = ccee_Departamento.Dep_NoDepartamento ";
            //    string selecStpecialty = @", ccee_TipoEspecialidad.TEs_NombreTipoEsp 
            //                    ,ccee_Universidad.Uni_Descripcion as Uni_NombreUniversidad 
            //                    ,ccee_Especialidad.Esp_NombreEspecialidad 
            //                    , ccee_Especialidad.Esp_Tema_Graduacion 
            //                    , ccee_Especialidad.Esp_Evaluacion 
            //                    , ccee_Especialidad.Esp_FechaObtuvo 
            //                    , ccee_TipoEspecialidad.TES_Abreviacion 
            //                    , ccee_Universidad.Uni_NombreUniversidad as Uni_NombreUniversidadabre";

            //    string fromSpecialty = @", ccee_Especialidad, ccee_Universidad, ccee_TipoEspecialidad ";
            //    string whereSpecialty = @"AND ccee_Colegiado.Col_NoColegiado = ccee_Especialidad.pkfk_Colegiado 
            //                            AND ccee_Especialidad.pkfk_TipoEspecialidad = ccee_TipoEspecialidad.TEs_NoTipoEspecialidad 
            //                            AND ccee_Especialidad.pkfk_Universidad = ccee_Universidad.Uni_NoUniversidad";
            //    bool isAddress = false;
            //    bool isSpecialty = false;

            //    isAddress = (value.check6 || (value.departament != null && !value.departament.Equals("")) || (value.typeDir != null && !value.typeDir.Equals("")));
            //    isSpecialty = (value.check10 || value.check11 || value.check16 || value.check36 || value.check20 || value.check19 || (value.idUniversity != null && !value.idUniversity.Equals(""))
            //        || (value.idTypeSpecialisti != null && !value.idTypeSpecialisti.Equals(""))
            //        || (value.typeAcademi != null && !value.typeAcademi.Equals(""))
            //        || (value.academy != null && !value.academy.Equals("")));


            //    string query = "SELECT    distinct " +
            //                    "ccee_Colegiado.Col_NoColegiado " +
            //                    ",ccee_Colegiado.Col_PrimerApellido " +
            //                    ",ccee_Colegiado.Col_SegundoApellido " +
            //                    ",ccee_Colegiado.Col_CasdaApellido " +
            //                    ",ccee_Colegiado.Col_PrimerNombre " +
            //                    ",ccee_Colegiado.Col_SegundoNombre " +
            //                    ",ccee_Colegiado.Col_TercerNombre " +
            //                    ",convert(nvarchar(MAX), ccee_Colegiado.Col_FechaNacimiento, 103) AS nacimiento " +
            //                    ",ccee_Colegiado.Col_FechaNacimiento " +
            //                    ", ccee_Colegiado.Col_FechaColegiacion " +
            //                    ",CASE ccee_Colegiado.Col_Estatus WHEN 1 THEN 'Activo' WHEN 0 THEN 'Inactivo' ELSE '--' END AS activo " +
            //                    ",CASE ccee_Colegiado.Col_Sexo WHEN 1 THEN 'F' WHEN 0 THEN 'M' ELSE '--' END AS sexo " +
            //                    ",ccee_Colegiado.fk_Sede " +
            //                    ",ccee_Colegiado.fk_SedeRegistrada " +
            //                    ",ccee_TipoColegiado.Tco_Descripcion " +
            //                    ",ccee_Colegiado.Col_Dpi " +
            //                    ",ccee_Colegiado.Col_FechaFallecimiento " +
            //                    ",ccee_Colegiado.Col_FechaJuramentacion " +
            //                    ",ccee_Colegiado.Col_jubilado " +

            //                    ",ccee_Colegiado.Col_Nacionalidad " +
            //                    ",ccee_Colegiado.Col_DpiExtendido " +
            //                    ",ccee_Colegiado.Col_Experiencia_en " +
            //                    ",CASE ccee_Colegiado.Col_EstadoCivil " +
            //                    "WHEN 0 THEN 'NA' WHEN 1 THEN 'Soltero/a' WHEN 2 THEN 'Casado/a' WHEN 3 THEN 'Separado/a' WHEN 4 THEN 'Viudo/a' ELSE '--' END AS estadoCivil " +
            //                    " " + ((isAddress) ? selectAddress : " ") + " " +
            //                    " " + ((isSpecialty) ? selecStpecialty : " ") + " " +
            //                    "FROM " +
            //                    "ccee_Colegiado " +
            //                    ", ccee_TipoColegiado " +
            //                    " " + ((isAddress) ? fromAddress : " ") + " " +
            //                    " " + ((isSpecialty) ? fromSpecialty : " ") + " " +
            //                    "WHERE " +
            //                    "ccee_Colegiado.fk_TipoColegiado = ccee_TipoColegiado.Tco_NoTipoColegiado " +
            //                    " " + ((isAddress) ? whereAddress : " ") + " " +
            //                    " " + ((isSpecialty) ? whereSpecialty : " ") + " ";


            //    if(value.edad != null && !value.edad.Equals(""))
            //    {
            //        DateTime nowDate = DateTime.Today;
            //        string[] listYear = value.edad.Split('-');
            //        if(listYear.Count() >= 1)
            //        {
            //            int yearinit = Convert.ToInt32(listYear[0]);
            //            int yearTemp = nowDate.Year - yearinit;

            //            query += " and ccee_Colegiado.Col_FechaNacimiento <= '" + yearTemp + "-" + nowDate.Month + "-" + nowDate.Day + "' ";
            //        }
            //        if (listYear.Count() >= 2)
            //        {
            //            int yearinit = Convert.ToInt32(listYear[1]);
            //            int yearTemp = nowDate.Year - yearinit;

            //            query += " and ccee_Colegiado.Col_FechaNacimiento >= '" + yearTemp + "-" + nowDate.Month + "-" + nowDate.Day + "' ";
            //        }
            //    }

            //    if (value.year != null && !value.year.Equals(""))
            //    {
            //        DateTime nowDate = DateTime.Today;
            //        string[] listYear = value.year.Split('-');
            //        if (listYear.Count() >= 1)
            //        {
            //            query += " and ccee_Colegiado.Col_FechaJuramentacion >= '" + listYear[0] + "-01-01' ";
            //        }
            //        if (listYear.Count() >= 2)
            //        {
            //            query += " and ccee_Colegiado.Col_FechaJuramentacion <= '" + listYear[1] + "-01-01' ";
            //        }
            //    }//typeDir

            //    query += GeneralFunction.generateQuery("ccee_Universidad.Uni_NoUniversidad", value.idUniversity, Constant.text_query_is_number);
            //    query += GeneralFunction.generateQuery("ccee_TipoColegiado.Tco_NoTipoColegiado", value.idTypeReferee, Constant.text_query_is_number);
            //    query += GeneralFunction.generateQuery("ccee_Colegiado.fk_Sede", value.idSede, Constant.text_query_is_number);

            //    query += GeneralFunction.generateQuery("ccee_Colegiado.fk_SedeRegistrada", value.idSedeReg, Constant.text_query_is_number);
            //    query += GeneralFunction.generateQuery("ccee_TipoEspecialidad.TEs_NoTipoEspecialidad", value.idTypeSpecialisti, Constant.text_query_is_number);
            //    query += GeneralFunction.generateQuery("ccee_Colegiado.Col_Estatus", value.status, Constant.text_query_is_number);
            //    query += GeneralFunction.generateQuery("ccee_Especialidad.Esp_NombreEspecialidad", value.academy, Constant.text_query_is_text);

            //    if(value.loadMasive == 0)
            //    {
            //        query += GeneralFunction.generateQuery("ccee_Colegiado.Col_NoColegiado", value.referees, Constant.text_query_is_number);
            //    }
            //    else
            //    {
            //        string dataListColegiado = db.ccee_TempReporte.Find(value.loadMasive).TemRep_Listado;
            //        query += " and in (" + dataListColegiado + ")  ";
            //    }


            //    query += GeneralFunction.generateQuery("ccee_Especialidad.Esp_Evaluacion", value.typeAcademi, Constant.text_query_is_text);
            //    query += GeneralFunction.generateQuery("ccee_Direccion.Dir_TipoDireccion", value.typeDir, Constant.text_query_is_number);
            //    if (isAddress)
            //    {
            //        query += GeneralFunction.generateQuery("ccee_Departamento.Dep_NoDepartamento", value.departament, Constant.text_query_is_number);
            //    }

            //    List<ResponseListGeneralJson> pLis = db.Database.SqlQuery<ResponseListGeneralJson>(query).ToList();

            //    var listPhone = (from pho in db.ccee_Telefono
            //                    select new temPhone()
            //                    {
            //                        referee = pho.fk_Colegiado,
            //                        number = pho.Tel_NumTelefono,
            //                        type = pho.Tel_TipoTelefono
            //                    }).ToList();
            //    var listMail = (from mai in db.ccee_Mail
            //                   select new {
            //                       referee = mai.fk_Colegiado,
            //                       mail = mai.Mai_MailDir
            //                   }).ToList();

            //    var listLenguage = (from coll in db.ccee_ColegiadoIdioma
            //                       join len in db.ccee_Idioma on coll.pkfk_Idioma equals len.Idi_NoIdioma
            //                       select new
            //                       {
            //                           referee = coll.pkfk_Colegiado,
            //                           lenguage = len.Idi_Descripcion
            //                       }).ToList();

            //    var listPhoneRe = listPhone
            //        .GroupBy(o => new
            //        {
            //            Referee = o.referee
            //        })
            //        .Select(g => new
            //        {
            //            referee = g.Key.Referee,
            //            phone = string.Join(" / ", g.Select(p => p.numberType))
            //        })
            //        .OrderBy(a => a.referee)
            //        .ToList();

            //    var listPhoneMai = listMail
            //        .GroupBy(o => new
            //        {
            //            Referee = o.referee
            //        })
            //        .Select(g => new
            //        {
            //            referee = g.Key.Referee,
            //            mail = string.Join(" / ", g.Select(p => p.mail))
            //        })
            //        .OrderBy(a => a.referee)
            //        .ToList();

            //    var listLanguageRe = listLenguage
            //        .GroupBy(o => new
            //        {
            //            Referee = o.referee
            //        })
            //        .Select(g => new
            //        {
            //            referee = g.Key.Referee,
            //            language = string.Join(" / ", g.Select(p => p.lenguage))
            //        })
            //        .OrderBy(a => a.referee)
            //        .ToList();

            //    var listSede = db.ccee_Sede.ToList();
            //    for (int i = 0; i < pLis.Count; i++)
            //    {
            //        if (pLis[i].fk_Sede == null)
            //        {
            //            pLis[i].fk_Sede_Nombre = "";
            //        }
            //        else
            //        {
            //            pLis[i].fk_Sede_Nombre = listSede.Find(p => p.Sed_NoSede == pLis[i].fk_Sede).Sed_Direccion;
            //        }

            //        if (pLis[i].fk_SedeRegistrada == null)
            //        {
            //            pLis[i].fk_SedeRegistrada_Nombre = "";
            //        }
            //        else
            //        {
            //            pLis[i].fk_SedeRegistrada_Nombre = listSede.Find(p => p.Sed_NoSede == pLis[i].fk_SedeRegistrada).Sed_Direccion;
            //        }

            //        /*if(pLis[i].Col_NoColegiado != null)
            //        {

            //        }*/

            //        if (pLis[i].Col_NoColegiado != null)
            //        {
            //            var tempPhone = listPhoneRe
            //                .Find(p => p.referee == pLis[i].Col_NoColegiado.Value);
            //            if (tempPhone != null)
            //            {
            //                pLis[i].phones = tempPhone.phone;
            //            }

            //            var tempMail = listPhoneMai
            //                .Find(p => p.referee == pLis[i].Col_NoColegiado.Value);
            //            if(tempMail != null)
            //            {
            //                pLis[i].mails = tempMail.mail;
            //            }

            //            var tempLanguage = listLanguageRe
            //                .Find(p => p.referee == pLis[i].Col_NoColegiado.Value);
            //            if (tempLanguage != null)
            //            {
            //                pLis[i].idiomas = tempLanguage.language;
            //            }
            //        }

            //        if(pLis[i].Col_FechaNacimiento != null)
            //        {
            //            int meses = GeneralFunction.getDiffMonths(DateTime.Today, pLis[i].Col_FechaNacimiento.Value);
            //            decimal tempAnios = meses / 12;
            //            pLis[i].edad = Convert.ToInt32(Math.Floor(tempAnios));
            //        }


            //    }

            //    if (value.groupReferee)
            //    {

            //        pLis = pLis
            //            .GroupBy(o => new
            //            {
            //                Referee = o.Col_NoColegiado
            //            })
            //            .Select(g => new ResponseListGeneralJson()
            //            {
            //                Col_NoColegiado = g.Key.Referee,
            //                Col_PrimerApellido = g.Select(p => p.Col_PrimerApellido).FirstOrDefault(),
            //                Col_SegundoApellido = g.Select(p => p.Col_SegundoApellido).FirstOrDefault(),
            //                Col_PrimerNombre = g.Select(p => p.Col_PrimerNombre).FirstOrDefault(),
            //                Col_SegundoNombre = g.Select(p => p.Col_SegundoNombre).FirstOrDefault(),
            //                nacimiento = g.Select(p => p.nacimiento).FirstOrDefault(),
            //                Col_FechaColegiacion = g.Select(p => p.Col_FechaColegiacion).FirstOrDefault(),
            //                activo = g.Select(p => p.activo).FirstOrDefault(),
            //                sexo = g.Select(p => p.sexo).FirstOrDefault(),
            //                fk_Sede_Nombre = g.Select(p => p.fk_Sede_Nombre).FirstOrDefault(),
            //                fk_SedeRegistrada_Nombre = g.Select(p => p.fk_Sede_Nombre).FirstOrDefault() ,
            //                Tco_Descripcion = g.Select(p => p.Tco_Descripcion).FirstOrDefault(),
            //                estadoCivil = g.Select(p => p.estadoCivil).FirstOrDefault(),
            //                Dir_TipoDireccion = g.Select(p => p.Dir_TipoDireccion).FirstOrDefault() ,
            //                direccion = g.Select(p => p.direccion).FirstOrDefault() ,
            //                TEs_NombreTipoEsp = g.Select(p => p.TEs_NombreTipoEsp).FirstOrDefault() ,
            //                Uni_NombreUniversidad = g.Select(p => p.Uni_NombreUniversidad).FirstOrDefault() ,
            //                Esp_NombreEspecialidad = g.Select(p => p.Esp_NombreEspecialidad).FirstOrDefault() ,
            //                Esp_Tema_Graduacion = g.Select(p => p.Esp_Tema_Graduacion).FirstOrDefault(),
            //                Esp_Evaluacion = g.Select(p => p.Esp_Evaluacion).FirstOrDefault(),
            //                phones = g.Select(p => p.phones).FirstOrDefault(),
            //                mails = g.Select(p => p.mails).FirstOrDefault(),
            //                Esp_FechaObtuvo = g.Select(p => p.Esp_FechaObtuvo).FirstOrDefault(),
            //                Uni_NombreUniversidadabre = g.Select(p => p.Uni_NombreUniversidadabre).FirstOrDefault(),
            //                Col_Dpi = g.Select(p=>p.Col_Dpi).FirstOrDefault(),
            //                Col_FechaFallecimiento = g.Select(p=>p.Col_FechaFallecimiento).FirstOrDefault(),
            //                Col_FechaJuramentacion = g.Select(p=>p.Col_FechaJuramentacion).FirstOrDefault(),
            //                Col_jubilado = g.Select(p=>p.Col_jubilado).FirstOrDefault(),
            //                Col_CasdaApellido = g.Select(p=>p.Col_CasdaApellido).FirstOrDefault(),
            //                Col_DpiExtendido = g.Select(p=>p.Col_DpiExtendido).FirstOrDefault(),
            //                Col_Experiencia_en = g.Select(p=>p.Col_Experiencia_en).FirstOrDefault(),
            //                Col_FechaNacimiento = g.Select(p=>p.Col_FechaNacimiento).FirstOrDefault(),
            //                Col_Nacionalidad = g.Select(p=>p.Col_Nacionalidad).FirstOrDefault(),
            //                Col_TercerNombre = g.Select(p=>p.Col_TercerNombre).FirstOrDefault(),
            //                edad = g.Select(p=>p.edad).FirstOrDefault(),
            //                TES_Abreviacion = g.Select(p=>p.TES_Abreviacion).FirstOrDefault(),
            //                idiomas = g.Select(p=>p.idiomas).FirstOrDefault()
            //            })
            //            .OrderBy(a => a.Col_NoColegiado)
            //            .ToList();

            //    }

            //    return pLis;


            //}
            //catch (Exception)
            //{
            //    return null;
            //}

        }

        // PUT: api/Api_ListGeneral/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_ListGeneral/5
        public void Delete(int id)
        {
        }

        // List<ResponseListGeneralJson> Post(RequestListGeneralJson value)
        private List<ResponseListGeneralJson> getListToBD(RequestListGeneralJson value)
        {
            

            if (value.loadMasive == 0)
            {
                return getListData(value);
            }
            else
            {

                string dataListColegiado = db.ccee_TempReporte.Find(value.loadMasive).TemRep_Listado;

                List<string> listOfColegiados = GeneralFunction.getListToStringComa(dataListColegiado);

                List<ResponseListGeneralJson> listGeneral = new List<ResponseListGeneralJson>();
                for (int i = 0; i < listOfColegiados.Count; i++)
                {
                    value.referees = listOfColegiados[i];
                    var pList = getListData(value);
                    listGeneral.AddRange(pList);
                }

                return listGeneral;
            }
        }


        private List<ResponseListGeneralJson> getListData(RequestListGeneralJson value)
        {
            try
            {
                string selectAddress = @",CASE ccee_Direccion.Dir_TipoDireccion 
                                WHEN 1 THEN 'Residencia' WHEN 2 THEN 'Trabajo' WHEN 3 THEN 'Notificación' WHEN 4 THEN 'Correspondencia' ELSE '--' END AS Dir_TipoDireccion 
                                ,CONCAT(ccee_Direccion.Dir_CalleAvenida, ' ', ccee_Direccion.Dir_Colonia, ccee_Direccion.Dir_Zona, ' ', ccee_Direccion.Dir_Numero) AS direccion 
                                ";
                string fromAddress = ", ccee_Direccion, ccee_Municipio, ccee_Departamento ";
                string whereAddress = @"AND ccee_Colegiado.Col_NoColegiado = ccee_Direccion.fk_Colegiado 
                                        AND ccee_Direccion.fk_Municipio = ccee_Municipio.Mun_NoMunicipio 
                                        AND ccee_Municipio.fk_Departamento = ccee_Departamento.Dep_NoDepartamento ";
                string selecStpecialty = @", ccee_TipoEspecialidad.TEs_NombreTipoEsp 
                                ,ccee_Universidad.Uni_Descripcion as Uni_NombreUniversidad 
                                ,ccee_Especialidad.Esp_NombreEspecialidad 
                                , ccee_Especialidad.Esp_Tema_Graduacion 
                                , ccee_Especialidad.Esp_Evaluacion 
                                , ccee_Especialidad.Esp_FechaObtuvo 
                                , ccee_TipoEspecialidad.TES_Abreviacion 
                                , ccee_Especialidad.Esp_Titulo 
                                , ccee_Universidad.Uni_NombreUniversidad as Uni_NombreUniversidadabre";

                string fromSpecialty = @", ccee_Especialidad, ccee_Universidad, ccee_TipoEspecialidad ";
                string whereSpecialty = @"AND ccee_Colegiado.Col_NoColegiado = ccee_Especialidad.pkfk_Colegiado 
                                        AND ccee_Especialidad.pkfk_TipoEspecialidad = ccee_TipoEspecialidad.TEs_NoTipoEspecialidad 
                                        AND ccee_Especialidad.pkfk_Universidad = ccee_Universidad.Uni_NoUniversidad";
                bool isAddress = false;
                bool isSpecialty = false;

                isAddress = (value.check6 || (value.departament != null && !value.departament.Equals("")) || (value.typeDir != null && !value.typeDir.Equals("")));
                isSpecialty = (value.check10 || value.check11 || value.check16 || value.check36 || value.check42 || value.check20 || value.check19 || (value.idUniversity != null && !value.idUniversity.Equals(""))
                    || (value.idTypeSpecialisti != null && !value.idTypeSpecialisti.Equals(""))
                    || (value.typeAcademi != null && !value.typeAcademi.Equals(""))
                    || (value.academy != null && !value.academy.Equals("")));


                string query = "SELECT    distinct " +
                                "ccee_Colegiado.Col_NoColegiado " +
                                ",ccee_Colegiado.Col_PrimerApellido " +
                                ",ccee_Colegiado.Col_SegundoApellido " +
                                ",ccee_Colegiado.Col_CasdaApellido " +
                                ",ccee_Colegiado.Col_PrimerNombre " +
                                ",ccee_Colegiado.Col_SegundoNombre " +
                                ",ccee_Colegiado.Col_TercerNombre " +
                                ",convert(nvarchar(MAX), ccee_Colegiado.Col_FechaNacimiento, 103) AS nacimiento " +
                                ",ccee_Colegiado.Col_FechaNacimiento " +
                                ", ccee_Colegiado.Col_FechaColegiacion " +
                                ",CASE ccee_Colegiado.Col_Estatus WHEN 1 THEN 'Activo' WHEN 0 THEN 'Inactivo' ELSE '--' END AS activo " +
                                ",CASE ccee_Colegiado.Col_Sexo WHEN 1 THEN 'F' WHEN 0 THEN 'M' ELSE '--' END AS sexo " +
                                ",ccee_Colegiado.fk_Sede " +
                                ",ccee_Colegiado.fk_SedeRegistrada " +
                                ",ccee_TipoColegiado.Tco_Descripcion " +
                                ",ccee_Colegiado.Col_Dpi " +
                                ",ccee_Colegiado.Col_FechaFallecimiento " +
                                ",ccee_Colegiado.Col_FechaJuramentacion " +
                                ",ccee_Colegiado.Col_jubilado " +

                                ",ccee_Colegiado.Col_Nacionalidad " +
                                ",ccee_Colegiado.Col_DpiExtendido " +
                                ",ccee_Colegiado.Col_Experiencia_en " +
                                ",CASE ccee_Colegiado.Col_EstadoCivil " +
                                "WHEN 0 THEN 'NA' WHEN 1 THEN 'Soltero/a' WHEN 2 THEN 'Casado/a' WHEN 3 THEN 'Separado/a' WHEN 4 THEN 'Viudo/a' ELSE '--' END AS estadoCivil " +
                                " " + ((isAddress) ? selectAddress : " ") + " " +
                                " " + ((isSpecialty) ? selecStpecialty : " ") + " " +
                                "FROM " +
                                "ccee_Colegiado " +
                                ", ccee_TipoColegiado " +
                                " " + ((isAddress) ? fromAddress : " ") + " " +
                                " " + ((isSpecialty) ? fromSpecialty : " ") + " " +
                                "WHERE " +
                                "ccee_Colegiado.fk_TipoColegiado = ccee_TipoColegiado.Tco_NoTipoColegiado " +
                                " " + ((isAddress) ? whereAddress : " ") + " " +
                                " " + ((isSpecialty) ? whereSpecialty : " ") + " ";


                if (value.edad != null && !value.edad.Equals(""))
                {
                    DateTime nowDate = DateTime.Today;
                    string[] listYear = value.edad.Split('-');
                    if (listYear.Count() >= 1)
                    {
                        int yearinit = Convert.ToInt32(listYear[0]);
                        int yearTemp = nowDate.Year - yearinit;

                        query += " and ccee_Colegiado.Col_FechaNacimiento <= '" + yearTemp + "-" + nowDate.Month + "-" + nowDate.Day + "' ";
                    }
                    if (listYear.Count() >= 2)
                    {
                        int yearinit = Convert.ToInt32(listYear[1]);
                        int yearTemp = nowDate.Year - yearinit;

                        query += " and ccee_Colegiado.Col_FechaNacimiento >= '" + yearTemp + "-" + nowDate.Month + "-" + nowDate.Day + "' ";
                    }
                }

                if (value.year != null && !value.year.Equals(""))
                {
                    DateTime nowDate = DateTime.Today;
                    string[] listYear = value.year.Split('-');
                    if (listYear.Count() >= 1)
                    {
                        query += " and ccee_Colegiado.Col_FechaJuramentacion >= '" + listYear[0] + "-01-01' ";
                    }
                    if (listYear.Count() >= 2)
                    {
                        query += " and ccee_Colegiado.Col_FechaJuramentacion <= '" + listYear[1] + "-01-01' ";
                    }
                }//typeDir

                query += GeneralFunction.generateQuery("ccee_Universidad.Uni_NoUniversidad", value.idUniversity, Constant.text_query_is_number);
                query += GeneralFunction.generateQuery("ccee_TipoColegiado.Tco_NoTipoColegiado", value.idTypeReferee, Constant.text_query_is_number);
                query += GeneralFunction.generateQuery("ccee_Colegiado.fk_Sede", value.idSede, Constant.text_query_is_number);

                query += GeneralFunction.generateQuery("ccee_Colegiado.fk_SedeRegistrada", value.idSedeReg, Constant.text_query_is_number);
                query += GeneralFunction.generateQuery("ccee_TipoEspecialidad.TEs_NoTipoEspecialidad", value.idTypeSpecialisti, Constant.text_query_is_number);
                query += GeneralFunction.generateQuery("ccee_Colegiado.Col_Estatus", value.status, Constant.text_query_is_number);
                query += GeneralFunction.generateQuery("ccee_Especialidad.Esp_NombreEspecialidad", value.academy, Constant.text_query_is_text);

                query += GeneralFunction.generateQuery("ccee_Colegiado.Col_NoColegiado", value.referees, Constant.text_query_is_number);


                query += GeneralFunction.generateQuery("ccee_Especialidad.Esp_Evaluacion", value.typeAcademi, Constant.text_query_is_text);
                query += GeneralFunction.generateQuery("ccee_Direccion.Dir_TipoDireccion", value.typeDir, Constant.text_query_is_number);
                if (isAddress)
                {
                    query += GeneralFunction.generateQuery("ccee_Departamento.Dep_NoDepartamento", value.departament, Constant.text_query_is_number);
                }

                List<ResponseListGeneralJson> pLis = db.Database.SqlQuery<ResponseListGeneralJson>(query).ToList();

                var listPhone = (from pho in db.ccee_Telefono
                                 select new temPhone()
                                 {
                                     referee = pho.fk_Colegiado,
                                     number = pho.Tel_NumTelefono,
                                     type = pho.Tel_TipoTelefono
                                 }).ToList();
                var listMail = (from mai in db.ccee_Mail
                                select new
                                {
                                    referee = mai.fk_Colegiado,
                                    mail = mai.Mai_MailDir
                                }).ToList();

                var listLenguage = (from coll in db.ccee_ColegiadoIdioma
                                    join len in db.ccee_Idioma on coll.pkfk_Idioma equals len.Idi_NoIdioma
                                    select new
                                    {
                                        referee = coll.pkfk_Colegiado,
                                        lenguage = len.Idi_Descripcion
                                    }).ToList();

                var listPhoneRe = listPhone
                    .GroupBy(o => new
                    {
                        Referee = o.referee
                    })
                    .Select(g => new
                    {
                        referee = g.Key.Referee,
                        phone = string.Join(" / ", g.Select(p => p.numberType))
                    })
                    .OrderBy(a => a.referee)
                    .ToList();

                var listPhoneMai = listMail
                    .GroupBy(o => new
                    {
                        Referee = o.referee
                    })
                    .Select(g => new
                    {
                        referee = g.Key.Referee,
                        mail = string.Join(" / ", g.Select(p => p.mail))
                    })
                    .OrderBy(a => a.referee)
                    .ToList();

                var listLanguageRe = listLenguage
                    .GroupBy(o => new
                    {
                        Referee = o.referee
                    })
                    .Select(g => new
                    {
                        referee = g.Key.Referee,
                        language = string.Join(" / ", g.Select(p => p.lenguage))
                    })
                    .OrderBy(a => a.referee)
                    .ToList();

                var listSede = db.ccee_Sede.ToList();
                for (int i = 0; i < pLis.Count; i++)
                {
                    if (pLis[i].fk_Sede == null)
                    {
                        pLis[i].fk_Sede_Nombre = "";
                    }
                    else
                    {
                        pLis[i].fk_Sede_Nombre = listSede.Find(p => p.Sed_NoSede == pLis[i].fk_Sede).Sed_Direccion;
                    }

                    if (pLis[i].fk_SedeRegistrada == null)
                    {
                        pLis[i].fk_SedeRegistrada_Nombre = "";
                    }
                    else
                    {
                        pLis[i].fk_SedeRegistrada_Nombre = listSede.Find(p => p.Sed_NoSede == pLis[i].fk_SedeRegistrada).Sed_Direccion;
                    }

                    /*if(pLis[i].Col_NoColegiado != null)
                    {
                        
                    }*/

                    if (pLis[i].Col_NoColegiado != null)
                    {
                        var tempPhone = listPhoneRe
                            .Find(p => p.referee == pLis[i].Col_NoColegiado.Value);
                        if (tempPhone != null)
                        {
                            pLis[i].phones = tempPhone.phone;
                        }

                        var tempMail = listPhoneMai
                            .Find(p => p.referee == pLis[i].Col_NoColegiado.Value);
                        if (tempMail != null)
                        {
                            pLis[i].mails = tempMail.mail;
                        }

                        var tempLanguage = listLanguageRe
                            .Find(p => p.referee == pLis[i].Col_NoColegiado.Value);
                        if (tempLanguage != null)
                        {
                            pLis[i].idiomas = tempLanguage.language;
                        }
                    }

                    if (pLis[i].Col_FechaNacimiento != null)
                    {
                        int meses = GeneralFunction.getDiffMonths(DateTime.Today, pLis[i].Col_FechaNacimiento.Value);
                        decimal tempAnios = meses / 12;
                        pLis[i].edad = Convert.ToInt32(Math.Floor(tempAnios));
                    }


                }

                if (value.groupReferee)
                {

                    pLis = pLis
                        .GroupBy(o => new
                        {
                            Referee = o.Col_NoColegiado
                        })
                        .Select(g => new ResponseListGeneralJson()
                        {
                            Col_NoColegiado = g.Key.Referee,
                            Col_PrimerApellido = g.Select(p => p.Col_PrimerApellido).FirstOrDefault(),
                            Col_SegundoApellido = g.Select(p => p.Col_SegundoApellido).FirstOrDefault(),
                            Col_PrimerNombre = g.Select(p => p.Col_PrimerNombre).FirstOrDefault(),
                            Col_SegundoNombre = g.Select(p => p.Col_SegundoNombre).FirstOrDefault(),
                            nacimiento = g.Select(p => p.nacimiento).FirstOrDefault(),
                            Col_FechaColegiacion = g.Select(p => p.Col_FechaColegiacion).FirstOrDefault(),
                            activo = g.Select(p => p.activo).FirstOrDefault(),
                            sexo = g.Select(p => p.sexo).FirstOrDefault(),
                            fk_Sede_Nombre = g.Select(p => p.fk_Sede_Nombre).FirstOrDefault(),
                            fk_SedeRegistrada_Nombre = g.Select(p => p.fk_Sede_Nombre).FirstOrDefault(),
                            Tco_Descripcion = g.Select(p => p.Tco_Descripcion).FirstOrDefault(),
                            estadoCivil = g.Select(p => p.estadoCivil).FirstOrDefault(),
                            Dir_TipoDireccion = g.Select(p => p.Dir_TipoDireccion).FirstOrDefault(),
                            direccion = g.Select(p => p.direccion).FirstOrDefault(),
                            TEs_NombreTipoEsp = g.Select(p => p.TEs_NombreTipoEsp).FirstOrDefault(),
                            Uni_NombreUniversidad = g.Select(p => p.Uni_NombreUniversidad).FirstOrDefault(),
                            Esp_NombreEspecialidad = g.Select(p => p.Esp_NombreEspecialidad).FirstOrDefault(),
                            Esp_Tema_Graduacion = g.Select(p => p.Esp_Tema_Graduacion).FirstOrDefault(),
                            Esp_Evaluacion = g.Select(p => p.Esp_Evaluacion).FirstOrDefault(),
                            phones = g.Select(p => p.phones).FirstOrDefault(),
                            mails = g.Select(p => p.mails).FirstOrDefault(),
                            Esp_FechaObtuvo = g.Select(p => p.Esp_FechaObtuvo).FirstOrDefault(),
                            Uni_NombreUniversidadabre = g.Select(p => p.Uni_NombreUniversidadabre).FirstOrDefault(),
                            Col_Dpi = g.Select(p => p.Col_Dpi).FirstOrDefault(),
                            Col_FechaFallecimiento = g.Select(p => p.Col_FechaFallecimiento).FirstOrDefault(),
                            Col_FechaJuramentacion = g.Select(p => p.Col_FechaJuramentacion).FirstOrDefault(),
                            Col_jubilado = g.Select(p => p.Col_jubilado).FirstOrDefault(),
                            Col_CasdaApellido = g.Select(p => p.Col_CasdaApellido).FirstOrDefault(),
                            Col_DpiExtendido = g.Select(p => p.Col_DpiExtendido).FirstOrDefault(),
                            Col_Experiencia_en = g.Select(p => p.Col_Experiencia_en).FirstOrDefault(),
                            Col_FechaNacimiento = g.Select(p => p.Col_FechaNacimiento).FirstOrDefault(),
                            Col_Nacionalidad = g.Select(p => p.Col_Nacionalidad).FirstOrDefault(),
                            Col_TercerNombre = g.Select(p => p.Col_TercerNombre).FirstOrDefault(),
                            edad = g.Select(p => p.edad).FirstOrDefault(),
                            TES_Abreviacion = g.Select(p => p.TES_Abreviacion).FirstOrDefault(),
                            idiomas = g.Select(p => p.idiomas).FirstOrDefault()
                        })
                        .OrderBy(a => a.Col_NoColegiado)
                        .ToList();

                }

                return pLis;


            }
            catch (Exception)
            {
                return null;
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.DocumentEvent
{
    public class ResponseDocEventJson
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.IssuingBank;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_IssuingBankController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_IssuingBank
        public IEnumerable<ResponseIssuingBankJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempIssuing = from ban in db.ccee_BancoOEmisor
                                  orderby ban.BcE_NombreBancoEmisor ascending
                                 select new ResponseIssuingBankJson()
                                 {
                                     id = ban.BanE_NoBancoEmisor,
                                     name = ban.BcE_NombreBancoEmisor,
                                     address = ban.BcE_Direccion,
                                     phone = ban.BcE_Telefono,
                                     typeInstitution = ban.BcE_TipoInstitucion,
                                     service = ban.BcE_Servicio
                                 };
                Autorization.saveOperation(db, autorization, "Obteniendo banco emisor");
                return tempIssuing;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_IssuingBank/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_IssuingBank
        public IHttpActionResult Post(RequestIssuingBankJson issuing)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_BancoOEmisor tempIssuing = new ccee_BancoOEmisor()
                {
                    BcE_NombreBancoEmisor = issuing.name,
                    BcE_Direccion = issuing.address,
                    BcE_Telefono = issuing.phone,
                    BcE_Servicio = issuing.service,
                    BcE_TipoInstitucion = issuing.typeInstitution
                };

                db.ccee_BancoOEmisor.Add(tempIssuing);
                Autorization.saveOperation(db, autorization, "Agregando banco emisor");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_IssuingBank/5
        public IHttpActionResult Put(int id, RequestIssuingBankJson issuing)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_BancoOEmisor tempIssuing = db.ccee_BancoOEmisor.Find(id);

                if (tempIssuing == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempIssuing.BcE_NombreBancoEmisor = issuing.name;
                tempIssuing.BcE_Direccion = issuing.address;
                tempIssuing.BcE_Telefono = issuing.phone;
                tempIssuing.BcE_Servicio = issuing.service;
                tempIssuing.BcE_TipoInstitucion = issuing.typeInstitution;

                db.Entry(tempIssuing).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando banco emisor");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_IssuingBank/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_BancoOEmisor tempIssuing = db.ccee_BancoOEmisor.Find(id);
                if (tempIssuing == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_BancoOEmisor.Remove(tempIssuing);
                Autorization.saveOperation(db, autorization, "Eliminando banco emisor");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.ViewModels
{
    public class FileDocumentModelView
    {
        public int id { get; set; }
        public int idReferee { get; set; }
        public string nameFile { get; set; }
        public string nameFileRequirement { get; set; }
        public int stateFileRequirement { get; set; }
        public string stateFileRequirementText { get; set; }
        public string document { get; set; }
        public string stateDocument { get; set; }
        public int? external { get; set; }
    }
}
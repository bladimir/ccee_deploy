﻿(function () {
    angular.module('reports')
        .controller('etiqueta', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'reportFactory', function ($scope, $http, CONSTANT, $anchorScroll, reportFactory) {
            $scope.direction;
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquaters;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.regexReferee = CONSTANT.REGEX_REFEREE;
            $scope.loadmasive = 0;
            $scope.idloadmasive = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.identical = function (actual, expected) {
                return actual === parseInt(expected);
            }

            $scope.init = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        reportFactory.setAuthorization(response.data);
                        $scope.initF();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.initF = function () {
                $scope.getTypeReferee();
                $scope.getHeadquarter();
                $scope.getUniversity();
                $scope.getTypeEspeciality();
            }

            $scope.getUniversity = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_UNIVERSITY)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listUniversity = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getTypeReferee = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_TYPEREFEREE)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listTypeReferee = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getHeadquarter = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_HEADQUARTERS)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listHeadquarter = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getTypeEspeciality = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_TYPESPECIALTY)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listTypeEspeciality = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getSelectListType = function () {
                var listActivo = [1, 3, 7, 8, 14, 15, 16];
                /*if ($scope.inActive != "") {
                    $scope.inTypeReferee = [];
                    for (var i = 0; i < listActivo.length; i++) {
                        for (var j = 0; j < $scope.listTypeReferee.length; j++) {
                            if (listActivo[i] == $scope.listTypeReferee[j].idTypeReferee) {
                                $scope.inTypeReferee.push($scope.listTypeReferee[j]);
                            }
                        }
                    }
                }*/
            }

            $scope.setSelectList = function () {

                var listActivo = [1, 3, 7, 8, 14, 15, 16];
                if ($scope.inActive == "" || $scope.inActive == null) {
                    return $scope.getDataSelectType($scope.inTypeReferee);
                } else {
                    var listTotal = "";
                    if ($scope.inTypeReferee != null && $scope.inTypeReferee.length > 0) {
                        for (var i = 0; i < $scope.inTypeReferee.length; i++) {
                            for (var j = 0; j < listActivo.length; j++) {
                                if ($scope.inTypeReferee[i].idTypeReferee == listActivo[j]) {
                                    if (listTotal === "") {
                                        listTotal += listActivo[j];
                                    } else {
                                        listTotal += "," + listActivo[j];
                                    }
                                }
                            }
                        }
                        if (listTotal === "") return "1,3,7,8,14,15,16";
                        return listTotal;
                    }
                    return "1,3,7,8,14,15,16";
                }
            }

            $scope.getDataSelect = function (list) {
                if (list == null) return "";
                var len = list.length;

                var listTotal = "";
                for (var i = 0; i < len; i++) {
                    listTotal += list[i].id + ((len > 0 && i < len - 1) ? "," : "");
                }
                return listTotal;
            }

            $scope.getDataSelectType = function (list) {
                if (list == null) return "";
                var len = list.length;

                var listTotal = "";
                for (var i = 0; i < len; i++) {
                    listTotal += list[i].idTypeReferee + ((len > 0 && i < len - 1) ? "," : "");
                }
                return listTotal;
            }


            $scope.generateList = function () {
                var textGetReport = "";
                console.log(($scope.selectHead != null) ? $scope.selectHead.id : "");
                textGetReport = "&universidad=" + $scope.getDataSelect($scope.inUniversity) +
                                "&type=" + $scope.setSelectList() +
                                "&head=" + $scope.getDataSelect($scope.inHead) +
                                "&special=" + $scope.getDataSelect($scope.inSpecial) +
                                "&active=" + (($scope.inActive == "2") ? "" : $scope.inActive) +
                                "&masiva=" + $scope.loadmasive +
                                "&idmasiva=" + $scope.idloadmasive +
                                "&referee=" + $scope.inReferee;
                console.log(textGetReport);
                $scope.direction = "../../Reports/aspxReports/EtiquetasAspx.aspx?" + textGetReport;
            }

            $scope.define = function () {
                $scope.showLoadingFunction();
                var information = {
                    datos: $scope.fileContent.replace(/\r\n/g, ',')
                }
                console.log(information);
                reportFactory.actionPostFunction(CONSTANT.URL_GET_TEMP_REPORT, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            console.log(response);
                            if (response.data != 0) {
                                $scope.idloadmasive = response.data;
                                $scope.loadmasive = 1;
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

        }]);

})()
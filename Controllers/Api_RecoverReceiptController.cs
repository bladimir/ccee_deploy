﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.JsonModels.RecoverReceipt;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_RecoverReceiptController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_RecoverReceipt
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_RecoverReceipt/5
        public List<ResponseRecoverReceiptJson> Get(int id)
        {
            try
            {

                string query = "select t.pago as pay, t.col as referee, sum(t.monto) as amount, t.caj as cashier, t.fech as datepay " +
                                "from(  " +
                                "select ccee_CargoEconomico_otros.CEcO_NoCargoEconomico as cargo, ccee_Pago.Pag_NoPago as pago  " +
                                        ", ccee_CargoEconomico_otros.fk_Colegiado as col, ccee_CargoEconomico_otros.fk_DocContable as contable  " +
                                        ", ccee_CargoEconomico_otros.CEcO_Monto as monto, ccee_Pago.fk_Cajero as caj, ccee_Pago.Pag_Fecha as fech  " +
                                "from ccee_CargoEconomico_otros, ccee_Pago  " +
                                "where ccee_CargoEconomico_otros.fk_Pago = ccee_Pago.Pag_NoPago " +
                                "and ccee_CargoEconomico_otros.fk_Colegiado = " + id + " " +
                                "and ccee_CargoEconomico_otros.fk_DocContable is null " +
                                "UNION ALL " +
                                "select ccee_CargoEconomico_timbre.CEcT_NoCargoEconomico as cargo, ccee_Pago.Pag_NoPago as pago " +
                                        ", ccee_CargoEconomico_timbre.fk_Colegiado as col, ccee_CargoEconomico_timbre.fk_DocContable as contable " +
                                        ", ccee_CargoEconomico_timbre.CEcT_Monto as monto, ccee_Pago.fk_Cajero as caj, ccee_Pago.Pag_Fecha as fech " +
                                "from ccee_CargoEconomico_timbre, ccee_Pago " +
                                "where ccee_CargoEconomico_timbre.fk_Pago = ccee_Pago.Pag_NoPago " +
                                "and ccee_CargoEconomico_timbre.fk_Colegiado = " + id + " " +
                                "and ccee_CargoEconomico_timbre.fk_DocContable is null " +
                                "UNION ALL " +
                                "select ccee_CargoEconomico_colegiado.CEcC_NoCargoEconomico as cargo, ccee_Pago.Pag_NoPago as pago " +
                                        ", ccee_CargoEconomico_colegiado.fk_Colegiado as col, ccee_CargoEconomico_colegiado.fk_DocContable as contable " +
                                        ", ccee_CargoEconomico_colegiado.CEcC_Monto as monto, ccee_Pago.fk_Cajero as caj, ccee_Pago.Pag_Fecha as fech " +
                                "from ccee_CargoEconomico_colegiado, ccee_Pago " +
                                "where ccee_CargoEconomico_colegiado.fk_Pago = ccee_Pago.Pag_NoPago " +
                                "and ccee_CargoEconomico_colegiado.fk_Colegiado = " + id + " " +
                                "and ccee_CargoEconomico_colegiado.fk_DocContable is null " +
                                ") as t " +
                                "group by t.pago, t.col, t.caj,t.fech " +
                                "order by t.fech desc";

                var pLis = db.Database.SqlQuery<ResponseRecoverReceiptJson>(query).ToList();
                return pLis;
            }
            catch (Exception)
            {
                return null;
            }
            
        }

        [Route("api/Api_RecoverReceipt/{id}/{status}")]
        // GET: api/Api_RecoverReceipt/5
        public List<ResponseRecoverReceiptJson> Get(int id, int status)
        {
            try
            {

                string query = @"select t.pago as pay, t.col as referee, sum(t.monto) as amount, t.caj as cashier, t.fech as datepay, [id] as document 
                                from(  
                                select ccee_CargoEconomico_otros.CEcO_NoCargoEconomico as cargo, ccee_Pago.Pag_NoPago as pago  
                                        , ccee_CargoEconomico_otros.fk_Colegiado as col, ccee_CargoEconomico_otros.fk_DocContable as contable  
                                        , ccee_CargoEconomico_otros.CEcO_Monto as monto, ccee_Pago.fk_Cajero as caj, ccee_Pago.Pag_Fecha as fech  
                                from ccee_CargoEconomico_otros, ccee_Pago  
                                where ccee_CargoEconomico_otros.fk_Pago = ccee_Pago.Pag_NoPago 
                                and ccee_CargoEconomico_otros.fk_DocContable = [id] 
                                UNION ALL 
                                select ccee_CargoEconomico_timbre.CEcT_NoCargoEconomico as cargo, ccee_Pago.Pag_NoPago as pago 
                                        , ccee_CargoEconomico_timbre.fk_Colegiado as col, ccee_CargoEconomico_timbre.fk_DocContable as contable 
                                        , ccee_CargoEconomico_timbre.CEcT_Monto as monto, ccee_Pago.fk_Cajero as caj, ccee_Pago.Pag_Fecha as fech 
                                from ccee_CargoEconomico_timbre, ccee_Pago 
                                where ccee_CargoEconomico_timbre.fk_Pago = ccee_Pago.Pag_NoPago 
                                and ccee_CargoEconomico_timbre.fk_DocContable = [id]
                                UNION ALL 
                                select ccee_CargoEconomico_colegiado.CEcC_NoCargoEconomico as cargo, ccee_Pago.Pag_NoPago as pago 
                                        , ccee_CargoEconomico_colegiado.fk_Colegiado as col, ccee_CargoEconomico_colegiado.fk_DocContable as contable 
                                        , ccee_CargoEconomico_colegiado.CEcC_Monto as monto, ccee_Pago.fk_Cajero as caj, ccee_Pago.Pag_Fecha as fech 
                                from ccee_CargoEconomico_colegiado, ccee_Pago 
                                where ccee_CargoEconomico_colegiado.fk_Pago = ccee_Pago.Pag_NoPago 
                                and ccee_CargoEconomico_colegiado.fk_DocContable = [id] 
                                ) as t 
                                group by t.pago, t.col, t.caj,t.fech 
                                order by t.fech desc";
                query = query.Replace("[id]", id.ToString());
                var pLis = db.Database.SqlQuery<ResponseRecoverReceiptJson>(query).ToList();
                return pLis;
            }
            catch (Exception)
            {
                return null;
            }

        }

        // POST: api/Api_RecoverReceipt
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_RecoverReceipt/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_RecoverReceipt/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}

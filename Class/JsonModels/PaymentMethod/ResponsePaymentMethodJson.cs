﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.PaymentMethod
{
    public class ResponsePaymentMethodJson
    {
        public int id { get; set; }
        public int? typeMethod { get; set; }
        public string description { get; set; }
    }
}
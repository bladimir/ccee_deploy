﻿using reCaptcha;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using webcee.Class.JsonModels._Tools;
using webcee.Models;

namespace webcee.Controllers
{
    public class AccountController : Controller
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: Account
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Recaptcha = ReCaptcha.GetHtml(ConfigurationManager.AppSettings["ReCaptcha:SiteKey"]);
            ViewBag.publicKey = ConfigurationManager.AppSettings["ReCaptcha:SiteKey"];
            ViewBag.status = true;
            return View();
        }

        [HttpPost]
        public ActionResult Index(string email)
        {
            try
            {
                if (ModelState.IsValid && ReCaptcha.Validate(ConfigurationManager.AppSettings["ReCaptcha:SecretKey"]))
                {
                    try
                    {
                        
                        ccee_Usuario user = db.ccee_Usuario.Single(d => d.Usu_Correo == email);
                        ViewBag.question = user.Usu_PreguntaClave;
                        ViewBag.idmail = user.Usu_NoUsuario;
                        ViewBag.status = false;
                    }
                    catch
                    {
                        ViewBag.status = true;
                    }
                    
                    return View();
                }

                ViewBag.RecaptchaLastErrors = ReCaptcha.GetLastErrors(this.HttpContext);
                ViewBag.publicKey = ConfigurationManager.AppSettings["ReCaptcha:SiteKey"];
                ViewBag.status = true;
                return View();
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        public ActionResult NewPassword()
        {
            return View();
        }

        public ActionResult TermsAndConditions()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Register()
        {
            ViewBag.Recaptcha = ReCaptcha.GetHtml(ConfigurationManager.AppSettings["ReCaptcha:SiteKey"]);
            ViewBag.publicKey = ConfigurationManager.AppSettings["ReCaptcha:SiteKey"];
            return View();
        }


        [HttpPost]
        public ActionResult Register(string email)
        {
            try
            {
                if (ModelState.IsValid && ReCaptcha.Validate(ConfigurationManager.AppSettings["ReCaptcha:SecretKey"]))
                {
                    // Do what you need

                    return View("RegisterConfirmation");
                }

                ViewBag.RecaptchaLastErrors = ReCaptcha.GetLastErrors(this.HttpContext);

                ViewBag.publicKey = ConfigurationManager.AppSettings["ReCaptcha:SiteKey"];

                return View();
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
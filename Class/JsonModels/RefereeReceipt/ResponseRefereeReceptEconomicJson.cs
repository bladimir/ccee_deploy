﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.RefereeReceipt
{
    public class ResponseRefereeReceptEconomicJson
    {
        public DateTime? date { get; set; }
        public string economic { get; set; }
        public decimal? amount { get; set; }
        public int month { get; set; }
        public int year { get; set; }
        public string textEconomic {
            get
            {
                CultureInfo culture;
                culture = CultureInfo.CreateSpecificCulture("es-GT");
                if (month <= 0) return economic;
                 
                return this.economic + " " + culture.DateTimeFormat.GetMonthName(this.month)
                            + " del " + year;
            }
        }
    }
}
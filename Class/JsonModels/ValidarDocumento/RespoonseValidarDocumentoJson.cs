﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.ValidarDocumento
{
    public class RespoonseValidarDocumentoJson
    {
        public int number { get; set; }
        public string name { get; set; }
        public DateTime? dateEmit { get; set; }
        public DateTime? dateFinish { get; set; }
        public string institution { get; set; }
        public DateTime? dateConsumption { get; set; }
    }
}
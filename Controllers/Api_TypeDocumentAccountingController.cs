﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.TypeDocumentAccounting;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_TypeDocumentAccountingController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_TypeDocumentAccounting
        public IEnumerable<ResponseTypeDocumentAccountingJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                var tempTypeDocumentAcc = from doc in db.ccee_TipoDocContable
                                       select new ResponseTypeDocumentAccountingJson()
                                       {
                                           id = doc.TDC_NoTipoDocContable,
                                           name = doc.TDC_Nombre,
                                           description = doc.TDC_Descripcion,
                                           templateXML = doc.TDC_XmlPlantilla
                                       };
                Autorization.saveOperation(db, autorization, "Obteniendo tipo documento contable");
                return tempTypeDocumentAcc;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_TypeDocumentAccounting/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_TypeDocumentAccounting
        public IHttpActionResult Post(RequestTypeDocumentAccountingJson typeDocument)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_TipoDocContable tempTypeDocumentAcc = new ccee_TipoDocContable()
                {
                    TDC_Nombre = typeDocument.name,
                    TDC_Descripcion = typeDocument.description,
                    TDC_XmlPlantilla = typeDocument.templateXML
                };

                db.ccee_TipoDocContable.Add(tempTypeDocumentAcc);
                Autorization.saveOperation(db, autorization, "Agregando tipo documento contable");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_TypeDocumentAccounting/5
        public IHttpActionResult Put(int id, RequestTypeDocumentAccountingJson typeDocument)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_TipoDocContable tempTypeDocumentAcc = db.ccee_TipoDocContable.Find(id);

                if (tempTypeDocumentAcc == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempTypeDocumentAcc.TDC_Nombre = typeDocument.name;
                tempTypeDocumentAcc.TDC_Descripcion = typeDocument.description;
                tempTypeDocumentAcc.TDC_XmlPlantilla = typeDocument.templateXML;

                db.Entry(tempTypeDocumentAcc).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando tipo documento contable");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_TypeDocumentAccounting/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_TipoDocContable tempTypeDocumentAcc = db.ccee_TipoDocContable.Find(id);
                if (tempTypeDocumentAcc == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_TipoDocContable.Remove(tempTypeDocumentAcc);
                Autorization.saveOperation(db, autorization, "Eliminando tipo documento contable");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}

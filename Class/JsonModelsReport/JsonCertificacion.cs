﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonCertificacion
    {
        public DateTime? tempFecha { get; set; }
        public string fecha {
            get
            {
                return (tempFecha == null) ? "" : tempFecha.Value.ToString("dd/MM/yyyy");
            }
        }
        public string correlativo {  get {
                return "#" + id;
            }
        }
        public string colegiado { get; set; }
        public string cajero { get; set; }
        public string estatus { get {
                if(tempestado == null || tempestado == 0)
                {
                    return "(NC)";
                }else if(tempestado == 1)
                {
                    return "(C)";
                }else if (tempestado == 2)
                {
                    return "(I)";
                }else
                {
                    return "";
                }
            }
        }

        public int id { get; set; }
        public string hashd { get; set; }
        public int? tempestado { get; set; }
        public int col { get; set; }
    }
}
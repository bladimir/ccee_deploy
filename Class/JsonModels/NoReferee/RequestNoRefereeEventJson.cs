﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.NoReferee
{
    public class RequestNoRefereeEventJson
    {
        public string name { get; set; }
        public string lastname { get; set; }
        public string nit { get; set; }
        public int movil { get; set; }
        public string address { get; set; }
        public string remark { get; set; }
        public int idSubEvent { get; set; }
        public int idType { get; set; }
        public int idNoReferee { get; set; }
    }
}
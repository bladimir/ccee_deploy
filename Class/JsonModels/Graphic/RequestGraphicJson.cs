﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Graphic
{
    public class RequestGraphicJson
    {
        public string description { get; set; }
        public string reportId { get; set; }
        public string workspaceCollection { get; set; }
        public string workspaceId { get; set; }
    }
}
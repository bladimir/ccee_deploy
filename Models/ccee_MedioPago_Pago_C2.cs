//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace webcee.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ccee_MedioPago_Pago_C2
    {
        public Nullable<int> MPP_NoMedioPP { get; set; }
        public Nullable<decimal> MPP_Monto { get; set; }
        public string MPP_NoTransaccion { get; set; }
        public string MPP_NoDocumento { get; set; }
        public Nullable<int> MPP_Reserva { get; set; }
        public Nullable<int> MPP_Rechazado { get; set; }
        public Nullable<System.DateTime> MPP_FechaHora { get; set; }
        public int fk_MedioPago { get; set; }
        public Nullable<int> fk_Pago { get; set; }
        public int fk_Banco { get; set; }
        public string MPP_Observacion { get; set; }
        public Nullable<int> MPP_Nomina { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.TypeAccounting
{
    public class ResponseTypeAccountingJson
    {
        public int id { get; set; }
        public string description { get; set; }
    }
}
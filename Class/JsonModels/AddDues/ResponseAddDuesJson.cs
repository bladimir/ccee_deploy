﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.AddDues
{
    public class ResponseAddDuesJson
    {
        public decimal totalC { get; set; }
        public decimal totalT { get; set; }
    }
}
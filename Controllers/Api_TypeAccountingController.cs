﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.TypeAccounting;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_TypeAccountingController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_TypeAccounting
        public IEnumerable<ResponseTypeAccountingJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                var tempTypeAccounting = from acc in db.ccee_TipoPartida
                                     select new ResponseTypeAccountingJson()
                                     {
                                         id = acc.TPa_NoTipoPartida,
                                         description = acc.TPa_Descripcion
                                     };
                Autorization.saveOperation(db, autorization, "Obteniendo tipo partida");
                return tempTypeAccounting;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_TypeAccounting/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_TypeAccounting
        public IHttpActionResult Post(RequestTypeAccountingJson typeAccounting)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_TipoPartida tempTypeAccounting = new ccee_TipoPartida()
                {
                    TPa_Descripcion = typeAccounting.description
                };

                db.ccee_TipoPartida.Add(tempTypeAccounting);
                Autorization.saveOperation(db, autorization, "Agregando tipo partida");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_TypeAccounting/5
        public IHttpActionResult Put(int id, RequestTypeAccountingJson typeAccounting)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_TipoPartida tempTypeAccounting = db.ccee_TipoPartida.Find(id);

                if (tempTypeAccounting == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempTypeAccounting.TPa_Descripcion = typeAccounting.description;

                db.Entry(tempTypeAccounting).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando tipo partida");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_TypeAccounting/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_TipoPartida tempTypeAccounting = db.ccee_TipoPartida.Find(id);
                if (tempTypeAccounting == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_TipoPartida.Remove(tempTypeAccounting);
                Autorization.saveOperation(db, autorization, "Eliminando tipo partida");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

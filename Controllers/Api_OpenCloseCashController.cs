﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using webcee.Class.Class;
using webcee.Class.JsonModels.Bill;
using webcee.Class.JsonModels.OpenCloseCash;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_OpenCloseCashController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_OpenCloseCash
        public IEnumerable<ResponseOpenCloseCashJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempOpenClose = from apc in db.ccee_AperturaCierre
                                    where DbFunctions.TruncateTime(apc.ApC_Fecha) == DbFunctions.TruncateTime(DateTime.Now)
                                    orderby apc.ApC_Fecha descending
                                    select new ResponseOpenCloseCashJson()
                                    {
                                        id = apc.ApC_NoAperturaCierre,
                                        amountCash = apc.ApC_MontoEfectivo,
                                        amountDoc = apc.ApC_MontoDocumento,
                                        amountTrans = apc.ApC_MontoTransaccionElectronica,
                                        openClose = apc.ApC_AbreCierra,
                                        total = apc.ApC_Total, 
                                        date = apc.ApC_Fecha,
                                        idCash = apc.fk_Caja
                                    };
                Autorization.saveOperation(db, autorization, "Obteniendo apertura cierre por fecha");
                return tempOpenClose;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_OpenCloseCash/5
        public ResponseOpenCloseCashJson Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ResponseOpenCloseCashJson tempOpenClose = (from apc in db.ccee_AperturaCierre
                                                            where  apc.fk_Cajero == id
                                                            orderby apc.ApC_Fecha descending
                                                            select new ResponseOpenCloseCashJson()
                                                            {
                                                                id = apc.ApC_NoAperturaCierre,
                                                                amountCash = apc.ApC_MontoEfectivo,
                                                                amountDoc = apc.ApC_MontoDocumento,
                                                                amountTrans = apc.ApC_MontoTransaccionElectronica,
                                                                openClose = apc.ApC_AbreCierra,
                                                                total = apc.ApC_Total,
                                                                date = apc.ApC_Fecha,
                                                                idCash = apc.fk_Caja
                                                            }).FirstOrDefault();
                if (tempOpenClose != null)
                {
                    DateTime dateOC = tempOpenClose.date.Value.Date;
                    DateTime dateNow = DateTime.Now.Date;
                    if (dateOC < dateNow && tempOpenClose.openClose != 3)
                    {
                        tempOpenClose.openClose = 4;
                    }
                }
                else
                {
                    ccee_Cajero caj = db.ccee_Cajero.Find(id);
                    if (caj != null)
                    {
                        tempOpenClose = new ResponseOpenCloseCashJson();
                        tempOpenClose.openClose = 3;
                        return tempOpenClose;
                    }
                }

                Autorization.saveOperation(db, autorization, "Obteniendo apertura cierre de cajero");
                return tempOpenClose;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_OpenCloseCash
        public IHttpActionResult Post(RequestOpenCloseCashJson openClose)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    string autorization = Request.Headers.Authorization.Parameter;
                    if (!Autorization.enableAutorization(db, autorization)) return null;


                    ccee_AperturaCierre tempOpenClose = new ccee_AperturaCierre()
                    {
                        ApC_MontoEfectivo = openClose.amountCash,
                        ApC_MontoDocumento = openClose.amountDoc,
                        ApC_MontoTransaccionElectronica = openClose.amountTrans,
                        ApC_Total = openClose.total,
                        ApC_AbreCierra = openClose.openClose,
                        ApC_Fecha = DateTime.Now,
                        fk_Caja = openClose.idCash,
                        fk_Cajero = openClose.idCashier,
                        ApC_NoRecibo = openClose.receipt - 1
                    };

                    db.ccee_AperturaCierre.Add(tempOpenClose);
                    db.SaveChanges();

                    if (openClose.openClose == 3)
                    {
                        RequestBillJson bill = openClose.bills;

                        var jsonSerialiser = new JavaScriptSerializer();
                        string json = jsonSerialiser.Serialize(bill.list);

                        ccee_BilletesDen tempBill = new ccee_BilletesDen();
                        tempBill.BiD_Fecha = bill.date.Date;
                        tempBill.fk_AperturaCierre = tempOpenClose.ApC_NoAperturaCierre;
                        tempBill.BiD_JsonDenominacion = json;
                        db.ccee_BilletesDen.Add(tempBill);
                        db.SaveChanges();
                    }
                    Autorization.saveOperation(db, autorization, "Agregando apertura cierre");
                    transaction.Commit();
                    return Ok(HttpStatusCode.OK);
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return Ok(HttpStatusCode.BadRequest);
                }
            }
        }

        // PUT: api/Api_OpenCloseCash/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_OpenCloseCash/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Cash
{
    public class RequestCashJson
    {
        public string name { get; set; }
        public string activo { get; set; }
        public int fkPaymentCenter { get; set; }
    }
}
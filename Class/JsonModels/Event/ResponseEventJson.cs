﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Event
{
    public class ResponseEventJson
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string initation { get; set; }
        public string finalize { get; set; }
        public int status { get; set; }
    }
}
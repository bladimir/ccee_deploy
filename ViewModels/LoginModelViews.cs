﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webcee.Models;

namespace webcee.ViewModels
{
    public class LoginModelViews
    {

        public ccee_Usuario User { get; set; }
        public List<string> Operations { get; set; }
        public int idCashier { get; set; }
        public int idCash { get; set; }
        public int idOpenClose { get; set; }
        public string keyAutorization { get; set; }
    }
}
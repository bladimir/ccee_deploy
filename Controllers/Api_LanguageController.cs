﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.Language;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_LanguageController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Language
        public IEnumerable<ResponseLanguageJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var contentLanguage = from type in db.ccee_Idioma
                                      orderby type.Idi_Descripcion ascending
                                  select new ResponseLanguageJson()
                                  {
                                      id = type.Idi_NoIdioma,
                                      name = type.Idi_Descripcion
                                  };
                Autorization.saveOperation(db, autorization, "Obteniendo lenguajes");
                return contentLanguage;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_Language/5
        public IEnumerable<ResponseLanguageJson> Get(int id)
        {
            try
            {
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_Language
        public IHttpActionResult Post(RequestLanguageJson language)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Idioma tempLanguage = new ccee_Idioma()
                {
                    Idi_Descripcion = language.name
                };

                db.ccee_Idioma.Add(tempLanguage);
                Autorization.saveOperation(db, autorization, "Agregando lenguaje");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_Language/5
        public IHttpActionResult Put(int id, RequestLanguageJson language)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Idioma tempLanguage = db.ccee_Idioma.Find(id);

                if (tempLanguage == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempLanguage.Idi_Descripcion = language.name;

                db.Entry(tempLanguage).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando lenguaje");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_Language/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Idioma tempLanguage = db.ccee_Idioma.Find(id);
                if (tempLanguage == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_Idioma.Remove(tempLanguage);
                Autorization.saveOperation(db, autorization, "Eliminando lenguaje");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}

﻿(function () {
    angular.module('generals')
        .controller('listedDocuments', ['$scope', '$http', 'CONSTANT', '$location', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $location, $anchorScroll, generalactionsFactory) {
            var idPay = $location.search().id;
            $scope.list;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.idUniversidadUpdate = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }


            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getListDocs = function (idCashier) {
                console.log(idPay);
                $scope.textError = "";
                if (idPay == null && $scope.typeDoc == 1) {
                    $scope.textError = "No se puede seleccionar certificaciones";
                    return;
                }
                $scope.showLoadingFunction();
                var information = {
                    range: $scope.range,
                    typeDocument: $scope.typeDoc,
                    idPay: idPay,
                    idCashier: idCashier
                }
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_GET_LISTENEDDOCUMENTS, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            console.log(response);
                            if (response.data != null) {
                                $scope.list = response.data;
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            
            

            $scope.cleanData = function () {
                $scope.idUniversidadUpdate = 0;
                $scope.universityname = "";
            }


        }]);

})()
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Constant;
using webcee.Class.JsonModels.TimbreUser;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_TimbreUserController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_TimbreUser
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_TimbreUser/5
        public List<ResponseListTimbreUserJson> Get(int id)
        {
            try
            {
                var listTimbres = (from tim in db.ccee_Timbre
                                  join hoj in db.ccee_HojaTimbre on tim.fk_HojaTimbre equals hoj.HoT_NoHojaTimbre
                                  join val in db.ccee_ValorTimbre on tim.fk_ValorTimbre equals val.VaT_NoValorTimbre
                                  where tim.Tim_Colegiado == id && tim.Tim_Estatus == Constant.timbre_status_sold
                                  select new ResponseListTimbreUserJson()
                                  {
                                      idTimbre = tim.Tim_NoTimbre,
                                      idPage = hoj.HoT_NoHojaTimbre,
                                      value = val.VaT_Denominacion,
                                      noTimbre = tim.Tim_NumeroTimbre,
                                      noPage = hoj.HoT_NumeroHoja
                                  }).ToList();
                return listTimbres;
            }
            catch (Exception)
            {

            }


            return null;
        }

        // POST: api/Api_TimbreUser
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_TimbreUser/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_TimbreUser/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

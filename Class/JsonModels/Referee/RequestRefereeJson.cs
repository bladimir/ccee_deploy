﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Referee
{
    public class RequestRefereeJson
    {
        public int NoColegiado { get; set; }
        public Nullable<DateTime> FechaColegiacion { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string TercerNombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string CasdaApellido { get; set; }
        public string RegCedula { get; set; }
        public string Registro { get; set; }
        public string CedulaExtendida { get; set; }
        public string Dpi { get; set; }
        public string DpiExtendido { get; set; }
        public int? EstadoCivil { get; set; }
        public int? Sexo { get; set; }
        public Nullable<DateTime> FechaNacimiento { get; set; }
        public string Nit { get; set; }
        public string Observaciones { get; set; }
        public int idTipoColegiacion { get; set; }
        public string Notificacion { get; set; }
        public int Estatus { get; set; }
        public Nullable<DateTime> FechaJuramentacion { get; set; }
        public int? idSede { get; set; }
        public decimal? salario { get; set; }
        public int? idSedeActual { get; set; }
        public DateTime? fechaFallecimiento { get; set; }
        public DateTime? fechaGraduacion { get; set; }
        public int esJuramentado { get; set; }
        public int verTexto { get; set; }
        public string placeBirth { get; set; }
        public string nacionality { get; set; }
        public DateTime? dateJubilacion { get; set; }
        public string experence { get; set; }
    }
}
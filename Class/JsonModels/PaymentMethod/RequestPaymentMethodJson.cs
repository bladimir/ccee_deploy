﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.PaymentMethod
{
    public class RequestPaymentMethodJson
    {
        public int typeMethod { get; set; }
        public string description { get; set; }
        public int idIssuingBank { get; set; }
    }
}
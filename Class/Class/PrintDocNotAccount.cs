﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using webcee.Models;

namespace webcee.Class.Class
{
    public class PrintDocNotAccount
    {
        public class tempListEconomicPay
        {
            public int? idReferee { get; set; }
            public int? idPay { get; set; }
            public DateTime? datePay { get; set; }
            public DateTime? dateCreate { get; set; }
            public int typeEconomic { get; set; }
        }

        CCEEEntities db;
        List<tempListEconomicPay> tempListRefereePay;
        List<tempListEconomicPay> tempListTimbrePay;
        public PrintDocNotAccount(CCEEEntities db, int idReferee)
        {
            this.db = db;
            tempListRefereePay = listEconomicReferee(idReferee);
            tempListTimbrePay = listEconomictimbre(idReferee);
        }

        private List<tempListEconomicPay> listEconomicReferee(int idReferee)
        {
            List<tempListEconomicPay> temp = (from car in db.ccee_CargoEconomico_colegiado
                                              join pag in db.ccee_Pago on car.fk_Pago equals pag.Pag_NoPago
                                              where car.fk_Colegiado == idReferee
                                              select new tempListEconomicPay
                                              {
                                                  idReferee = car.fk_Colegiado,
                                                  idPay = car.fk_Pago,
                                                  datePay = pag.Pag_Fecha,
                                                  dateCreate = car.CEcC_FechaGeneracion
                                              }).ToList();
            return temp;
        }

        private List<tempListEconomicPay> listEconomictimbre(int idReferee)
        {
            List<tempListEconomicPay> temp = (from car in db.ccee_CargoEconomico_timbre
                                              join pag in db.ccee_Pago on car.fk_Pago equals pag.Pag_NoPago
                                              where car.fk_Colegiado == idReferee
                                              select new tempListEconomicPay
                                              {
                                                  idReferee = car.fk_Colegiado,
                                                  idPay = car.fk_Pago,
                                                  datePay = pag.Pag_Fecha,
                                                  dateCreate = car.CEcT_FechaGeneracion,
                                                  typeEconomic = car.fk_TipoCargoEconomico
                                              }).ToList();
            return temp;
        }


        public int createDocCertification(int idTypeDoc, int idEconomic, string nameReferee, string idReferee
                                            , string templateHTML, string nameUser, int referee,
                                            DateTime? dateReferee, string idOldDocument, DateTime? dateTempCol, DateTime? dateTempTim,
                                            int idCashier)
        {
            ccee_TipoDoc tempDoc = db.ccee_TipoDoc.Find(idTypeDoc);
            ccee_Documento doc = new ccee_Documento();
            string html = createPrintFileReferee(nameReferee, idReferee, templateHTML, nameUser, 
                                                    referee, dateReferee, dateTempCol, dateTempTim, doc );
            string hashKey = GeneralFunction.MD5Encrypt(html);
            html = html.Replace("[hashkey]", hashKey);
            
            doc.Doc_Observacion = "";
            doc.Doc_FechaHoraEmision = DateTime.Today;
            doc.Doc_Xml = html;
            doc.Doc_HashDoc = hashKey;
            doc.Doc_ANombre = nameReferee;
            doc.fk_TipoDoc = tempDoc.TDC_NoTipoDoc;
            doc.fk_CargoEconomicoOtros = idEconomic;
            doc.Doc_NoDocumentoOld = idOldDocument;
            doc.fk_Cajero = idCashier;

            db.ccee_Documento.Add(doc);
            db.SaveChanges();
            return doc.Doc_NoDocumento;
        }

        private string createPrintFileReferee(string nameREferee, string idReferee,
                                                string templateHTML, string nameUser, int referee,
                                                DateTime? dateReferee, DateTime? dateTempCol,
                                                DateTime? dateTempTim, ccee_Documento document)
        {

            CultureInfo culture;
            culture = CultureInfo.CreateSpecificCulture("es-GT");

            ccee_Colegiado tempReferee = db.ccee_Colegiado.Find(referee);

            tempListEconomicPay refereeEconomic = tempListRefereePay
                                                .Where(o => o.idPay != null)
                                                .OrderByDescending(p => p.dateCreate)
                                                .FirstOrDefault();

            if (dateTempCol != null) refereeEconomic.dateCreate = dateTempCol;


            tempListEconomicPay timbreEconomic = tempListTimbrePay
                                                    .Where(o => o.idPay != null)
                                                    .OrderByDescending(p => p.dateCreate)
                                                    .FirstOrDefault();

            if (dateTempTim != null) timbreEconomic.dateCreate = dateTempTim;

            var tempProf = (from esp in db.ccee_Especialidad
                            join col in db.ccee_Colegiado on esp.pkfk_Colegiado equals col.Col_NoColegiado
                            join tes in db.ccee_TipoEspecialidad on esp.pkfk_TipoEspecialidad equals tes.TEs_NoTipoEspecialidad
                            join uni in db.ccee_Universidad on esp.pkfk_Universidad equals uni.Uni_NoUniversidad
                            where col.Col_NoColegiado == referee && tes.TES_GradoAcademico == "Licenciatura"
                            orderby esp.Esp_FechaObtuvo descending
                            select new
                            {
                                tipo = tes.TEs_NombreTipoEsp,
                                universidad = uni.Uni_NombreUniversidad
                            });

            string academyEsp = "";
            foreach (var prof in tempProf)
            {
                academyEsp += prof.tipo + "/" + prof.universidad + ";";
            }



            //Si es de ambos enviar referencia de Referee para mejor agilidad

            //DateTime? last = (from col in db.ccee_CargoEconomico_colegiado
            //           where col.fk_Colegiado == referee && col.fk_Pago != null
            //           select col.CEcC_FechaGeneracion)
            //           .OrderByDescending(p=>p.Value).FirstOrDefault();

            DateTime? last = (from col in tempListRefereePay
                              where col.idPay != null
                              select col.dateCreate)
                                   .OrderByDescending(p => p.Value).FirstOrDefault();

            DateTime? lastT = (from col in tempListTimbrePay
                              where col.idPay != null
                              select col.dateCreate)
                                   .OrderByDescending(p => p.Value).FirstOrDefault();
            DateTime? lastTempCol = last;

            if(dateTempCol != null)
                last = dateTempCol;
            

            if(dateTempTim != null)
                lastT = dateTempTim;
            
            if (lastT <= last)
            {
                last = lastT;
            }

            if(tempReferee.fk_TipoColegiado == 7)
            {
                // colegiado
                last = lastTempCol;
            }

            if (tempReferee.fk_TipoColegiado == 8)
            {
                // solo seguro
                lastT = (from col in tempListTimbrePay
                         where col.idPay != null && col.typeEconomic ==
                         Constant.Constant.type_economic_postumo
                         select col.dateCreate)
                        .OrderByDescending(p => p.Value).FirstOrDefault();
                last = lastT;
            }

            if (tempReferee.fk_TipoColegiado == 3 || 
                tempReferee.fk_TipoColegiado == 4 )
            {
                last = lastTempCol;
                lastT = (from col in tempListTimbrePay
                         where col.idPay != null && col.typeEconomic == 
                         Constant.Constant.type_economic_postumo
                         select col.dateCreate)
                        .OrderByDescending(p => p.Value).FirstOrDefault();

                if (dateTempCol != null)
                    last = dateTempCol;
                
                if (dateTempTim != null)
                    lastT = dateTempTim;

                if (lastT <= last)
                {
                    last = lastT;
                }
            }

            //if (dateTempCol != null)
            //{
            //    last = dateTempCol;
            //}else
            //{
            //    if(lastT <= last)
            //    {
            //        last = lastT;
            //    }
            //}


            //var listActive = (from his in db.ccee_HistoricoActivo
            //              where his.fk_Colegiado == referee && his.HisA_Activo == 1
            //              select new
            //              {
            //                  Month = his.HisA_Mes,
            //                  Year = his.HisA_Ano
            //              }).ToList();

            //var lastActive = (from li in listActive
            //                 select new
            //                 {
            //                     DateCreate = Convert.ToDateTime(li.Month + "/1/" + li.Year)
            //                 }).OrderByDescending(p=>p.DateCreate).FirstOrDefault();


            DateTime dateNow = DateTime.Now;
            string textDateNow = GeneralFunction.enletras(dateNow.Day.ToString()).Replace(" EXACTOS", "") + " días del mes de " +
                                    dateNow.ToString("MMMM", culture) + " de " +
                                    GeneralFunction.enletras(dateNow.Year.ToString()).Replace(" EXACTOS", "");


            string createdFor = " ";
            string[] divName = nameUser.Split(' ');
            for (int i = 0; i < divName.Count(); i++)
            {
                if (divName[i] == "" || divName[i] == null) continue;
                createdFor += divName[i].Substring(0, 1);
            }

            templateHTML = templateHTML.Replace("[Nombre]", nameREferee);
            if (refereeEconomic != null)
                templateHTML = templateHTML.Replace("[MesColegiado]", refereeEconomic.dateCreate.Value.ToString("MMMM", culture) + "/" + refereeEconomic.dateCreate.Value.Year);


            if (tempReferee.fk_TipoColegiado == 3)
            {
                templateHTML = templateHTML.Replace("[MesTimbre]", " J ");
            } else if (tempReferee.fk_TipoColegiado == 6)
            {
                string data = timbreEconomic.dateCreate.Value.ToString("MMMM", culture) + "/" + timbreEconomic.dateCreate.Value.Year;
                templateHTML = templateHTML.Replace("[MesTimbre]", data + " F ");
            }
            else if (tempReferee.fk_TipoColegiado == 8)
            {
                string data = timbreEconomic.dateCreate.Value.ToString("MMMM", culture) + "/" + timbreEconomic.dateCreate.Value.Year;
                templateHTML = templateHTML.Replace("[MesTimbre]", data + " I ");
            }
            else if ((tempReferee.fk_TipoColegiado == 14 || tempReferee.fk_TipoColegiado == 15) && tempReferee.Col_Estatus == 0)
            {
                string data = timbreEconomic.dateCreate.Value.ToString("MMMM", culture) + "/" + timbreEconomic.dateCreate.Value.Year;
                templateHTML = templateHTML.Replace("[MesTimbre]", data + " CP ");
            }
            else if (tempReferee.fk_TipoColegiado == 7 )
            {
                templateHTML = templateHTML.Replace("[MesTimbre]", " NO INCORPORADO ");
            }else
            {
                if (timbreEconomic != null)
                    templateHTML = templateHTML.Replace("[MesTimbre]", timbreEconomic.dateCreate.Value.ToString("MMMM", culture) + "/" + timbreEconomic.dateCreate.Value.Year);
            }



            string lastDate = "SIN HISTORIAL";
            if (last != null)
            {
                last = last.Value.AddMonths(3);
                lastDate = last.Value.ToString("MMMM", culture) + " del " + last.Value.Year;
            }
            document.Doc_FechaFinal = last;

            DateTime datePrint = DateTime.Now;
            if (tempReferee.fk_TipoColegiado == 16)
            {
                if(tempReferee.Col_FechaColegiacion != null)
                {
                    if(tempReferee.Col_FechaColegiacion.Value > datePrint)
                    {
                        datePrint = tempReferee.Col_FechaColegiacion.Value;
                    }
                }
                //datePrint = ((tempReferee.Col_FechaColegiacion != null) ? tempReferee.Col_FechaColegiacion.Value : DateTime.Now);
            }

            string dateActual = datePrint.Day + " de " + datePrint.ToString("MMMM", culture) + " de " + datePrint.Year;

            

            templateHTML = templateHTML.Replace("[NoColegiado]", idReferee);
            templateHTML = templateHTML.Replace("[FechaLetras]", textDateNow.ToLower());
            templateHTML = templateHTML.Replace("[NombreHoraElaborado]", createdFor);
            templateHTML = templateHTML.Replace("[Profefesion]", ((tempProf == null) ? "": academyEsp));
            templateHTML = templateHTML.Replace("[Universidad]", ((tempProf == null) ? "" : ""));
            templateHTML = templateHTML.Replace("[FechaColegacion]", ((dateReferee == null) ? "" : dateReferee.Value.ToString("dd/MM/yyyy")));
            templateHTML = templateHTML.Replace("[fechaActivo]", lastDate);
            templateHTML = templateHTML.Replace("[HoraElaborado]", datePrint.ToString("dd/MM/yyyy HH:mm"));
            templateHTML = templateHTML.Replace("[FechaActual]", dateActual);
            return templateHTML;

        }
        

    }
}
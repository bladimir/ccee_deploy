﻿(function () {
    angular.module('referee')
        .factory('refereeFactory', ['$http', 'CONSTANT', function ($http, CONSTANT) {

            var refereeFactory = {};
            var authorization = "";

            refereeFactory.setAuthorization = function (id) {
                authorization = id;
            };

            refereeFactory.getAuthorization = function () {
                return authorization;
            };

            refereeFactory.actionGetFunction = function (dirr) {
                return $http.get(dirr, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            };

            refereeFactory.actionGetIntFunction = function (dirr, parameter) {
                return $http.get(dirr + "/" + parameter, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            refereeFactory.actionGetGetIntFunction = function (dirr, parameter, secoundparameter) {
                return $http.get(dirr + "/" + parameter + "/" + secoundparameter, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            refereeFactory.actionGetGetGetGetIntFunction = function (dirr, parameter, secoundparameter, thridparameter, fourthparameter) {
                return $http.get(dirr + "/" + parameter + "/" + secoundparameter + "/" + thridparameter + "/" + fourthparameter, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            refereeFactory.actionGetGetGetGetGetGetIntFunction = function (dirr, parameter, secoundparameter, thridparameter, fourthparameter, fiveparameter, sixparameter) {
                return $http.get(dirr + "/" + parameter + "/" + secoundparameter + "/" + thridparameter + "/" + fourthparameter + "/" + fiveparameter + "/" + sixparameter, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            refereeFactory.actionPostFunction = function (dirr, infojson) {
                return $http.post(dirr, JSON.stringify(infojson),
                    {
                        headers: {
                            'Content-Type': CONSTANT.HEADER_CONTENT_TYPE_JSON,
                            'Authorization': 'Basic ' + authorization
                        }
                    });
            }

            refereeFactory.actionPutFunction = function (dirr, id, infojson) {
                return $http.put(dirr + "/" + id, JSON.stringify(infojson),
                    {
                        headers: {
                            'Content-Type': CONSTANT.HEADER_CONTENT_TYPE_JSON,
                            'Authorization': 'Basic ' + authorization
                        }
                    });
            }

            refereeFactory.actionPutFunctionSpecial = function (dirr, id, esp, uni, infojson) {
                return $http.put(dirr + "/" + id + "/" + esp + "/" + uni, JSON.stringify(infojson),
                    {
                        headers: {
                            'Content-Type': CONSTANT.HEADER_CONTENT_TYPE_JSON,
                            'Authorization': 'Basic ' + authorization
                        }
                    });
            }

            refereeFactory.actionDeleteFunction = function (dirr, parameter) {
                return $http.delete(dirr + "/" + parameter, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            refereeFactory.actionDeleteFunctionSpecial = function (dirr, id, esp, uni) {
                return $http.delete(dirr + "/" + id + "/" + esp + "/" + uni, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            refereeFactory.actionDeleteTwoFunction= function (dirr, id, lan) {
                return $http.delete(dirr + "/" + id + "/" + lan, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            return refereeFactory;
    }]);

})()
﻿(function () {
    angular.module('generals')
        .factory('generalactionsFactory', ['$http', 'CONSTANT', function ($http, CONSTANT) {

            var refereeFactory = {};
            var authorization = "";

            refereeFactory.setAuthorization = function (id) {
                authorization = id;
            };

            refereeFactory.getAuthorization = function () {
                return authorization;
            };

            refereeFactory.actionGetFunction = function (dirr) {
                console.log(authorization);
                return $http.get(dirr, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            };

            refereeFactory.actionGetIntFunction = function (dirr, parameter) {
                return $http.get(dirr + "/" + parameter, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            refereeFactory.actionGetGetIntFunction = function (dirr, parameter, secoundparameter) {
                return $http.get(dirr + "/" + parameter + "/" + secoundparameter, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            refereeFactory.actionGetGetGetIntFunction = function (dirr, parameter, secoundparameter, thirdParameter) {
                return $http.get(dirr + "/" + parameter + "/" + secoundparameter + "/" + thirdParameter, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            refereeFactory.actionGetGetGetGetIntFunction = function (dirr, parameter, secoundparameter, thirdParameter, fourthParaneter) {
                return $http.get(dirr + "/" + parameter + "/" + secoundparameter + "/" + thirdParameter + "/" + fourthParaneter, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            refereeFactory.actionGetGetGetGetGetIntFunction = function (dirr, parameter, secoundparameter, thirdParameter, fourthParaneter, fiveParaneter) {
                return $http.get(dirr + "/" + parameter + "/" + secoundparameter + "/" + thirdParameter + "/" + fourthParaneter + "/" + fiveParaneter, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            refereeFactory.actionGetGetGetGetGetGetIntFunction = function (dirr, parameter, secoundparameter, thirdParameter, fourthParaneter, fiveParaneter, sixParameter) {
                return $http.get(dirr + "/" + parameter + "/" + secoundparameter + "/" + thirdParameter + "/" + fourthParaneter + "/" + fiveParaneter + "/" + sixParameter, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            refereeFactory.actionPostFunction = function (dirr, infojson) {
                return $http.post(dirr, JSON.stringify(infojson),
                    {
                        headers: {
                            'Content-Type': CONSTANT.HEADER_CONTENT_TYPE_JSON,
                            'Authorization': 'Basic ' + authorization
                        }
                    });
            }

            refereeFactory.actionPutFunction = function (dirr, id, infojson) {
                return $http.put(dirr + "/" + id, JSON.stringify(infojson),
                    {
                        headers: {
                            'Content-Type': CONSTANT.HEADER_CONTENT_TYPE_JSON,
                            'Authorization': 'Basic ' + authorization
                        }
                    });
            }

            refereeFactory.actionDeleteFunction = function (dirr, parameter) {
                return $http.delete(dirr + "/" + parameter, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            refereeFactory.actionDeleteFunctionSpecial = function (dirr, id, esp) {
                return $http.delete(dirr + "/" + id + "/" + esp, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            return refereeFactory;
        }]);

})()
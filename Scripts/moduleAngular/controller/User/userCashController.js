﻿(function () {
    angular.module('user')
        .controller('userCash', ['$scope', '$http', 'CONSTANTE', '$anchorScroll', 'userFactory', function ($scope, $http, CONSTANTE, $anchorScroll, userFactory) {
            $scope.listCashier;
            $scope.listCashierAssing;
            $scope.listUser;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANTE.NOTIFY_SHOW_CSS;
            $scope.idRolUpdate = 0;
            $scope.idRolOperation = 0;
            $scope.showRepeat = false;


            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANTE.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANTE.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                userFactory.actionGetIntFunction(CONSTANTE.URL_API_USERCASHIER, 0)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listCashier = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });

                userFactory.actionGetIntFunction(CONSTANTE.URL_API_USERCASHIER, 1)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listCashierAssing = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });

                userFactory.actionGetFunction(CONSTANTE.URL_API_USER)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listUser = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.setAuth = function () {
                userFactory.actionGetFunction(CONSTANTE.URL_GET_TOKEN)
                    .then(function (response) {
                        userFactory.setAuthorization(response.data);
                        $scope.init();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.get = function () {
                $scope.showLoadingFunction();
                userFactory.actionGetIntFunction(CONSTANTE.URL_API_USERCASHIER, 1)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listCashierAssing = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });

                userFactory.actionGetIntFunction(CONSTANTE.URL_API_USERCASHIER, 0)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listCashier = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });

            }


            $scope.verifyUser = function () {
                $scope.showRepeat = false;
                for (var i = 0; i < $scope.listCashierAssing.length; i++) {
                    if ($scope.listCashierAssing[i].idUser == $scope.selectUser.id) {
                        $scope.showRepeat = true;
                    }
                }
                if (!$scope.showRepeat) {
                    $scope.updateAddUser();
                }
            }

            $scope.updateAddUser = function () {
                $scope.showLoadingFunction();
                var information = {
                    idUser: $scope.selectUser.id,
                    nameUser: $scope.selectUser.nombre + ' ' + $scope.selectUser.apellido
                }
                userFactory.actionPutFunction(CONSTANTE.URL_API_USERCASHIER, $scope.selectCashier.id, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.get();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.updateDeleteUser = function (cashier) {
                $scope.showLoadingFunction();
                var information = {
                    idUser: 0,
                    nameUser: "Cajero - " + cashier.id
                }
                userFactory.actionPutFunction(CONSTANTE.URL_API_USERCASHIER, cashier.id, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.get();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.changeActive = function (state) {
                if (state == 1) {
                    return "Activo";
                } else if (state == 0) {
                    return "Inactivo";
                } else {
                    return "Indefinido";
                }
            }




        }]);

})()
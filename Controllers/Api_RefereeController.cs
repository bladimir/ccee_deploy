﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.Referee;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_RefereeController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Referee
        public IEnumerable<ResponseRefereeJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var listReferee = from col in db.ccee_Colegiado
                                  select new ResponseRefereeJson()
                                  {
                                      NoColegiado = col.Col_NoColegiado,
                                      FechaColegiacion = col.Col_FechaColegiacion,
                                      PrimerNombre = (col.Col_PrimerNombre != null) ? col.Col_PrimerNombre : "",
                                      SegundoNombre = (col.Col_SegundoNombre != null) ? col.Col_SegundoNombre : "",
                                      TercerNombre = (col.Col_TercerNombre != null) ? col.Col_TercerNombre : "",
                                      PrimerApellido = (col.Col_PrimerApellido != null) ? col.Col_PrimerApellido : "",
                                      SegundoApellido = (col.Col_SegundoApellido != null) ? col.Col_SegundoApellido : "",
                                      CasdaApellido = (col.Col_CasdaApellido != null) ? col.Col_CasdaApellido : "",
                                      RegCedula = col.Col_RegCedula,
                                      Registro = col.Col_Registro,
                                      CedulaExtendida = col.Col_CedulaExtendida,
                                      Dpi = col.Col_Dpi,
                                      DpiExtendido = col.Col_DpiExtendido,
                                      EstadoCivil = col.Col_EstadoCivil,
                                      Sexo = col.Col_Sexo,
                                      FechaNacimiento = col.Col_FechaNacimiento,
                                      Nit = col.Col_Nit,
                                      Observaciones = col.Col_Observaciones,
                                      Notificacion = col.Col_Notificacion,
                                      Estatus = col.Col_Estatus.Value,
                                      imagen = (col.Col_Foto == null || col.Col_Foto.Equals("")) ? Constant.url_dir_images_no_found : col.Col_Foto,
                                      FechaJuramentacion = col.Col_FechaJuramentacion,
                                      Salario = col.Col_Salario,
                                      fechaFallecimiento = col.Col_FechaFallecimiento,
                                      esJuramentado = col.Col_EsJuramentado,
                                      verTexto = col.Col_VerTexto,
                                      placeBirth = col.Col_LugarNacimiento,
                                      nacionality = col.Col_Nacionalidad,
                                      dateJubilacion = col.Col_jubilado,
                                      experence = col.Col_Experiencia_en
                                  };
                Autorization.saveOperation(db, autorization, "Obteniendo listado completo colegiado");
                return listReferee;
            }
            catch (Exception)
            {
                return null;
            }

        }

        // GET: api/Api_Referee/5
        public ResponseRefereeJson Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var listReferee = from col in db.ccee_Colegiado
                                  join typeCol in db.ccee_TipoColegiado on col.fk_TipoColegiado equals typeCol.Tco_NoTipoColegiado
                                  where col.Col_NoColegiado == id
                                  select new ResponseRefereeJson()
                                  {
                                      FechaColegiacion = col.Col_FechaColegiacion,
                                      PrimerNombre = (col.Col_PrimerNombre != null) ? col.Col_PrimerNombre : "",
                                      SegundoNombre = (col.Col_SegundoNombre != null) ? col.Col_SegundoNombre : "",
                                      TercerNombre = (col.Col_TercerNombre != null) ? col.Col_TercerNombre : "",
                                      PrimerApellido = (col.Col_PrimerApellido != null) ? col.Col_PrimerApellido : "",
                                      SegundoApellido = (col.Col_SegundoApellido != null) ? col.Col_SegundoApellido : "",
                                      CasdaApellido = (col.Col_CasdaApellido != null) ? col.Col_CasdaApellido : "",
                                      RegCedula = col.Col_RegCedula,
                                      Registro = col.Col_Registro,
                                      CedulaExtendida = col.Col_CedulaExtendida,
                                      Dpi = col.Col_Dpi,
                                      DpiExtendido = col.Col_DpiExtendido,
                                      EstadoCivil = col.Col_EstadoCivil,
                                      Sexo = col.Col_Sexo,
                                      FechaNacimiento = col.Col_FechaNacimiento,
                                      Nit = col.Col_Nit,
                                      Observaciones = col.Col_Observaciones,
                                      typeReferee = typeCol.Tco_Descripcion,
                                      Notificacion = col.Col_Notificacion,
                                      Estatus = col.Col_Estatus.Value,
                                      imagen = (col.Col_Foto == null || col.Col_Foto.Equals("")) ? Constant.url_dir_images_no_found : col.Col_Foto,
                                      FechaJuramentacion = col.Col_FechaJuramentacion,
                                      Salario = col.Col_Salario,
                                      idSedeJuramentada = col.fk_Sede,
                                      idSedeActual = col.fk_SedeRegistrada,
                                      fechaFallecimiento = col.Col_FechaFallecimiento,
                                      esJuramentado = col.Col_EsJuramentado,
                                      verTexto = col.Col_VerTexto,
                                      placeBirth = col.Col_LugarNacimiento,
                                      nacionality = col.Col_Nacionalidad,
                                      dateJubilacion = col.Col_jubilado,
                                      experence = col.Col_Experiencia_en
                                  };

                var mailrefereeL = (from ml in db.ccee_Mail
                                    where ml.fk_Colegiado == id
                                    select ml.Mai_MailDir).ToList();
                string mailreferee = "";
                if (mailrefereeL != null && mailrefereeL.Count != 0){
                        mailreferee = mailrefereeL.Last();
                }

                Autorization.saveOperation(db, autorization, "Obteniendo colegiado " + id);

                foreach (ResponseRefereeJson contReferee in listReferee)
                {
                    contReferee.FechaColegiacionTexto = GeneralFunction.DateOfString(contReferee.FechaColegiacion);
                    contReferee.FechaNacimientoTexto = GeneralFunction.DateOfString(contReferee.FechaNacimiento);
                    contReferee.FechaJuramentacionTexto = GeneralFunction.DateOfString(contReferee.FechaJuramentacion);
                    contReferee.fechaJubilacion = GeneralFunction.DateOfString(contReferee.dateJubilacion);
                    contReferee.nameSedeActual = getHeadquarter(contReferee.idSedeActual);
                    contReferee.nameSedeJuramentada = getHeadquarter(contReferee.idSedeJuramentada);
                    if (mailreferee != null && !mailreferee.Equals(""))
                    {
                        contReferee.mail = mailreferee;
                    }
                    return contReferee;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }


        [Route("api/Api_Referee/{first}/{secound}/{third}/{last}/{lastSecound}/{married}")]
        public IEnumerable<ResponseRefereeJson> Get(string first, string secound, string third, string last, string lastSecound, string married)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                /*first = ((first.Equals("0")) ? ("WHERE Col_PrimerNombre like '" + first + "'") : first);
                secound = ((secound.Equals("0")) ? "" : secound);
                last = ((last.Equals("0")) ? "" : last);
                lastSecound = ((lastSecound.Equals("0")) ? "" : lastSecound);*/

                string searchReferee = @"SELECT [Col_NoColegiado] as NoColegiado 
                                              ,[Col_PrimerNombre] as PrimerNombre 
                                              ,[Col_SegundoNombre] as SegundoNombre 
                                              ,[Col_TercerNombre] as TercerNombre 
                                              ,[Col_PrimerApellido] as PrimerApellido
                                              ,[Col_SegundoApellido] as SegundoApellido
                                              ,[Col_CasdaApellido] as CasdaApellido
                                              ,[Col_Dpi] as Dpi 
                                          FROM [dbo].[ccee_Colegiado]
                                          WHERE 1 = 1 ";
                searchReferee += ((first.Equals("0")) ? "" : (" AND Col_PrimerNombre like '" + first + "%' "));
                searchReferee += ((secound.Equals("0")) ? "" : (" AND Col_SegundoNombre like '" + secound + "%' "));
                searchReferee += ((third.Equals("0")) ? "" : (" AND Col_TercerNombre like '" + third + "%' "));
                searchReferee += ((last.Equals("0")) ? "" : (" AND Col_PrimerApellido like '" + last + "%' "));
                searchReferee += ((lastSecound.Equals("0")) ? "" : (" AND Col_SegundoApellido like '" + lastSecound + "%' "));
                searchReferee += ((married.Equals("0")) ? "" : (" AND Col_CasdaApellido like '" + married + "%' "));

                var listReferee = db.Database.SqlQuery<ResponseRefereeJson>(searchReferee).ToList();

                /*var listReferee = from col in db.ccee_Colegiado
                                  join typeCol in db.ccee_TipoColegiado on col.fk_TipoColegiado equals typeCol.Tco_NoTipoColegiado
                                  where col.Col_PrimerNombre.Contains(first) &&
                                  col.Col_SegundoNombre.Contains(secound) &&
                                  col.Col_PrimerApellido.Contains(last) &&
                                  col.Col_SegundoApellido.Contains(lastSecound)
                                  select new ResponseRefereeJson()
                                  {
                                      NoColegiado = col.Col_NoColegiado,
                                      FechaColegiacion = col.Col_FechaColegiacion,
                                      PrimerNombre = (col.Col_PrimerNombre != null) ? col.Col_PrimerNombre : "",
                                      SegundoNombre = (col.Col_SegundoNombre != null) ? col.Col_SegundoNombre : "",
                                      TercerNombre = (col.Col_TercerNombre != null) ? col.Col_TercerNombre : "",
                                      PrimerApellido = (col.Col_PrimerApellido != null) ? col.Col_PrimerApellido : "",
                                      SegundoApellido = (col.Col_SegundoApellido != null) ? col.Col_SegundoApellido : "",
                                      CasdaApellido = (col.Col_CasdaApellido != null) ? col.Col_CasdaApellido : "",
                                      RegCedula = col.Col_RegCedula,
                                      Registro = col.Col_Registro,
                                      CedulaExtendida = col.Col_CedulaExtendida,
                                      Dpi = col.Col_Dpi,
                                      DpiExtendido = col.Col_DpiExtendido,
                                      EstadoCivil = col.Col_EstadoCivil,
                                      Sexo = col.Col_Sexo,
                                      FechaNacimiento = col.Col_FechaNacimiento,
                                      Nit = col.Col_Nit,
                                      Observaciones = col.Col_Observaciones,
                                      typeReferee = typeCol.Tco_Descripcion,
                                      Notificacion = col.Col_Notificacion,
                                      Estatus = col.Col_Estatus.Value,
                                      imagen = (col.Col_Foto == null || col.Col_Foto.Equals("")) ? Constant.url_dir_images_no_found : col.Col_Foto,
                                      FechaJuramentacion = col.Col_FechaJuramentacion,
                                      Salario = col.Col_Salario,
                                      idSedeJuramentada = col.fk_Sede,
                                      idSedeActual = col.fk_SedeRegistrada,
                                      fechaFallecimiento = col.Col_FechaFallecimiento,
                                      esJuramentado = col.Col_EsJuramentado,
                                      verTexto = col.Col_VerTexto,
                                      placeBirth = col.Col_LugarNacimiento,
                                      nacionality = col.Col_Nacionalidad
                                  };*/
                Autorization.saveOperation(db, autorization, "Obteniendo listado colegiado por nombre: " + first + " " + last);

                return listReferee;
            }
            catch (Exception)
            {
                return null;
            }
        }



        // POST: api/Api_Referee
        public IHttpActionResult Post(RequestRefereeJson referee)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        ccee_Colegiado tempReferee = new ccee_Colegiado()
                        {
                            Col_NoColegiado = referee.NoColegiado,
                            Col_FechaColegiacion = referee.FechaColegiacion,
                            Col_PrimerNombre = referee.PrimerNombre,
                            Col_SegundoNombre = referee.SegundoNombre,
                            Col_TercerNombre = referee.TercerNombre,
                            Col_PrimerApellido = referee.PrimerApellido,
                            Col_SegundoApellido = referee.SegundoApellido,
                            Col_CasdaApellido = referee.CasdaApellido,
                            Col_RegCedula = referee.RegCedula,
                            Col_Registro = referee.Registro,
                            Col_CedulaExtendida = referee.CedulaExtendida,
                            Col_Dpi = referee.Dpi,
                            Col_DpiExtendido = referee.DpiExtendido,
                            Col_EstadoCivil = referee.EstadoCivil,
                            Col_Sexo = referee.Sexo,
                            Col_FechaNacimiento = referee.FechaNacimiento,
                            Col_Nit = referee.Nit,
                            Col_Observaciones = referee.Observaciones,
                            Col_Notificacion = referee.Notificacion,
                            Col_Estatus = referee.Estatus,
                            Col_FechaJuramentacion = referee.FechaJuramentacion,
                            Col_Salario = referee.salario,
                            Col_FechaGraduacion = referee.fechaGraduacion,
                            fk_TipoColegiado = referee.idTipoColegiacion,
                            fk_Sede = referee.idSede,
                            fk_SedeRegistrada = referee.idSedeActual,
                            Col_EsJuramentado = 0,
                            Col_VerTexto = 0,
                            Col_LugarNacimiento = referee.placeBirth,
                            Col_Nacionalidad = referee.nacionality
                        };

                        db.ccee_Colegiado.Add(tempReferee);

                    
                        db.SaveChanges();
                        createEconomicNewreferee(referee.NoColegiado, referee.FechaJuramentacion);
                        createPenality(referee.fechaGraduacion, referee.NoColegiado);
                        db.SaveChanges();
                        Autorization.saveOperation(db, autorization, "Agregando colegiado " + referee.NoColegiado);
                        transaction.Commit();
                        return Ok(HttpStatusCode.OK);
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        return Ok(HttpStatusCode.Conflict);
                    }
                    
                }
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_Referee/5
        public IHttpActionResult Put(int id, RequestRefereeJson referee)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;


                ccee_Colegiado tempReferee = db.ccee_Colegiado.Find(id);
                if (tempReferee != null)
                {
                    tempReferee.Col_FechaColegiacion = (referee.FechaColegiacion == null) ? tempReferee.Col_FechaColegiacion : referee.FechaColegiacion;
                    tempReferee.Col_PrimerNombre = referee.PrimerNombre;
                    tempReferee.Col_SegundoNombre = referee.SegundoNombre;
                    tempReferee.Col_TercerNombre = referee.TercerNombre;
                    tempReferee.Col_PrimerApellido = referee.PrimerApellido;
                    tempReferee.Col_SegundoApellido = referee.SegundoApellido;
                    tempReferee.Col_CasdaApellido = referee.CasdaApellido;

                    tempReferee.Col_RegCedula = referee.RegCedula;
                    tempReferee.Col_Registro = referee.Registro;
                    tempReferee.Col_CedulaExtendida = referee.CedulaExtendida;
                    tempReferee.Col_Dpi = referee.Dpi;
                    tempReferee.Col_DpiExtendido = referee.DpiExtendido;
                    tempReferee.Col_EstadoCivil = (referee.EstadoCivil == null) ? tempReferee.Col_EstadoCivil : referee.EstadoCivil;
                    tempReferee.Col_Sexo = (referee.Sexo == null) ? tempReferee.Col_Sexo : referee.Sexo;
                    tempReferee.Col_FechaNacimiento = (referee.FechaNacimiento == null) ? tempReferee.Col_FechaNacimiento : referee.FechaNacimiento;

                    tempReferee.Col_Nit = referee.Nit;
                    tempReferee.Col_Observaciones = referee.Observaciones;
                    tempReferee.Col_Notificacion = referee.Notificacion;
                    tempReferee.Col_FechaJuramentacion = (referee.FechaJuramentacion == null) ? tempReferee.Col_FechaJuramentacion : referee.FechaJuramentacion;
                    tempReferee.Col_Salario = referee.salario;
                    tempReferee.fk_TipoColegiado = (referee.idTipoColegiacion == 0) ? tempReferee.fk_TipoColegiado : referee.idTipoColegiacion;
                    

                    tempReferee.fk_Sede = (referee.idSede == 0) ? tempReferee.fk_Sede : referee.idSede;
                    tempReferee.fk_SedeRegistrada = (referee.idSedeActual == 0) ? tempReferee.fk_SedeRegistrada : referee.idSedeActual;
                    tempReferee.Col_FechaFallecimiento = (referee.fechaFallecimiento == null) ? tempReferee.Col_FechaFallecimiento : referee.fechaFallecimiento;
                    tempReferee.Col_Revertido = null;
                    tempReferee.Col_EsJuramentado = referee.esJuramentado;
                    tempReferee.Col_VerTexto = referee.verTexto;
                    tempReferee.Col_LugarNacimiento = referee.placeBirth;
                    tempReferee.Col_Nacionalidad = referee.nacionality;
                    tempReferee.Col_jubilado = (referee.dateJubilacion == null) ? tempReferee.Col_jubilado : referee.dateJubilacion;
                    tempReferee.Col_Experiencia_en = referee.experence;

                    if (isDisableStatus(tempReferee.fk_TipoColegiado, @"^(4|5)?$"))
                    {
                        tempReferee.Col_Estatus = 0;
                    }


                    db.Entry(tempReferee).State = EntityState.Modified;
                    Autorization.saveOperation(db, autorization, "Actualizando colegiado " + tempReferee.Col_NoColegiado);
                    db.SaveChanges();



                    return Ok(Constant.status_code_success);

                }

            }
            catch
            {
                return Ok(Constant.status_code_error);
            }

            return Ok(Constant.status_code_error);
        }

        // DELETE: api/Api_Referee/5
        public IHttpActionResult Delete(int id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    string autorization = Request.Headers.Authorization.Parameter;
                    if (!Autorization.enableAutorization(db, autorization)) return null;

                    var listDocs = from doc in db.ccee_DocExpRequisito
                                   where doc.fk_Colegiado == id
                                   select doc;
                    foreach (var docs in listDocs)
                    {
                        db.ccee_DocExpRequisito.Remove(docs);
                    }

                    ccee_Colegiado tempCol = db.ccee_Colegiado.Find(id);
                    if(tempCol == null)
                    {
                        return Ok(HttpStatusCode.NotFound);
                    }
                    db.ccee_Colegiado.Remove(tempCol);

                    db.SaveChanges();
                    Autorization.saveOperation(db, autorization, "Eliminando colegiado: " + id);
                    transaction.Commit();
                    return Ok(HttpStatusCode.OK);
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return Ok(HttpStatusCode.BadRequest);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void createPenality(DateTime? dateGraduation, int idReferee)
        {
            if(dateGraduation != null)
            {
                DateTime lastSix = DateTime.Today.AddMonths(-6);
                if(dateGraduation.Value < lastSix)
                {
                    int typePenality = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "VarCargoMulta"));
                    ccee_TipoCargoEconomico tempTypeEco = db.ccee_TipoCargoEconomico.Find(typePenality);
                    ccee_CargoEconomico_otros tempEco = new ccee_CargoEconomico_otros();
                    tempEco.CEcO_Monto = tempTypeEco.TCE_Valor;
                    tempEco.fk_Colegiado = idReferee;
                    tempEco.fk_TipoCargoEconomico = tempTypeEco.TCE_NoTipoCargoEconomico;
                    tempEco.CEcO_FechaGeneracion = DateTime.Today;
                    db.ccee_CargoEconomico_otros.Add(tempEco);
                }
            }
        }


        private void createEconomicNewreferee(int idReferee, DateTime? date)
        {
            int monthsAddNewReferee = 3;
            int idBase = Constant.type_economic_base;
            int idPostumo = Constant.type_economic_postumo;
            DateTime datePayValue = ((date == null) ? DateTime.Today : date.Value);

            string newReferee = GeneralFunction.getKeyTableVariable(db, Constant.table_var_new_referee);
            int idNewReferee = Convert.ToInt32(newReferee);
            ccee_TipoCargoEconomico newTypeEco = (from tca in db.ccee_TipoCargoEconomico
                                                   where tca.TCE_NoTipoCargoEconomico == idNewReferee
                                                   select tca).Single();

            string calculation = newTypeEco.TCE_CondicionCalculo;
            string[] arrayCalculation = calculation.Split(',');
            foreach (string item in arrayCalculation)
            {

                int economic = Convert.ToInt32(item);
                ccee_TipoCargoEconomico tempTypeEco = (from tca in db.ccee_TipoCargoEconomico
                                                      where tca.TCE_NoTipoCargoEconomico == economic
                                                      select tca).Single();
                ccee_CargoEconomico_otros tempEco = new ccee_CargoEconomico_otros();
                tempEco.CEcO_Monto = tempTypeEco.TCE_Valor;
                tempEco.fk_Colegiado = idReferee;
                tempEco.fk_TipoCargoEconomico = tempTypeEco.TCE_NoTipoCargoEconomico;
                tempEco.CEcO_FechaGeneracion = datePayValue;
                db.ccee_CargoEconomico_otros.Add(tempEco);

            }

            List<ccee_TipoCargoEconomico> listTipoCargos = getListTypeEco(Constant.table_var_colegiado);
            ccee_CargoEconomico_colegiado tempEconomicoCol;
            DateTime datePay = datePayValue;

            for (int k = 0; k < monthsAddNewReferee; k++)
            {
                DateTime nextMount = datePay.AddMonths(k);
                for (int j = 0; j < listTipoCargos.Count; j++)
                {

                    tempEconomicoCol = new ccee_CargoEconomico_colegiado()
                    {
                        CEcC_Monto = listTipoCargos[j].TCE_Valor,
                        CEcC_FechaGeneracion = nextMount,
                        fk_TipoCargoEconomico = listTipoCargos[j].TCE_NoTipoCargoEconomico,
                        fk_Colegiado = idReferee,
                        fk_Pago = null
                    };
                    db.ccee_CargoEconomico_colegiado.Add(tempEconomicoCol);

                }
            }
            

            listTipoCargos = getListTypeEco(Constant.table_var_timbre);

            decimal valorP = db.ccee_TipoCargoEconomico.Find(idPostumo).TCE_Valor.Value;
            decimal montoTimbreGen = GeneralFunction.generarMontoTimbre(idReferee, db);
            ccee_CargoEconomico_timbre tempTim;
            datePay = datePayValue;
            for (int k = 0; k < monthsAddNewReferee; k++)
            {
                DateTime nextMount = datePay.AddMonths(k);
                tempTim = new ccee_CargoEconomico_timbre()
                {
                    CEcT_Monto = montoTimbreGen,
                    CEcT_FechaGeneracion = nextMount,
                    fk_TipoCargoEconomico = idBase,
                    fk_Colegiado = idReferee,
                    fk_Pago = null
                };
                db.ccee_CargoEconomico_timbre.Add(tempTim);

                tempTim = new ccee_CargoEconomico_timbre()
                {
                    CEcT_Monto = valorP,
                    CEcT_FechaGeneracion = nextMount,
                    fk_TipoCargoEconomico = idPostumo,
                    fk_Colegiado = idReferee,
                    fk_Pago = null
                };
                db.ccee_CargoEconomico_timbre.Add(tempTim);
            }
            
            db.SaveChanges();


        }
        
        private string getHeadquarter(int? idHead)
        {
            if (idHead == null) return "";
            var nameHead = db.ccee_Sede.Find(idHead).Sed_Direccion;
            return nameHead;
        }

        

        private List<ccee_TipoCargoEconomico> getListTypeEco(string constant)
        {
            string cole = GeneralFunction.getKeyTableVariable(db, constant);
            int idEconomicReferee = Convert.ToInt32(cole);
            ccee_TipoCargoEconomico tempTypeEco = (from tca in db.ccee_TipoCargoEconomico
                                                   where tca.TCE_NoTipoCargoEconomico == idEconomicReferee
                                                   select tca).Single();

            string calculation = tempTypeEco.TCE_CondicionCalculo;
            string[] arrayCalculation = calculation.Split(',');
            List<int> intCalculation = new List<int>();
            foreach (string item in arrayCalculation)
            {
                intCalculation.Add(Convert.ToInt32(item));
            }

            var listE = from tca in db.ccee_TipoCargoEconomico
                        where intCalculation.Contains(tca.TCE_NoTipoCargoEconomico)
                        select tca;
            return listE.ToList();
        }

        public bool isDisableStatus(int? status, string regex)
        {
            if (status == null) return false;
            Regex r = new Regex(regex, RegexOptions.IgnoreCase);
            Match mat = r.Match(status.ToString());
            return mat.Success;
        } 


    }
}

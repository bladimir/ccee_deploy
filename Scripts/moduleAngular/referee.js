﻿(function () {

    var app = angular.module('referee', ['ngMaterial']);

    app.config(function ($mdThemingProvider) {

        $mdThemingProvider.definePalette('amazingPaletteName', {
            '50': 'ffffff',
            '100': 'F1441C',
            '200': 'ef9a9a',
            '300': 'F1441C',
            '400': 'ef5350',
            '500': '00B60E',
            '600': 'e53935',
            '700': 'd32f2f',
            '800': 'c62828',
            '900': '000000',
            'A100': 'ffffff',
            'A200': '000000',
            'A400': 'ff1744',
            'A700': 'd50000',
            'contrastDefaultColor': 'dark',    // whether, by default, text (contrast)
            // on this palette should be dark or light

            'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
             '200', '300', '400', 'A100'],
            'contrastLightColors': undefined    // could also specify this if default was 'dark'
        });

        $mdThemingProvider.theme('default')
        .backgroundPalette('amazingPaletteName')
        .primaryPalette('orange');
    });

    app.controller("showreferee", ['$scope', '$http', '$location', '$anchorScroll', 'CONSTANT', '$window', 'refereeFactory',
        function ($scope, $http, $location, $anchorScroll, CONSTANT, $window, refereeFactory) {

            var idReferee = $location.search().id;
            var isConvenio = false;
            var isPlanilla = false;

            $scope.regexNit = CONSTANT.REGEX_NIT;
        $scope.regexDate = CONSTANT.REGEX_FECHA;
        $scope.regexMail = CONSTANT.REGEX_EMAIL;
        $scope.regexPhone = CONSTANT.REGEX_PHONE;
        $scope.regexPhoneState = CONSTANT.REGEX_STATEPHONE;
        $scope.formatDate = CONSTANT.FORMAT_DATE;
        $scope.regexAmount = CONSTANT.REGEX_AMOUNT;
        $scope.regexDigit = CONSTANT.REGEX_DIGIT;
        $scope.idRefereeContent = 0;
        $scope.completeNameReferee = "";
        $scope.listPhone;
        $scope.listTypeReferee;
        $scope.listAddress;
        $scope.listMail;
        $scope.listFamily;
        $scope.listBeneficiary;
        $scope.listPhoneFamily;
        $scope.listAddressfamily;
        $scope.listEmployment;
        $scope.listTypeEspecialty;
        $scope.listUniversity;
        $scope.listAcademy;
        $scope.listCountry;
        $scope.listDepartament;
        $scope.listMunicipality;
        $scope.listLanguage;
        $scope.listLanguageGeneral;
        $scope.listDataFozen;
        $scope.menuAddress = false;
        $scope.menuAddressDelete = true;
        $scope.menuFamily = false;
        $scope.menuFamilyDelete = true;
        $scope.menuFamilyPhone = false;
        $scope.menuFamilyAddress = false;
        $scope.menuEmployment = false;
        $scope.menuEmploymentDelete = false;
        $scope.menuAcademy = false;
        $scope.menuAcademyDelete = false;
        $scope.updateIdReferee = null;
        $scope.updateIdRefereePhoneMAil = null;
        $scope.updateObjectAcademy = null;
        $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
        $scope.listHeadquarters;
        $scope.showForzer = false;

        
        $scope.getIdReferee = function () {
            $scope.idReferee = idReferee;
            if ($scope.idReferee > 0) {
                refereeFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        refereeFactory.setAuthorization(response.data);
                        console.log(refereeFactory.getAuthorization());
                        $scope.loadReferee($scope.idReferee);
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }
        }

        $scope.setAuth = function () {
            refereeFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
            .then(function (response) {
                refereeFactory.setAuthorization(response.data);
                console.log(refereeFactory.getAuthorization());
            }, function (error) {
                $scope.hideLoadingFunction();
                console.log(error);
            });
        }

        $scope.sortType = 'datestartText';
        $scope.sortReverse = false;


        //Muestra de pantalla de Crear y Editar Direccion
        $scope.showMenuAddress = function () {
            $scope.menuAddress = !$scope.menuAddress;
            $scope.updateIdReferee = null;
        }

        //Muesta el menu de eliminar para confirmar
        $scope.showMenuAddressDelete = function () {
            $scope.menuAddressDelete = !$scope.menuAddressDelete;
            $scope.updateIdReferee = null;
        }

        //Muestra de pantalla de Crear y Editar Direccion
        $scope.showMenuFamily = function () {
            $scope.menuFamily = !$scope.menuFamily
            $scope.updateIdReferee = null;
        }

        //Muesta el menu de agregar y ver telefonos asi como la llamada al servidor
        $scope.showMenuFamilyPhone = function (family) {
            $scope.menuFamilyPhone = (($scope.menuFamilyPhone) ? $scope.menuFamilyPhone : !$scope.menuFamilyPhone);
            $scope.selectedFamilyReferee(family);
            $scope.getPhoneFamilyReferee(family);
        }

        $scope.hideMenuFamilyPhone = function () {
            $scope.menuFamilyPhone = (($scope.menuFamilyPhone) ? !$scope.menuFamilyPhone : $scope.menuFamilyPhone);
        }

        $scope.hideMenuFamilyAddress = function () {
            $scope.menuFamilyAddress = (($scope.menuFamilyAddress) ? !$scope.menuFamilyAddress : $scope.menuFamilyAddress);
        }

        //Muesta el menu de agregar y ver direccion asi como la llamada al servidor
        $scope.showMenuFamilyAddress = function (family) {
            $scope.menuFamilyAddress = (($scope.menuFamilyAddress) ? $scope.menuFamilyAddress : !$scope.menuFamilyAddress);
            $scope.selectedFamilyReferee(family);
            $scope.getAddressRefereeFamily(family);
        }

        //Muestra el menu principal de eliminar en family
        $scope.showMenuFamilyDelete = function () {
            $scope.menuFamilyDelete = !$scope.menuFamilyDelete;
            $scope.updateIdReferee = null;
        }

        ///muestra para editar y definir cual es el que hay que editar
        $scope.showEditAddress = function (Address) {
            console.log(Address);
            $scope.updateIdReferee = Address.idAddress;
            $scope.addressStreet = Address.street;
            $scope.addressNumber = Address.number;
            $scope.addressZone = Address.zone;
            $scope.addressColony = Address.colony;
            $scope.addressCaserio = Address.caserio;
            $scope.addressBarrio = Address.barrio;
            $scope.addressType = $scope.changeTypeAddress(Address.typeAddress);
            $scope.addressCountry = Address.municipality;
            $scope.addressDep = Address.departament;
            $scope.addressMuni = Address.country;
            $scope.menuAddress = (($scope.menuAddress) ? $scope.menuAddress : !$scope.menuAddress);
            $scope.addressEnableNot = ((Address.notification == 1) ? true : false);
        }

        ///muestra para editar y definir cual es el que hay que editar Family
        $scope.showEditFamily = function (Family) {
            $scope.updateIdReferee = Family.id;
            $scope.showDataforUpdateFamily(Family);
            $scope.menuFamily = (($scope.menuFamily) ? $scope.menuFamily : !$scope.menuFamily);
        }

        $scope.showMenuEmployCreate = function () {
            $scope.menuEmployment = !$scope.menuEmployment;
            $scope.updateIdReferee = null;
        }

        $scope.showMenuEmployEdit = function (employ) {
            $scope.updateIdReferee = employ.id;
            $scope.showDataforUpdateEmploy(employ);
            $scope.menuEmployment = (($scope.menuEmployment) ? $scope.menuEmployment : !$scope.menuEmployment);
        }

        $scope.showMenuAcademyCreate = function () {
            $scope.menuAcademy = !$scope.menuAcademy;
            $scope.updateObjectAcademy = null;
        }

        $scope.showMenuAcademyEdit = function (academy) {
            $scope.updateObjectAcademy = academy;
            $scope.showDataforUpdateAcademy(academy);
            $scope.menuAcademy = (($scope.menuAcademy) ? $scope.menuAcademy : !$scope.menuAcademy);
        }

        /////////////

        //Carga de todos los datos de la vista
        $scope.loadReferee = function (idreferee) {
            $scope.showLoadingFunction();

            //$scope.getReferee(idreferee);
            $scope.getStatusInit(idreferee);
            $scope.getAddressreferee(idreferee);
            $scope.getPhoneReferee(idreferee);
            $scope.getTypeReferee();
            $scope.getMailReferee(idreferee);
            $scope.getFamilyReferee(idreferee);
            $scope.getBeneficiaryReferee();
            $scope.getEmploymentReferee(idreferee);
            $scope.getUniversity();
            $scope.getTypeEspecialty();
            $scope.getAcademyReferee(idreferee);
            $scope.getCountryReferee();
            $scope.getLanguageGeneralReferee();
            $scope.getLanguageReferee(idreferee);
            $scope.getBalanceReferee(idreferee);
            $scope.getBalanceFrozen(idreferee);
            $scope.headquarter();
        }

        //obtener los datos del colegiado
        $scope.getReferee = function (idreferee) {
            $scope.statusUpdate = "";
            console.log("aqui va referey")
            refereeFactory.actionGetIntFunction(CONSTANT.URL_API_REFEREE, idreferee)
                .then(function (response) {
                    var data = response.data;
                    if (data != null) {
                        $scope.idRefereeContent = idreferee;
                        $scope.completeNameReferee = data.PrimerNombre + " " + data.SegundoNombre + " "
                                                     + data.TercerNombre + " " + data.PrimerApellido + " " + data.SegundoApellido + " " + data.CasdaApellido;
                        $scope.firstNameReferee = data.PrimerNombre;
                        $scope.secoundNameReferee = data.SegundoNombre;
                        $scope.thirdNameReferee = data.TercerNombre;
                        $scope.fistLastNameReferee = data.PrimerApellido;
                        $scope.secoundLastNameReferee = data.SegundoApellido;
                        $scope.marriedNameReferee = data.CasdaApellido;

                        $scope.nitReferee = data.Nit;
                        $scope.noCedulaReferee = data.RegCedula;
                        $scope.extCedulaReferee = data.CedulaExtendida;
                        $scope.regCedulaReferee = data.Registro;
                        $scope.noDpiReferee = data.Dpi;
                        $scope.extDpieferee = data.DpiExtendido;
                        $scope.dateReferee = data.FechaColegiacionTexto;
                        $scope.birthdayReferee = data.FechaNacimientoTexto;
                        $scope.notifyReferee = data.Notificacion;
                        $scope.statusReferee = data.Estatus;
                        $scope.dateSweReferee = data.FechaJuramentacionTexto;
                        $scope.salaryReferee = data.Salario;
                        $scope.headquarterRefereeJu = data.nameSedeJuramentada;
                        $scope.headquarterReferee = data.nameSedeActual;
                        $scope.placeBirthReferee = data.placeBirth;
                        $scope.nacionalityReferee = data.nacionality;
                        $scope.dateJubilacionReferee = data.fechaJubilacion;
                        $scope.experenceReferee = data.experence;

                        if (data.Sexo == 0) {
                            $scope.genderReferee = "Masculino";
                        } else if (data.Sexo == 1) {
                            $scope.genderReferee = "Femenino";
                        } else {
                            $scope.genderReferee = "No definido";
                        }

                        $scope.statusReferee = data.Estatus;
                        $scope.statuscivilReferee = data.EstadoCivil;
                        $scope.remarkReferee = data.Observaciones;
                        $scope.typeReferee = data.typeReferee;
                        $scope.imageReferee = data.imagen;
                        $scope.deathReferee = data.fechaFallecimiento;

                        $scope.enableJu = ((data.esJuramentado == 1) ? true : false);
                        $scope.enablePen = ((data.verTexto == 1) ? true : false);

                        console.log("----------------", data);
                        $scope.hideLoadingFunction();
                    }
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
        }

        //actualizar colegiado
        $scope.getStatus = function () {
            refereeFactory.actionGetIntFunction(CONSTANT.URL_GET_STATUS, $scope.idRefereeContent)
                .then(function (response) {
                    $scope.loadReferee($scope.idRefereeContent);
                }, function (error) {
                    console.log(error);
                });
        }

        //init actualizar colegiado
        $scope.getStatusInit = function (idReferee) {
            refereeFactory.actionGetIntFunction(CONSTANT.URL_GET_STATUS, idReferee)
                .then(function (response) {
                    $scope.getReferee(idReferee);
                }, function (error) {
                    console.log(error);
                });
        }

        //-----------------------------------------------------Sedes En colegiado --------------------

        $scope.headquarter = function () {
            refereeFactory.actionGetFunction(CONSTANT.URL_API_HEADQUARTERS)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listHeadquarters = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
        }

        


        //-------------------------------------------------------Address------------------------------------------------

        //obtiene la direccion del colegiado
        $scope.getAddressreferee = function (idReferee) {
            refereeFactory.actionGetIntFunction(CONSTANT.URL_API_ADDRESS, idReferee)
                .then(function (response) {
                    console.log(response);
                    $scope.listAddress = response.data;
                }, function (error) {
                    console.log(error);
                });
        }

        //crear direccion por ahora sin la parte del municipio
        $scope.addAddressReferee = function () {
            var statusT = (($scope.addressEnableNot == true) ? 1 : 0);
            $scope.showLoadingFunction();
            if ($scope.formaddressreferee.$valid) {

                var information = {
                    street: $scope.addressStreet,
                    number: $scope.addressNumber,
                    zone: $scope.addressZone,
                    colony: $scope.addressColony,
                    caserio: $scope.addressCaserio,
                    barrio: $scope.addressBarrio,
                    typeAddress: $scope.addressSelectType,
                    municipality: $scope.addressSelectMuni.id,
                    idReferee: $scope.idRefereeContent,
                    notification: statusT
                }

                refereeFactory.actionPostFunction(CONSTANT.URL_API_ADDRESS, information)
                    .then(function (response) {
                        $scope.showMenuAddress();
                        $scope.getAddressreferee($scope.idRefereeContent);
                        $scope.hideLoadingFunction();
                        $scope.clearDataAddress();
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
            }
        }

        //Actualizar la direccion de un usuario sin la parte de municipio
        $scope.updateAddressReferee = function () {
            var statusT = (($scope.addressEnableNot == true) ? 1 : 0);
            $scope.showLoadingFunction();
            var information = {
                street: $scope.addressStreet,
                number: $scope.addressNumber,
                zone: $scope.addressZone,
                colony: $scope.addressColony,
                caserio: $scope.addressCaserio,
                barrio: $scope.addressBarrio,
                typeAddress: (($scope.addressSelectType == null) ? 0 : $scope.addressSelectType),
                municipality: (($scope.addressSelectMuni == null) ? 0 : $scope.addressSelectMuni.id),
                idReferee: $scope.idRefereeContent,
                notification: statusT
            };
            
            refereeFactory.actionPutFunction(CONSTANT.URL_API_ADDRESS, $scope.updateIdReferee, information)
                    .then(function (response) {
                        console.log(response);
                        $scope.hideLoadingFunction();
                        $scope.showMenuAddress();
                        $scope.getAddressreferee($scope.idRefereeContent);
                        $scope.clearDataAddress();
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
        }

        //funcion para eliminar la direccion
        $scope.deleteAddressReferee = function (Address) {
            $scope.showLoadingFunction();
            refereeFactory.actionDeleteFunction(CONSTANT.URL_API_ADDRESS, Address.idAddress)
                    .then(function (response) {
                        console.log(response);
                        $scope.hideLoadingFunction();
                        $scope.getAddressreferee($scope.idRefereeContent);
                        $scope.clearDataAddress();
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
        }

        //Limpia la pantalla
        $scope.clearDataAddress = function () {
            $scope.addressStreet = "";
            $scope.addressNumber = "";
            $scope.addressZone = "";
            $scope.addressColony = "";
            $scope.addressCaserio = "";
            $scope.addressBarrio = "";
            $scope.addressType = "";
            $scope.addressCountry = "";
            $scope.addressDep = "";
            $scope.addressMuni = "";
            $scope.addressEnableNot = false;
            $scope.addressSelectType = null;
            $scope.addressSelectMuni = null;
        }

        //-----------------------------------------------------AddressFamily----------------------------------------------

        //obtiene la direccion del colegiado respecto al familiar
        $scope.getAddressRefereeFamily = function (idFamily) {
            refereeFactory.actionGetIntFunction(CONSTANT.URL_API_ADDRESSFAMILY, idFamily.id)
                .then(function (response) {
                    console.log(response);
                    $scope.listAddressfamily = response.data;
                }, function (error) {
                    console.log(error);
                });
        }

        //define la direccion almacenada, en colegiada a un familiar
        $scope.addAddressRefereeFamily = function (address) {
            $scope.showLoadingFunction();
            var information = {
                idFamily: $scope.updateIdRefereePhoneMAil,
                idAddress: address.idAddress
            }

            refereeFactory.actionPostFunction(CONSTANT.URL_API_ADDRESSFAMILY, information)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    if(response.data == 200){
                        $scope.hideMenuFamilyAddress();
                    }
                    
                }, function (error) {
                    console.log(error);
                    $scope.hideLoadingFunction();
                });
        }

        //Elimina una referencia de la direccion de la familia
        $scope.deleteAddressRefereeFamily = function (addres) {
            $scope.showLoadingFunction();
            refereeFactory.actionDeleteFunction(CONSTANT.URL_API_ADDRESSFAMILY, addres.idAddressFamily)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.hideMenuFamilyAddress();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
        }

        

        //-------------------------------------------------------Phone------------------------------------------------

        //Agregamos telefono
        $scope.addPhone = function (phone) {
            $scope.showLoadingFunction();

            var information = {
                number: phone.number,
                cod: ((phone.cod == null || phone.cod == "") ? 502 : phone.cod),
                idReferee: $scope.idRefereeContent,
                idFamily: null,
                type: phone.type
            }

            console.log(information);
            refereeFactory.actionPostFunction(CONSTANT.URL_API_PHONE, information)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    if (response.data == 200) {
                        $scope.hideMenuFamilyPhone();
                        $scope.getPhoneReferee($scope.idRefereeContent);
                    }

                }, function (error) {
                    console.log(error);
                    $scope.hideLoadingFunction();
                });
        }

        //obtiene el listado de los telefonos
        $scope.getPhoneReferee = function (idReferee) {

            refereeFactory.actionGetIntFunction(CONSTANT.URL_API_PHONE, idReferee)
                .then(function (response) {
                    console.log(response);
                    $scope.listPhone = response.data;
                }, function (error) {
                    console.log(error);
                });

        }

        //Cambio de nombre a tipo de telefono
        $scope.defineTypePhone = function (type) {
            switch (type) {
                case 1:
                    return "Teléfono casa";
                    break;
                case 2:
                    return "Teléfono oficina";
                    break;
                case 3:
                    return "Fax oficina";
                    break;
                case 4:
                    return "Celular";
                    break;
                case 5:
                    return "Otro";
                    break;
                default:
                    return "";

            }
        }

        //elimina un telefono del listado
        $scope.deletePhoneReferee = function (phone) {
            $scope.showLoadingFunction();
            refereeFactory.actionDeleteFunction(CONSTANT.URL_API_PHONE, phone.id)
                    .then(function (response) {
                        if(response.data){
                            $scope.getPhoneReferee($scope.idRefereeContent);
                        }
                        $scope.hideLoadingFunction();
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
        }

        //Obtiene el listado de los telefonos de un familiar 
        $scope.getPhoneFamilyReferee = function (family) {
            refereeFactory.actionGetGetIntFunction(CONSTANT.URL_API_PHONE, $scope.idRefereeContent, family.id)
                .then(function (response) {
                    console.log(response);
                    $scope.listPhoneFamily = response.data;
                }, function (error) {
                    console.log(error);
                });
        }

        //Agregamos un telefono al familiar en conjunto al colegiado
        $scope.addPhoneFamilyReferee = function (phone) {
            if ($scope.formphonefamily.$valid) {
                $scope.showLoadingFunction();

                var information = {
                    number: phone.number,
                    cod: ((phone.cod == null || phone.cod == "") ? 502 : phone.cod),
                    idReferee: $scope.idRefereeContent,
                    idFamily: $scope.updateIdRefereePhoneMAil
                }

                console.log(information);
                refereeFactory.actionPostFunction(CONSTANT.URL_API_PHONE, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.hideMenuFamilyPhone();
                        }
                        
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
            }
        }

        //agregamos un telefono existente a un familiar pero este actualiza
        $scope.addPhoneFamilyRefereeWithSelect = function (phone) {
            if ($scope.formphonefamilyselect.$valid) {
                $scope.updatePhoneFamilyRefereeWithSelect(phone, $scope.updateIdRefereePhoneMAil);
            }
        }

        //Quita la referencia de un telefono de un colegiado al familiar
        $scope.deletePhoneFamilyRefereeWithSelect = function (phone) {
            $scope.updatePhoneFamilyRefereeWithSelect(phone, null);
        }
        
        //Metodo que sirve para actualizar los otros
        $scope.updatePhoneFamilyRefereeWithSelect = function (phone, idFamily) {
            $scope.showLoadingFunction();
            console.log(phone);
            var information = {
                number: phone.number,
                cod: ((phone.state == null || phone.state == "") ? 502 : phone.state),
                idReferee: $scope.idRefereeContent,
                idFamily: idFamily
            }

            console.log(information);
            refereeFactory.actionPutFunction(CONSTANT.URL_API_PHONE, phone.id, information)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    if (response.data == 200) {
                        $scope.hideMenuFamilyPhone();
                    }

                }, function (error) {
                    console.log(error);
                    $scope.hideLoadingFunction();
                });
        }

        //-------------------------------------------------------Mail------------------------------------------------

        //obtener el listado de los mail
        $scope.getMailReferee = function (idReferee) {
            refereeFactory.actionGetIntFunction(CONSTANT.URL_API_MAIL, idReferee)
                .then(function (response) {
                    console.log(response);
                    $scope.listMail = response.data;
                }, function (error) {
                    console.log(error);
                });
        }

        //agrega en correo al sistema
        $scope.addMailReferee = function (mail) {
            $scope.showLoadingFunction();
            var information = {
                mail: mail.text,
                idReferee: $scope.idRefereeContent
            }

            refereeFactory.actionPostFunction(CONSTANT.URL_API_MAIL, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        $scope.getMailReferee($scope.idRefereeContent);
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

        }

        //Eliminar el listado de mail
        $scope.deleteMailReferee = function (mail) {
            $scope.showLoadingFunction();
            refereeFactory.actionDeleteFunction(CONSTANT.URL_API_MAIL, mail.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if(response.data == 200){
                            $scope.getMailReferee($scope.idRefereeContent);
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
        }

        //-------------------------------------------------------Family------------------------------------------------

        //muestra la data en la pantalla para que se pueda editar
        $scope.showDataforUpdateFamily = function (family) {
            $scope.familyfirstname = family.firstName;
            $scope.familysecoundname = family.secoundName;
            $scope.familythirdname = family.thirdName;
            $scope.familyfirslastname = family.firstLastName;
            $scope.familysecoundlastname = family.secoundLastName;
            $scope.familymarriedname = family.marriedName;
            $scope.familyTypeBeneficiary = family.typeBeneficiary;
            $scope.familypercentage = family.percentage;
            $scope.familyremark = family.remark;
            $scope.familydpi = family.dpi;
        }

        //obtiene la informacion de la familia
        $scope.getFamilyReferee = function (idReferee) {
            refereeFactory.actionGetIntFunction(CONSTANT.URL_API_FAMILY, idReferee)
                .then(function (response) {
                    console.log(response);
                    $scope.listFamily = response.data;
                }, function (error) {
                    console.log(error);
                });
        }

        //Agrega un nuevo familiar al sistema
        $scope.addFamilyReferee = function () {
            $scope.showLoadingFunction();
            var information = {
                firstName: $scope.familyfirstname,
                secoundName: $scope.familysecoundname,
                thirdName: $scope.familythirdname,
                firstLastName: $scope.familyfirslastname,
                secoundLastName: $scope.familysecoundlastname,
                marriedName: $scope.familymarriedname,
                typeBeneficiary: (($scope.familyselectBeneficiary == null) ? 0 : $scope.familyselectBeneficiary.id),
                idReferee: $scope.idRefereeContent,
                percentage: $scope.familypercentage,
                remark: $scope.familyremark,
                dpi: $scope.familydpi
            }
            console.log(information);
            refereeFactory.actionPostFunction(CONSTANT.URL_API_FAMILY, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.showMenuFamily();
                            $scope.getFamilyReferee($scope.idRefereeContent);
                            $scope.clearDataFamily();
                        }
                        
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

        }

        //Eliminar el familiar en el sistema
        $scope.deleteFamilyReferee = function (family) {
            $scope.showLoadingFunction();
            refereeFactory.actionDeleteFunction(CONSTANT.URL_API_FAMILY, family.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.getFamilyReferee($scope.idRefereeContent);
                            $scope.clearDataFamily();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
        }

        $scope.updateFamilyReferee = function () {
            $scope.showLoadingFunction();
            var information = {
                firstName: $scope.familyfirstname,
                secoundName: $scope.familysecoundname,
                thirdName: $scope.familythirdname,
                firstLastName: $scope.familyfirslastname,
                secoundLastName: $scope.familysecoundlastname,
                marriedName: $scope.familymarriedname,
                typeBeneficiary: (($scope.familyselectBeneficiary == null) ? 0 : $scope.familyselectBeneficiary.id),
                idReferee: $scope.idRefereeContent,
                percentage: $scope.familypercentage,
                remark: $scope.familyremark,
                dpi: $scope.familydpi
            }

            refereeFactory.actionPutFunction(CONSTANT.URL_API_FAMILY, $scope.updateIdReferee, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.showMenuFamily();
                            $scope.getFamilyReferee($scope.idRefereeContent);
                            $scope.clearDataFamily();
                        }

                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

        }

        //Se selecciona el familiar para agregar el telefono y la direccion de un familiar
        $scope.selectedFamilyReferee = function (family) {
            $scope.nameFamily = family.firstName + " " + family.secoundName;
            $scope.updateIdRefereePhoneMAil = family.id;
        }

        $scope.findTypeFamily = function (idType) {
            if ($scope.listBeneficiary == null) return "";
            for (var i = 0; i < $scope.listBeneficiary.length; i++) {
                if ($scope.listBeneficiary[i].id == idType) {
                    return $scope.listBeneficiary[i].name;
                }
            }
        }

        //Limpiar pantalla de Family
        $scope.clearDataFamily = function () {
            $scope.familyfirstname = "";
            $scope.familysecoundname = "";
            $scope.familythirdname = "";
            $scope.familyfirslastname = "";
            $scope.familysecoundlastname = "";
            $scope.familymarriedname = "";
            $scope.familydpi = "";
        }


        //-------------------------------------------------------Beneficiary------------------------------------------------

        //Obtiene el listado de los beneficiarios para la parte de familia
        $scope.getBeneficiaryReferee = function () {
            if ($scope.listBeneficiary == null) {
                refereeFactory.actionGetFunction(CONSTANT.URL_API_BENEFICIARY)
                    .then(function (response) {
                        console.log(response);
                        $scope.listBeneficiary = response.data;
                }, function (error) {
                            console.log(error);
                });
            }
        }

        //-----------------------------------------------------Employment----------------------------------------------------

        //Cuando se le da actualizar muestra el listado de informacion para que se pueda actualizar
        $scope.showDataforUpdateEmploy = function (employment) {
            $scope.employmentdatestartText = employment.datestartText;
            $scope.employmentdatefinishText = employment.datefinishText;
            $scope.employmentemploy = employment.empleador;
            $scope.employmentpuesto = employment.puesto;
            $scope.employmentphone = employment.phone;
            $scope.employmentaddress = employment.address;
            $scope.employmentToDate = ((employment.actual == 1) ? true : false );
            $scope.employmentsalario = employment.salario;
        }

        //Obtiene el listado del historial de trabajo
        $scope.getEmploymentReferee = function (idReferee) {
            refereeFactory.actionGetIntFunction(CONSTANT.URL_API_EMPLOYMENT, idReferee)
                .then(function (response) {
                    console.log(response);
                    $scope.listEmployment = response.data;
                }, function (error) {
                    console.log(error);
                });
        }

        //Agregacion de un nuevo historial de trabajo 
        $scope.addEmploymentReferee = function () {
            $scope.showLoadingFunction();
            var information = {
                datestart: $scope.employmentdatestart,
                datefinish: $scope.employmentdatefinish,
                empleador: $scope.employmentemploy,
                puesto: $scope.employmentpuesto,
                phone: $scope.employmentphone,
                address: $scope.employmentaddress,
                idReferee: $scope.idRefereeContent,
                actual: $scope.employmentToDate,
                salario: $scope.employmentsalario
            }
            console.log(information);
            refereeFactory.actionPostFunction(CONSTANT.URL_API_EMPLOYMENT, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.showMenuEmployCreate();
                            $scope.getEmploymentReferee($scope.idRefereeContent);
                            $scope.clearDataEmployment();
                        }

                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
        }

        //Actualiza la informacion de un historial de trabajo
        $scope.updateEmploymentReferee = function () {
            $scope.showLoadingFunction();
            var information = {
                datestart: $scope.employmentdatestart,
                datefinish: $scope.employmentdatefinish,
                empleador: $scope.employmentemploy,
                puesto: $scope.employmentpuesto,
                phone: $scope.employmentphone,
                address: $scope.employmentaddress,
                idReferee: $scope.idRefereeContent,
                actual: $scope.employmentToDate,
                salario: $scope.employmentsalario
            }
            console.log(information);
            refereeFactory.actionPutFunction(CONSTANT.URL_API_EMPLOYMENT, $scope.updateIdReferee, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.showMenuEmployCreate();
                            $scope.getEmploymentReferee($scope.idRefereeContent);
                            $scope.clearDataEmployment();
                        }

                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
        }

        //Emilina la informacion del historial de trabajo
        $scope.deleteEmploymentReferee = function (employ) {
            $scope.showLoadingFunction();
            refereeFactory.actionDeleteFunction(CONSTANT.URL_API_EMPLOYMENT, employ.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.getEmploymentReferee($scope.idRefereeContent);
                            $scope.clearDataEmployment();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
        }

        //Cambio de estado de Actuamente trabajando en el lugar
        $scope.changeActualEmployment = function (actual) {
            if(actual == 1){
                return "Actualmente trabajando";
            } else {
                return "";
            }
        }

        //Limiar data de trabajo
        $scope.clearDataEmployment = function () {
            $scope.employmentdatestartText = "";
            $scope.employmentdatefinishText = "";
            $scope.employmentemploy = "";
            $scope.employmentpuesto = "";
            $scope.employmentphone = "";
            $scope.employmentaddress = "";
            $scope.employmentToDate = false;
            $scope.employmentsalario = "";
        }


        //------------------------------------Academy-------------------------------------------------

        $scope.showDataforUpdateAcademy = function (academy) {
            $scope.academytextEspecial = academy.specialty;
            $scope.academytextUniversity = academy.university;
            $scope.academyspeciality = academy.name;
            $scope.academytitle = academy.title;
            $scope.academydateText = academy.dateText;
            $scope.academyEvaluation = academy.evaluation;
            $scope.academyGraduation = academy.graduationSubject;
        }


        $scope.getAcademyReferee = function (idReferee) {
            $scope.showLoadingFunction();
            refereeFactory.actionGetIntFunction(CONSTANT.URL_API_ACADEMY, idReferee)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listAcademy = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                })
        }

        //Agregacion de un nuevo historial de trabajo 
        $scope.addAcademyReferee = function () {
            $scope.showLoadingFunction();
            var information = {
                specialty: $scope.academyselectEspecial.id,
                university: $scope.academyselectUniversity.id,
                name: $scope.academyspeciality,
                title: $scope.academytitle,
                idReferee: $scope.idRefereeContent,
                evaluation: $scope.academyEvaluation,
                graduationSubject: $scope.academyGraduation,
                date: $scope.academydate
            }
            console.log(information);
            refereeFactory.actionPostFunction(CONSTANT.URL_API_ACADEMY, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.showMenuAcademyCreate();
                            $scope.getAcademyReferee($scope.idRefereeContent);
                            $scope.clearDataAcademy();
                        }

                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
        }

        $scope.updateAcademyReferee = function () {
            $scope.showLoadingFunction();
            var information = {
                name: $scope.academyspeciality,
                title: $scope.academytitle,
                evaluation: $scope.academyEvaluation,
                graduationSubject: $scope.academyGraduation,
                date: $scope.academydate
            }
            console.log(information);
            refereeFactory.actionPutFunctionSpecial(CONSTANT.URL_API_ACADEMY, $scope.idRefereeContent, $scope.updateObjectAcademy.idSpecialty, $scope.updateObjectAcademy.idUniversity, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.showMenuAcademyCreate();
                            $scope.getAcademyReferee($scope.idRefereeContent);
                            $scope.clearDataAcademy();
                        }

                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
        }

        $scope.deleteAcademyReferee = function (academy) {
            $scope.showLoadingFunction();
            refereeFactory.actionDeleteFunctionSpecial(CONSTANT.URL_API_ACADEMY, $scope.idRefereeContent, academy.idSpecialty, academy.idUniversity)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.getAcademyReferee($scope.idRefereeContent);
                            $scope.clearDataAcademy();
                        }
                    }, function (error) {
                        console.log(error);
                    });
        }

        //Limpiar data de academia
        $scope.clearDataAcademy = function () {
            $scope.academytextEspecial = "";
            $scope.academytextUniversity = "";
            $scope.academyspeciality = "";
            $scope.academytitle = "";
            $scope.academydateText = "";
            $scope.academyselectEspecial = null;
            $scope.academyselectUniversity = null;
            $scope.academyspeciality = null;
            $scope.academydate = null;
            $scope.academyEvaluation = "";
            $scope.academyGraduation = "";
        }


        //-------------------------------------TypeSpecialty------------------------------------------

        $scope.getTypeEspecialty = function () {
            $scope.showLoadingFunction();
            refereeFactory.actionGetFunction(CONSTANT.URL_API_TYPESPECIALTY)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listTypeEspecialty = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                })
        }


        //-----------------------------------------university-----------------------------------------

        $scope.getUniversity = function () {
            $scope.showLoadingFunction();
            refereeFactory.actionGetFunction(CONSTANT.URL_API_UNIVERSITY)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listUniversity = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                })
        }

        //-----------------------------------------Country-------------------------------------------

        $scope.getCountryReferee = function () {
            refereeFactory.actionGetFunction(CONSTANT.URL_API_COUNTRY)
                .then(function (response) {
                    console.log(response);
                    $scope.listCountry = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                })
        }

        //--------------------------------------------Departament------------------------------------

        $scope.getDepartamentReferee = function () {
            refereeFactory.actionGetIntFunction(CONSTANT.URL_API_DEPARTAMENT, $scope.addressSelectCountry.id)
                .then(function (response) {
                    console.log(response);
                    $scope.listDepartament = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                })
        }

        //---------------------------------------------Municipality----------------------------------

        $scope.getMunicipalityReferee = function () {
            refereeFactory.actionGetIntFunction(CONSTANT.URL_API_MUNICIPALITY, $scope.addressSelectDep.id)
                .then(function (response) {
                    console.log(response);
                    $scope.listMunicipality = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                })
        }


        //-----------------------------------------------Language--------------------------------------

        $scope.getLanguageGeneralReferee = function () {
            refereeFactory.actionGetFunction(CONSTANT.URL_API_LANGUAGE)
                .then(function (response) {
                    console.log(response);
                    $scope.listLanguageGeneral = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                })
        }

        $scope.getLanguageReferee = function (idReferee) {
            $scope.showLoadingFunction();
            refereeFactory.actionGetIntFunction(CONSTANT.URL_API_LANGUAGEREFEREE, idReferee)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listLanguage = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                })
        }

        $scope.addLanguageReferee = function () {
            $scope.showLoadingFunction();
            var information = {
                idReferee: $scope.idRefereeContent,
                idLanguage: $scope.selectLanguageReferee.id
            }
            console.log(information);
            refereeFactory.actionPostFunction(CONSTANT.URL_API_LANGUAGEREFEREE, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.getLanguageReferee($scope.idRefereeContent);
                        }

                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
        }

        $scope.deleteLanguageReferee = function (language) {
            $scope.showLoadingFunction();
            refereeFactory.actionDeleteTwoFunction(CONSTANT.URL_API_LANGUAGEREFEREE, $scope.idRefereeContent, language.idLanguage)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.getLanguageReferee($scope.idRefereeContent);
                        }
                    }, function (error) {
                        console.log(error);
                    });
        }
        

       //---------------------------------------------Saldo congelado ------------------------
        $scope.getBalanceFrozen = function (idReferee) {
            refereeFactory.actionGetIntFunction(CONSTANT.URL_API_REFUNDPAYMENT, idReferee)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listDataFozen = response.data;

                    if ($scope.listDataFozen != null) {
                        if ($scope.listDataFozen.length > 0) {
                            $scope.showForzer = true;
                        } else {
                            $scope.showForzer = false;
                        }
                    } else {
                        $scope.showForzer = false;
                    }

                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            
            

        }


        //-----------------------------------------------Saldo--------------------------------------


        $scope.getBalanceReferee = function (idReferee) {
            $scope.showLoadingFunction();
            refereeFactory.actionGetIntFunction(CONSTANT.URL_API_BALANCE, idReferee)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    
                    $scope.balanceCountReferee = response.data.countReferee;
                    $scope.balanceMountReferee = response.data.referee;
                    $scope.balanceCountTimbre = response.data.countTimbre;
                    $scope.balanceMountTimbre = response.data.timbre;
                    $scope.balanceCountOther = response.data.countOther;
                    $scope.balanceMountOther = response.data.other;
                    $scope.balanceMountMora = response.data.mora;

                    $scope.balanceDateReferee = response.data.dateReferee;
                    $scope.balanceDateTimbre = response.data.dateTimbre;
                    $scope.balanceDatePostumo = response.data.datePostumo;

                    $scope.balanceCountPstumo = response.data.countPostumo;
                    $scope.balanceMountPostumo = response.data.postumo;


                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                })
        }


        //------------------------------------Diploma-------------------------------------------------


        $scope.getDiploma = function () {
            $scope.showLoadingFunction();
            refereeFactory.actionGetIntFunction("/Referre/CreateDiploma", $scope.idRefereeContent)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    if (response.data != "") {
                        $window.open(response.data);
                    }
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                })
        }


        //--------------------------------------------------------------------------------------------
        
        $scope.setAuthorizationType = function (convenio, planilla) {
            isConvenio = convenio;
            isPlanilla = planilla;
        }

        //obtine el listado de los tipos de colegiados
        $scope.getTypeReferee = function () {

            refereeFactory.actionGetGetIntFunction(CONSTANT.URL_API_TYPEREFEREE, isConvenio, isPlanilla)
                .then(function (response) {
                    console.log(response);
                    $scope.listTypeReferee = response.data;
                }, function (error) {
                    console.log(error);
                });

            
        }

        //Actualizamos los datos del colegiado
        $scope.updateReferee = function () {
            $scope.showLoadingFunction();
            var gender = "";
            $scope.statusUpdate = "";
            console.log("----->");
            console.log($scope.enableJu);
            var statusJu = (($scope.enableJu == true) ? 1 : 0);
            var statusPen = (($scope.enablePen == true) ? 1 : 0);

            console.log(statusJu);
            console.log($scope.dateSweReferee);

            var informacion = {

                FechaColegiacion: $scope.dateTextReferee,

                PrimerNombre: $scope.firstNameReferee,
                SegundoNombre: $scope.secoundNameReferee,
                TercerNombre: $scope.thirdNameReferee,
                PrimerApellido: $scope.fistLastNameReferee,
                SegundoApellido: $scope.secoundLastNameReferee,
                CasdaApellido: $scope.marriedNameReferee,

                RegCedula: $scope.noCedulaReferee,
                Registro: $scope.regCedulaReferee,
                CedulaExtendida: $scope.extCedulaReferee,
                Dpi: $scope.noDpiReferee,
                DpiExtendido: $scope.extDpieferee,

                FechaJuramentacion: $scope.dateTextSweReferee,
                EstadoCivil: $scope.statuscivilDefineReferee,
                Sexo: $scope.genderDefineReferee,
                FechaNacimiento: $scope.birthdayTextReferee,
                Nit: $scope.nitReferee,
                Observaciones: $scope.remarkReferee,
                Notificacion: $scope.notifyReferee,
                Estatus:  -1,
                idTipoColegiacion: (($scope.selectTypeReferee == null) ? 0 : $scope.selectTypeReferee.idTypeReferee),
                Salario: $scope.salaryReferee,
                idSede: (($scope.selectHeadquarJuReferee == null) ? 0 : $scope.selectHeadquarJuReferee.id),
                idSedeActual: (($scope.selectHeadquarReferee == null) ? 0 : $scope.selectHeadquarReferee.id),
                fechaFallecimiento: $scope.deathTextReferee,
                esJuramentado: statusJu,
                verTexto: statusPen,
                placeBirth: $scope.placeBirthReferee,
                nacionality: $scope.nacionalityReferee,
                experence: $scope.experenceReferee,
                dateJubilacion: $scope.dateJubilacionTextReferee
            }
            console.log(informacion); 

            refereeFactory.actionPutFunction(CONSTANT.URL_API_REFEREE, $scope.idRefereeContent, informacion)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.statusUpdate = "Actualizado";
                        } else {
                            $scope.mensajeSubEvent = "";
                        }
                        $scope.loadReferee($scope.idReferee);
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

        }

        $scope.changeStatus = function (type) {
            if(type == "1"){
                return "Activo";
            } else if (type == "0") {
                return "Inactivo";
            }
        }


        $scope.changeTypeAddress = function (type) {
            if (type == 1) {
                return "Residencia";
            } else if (type == 2) {
                return "Trabajo";
            } else if (type == 3) {
                return "Notificación";
            } else if (type == 4) {
                return "Correspondencia";
            } else {
                return "No definido";
            }
        }
        
        $scope.changeTypeStateCivil = function (type) {
            if (type == 0) {
                return "NA";
            } else if (type == 1) {
                return "Soltero/a";
            } else if (type == 2) {
                return "Casado/a";
            } else if (type == 3) {
                return "Separado/a";
            } else if (type == 4) {
                return "Viudo/a";
            } else {
                return "No definido";
            }
        }

        //Cambionos la referencia de Y y N del genero
        $scope.changeGender = function (type) {
            if (type == 0) {
                return "Masculino";
            }else if(type == 1){
                return "Femenino";
            } else {
                return "No definido";
            }
        }

        $scope.aproxNext = function (total) {
            return Math.ceil(total);
        }

        //Cambio de pantalla a la fono del colegiado
        $scope.changePhoto = function () {
            window.location.href = '/Referre/Photo/' + $scope.idRefereeContent;
        }

        $scope.changeFirm = function () {
            window.location.href = '/Referre/Firm/' + $scope.idRefereeContent;
        }

        //Cambio de pantalla al listado dedocumentos
        $scope.changeDocument = function () {
            window.location.href = '/Referre/Files/' + "#?id=" + $scope.idRefereeContent;
        }

        $scope.changePay = function () {
            window.location.href = '/Payment/Economic/#?id=' + $scope.idRefereeContent;
        }

        $scope.changeSaldo = function () {
            window.location.href = '/Reports/SaldoColegiado/#?id=' + $scope.idRefereeContent;
        }

        $scope.showLoadingFunction = function () {
            $anchorScroll();
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
        }

        $scope.hideLoadingFunction = function () {
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
        }

        $scope.getFormatDate = function(date){
            return date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
        }


    }])


})();
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Constant;
using webcee.Class.JsonModels.EventAssing;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_EventAssistantController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        public IEnumerable<ResponseAssistentJson> Get(int id)
        {

            List<ResponseAssistentJson> contenReferee = new List<ResponseAssistentJson>();

            var ceSubCol = from col in db.ccee_Colegiado
                           join subcol in db.ccee_SubEventoColegiado on col.Col_NoColegiado equals subcol.pkfk_Colegiado
                           where subcol.pkfk_SubEvento == id && subcol.SEC_Estatus == Constant.state_sub_event_assing
                           select col;

            foreach ( ccee_Colegiado temReferee in ceSubCol )
            {
                contenReferee.Add( new ResponseAssistentJson() {
                    id = temReferee.Col_NoColegiado,
                    name = temReferee.Col_PrimerNombre + " " + temReferee.Col_SegundoNombre + " " + temReferee.Col_TercerNombre,
                    lastname = temReferee.Col_PrimerApellido + " " + temReferee.Col_SegundoApellido
                });
            }

            return contenReferee;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }




    }
}

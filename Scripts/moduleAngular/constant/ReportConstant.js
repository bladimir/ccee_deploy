﻿(function () {
    angular.module('reports')
        .constant('CONSTANT', {
            REGEX_DIGIT: /^\d+$/,
            REGEX_EMAIL: /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/,
            REGEX_NIT: /^[0-9]{6,9}-([0-9]|[a-zA-Z])$/,
            REGEX_FECHA: /^(0?[1-9]|[12][0-9]|3[01])[\/](0?[1-9]|1[012])[\/](19|20)\d{2}$/,
            REGEX_PHONE: /^[0-9]{8}$/,
            REGEX_STATEPHONE: /^[0-9]{3}$/,
            REGEX_REFEREE: /^[0-9]+(((,[0-9]+)*)|(-[0-9]*))?$/,
            FORMAT_DATE: "DD/MM/YYYY",
            NOTIFY_HIDE_CSS: 'emergente1 notify-event-content',
            NOTIFY_SHOW_CSS: 'emergente2 notify-event-content',
            URL_API_GRAPHIC: '/api/Api_Graphics',
            URL_API_LANGUAGE: '/api/Api_Language',
            URL_API_COUNTRY: '/api/Api_Pais',
            URL_API_DEPARTAMENT: '/api/Api_Departament',
            URL_API_MUNICIPALITY: '/api/Api_Municipality',
            URL_API_REFEREE: '/api/Api_Referee',
            URL_API_HEADQUARTERS: '/api/Api_Headquarters',
            URL_API_UNIVERSITY: '/api/Api_University',
            URL_API_TYPEREFEREE: '/api/Api_TypeReferee',
            URL_API_TYPESPECIALTY: '/api/Api_TypeEspecialty',
            URL_API_CASH: '/api/Api_Cash',
            URL_API_CASHIER: '/api/Api_Cashier',
            URL_API_CLOSECASHRANGE: '/api/Api_CloseCashRange',
            URL_GET_TOKEN: '/Users/GetToken',
            URL_API_USER: '/api/Api_User',
            URL_GET_LISTGENERAL: '/api/Api_ListGeneral',
            URL_GET_TEMP_REPORT: '/api/Api_TempReporte',
            URL_GET_CALCULO_ACTUARIO: '/api/Api_CalculoActuario',
            HEADER_CONTENT_TYPE_JSON: 'application/json',
            MENSAJE_SUCCESS_SUB_EVENT: "Sub Evento Guardado"

        });

})();
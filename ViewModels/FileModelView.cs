﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace webcee.ViewModels
{
    public class FileModelView
    {
        public string nameFile { get; set; }
        public string nameFileRequirement { get; set; }

        [Display(Name = "Nombre Documento")]
        public string nameDefine { get; set; }

        public List<SelectListItem> itemss { get; set; }
    }
}
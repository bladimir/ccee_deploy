﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.TimbreSign
{
    public class RequestTimbreSignJson
    {
        public int idIntitution { get; set; }
        public string base64 { get; set; }
        public int colegiado { get; set; }
        public string name { get; set; }
        public List<TimbresSignDocs> timbres { get; set; }
        public List<decimal> valores { get; set; }
    }
}
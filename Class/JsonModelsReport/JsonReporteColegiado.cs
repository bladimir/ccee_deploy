﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonReporteColegiado
    {
        public int Col_NoColegiado { get; set; }
        public string Col_PrimerNombre { get; set; }
        public string Col_SegundoNombre { get; set; }
        public string Col_PrimerApellido { get; set; }
        public string Col_SegundoApellido { get; set; }
        public string Esp_NombreEspecialidad { get; set; }
        public string sexo { get; set; }
        public string estatus { get; set; }
        public string Mai_MailDir { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.JsonModels.ReportReceipt;

namespace webcee.Controllers
{
    public class Api_ReportReceiptController : ApiController
    {
        // GET: api/Api_ReportReceipt
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_ReportReceipt/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_ReportReceipt
        public void Post(RequestReportReceiptJson receipt)
        {
            try
            {
                
            }
            catch (Exception)
            {
                
            }
        }

        // PUT: api/Api_ReportReceipt/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_ReportReceipt/5
        public void Delete(int id)
        {
        }
    }
}

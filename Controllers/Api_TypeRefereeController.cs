﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.TypeReferee;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_TypeRefereeController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_TypeReferee
        public List<ResponseTypeRefereeJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var contentType = from type in db.ccee_TipoColegiado
                                  select new ResponseTypeRefereeJson()
                                  {
                                      idTypeReferee = type.Tco_NoTipoColegiado,
                                      nombre = type.Tco_Descripcion
                                  };
                Autorization.saveOperation(db, autorization, "Obteniendo tipo colegiado");
                return contentType.ToList();

            }catch(Exception)
            {
                return null;
            }
   
        }

        [Route("api/Api_TypeReferee/{isConvenio}/{isPlanilla}")]
        // GET: api/Api_TypeReferee/5
        public List<ResponseTypeRefereeJson> Get(bool isConvenio, bool isPlanilla)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                List<ResponseTypeRefereeJson> contentType = (from type in db.ccee_TipoColegiado
                                  select new ResponseTypeRefereeJson()
                                  {
                                      idTypeReferee = type.Tco_NoTipoColegiado,
                                      nombre = type.Tco_Descripcion
                                  }).ToList();

                if (!isConvenio)
                {
                    ResponseTypeRefereeJson convenio = contentType.Find(p => p.idTypeReferee == 14);
                    contentType.Remove(convenio);
                }

                if (!isPlanilla)
                {
                    ResponseTypeRefereeJson convenio = contentType.Find(p => p.idTypeReferee == 15);
                    contentType.Remove(convenio);
                }

                Autorization.saveOperation(db, autorization, "Obteniendo tipo colegiado");
                return contentType;

            }
            catch (Exception)
            {
                return null;
            }

            
        }

        // POST: api/Api_TypeReferee
        public IHttpActionResult Post(RequestTypeRefereeJson typeReferee)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_TipoColegiado tempTypeReferee = new ccee_TipoColegiado()
                {
                    Tco_Descripcion = typeReferee.description
                };

                db.ccee_TipoColegiado.Add(tempTypeReferee);
                Autorization.saveOperation(db, autorization, "Agregando tipo colegiado");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_TypeReferee/5
        public IHttpActionResult Put(int id, RequestTypeRefereeJson typeReferee)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_TipoColegiado tempTypeReferee = db.ccee_TipoColegiado.Find(id);

                if (tempTypeReferee == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempTypeReferee.Tco_Descripcion = typeReferee.description;

                db.Entry(tempTypeReferee).State = EntityState.Modified;

                Autorization.saveOperation(db, autorization, "Actualizando Universidades");

                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_TypeReferee/5
        public IHttpActionResult Delete(int id)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_TipoColegiado tempTypeReferee = db.ccee_TipoColegiado.Find(id);
                if (tempTypeReferee == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_TipoColegiado.Remove(tempTypeReferee);
                Autorization.saveOperation(db, autorization, "Eliminando tipo colegiado");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

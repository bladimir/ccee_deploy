﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonHistoricoActivo
    {
        public int? mesNumer { get; set; }
        public int? anio { get; set; }
        public string mes
        {
            get
            {
                if (mesNumer > 0)
                {
                    CultureInfo culture;
                    culture = CultureInfo.CreateSpecificCulture("es-GT");
                    return culture.DateTimeFormat.GetMonthName(this.mesNumer.Value);
                }
                return "No declarado";
                
            }
        }
        public int? activoNumber { get; set; }
        public string activo
        {
            get
            {
                return ((activoNumber == 1) ? "Activo": "Inactivo");
            }

        }
        public string recibos { get; set; }
        public decimal? montoTimbres { get; set; }
        public decimal? montoSeguro { get; set; }
        public DateTime fechaActivo {
            get
            {
                return Convert.ToDateTime(mesNumer + "/1/" + anio);
            }
        }
        public int? tipo { get; set; }
        public DateTime? fecha { get; set; }
        public string fechaRecibo {
            get
            {
                return (fecha == null) ? "" : fecha.Value.ToString("dd/MM/yyyy");
            }
        }
    }
}
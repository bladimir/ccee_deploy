﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.NoReferee;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_NoRefereeController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_NoReferee
        public IEnumerable<ResponseNoRefereeJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempNoReferee = from nor in db.ccee_NoColegiado
                                    select new ResponseNoRefereeJson()
                                    {
                                        id = nor.NoC_NoNoColegiado,
                                        name = nor.NoC_Nombre,
                                        address = nor.NoC_Direccion,
                                        nit = nor.NoC_Nit,
                                        isAfiliado = nor.NoC_Afiliado,
                                        movil = nor.NoC_NumTelefono,
                                        remark = nor.NoC_Observacion,
                                        lastname = nor.NoC_Apellido
                                    };
                Autorization.saveOperation(db, autorization, "Obteniendo no colegiados");
                return tempNoReferee;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_NoReferee/5
        public IEnumerable<ResponseNoRefereeJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempNoReferee = from nor in db.ccee_NoColegiado
                                    where nor.NoC_Afiliado == id
                                    select new ResponseNoRefereeJson()
                                    {
                                        id = nor.NoC_NoNoColegiado,
                                        name = nor.NoC_Nombre,
                                        address = nor.NoC_Direccion,
                                        nit = nor.NoC_Nit,
                                        lastname = nor.NoC_Apellido
                                    };
                Autorization.saveOperation(db, autorization, "Obteniendo no colegiados por afiliado");
                return tempNoReferee;
                
            }
            catch (Exception)
            {
                return null;
            }
        }

        [Route("api/Api_NoReferee/{afiliado}/{id}")]
        // GET: api/Api_NoReferee/5
        public ResponseNoRefereeJson Get(int afiliado, int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                int? temp = afiliado;
                if(temp == 0)
                {
                    temp = null;
                }

                var tempNoReferee = (from nor in db.ccee_NoColegiado
                                    where nor.NoC_NoNoColegiado == id
                                    select new ResponseNoRefereeJson()
                                    {
                                        id = nor.NoC_NoNoColegiado,
                                        name = nor.NoC_Nombre,
                                        address = nor.NoC_Direccion,
                                        nit = nor.NoC_Nit,
                                        lastname = nor.NoC_Apellido
                                    }).FirstOrDefault();
                Autorization.saveOperation(db, autorization, "Obteniendo no colegiado");
                return tempNoReferee;

            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_NoReferee
        public IHttpActionResult Post(RequestNoRefereeJson notREferee)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_NoColegiado tempNotReferee = new ccee_NoColegiado()
                {
                    NoC_Nombre = notREferee.name,
                    NoC_Apellido = notREferee.lastname,
                    NoC_Direccion = notREferee.address,
                    NoC_Nit = notREferee.nit,
                    NoC_NumTelefono = notREferee.movil,
                    NoC_Observacion = notREferee.remark,
                    NoC_Afiliado = notREferee.isAfiliado
                };
                db.ccee_NoColegiado.Add(tempNotReferee);
                Autorization.saveOperation(db, autorization, "Agregando no colegiados");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_NoReferee/5
        public IHttpActionResult Put(int id, RequestNoRefereeJson notREferee)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_NoColegiado tempNoReferee = db.ccee_NoColegiado.Find(id);
                if (tempNoReferee == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }
                tempNoReferee.NoC_Nombre = notREferee.name;
                tempNoReferee.NoC_Apellido = notREferee.lastname;
                tempNoReferee.NoC_Direccion = notREferee.address;
                tempNoReferee.NoC_Nit = notREferee.nit;
                tempNoReferee.NoC_NumTelefono = notREferee.movil;
                tempNoReferee.NoC_Observacion = notREferee.remark;
                tempNoReferee.NoC_Afiliado = notREferee.isAfiliado;

                db.Entry(tempNoReferee).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando no colegiado");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);

            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_NoReferee/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_NoColegiado tempNoReferee = db.ccee_NoColegiado.Find(id);
                if (tempNoReferee == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_NoColegiado.Remove(tempNoReferee);
                Autorization.saveOperation(db, autorization, "Eliminando no colegiado");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿(function () {
    angular.module('reports')
        .controller('listReferee', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'reportFactory', function ($scope, $http, CONSTANT, $anchorScroll, reportFactory) {
            $scope.direction;
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquaters;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        reportFactory.setAuthorization(response.data);
                        $scope.initF();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.initF = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_COUNTRY)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listCountry = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });


                reportFactory.actionGetFunction(CONSTANT.URL_API_HEADQUARTERS)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listHeadquaters = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.getDepartament = function () {
                $scope.showLoadingFunction();
                reportFactory.actionGetIntFunction(CONSTANT.URL_API_DEPARTAMENT, $scope.selectCountry.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listDepartament = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getMunicipality = function () {
                $scope.showLoadingFunction();
                reportFactory.actionGetIntFunction(CONSTANT.URL_API_MUNICIPALITY, $scope.selectDepartament.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listMunicipality = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.generateList = function () {
                var textGetReport = "";
                console.log( ($scope.selectHead != null) ? $scope.selectHead.id : "" );
                textGetReport = "referee=" + $scope.inReferee + 
                                "&civil=" + $scope.inCivilState +
                                "&birth=" + $scope.inDateBirth +
                                "&dreferee=" + $scope.inDateReferee +
                                "&active=" + $scope.inActive +
                                "&headq=" + (($scope.selectHead != null)? $scope.selectHead.id : "") +
                                "&munici=" + (($scope.selectMunicipality != null)? $scope.selectMunicipality.id : "")  +
                                "&gender=" + $scope.inGender;
                console.log(textGetReport);
                $scope.direction = "../../Reports/aspxReports/webListreferee.aspx?" + textGetReport;
            }

        }]);

})()
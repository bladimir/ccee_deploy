﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.TypeSpecialty;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_TypeEspecialtyController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();


        // GET: api/Api_TypeEspecialty
        public IEnumerable<ResponseTypeSpecialtyJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempTypeSpecialty = (from typ in db.ccee_TipoEspecialidad
                                        select new ResponseTypeSpecialtyJson()
                                        {
                                            id = typ.TEs_NoTipoEspecialidad,
                                            name = typ.TEs_NombreTipoEsp,
                                            degree = typ.TES_GradoAcademico,
                                            abbreviation = typ.TES_Abreviacion
                                        }).OrderBy(p=>p.name);
                Autorization.saveOperation(db, autorization, "Obteniendo tipo especialidad");
                return tempTypeSpecialty;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_TypeEspecialty/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_TypeEspecialty
        public IHttpActionResult Post(RequestTypeSpecialtyJson typeSpecialty)
        {

            try
            {

                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                int last = db.ccee_TipoEspecialidad
                                .Max(p=>p.TEs_NoTipoEspecialidad);
                last += 1;

                ccee_TipoEspecialidad tempTypeEspecialty = new ccee_TipoEspecialidad()
                {
                    TEs_NoTipoEspecialidad = last,
                    TEs_NombreTipoEsp = typeSpecialty.name,
                    TES_GradoAcademico = typeSpecialty.degree,
                    TES_Abreviacion = typeSpecialty.abbreviation
                };

                db.ccee_TipoEspecialidad.Add(tempTypeEspecialty);
                Autorization.saveOperation(db, autorization, "Agregando tipo especialidad");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        // PUT: api/Api_TypeEspecialty/5
        public IHttpActionResult Put(int id, RequestTypeSpecialtyJson typeSpecialty)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_TipoEspecialidad tempTypeEspecialty = db.ccee_TipoEspecialidad.Find(id);

                if(tempTypeEspecialty == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempTypeEspecialty.TEs_NombreTipoEsp = typeSpecialty.name;
                tempTypeEspecialty.TES_GradoAcademico = typeSpecialty.degree;
                tempTypeEspecialty.TES_Abreviacion = typeSpecialty.abbreviation;

                db.Entry(tempTypeEspecialty).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando tipo especialidad");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_TypeEspecialty/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_TipoEspecialidad tempTypeEspecialty = db.ccee_TipoEspecialidad.Find(id);
                if (tempTypeEspecialty == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_TipoEspecialidad.Remove(tempTypeEspecialty);
                Autorization.saveOperation(db, autorization, "Eliminando tipo especialidad");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

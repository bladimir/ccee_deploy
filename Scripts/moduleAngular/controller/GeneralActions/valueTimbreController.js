﻿(function () {
    angular.module('generals')
        .controller('valueTimbre', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.list;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idObject = 0;
            $scope.regexAmount = CONSTANT.REGEX_AMOUNT;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            

            $scope.get = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_VALUETIMBRE)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.list = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }



            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.get();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }


            $scope.add = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.name,
                    denomination: $scope.denomination,
                }
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_VALUETIMBRE, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.get();
                                $scope.cleanData();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (uni) {
                $scope.idObject = uni.id;
                $scope.name = uni.name;
                $scope.denomination = uni.denomination;
                $anchorScroll();
            }

            $scope.update = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.name,
                    denomination: $scope.denomination,
                }
                generalactionsFactory.actionPutFunction(CONSTANT.URL_API_VALUETIMBRE, $scope.idObject, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.get();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.delete = function (uni) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionDeleteFunction(CONSTANT.URL_API_VALUETIMBRE, uni.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.get();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.name = "";
                $scope.denomination = "";
            }


        }]);

})()
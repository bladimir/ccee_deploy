﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.Family;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_FamilyController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Family
        public IEnumerable<string> Get()
        {
            return null;
        }

        // GET: api/Api_Family/5
        public IEnumerable<ResponseFamilyJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var listFamily = from fam in db.ccee_Familiar
                                 join typ in db.ccee_TipoBeneficiario on fam.fk_TipoBeneficiario equals typ.TBe_NoTipoBeneficiario
                                 where fam.fk_Colegiado == id
                                 select new ResponseFamilyJson()
                                 {
                                     id = fam.Fam_NoFamiliar,
                                     firstName = (fam.Fam_PrimerNombre != null) ? fam.Fam_PrimerNombre : "",
                                     secoundName = (fam.Fam_SegundoNombre != null) ? fam.Fam_SegundoNombre : "",
                                     thirdName = (fam.Fam_TercerNombre != null) ? fam.Fam_TercerNombre : "",
                                     firstLastName = (fam.Fam_PrimerApellido != null) ? fam.Fam_PrimerApellido : "",
                                     secoundLastName = (fam.Fam_SegundoApellido != null) ? fam.Fam_SegundoApellido : "",
                                     marriedName = (fam.Fam_CasdaApellido != null) ? fam.Fam_CasdaApellido : "",
                                     typeBeneficiary = fam.fk_TipoBeneficiario,
                                     nameTypeBeneficiary = typ.TBe_NombreTipo,
                                     percentage = fam.Fam_Porcentaje,
                                     remark = fam.Fam_Observaciones,
                                     dpi = fam.Fam_Dpi
                                 };
                List<ResponseFamilyJson> temFamily = listFamily.ToList();
                Autorization.saveOperation(db, autorization, "Obteniendo beneficiarios");

                return listFamily;
            }
            catch (Exception)
            {
                return null;
            }
            
        }

        // POST: api/Api_Family
        public IHttpActionResult Post(RequestFamilyJson family)
        {
            string autorization = Request.Headers.Authorization.Parameter;
            if (!Autorization.enableAutorization(db, autorization)) return null;

            ccee_Familiar tempFamily = new ccee_Familiar()
            {
                Fam_PrimerNombre = family.firstName,
                Fam_SegundoNombre = family.secoundName,
                Fam_TercerNombre = family.thirdName,
                Fam_PrimerApellido = family.firstLastName,
                Fam_SegundoApellido = family.secoundLastName,
                Fam_CasdaApellido = family.marriedName,
                fk_TipoBeneficiario = ((family.typeBeneficiary == 0) ? 1 : family.typeBeneficiary ),
                fk_Colegiado = family.idReferee,
                Fam_Porcentaje = family.percentage,
                Fam_Observaciones = family.remark,
                Fam_Dpi = family.dpi
            };

            try
            {
                db.ccee_Familiar.Add(tempFamily);
                Autorization.saveOperation(db, autorization, "Agregando beneficiarios");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }


        }

        // PUT: api/Api_Family/5
        public IHttpActionResult Put(int id, RequestFamilyJson family)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Familiar tempFamily = db.ccee_Familiar.Find(id);
                if (tempFamily == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempFamily.Fam_PrimerNombre = family.firstName;
                tempFamily.Fam_SegundoNombre = family.secoundName;
                tempFamily.Fam_TercerNombre = family.thirdName;
                tempFamily.Fam_PrimerApellido = family.firstLastName;
                tempFamily.Fam_SegundoApellido = family.secoundLastName;
                tempFamily.Fam_CasdaApellido = family.marriedName;
                tempFamily.Fam_Porcentaje = family.percentage;
                tempFamily.Fam_Observaciones = family.remark;
                tempFamily.Fam_Dpi = family.dpi;
                tempFamily.fk_TipoBeneficiario = ((family.typeBeneficiary == 0) ? tempFamily.fk_TipoBeneficiario : family.typeBeneficiary);

                db.Entry(tempFamily).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando beneficiarios");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        // DELETE: api/Api_Family/5
        public IHttpActionResult Delete(int id)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Familiar tempFamily = db.ccee_Familiar.Find(id);
                if (tempFamily == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_Familiar.Remove(tempFamily);
                Autorization.saveOperation(db, autorization, "Eliminando beneficiarios");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);

            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.AddDues
{
    public class RequestAddDuesJson
    {
        public int countC { get; set; }
        public int countT { get; set; }
        public int referee { get; set; }
        public DateTime? dateChange { get; set; }
    }
}
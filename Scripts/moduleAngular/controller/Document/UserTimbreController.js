﻿(function () {
    angular.module('document')
        .controller('userTimbre', ['$scope', '$http', '$anchorScroll', 'CONSTANT', '$window', 'documentFactory', function ($scope, $http, $anchorScroll, CONSTANT, $window, documentFactory) {

            $scope.listData;
            $scope.listDataTimbre;
            $scope.showDelete = false;
            $scope.showNotify = 'emergente1 notify-event-content';
            $scope.idUpdate = 0;
            $scope.objectUpdate;
            $scope.dateCon = null;
            $scope.statusFile = false;
            $scope.idReferee = 0;
            $scope.position = 1;
            $scope.limitPage = 10;
            $scope.listTim = []
            $scope.regexAmount = CONSTANT.REGEX_AMOUNT;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = 'emergente2 notify-event-content';
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = 'emergente1 notify-event-content';
            }

            $scope.init = function () {
                $scope.getValue();
                $scope.getListPage($scope.position);
            }

            $scope.getReferee = function (idreferee) {
                $scope.statusUpdate = "";

                documentFactory.actionGetIntFunction(CONSTANT.URL_API_REFEREE, idreferee)
                    .then(function (response) {
                        var data = response.data;
                        if (data != null) {
                            $scope.idRefereeContent = idreferee;
                            $scope.completeNameReferee = data.PrimerNombre + " " + data.SegundoNombre + " "
                                                         + data.TercerNombre + " " + data.PrimerApellido + " " + data.SegundoApellido + " " + data.CasdaApellido;
                            $scope.firstNameReferee = data.PrimerNombre;
                            $scope.secoundNameReferee = data.SegundoNombre;
                            $scope.thirdNameReferee = data.TercerNombre;
                            $scope.fistLastNameReferee = data.PrimerApellido;
                            $scope.secoundLastNameReferee = data.SegundoApellido;
                            $scope.marriedNameReferee = data.CasdaApellido;

                            $scope.nitReferee = data.Nit;
                            $scope.noCedulaReferee = data.RegCedula;
                            $scope.extCedulaReferee = data.CedulaExtendida;
                            $scope.regCedulaReferee = data.Registro;
                            $scope.noDpiReferee = data.Dpi;
                            $scope.extDpieferee = data.DpiExtendido;
                            $scope.dateReferee = data.FechaColegiacionTexto;
                            $scope.birthdayReferee = data.FechaNacimientoTexto;
                            $scope.notifyReferee = data.Notificacion;
                            $scope.statusReferee = data.Estatus;
                            $scope.dateSweReferee = data.FechaJuramentacionTexto;
                            $scope.salaryReferee = data.Salario;
                            $scope.headquarterRefereeJu = data.nameSedeJuramentada;
                            $scope.headquarterReferee = data.nameSedeActual;
                            $scope.placeBirthReferee = data.placeBirth;
                            $scope.nacionalityReferee = data.nacionality;
                            $scope.dateJubilacionReferee = data.fechaJubilacion;
                            $scope.experenceReferee = data.experence;

                            if (data.Sexo == 0) {
                                $scope.genderReferee = "Masculino";
                            } else if (data.Sexo == 1) {
                                $scope.genderReferee = "Femenino";
                            } else {
                                $scope.genderReferee = "No definido";
                            }

                            $scope.statusReferee = data.Estatus;
                            $scope.statuscivilReferee = data.EstadoCivil;
                            $scope.remarkReferee = data.Observaciones;
                            $scope.typeReferee = data.typeReferee;
                            $scope.imageReferee = data.imagen;
                            $scope.deathReferee = data.fechaFallecimiento;
                            $scope.bloodTypeReferee = data.TipoSangre;

                            $scope.statusRefereeCard = data.EstadoTarjetaSeguro;
                            console.log("Estado Tarjeta S:" + data.EstadoTarjetaSeguro);
                            $scope.enableJu = ((data.esJuramentado == 1) ? true : false);
                            $scope.enablePen = ((data.verTexto == 1) ? true : false);

                           
                            $scope.hideLoadingFunction();
                        }
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.getDocs = function (idReferee) {
                console.log('Cargando certificaciones...');
                documentFactory.actionGetIntFunction("/Referre/GetFilesReferee", idReferee)
                    .then(function (response) {
                        if (response.data != null) {
                            console.log("No viene vacio");
                            $scope.listDocumentPrint = response.data;

                        }
                    }, function (error) {
                        console.log(error);
                    });
            }

            $scope.setAuth = function (idReferee) {
                $scope.idReferee = idReferee;
                documentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN_REFEREE)
                .then(function (response) {
                    documentFactory.setAuthorization(response.data);
                    $scope.init();
                    $scope.getDocs(idReferee);
                    $scope.getReferee(idReferee);
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getValue = function () {
                documentFactory.actionGetFunction(CONSTANT.URL_API_VALUETIMBRE)
                    .then(function (response) {
                        $scope.listT = response.data;
                        $scope.getTimbreUser();
                    }, function (error) {
                        console.log(error);
                    });
            } //URL_API_TIMBRE_USER

            $scope.getTimbreUser = function () {
                $scope.showLoadingFunction();
                documentFactory.actionGetIntFunction(CONSTANT.URL_API_TIMBRE_USER, $scope.idReferee)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        $scope.listDataTimbre = response.data;
                        $scope.setListTimbre();
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
            }

            $scope.setListTimbre = function () {
                for (var i = 0; i < $scope.listT.length; i++) {
                    $scope.listT[i].timbres = [];

                    for (var j = 0; j < $scope.listDataTimbre.length; j++) {
                        if ($scope.listT[i].denomination == $scope.listDataTimbre[j].value) {
                            $scope.listT[i].timbres.push($scope.listDataTimbre[j]);
                        }
                    }
                }

            }

            $scope.solicitarTimbre = function () {
                $scope.showLoadingFunction();
                var values = [];
                var timbres = [];

                for (var i = 0; i < $scope.listT.length; i++) {
                    if ($scope.listT[i].selectVal > 0) {
                        values.push($scope.listT[i].denomination);
                        var tempTimbres = $scope.listT[i].timbres;
                        for (var j = 0; j < $scope.listT[i].selectVal; j++) {
                            var timbre = {
                                "No": tempTimbres[j].noTimbre,
                                "page": tempTimbres[j].noPage,
                                "valor": $scope.listT[i].denomination,
                                "idTimbre": tempTimbres[j].idTimbre
                            }
                            timbres.push(timbre);
                        }
                    }
                }

                $scope.idFiletimbre = "";
                var information = {
                    "idIntitution": 1,
                    "base64": (($scope.modeltimbase == null) ? "" : $scope.modeltimbase.base64),
                    "colegiado": $scope.idReferee,
                    "timbres": timbres,
                    "valores": values,
                    "name": (($scope.modeltimbase == null) ? "solo_timbre.pdf" : $scope.modeltimbase.filename)
                }
                documentFactory.actionPostFunction(CONSTANT.URL_API_TIMBRESSIGN, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            console.log(response.data);
                            $scope.getValue();

                            if (response.data != null) {
                                $scope.statusFile = true;
                                $scope.msn = "Cargado exitosamente";
                            } else {
                                $scope.msn = "Problemas de carga, por favor intentar en unos minutos";
                            }

                            /*$scope.idFiletimbre = response.data.id;
                            $scope.statusT = 1;*/
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.solicitarTimbreOnly = function () {
                $scope.showLoadingFunction();
                $scope.msn = "Cargando";
                var values = [];
                var timbres = [];
                if ($scope.modelValue < 0.25) {
                    alert("No se puede realizar un timbrado de valor: Q." + $scope.modelValue)
                } else {
                    if ($scope.Monedero != null && $scope.Monedero >= $scope.modelValue) {

                        var timbre = {
                            "No": 1,
                            "page": 1,
                            "valor": $scope.modelValue,
                            "idTimbre": 2
                        }
                        timbres.push(timbre);
                        values.push($scope.modelValue)

                        $scope.idFiletimbre = "";
                        var information = {
                            "idIntitution": 1,
                            "base64": (($scope.modeltimbase == null) ? "" : $scope.modeltimbase.base64),
                            "colegiado": $scope.idReferee,
                            "urlImage": $scope.URLImagenTimbre,
                            "timbres": timbres,
                            "valores": values,
                            "name": (($scope.modeltimbase == null) ? "solo_timbre.pdf" : $scope.modeltimbase.filename)
                        }
                        console.log(information);
                        console.log(CONSTANT.URL_API_TIMBRESSIGN);
                        documentFactory.actionPostFunction(CONSTANT.URL_API_TIMBRESSIGN, information)
                                .then(function (response) {
                                    $scope.hideLoadingFunction();
                                    console.log(response.data);
                                    $scope.getValue();

                                    if (response.data != null) {
                                        $scope.statusFile = true;
                                        $scope.msn = "Cargado exitosamente";
                                        $scope.getReferee($scope.idReferee);
                                    } else {
                                        $scope.msn = "Problemas de carga, por favor intentar en unos minutos";
                                    }

                                    /*$scope.idFiletimbre = response.data.id;
                                    $scope.statusT = 1;*/
                                }, function (error) {
                                    console.log(error);
                                    $scope.hideLoadingFunction();
                                });

                    } else {
                        alert("No posee los fondos suficientes en el Monedero de Timbres para realizar un timbrado de valor:Q." + $scope.modelValue);
                    }
                }
            }

            $scope.resetFiles = function () {
                $scope.modeltimbase = null;
                $scope.statusFile = false;
                $anchorScroll();
            }




            $scope.showValues = function () {
                console.log($scope.listT);
            }


            $scope.getListPage = function (page) {
                documentFactory.actionGetGetIntFunction(CONSTANT.URL_API_TIMBRESSIGN, $scope.idReferee, page)
                    .then(function (response) {
                        console.log(response);
                        $scope.listTim = response.data;
                        if ($scope.listTim.length < $scope.limitPage) {
                            $scope.nextButton = false;
                        } else {
                            $scope.nextButton = true;
                        }
                    }, function (error) {
                        console.log(error);
                    });
            }

            $scope.next = function () {
                $scope.position++;
                $scope.getListPage($scope.position);
            }

            $scope.back = function () {
                $scope.position--;
                $scope.getListPage($scope.position);
            }

            $scope.donwloadFile = function (item) {
                var arrayName = item.name.split('.');
                //window.location.href = '/Document/DocumentTimbre/' + item.key + "/" + arrayName[0];
                //$window.open('/Document/DocumentTimbre/' + item.key + "/" + arrayName[0], "_blank")
                //$window.open('http://aulavirtualtoofacyl.com:42023/api/timbre/download/' + item.key, "_blank")
                $window.open('http://104.248.233.86:42023/api/timbre/download/' + item.key, "_blank")
            }



        }]);
})()
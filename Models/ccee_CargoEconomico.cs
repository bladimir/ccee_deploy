//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace webcee.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ccee_CargoEconomico
    {
        public int CEc_NoCargoEconomico { get; set; }
        public Nullable<decimal> CEc_Monto { get; set; }
        public int fk_TipoCargoEconomico { get; set; }
        public Nullable<int> fk_Pago { get; set; }
        public Nullable<int> fk_Colegiado { get; set; }
        public Nullable<int> fk_NoColegiado { get; set; }
        public Nullable<System.DateTime> CEc_FechaGeneracion { get; set; }
        public Nullable<int> cEc_ctapadre { get; set; }
        public Nullable<int> cEc_oldcta { get; set; }
        public Nullable<int> CEc_RelacionCargoEco { get; set; }
    
        public virtual ccee_Colegiado ccee_Colegiado { get; set; }
        public virtual ccee_NoColegiado ccee_NoColegiado { get; set; }
        public virtual ccee_TipoCargoEconomico ccee_TipoCargoEconomico { get; set; }
    }
}

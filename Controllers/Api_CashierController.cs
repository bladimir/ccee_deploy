﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.Cashier;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_CashierController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Cashier
        public IEnumerable<ResponseCashierJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempCashier = from caj in db.ccee_Cajero
                                  orderby caj.Cjo_Name ascending
                               select new ResponseCashierJson()
                               {
                                   id = caj.Cjo_NoCajero,
                                   active = caj.Cjo_Activo,
                                   initHour = caj.Cjo_HoraInicia,
                                   finalHour = caj.Cjo_HoraFin,
                                   name = caj.Cjo_Name
                               };
                Autorization.saveOperation(db, autorization, "Obteniendo cajeros");
                return tempCashier;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_Cashier/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_Cashier
        public IHttpActionResult Post(RequestCashierJson cashier)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Cajero tempCashier = new ccee_Cajero()
                {
                    Cjo_Activo = cashier.active,
                    Cjo_HoraFin = cashier.finalHour,
                    Cjo_HoraInicia = cashier.initHour,
                    Cjo_Name = cashier.name
                };

                db.ccee_Cajero.Add(tempCashier);
                Autorization.saveOperation(db, autorization, "Agregando cajero");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_Cashier/5
        public IHttpActionResult Put(int id, RequestCashierJson cashier)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Cajero tempCashier = db.ccee_Cajero.Find(id);

                if (tempCashier == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempCashier.Cjo_Activo = cashier.active;
                tempCashier.Cjo_HoraFin = cashier.finalHour;
                tempCashier.Cjo_HoraInicia = cashier.initHour;
                tempCashier.Cjo_Name = cashier.name;

                db.Entry(tempCashier).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando cajero");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_Cashier/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Cajero tempCashier = db.ccee_Cajero.Find(id);
                if (tempCashier == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_Cajero.Remove(tempCashier);

                Autorization.saveOperation(db, autorization, "Eliminando cajero");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webcee.Class.JsonModels.Bill;

namespace webcee.Class.JsonModels.OpenCloseCash
{
    public class RequestOpenCloseCashJson
    {
        public int date { get; set; }
        public decimal? amountCash { get; set; }
        public decimal? amountDoc { get; set; }
        public decimal? amountTrans { get; set; }
        public decimal? total { get; set; }
        public int? openClose { get; set; }
        public int idCashier { get; set; }
        public int idCash { get; set; }
        public int receipt { get; set; }
        public RequestBillJson bills { get; set; }
    }
}
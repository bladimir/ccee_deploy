﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonDiploma
    {
        public string nombre { get; set; }
        public string titulo { get; set; }
        public string colegiado { get; set; }
        public string fecha { get; set; }
        public string lic { get; set; }
    }
}
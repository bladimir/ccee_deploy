﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_AccountController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Account
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_Account/5
        public string Get(int correo)
        {
            return "value";
        }

        // POST: api/Api_Account
        public string Post([FromBody]JToken value)
        {
            

            if( value["type"] != null)
            {
                if (value["type"].ToString().Equals("question"))
                {
                    if(value["email"] != null)
                    {
                        try
                        {
                            string email = value["email"].ToString();
                            ccee_Usuario user = db.ccee_Usuario.Single(d => d.Usu_Correo == email);
                            return user.Usu_PreguntaClave;
                        }
                        catch
                        {
                            return "400";
                        }
                    }

                }else if (value["type"].ToString().Equals("answer"))
                {
                    if (value["email"] != null && value["answer"] != null)
                    {
                        try
                        {
                            int email = Convert.ToInt32( value["email"].ToString() );
                            string answer = value["answer"].ToString();

                            //ccee_Usuario user = db.ccee_Usuario.Single(d => d.Usu_Correo == email);
                            ccee_Usuario user = db.ccee_Usuario.Single(o => o.Usu_NoUsuario == email);
                            if (user.Usu_RespuestaClave.Equals(answer))
                            {
                                user.Usu_Clave = "0";
                                db.Entry(user).State = EntityState.Modified;
                                try
                                {
                                    db.SaveChanges();
                                    return "200";
                                }
                                catch
                                {
                                    return "400";
                                }
                            }
                            return "400";
                        }
                        catch
                        {
                            return "400";
                        }
                    }
                }
            }
            return "400";
        }


        public IHttpActionResult Put([FromBody]JToken value)
        {
            if ( value["pass"] != null && value["email"] != null )
            {
                string pass = value["pass"].ToString();
                int email = Convert.ToInt32(value["email"].ToString());


                ccee_Usuario user = db.ccee_Usuario.Single(o => o.Usu_NoUsuario == email);
                user.Usu_Clave = GeneralFunction.MD5Encrypt(pass);
                db.Entry(user).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    return Ok(200);
                }
                catch
                {
                    return Ok(400);
                }

            }
            return Ok(400);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

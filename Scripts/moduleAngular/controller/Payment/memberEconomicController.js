﻿(function () {
    angular.module('payment')
        .controller('memberEconomic', ['$scope', '$http', 'CONSTANT', '$window', '$anchorScroll', '$location', '$base64', 'paymentFactory',
            function ($scope, $http, CONSTANT, $window, $anchorScroll, $location, $base64, paymentFactory) {

                var idReferee = -1;
                var typeEconomicVar = 0;
                var typeEconomicVarTP = 0;
                var typeEconomicConvenio = 0;
                var documentCertifyOld = 0;
                $scope.cancelPay;
                $scope.listEconomic;
                $scope.listTypeEconomicOff;
                $scope.listCash;
                $scope.listCashier;
                $scope.listIssuingBank;
                $scope.listPaymentMethod;
                $scope.listPaymentMethodPay = [];
                $scope.listDataTypeDoc;
                $scope.visiblePay = 0;
                $scope.idPay = 1;
                $scope.credit = 0.0;
                $scope.aumontoPaymentMethod;
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
                $scope.success = false;
                $scope.regexAmount = CONSTANT.REGEX_AMOUNT;
                $scope.regexAmountNegative = CONSTANT.REGEX_AMOUNT_NEGATIVE;
                $scope.regexDigit = CONSTANT.REGEX_DIGIT;
                $scope.regexDocument = CONSTANT.REGEX_DOCUMENT;
                $scope.listTimbre = [];
                $scope.text10000 = "";
                $scope.enableTimbre = false;
                $scope.totalPaymentTimbre = 0;
                $scope.amountSelectTimbre = 0;
                $scope.showPayTimbre = false;
                $scope.idPrintReferee = 0;
                $scope.idPrintOthers = 0;
                $scope.butonDocs = true;
                $scope.idPayMethodCash;
                $scope.idPayment = 0;
                $scope.Nodocument = 0;
                $scope.moraAprox = 0;
                $scope.showListDocs = false;
                $scope.showOld = false;
                $scope.sumInTotal = 0;

                $scope.idCahier = 0;
                $scope.itemsTimbre = [];



                $scope.showLoadingFunction = function () {
                    $anchorScroll();
                    $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
                }

                $scope.hideLoadingFunction = function () {
                    $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
                }

                $scope.defineDocument = function (doc) {
                    $scope.Nodocument = doc;
                    console.log($scope.Nodocument);
                }

                $scope.setAuth = function (idRefe, idCashier) {
                    idReferee = idRefe;
                    paymentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN_COLEGIADO)
                        .then(function (response) {
                            paymentFactory.setAuthorization(response.data);
                            $scope.initFunction(idCashier);
                        }, function (error) {
                            $scope.hideLoadingFunction();
                            console.log(error);
                        });
                }

                $scope.initFunction = function (idCashier) {

                    $scope.idCahier = idCashier;
                    $scope.getInfoReferee();
                    $scope.getValueTimbre();

                    // obtener los cargos economicos cargados en esta misma pag.
                    paymentFactory.actionGetIntFunction(CONSTANT.URL_API_ECONOMIC_SHOPPING_CART, idReferee)
                    .then(function (response) {
                        //$scope.amount = $scope.totalPay(response.data);
                        $scope.listEconomic = response.data;
                        console.log($scope.listEconomic);

                        $scope.getTotalForType(1);
                        $scope.getTotalForType(2);
                        $scope.getTotalForType(3);

                        $scope.hideLoadingFunction();
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

                    paymentFactory.actionGetIntFunction(CONSTANT.URL_API_TYPEECONOMIC, CONSTANT.TYPE_ECONOMIC_OFF)
                        .then(function (response) {
                            $scope.listTypeEconomicOff = response.data;
                        }, function (error) {
                            console.log(error);
                        });

                    paymentFactory.actionGetFunction(CONSTANT.URL_API_TYPEDOCUMENTACCOUNTING)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            console.log(response);
                            $scope.listDataTypeDoc = response.data;
                        }, function (error) {
                            $scope.hideLoadingFunction();
                            console.log(error);
                        });

                    paymentFactory.actionGetIntFunction(CONSTANT.URL_API_VARS, CONSTANT.VAR_ECONOMIC_VAR)
                        .then(function (response) {
                            typeEconomicVar = response.data;
                            console.log(typeEconomicVar);
                        }, function (error) {
                            console.log(error);
                        });

                    paymentFactory.actionGetIntFunction(CONSTANT.URL_API_VARS, CONSTANT.VAR_ECONOMIC_VAR_TP)
                        .then(function (response) {
                            typeEconomicVarTP = response.data;
                            console.log(typeEconomicVarTP);
                        }, function (error) {
                            console.log(error);
                        });

                    paymentFactory.actionGetIntFunction(CONSTANT.URL_API_VARS, CONSTANT.VAR_ECONOMIC_CONVENIO)
                        .then(function (response) {
                            typeEconomicConvenio = response.data;
                            console.log(typeEconomicConvenio);
                        }, function (error) {
                            console.log(error);
                        });

                    paymentFactory.actionGetIntFunction(CONSTANT.URL_API_VARS, CONSTANT.VAR_ECONOMIC_DOCUMENT)
                        .then(function (response) {
                            documentCertifyOld = response.data;
                            console.log(documentCertifyOld);
                        }, function (error) {
                            console.log(error);
                        });

                    $scope.getBalanceReferee(idReferee);

                }

                $scope.getValueTimbre = function () {
                    paymentFactory.actionGetIntFunction(CONSTANT.URL_API_VALUETIMBRE, $scope.idCahier)
                    .then(function (response) {
                        console.log(response);
                        $scope.itemsTimbre = response.data;
                    }, function (error) {
                        console.log(error);
                    });
                }

                $scope.getInfoReferee = function () {
                    $scope.statusUpdate = "";

                    paymentFactory.actionGetIntFunction(CONSTANT.URL_API_REFEREE, idReferee)
                        .then(function (response) {
                            var data = response.data;
                            if (data != null) {
                                $scope.completeNameReferee = data.PrimerNombre + " " + data.SegundoNombre + " "
                                                             + data.TercerNombre + " " + data.PrimerApellido + " " + data.SegundoApellido + " " + data.CasdaApellido;
                                $scope.idreferee = idReferee;
                                console.log(data);
                                $scope.hideLoadingFunction();
                            }
                        }, function (error) {
                            console.log(data);
                            $scope.hideLoadingFunction();
                        });
                }





                $scope.totalPay = function (listPay) {
                    var contPay = 0;
                    if (listPay == null) return 0;
                    for (var i = 0; i < listPay.length; i++) {
                        contPay += listPay[i].amount;
                    }
                    return contPay;

                }

                $scope.addIdPay = function (idPay, list) {
                    for (var i = 0; i < list.length; i++) {
                        list[i].idPay = idPay;
                    }
                }

                // obtener los cargos economicos cargados en esta misma pag.
                $scope.getPay = function () {
                    $scope.credit = 0.0;
                    $scope.moraAprox = 0.0;
                    $scope.showLoadingFunction();
                    paymentFactory.actionGetIntFunction(CONSTANT.URL_API_ECONOMIC_SHOPPING_CART, idReferee)
                    .then(function (response) {
                        $scope.listEconomic = response.data;
                        $scope.getTotalForType(1);
                        $scope.getTotalForType(2);
                        $scope.getTotalForType(3);
                        $scope.hideLoadingFunction();
                    }, function (error) {
                        console.log(error);
                        $scope.error = "Problemas de conectividad, intentar de nuevo";
                        $scope.hideLoadingFunction();
                    });
                }

                $scope.getCash = function () {
                    $scope.showLoadingFunction();
                    paymentFactory.actionGetFunction(CONSTANT.URL_API_CASH)
                    .then(function (response) {
                        $scope.listCash = response.data;
                        $scope.hideLoadingFunction();
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
                }

                $scope.getCashier = function () {
                    $scope.showLoadingFunction();
                    paymentFactory.actionGetFunction(CONSTANT.URL_API_CASHIER)
                    .then(function (response) {
                        $scope.listCashier = response.data;
                        $scope.hideLoadingFunction();
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
                }

                $scope.getIssuingBank = function () {
                    $scope.showLoadingFunction();
                    paymentFactory.actionGetFunction(CONSTANT.URL_API_ISSUINGBANK)
                    .then(function (response) {
                        $scope.listIssuingBank = response.data;
                        $scope.hideLoadingFunction();
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
                }

                $scope.getPaymentMethod = function () {
                    $scope.showLoadingFunction();
                    paymentFactory.actionGetIntFunction(CONSTANT.URL_API_PAYMENTMETHOD, 40)
                    .then(function (response) {
                        $scope.listPaymentMethod = response.data;
                        $scope.hideLoadingFunction();
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
                }

                // Agregar TODOS los pagos
                $scope.addPay = function (idCashier, idCash) {
                    $scope.showLoadingFunction();
                    var information = {
                        amount: $scope.amountpay,
                        remake: $scope.remakepay,
                        idCashier: idCashier,
                        idCash: idCash,
                        id: 1,
                        idReferee: idReferee,
                        jsonContent: $scope.listEconomic,
                        jsonContentMethod: $scope.listPaymentMethodPay
                    }
                    console.log(information);
                    paymentFactory.actionPostFunction(CONSTANT.URL_API_PAY_SHOPPING_CART, information)
                            .then(function (response) {
                                $scope.hideLoadingFunction();
                                if (response.data != null) {
                                    console.log(response.data);
                                    //$scope.visiblePay = 0;
                                    //$scope.credit = 0;
                                    //$scope.getPay();
                                    if (response.data > 0) {
                                        $scope.listPaymentMethodPay = [];
                                        console.log("Entro");
                                        $scope.showPayTimbre = true;
                                        $scope.idPayment = response.data;
                                        $scope.error = "";
                                    } else {
                                        if (response.data == 0) {
                                            $scope.error = "Problemas de conectividad, intentar de nuevo";
                                        } else if (response.data == -1) {
                                            $scope.error = "No se ha agregado medios de pago";
                                        } else if (response.data == -2) {
                                            $scope.error = "Número de boleta ya procesada, por favor ingresar nuevo número de boleta";
                                        }
                                    }
                                }

                            }, function (error) {
                                console.log(error);
                                $scope.error = "Problemas de conectividad, intentar de nuevo";
                                $scope.idPay = 0;
                                $scope.hideLoadingFunction();
                            });
                }
                // agregar un cargo economico
                $scope.addEconomic = function () {
                    $scope.showLoadingFunction();
                    var information = {
                        idTypeEconomic: $scope.listTypeOff.id,
                        idReferee: idReferee,
                    }
                    paymentFactory.actionPostFunction(CONSTANT.URL_API_ECONOMIC_SHOPPING_CART, information)
                            .then(function (response) {
                                $scope.hideLoadingFunction();
                                if (response.data != null) {
                                    $scope.listEconomic.push(response.data);
                                    $scope.getTotalForType(3);
                                }
                            }, function (error) {
                                console.log(error);
                                $scope.error = "Problemas de conectividad, intentar de nuevo";
                                $scope.hideLoadingFunction();
                            });
                }

                $scope.addEconomicVariable = function () {
                    $scope.showLoadingFunction();
                    var information = {
                        idTypeEconomic: typeEconomicVar,
                        idReferee: idReferee,
                        amount: $scope.amountEconomicVariable
                    }
                    paymentFactory.actionPostFunction(CONSTANT.URL_API_ECONOMIC_SHOPPING_CART, information)
                            .then(function (response) {
                                $scope.hideLoadingFunction();
                                if (response.data != null) {
                                    $scope.listEconomic.push(response.data);
                                    $scope.getTotalForType(3);
                                }
                            }, function (error) {
                                console.log(error);
                                $scope.error = "Problemas de conectividad, intentar de nuevo";
                                $scope.hideLoadingFunction();
                            });
                }

                $scope.addEconomicVariableTP = function () {
                    $scope.showLoadingFunction();
                    var information = {
                        idTypeEconomic: typeEconomicVarTP,
                        idReferee: idReferee,
                        amount: $scope.amountEconomicVariable
                    }
                    paymentFactory.actionPostFunction(CONSTANT.URL_API_ECONOMIC_SHOPPING_CART, information)
                            .then(function (response) {
                                $scope.hideLoadingFunction();
                                if (response.data != null) {
                                    $scope.listEconomic.push(response.data);
                                    $scope.getTotalForType(3);
                                }
                            }, function (error) {
                                console.log(error);
                                $scope.error = "Problemas de conectividad, intentar de nuevo";
                                $scope.hideLoadingFunction();
                            });
                }

                $scope.addEconomicConvenio = function () {
                    $scope.showLoadingFunction();
                    var information = {
                        idTypeEconomic: typeEconomicConvenio,
                        idReferee: idReferee,
                        amount: $scope.amountEconomicConvenio
                    }
                    paymentFactory.actionPostFunction(CONSTANT.URL_API_ECONOMIC_SHOPPING_CART, information)
                            .then(function (response) {
                                $scope.hideLoadingFunction();
                                if (response.data != null) {
                                    $scope.listEconomic.push(response.data);
                                    $scope.getTotalForType(3);
                                }
                            }, function (error) {
                                console.log(error);
                                $scope.error = "Problemas de conectividad, intentar de nuevo";
                                $scope.hideLoadingFunction();
                            });
                }


                $scope.editEconomic = function (data, amount) {
                    $scope.showLoadingFunction();
                    var information = {
                        amount: amount,
                        idType: data.typeEconomic
                    }
                    console.log(information);
                    console.log(data.id);
                    paymentFactory.actionPutFunction(CONSTANT.URL_API_ECONOMIC_SHOPPING_CART, data.id, information)
                            .then(function (response) {
                                $scope.hideLoadingFunction();
                                console.log(response.data);
                                if (response.data == 200) {
                                    $scope.hideLoadingFunction();
                                    $scope.getPay();
                                }
                            }, function (error) {
                                console.log(error);
                                $scope.error = "Problemas de conectividad, intentar de nuevo";
                                $scope.hideLoadingFunction();
                            });
                }


                $scope.deleteEconomic = function (data, indexD, type) {

                    if (type == 1) {
                        var count = 0;
                        var listReferee = data.ListReferee;
                        var len = listReferee.length;
                        count = len;
                        $scope.showLoadingFunction();
                        for (var i = 0; i < len; i++) {
                            var id = listReferee[i].id;
                            paymentFactory.actionDeleteTwoFunction(CONSTANT.URL_API_ECONOMIC_SHOPPING_CART, id, type)
                                    .then(function (response) {
                                        count--;
                                        if (count <= 0) {
                                            $scope.hideLoadingFunction();
                                            $scope.getPay();
                                        };
                                    }, function (error) {
                                        $scope.error = "Problemas de conectividad, intentar de nuevo";
                                        console.log(error);
                                        count--;
                                        if (count <= 0) {
                                            $scope.hideLoadingFunction();
                                            $scope.getPay();
                                        };
                                    });
                            console.log(id);
                        }
                    } else {
                        console.log(data.id);
                        $scope.showLoadingFunction();
                        paymentFactory.actionDeleteTwoFunction(CONSTANT.URL_API_ECONOMIC_SHOPPING_CART, data.id, type)
                                .then(function (response) {
                                    $scope.hideLoadingFunction();
                                    console.log(response.data);
                                    if (response.data == 200) {
                                        var index = $scope.listEconomic.indexOf(data);
                                        $scope.listEconomic.splice(index, 1);
                                        $scope.getTotalForType(3);
                                    }
                                }, function (error) {
                                    console.log(error);
                                    $scope.error = "Problemas de conectividad, intentar de nuevo";
                                    $scope.hideLoadingFunction();
                                });
                    }
                }


















                $scope.addTimbres = function () {
                    $scope.showLoadingFunction();
                    var information = {
                        name: "Pagos Timbre",
                        idPay: $scope.idPayment,
                        timbres: $scope.listTimbre,
                        referee: idReferee
                    }
                    console.log(information);
                    paymentFactory.actionPostFunction(CONSTANT.URL_API_TIMBRE_SHOPPING_CART, information)
                            .then(function (response) {
                                $scope.hideLoadingFunction();
                                if (response.data == 200) {
                                    $scope.correlative();
                                    $scope.viewCertificateOld();
                                    $scope.visiblePay = 3;
                                }

                            }, function (error) {
                                console.log(error);
                                $scope.error = "Problemas de conectividad, intentar de nuevo";
                                $scope.hideLoadingFunction();
                            });
                }


                $scope.prueba = function (list) {
                    console.log(list);
                }

                $scope.sumCurrent = function (economic) {
                    economic.enable = 1;
                    $scope.credit = $scope.sumTwoDecimal($scope.credit, economic.amount);
                }

                $scope.sumCurrentList = function (idSelect) {
                    var len = $scope.listEconomic.length;

                    for (var i = 0; i < len ; i++) {
                        var idType = $scope.listEconomic[i].typeEconomic;
                        var enable = $scope.listEconomic[i].enable;
                        if (idType == idSelect && enable == 0) {
                            $scope.listEconomic[i].enable = 1;
                            $scope.credit = $scope.sumTwoDecimal($scope.credit, $scope.listEconomic[i].amount);
                        }
                    }
                }

                $scope.subCurrent = function (economic) {
                    economic.enable = 0;
                    console.log(economic);
                    $scope.credit = $scope.subTwoDecimal($scope.credit, economic.amount);
                    if ($scope.credit < 0.01) {
                        $scope.credit = 0.0;
                    }
                }

                $scope.subCurrentList = function (idSelect) {
                    var len = $scope.listEconomic.length;

                    for (var i = 0; i < len ; i++) {
                        var idType = $scope.listEconomic[i].typeEconomic;
                        var enable = $scope.listEconomic[i].enable;
                        if (idType == idSelect && enable == 1) {
                            $scope.listEconomic[i].enable = 0;
                            $scope.credit = $scope.subTwoDecimal($scope.credit, $scope.listEconomic[i].amount);
                        }
                    }
                    if ($scope.credit < 0.01) {
                        $scope.credit = 0.0;
                    }
                }

                $scope.viewtimbre = function () {
                    var found = false;
                    var len = $scope.listEconomic.length;
                    for (var i = 0; i < len; i++) {
                        var enable = $scope.listEconomic[i].enable;
                        var idType = $scope.listEconomic[i].typeEconomic;
                        if (enable == 1 && idType == 2) {
                            found = true;
                            break;
                        }
                    }

                    var sumMora = 0;
                    for (var i = 0; i < len; i++) {
                        var enable = $scope.listEconomic[i].enable;
                        var idType = $scope.listEconomic[i].typeEconomic;
                        var idEco = $scope.listEconomic[i].id;
                        if (enable == 1 && idType == 2 && idEco == 0) {
                            console.log()
                            sumMora = $scope.sumTwoDecimal(sumMora, $scope.listEconomic[i].amount);
                            //sumMora += $scope.listEconomic[i].amount;
                        }
                    }

                    for (var i = len - 1 ; i >= 0; i--) {
                        var enable = $scope.listEconomic[i].enable;
                        var idType = $scope.listEconomic[i].typeEconomic;
                        var idEco = $scope.listEconomic[i].id;
                        console.log(idEco);
                        if (enable == 1 && idType == 2 && idEco == 0) {
                            var tempSumMora = ((100 - ((parseFloat(sumMora) * 100) % 100)) / 100);
                            tempSumMora = parseFloat(tempSumMora.toFixed(2))
                            $scope.listEconomic[i].amount = $scope.sumTwoDecimal($scope.listEconomic[i].amount, tempSumMora);
                            //$scope.listEconomic[i].amount += tempSumMora;
                            $scope.sumInTotal = tempSumMora;
                            $scope.credit = $scope.sumTwoDecimal($scope.credit, tempSumMora);
                            console.log($scope.credit);
                            break;
                        }
                    }



                    if (found == true) {
                        $scope.visiblePay = 1;
                        $scope.showPaymentTimbre();
                    } else {
                        $scope.viewPay();
                    }


                }

                $scope.sumTwoDecimal = function (first, secound) {
                    var Total = parseFloat(first) + parseFloat(secound);
                    return parseFloat(Total.toFixed(2));
                    //return ((parseInt(first * 100) + parseInt(secound * 100)) / 100);
                }

                $scope.subTwoDecimal = function (first, secound) {
                    var Total = parseFloat(first) - parseFloat(secound);
                    return parseFloat(Total.toFixed(2));
                    //return ((parseInt(first * 100) - parseInt(secound * 100)) / 100);
                }


                $scope.updateListTimbre = function (value, timbre) {
                    var len = $scope.listTimbre.length;
                    var exist = false;
                    for (var i = 0; i < len; i++) {
                        if ($scope.listTimbre[i].value == value) {
                            $scope.listTimbre[i].content = timbre;
                            exist = true;
                            break;
                        }
                    }

                    if (!exist) {
                        $scope.listTimbre.push({ value: value, content: timbre });
                    }
                    console.log($scope.listTimbre)
                }

                $scope.getTimbres = function (idCahier, countContent, idValue, vIndex) {
                    if (countContent == null || countContent == "") {
                        $scope.itemsTimbre[vIndex].textDemand = "";
                        $scope.itemsTimbre[vIndex].isDisable = 0;
                        $scope.updateListTimbre($scope.itemsTimbre[vIndex].denomination, null);
                        return null
                    };

                    $scope.itemsTimbre[vIndex].textDemand = "Cargando..";
                    console.log(countContent);

                    paymentFactory.actionGetGetGetIntFunction(CONSTANT.URL_API_TIMBRE_SHOPPING_CART, idCahier, countContent, idValue)
                    .then(function (response) {
                        if (response.status == 200) {
                            //$scope.addTimbreInList(response.data, amount, countContent)
                            //$scope.showTextTimbres(response.data, viewText);
                            $scope.itemsTimbre[vIndex].textDemand = $scope.showTextTimbres(response.data);
                            $scope.itemsTimbre[vIndex].isDisable = ((response.data == null) ? 0 : 1);
                            $scope.updateListTimbre($scope.itemsTimbre[vIndex].denomination, response.data);
                        }
                        console.log(response);
                    }, function (error) {
                        $scope.error = "Problemas de conectividad, intentar de nuevo";
                        console.log(error);
                    });
                }



                $scope.showTextTimbres = function (data) {
                    console.log(data);
                    var text = "";
                    if (data == null) {
                        text = text + "Timbres insuficientes";
                    } else {
                        var len = data.length;
                        console.log(len)
                        for (var i = 0; i < len; i++) {
                            text = text + "T)" + data[i].numberPage + "-" + data[i].numberTimbre + " ";
                        }
                    }
                    console.log(text);
                    return text;
                }

                $scope.verifyPayTimbre = function () {

                    var valueTotal = 0;
                    var sumTimbre = 0;

                    var len = $scope.itemsTimbre.length;
                    for (var j = 0; j < len; j++) {
                        if ($scope.itemsTimbre[j].isDisable == 1) {
                            sumTimbre = sumTimbre + ($scope.itemsTimbre[j].denomination * (($scope.itemsTimbre[j].nameInput != null) ? $scope.itemsTimbre[j].nameInput : 0));
                        }
                    }
                    console.log(sumTimbre)
                    $scope.amountSelectTimbre = sumTimbre;

                    if (($scope.totalPaymentTimbre + $scope.moraAprox) == sumTimbre) {
                        $scope.enableTimbre = true;
                    } else {
                        $scope.enableTimbre = false;
                    }
                }

                $scope.showPaymentTimbre = function () {
                    var len = $scope.listEconomic.length;
                    var valueTotal = 0;

                    for (var i = 0; i < len; i++) {
                        var enable = $scope.listEconomic[i].enable;
                        var idType = $scope.listEconomic[i].typeEconomic;
                        if (enable == 1 && idType == 2) {
                            var listTimbre = $scope.listEconomic[i];
                            var isTimbre = $scope.listEconomic[i].isTimbre;

                            if (isTimbre == 1) {
                                valueTotal = $scope.sumTwoDecimal(valueTotal, $scope.listEconomic[i].amount);
                            }

                            //for (var j = 0; j < listTimbre.length; j++) {
                            //    var isTimbre = listTimbre[j].isTimbre;
                            //    if (isTimbre == 1) {
                            //        valueTotal = valueTotal + listTimbre[j].amount;
                            //    }
                            //}

                        }
                    }
                    $scope.totalPaymentTimbre = valueTotal;
                    $scope.evaluateAproxMora($scope.totalPaymentTimbre);
                    var calulateMoraTotal = $scope.sumTwoDecimal(valueTotal, $scope.moraAprox);
                    $scope.calculateTimbre(calulateMoraTotal);
                }


                $scope.viewPay = function () {
                    //console.log($scope.listTimbre);
                    //console.log('------------------------------------------------p---');
                    //console.log(parseInt($scope.credit * 100));
                    //console.log(parseInt($scope.moraAprox * 100));
                    //console.log((parseInt($scope.credit * 100) + parseInt($scope.moraAprox * 100))/100);
                    //console.log('------------------------------------------------p---');

                    var totalAmountPay = $scope.sumTwoDecimal($scope.credit, $scope.moraAprox);
                    //var div = amount.toString().split('.');
                    //var entero = parseFloat(div[0]);
                    //var decimal = parseFloat("0." + div[1]);
                    //if (decimal < 0.01) {  decimal = 0; }

                    //var totalAmountPay = entero + decimal;

                    $scope.amountpay = totalAmountPay;
                    $scope.aumontoPaymentMethod = totalAmountPay;
                    $scope.selectReservation = { id: 1 };
                    $scope.getIdPayMethodCash();
                    $scope.visiblePay = 2;
                    $scope.addApoxPay();
                    $scope.getCash();
                    $scope.getCashier();
                    $scope.getIssuingBank();
                    $scope.getPaymentMethod();
                }

                $scope.getIdPayMethodCash = function () {
                    paymentFactory.actionGetIntFunction(CONSTANT.URL_API_VARS, "CCEEMedioPago")
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        $scope.idPayMethodCash = response.data;
                        console.log(response.data);
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
                }

                $scope.generatePay = function (idCashier, idCash) {
                    if ($scope.listPaymentMethodPay.length <= 0) {
                        $scope.error = "No se ha agregado medios de pago";
                        return;
                    }
                    $scope.addPay(idCashier, idCash);
                    //$scope.listPaymentMethodPay = [];
                }

                $scope.addListMethodZero = function () {
                    var paymentMethod = {
                        amount: 0,
                        noTransaction: 0,
                        noDocument: 0,
                        reservation: 0,
                        rejection: 0,
                        remarke: "",
                        idBank: 40,
                        idPaymentMethod: $scope.idPayMethodCash
                    };
                    $scope.listPaymentMethodPay.push(paymentMethod);
                    $scope.aumontoPaymentMethod = parseFloat($scope.aumontoPaymentMethod) - parseFloat($scope.amountpayMethod);
                    $scope.amountpayMethod = "";
                    $scope.numberpayTrans = "";
                    $scope.numberpayDoc = "";
                    $scope.alertamountPayment = "";
                }

                $scope.addListMethodCash = function () {
                    if ($scope.amountpayMethod <= $scope.aumontoPaymentMethod && $scope.aumontoPaymentMethod > 0) {
                        var paymentMethod = {
                            amount: parseFloat($scope.amountpayMethod),
                            noTransaction: 0,
                            noDocument: 0,
                            reservation: 0,
                            rejection: 0,
                            remarke: "",
                            idBank: 40,
                            idPaymentMethod: $scope.idPayMethodCash
                        };
                        $scope.listPaymentMethodPay.push(paymentMethod);
                        $scope.aumontoPaymentMethod = $scope.subTwoDecimal($scope.aumontoPaymentMethod, $scope.amountpayMethod);
                        $scope.amountpayMethod = "";
                        $scope.numberpayTrans = "";
                        $scope.numberpayDoc = "";
                        $scope.alertamountPayment = "";
                    } else {
                        $scope.alertamountPayment = ": El monto no es permitido";
                    }
                }

                $scope.addListMethod = function () {
                    var idMethodPay = 0;
                    var len = $scope.listPaymentMethod.length;
                    for (var i = 0; i < len; i++) {
                        var type = $scope.listPaymentMethod[i].typeMethod;
                        if (type == $scope.radioPaymentMethod) {
                            idMethodPay = $scope.listPaymentMethod[i].id;
                            break;
                        }
                    }

                    console.log(idMethodPay);

                    if ($scope.amountpayMethod <= $scope.aumontoPaymentMethod && $scope.aumontoPaymentMethod > 0) {
                        var paymentMethod = {
                            amount: parseFloat($scope.amountpayMethod),
                            noTransaction: $scope.numberpayTrans,
                            noDocument: $scope.numberpayDoc,
                            reservation: 1,
                            rejection: 0,
                            remarke: $scope.remakepayMothod,
                            idBank: $scope.selectIssuingBank.id,
                            idPaymentMethod: idMethodPay
                        };
                        $scope.listPaymentMethodPay.push(paymentMethod);
                        $scope.aumontoPaymentMethod = parseFloat($scope.aumontoPaymentMethod) - parseFloat($scope.amountpayMethod);
                        $scope.amountpayMethod = "";
                        $scope.numberpayTrans = "";
                        $scope.numberpayDoc = "";
                        $scope.alertamountPayment = "";
                        $scope.viewBank = false;
                    } else {
                        $scope.alertamountPayment = ": El monto no es permitido";
                    }
                }



                $scope.deleteListMethod = function (data) {
                    var pos = $scope.listPaymentMethodPay.indexOf(data);
                    console.log(data);
                    $scope.aumontoPaymentMethod = $scope.sumTwoDecimal($scope.aumontoPaymentMethod, data.amount);
                    $scope.listPaymentMethodPay.splice(pos, 1);
                }

                //Area para los recibos
                //// ERRORES ------------------------------------------------
                $scope.addReceipt = function (idType, idCahier) {


                    $scope.showLoadingFunction();
                    var information = {
                        titleName: "",
                        address: "Guatemala",
                        nit: "",
                        remark: $scope.remakepay,
                        idCashier: idCahier,
                        idReferee: idReferee,
                        idPay: $scope.idPayment,
                        idTypeDocumentAccounting: 1,
                        NoDocumentCol: $scope.reciColegiado,
                        NoDocumentOther: $scope.reciOther,
                        listIdDocs: $scope.numberPagesCerti,
                        dateTempCol: $scope.dateRefereeCert,
                        dateTempTim: $scope.dateTimbreCert,
                        isReferee: true
                    }

                    $scope.recovery = $base64.encode(JSON.stringify(information));

                    console.log(information);
                    paymentFactory.actionPostFunction("/Referre/PrintFileReferee", information)
                            .then(function (response) {
                                $scope.hideLoadingFunction();
                                if (response.data != null) {
                                    console.log(response.data);
                                    if (response.data == "1") {
                                        $scope.error = "El numero de recibo ya esta utilizado";
                                    } else {
                                        $scope.listDocumentPrint = response.data;
                                        $scope.showListDocs = true;
                                    }
                                } else {
                                    $scope.error = "Problemas de la información";
                                }

                            }, function (error) {
                                console.log(error);
                                $scope.error = "Problemas de conectividad, intentar de nuevo";
                                $scope.hideLoadingFunction();
                            });
                }
                //// ERRORES ANTES ------------------------------------------------
                $scope.correlative = function () {
                    console.log($scope.Nodocument);
                    var len = $scope.listEconomic.length;
                    var enableReColegiado = true;
                    var enableReOther = false;
                    //for (var i = 0; i < len ; i++) {
                    //    var enable = $scope.listEconomic[i].enable;
                    //    var idType = $scope.listEconomic[i].typeEconomic;
                    //    if (enable == 1 && (idType == 2 || idType == 1 || idType == 3)) {
                    //        enableReColegiado = true;
                    //    }

                    //    if (enable == 1 && idType == 3) {
                    //        enableReOther = true;
                    //    }
                    //}


                    if (enableReColegiado) {
                        $scope.Nodocument += 1;
                        $scope.reciColegiado = $scope.Nodocument;
                    } else {
                        $scope.reciColegiado = 0;
                    }



                }

                $scope.viewCertificateOld = function () {
                    var len = $scope.listEconomic.length;
                    var enableReColegiado = true;
                    var enableReOther = false;
                    console.log(len);
                    for (var i = 0; i < len ; i++) {
                        var enable = $scope.listEconomic[i].enable;
                        var idType = $scope.listEconomic[i].idTypeEconomic;
                        console.log(enable);
                        console.log(idType);
                        console.log(documentCertifyOld);
                        if (enable == 1 && idType == documentCertifyOld) {
                            $scope.showOld = true;
                        }
                        console.log($scope.showOld);
                    }
                }


                $scope.getTotalForType = function (idSelect) {
                    var len = $scope.listEconomic.length;
                    var total = 0;

                    for (var i = 0; i < len ; i++) {
                        var idType = $scope.listEconomic[i].typeEconomic;
                        if (idType == idSelect) {
                            total += $scope.listEconomic[i].amount;
                        }
                    }
                    if (idSelect == 1) {
                        $scope.totalReferee = total;
                    } else if (idSelect == 2) {
                        $scope.totalTimbre = total;
                    } else if (idSelect == 3) {
                        $scope.totalOther = total;
                    }
                }

                $scope.getDues = function () {
                    $scope.showLoadingFunction();
                    paymentFactory.actionGetGetGetIntFunction(CONSTANT.URL_API_ADDDUES_SHOPPING_CART, $scope.duesCol, $scope.duesTim, idReferee)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data != null) {
                            $scope.duesTextCol = response.data.totalC;
                            $scope.duesTextTim = response.data.totalT;
                        }
                        console.log(response.data);
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
                }

                $scope.addDues = function () {
                    $scope.showLoadingFunction();
                    var information = {
                        countC: $scope.duesCol,
                        countT: $scope.duesTim,
                        referee: idReferee,
                        dateChange: $scope.dateDues
                    }
                    console.log(information);
                    paymentFactory.actionPostFunction(CONSTANT.URL_API_ADDDUES_SHOPPING_CART, information)
                            .then(function (response) {
                                $scope.hideLoadingFunction();
                                console.log(response.data);
                                if (response.data == 200) {
                                    $scope.dateDues = null;
                                    $scope.getPay();
                                    $scope.viewDues = false;
                                }

                            }, function (error) {
                                console.log(error);
                                $scope.error = "Problemas de conectividad, intentar de nuevo";
                                $scope.hideLoadingFunction();
                            });
                }

                $scope.evaluateAproxMora = function (amount) {


                    var div = amount.toString().split('.');
                    var entero = parseFloat(div[0]);
                    var decimal = parseFloat("0." + div[1]);
                    var aproxi = 0;
                    var valor = 0.25;

                    var multi = Math.ceil(decimal / valor);
                    var aproxi = multi * valor;

                    if (decimal < 0.01) {
                        decimal = 0;
                    }

                    $scope.moraAprox = $scope.subTwoDecimal(aproxi, decimal);

                    return amount;
                }

                $scope.addApoxPay = function () {
                    var len = $scope.listEconomic.length;

                    for (var i = 0; i < len ; i++) {
                        var enable = $scope.listEconomic[i].enable;
                        var idType = $scope.listEconomic[i].typeEconomic;

                        if (idType == 4) {

                            if ($scope.moraAprox > 0) {
                                $scope.listEconomic[i].enable = 1;
                                $scope.listEconomic[i].amount = $scope.moraAprox;
                            } else {
                                $scope.listEconomic[i].enable = 0;
                                $scope.listEconomic[i].amount = $scope.moraAprox;
                            }

                        }
                    }
                    console.log($scope.listEconomic);

                }

                $scope.calculateTimbre = function (valueAmount) {
                    var len = $scope.itemsTimbre.length;
                    var total = valueAmount;
                    for (var i = len - 1 ; i >= 0; i--) {
                        var valueTimbre = $scope.itemsTimbre[i].denomination;
                        var countTimbre = 0;
                        for (var j = 0; total >= valueTimbre ; j++) {
                            countTimbre++;
                            total = total - valueTimbre;
                        }
                        console.log(countTimbre + " " + valueTimbre);
                        if (countTimbre > 0) {
                            $scope.itemsTimbre[i].nameInput = countTimbre;
                            $scope.getTimbres($scope.idCahier, countTimbre, $scope.itemsTimbre[i].id, i)
                        }
                    }
                }
                /*
                $scope.validateVoucher = function () {
                    if ($scope.selectIssuingBank == null) return;

                    $scope.showLoadingFunction();
                    paymentFactory.actionGetGetIntFunction(CONSTANT.URL_GET_VOUCHER, $scope.numberpayTrans, $scope.selectIssuingBank.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data != null) {
                            $scope.textValidVoucher = response.data.status;
                        }
                        console.log(response.data);
                    }, function (error) {
                        console.log(error);

                        $scope.hideLoadingFunction();
                    });
                }
                */

                $scope.clearMethodPay = function () {
                    $scope.numberpayTrans = "";
                    $scope.numberpayDoc = "";
                    $scope.remakepayMothod = "";
                    $scope.selectReservation = "";
                    $scope.textValidVoucher = "";
                }
                
                $scope.getBalanceReferee = function (idRef) {
                    console.log(idRef);
                    console.log("------------");
                    $scope.showLoadingFunction();
                    paymentFactory.actionGetIntFunction(CONSTANT.URL_API_BALANCE, idRef)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            console.log(response);


                            $scope.balanceDateReferee = response.data.dateReferee;
                            $scope.balanceDateTimbre = response.data.dateTimbre;
                            $scope.balanceDatePostumo = response.data.datePostumo;



                        }, function (error) {
                            $scope.hideLoadingFunction();
                            console.log(error);
                        })
                }
                

                $scope.printReferee = function () {
                    $window.open('/Payment/Print/' + $scope.idPrintReferee);
                }

                $scope.printOther = function () {
                    $window.open('/Payment/Print/' + $scope.idPrintOthers);
                }

                $scope.chengeTimbre = function () {
                    window.location.href = '/GeneralActions/TimbresOfDemand';
                }

                /*
                $scope.reloadPay = function () {
                    window.location.href = '/Referre/Index';
                }

                $scope.changeToDocs = function () {
                    window.location.href = '/GeneralActions/ListedDocuments/#?id=' + $scope.idPayment;
                }
                */
                $scope.changePay = function () {
                    $scope.idCahier
                }

                $scope.changeType = function (state) {
                    if (state == 0) {
                        return "Efectivo";
                    } else if (state == 1) {
                        return "Cheque";
                    } else if (state == 2) {
                        return "Transferencia";
                    } else if (state == 3) {
                        return "Tarjeta de credito";
                    } else if (state == 4) {
                        return "Tarjeta de debito";
                    } else if (state == 5) {
                        return "Lote institucional";
                    } else {
                        return "Indefinido";
                    }
                }

                $scope.changeReservation = function (state) {
                    if (state == 0) {
                        return "Sin reserva";
                    } else if (state == 1) {
                        return "En reserva";
                    } else if (state == 2) {
                        return "Rechazado";
                    } else {
                        return "No definido"
                    }
                }

                $scope.changeRejection = function (state) {
                    if (state == 0) {
                        return "No";
                    } else if (state == 1) {
                        return "Si";
                    } else {
                        return "No definido"
                    }
                }

            }]);
})()
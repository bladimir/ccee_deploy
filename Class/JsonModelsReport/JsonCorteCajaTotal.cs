﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonCorteCajaTotal
    {
        public decimal? tempTotal { get; set; }
        public decimal? total {
            get
            {
                return (tempTotal == null)? 0m : Math.Round(tempTotal.Value, 2);
            }
            set { tempTotal = value; }
        }
    }
}
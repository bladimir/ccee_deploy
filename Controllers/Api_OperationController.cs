﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.Operation;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_OperationController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Operation
        public IEnumerable<ResponseOperationJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                var tempOperation = from ope in db.ccee_Operacion
                                    select new ResponseOperationJson()
                                    {
                                        id = ope.Ope_NoOperacion,
                                        name = ope.Ope_Nombre
                                    };
                Autorization.saveOperation(db, autorization, "Obteniendo operaciones");
                return tempOperation;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_Operation/5
        public string Get(int id)
        {
            return "";
        }

        // POST: api/Api_Operation
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_Operation/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_Operation/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

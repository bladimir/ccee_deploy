﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.RefereeReceipt;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_RefereeReceiptController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_RefereeReceipt
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_RefereeReceipt/5
        public List<ResponseRefereeReceiptJson> Get(int id)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                List<ResponseRefereeReceiptJson> receipt = new List<ResponseRefereeReceiptJson>();

                var tempC = from car in db.ccee_CargoEconomico_colegiado
                            join doc in db.ccee_DocContable on car.fk_DocContable equals doc.DCo_NoDocContable
                            join pay in db.ccee_Pago on car.fk_Pago equals pay.Pag_NoPago
                            join med in db.ccee_MedioPago_Pago_C on pay.Pag_NoPago equals med.fk_Pago
                            where car.fk_Colegiado == id
                            select new ResponseRefereeReceiptJson()
                            {
                                id = doc.DCo_NoDocContable,
                                dateCreate = doc.DCo_FechaHora,
                                NoReceipt = doc.DCo_NoRecibo,
                                ToName = doc.DCo_ANombre,
                                reject = med.MPP_Rechazado,
                                delete = doc.DCo_Eliminado
                            };

                var tempT = from car in db.ccee_CargoEconomico_timbre
                            join doc in db.ccee_DocContable on car.fk_DocContable equals doc.DCo_NoDocContable
                            join pay in db.ccee_Pago on car.fk_Pago equals pay.Pag_NoPago
                            join med in db.ccee_MedioPago_Pago_C on pay.Pag_NoPago equals med.fk_Pago
                            where car.fk_Colegiado == id
                            select new ResponseRefereeReceiptJson()
                            {
                                id = doc.DCo_NoDocContable,
                                dateCreate = doc.DCo_FechaHora,
                                NoReceipt = doc.DCo_NoRecibo,
                                ToName = doc.DCo_ANombre,
                                reject = med.MPP_Rechazado,
                                delete = doc.DCo_Eliminado
                            };

                var tempO = from car in db.ccee_CargoEconomico_otros
                            join doc in db.ccee_DocContable on car.fk_DocContable equals doc.DCo_NoDocContable
                            join pay in db.ccee_Pago on car.fk_Pago equals pay.Pag_NoPago
                            join med in db.ccee_MedioPago_Pago_C on pay.Pag_NoPago equals med.fk_Pago
                            where car.fk_Colegiado == id
                            select new ResponseRefereeReceiptJson()
                            {
                                id = doc.DCo_NoDocContable,
                                dateCreate = doc.DCo_FechaHora,
                                NoReceipt = doc.DCo_NoRecibo,
                                ToName = doc.DCo_ANombre,
                                reject = med.MPP_Rechazado,
                                delete = doc.DCo_Eliminado
                            };
                receipt.AddRange(tempC);
                receipt.AddRange(tempT);
                receipt.AddRange(tempO);

                receipt = receipt
                    .GroupBy(o => new
                    {
                        Id = o.id,
                        DateCreate = o.dateCreate,
                        NoReceipt = o.NoReceipt,
                        ToName = o.ToName
                    })
                    .Select(g => new ResponseRefereeReceiptJson
                    {
                        id = g.Key.Id,
                        dateCreate = g.Key.DateCreate,
                        NoReceipt = g.Key.NoReceipt,
                        ToName = g.Key.ToName,
                        url = Constant.url_dir_print_doc + g.Key.Id,
                        delete = g.Max(p=>p.delete),
                        reject = g.Max(q => q.reject)
                    })
                    .OrderByDescending(p=>p.dateCreate)
                    .ToList();

                Autorization.saveOperation(db, autorization, "Obteniendo recibos por colegiado");
                return receipt;




            }
            catch (Exception)
            {
                return null;
            }
            
        }

        [Route("api/Api_RefereeReceipt/{idReferee}/{idReceipt}")]
        public List<ResponseRefereeReceptEconomicJson> Get(int idReferee, int idReceipt)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                List<ResponseRefereeReceptEconomicJson> list =
                    new List<ResponseRefereeReceptEconomicJson>();

                var tempC = (from car in db.ccee_CargoEconomico_colegiado
                            join tip in db.ccee_TipoCargoEconomico on car.fk_TipoCargoEconomico equals tip.TCE_NoTipoCargoEconomico
                            where car.fk_DocContable == idReceipt && car.fk_Colegiado == idReferee
                            select new ResponseRefereeReceptEconomicJson()
                            {
                                economic = tip.TCE_Descripcion,
                                amount = car.CEcC_Monto,
                                date = car.CEcC_FechaGeneracion
                            }).ToList();
                var tempT = (from car in db.ccee_CargoEconomico_timbre
                            join tip in db.ccee_TipoCargoEconomico on car.fk_TipoCargoEconomico equals tip.TCE_NoTipoCargoEconomico
                            where car.fk_DocContable == idReceipt && car.fk_Colegiado == idReferee
                             select new ResponseRefereeReceptEconomicJson()
                            {
                                economic = tip.TCE_Descripcion,
                                amount = car.CEcT_Monto,
                                date = car.CEcT_FechaGeneracion
                            }).ToList();
                var tempO = (from car in db.ccee_CargoEconomico_otros
                            join tip in db.ccee_TipoCargoEconomico on car.fk_TipoCargoEconomico equals tip.TCE_NoTipoCargoEconomico
                            where car.fk_DocContable == idReceipt && car.fk_Colegiado == idReferee
                             select new ResponseRefereeReceptEconomicJson()
                            {
                                economic = tip.TCE_Descripcion,
                                amount = car.CEcO_Monto,
                                date = car.CEcO_FechaGeneracion
                            }).ToList();

                list.AddRange(unionList(tempC, -2, "Cuota Colegiado"));
                list.AddRange(unionList(tempT, -1, "Cuota Timbre"));
                list.AddRange(tempO);
                Autorization.saveOperation(db, autorization, "Obteniendo cargos por recibo y cole.");
                return list;


            }
            catch (Exception)
            {
                return null;
            }
            
        }


        // POST: api/Api_RefereeReceipt
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_RefereeReceipt/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_RefereeReceipt/5
        public void Delete(int id)
        {
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public List<ResponseRefereeReceptEconomicJson> unionList(List<ResponseRefereeReceptEconomicJson> listEco, int typeEco, string title)
        {
            if (listEco.Count <= 0) return listEco;

            var model = listEco
                .GroupBy(o => new
                {
                    Month = o.date.Value.Month,
                    Year = o.date.Value.Year
                })
                .Select(g => new ResponseRefereeReceptEconomicJson
                {
                    month = g.Key.Month,
                    year = g.Key.Year,
                    economic = title,
                    date = g.Select(p=>p.date).FirstOrDefault(),
                    amount = g.Sum(p => p.amount)
                })
                .OrderBy(p=>p.month)
                .ThenBy(q=>q.year)
                .ToList();

            return model;
        }

    }
}

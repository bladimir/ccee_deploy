﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Phone
{
    public class ResponsePhoneJson
    {

        public int id { get; set; }
        public string number { get; set; }
        public int? state { get; set; }
        public int? idfamily { get; set; }
        public int? type { get; set; }
    }
}
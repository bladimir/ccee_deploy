﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class;
using webcee.Class.JsonMolelsWS;
using webcee.Class.JsonMolelsWS.SaldoColegiado;
using webcee.Models;

namespace webcee.Controllers
{
    public class WS_SaldoColegiadoController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/WS_SaldoColegiado
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/WS_SaldoColegiado/5
        public ResponseSaldoColegiadoJson Get(int id)
        {
            decimal montoColegiado = 0.0m;
            decimal montoTimbre = 0.0m;
            decimal montoPostumo = 0.0m;
            decimal montoMora = 0.0m;
            Nullable<DateTime> ultimoTimbre = null;
            Nullable<DateTime> ultimoColegiado = null;
            int cuotasColegiado = 0;
            int cuotasTimbre = 0;

            var tempCuotatimbreTotal = from cet in db.ccee_CargoEconomico_timbre
                                       where cet.fk_Colegiado == id
                                       select cet;
            var tempCuotaColegiadoTotal = from col in db.ccee_CargoEconomico_colegiado
                                          where col.fk_Colegiado == id
                                          select col;

            List<ccee_CargoEconomico_timbre> listCuotaTimbreTotal = tempCuotatimbreTotal.ToList();
            listCuotaTimbreTotal = listCuotaTimbreTotal.OrderByDescending(o => o.CEcT_NoCargoEconomico).ToList();

            List<ccee_CargoEconomico_colegiado> listCuotaColegiadoTotal = tempCuotaColegiadoTotal.ToList();
            listCuotaColegiadoTotal = listCuotaColegiadoTotal.OrderByDescending(o => o.CEcC_NoCargoEconomico).ToList();

            List<ccee_CargoEconomico_timbre> listCuotaTimbreNull = listCuotaTimbreTotal.Where(p => p.fk_Pago == null).ToList();
            listCuotaTimbreNull = listCuotaTimbreNull.OrderByDescending(o => o.CEcT_FechaGeneracion).ToList();

            List<ModelAnioMesJson> tempListAnioMes = obtenerCantidadCargosTimbre(listCuotaTimbreNull).ToList();
            cuotasTimbre =  tempListAnioMes.Count();

            for (int i = 0; i < tempListAnioMes.Count(); i++)
            {
                ModelAnioMesJson tempAnioMes = tempListAnioMes[i];
                int initList = i * tempAnioMes.Total;
                for (int j = 0; j < tempAnioMes.Total; j++)
                {
                    ccee_CargoEconomico_timbre tempEcoCol = listCuotaTimbreNull[initList + j];
                    switch (tempEcoCol.fk_TipoCargoEconomico)
                    {
                        case 4:
                            montoTimbre += tempEcoCol.CEcT_Monto.Value;
                            break;
                        case 5:
                            montoPostumo += tempEcoCol.CEcT_Monto.Value;
                            break;
                        case 120:
                            montoMora += tempEcoCol.CEcT_Monto.Value;
                            break;
                        default:
                            break;
                    }

                }
            }

            
            List<ccee_CargoEconomico_colegiado> listCuotaColegiadoNull = listCuotaColegiadoTotal.Where(p => p.fk_Pago == null).ToList();

            listCuotaColegiadoNull = listCuotaColegiadoNull.OrderByDescending(o => o.CEcC_FechaGeneracion).ToList();
            tempListAnioMes = obtenerCantidadCargosColegiado(listCuotaColegiadoNull).ToList();
            cuotasColegiado = tempListAnioMes.Count();

            for (int i = 0; i < tempListAnioMes.Count(); i++)
            {
                ModelAnioMesJson tempAnioMes = tempListAnioMes[i];
                int initList = i * tempAnioMes.Total;
                for (int j = 0; j < tempAnioMes.Total; j++)
                {
                    ccee_CargoEconomico_colegiado tempEcoCol = listCuotaColegiadoNull[initList + j];
                    montoColegiado += tempEcoCol.CEcC_Monto.Value;

                }
            }

            ultimoTimbre = (from listTim in listCuotaTimbreTotal
                            where listTim.fk_Pago != null
                            orderby listTim.CEcT_FechaGeneracion descending
                            select listTim.CEcT_FechaGeneracion).FirstOrDefault();

            ultimoColegiado = (from listCol in listCuotaColegiadoTotal
                               where listCol.fk_Pago != null
                               orderby listCol.CEcC_FechaGeneracion descending
                               select listCol.CEcC_FechaGeneracion).FirstOrDefault();

            ccee_Colegiado tempColegiado = (from col in db.ccee_Colegiado
                                            where col.Col_NoColegiado == id
                                            select col).Single();

            decimal montoTotal = montoColegiado + montoTimbre + montoPostumo + montoMora;
            return new ResponseSaldoColegiadoJson()
            {
                noCuotasCol = cuotasColegiado,
                noCuotasTim = cuotasTimbre,
                saldoColegiado = montoColegiado,
                saldoTimbre = montoTimbre,
                postumo = montoPostumo,
                mora = montoMora,
                codigo = 0,
                nombreCompleto = tempColegiado.Col_PrimerNombre + " "
                                + tempColegiado.Col_SegundoNombre + " "
                                + tempColegiado.Col_PrimerApellido + " "
                                + tempColegiado.Col_SegundoApellido,
                estadoColegiado = ((tempColegiado.Col_Estatus == null) ? 0 : tempColegiado.Col_Estatus.Value),
                fechaUltimoPagoTim = GeneralFunction.DateOfStringWS(ultimoTimbre),
                fechaUltimoPagoCol = GeneralFunction.DateOfStringWS(ultimoColegiado),
                total = montoTotal
            };
            
        }

        [Route("api/WS_SaldoColegiado/{colegiado}/{cuotaTimbre}/{cuotaCol}")]
        public ResponseSaldoColegiadoJson Get(int colegiado, int cuotaTimbre, int cuotaCol)
        {
            decimal montoColegiado = 0.0m;
            decimal montoTimbre = 0.0m;
            decimal montoPostumo = 0.0m;
            decimal montoMora = 0.0m;
            Nullable<DateTime> ultimoTimbre = null;
            Nullable<DateTime> ultimoColegiado = null;

            var tempCuotatimbreTotal = from cet in db.ccee_CargoEconomico_timbre
                                  where cet.fk_Colegiado == colegiado
                                  select cet;
            var tempCuotaColegiadoTotal = from col in db.ccee_CargoEconomico_colegiado
                                          where col.fk_Colegiado == colegiado
                                          select col;


            List < ccee_CargoEconomico_timbre > listCuotaTimbreTotal = tempCuotatimbreTotal.ToList();
            listCuotaTimbreTotal = listCuotaTimbreTotal.OrderByDescending(o => o.CEcT_NoCargoEconomico).ToList();

            List<ccee_CargoEconomico_colegiado> listCuotaColegiadoTotal = tempCuotaColegiadoTotal.ToList();
            listCuotaColegiadoTotal = listCuotaColegiadoTotal.OrderByDescending(o => o.CEcC_NoCargoEconomico).ToList();



            List<ccee_CargoEconomico_timbre> listCuotaTimbreNull = listCuotaTimbreTotal.Where(o => o.fk_Pago == null)
                                                                                        .OrderBy(o => o.CEcT_FechaGeneracion)
                                                                                        .ToList();
            
            List<ModelAnioMesJson> tempListAnioMes = obtenerCantidadCargosTimbre(listCuotaTimbreNull).ToList();
            if (tempListAnioMes.Count() < cuotaTimbre)
            {
                int diferencia = cuotaTimbre - tempListAnioMes.Count();
                decimal montoTimbreGen = generarMontoTimbre(colegiado);
                decimal? valorUltimoPostumo = (from tce in db.ccee_TipoCargoEconomico
                                               where tce.TCE_NoTipoCargoEconomico == 5
                                               select tce.TCE_Valor).Single();

                for (int i = 0; i < diferencia; i++)
                {
                    montoTimbre += montoTimbreGen;
                    montoPostumo += valorUltimoPostumo.Value;
                }

            }

            //listCuotaTimbreTotal = listCuotaTimbreTotal.Where(p => p.fk_Pago == null)
            //                                            .OrderBy(o => o.CEcT_FechaGeneracion).ToList();
            //listCuotaTimbreTotal = listCuotaTimbreTotal.OrderByDescending(o => o.CEcT_FechaGeneracion).ToList();

            if (cuotaTimbre > 0 && listCuotaTimbreNull.Count > 0)
            {
                for (int i = 0; i < tempListAnioMes.Count(); i++)
                {
                    ModelAnioMesJson tempAnioMes = tempListAnioMes[i];
                    int initList = i * tempAnioMes.Total;
                    for (int j = 0; j < tempAnioMes.Total; j++)
                    {
                        ccee_CargoEconomico_timbre tempEcoCol = listCuotaTimbreNull[initList + j];
                        switch (tempEcoCol.fk_TipoCargoEconomico)
                        {
                            case 4:
                                montoTimbre += tempEcoCol.CEcT_Monto.Value;
                                break;
                            case 5:
                                montoPostumo += tempEcoCol.CEcT_Monto.Value;
                                break;
                            case 120:
                                montoMora += tempEcoCol.CEcT_Monto.Value;
                                break;
                            default:
                                break;
                        }

                    }
                    if (i == cuotaTimbre - 1) break;
                }
            }

            List<ccee_CargoEconomico_colegiado> listCuotaColegiadoNull = listCuotaColegiadoTotal.Where(o => o.fk_Pago == null)
                                                                                        .OrderBy(o => o.CEcC_FechaGeneracion)
                                                                                        .ToList();


            //Datos para colegiado
            tempListAnioMes = obtenerCantidadCargosColegiado(listCuotaColegiadoNull).ToList();

            if(tempListAnioMes.Count() < cuotaCol)
            {
                int diferencia = cuotaCol - tempListAnioMes.Count();
                decimal montoGeneralcol = generarMontoColegiado().Select(o=>o.TCE_Valor.Value).Sum();
                for (int i = 0; i < diferencia; i++)
                {
                    montoColegiado += montoGeneralcol;
                }
            }

            //listCuotaColegiadoTotal = listCuotaColegiadoTotal.Where(p => p.fk_Pago == null)
            //                                                .OrderBy(o => o.CEcC_FechaGeneracion).ToList();
            //listCuotaColegiadoTotal = listCuotaColegiadoTotal.OrderBy(o => o.CEcC_FechaGeneracion).ToList();

            if (cuotaCol > 0 && listCuotaColegiadoNull.Count > 0)
            {
                for (int i = 0; i < tempListAnioMes.Count(); i++)
                {
                    ModelAnioMesJson tempAnioMes = tempListAnioMes[i];
                    int initList = i * tempAnioMes.Total;
                    for (int j = 0; j < tempAnioMes.Total; j++)
                    {
                        ccee_CargoEconomico_colegiado tempEcoCol = listCuotaColegiadoNull[initList + j];
                        montoColegiado += tempEcoCol.CEcC_Monto.Value;

                    }
                    if (i == cuotaCol - 1) break;
                }
            }

            

            ultimoTimbre = (from listTim in listCuotaTimbreTotal
                            where listTim.fk_Pago != null
                            orderby listTim.CEcT_FechaGeneracion descending
                            select listTim.CEcT_FechaGeneracion).FirstOrDefault();

            ultimoColegiado = (from listCol in listCuotaColegiadoTotal
                            where listCol.fk_Pago != null
                            orderby listCol.CEcC_FechaGeneracion descending
                            select listCol.CEcC_FechaGeneracion).FirstOrDefault();

            ccee_Colegiado tempColegiado = (from col in db.ccee_Colegiado
                                           where col.Col_NoColegiado == colegiado
                                           select col).Single();

            decimal montoTotal = montoColegiado + montoTimbre + montoPostumo + montoMora;
            return new ResponseSaldoColegiadoJson() {
                noCuotasCol = cuotaCol,
                noCuotasTim = cuotaTimbre,
                saldoColegiado = montoColegiado,
                saldoTimbre = montoTimbre,
                postumo = montoPostumo,
                mora = montoMora,
                codigo = 0,
                nombreCompleto = tempColegiado.Col_PrimerNombre + " "
                                + tempColegiado.Col_SegundoNombre + " "
                                + tempColegiado.Col_PrimerApellido + " "
                                + tempColegiado.Col_SegundoApellido,
                estadoColegiado = ((tempColegiado.Col_Estatus == null) ? 0 : tempColegiado.Col_Estatus.Value),
                fechaUltimoPagoTim = GeneralFunction.DateOfStringWS(ultimoTimbre),
                fechaUltimoPagoCol = GeneralFunction.DateOfStringWS(ultimoColegiado),
                total = montoTotal
            };
        }

        // POST: api/WS_SaldoColegiado
        public IHttpActionResult Post(RequestSaldoColegiadoJson rSaldoColegiado)
        {

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {

                    int cuotaTimbre = rSaldoColegiado.noCoutasTimbre;
                    int cuotaCol = rSaldoColegiado.noCuotasColegiado;
                    int colegiado = rSaldoColegiado.noColegiado;
                    Nullable<DateTime> ultimoTimbre = null;
                    Nullable<DateTime> ultimoColegiado = null;

                    Nullable<DateTime> datePay = GeneralFunction.changeStringofDateWS(
                                                    rSaldoColegiado.fechaPago + "-" + 
                                                    rSaldoColegiado.horaPago);

                    if (datePay == null) return Ok(HttpStatusCode.NoContent);

                    ccee_Pago tempPay = new ccee_Pago()
                    {
                        Pag_Monto = rSaldoColegiado.montoTotal,
                        Pag_Observacion = "Transferencia bancaria",
                        Pag_Fecha = datePay,
                        fk_Caja = rSaldoColegiado.idCaja,
                        fk_Cajero = rSaldoColegiado.idCajero
                    };

                    db.ccee_Pago.Add(tempPay);
                    db.SaveChanges();

                    //Obtengo todo lo relacionado con los pagos de timbre y colegiado respecto al numero de colegiado
                    var tempCuotatimbreTotal = from cet in db.ccee_CargoEconomico_timbre
                                               where cet.fk_Colegiado == colegiado
                                               select cet;
                    var tempCuotaColegiadoTotal = from col in db.ccee_CargoEconomico_colegiado
                                                  where col.fk_Colegiado == colegiado
                                                  select col;

                    
                    //Los combierto a List para la manipulacion
                    List<ccee_CargoEconomico_timbre> listCuotaTimbreTotal = tempCuotatimbreTotal.ToList();
                    listCuotaTimbreTotal = listCuotaTimbreTotal.OrderByDescending(o => o.CEcT_NoCargoEconomico).ToList();

                    List<ccee_CargoEconomico_colegiado> listCuotaColegiadoTotal = tempCuotaColegiadoTotal.ToList();
                    listCuotaColegiadoTotal = listCuotaColegiadoTotal.OrderByDescending(o => o.CEcC_NoCargoEconomico).ToList();

                    //Obtego un listado solo de cargos economico que no se han pagado
                    List<ccee_CargoEconomico_timbre> listCuotaTimbreNull = listCuotaTimbreTotal.Where(p => p.fk_Pago == null)
                                                                                                .OrderBy(o => o.CEcT_FechaGeneracion).ToList();

                    //Obtengo la lista de meses con año para saber cuantos meses no ha pagado
                    List<ModelAnioMesJson> tempListAnioMes = obtenerCantidadCargosTimbre(listCuotaTimbreNull).ToList();

                    if (tempListAnioMes.Count() < cuotaTimbre)
                    {
                        //Obtengo la ultima fecha de pago generada para comenzar el cargo desde ahi
                        ultimoTimbre = (from listTim in listCuotaTimbreTotal
                                        orderby listTim.CEcT_FechaGeneracion descending
                                        select listTim.CEcT_FechaGeneracion).FirstOrDefault();

                        ultimoTimbre = ((ultimoTimbre == null) ? DateTime.Now : ultimoTimbre);

                        int diferencia = cuotaTimbre - tempListAnioMes.Count();

                        //Obtengo los valores de Tipo cargo economico
                        decimal montoTimbreGen = generarMontoTimbre(colegiado);
                        decimal? valorUltimoPostumo = (from tce in db.ccee_TipoCargoEconomico
                                                       where tce.TCE_NoTipoCargoEconomico == 5
                                                       select tce.TCE_Valor).Single();

                        //Creo el Cargo economico timbre y se paga de una vez
                        ccee_CargoEconomico_timbre tempTim;
                        ccee_CargoEconomico_timbre tempPostumo;
                        ccee_CargoEconomico_timbre tempInteres;
                        for (int i = 0; i < diferencia; i++)
                        {
                            DateTime siguienteMes = ultimoTimbre.Value.AddMonths(i + 1);
                            tempTim = new ccee_CargoEconomico_timbre()
                            {
                                CEcT_Monto = montoTimbreGen,
                                CEcT_FechaGeneracion = siguienteMes,
                                fk_TipoCargoEconomico = 4,
                                fk_Colegiado = colegiado,
                                fk_Pago = tempPay.Pag_NoPago
                            };

                            db.ccee_CargoEconomico_timbre.Add(tempTim);
                            db.SaveChanges();

                            tempPostumo = new ccee_CargoEconomico_timbre()
                            {
                                CEcT_Monto = valorUltimoPostumo,
                                CEcT_FechaGeneracion = siguienteMes,
                                fk_TipoCargoEconomico = 5,
                                fk_Colegiado = colegiado,
                                fk_Pago = tempPay.Pag_NoPago,
                                CEcT_RelacionCargoEco = tempTim.CEcT_NoCargoEconomico
                            };

                            tempInteres = new ccee_CargoEconomico_timbre()
                            {
                                CEcT_Monto = 0,
                                CEcT_FechaGeneracion = siguienteMes,
                                fk_TipoCargoEconomico = 120,
                                fk_Colegiado = colegiado,
                                fk_Pago = tempPay.Pag_NoPago,
                                CEcT_RelacionCargoEco = tempTim.CEcT_NoCargoEconomico
                            };

                            db.ccee_CargoEconomico_timbre.Add(tempPostumo);
                            db.ccee_CargoEconomico_timbre.Add(tempInteres);
                            db.SaveChanges();

                            //montoTimbre += montoTimbreGen;
                            //montoPostumo += valorUltimoPostumo.Value;
                        }

                    }

                    
                    //Se agrega el id del pago a cada uno de los pagos desde los primeros meses
                    if (cuotaTimbre > 0 && listCuotaTimbreNull.Count > 0)
                    {
                        for (int i = 0; i < tempListAnioMes.Count(); i++)
                        {
                            ModelAnioMesJson tempAnioMes = tempListAnioMes[i];
                            int initList = i * tempAnioMes.Total;
                            for (int j = 0; j < tempAnioMes.Total; j++)
                            {
                                ccee_CargoEconomico_timbre tempEcoTim = listCuotaTimbreNull[initList + j];
                                tempEcoTim.fk_Pago = tempPay.Pag_NoPago;
                                db.Entry(tempEcoTim).State = EntityState.Modified;

                            }
                            if (i == cuotaTimbre - 1) break;
                        }
                    }
                    //Se guardan los cambios
                    db.SaveChanges();

                    // Generar pago de cuota Colegiado ------------------------------

                    //Creo una lista de solo los pagos que no se han realizado
                    List<ccee_CargoEconomico_colegiado> listCuotaColegiadoNull = listCuotaColegiadoTotal.Where(p => p.fk_Pago == null)
                                                                                                        .OrderBy(o=>o.CEcC_FechaGeneracion).ToList();
                    
                    tempListAnioMes = obtenerCantidadCargosColegiado(listCuotaColegiadoNull).ToList();

                    if (tempListAnioMes.Count() < cuotaCol)
                    {
                        //Obtengo la ultima fecha de pago en la totalidad para comenzar desde esa fecha
                        int diferencia = cuotaCol - tempListAnioMes.Count();
                        ultimoColegiado = (from listCol in listCuotaColegiadoTotal
                                           orderby listCol.CEcC_FechaGeneracion descending
                                           select listCol.CEcC_FechaGeneracion).FirstOrDefault();

                        ultimoColegiado = ((ultimoColegiado == null) ? DateTime.Now : ultimoColegiado);

                        //Obtengo la lista de cargos economicos de esa temporada
                        List<ccee_TipoCargoEconomico> listTipoCargos = generarMontoColegiado();
                        ccee_CargoEconomico_colegiado tempEconomicoCol;
                        for (int i = 0; i < diferencia; i++)
                        {
                            DateTime siguienteMes = ultimoColegiado.Value.AddMonths(i + 1);
                            for (int j = 0; j < listTipoCargos.Count(); j++)
                            {
                                //Genero los cargos economicos y se pagan de una vez
                                tempEconomicoCol = new ccee_CargoEconomico_colegiado()
                                {
                                    CEcC_Monto = listTipoCargos[j].TCE_Valor,
                                    CEcC_FechaGeneracion = siguienteMes,
                                    fk_TipoCargoEconomico = listTipoCargos[j].TCE_NoTipoCargoEconomico,
                                    fk_Colegiado = colegiado,
                                    fk_Pago = tempPay.Pag_NoPago
                                };
                                db.ccee_CargoEconomico_colegiado.Add(tempEconomicoCol);
                            }

                            //montoColegiado += montoGeneralcol;
                        }
                        db.SaveChanges();
                    }


                    if (cuotaCol > 0 && listCuotaColegiadoNull.Count > 0)
                    {
                        //Recorremos los meses que se deben
                        for (int i = 0; i < tempListAnioMes.Count(); i++)
                        {

                            ModelAnioMesJson tempAnioMes = tempListAnioMes[i];
                            int initList = i * tempAnioMes.Total;
                            for (int j = 0; j < tempAnioMes.Total; j++)
                            {
                                //Generamos el pago
                                ccee_CargoEconomico_colegiado tempEcoCol = listCuotaColegiadoNull[initList + j];
                                tempEcoCol.fk_Pago = tempPay.Pag_NoPago;
                                db.Entry(tempEcoCol).State = EntityState.Modified;
                                //montoColegiado += tempEcoCol.CEcC_Monto.Value;

                            }
                            if (i == cuotaCol - 1) break;
                        }
                    }
                    //Se guardan los cambios
                    db.SaveChanges();

                    
                    //transaction.Rollback();
                    transaction.Commit();
                    return Ok(HttpStatusCode.OK);
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return Ok(HttpStatusCode.BadRequest);
                }
            }


        }

        // PUT: api/WS_SaldoColegiado/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/WS_SaldoColegiado/5
        public void Delete(int id)
        {
        }


        public decimal generarMontoTimbre(int colegiado)
        {
            decimal salarioBase = 8000;
            decimal monto = 0;

            ccee_Colegiado tempCol = (from col in db.ccee_Colegiado
                                      where col.Col_NoColegiado == colegiado
                                      select col).Single();


            if(tempCol.Col_Salario.Value <= salarioBase)
            {
                monto = salarioBase * (decimal)0.01;
            }else
            {
                decimal tempMonto = Math.Round(tempCol.Col_Salario.Value * (decimal)0.01, 2);
                decimal tempMontoT = tempMonto - Math.Round(tempMonto, 0);
                tempMonto = Math.Round(tempMonto, 0);
                if (tempMontoT > 0 && tempMontoT <= 0.25m)
                {
                    tempMonto = tempMonto + 0.25m;
                }else if (tempMontoT > 0.25m && tempMontoT <= 0.50m)
                {
                    tempMonto = tempMonto + 0.50m;
                }
                else if (tempMontoT > 0.50m && tempMontoT <= 0.75m)
                {
                    tempMonto = tempMonto + 0.75m;
                }
                else if (tempMontoT > 0.75m && tempMontoT <= 0.99m)
                {
                    tempMonto = tempMonto + 1.00m;
                }
                monto = tempMonto;
            }
            return monto;
        }

        private List<ccee_TipoCargoEconomico> generarMontoColegiado()
        {
            int idEconomicReferee = 118;
            ccee_TipoCargoEconomico tempTypeEco = (from tca in db.ccee_TipoCargoEconomico
                                                   where tca.TCE_NoTipoCargoEconomico == idEconomicReferee
                                                   select tca).Single();

            string calculation = tempTypeEco.TCE_CondicionCalculo;
            string[] arrayCalculation = calculation.Split(',');
            List<int> intCalculation = new List<int>();
            foreach (string item in arrayCalculation)
            {
                intCalculation.Add(Convert.ToInt32(item));
            }

            var listE = from tca in db.ccee_TipoCargoEconomico
                                         where intCalculation.Contains(tca.TCE_NoTipoCargoEconomico)
                                         select tca;
            return listE.ToList();

        }


        private IEnumerable<ModelAnioMesJson> obtenerCantidadCargosColegiado(List<ccee_CargoEconomico_colegiado> listCuotaColegiadoTotal)
        {
            //DateTime dt = DateTime.Now.AddMonths(-1);
            //TimeSpan ttt = dt.TimeOfDay;
            //var tt = listCuotaColegiadoTotal.Where(o=>o.CEcC_FechaGeneracion.Value.TimeOfDay <= ttt);

            var model = listCuotaColegiadoTotal
                .GroupBy(o => new
                {
                    Month = o.CEcC_FechaGeneracion.Value.Month,
                    Year = o.CEcC_FechaGeneracion.Value.Year
                })
                .Select(g => new ModelAnioMesJson
                {
                    Month = g.Key.Month,
                    Year = g.Key.Year,
                    Total = g.Count()
                })
                .OrderByDescending(a => a.Year)
                .ThenByDescending(a => a.Month)
                .ToList();

            IEnumerable<ModelAnioMesJson> tempAnioMes = model;


            return model;

        }

        private IEnumerable<ModelAnioMesJson> obtenerCantidadCargosTimbre(List<ccee_CargoEconomico_timbre> listCuotaColegiadoTotal)
        {
            //DateTime dt = DateTime.Now.AddMonths(-1);
            //TimeSpan ttt = dt.TimeOfDay;
            //var tt = listCuotaColegiadoTotal.Where(o=>o.CEcC_FechaGeneracion.Value.TimeOfDay <= ttt);

            var model = listCuotaColegiadoTotal
                .GroupBy(o => new
                {
                    Month = o.CEcT_FechaGeneracion.Value.Month,
                    Year = o.CEcT_FechaGeneracion.Value.Year
                })
                .Select(g => new ModelAnioMesJson
                {
                    Month = g.Key.Month,
                    Year = g.Key.Year,
                    Total = g.Count()
                })
                .OrderByDescending(a => a.Year)
                .ThenByDescending(a => a.Month)
                .ToList();

            IEnumerable<ModelAnioMesJson> tempAnioMes = model;


            return model;

        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

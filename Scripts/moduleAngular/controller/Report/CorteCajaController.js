﻿(function () {
    angular.module('reports')
        .controller('corteCaja', ['$scope', '$http', 'CONSTANT', '$anchorScroll', '$location', 'reportFactory', function ($scope, $http, CONSTANT, $anchorScroll, $location, reportFactory) {
            var dateReport = $location.search().date;
            var cashierReport = $location.search().cashier;
            $scope.direction;
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquaters;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            console.log(dateReport);
            console.log(cashierReport);


            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.identical = function (actual, expected) {
                return actual === parseInt(expected);
            }

            $scope.generateList = function () {
                var textGetReport = "";
                console.log(($scope.selectHead != null) ? $scope.selectHead.id : "");
                textGetReport = "&cashier=" + cashierReport +
                                "&date=" + dateReport;
                console.log(textGetReport);
                $scope.direction = "../../Reports/aspxReports/CorteCajaAspx.aspx?" + textGetReport;
            }

        }]);

})()
﻿(function () {
    angular.module('generals')
        .controller('testDocumento', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', '$window', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory, $window) {
            $scope.listUniversity;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.idUniversidadUpdate = 0;
            $scope.idFile = "";
            $scope.idFiletimbre = "";
            $scope.status = 0;
            $scope.statusT = 0;
            $scope.timbres = [];
            $scope.values = [];

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }


            $scope.mm = function () {
                $scope.showLoadingFunction();
                $scope.idFile = "";
                var information = {
                    "idIntitution": 1,
                    "base64": $scope.modelcerbase.base64,
                    "posx": 50,
                    "posy": 50,
                    "page": 1
                }
                console.log(information);
                generalactionsFactory.actionPostFunction('http://104.248.227.154:4023/api/certification', information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            console.log(response.data);
                            $scope.idFile = response.data.id;
                            $scope.status = 1;
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });

            }

            $scope.mmdescarga = function () {
                $scope.showLoadingFunction();
                var information = {
                    "id": $scope.idFile
                }
                console.log(information);
                generalactionsFactory.actionPostFunction('http://104.248.227.154:4023/api/certification/find', information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            console.log(response.data);
                            if (response.data.status == 1) {
                                $scope.status = 2;
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });

            }

            $scope.addTimbres = function (value, count) {
                var valueInt = parseInt(value)
                $scope.values.push(valueInt);
                var x = Math.floor((Math.random() * 500) + 100);
                for (var i = 0; i < count; i++) {
                    var timbre = {
                        "No": i,
                        "page": x,  
                        "valor": valueInt
                    }
                    $scope.timbres.push(timbre);
                }
            }

            $scope.solicitarTimbre = function () {
                $scope.showLoadingFunction();
                $scope.idFiletimbre = "";
                var information = {
                    "idIntitution": 1,
                    "base64": (($scope.modeltimbase == null) ? "" : $scope.modeltimbase.base64),
                    "colegiado": 989898,
                    "timbres": $scope.timbres,
                    "valores": $scope.values
                }
                console.log(information);
                generalactionsFactory.actionPostFunction('http://104.248.227.154:4023/api/timbre', information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            console.log(response.data);
                            $scope.idFiletimbre = response.data.id;
                            $scope.statusT = 1;
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }


            $scope.revisionTimbre = function () {
                $scope.showLoadingFunction();
                var information = {
                    "id": $scope.idFiletimbre
                }
                console.log(information);
                generalactionsFactory.actionPostFunction('http://104.248.227.154:4023/api/timbre/find', information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            console.log(response.data);
                            if (response.data.status == 1) {
                                $scope.statusT = 2;
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

        }]);

})()
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.UserWS
{
    public class RequestUserWSJson
    {
        public string email { get; set; }
        public string pass { get; set; }
    }
}
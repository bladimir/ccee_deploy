﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonChequeRechazado
    {
        public int colegiado { get; set; }
        public string nombre { get; set; }
        public string documento { get; set; }
        public string banco { get; set; }
        public decimal monto { get; set; }
        public string sede { get; set; }
        public string cajero { get; set; }
        public string caja { get; set; }
        public DateTime? fecha { get; set; }
        public string fechaTexto
        {
            get
            {
                return ((fecha != null)? fecha.Value.ToString("dd/MM/yyyy") : "" );
            }
        }
        public string rechazado { get; set; }
        public string congelado { get; set; }
    }
}
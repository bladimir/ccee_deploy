﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Pay
{
    public class ResponsePayJson
    {
        public int id { get; set; }
        public Nullable<DateTime> date { get; set; }
        public Nullable<int> amount { get; set; }
        public string remake { get; set; }
        public int idCashier { get; set; }
        public int idCash { get; set; }
    }
}
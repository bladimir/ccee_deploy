﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonFichaResumida
    {
        public int NoCoelgiado { get; set; }
        public string nombre { get; set; }
        public string lugarNacimiento { get; set; }
        public string fechaNacimiento { get; set; }
        public string nacionalidad { get; set; }
        public string dpi { get; set; }
        public string tituloProfesional { get; set; }
        public string gradoAcademico { get; set; }
        public string egresado { get; set; }
        public string fechaGraduacion { get; set; }
        public string tipoEcaluacion { get; set; }
        public string temaDesarrollado { get; set; }
        public string fechaColegiacion { get; set; }
    }
}
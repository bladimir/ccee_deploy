﻿(function () {
    angular.module('generals')
        .controller('municipality', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.idDepartament = 0;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idMunicipalityUpdate = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_COUNTRY)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listCountry = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            


            $scope.getMunici = function () {
                $scope.idDepartament = $scope.selectDepartament.id;
                $scope.getMunicipality($scope.idDepartament);
            }


            $scope.getDepartament = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_DEPARTAMENT, $scope.selectCountry.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listDepartament = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getMunicipality = function (id) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_MUNICIPALITY, id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listMunicipality = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.addMunicipality = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.municipalityname,
                    idDepartament: $scope.idDepartament
                }
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_MUNICIPALITY, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getMunicipality($scope.idDepartament);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (mun) {
                $scope.idMunicipalityUpdate = mun.id;
                $scope.municipalityname = mun.name;
                $anchorScroll();
            }

            $scope.updateMunicipality = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.municipalityname,
                    idDepartament: $scope.idDepartament
                }
                generalactionsFactory.actionPutFunction(CONSTANT.URL_API_MUNICIPALITY, $scope.idMunicipalityUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.getMunicipality($scope.idDepartament);
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.deleteMunicipality = function (mun) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionDeleteFunction(CONSTANT.URL_API_MUNICIPALITY, mun.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getMunicipality($scope.idDepartament);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idMunicipalityUpdate = 0;
                $scope.municipalityname = "";
            }


        }]);

})()
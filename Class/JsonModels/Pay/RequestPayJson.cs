﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Pay
{
    public class RequestPayJson
    {
        public decimal amount { get; set; }
        public string remake { get; set; }
        public int idCashier { get; set; }
        public int idCash { get; set; }
    }
}
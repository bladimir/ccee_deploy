﻿(function () {
    angular.module('reports')
        .controller('calidadyEstatusSaldos', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'reportFactory', function ($scope, $http, CONSTANT, $anchorScroll, reportFactory) {
            $scope.direction;
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquaters;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }


            $scope.generateList = function () {
                var textGetReport = "";
                console.log(($scope.selectHead != null) ? $scope.selectHead.id : "");
                textGetReport = "&universidad=" + $scope.inUniversity +
                                "&type=" + $scope.inTypeReferee +
                                "&head=" + $scope.inHead +
                                "&special=" + $scope.inSpecial +
                                "&active=" + $scope.inActive;
                console.log(textGetReport);
                $scope.direction = "../../Reports/aspxReports/webCalidadyEstatusSaldos.aspx?" + textGetReport;
            }

        }]);

})()
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.AddressFamily
{
    public class ResponseAddressFamilyJson
    {
        public int idAddressFamily { get; set; }
        public string street { get; set; }
        public string number { get; set; }
        public string zone { get; set; }
        public string colony { get; set; }
    }
}
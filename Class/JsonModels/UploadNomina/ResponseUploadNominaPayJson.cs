﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webcee.Class.JsonModels.Economic;

namespace webcee.Class.JsonModels.UploadNomina
{
    public class ResponseUploadNominaPayJson
    {
        public int status { get; set; }
        public string textError { get; set; }
        public string timbres { get; set; }
        public string recibo { get; set; }
        public decimal total { get; set; }
        public decimal extra { get; set; }
        public List<ResponseEconomicJson> listEco { get; set; }
        public List<JsonDataResponse> listDoc { get; set; }
    }
}
﻿(function () {
    angular.module('generals')
        .controller('diplomaNewReferee', ['$scope', '$http', 'CONSTANT', '$window', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $window, $anchorScroll, generalactionsFactory) {
            $scope.listUniversity;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.idUniversidadUpdate = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            
            


            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }


            $scope.getDiploma = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction("/Referre/CreateDiploma", $scope.idRefereeContent)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        if (response.data != "") {
                            $window.open(response.data);
                        }
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.generateList = function () {
                var textGetReport = "";
                console.log(($scope.selectHead != null) ? $scope.selectHead.id : "");
                textGetReport = "&colegiado=" + $scope.idRefereeContent;
                console.log(textGetReport);
                $scope.direction = "../../Reports/aspxReports/DiplomaAspx.aspx?" + textGetReport;
            }


        }]);

})()
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_CalculoActuarioController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();
        public class tempActuario
        {
            public int Col_NoColegiado { get; set; }
            public decimal? Col_Salario { get; set; }
            public decimal? Acumulado { get; set; }
            public int? Meses { get; set; }
            public decimal? Promedio { get; set; }
        }


        // GET: api/Api_CalculoActuario
        public List<tempActuario> Get()
        {
            try
            {
                List<tempActuario> actuario = new List<tempActuario>();
                var num = db.Calculo_Actuario().ToList();
                foreach (var item in num)
                {
                    actuario.Add(new tempActuario() {
                        Col_NoColegiado = item.Col_NoColegiado,
                        Col_Salario = item.Col_Salario,
                        Acumulado = item.Acumulado,
                        Meses = item.Meses,
                        Promedio = item.Promedio
                    });
                }
                return actuario;

            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_CalculoActuario/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_CalculoActuario
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_CalculoActuario/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_CalculoActuario/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

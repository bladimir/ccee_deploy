﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.DocumentAccouting
{
    public class listBreakdownAccountJson
    {
        public int id { get; set; }
        public string codigo { get; set; }
        public int idTypeEconomic { get; set; }
        public string condition { get; set; }
    }
}
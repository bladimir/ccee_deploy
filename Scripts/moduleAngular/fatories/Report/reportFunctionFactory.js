﻿(function () {
    angular.module('reports')
        .factory('reportFunctionFactory', ['CONSTANT', 'reportFactory', function (CONSTANT, reportFactory) {

            var reportFunctionFactory = {};
            var idReferee = 0;

            reportFunctionFactory.actionGetFunction = function (dirr) {
                return $http.get(dirr);
            };

            reportFunctionFactory.setIdReferee = function (id) {
                console.log("Numero: " + id);
                idReferee = id;
            };

            reportFunctionFactory.getIdReferee = function () {
                return idReferee;
            };

            reportFunctionFactory.getUniversity = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_UNIVERSITY)
                .then(function (response) {
                    console.log(response);
                    return response.data;
                }, function (error) {
                    console.log(error);
                    return null;
                });
            };

            return reportFunctionFactory;
        }]);

})()
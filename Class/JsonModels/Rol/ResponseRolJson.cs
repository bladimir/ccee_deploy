﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Rol
{
    public class ResponseRolJson
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
﻿(function () {
    angular.module('document')
        .controller('userCertificacion', ['$scope', '$http', '$anchorScroll', 'CONSTANT', '$window', 'documentFactory', function ($scope, $http, $anchorScroll, CONSTANT, $window, documentFactory) {

            $scope.listData;
            $scope.listDataTimbre;
            $scope.showDelete = false;
            $scope.showNotify = 'emergente1 notify-event-content';
            $scope.idUpdate = 0;
            $scope.objectUpdate;
            $scope.dateCon = null;
            $scope.statusFile = false;
            $scope.idReferee = 0;
            $scope.position = 0;
            $scope.pointer = 0;
            $scope.limitPage = 10;
            $scope.listTim = [];
            $scope.listDocumentPrint = [];
            $scope.listDocumentShow = [];
            $scope.numPages = 1;
            $scope.limitPerPage = 10;
            $scope.regexAmount = CONSTANT.REGEX_AMOUNT;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = 'emergente2 notify-event-content';
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = 'emergente1 notify-event-content';
            }

            $scope.getDocs = function (idReferee) {
                $scope.showLoadingFunction();
                documentFactory.actionGetIntFunction("/Referre/GetFilesReferee", idReferee)
                    .then(function (response) {
                        if (response.data != null) {
                            $scope.hideLoadingFunction();
                            $scope.listDocumentPrint = response.data;
                            $scope.setNumPaginas();
                            $scope.mostrarPaginas($scope.pointer);
                        }
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.setAuth = function (idReferee) {
                $scope.showLoadingFunction();
                $scope.idReferee = idReferee;
                documentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN_REFEREE)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    documentFactory.setAuthorization(response.data);
                    $scope.getDocs(idReferee);
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            // ------------------------------ PAGINACION -------------------------------
            $scope.mostrarPaginas = function (numPagina) {
                $scope.pointer = numPagina;
                $scope.numPages = Math.ceil($scope.listDocumentPrint.length / $scope.limitPerPage);
                // definir rango
                var rangoArriba = numPagina * $scope.limitPerPage + $scope.limitPerPage;
                var rangoAbajo = rangoArriba - $scope.limitPerPage;
                var i;
                $scope.listDocumentShow = [];
                for (i = rangoAbajo; i < rangoArriba; i++) {
                    if (i < $scope.listDocumentPrint.length) {
                        $scope.listDocumentShow.push($scope.listDocumentPrint[i]);
                    }
                }
            }

            $scope.setNumPaginas = function () {
                $scope.pointer = 0;
                $scope.numPages = Math.ceil($scope.listDocumentPrint.length / $scope.limitPerPage);
            }

            $scope.next = function () {
                if ($scope.pointer + 1 < $scope.numPages) {
                    $scope.pointer++;
                    $scope.mostrarPaginas($scope.pointer);
                }
            }

            $scope.back = function () {
                if ($scope.pointer - 1 >= 0) {
                    $scope.pointer--;
                    $scope.mostrarPaginas($scope.pointer);
                }
            }

            // ------------------------------ COSAS DE TIMBRE -------------------------------------


            $scope.resetFiles = function () {
                $scope.modeltimbase = null;
                $scope.statusFile = false;
                $anchorScroll();
            }
        }]);
})()
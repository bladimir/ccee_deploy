﻿(function () {
    angular.module('referee')
        .controller('changeAllReferee', ['$scope', '$http', '$location', 'CONSTANT', '$anchorScroll', 'refereeFactory', function ($scope, $http, $location, CONSTANT, $anchorScroll, refereeFactory) {

            $scope.numbers = CONSTANT.REGEX_DIGIT;
            $scope.regexNit = CONSTANT.REGEX_NIT;
            $scope.regexPhone = CONSTANT.REGEX_PHONE;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.info = "";
            $scope.listData;
            $scope.idUpdate = 0;
            $scope.position = 0;
            $scope.rangeReferee = [];
            $scope.errors = [];

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.setAuth = function () {
                refereeFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    refereeFactory.setAuthorization(response.data);
                    console.log(refereeFactory.getAuthorization());
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.init = function () {
                //$scope.getReferee();
            }

            $scope.getReferee = function () {
                console.log(CONSTANT.URL_API_REFEREE);
                $scope.showLoadingFunction();
                refereeFactory.actionGetFunction(CONSTANT.URL_API_REFEREE)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        $scope.listData = response.data;
                        console.log($scope.listData);
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getStatus = function (idReferee) {
                refereeFactory.actionGetIntFunction(CONSTANT.URL_GET_STATUS, idReferee)
                .then(function (response) {
                    $scope.position++;
                    var positionReferee = $scope.listData[$scope.position].NoColegiado;
                    console.log($scope.position);
                    $scope.processReferee = positionReferee;
                    $scope.getStatus(positionReferee);
                }, function (error) {
                    console.log(error);
                    $scope.processReferee = "Error en la carga, vuelva a intentar";
                });
            }

            $scope.getListStatus = function () {
                var positionReferee = $scope.listData[$scope.position].NoColegiado;
                $scope.getStatus(positionReferee);
            }

            $scope.getStatusList = function (referress) {
                console.log(referress);
                refereeFactory.actionGetIntFunction(CONSTANT.URL_GET_STATUS, referress.temp)
                .then(function (response) {
                    console.log(response);
                    if (response.data == false) $scope.errors.push("No se cargo: " + referress.temp);
                    if (referress.init <= referress.temp && referress.finish >= referress.temp) {
                        referress.msn = "" + referress.temp;
                        referress.temp++;
                        $scope.getStatusList(referress);
                    }

                }, function (error) {
                    console.log(error);
                    $scope.processReferee = "Error en la carga, vuelva a intentar";
                });
            }

            $scope.changeReferee = function (idReferee) {
                window.location.href = '/Referre/Index/#?id=' + idReferee;
            }

            $scope.addRangeReferee = function (init, finish) {
                var listReferee = {
                    init: init,
                    finish: finish,
                    temp: init,
                    msn: ""
                };
                $scope.rangeReferee.push(listReferee);
            }



        }]);

})()
﻿(function () {
    angular.module('document')
        .controller('inboxTimbre', ['$scope', '$http', '$anchorScroll', 'CONSTANT', '$window', 'documentFactory', function ($scope, $http, $anchorScroll, CONSTANT, $window, documentFactory) {

            $scope.listData;
            $scope.listDataTimbre;
            $scope.showDelete = false;
            $scope.showNotify = 'emergente1 notify-event-content';
            $scope.idUpdate = 0;
            $scope.objectUpdate;
            $scope.position = 1;
            $scope.limitPage = 2;
            $scope.idReferee = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = 'emergente2 notify-event-content';
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = 'emergente1 notify-event-content';
            }

            $scope.init = function () {
                $scope.getListPage($scope.position);
            }

            $scope.setAuth = function (idReferee) {
                $scope.idReferee = idReferee;
                documentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN_REFEREE)
                .then(function (response) {
                    console.log(response);
                    documentFactory.setAuthorization(response.data);
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            

            $scope.getListPage = function (page) {
                documentFactory.actionGetGetIntFunction(CONSTANT.URL_API_TIMBRESSIGN, $scope.idReferee, page)
                    .then(function (response) {
                        console.log(response);
                        $scope.listTim = response.data;
                        if ($scope.listTim.length < $scope.limitPage) {
                            $scope.nextButton = false;
                        } else {
                            $scope.nextButton = true;
                        }
                    }, function (error) {
                        console.log(error);
                    });
            }

            $scope.next = function () {
                $scope.position++;
                $scope.getListPage($scope.position);
            }

            $scope.back = function () {
                $scope.position--;
                $scope.getListPage($scope.position);
            }

            $scope.donwloadFile = function (item) {
                var arrayName = item.name.split('.');
                //window.location.href = '/Document/DocumentTimbre/' + item.key + "/" + arrayName[0];
                $window.open('/Document/DocumentTimbre/' + item.key + "/" + arrayName[0], "_blank")
            }

        }]);
})()
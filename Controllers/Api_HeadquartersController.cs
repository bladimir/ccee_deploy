﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.Headquarters;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_HeadquartersController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Headquarters
        public IEnumerable<ResponseHeadquartersJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempHeadquarter = from hea in db.ccee_Sede
                                       select new ResponseHeadquartersJson()
                                       {
                                           id = hea.Sed_NoSede,
                                           address = hea.Sed_Direccion,
                                           phone = hea.Sed_Telefono,
                                           coordinates = hea.Sed_Coordenadas
                                       };
                Autorization.saveOperation(db, autorization, "Obteniendo sedes");

                return tempHeadquarter;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_Headquarters/5
        public IEnumerable<ResponseHeadquartersJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempHeadquarter = from hea in db.ccee_Sede
                                      where hea.fk_Municipio == id
                                       select new ResponseHeadquartersJson()
                                       {
                                           id = hea.Sed_NoSede,
                                           address = hea.Sed_Direccion,
                                           phone = hea.Sed_Telefono,
                                           coordinates = hea.Sed_Coordenadas
                                       };
                Autorization.saveOperation(db, autorization, "Obteniendo una sedes: id " + id);
                return tempHeadquarter;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_Headquarters
        public IHttpActionResult Post(RequestHeadquartersJson headquarter)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Sede tempHeadquarter = new ccee_Sede()
                {
                    Sed_Direccion = headquarter.address,
                    Sed_Telefono = headquarter.phone,
                    Sed_Coordenadas = headquarter.coordinates,
                    fk_Municipio = headquarter.idMunicipality
                };

                db.ccee_Sede.Add(tempHeadquarter);

                Autorization.saveOperation(db, autorization, "Agregando sede");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_Headquarters/5
        public IHttpActionResult Put(int id, RequestHeadquartersJson headquarter)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Sede tempHeadquarter = db.ccee_Sede.Find(id);

                if (tempHeadquarter == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempHeadquarter.Sed_Direccion = headquarter.address;
                tempHeadquarter.Sed_Telefono = headquarter.phone;
                tempHeadquarter.Sed_Coordenadas = headquarter.coordinates;

                db.Entry(tempHeadquarter).State = EntityState.Modified;

                Autorization.saveOperation(db, autorization, "Actualizando sede: id " + id);
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_Headquarters/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Sede tempHeadquarter = db.ccee_Sede.Find(id);
                if (tempHeadquarter == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_Sede.Remove(tempHeadquarter);

                Autorization.saveOperation(db, autorization, "Eliminando sede: id " + id);
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}

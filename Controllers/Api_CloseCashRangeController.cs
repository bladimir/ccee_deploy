﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.CloseCashRange;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_CloseCashRangeController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_CloseCashRange
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [Route("api/Api_CloseCashRange/{dateI}/{dateF}/{cashier}")]
        // GET: api/Api_CloseCashRange/5
        public List<RequestCloseCashRangeJson> Get(string dateI, string dateF, string cashier)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                DateTime dateTimeI = GeneralFunction.changeStringofDateSimple(dateI).Value;
                DateTime dateTimeF = GeneralFunction.changeStringofDateSimple(dateF).Value;
                if (dateTimeF >= DateTime.Today) dateTimeF = DateTime.Today.AddDays(0);
                string query = "select [BiD_Fecha] as dateR, [fk_Cajero] as idCashier " +
                                "FROM[ccee_AperturaCierre] " +
                                "INNER JOIN[ccee_BilletesDen] ON[ccee_AperturaCierre].[ApC_NoAperturaCierre] = [ccee_BilletesDen].[fk_AperturaCierre] " +
                                "WHERE[BiD_Fecha] >= '"+ dateTimeI.ToString("yyyy-MM-dd") + "' and[BiD_Fecha] <= '" + dateTimeF.ToString("yyyy-MM-dd") + "'";
                query += GeneralFunction.generateQuery("[fk_Cajero]", cashier, Constant.text_query_is_number);
                
                List<RequestCloseCashRangeJson> tempClose = db.Database.SqlQuery<RequestCloseCashRangeJson>(query).ToList();
                for (int i = 0; i < tempClose.Count; i++)
                {
                    tempClose[i].direction = "#?date=" + tempClose[i].dateR.ToString("yyyy-MM-dd") + "&cashier=" + tempClose[i].idCashier;
                }

                Autorization.saveOperation(db, autorization, "Obteniendo listado cierre de caja");
                return tempClose;

            }
            catch (Exception)
            {
                return null;
            }


            //try
            //{
            //    List<RequestCloseCashRangeJson> directions = new List<RequestCloseCashRangeJson>();
            //    DateTime dateTimeI = GeneralFunction.changeStringofDateSimple(dateI).Value;
            //    DateTime dateTimeF = GeneralFunction.changeStringofDateSimple(dateF).Value;
            //    if (dateTimeF == DateTime.Today) dateTimeF = dateTimeF.AddDays(-1);
            //    TimeSpan ts = dateTimeF - dateTimeI;
            //    int differenceInDays = ts.Days;
            //    DateTime dateTimeIncrement;

            //    for (int i = 0; i <= differenceInDays; i++)
            //    {
            //        dateTimeIncrement = dateTimeI.AddDays(i);
            //        string dateReport = dateTimeIncrement.ToString("yyyy-MM-dd");

            //        if (!cashier.Equals("0"))
            //        {
            //            string[] cashiers = cashier.Split(',');
            //            for (int j = 0; j < cashiers.Length; j++)
            //            {
            //                directions.Add(new RequestCloseCashRangeJson()
            //                {
            //                    date = dateReport,
            //                    idCashier = cashiers[j],
            //                    direction = "#?date="+ dateReport + "&cashier="+ cashiers[j]
            //                });
            //            }
            //        }else
            //        {
            //            directions.Add(new RequestCloseCashRangeJson()
            //            {
            //                date = dateReport,
            //                idCashier = "0",
            //                direction = dateReport 
            //            });
            //        }
                    
            //    }
            //    return directions;
            //}
            //catch (Exception)
            //{
            //    return null;
            //}
            
        }

        // POST: api/Api_CloseCashRange
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_CloseCashRange/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_CloseCashRange/5
        public void Delete(int id)
        {
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}

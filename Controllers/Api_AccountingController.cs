﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.Accounting;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_AccountingController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Accounting
        public IEnumerable<ResponseAccountingJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempAccounting = from acc in db.ccee_PartidaContable
                                         select new ResponseAccountingJson()
                                         {
                                             id = acc.PCo_NoPartidaContable,
                                             description = acc.PCo_Descripcion,
                                             action = acc.PCo_Accion,
                                             key = acc.PCo_Codigo,
                                             name = acc.PCo_Nombre,
                                             position = acc.PCo_Posicion,
                                             groupAccouting = acc.PCo_Agrupar
                                         };
                Autorization.saveOperation(db, autorization, "Obteniendo partidas contable");
                return tempAccounting;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_Accounting/5
        public IEnumerable<ResponseAccountingJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                

                var tempAccounting = from acc in db.ccee_PartidaContable
                                       where acc.fk_TipoPartida == id
                                       select new ResponseAccountingJson()
                                       {
                                           id = acc.PCo_NoPartidaContable,
                                           action = acc.PCo_Accion,
                                           description = acc.PCo_Descripcion,
                                           key = acc.PCo_Codigo,
                                           name = acc.PCo_Nombre,
                                           position = acc.PCo_Posicion,
                                           groupAccouting = acc.PCo_Agrupar
                                       };
                Autorization.saveOperation(db, autorization, "Obteniendo partidas contable por tipo");
                return tempAccounting;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_Accounting
        public IHttpActionResult Post(RequestAccountingJson account)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_PartidaContable tempAccounting = new ccee_PartidaContable()
                {
                    PCo_Descripcion = account.description,
                    PCo_Accion = account.action,
                    fk_TipoPartida = account.idTypeAccount,
                    PCo_Codigo = account.key,
                    PCo_Nombre = account.name,
                    PCo_Posicion = account.position,
                    PCo_Agrupar = account.groupAccouting
                };

                db.ccee_PartidaContable.Add(tempAccounting);
                Autorization.saveOperation(db, autorization, "Agregando partida contable");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_Accounting/5
        public IHttpActionResult Put(int id, RequestAccountingJson account)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_PartidaContable tempAccounting = db.ccee_PartidaContable.Find(id);

                if (tempAccounting == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempAccounting.PCo_Descripcion = account.description;
                tempAccounting.PCo_Accion = account.action;
                tempAccounting.PCo_Nombre = account.name;
                tempAccounting.PCo_Codigo = account.key;
                tempAccounting.PCo_Posicion = account.position;
                tempAccounting.PCo_Agrupar = account.groupAccouting;

                db.Entry(tempAccounting).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando partida contable");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_Accounting/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_PartidaContable tempAccounting = db.ccee_PartidaContable.Find(id);
                if (tempAccounting == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_PartidaContable.Remove(tempAccounting);
                Autorization.saveOperation(db, autorization, "Eliminando partida contable");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Address
{
    public class RequestAddressJson
    {
        public string street { get; set; }
        public string number { get; set; }
        public string zone { get; set; }
        public string colony { get; set; }
        public string caserio { get; set; }
        public string barrio { get; set; }
        public int? typeAddress { get; set; }
        public int? municipality { get; set; }
        public int idReferee { get; set; }
        public int notification { get; set; }
    }
}
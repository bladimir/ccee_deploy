﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.PaymentCenter;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_PaymentCenterController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_PaymentCenter
        public IEnumerable<ResponsePaymentCenterJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempPayment = from cen in db.ccee_CentroPago
                                      select new ResponsePaymentCenterJson()
                                      {
                                          id = cen.CeP_NoCentroPago,
                                          name = cen.CeP_Nombre
                                      };
                Autorization.saveOperation(db, autorization, "Obteniendo centros de pago");
                return tempPayment;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_PaymentCenter/5
        public IEnumerable<ResponsePaymentCenterJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempPayment = from cen in db.ccee_CentroPago
                                      where cen.fk_Sede == id
                                      select new ResponsePaymentCenterJson()
                                      {
                                          id = cen.CeP_NoCentroPago,
                                          name = cen.CeP_Nombre
                                      };
                Autorization.saveOperation(db, autorization, "Obteniendo centro de pago de una sede");
                return tempPayment;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_PaymentCenter
        public IHttpActionResult Post(RequestPaymentCenterJson paymentCenter)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_CentroPago tempPayment = new ccee_CentroPago()
                {
                    CeP_Nombre = paymentCenter.name,
                    fk_Sede = paymentCenter.idHeadquarter
                };

                db.ccee_CentroPago.Add(tempPayment);
                Autorization.saveOperation(db, autorization, "Agregando centro de pago");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_PaymentCenter/5
        public IHttpActionResult Put(int id, RequestPaymentCenterJson paymentCenter)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_CentroPago tempPayment = db.ccee_CentroPago.Find(id);

                if (tempPayment == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempPayment.CeP_Nombre = paymentCenter.name;

                db.Entry(tempPayment).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando centro de pago");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_PaymentCenter/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_CentroPago tempPayment = db.ccee_CentroPago.Find(id);
                if (tempPayment == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_CentroPago.Remove(tempPayment);
                Autorization.saveOperation(db, autorization, "Eliminando centro de pago");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonRecibosOperadosTemp
    {
        public int idPago { get; set; }
        public int contable { get; set; }
        public string document { get; set; }
        public decimal efectivo { get; set; }
        public decimal cheque { get; set; }
        public decimal tarjetaD { get; set; }
        public decimal tarjetaC { get; set; }
        public decimal transaccion { get; set; }
        public string nombre { get; set; }
        public string BcE_NombreBancoEmisor { get; set; }
        public string MPP_NoDocumento { get; set; }
        public int MPP_Rechazado { get; set; }
    }
}
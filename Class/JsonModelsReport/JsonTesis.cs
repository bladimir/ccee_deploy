﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonTesis
    {
        public int colegiado { get; set; }
        public string titulo { get; set; }
        public string universidad { get; set; }
        public string nivel { get; set; }
        public string tema { get; set; }
        public string evaluacion { get; set; }
        public DateTime? tempFecha { get; set; }
        public string fecha {
            get
            {
                return ((tempFecha == null) ? "" : tempFecha.Value.ToString("dd/MM/yyyy"));
            }
        }
    }
}
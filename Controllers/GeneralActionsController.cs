﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webcee.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels._Tools;
using webcee.Class.Prints;
using webcee.Models;
using webcee.ViewModels;

namespace webcee.Controllers
{
    public class GeneralActionsController : Controller
    {

        private readonly string apiKeyMaps;
        private CCEEEntities db = new CCEEEntities();

        public GeneralActionsController()
        {
            apiKeyMaps = ConfigurationManager.AppSettings["googlemaps:IdApi"];
        }

        // GET: GeneralActions
        public ActionResult Index()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, "") == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            return View();
        }


        public ActionResult TipeReferee()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_tipo_col) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult Typespecialty()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_tipo_esp) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult TypeBeneficiary()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_tipo_ben) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult University()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_universidad) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult Language()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_lenguaje) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult Country()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_expediente_pais) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult Department()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_expediente_departamento) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult Municipality()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_expediente_municipio) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult PaymentCenter()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_centro_pago) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult Cash()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_caja) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }


        public ActionResult BanckAccounts()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_cuenta_banc) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }


        public ActionResult Headquarters()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_sede) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult TestDocument()
        {
            //if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_sede) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult TimbreChange()
        {
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_cambio_timbre) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            @ViewBag.idCahier = login.idCashier;
            @ViewBag.idCash = login.idCash;
            return View();
        }

        public ActionResult Cellar()
        {
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_bodega) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            @ViewBag.idCahier = login.idCashier;
            @ViewBag.idCash = login.idCash;
            return View();
        }

        public ActionResult TimbresOfDemand()
        {
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_timbre) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            @ViewBag.idCahier = login.idCashier;
            @ViewBag.idCash = login.idCash;
            var openClose = (from ope in db.ccee_AperturaCierre
                             join caj in db.ccee_Cajero on ope.fk_Cajero equals caj.Cjo_NoCajero
                             where ope.ApC_NoAperturaCierre == login.idOpenClose
                             select new
                             {
                                 timeI = caj.Cjo_HoraInicia,
                                 timeF = caj.Cjo_HoraFin,
                                 document = ope.ApC_NoRecibo
                             }).FirstOrDefault();

            DateTime now = DateTime.Now;

            if(openClose == null)
            {
                return RedirectToAction("Index", "Referre");
            }

            if (openClose.timeF != null && openClose.timeI != null)
            {
                string[] parseTimeI = openClose.timeI.Split(':');
                int timeIHour;
                string[] parseTimeF = openClose.timeF.Split(':');
                int timeFHour;
                try
                {
                    timeIHour = Convert.ToInt32(parseTimeI[0]);
                    timeFHour = Convert.ToInt32(parseTimeF[0]);
                    int timeHour = now.Hour;
                    //int timeHour = 10;
                    if (!(timeIHour <= timeHour && timeFHour >= timeHour))
                    {
                        return RedirectToAction("Index", "Referre");
                    }
                }
                catch (Exception)
                {
                    return RedirectToAction("Index", "Referre");
                }

            }
            @ViewBag.receipt = openClose.document;

            if(login.idCashier == 0)
            {
                return RedirectToAction("Index", "Referre");
            }


            return View();
        }

        public ActionResult PruebarPrint(int? id)
        {
            //if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews) == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);

            ccee_TipoDocContable tip = db.ccee_TipoDocContable.Find(5);
            ccee_DocContable doc = db.ccee_DocContable.Find(id);
            string html = doc.DCo_Xml;
            return File(GeneralFunction.GetPDFRecibo(html), Constant.http_content_header_json);
        }


        public ActionResult OpenCloseCash()
        {
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_caja) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            @ViewBag.idCahier = login.idCashier;
            @ViewBag.idCash = login.idCash;
            return View();
        }

        public int OpenCloseCashDefineCash(int id)
        {
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            login.idCash = id;
            Session[Constant.session_user] = login;
            return id;
        }

        public ActionResult RejectedCheck()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_cheque) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult ValueTimbre()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_valor_timb) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult RefereeReceipt()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_historial_recibo) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult ReceiptPayReferee()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_historial_recibo) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult ListedDocuments()
        {
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_listado_doc) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            @ViewBag.idCahier = login.idCashier;
            return View();
        }

        public ActionResult GlobalVars()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_var_global) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult DocumentForReceipt()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_documentos_rec) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult DiplomaNewReferee()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_diploma_nuevo) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult HeadquartersMaps(int? id)
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_sede) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            try
            {

                if (id == null) return View();

                ccee_Sede tempHeadquarter = db.ccee_Sede.Find(id);
                if (tempHeadquarter == null) return View();

                string[] coordinated = tempHeadquarter.Sed_Coordenadas.Split(',');


                ViewBag.idApi = this.apiKeyMaps;
                ViewBag.lat = coordinated[0];
                ViewBag.log = coordinated[1];
                ViewBag.title = "'"+tempHeadquarter.Sed_Direccion+"'";
                return View();
            }
            catch (Exception)
            {
                return View();
            }
            
        }


        public string GlobalPrint()
        {
            List<VarsJson> vars = null;
            string connectionString = ConfigurationManager.ConnectionStrings["CCEEConnectionReport"].ConnectionString;
            
            ccee_TipoDoc tem = db.ccee_TipoDoc.Find(4);
            string text = tem.TDC_XmlPlantilla;
            JObject jObject = JObject.Parse(text);
            JObject jTamanio = (JObject) jObject["tamanio"];
            JArray variables = (JArray)jObject["data"]["variables"];
            string textDocument = (string)jObject["documento"];
            textDocument = GeneralFunction.Base64Decode(textDocument);
            vars = createVarsGlobalPrint(variables);

            JArray jQuerys = (JArray)jObject["data"]["consultas"];
            foreach (JObject dataQuery in jQuerys)
            {
                JArray listContainers = (JArray)dataQuery["contedores"];
                string queryString = (string)dataQuery["consulta"];
                queryString = GeneralFunction.Base64Decode(queryString);
                List<QueryConteinerJson> queryCont = createQueryConteinerGlobalPrint(listContainers);
                using (SqlConnection connection = new SqlConnection(connectionString))
                {

                    try
                    {
                        SqlCommand command = new SqlCommand(queryString, connection);
                        command.Connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {

                            for (int i = 0; i < queryCont.Count; i++)
                            {
                                textDocument = textDocument.Replace(getTextRemplaceHTML(vars, queryCont[i].conteiner), Convert.ToString(reader[queryCont[i].column]));
                            }
                            
                        }
                        reader.Close();
                    }
                    catch (Exception)
                    {
                        return null;
                    }

                }
            }

            jQuerys = (JArray)jObject["data"]["consultasTabla"];
            foreach (JObject dataQuery in jQuerys)
            {
                JArray listContainers = (JArray)dataQuery["contedores"];
                string queryString = (string)dataQuery["consulta"];
                queryString = GeneralFunction.Base64Decode(queryString);
                List<QueryConteinerJson> queryCont = createQueryConteinerGlobalPrint(listContainers);
                string textContainerRow = getTextRemplaceHTMLRow(vars, (string)dataQuery["contedor"]);
                textContainerRow = GeneralFunction.Base64Decode(textContainerRow);
                int maxRows = (int)dataQuery["maximo"];
                string textTemp = "";
                

                using (SqlConnection connection = new SqlConnection(connectionString))
                {

                    try
                    {
                        
                        SqlCommand command = new SqlCommand(queryString, connection);
                        command.Connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            string textConteiner = textContainerRow;

                            for (int i = 0; i < queryCont.Count; i++)
                            {
                                textConteiner = textConteiner.Replace(getTextRemplaceHTML(vars, queryCont[i].conteiner), Convert.ToString(reader[queryCont[i].column]));
                            }

                            textTemp += textConteiner;
                        }
                        reader.Close();
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                    textDocument = textDocument.Replace(getTextRemplaceHTML(vars, (string)dataQuery["contedor"]), textTemp);
                }
            }


            return textDocument;
        }


        private List<VarsJson> createVarsGlobalPrint(JArray jArray)
        {
            List<VarsJson> tempoVars = new List<VarsJson>();
            foreach (JObject contenJson in jArray)
            {
                tempoVars.Add(new VarsJson() {
                    content = (string)contenJson["contedor"],
                    isRow = (int)contenJson["esFila"],
                    tag = (string)contenJson["etiqueta"],
                    textRow = (string)contenJson["textoFila"]
                });
            }
            return tempoVars;
        }

        private List<QueryConteinerJson> createQueryConteinerGlobalPrint(JArray jArray)
        {
            List<QueryConteinerJson> tempoVars = new List<QueryConteinerJson>();
            foreach (JObject contenJson in jArray)
            {
                tempoVars.Add(new QueryConteinerJson() {

                    conteiner = (string)contenJson["contedor"],
                    position = (int)contenJson["posicion"],
                    column = (string)contenJson["column"]

                });
            }
            return tempoVars;
        }

        private string getTextRemplaceHTML(List<VarsJson> vars, string varContent)
        {
            for (int i = 0; i < vars.Count(); i++)
            {
                if(vars[i].content == varContent)
                {
                    return vars[i].tag;
                }
            }
            return "";
        }

        private string getTextRemplaceHTMLRow(List<VarsJson> vars, string varContent)
        {
            for (int i = 0; i < vars.Count(); i++)
            {
                if (vars[i].content == varContent)
                {
                    return vars[i].textRow;
                }
            }
            return "";
        }


        private void conexReaderSQL(string query)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["CCEEConnectionReport"].ConnectionString;

            string queryString = query;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                try
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    string texto = "";
                    while (reader.Read())
                    {

                        texto += "Prueba " + reader["TDC_Nombre"];
                    }
                    reader.Close();
                }
                catch (Exception)
                {
                    
                }

            }



            // Provide the query string with a parameter placeholder.
            //string queryString =
            //    "SELECT * FROM dbo.ccee_TipoDoc;";
            


            //using (SqlConnection connection = new SqlConnection(connectionString))
            //{
            //    SqlCommand command = new SqlCommand(queryString, connection);
            //    command.Connection.Open();
            //    SqlDataReader reader = command.ExecuteReader();
            //    try
            //    {
            //        string texto = "";
            //        while (reader.Read())
            //        {

            //            texto += "Prueba " + reader["TDC_Nombre"];
            //        }
            //    }
            //    finally
            //    {
            //        // Always call Close when done reading.
            //        reader.Close();
            //    }
            //}

            
        }




    }
}
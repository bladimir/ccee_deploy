﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonCorteCajaCheque
    {
        public string noDocumento { get; set; }
        public string banco { get; set; }
        private decimal tempMonto { get; set; }
        public decimal monto {
            get
            {
                return Math.Round(tempMonto, 2);
            }
            set { tempMonto = value; }
        }
        public int? esNomina { get; set; }
    }
}
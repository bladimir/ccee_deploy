﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Web.Http;
using webcee.Class;
using webcee.Class.JsonModels.Accounting;
using webcee.Class.JsonModels.Referee;
using webcee.Models;

namespace webcee.Controllers
{
    public class NewPassController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();
        // GET: api/NewPass
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpPost]
        [Route("api/ChangePass")]
        public ResponseChangeMailJson ChangePass(RequestChangeMailJson data)
        {
            try
            {
                //Busca al colegiado 
                var colegiado = db.ccee_Colegiado.Where(x => x.Col_NoColegiado == data.idReferee).FirstOrDefault();

                if (colegiado != null)
                {
                    colegiado.Col_Password = GeneralFunction.MD5Encrypt(data.newPassword);
                    db.Entry(colegiado).State = EntityState.Modified;
                    db.SaveChanges();

                    return new ResponseChangeMailJson()
                    {
                        result = true
                    };
                }

                return new ResponseChangeMailJson()
                {
                    result = false
                };
            }
            catch (Exception)
            {
                return new ResponseChangeMailJson()
                {
                    result = false
                };
            }
        }

        [HttpPost]
        [Route("api/ChangeMail")]
        public ResponseChangeMailJson ChangeMail(RequestChangeMailJson data)
        {
            ResponseChangeMailJson respuesta = new ResponseChangeMailJson();

            try
            {
                //Crea un nuevo registro de MAIL
                ccee_Mail nMail = new ccee_Mail();
                nMail.Mai_MailDir = data.newPassword;
                nMail.fk_Colegiado = data.idReferee;
                nMail.Mai_default = 1;

                //Agrega el nuevo registro a la BD

                db.ccee_Mail.Add(nMail);
                db.SaveChanges();

                respuesta.result = true;
                respuesta.mensaje = "Nuevo correo registrado con exito.";
                //respuesta.nMail = data.newPassword;

            }
            catch (Exception)
            {
                respuesta.result = false;
                respuesta.mensaje = "No se pudo registrar el nuevo correo, por favor intente de nuevo.";
            }

            return respuesta;
        }

        [HttpPost]
        [Route("api/RestorePassword")]
        public ResponseNPassJson Restore(RequestNPassJson data)
        {
            ResponseNPassJson respuesta = new ResponseNPassJson();

            //Realiza una busqueda del colegiado por Numero de colegiado y tipo de colegiado.
            var colegiado = db.ccee_Colegiado.Where(x => x.Col_NoColegiado == data.idRef).FirstOrDefault();

            if (colegiado != null)
            {
                //Realiza la verificacion del email ingresado
                var email = db.ccee_Mail.Where(x => x.fk_Colegiado == data.idRef).FirstOrDefault();

                if (email != null)
                {
                    //Valida que el correo registrado sea el mismo que el correo ingresado.
                    if (email.Mai_MailDir.ToLower().Equals(data.mail.ToLower()))
                    {
                        //Genera una clave temporal 
                        string TempPass = RandomString(6, false);

                        //Asigna la nueva clave al colegiado 
                        colegiado.Col_Password = GeneralFunction.MD5Encrypt(TempPass);
                        try
                        {

                            //Actualiza el registro en la BD
                            db.Entry(colegiado).State = EntityState.Modified;
                            db.SaveChanges();


                            //Envia por correo la clave temporal
                            string subject = "Recuperacion de Contraseña";
                            string body = @"<html>
                                                <body>
                                                    <p>Estimado(a) " + colegiado.Col_PrimerNombre + " " + colegiado.Col_PrimerApellido + @",</p>
                                                    <p>Gracias por utilizar el sistema de recuperación de contraseña para acceder a la plataforma virtual del <b> Colegio de Economistas, Contadores Públicos y Auditores y Administradores de Empresas de Guatemala. </b>
                                                     Por medio de este correo le enviamos una clave temporal la cual podrá modificar dentro de su perfil si así lo desea.</p>
                                                    <p>Su clave temporal es: " + TempPass + @"</p>
                            	                    </br>
                            	                    <p><b>Att. Administración del Colegio de Economistas, Contadores Públicos y Auditores y Administradores de Empresas de Guatemala</b></p>
	                                            </body>
	                                        </html>
	                                        ";
                            string to = email.Mai_MailDir.ToLower();
                            MailMessage mm = new MailMessage();
                            mm.From = new MailAddress("cceeguatemala@gmail.com");
                            mm.To.Add(to);
                            mm.Subject = subject;
                            mm.Body = body;
                            mm.IsBodyHtml = true;
                            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
                            smtp.UseDefaultCredentials = true;
                            smtp.Port = 587;
                            smtp.EnableSsl = true;
                            smtp.Credentials = new System.Net.NetworkCredential("cceeguatemala@gmail.com", "CCEE456@");
                            smtp.Send(mm);

                            respuesta.Result = true;
                            respuesta.Mensaje = "Clave temporal enviada correctamente. Verifique su bandeja de entrada.";

                        }
                        catch (Exception ex)
                        {
                            respuesta.Result = false;
                            respuesta.Mensaje = "No se pudo enviar la clave temporal, por favor intente de nuevo." + ex.Message + ex.StackTrace;
                        }

                    }
                    else
                    {
                        respuesta.Result = false;
                        respuesta.Mensaje = "El correo ingresado no coincide con el correo registrado por el colegiado.";
                    }
                }
                else
                {
                    respuesta.Result = false;
                    respuesta.Mensaje = "El colegiado no tiene ningun correo registrado.";
                }
            }
            else
            {
                respuesta.Result = false;
                respuesta.Mensaje = "La informacion ingresada no coincide con ningun colegiado registrado.";
            }

            return respuesta;
        }


        [HttpPost]
        [Route("api/Registrar")]
        public ResponseNPassJson Registrar(RequestRegisterJson data)
        {
            ResponseNPassJson respuesta = new ResponseNPassJson();

            //Verifica los datos 
            /*
                NUMERO DE COLEGIADO 
                PRIMER NOMBRE  
                SEGUNDO NOMBRE OPCIONAL VA UNIDO AL PRIMER NOMBRE Y SEPARADO POR UN ESPACIO
                PRIMER APELLIDO
                SEGUNDO APELLIDO OPCIONAL 
                TIPO DE PROFESION
                CORREO A REGISTRAR
                CONFIRMAR CORREO 
                NUMERO DE TELEFONO 
                NIT 
                FECHA DE NACIMIENTO 
                 
             */

            //Busca al colegiado por numero de colegiado y tipo de profesion 

            var colegiado = db.ccee_Colegiado.Where(x => x.Col_NoColegiado == data.idR).FirstOrDefault();


            if (colegiado == null)
            {
                respuesta.Result = false;
                respuesta.Mensaje = "El No. de colegiado es incorrecto";
                return respuesta;
            }
            //Verifica que no se haya registrado previamente, confirmando que tiene una password registrada
            if (colegiado.Col_Password != null)
            {
                if (colegiado.Col_Password.Trim().Count() != 0)
                {
                    respuesta.Result = false;
                    respuesta.Mensaje = "Este colegiado ya ha realizado el proceso de registro una vez. Dicho proceso no puede repetirse.";
                    return respuesta;
                }
            }

            //Verifica primer nombre 
            if (!(colegiado.Col_PrimerNombre.Trim().ToLower().Equals((data.FNRef).Trim().ToLower())))
            {
                respuesta.Result = false;
                respuesta.Mensaje = "El Primer Nombre no coincide con nuestros registros!";
                return respuesta;
            }
            //Verifica si viene un segundo nombre para verificar
            if (data.SNRef != null)
            {
                if (data.SNRef.Trim().Count() != 0)
                {
                    if (colegiado.Col_SegundoNombre != null)
                    {
                        if (colegiado.Col_SegundoNombre.Trim().Count() != 0)
                        {
                            if (data.SNRef.Trim().ToLower() != colegiado.Col_SegundoNombre.Trim().ToLower())
                            {
                                //Verifica unicamente que no sea igual, ya que si es igual entonces continua el flujo normalmente
                                respuesta.Result = false;
                                respuesta.Mensaje = "El Segundo Nombre no coincide con nuestros registros!";
                                return respuesta;

                            }
                        }
                    }
                }
            }


            //Verifica primer apellido y segundo apellido 
            if (!(colegiado.Col_PrimerApellido.Trim().ToLower().Equals(data.FSNRef.Trim().ToLower())))
            {
                respuesta.Result = false;
                respuesta.Mensaje = "El primer apellido no coincide con los registros";
                return respuesta;
            }

            //Verifica si viene un segundo nombre para verificar
            if (data.SSNRef != null)
                {
                    if (data.SSNRef.Trim().Count() != 0)
                    {
                        if (colegiado.Col_SegundoApellido != null)
                        {
                            if (colegiado.Col_SegundoApellido.Trim().Count() != 0)
                            {
                                if (data.SSNRef.Trim().ToLower() != colegiado.Col_SegundoApellido.Trim().ToLower())
                                {
                                    //Verifica si son diferentes
                                    respuesta.Result = false;
                                    respuesta.Mensaje = "El Segundo Apellido no coincide con nuestros registros!";
                                    return respuesta;


                                }
                            }
                        }
                    }


                }


                //Verifica si el colegiado tiene un numero de NIT Registrado 

                if (colegiado.Col_Nit == null)
                {
                    colegiado.Col_Nit = data.nitR.Trim();

                }
                else
                {
                    if (colegiado.Col_Nit.Count() == 0)
                    {

                        //Si no tiene un nit registrado, le registra el nit ingresado y de todos modos sobre ese 
                        //mismo se valida 
                        colegiado.Col_Nit = data.nitR.Trim();

                    }


                }

                //Verifica numero de nit 
                if (!(colegiado.Col_Nit.Trim().Equals(data.nitR.Trim())))
                {
                    respuesta.Result = false;
                    respuesta.Mensaje = "El No. de NIT no coincide con nuestros registros!";
                    return respuesta;
                }

                //Verifica si el colegiado tiene un CUI registrado

                if (colegiado.Col_Dpi == null)
                {
                    colegiado.Col_Dpi = data.cuiR.Trim();
                }
                else
                {
                    if (colegiado.Col_Dpi.Count() == 0)
                    {
                        colegiado.Col_Dpi = data.cuiR.Trim();

                    }
                }


                //Verifica que el DPI coincida con el ingresado 

                if (!(colegiado.Col_Dpi.Trim().Equals(data.cuiR.Trim())))
                {
                    respuesta.Result = false;
                    respuesta.Mensaje = "El CUI no coincide con los registros.";
                    return respuesta;
                }

                DateTime oDate = Convert.ToDateTime(data.datenac);
                //Verfica fecha nacimiento
                if (!(colegiado.Col_FechaNacimiento.GetValueOrDefault().ToShortDateString().Equals(oDate.ToShortDateString())))
                {
                    respuesta.Result = false;
                    respuesta.Mensaje = "La fecha de nacimiento no coincide con los registros.";
                    return respuesta;
                }
                try
                    {
                        //Si cumple con todo Registra el correo 
                        ccee_Mail newmail = new ccee_Mail();
                        newmail.Mai_MailDir = data.mailReferee;
                        newmail.fk_Colegiado = colegiado.Col_NoColegiado;
                        newmail.Mai_default = 1;


                        db.ccee_Mail.Add(newmail);

                        try
                        {
                            if (data.telR != null && !data.telR.Equals(""))
                            {
                                try
                                {
                                    ccee_Telefono temp = new ccee_Telefono();
                                    int num = Int32.Parse(data.telR);
                                    temp.Tel_NoTelefono = num;
                                    temp.Tel_CodigoPais = 502;
                                    temp.fk_Colegiado = colegiado.Col_NoColegiado;
                                    db.ccee_Telefono.Add(temp);
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                        catch (Exception)
                        {

                        }


                        string TempPass = RandomString(6, false);

                        //Asigna la nueva clave al colegiado 
                        colegiado.Col_Password = GeneralFunction.MD5Encrypt(TempPass);
                        //Actualiza el registro en la BD
                        db.Entry(colegiado).State = EntityState.Modified;
                        db.SaveChanges();


                        //Envia por correo la clave temporal
                        string subject = "Registro Exitoso, CCEE de Guatemala";
                        string body = @"<html>
                                                <body>
                                                    <p>Estimado(a) " + colegiado.Col_PrimerNombre + " " + colegiado.Col_PrimerApellido + @",</p>
                                                    <p>Gracias por utilizar la plataforma virtual del <b> Colegio de Economistas, Contadores Públicos y Auditores y Administradores de Empresas de Guatemala. </b>
                                                     Por medio de este correo le enviamos una clave temporal la cual podrá modificar dentro de su perfil si así lo desea.</p>
                                                    <p>Su clave temporal es: " + TempPass + @"</p>
                                                    </br>
                            	                    <p><b>Att. Administración del Colegio de Economistas, Contadores Públicos y Auditores y Administradores de Empresas de Guatemala</b></p>
	                                            </body>
	                                        </html>
	                                        ";
                        string to = newmail.Mai_MailDir;
                        MailMessage mm = new MailMessage();
                        mm.From = new MailAddress("cceeguatemala@gmail.com");
                        mm.To.Add(to);
                        mm.Subject = subject;
                        mm.Body = body;
                        mm.IsBodyHtml = true;
                        SmtpClient smtp = new SmtpClient("smtp.gmail.com");
                        smtp.UseDefaultCredentials = true;
                        smtp.Port = 587;
                        smtp.EnableSsl = true;
                        smtp.Credentials = new System.Net.NetworkCredential("cceeguatemala@gmail.com", "CCEE456@");
                        smtp.Send(mm);

                        respuesta.Result = true;
                        respuesta.Mensaje = "Clave temporal enviada correctamente. Verifique su bandeja de entrada.";


                    }
                    catch (Exception)
                    {
                        respuesta.Result = false;
                        respuesta.Mensaje = "No se pudo realizar el registro, por favor vuelva a intentarlo.";
                    }

            return respuesta;
        }

        public string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }



        // GET: api/NewPass/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/NewPass
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/NewPass/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/NewPass/5
        public void Delete(int id)
        {
        }
    }
}

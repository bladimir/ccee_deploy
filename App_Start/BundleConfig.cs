﻿using System.Web;
using System.Web.Optimization;

namespace webcee
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/moment.min.js",
                      "~/Scripts/bootstrap-datetimepicker.min.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                      "~/Scripts/angular.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/sorter/style.css",
                      "~/Content/bootstrap-datetimepicker.css",
                      "~/Content/angular-material.min.css",
                      "~/Content/angular-material.layouts.min.css",
                      "~/Content/Site.css"));

            bundles.Add(new ScriptBundle("~/bundles/angularmaterial").Include(
                      "~/Scripts/angular-material/angular-material.min.js",
                      "~/Scripts/angular-animate/angular-animate.min.js",
                      "~/Scripts/angular-aria/angular-aria.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/containerPayAngular").Include(
                      "~/Scripts/moduleAngular/payment.js",
                      "~/Scripts/moduleAngular/constant/PaymentConstant.js",
                      "~/Scripts/moduleAngular/fatories/Payment/comunicationFactory.js",
                      "~/Scripts/moduleAngular/fatories/Payment/paymentFactory.js",
                      "~/Scripts/bootstrap-datetimepicker.min.js",
                      "~/Scripts/angular-base64.min.js",
                      "~/Scripts/ngclipboard.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/containerReportAngular").Include(
                      "~/Scripts/moduleAngular/report.js",
                      "~/Scripts/moduleAngular/constant/ReportConstant.js",
                      "~/Scripts/ng-csv.min.js",
                      "~/Scripts/angular-sanitize.min.js",
                      "~/Scripts/moduleAngular/fatories/Report/reportFunctionFactory.js",
                      "~/Scripts/moduleAngular/fatories/Report/reportFactory.js"));

            bundles.Add(new ScriptBundle("~/bundles/containerGeneralActionsAngular").Include(
                      "~/Scripts/moduleAngular/generalactions.js",
                      "~/Scripts/moduleAngular/constant/GeneralActionsConstant.js",
                      "~/Scripts/moduleAngular/fatories/GeneralActions/generalactionsFactory.js",
                      "~/Scripts/ng-csv.min.js",
                      "~/Scripts/angular-sanitize.min.js",
                      "~/Scripts/bootstrap-datetimepicker.min.js",
                      "~/Scripts/angular-base64-upload.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/containerDocumentAngular").Include(
                      "~/Scripts/moduleAngular/document.js",
                      "~/Scripts/moduleAngular/constant/DocumentConstant.js",
                      "~/Scripts/moduleAngular/fatories/Document/documentFactory.js",
                      "~/Scripts/ng-file-upload-shim.min.js",
                      "~/Scripts/angular-recaptcha.min.js",
                      "~/Scripts/ng-file-upload.min.js",
                      "~/Scripts/angular-base64-upload.min.js",
                      "~/Scripts/ui-bootstrap-tpls-1.3.3.js",
                      "~/Scripts/angular-touch.min.js",
                      "~/Scripts/angular-route.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/containerUserProfileAngular").Include(
                      "~/Scripts/moduleAngular/document.js",
                      "~/Scripts/moduleAngular/constant/DocumentConstant.js",
                      "~/Scripts/moduleAngular/fatories/Document/documentFactory.js",
                      "~/Scripts/moduleAngular/controller/Document/UserPController.js",
                      "~/Scripts/moduleAngular/modals/documentModal.js",
                      "~/Scripts/ng-file-upload-shim.min.js",
                      "~/Scripts/angular-recaptcha.min.js",
                      "~/Scripts/ng-file-upload.min.js",
                      "~/Scripts/angular-base64-upload.min.js",
                      "~/Scripts/ui-bootstrap-tpls-1.3.3.js",
                      "~/Scripts/angular-touch.min.js",
                      "~/Scripts/angular-route.min.js"));


            bundles.Add(new ScriptBundle("~/bundles/containerUserAngular").Include(
                      "~/Scripts/moduleAngular/user.js",
                      "~/Scripts/moduleAngular/constant/UserConstant.js",
                      "~/Scripts/moduleAngular/fatories/User/userFactory.js"));

            bundles.Add(new ScriptBundle("~/bundles/containerRefereeAngular").Include(
                      "~/Scripts/moduleAngular/referee.js",
                      "~/Scripts/moduleAngular/constant/RefereeConstant.js",
                      "~/Scripts/moduleAngular/fatories/Referee/refereeFactory.js",
                      "~/Scripts/moduleScripts/interfaceDatePiker.js"));

        }
    }
}

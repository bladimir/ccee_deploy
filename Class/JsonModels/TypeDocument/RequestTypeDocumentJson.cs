﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.TypeDocument
{
    public class RequestTypeDocumentJson
    {
        public string name { get; set; }
        public string description { get; set; }
        public string templateXML { get; set; }
        public int consumable { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.ValueTimbre
{
    public class ResponseValueTimbreJson
    {
        public int id { get; set; }
        public string name { get; set; }
        public decimal? denomination { get; set; }

        //Valores para manejo en Angular JS
        public string nameInput { get; set; }
        public string textDemand { get; set; }
        public int isDisable { get; set; }
    }
}
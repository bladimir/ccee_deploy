﻿(function () {
    angular.module('reports')
        .controller('listadoGeneralFiltro', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'reportFactory', function ($scope, $http, CONSTANT, $anchorScroll, reportFactory) {
            $scope.direction;
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquaters;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.regexReferee = CONSTANT.REGEX_REFEREE;
            $scope.listDataCol = [];
            $scope.listDataColG = [];
            $scope.currentPage = 0;
            $scope.pageSize = 10;
            $scope.pages = [];
            $scope.groupReferee = false;
            $scope.listDepartament;
            
            $scope.check1 = true;
            $scope.check2 = true;
            $scope.check3 = true;
            $scope.check4 = true;
            $scope.check5 = true;
            $scope.check6 = true;
            $scope.check7 = true;
            $scope.check8 = true;
            $scope.check9 = true;
            $scope.check10 = true;
            $scope.check11 = true;
            $scope.check12 = true;
            $scope.check13 = true;
            $scope.check14 = true;
            $scope.check15 = true;
            $scope.check16 = true;
            $scope.check17 = true;
            $scope.check18 = true;
            $scope.check19 = true;
            $scope.check20 = true;
            $scope.check21 = true;
            $scope.check22 = true;
            $scope.check23 = true;
            $scope.check24 = true;
            $scope.check25 = true;
            $scope.check26 = true;
            $scope.check30 = true;
            $scope.check31 = true;
            $scope.check32 = true;
            $scope.check33 = true;
            $scope.check34 = true;
            $scope.check35 = true;
            $scope.check36 = true;
            $scope.check37 = true;
            $scope.check38 = true;
            $scope.check39 = true;
            $scope.check40 = true;
            $scope.check41 = true;
            $scope.check42 = true;
            $scope.check90 = true;
            $scope.totalListData = 0;
            var nameUSer = "";
            $scope.listDataLoad = [];

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.identical = function (actual, expected) {
                return actual === parseInt(expected);
            }

            $scope.init = function (nameUser) {
                this.nameUSer = nameUser;
                console.log(nameUser);
                reportFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        reportFactory.setAuthorization(response.data);
                        $scope.initF();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.initF = function () {
                $scope.getTypeReferee();
                $scope.getHeadquarter();
                $scope.getUniversity();
                $scope.getTypeEspeciality();
                $scope.getCountry();
            }

            $scope.getUniversity = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_UNIVERSITY)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listUniversity = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getTypeReferee = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_TYPEREFEREE)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listTypeReferee = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getHeadquarter = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_HEADQUARTERS)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listHeadquarter = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getTypeEspeciality = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_TYPESPECIALTY)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listTypeEspeciality = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getDepartament = function () {
                $scope.showLoadingFunction();
                reportFactory.actionGetIntFunction(CONSTANT.URL_API_DEPARTAMENT, $scope.selectCountry.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listDepartament = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getCountry = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_COUNTRY)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listCountry = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.getSelectListType = function () {
                var listActivo = [1, 3, 7, 8, 14, 15, 16];
                /*if ($scope.inActive != "") {
                    $scope.inTypeReferee = [];
                    for (var i = 0; i < listActivo.length; i++) {
                        for (var j = 0; j < $scope.listTypeReferee.length; j++) {
                            if (listActivo[i] == $scope.listTypeReferee[j].idTypeReferee) {
                                $scope.inTypeReferee.push($scope.listTypeReferee[j]);
                            }
                        }
                    }
                }*/
            }

            $scope.setSelectList = function () {

                var listActivo = [1, 3, 7, 8, 14, 15, 16];
                if ($scope.inActive == "" || $scope.inActive == null) {
                    return $scope.getDataSelectType($scope.inTypeReferee);
                } else {
                    var listTotal = "";
                    if ($scope.inTypeReferee != null && $scope.inTypeReferee.length > 0) {
                        for (var i = 0; i < $scope.inTypeReferee.length; i++) {
                            for (var j = 0; j < listActivo.length; j++) {
                                if ($scope.inTypeReferee[i].idTypeReferee == listActivo[j]) {
                                    if (listTotal === "") {
                                        listTotal += listActivo[j];
                                    } else {
                                        listTotal += "," + listActivo[j];
                                    }
                                }
                            }
                        }
                        if (listTotal === "") return "1,3,7,8,14,15,16";
                        return listTotal;
                    }
                    return "1,3,7,8,14,15,16";
                }
            }


            $scope.getDataSelect = function (list) {
                if (list == null) return "";
                var len = list.length;

                var listTotal = "";
                for (var i = 0; i < len; i++) {
                    listTotal += list[i].id + ((len > 0 && i < len - 1) ? "," : "");
                }
                return listTotal;
            }

            $scope.getDataSelectType = function (list) {
                if (list == null) return "";
                var len = list.length;

                var listTotal = "";
                for (var i = 0; i < len; i++) {
                    listTotal += list[i].idTypeReferee + ((len > 0 && i < len - 1) ? "," : "");
                }
                return listTotal;
            }

            $scope.getListRecrusive = function (position) {
                var length = $scope.listDataLoad.length;
                var countList = 0;
                var list = "";
                for (var i = position; i < length - 1 && countList < 100; i++) {
                    countList++;

                }
            }


            $scope.getListCol = function () {
                $scope.showLoadingFunction();
                $scope.listDataCol = []; 
                var information = {
                    idUniversity: $scope.getDataSelect($scope.inUniversity),
                    idTypeReferee: $scope.setSelectList(),
                    idSede: $scope.getDataSelect($scope.inHead),
                    idTypeSpecialisti: $scope.getDataSelect($scope.inSpecial),
                    status: (($scope.inActive == "2") ? "" : $scope.inActive),
                    orderSearch: $scope.inOrder,
                    edad: $scope.inAge,
                    year: $scope.inYear,
                    academy: $scope.academyspeciality,
                    referees: $scope.inRefe,
                    typeAcademi: $scope.academyEvaluation,
                    groupReferee: $scope.groupReferee,
                    idSedeReg: $scope.getDataSelect($scope.inHeadReg),
                    departament: $scope.getDataSelect($scope.selectDepartament),
                    typeDir: $scope.typeDirection,
                    loadMasive: $scope.idloadmasive,

                    check1: $scope.check1,
                    check2: $scope.check2,
                    check3: $scope.check3,
                    check4: $scope.check4,
                    check5: $scope.check5,
                    check6: $scope.check6,
                    check7: $scope.check7,
                    check8: $scope.check8,
                    check9: $scope.check9,
                    check10: $scope.check10,
                    check11: $scope.check11,
                    check12: $scope.check12,
                    check13: $scope.check13,
                    check14: $scope.check14,
                    check15: $scope.check15,
                    check16: $scope.check16,
                    check17: $scope.check17,
                    check18: $scope.check18,

                    check22: $scope.check22,
                    check23: $scope.check23,
                    check24: $scope.check24,
                    check25: $scope.check25,
                    check26: $scope.check26,
                    check27: $scope.check27,
                    check28: $scope.check28,
                    check29: $scope.check29,
                    check30: $scope.check30,
                    check31: $scope.check31,
                    check32: $scope.check32,
                    check36: $scope.check36,
                    check42: $scope.check42
                }
                console.log(information);
                reportFactory.actionPostFunction(CONSTANT.URL_GET_LISTGENERAL, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            $scope.listDataCol = response.data;
                            $scope.createListData($scope.listDataCol);
                            $scope.currentPage = 0; 
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }


            $scope.createListData = function (listd) {
                $scope.listDataColG = [];
                var len = listd.length;

                
                $scope.listDataColG.push({
                    Col_NoColegiado : 'Colegio de Profesionales de Ciencias Económicas, Listado General'
                });

                var dd = {};

                if ($scope.check1) {
                    dd.Col_NoColegiado = "No.";
                }
                if ($scope.check2) {
                    dd.Col_PrimerApellido = "Primer ape.";
                }
                if ($scope.check3) {
                    dd.Col_SegundoApellido = "Segundo ape.";
                }
                if ($scope.check34) {
                    dd.Col_CasdaApellido = "Casada ape.";
                }
                if ($scope.check4) {
                    dd.Col_PrimerNombre = "Primer nom.";
                }
                if ($scope.check5) {
                    dd.Col_SegundoNombre = "Segundo nom.";
                }
                if ($scope.check35) {
                    dd.Col_TercerNombre = "Tercer nom.";
                }
                if ($scope.check8) {
                    dd.nacimiento = "Nacimiento";
                }
                if ($scope.check13) {
                    dd.Col_FechaColegiacion = "Fecha cole.";
                }
                if ($scope.check8) {
                    dd.activo = "Activo";
                }
                if ($scope.check12) {
                    dd.sexo = "Sexo";
                }
                if ($scope.check14) {
                    dd.fk_Sede = "Sede juramentación";
                }
                if ($scope.check18) {
                    dd.fk_SedeRegistrada = "Sede registrada";
                }
                if ($scope.check9) {
                    dd.Tco_Descripcion = "Descripción";
                }
                if ($scope.check7) {
                    dd.estadoCivil = "Estado civil";
                }
                if ($scope.check6) {
                    dd.Dir_TipoDireccion = "Tipo dir.";
                }
                if ($scope.check10) {
                    dd.TEs_NombreTipoEsp = "Título";
                }
                if ($scope.check11) {
                    dd.Uni_NombreUniversidad = "Universidad";
                }
                if ($scope.check16) {
                    dd.Esp_NombreEspecialidad = "Grado Académico";
                }
                if ($scope.check19) {
                    dd.Esp_Tema_Graduacion = "Tema de Graduación";
                }
                if ($scope.check20) {
                    dd.Esp_Evaluacion = "Evaluación";
                }
                if ($scope.check21) {
                    dd.phone = "Telefonos";
                }
                if ($scope.check22) {
                    dd.mails = "Correos";
                }

                //if ($scope.check23) {
                //    dd.TEs_NombreTipoEsp2 = "Título";
                //}
                if ($scope.check24) {
                    dd.Esp_FechaObtuvo = "Fecha de graduación";
                }
                if ($scope.check25) {
                    dd.Esp_Evaluacion = "Tipo de graduación";
                }
                if ($scope.check26) {
                    dd.Uni_NombreUniversidadabre = "Abreviatura Univ.";
                }
                if ($scope.check30) {
                    dd.Col_Dpi = "DPI";
                }
                if ($scope.check31) {
                    dd.Col_jubilado = "Fecha jubilación";
                }
                if ($scope.check32) {
                    dd.Col_FechaFallecimiento = "Fecha fallecimiento";
                }
                if ($scope.check33) {
                    dd.Col_FechaJuramentacion = "Fecha juramentación";
                }
                if ($scope.check36) {
                    dd.TES_Abreviacion = "Titulo abre.";
                }
                if ($scope.check37) {
                    dd.Edad = "Edad";
                }
                if ($scope.check38) {
                    dd.Col_Nacionalidad = "Nacionalidad";
                }
                if ($scope.check39) {
                    dd.Col_DpiExtendido = "Extendido en";
                }
                if ($scope.check40) {
                    dd.Col_Experiencia_en = "Experiencia en";
                }
                if ($scope.check41) {
                    dd.idiomas = "idiomas";
                }
                if ($scope.check42) {
                    dd.Esp_Titulo = "Especialidad";
                }
                if ($scope.check90) {
                    dd.direccion = "Direccion";
                }
                
                $scope.listDataColG.push(dd);

                for (var i = 0; i < len; i++) {
                    var dd = {};
                    
                    if ($scope.check1) {
                        dd.Col_NoColegiado = listd[i].Col_NoColegiado;
                    }
                    if ($scope.check2) {
                        dd.Col_PrimerApellido = listd[i].Col_PrimerApellido;
                    }
                    if ($scope.check3) {
                        dd.Col_SegundoApellido = listd[i].Col_SegundoApellido;
                    }
                    if ($scope.check34) {
                        dd.Col_CasdaApellido = listd[i].Col_CasdaApellido;
                    }
                    if ($scope.check4) {
                        dd.Col_PrimerNombre = listd[i].Col_PrimerNombre;
                    }
                    if ($scope.check5) {
                        dd.Col_SegundoNombre = listd[i].Col_SegundoNombre;
                    }
                    if ($scope.check35) {
                        dd.Col_TercerNombre = listd[i].Col_TercerNombre;
                    }
                    if ($scope.check8) {
                        dd.nacimiento = listd[i].nacimiento;
                    }
                    if ($scope.check13) {
                        dd.Col_FechaColegiacion = ((listd[i].Col_FechaColegiacion == null) ? '' : new Date(listd[i].Col_FechaColegiacion).toLocaleDateString()); //new Date(listd[i].Col_FechaColegiacion, 'dd/MM/yyyy');
                    }
                    if ($scope.check8) {
                        dd.activo = listd[i].activo;
                    }
                    if ($scope.check12) {
                        dd.sexo = listd[i].sexo;
                    }
                    if ($scope.check14) {
                        dd.fk_Sede = listd[i].fk_Sede_Nombre;
                    }
                    if ($scope.check18) {
                        dd.fk_SedeRegistrada = listd[i].fk_SedeRegistrada_Nombre;
                    }
                    if ($scope.check9) {
                        dd.Tco_Descripcion = listd[i].Tco_Descripcion;
                    }
                    if ($scope.check7) {
                        dd.estadoCivil = listd[i].estadoCivil;
                    }
                    if ($scope.check6) {
                        dd.Dir_TipoDireccion = listd[i].Dir_TipoDireccion;
                    }
                    if ($scope.check10) {
                        dd.TEs_NombreTipoEsp = listd[i].TEs_NombreTipoEsp;
                    }
                    if ($scope.check11) {
                        dd.Uni_NombreUniversidad = listd[i].Uni_NombreUniversidad;
                    }
                    if ($scope.check16) {
                        dd.Esp_NombreEspecialidad = listd[i].Esp_NombreEspecialidad;
                    }
                    if ($scope.check19) {
                        dd.Esp_Tema_Graduacion = listd[i].Esp_Tema_Graduacion;
                    }
                    if ($scope.check20) {
                        dd.Esp_Evaluacion = listd[i].Esp_Evaluacion;
                    }
                    if ($scope.check21) {
                        dd.phone = listd[i].phones;
                    }
                    if ($scope.check22) {
                        dd.mails = listd[i].mails;
                    }

                    //if ($scope.check23) {
                    //    dd.TEs_NombreTipoEsp2 = listd[i].TEs_NombreTipoEsp;
                    //}
                    if ($scope.check24) {
                        
                        dd.Esp_FechaObtuvo = ((listd[i].Esp_FechaObtuvo == null) ? '' : new Date(listd[i].Esp_FechaObtuvo).toLocaleDateString());  //toLocaleString
                    }
                    if ($scope.check25) {
                        dd.Esp_Evaluacion = listd[i].Esp_Evaluacion;
                    }
                    if ($scope.check26) {
                        dd.Uni_NombreUniversidadabre = listd[i].Uni_NombreUniversidadabre;
                    }
                    if ($scope.check30) {
                        dd.Col_Dpi = listd[i].Col_Dpi;
                    }
                    if ($scope.check31) {
                        dd.Col_jubilado = ((listd[i].Col_jubilado == null) ? '' : new Date(listd[i].Col_jubilado).toLocaleDateString()); 
                    }
                    if ($scope.check32) {
                        dd.Col_FechaFallecimiento = ((listd[i].Col_FechaFallecimiento == null) ? '' : new Date(listd[i].Col_FechaFallecimiento).toLocaleDateString());
                    }
                    if ($scope.check33) {
                        dd.Col_FechaJuramentacion = ((listd[i].Col_FechaJuramentacion == null) ? '' : new Date(listd[i].Col_FechaJuramentacion).toLocaleDateString());
                    }
                    if ($scope.check36) {
                        dd.TES_Abreviacion = listd[i].TES_Abreviacion;
                    }
                    if ($scope.check37) {
                        dd.Edad = listd[i].edad;
                    }
                    if ($scope.check38) {
                        dd.Col_Nacionalidad = ((listd[i].Col_Nacionalidad == null) ? '' : listd[i].Col_Nacionalidad);
                    }
                    if ($scope.check39) {
                        dd.Col_DpiExtendido = listd[i].Col_DpiExtendido;
                    }
                    if ($scope.check40) {
                        dd.Col_Experiencia_en = listd[i].Col_Experiencia_en;
                    }
                    if ($scope.check41) {
                        dd.idiomas = listd[i].idiomas;
                    }
                    if ($scope.check42) {
                        dd.Esp_Titulo = listd[i].Esp_Titulo;
                    }
                    if ($scope.check90) {
                        dd.direccion = listd[i].direccion;
                    }
                    
                    $scope.listDataColG.push(dd);
                }

                var dd2 = {};
                dd2.Col_FechaJuramentacion = "Total de Datos";
                dd2.direccion = $scope.listDataColG.length - 1;
                $scope.totalListData = $scope.listDataColG.length - 1;
                $scope.listDataColG.push(dd2);

                $scope.listDataColG.push({
                    Col_NoColegiado: (new Date()).toLocaleString(),
                    Col_PrimerApellido: this.nameUSer
                });

            }

            $scope.configPages = function() {
                $scope.pages.length = 0;
                var ini = $scope.currentPage - 0;
                var fin = $scope.currentPage + 0;
                if (ini < 1) {
                    ini = 1;
                    if (Math.ceil($scope.listDataCol.length / $scope.pageSize) > 10)
                        fin = 10;
                    else
                        fin = Math.ceil($scope.listDataCol.length / $scope.pageSize);
                } else {
                    if (ini >= Math.ceil($scope.listDataCol.length / $scope.pageSize) - 10) {
                        ini = Math.ceil($scope.listDataCol.length / $scope.pageSize) - 10;
                        fin = Math.ceil($scope.listDataCol.length / $scope.pageSize);
                    }
                }
                if (ini < 1) ini = 1;
                for (var i = ini; i <= fin; i++) {
                    $scope.pages.push({no: i});
                }

                if ($scope.currentPage >= $scope.pages.length)
                    $scope.currentPage = $scope.pages.length - 1;
            };

            $scope.setPage = function (index) {
                $scope.currentPage = index - 1;
            };


            $scope.generateList = function () {
                var textGetReport = "";
                console.log(($scope.selectHead != null) ? $scope.selectHead.id : "");
                textGetReport = "&universidad=" + $scope.getDataSelect($scope.inUniversity) +
                                "&type=" + $scope.setSelectList() +
                                "&head=" + $scope.getDataSelect($scope.inHead) +
                                "&special=" + $scope.getDataSelect($scope.inSpecial) +
                                "&active=" + (($scope.inActive == "2") ? "" : $scope.inActive) +
                                "&order=" + $scope.inOrder;
                console.log(textGetReport);
                $scope.direction = "../../Reports/aspxReports/ListadoGenerarColegiadoAspx.aspx?" + textGetReport;
            }

            $scope.setChecks = function (status) {
                $scope.check1 = status;
                $scope.check2 = status;
                $scope.check3 = status;
                $scope.check4 = status;
                $scope.check5 = status;
                $scope.check6 = status;
                $scope.check7 = status;
                $scope.check8 = status;
                $scope.check9 = status;
                $scope.check10 = status;
                $scope.check11 = status;
                $scope.check12 = status;
                $scope.check13 = status;
                $scope.check14 = status;
                $scope.check15 = status;
                $scope.check16 = status;
                $scope.check17 = status;
                $scope.check18 = status;
                $scope.check19 = status;
                $scope.check20 = status;
                $scope.check21 = status;
                $scope.check22 = status;

                $scope.check23 = status;
                $scope.check24 = status;
                $scope.check25 = status;
                $scope.check26 = status;
                $scope.check27 = status;
                $scope.check28 = status;
                $scope.check29 = status;
                $scope.check30 = status;
                $scope.check31 = status;
                $scope.check32 = status;
                $scope.check33 = status;
                $scope.check34 = status;
                $scope.check35 = status;
                $scope.check36 = status;
                $scope.check37 = status;
                $scope.check38 = status;
                $scope.check39 = status;
                $scope.check40 = status;
                $scope.check41 = status;
                $scope.check42 = status;
                $scope.check90 = status;
            }

            $scope.define = function () {
                $scope.showLoadingFunction();
                var information = {
                    datos: $scope.fileContent.replace(/\r\n/g, ',')
                }
                console.log(information);
                //$scope.listDataLoad = information.split(',');
                reportFactory.actionPostFunction(CONSTANT.URL_GET_TEMP_REPORT, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            console.log(response);
                            if (response.data != 0) {
                                $scope.idloadmasive = response.data;
                                $scope.loadmasive = 1;
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.diffAge = function (dateBirth) {
                console.log(dateBirth)
                console.log(new Date(dateBirth, 'dd/MM/yyyy'))
                var ageDifMs = Date.now() - (new Date(dateBirth)).getTime();
                var ageDate = new Date(ageDifMs); // miliseconds from epoch
                return Math.abs(ageDate.getUTCFullYear() - 1970);
            }


        }]);

    angular.module('reports')
        .filter('startFromGrid', function() {
            return function(input, start) {
                start = +start;
                return input.slice(start);
            }
        });

})()
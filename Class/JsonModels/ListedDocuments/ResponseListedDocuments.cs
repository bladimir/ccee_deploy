﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.ListedDocuments
{
    public class ResponseListedDocuments
    {
        public int idReferee { get; set; }
        public string address { get; set; }
        public int idDocument { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_UserController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_User
        public IEnumerable<DataUserJson> Getccee_Usuario()
        {
            string autorization = Request.Headers.Authorization.Parameter;
            if (!Autorization.enableAutorization(db, autorization)) return null;
            Autorization.saveOperation(db, autorization, "Obteniendo usuarios");
            return getListUsers();
        }

        // GET: api/Api_User/5
        public DataUserJson Getccee_Usuario(int id)
        {
            string autorization = Request.Headers.Authorization.Parameter;
            if (!Autorization.enableAutorization(db, autorization)) return null;
            ccee_Usuario ccee_Usuario = db.ccee_Usuario.Find(id);
            if (ccee_Usuario == null)
            {
                return null;
            }
            DataUserJson contentUsers = new DataUserJson()
            {
                id = ccee_Usuario.Usu_NoUsuario,
                nombre = ccee_Usuario.Usu_Nombre,
                apellido = ccee_Usuario.Usu_Apellido,
                correo = ccee_Usuario.Usu_Correo,
                telefonoMovil = ccee_Usuario.Usu_TelMovil,
                telefonoResidencial = ccee_Usuario.Usu_TelResidencial,
                question = ccee_Usuario.Usu_PreguntaClave,
                answer = ccee_Usuario.Usu_RespuestaClave
            };

            Autorization.saveOperation(db, autorization, "Obteniendo usuario");
            return contentUsers;
        }

        // PUT: api/Api_User/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putccee_Usuario(int id, DataUserJson usuario)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_Usuario tempUser = db.ccee_Usuario.Find(id);

                if (tempUser == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempUser.Usu_Nombre = usuario.nombre;
                tempUser.Usu_Apellido = usuario.apellido;
                tempUser.Usu_TelMovil = usuario.telefonoMovil;
                tempUser.Usu_TelResidencial = usuario.telefonoResidencial;
                tempUser.Usu_Per_NoPerfil = ((usuario.idProfile == 0) ? tempUser.Usu_Per_NoPerfil : usuario.idProfile);
                
                if(usuario.isAdmin == true && usuario.newPass != null && !usuario.newPass.Equals(""))
                {
                    tempUser.Usu_Clave = GeneralFunction.MD5Encrypt(usuario.newPass);
                }

                db.Entry(tempUser).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando usuario");
                db.SaveChanges();

                if (usuario.question != null)
                {
                    if (!usuario.question.Equals(""))
                    {
                        tempUser.Usu_PreguntaClave = usuario.question;
                        tempUser.Usu_RespuestaClave = usuario.answer;
                        db.Entry(tempUser).State = EntityState.Modified;
                        Autorization.saveOperation(db, autorization, "Actualizando pregunta secreta");
                        db.SaveChanges();
                    }
                }


                if(usuario.oldPass != null)
                {
                    if (!usuario.oldPass.Equals(""))
                    {
                        string md5OldPass = GeneralFunction.MD5Encrypt(usuario.oldPass);
                        if (md5OldPass.Equals(tempUser.Usu_Clave))
                        {
                            tempUser.Usu_Clave = GeneralFunction.MD5Encrypt(usuario.newPass);
                            db.Entry(tempUser).State = EntityState.Modified;
                            Autorization.saveOperation(db, autorization, "Actualizando contraseña");
                            db.SaveChanges();
                        }
                        else
                        {
                            return Ok(HttpStatusCode.NotFound);
                        }
                    }
                }
                
                return Ok(HttpStatusCode.OK);

            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // POST: api/Api_User
        [ResponseType(typeof(ccee_Usuario))]
        public IHttpActionResult Postccee_Usuario(ccee_Usuario ccee_Usuario)
        {
            string autorization = Request.Headers.Authorization.Parameter;
            if (!Autorization.enableAutorization(db, autorization)) return null;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var existEmail = db.ccee_Usuario.Single(email => email.Usu_Correo == ccee_Usuario.Usu_Correo);
                return Ok(Constant.status_code_user_repeat);
            }
            catch(Exception)
            {
                try
                {
                    ccee_Usuario.Usu_Clave = GeneralFunction.MD5Encrypt(ccee_Usuario.Usu_Clave);
                    db.ccee_Usuario.Add(ccee_Usuario);
                    Autorization.saveOperation(db, autorization, "Agregando usuario");
                    db.SaveChanges();
                }
                catch (Exception)
                {
                    return Ok(Constant.status_code_error);
                    throw;
                }
            }
            return Ok(Constant.status_code_success);
        }

        // DELETE: api/Api_User/5
        [ResponseType(typeof(ccee_Usuario))]
        public IHttpActionResult Deleteccee_Usuario(int id)
        {
            string autorization = Request.Headers.Authorization.Parameter;
            if (!Autorization.enableAutorization(db, autorization)) return null;
            ccee_Usuario ccee_Usuario = db.ccee_Usuario.Find(id);
            if (ccee_Usuario == null)
            {
                return Ok(Constant.status_code_user_not_found);
            }

            db.ccee_Usuario.Remove(ccee_Usuario);
            Autorization.saveOperation(db, autorization, "Eliminando usuario");
            db.SaveChanges();

            return Ok(Constant.status_code_success);
        }


        private IEnumerable<DataUserJson> getListUsers()
        {

            var users = from user in db.ccee_Usuario
                        join prof in db.ccee_Perfil on user.Usu_Per_NoPerfil equals prof.Per_NoPerfil
                        select new DataUserJson()
                        {
                            id = user.Usu_NoUsuario,
                            nombre = user.Usu_Nombre,
                            apellido = user.Usu_Apellido,
                            correo = user.Usu_Correo,
                            telefonoMovil = user.Usu_TelMovil,
                            telefonoResidencial = user.Usu_TelResidencial,
                            idProfile = prof.Per_NoPerfil,
                            profile = prof.Per_Nombre
                        };

            return users;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ccee_UsuarioExists(int id)
        {
            return db.ccee_Usuario.Count(e => e.Usu_NoUsuario == id) > 0;
        }
    }
}
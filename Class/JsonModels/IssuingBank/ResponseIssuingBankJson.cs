﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.IssuingBank
{
    public class ResponseIssuingBankJson
    {
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public int? typeInstitution { get; set; }
        public string service { get; set; }
    }
}
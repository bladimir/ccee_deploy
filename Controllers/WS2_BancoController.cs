﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.JsonModels._Tools;
using webcee.Class.JsonModels.AccountingDocument;
using webcee.Class.JsonModels.AddDues;
using webcee.Class.JsonModels.Balance;
using webcee.Class.JsonModels.Economic;
using webcee.Class.JsonModels.PaymentMethod_Pay;
using webcee.Models;

namespace webcee.Controllers
{
    public class WS2_BancoController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        public class responseItem
        {
            public ResponseBalanceJson cuotasActuales { get; set; }
            public ResponseAddDuesJson cuotasProximas { get; set; }
            public string name { get; set; }
        }

        public class metodo
        {
            public decimal monto { get; set; }
            public string noTransaccion { get; set; }
            public string noDocumento { get; set; }
            public string observaciones { get; set; }
            public int idBanco { get; set; }
            public int idMetodoPago { get; set; }
        }

        public class requestPay
        {
            public responseItem items { get; set; }
            public int countColegiadoExtras { get; set; }
            public int countTimbreExtras { get; set; }
            public int colegiado { get; set; }
            public int idCajero { get; set; }
            public int idCaja { get; set; }
            public metodo metodoPago { get; set; }
        }

        public class responsePago
        {
            public int status { get; set; }
            public int idPago { get; set; }
            public string msg { get; set; }
        }

        // GET: api/WS2_Banco
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/WS2_Banco/5
        public responseItem Get(int id)
        {
            Api_BalanceController api_BalanceController = new Api_BalanceController();
            Api_AddDuesController api_AddDuesController = new Api_AddDuesController();
            ResponseBalanceJson balance = api_BalanceController.proccessBalance(id);
            ResponseAddDuesJson dues = api_AddDuesController.getDues(1, 1, id); 
            Console.WriteLine(balance);

            var nombreRef = "";
            ccee_Colegiado tempEcoReferee = db.ccee_Colegiado.Find(id);
            try {
                // Construyendo String de Nombre Completo
                nombreRef = tempEcoReferee.Col_PrimerNombre + " " +
                            (tempEcoReferee.Col_SegundoNombre != "" ? tempEcoReferee.Col_SegundoNombre + " " : "") +
                            (tempEcoReferee.Col_TercerNombre != "" ? tempEcoReferee.Col_TercerNombre + " " : "") +
                            tempEcoReferee.Col_PrimerApellido + " " +
                            (tempEcoReferee.Col_SegundoApellido != "" ? tempEcoReferee.Col_SegundoApellido + " " : "") +
                            (tempEcoReferee.Col_CasdaApellido != "" ? tempEcoReferee.Col_CasdaApellido : "");

                // Transformar de 3 a 2 decimales
                balance.referee = formatToTwoDecimalPlaces(balance.referee);
                balance.timbre = formatToTwoDecimalPlaces(balance.timbre);
                balance.postumo = formatToTwoDecimalPlaces(balance.postumo);
                balance.other = formatToTwoDecimalPlaces(balance.other);
                balance.total = formatToTwoDecimalPlaces(balance.total);
                balance.mora = formatToTwoDecimalPlaces(balance.mora);

                dues.totalC = formatToTwoDecimalPlaces(dues.totalC);
                dues.totalT = formatToTwoDecimalPlaces(dues.totalT);
            }
            catch (Exception ex) {
            }

            return new responseItem()
            {
                cuotasActuales = balance,
                cuotasProximas = dues,
                name = nombreRef
            };
        }

        private decimal formatToTwoDecimalPlaces(decimal d) {
            //return Convert.ToDecimal(string.Format("{0:F2}", d));
            return decimal.Round(d+new decimal(0.000), 2, MidpointRounding.AwayFromZero);
        }

        // POST: api/WS2_Banco
        public responsePago Post(requestPay requestpay)
        {

            try
            {
                decimal totalPay = 0;

                RequestAddDuesJson requestAddDuesJson = new RequestAddDuesJson();
                requestAddDuesJson.countC = requestpay.countColegiadoExtras;
                requestAddDuesJson.countT = requestpay.countTimbreExtras;
                requestAddDuesJson.referee = requestpay.colegiado;
                requestAddDuesJson.dateChange = null;
                Api_AddDuesController api_AddDuesController = new Api_AddDuesController();
                api_AddDuesController.addDuesPost(requestAddDuesJson);

                List<ResponseEconomicJson> lastEconomic = new List<ResponseEconomicJson>();

                Api_EconomicController api_EconomicController = new Api_EconomicController();
                List<ResponseEconomicJson> listEconomic = api_EconomicController.listEconomic(requestpay.colegiado).ToList();
                List<ResponseEconomicJson> listColegiado = listEconomic.Where(p => p.typeEconomic == 1).ToList();
                List<ResponseEconomicJson> listTimbre = listEconomic.Where(p => p.typeEconomic == 2).ToList();
                for (int i = 0; i < listColegiado.Count(); i++)
                {
                    listColegiado[i].idPay = 1;
                    listColegiado[i].enable = 1;
                    totalPay += listColegiado[i].amount;
                    lastEconomic.Add(listColegiado[i]);
                }

                for (int i = 0; i < listTimbre.Count(); i++)
                {
                    listTimbre[i].idPay = 1;
                    listTimbre[i].enable = 1;
                    totalPay += listTimbre[i].amount;
                    lastEconomic.Add(listTimbre[i]);
                }

                List<RequestPaymentMethod_PayJson> listPaymentMethod = new List<RequestPaymentMethod_PayJson>();
                listPaymentMethod.Add(new RequestPaymentMethod_PayJson()
                {
                    amount = requestpay.metodoPago.monto,
                    noTransaction = requestpay.metodoPago.noTransaccion,
                    noDocument = requestpay.metodoPago.noDocumento,
                    reservation = 1,
                    rejection = 0,
                    remarke = "",
                    idBank = requestpay.metodoPago.idBanco,
                    idPaymentMethod = requestpay.metodoPago.idMetodoPago
                });

                

                PostBodyEconomicJson postBodyEconomicJson = new PostBodyEconomicJson();
                postBodyEconomicJson.amount = totalPay;
                postBodyEconomicJson.remake = "";
                postBodyEconomicJson.idCashier = requestpay.idCajero;
                postBodyEconomicJson.idCash = requestpay.idCaja;
                postBodyEconomicJson.id = 1;
                postBodyEconomicJson.idReferee = requestpay.colegiado;
                postBodyEconomicJson.jsonContent = lastEconomic;
                postBodyEconomicJson.jsonContentMethod = listPaymentMethod;

                Api_PayController api_PayController = new Api_PayController();
                var fk_pago = api_PayController.generatePay(postBodyEconomicJson, true);
                if (fk_pago < 0)
                {
                    return new responsePago
                    {
                        status = 400,
                        idPago = 0,
                        msg = "No se puede agregar el pago"
                    };
                }

                RequestAccountingDocumentJson request_Accounting = new RequestAccountingDocumentJson()
                {
                    titleName = "",
                    address = "Guatemala",
                    nit = "",
                    remark = "Pago banca virtual",
                    idCashier = requestpay.idCajero,
                    idReferee = requestpay.colegiado,
                    idPay = fk_pago,
                    idTypeDocumentAccounting = 1,
                    NoDocumentCol = "B-" + requestpay.metodoPago.idBanco + "-" + requestpay.metodoPago.noTransaccion,
                    //NoDocumentOther = null,   
                    listIdDocs = null,
                    dateTempCol = null,
                    dateTempTim = null,
                    isReferee = true
                };
                ReferreController refereeC = new ReferreController();
                string texto = refereeC.PrintFileReferee(request_Accounting);

                if (texto == "1")
                {
                    return new responsePago
                    {
                        status = 400,
                        idPago = 0,
                        msg = "El Numero de recibo ya esta siendo usado!"
                    };
                }

                return new responsePago
                {
                    status = 200,
                    idPago = 0,
                    msg = "Pago procesado correctamente"
                };

            }
            catch (Exception e)
            {
                return new responsePago()
                {
                    status = 400,
                    idPago = 0,
                    msg = e.ToString()
                };
            }

        }

        // PUT: api/WS2_Banco/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/WS2_Banco/5
        public void Delete(int id)
        {
        }
    }
}

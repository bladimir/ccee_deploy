﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webcee.Class.JsonModels.NoReferee;
using webcee.Class.JsonModels.PaymentMethod_Pay;

namespace webcee.Class.JsonModels.UploadNomina
{
    public class RequestUploadNominaJson
    {
        public ResponseNoRefereeJson NoColegiado { get; set; }
        public int idCash { get; set; }
        public int idCashier { get; set; }
        public string document { get; set; }
        public bool enableUpdate { get; set; }
        public string idType { get; set; }
        public string NoDocument { get; set; }
        public int idBank { get; set; }
        public bool negatives { get; set; }
        public int idTypeDocument { get; set; }
        public List<ResponseUploadNominaJson> data { get; set; }
        public int idBatch { get; set; }
        public int idDocAccount { get; set; }
        public IEnumerable<RequestPaymentMethod_PayJson> jsonContentMethod { get; set; }
        public DateTime? dateOfPay { get; set; }
        public string textOfEconomic { get; set; }
    }
}
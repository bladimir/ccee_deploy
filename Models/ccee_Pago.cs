//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace webcee.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ccee_Pago
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ccee_Pago()
        {
            this.ccee_CargoEconomico_colegiado = new HashSet<ccee_CargoEconomico_colegiado>();
            this.ccee_CargoEconomico_otros = new HashSet<ccee_CargoEconomico_otros>();
            this.ccee_CargoEconomico_timbre = new HashSet<ccee_CargoEconomico_timbre>();
            this.ccee_MedioPago_Pago = new HashSet<ccee_MedioPago_Pago>();
            this.ccee_MedioPago_Pago_C = new HashSet<ccee_MedioPago_Pago_C>();
            this.ccee_Timbre = new HashSet<ccee_Timbre>();
        }
    
        public int Pag_NoPago { get; set; }
        public Nullable<System.DateTime> Pag_Fecha { get; set; }
        public int fk_Cajero { get; set; }
        public int fk_Caja { get; set; }
        public Nullable<decimal> Pag_Monto { get; set; }
        public Nullable<int> Pag_Noold { get; set; }
        public string Pag_Observacion { get; set; }
        public Nullable<short> actualizacion { get; set; }
        public Nullable<int> Pag_Congelado { get; set; }
        public Nullable<int> pag_Nooldp { get; set; }
    
        public virtual ccee_Caja ccee_Caja { get; set; }
        public virtual ccee_Cajero ccee_Cajero { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ccee_CargoEconomico_colegiado> ccee_CargoEconomico_colegiado { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ccee_CargoEconomico_otros> ccee_CargoEconomico_otros { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ccee_CargoEconomico_timbre> ccee_CargoEconomico_timbre { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ccee_MedioPago_Pago> ccee_MedioPago_Pago { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ccee_MedioPago_Pago_C> ccee_MedioPago_Pago_C { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ccee_Timbre> ccee_Timbre { get; set; }
    }
}

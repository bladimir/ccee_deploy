﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using webcee.Class.Class;
using webcee.Class.JsonModels.Bill;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_BillController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Bill
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_Bill/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_Bill
        public IHttpActionResult Post(RequestBillJson bill)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var jsonSerialiser = new JavaScriptSerializer();
                string json = jsonSerialiser.Serialize(bill.list);

                ccee_BilletesDen tempBill = new ccee_BilletesDen();
                tempBill.BiD_Fecha = bill.date;
                tempBill.BiD_JsonDenominacion = json;
                db.ccee_BilletesDen.Add(tempBill);
                Autorization.saveOperation(db, autorization, "Agregando listado de denominaciones");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);

            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_Bill/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_Bill/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}

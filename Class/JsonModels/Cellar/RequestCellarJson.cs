﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Cellar
{
    public class RequestCellarJson
    {
        public string numberPage { get; set; }
        public int initRange { get; set; }
        public int finishRange { get; set; }
        public decimal value { get; set; }
        public int idValue { get; set; }
        public int idCashier { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webcee.Class.JsonModels.DocumentAccouting;
using webcee.Class.JsonModelsReport;
using webcee.Models;

namespace webcee.Class.Class
{
    public class DocumentAccouting
    {
        private CCEEEntities db;
        public DocumentAccouting(CCEEEntities db)
        {
            this.db = db;
        }


        public List<listDocumentJson> createFile(int idBoleta)
        {
            List<listDocumentJson> listDocs = listDocument(idBoleta);
            List<listBreakdownAccountJson> breakdown = breakdownAccount();
            for (int i = 0; i < listDocs.Count; i++)
            {
                List<listEconomicJson> economic = listEconomic(listDocs[i]);
                listDocs[i] = listTupla(economic, breakdown, listDocs[i]);
            }
            return listDocs;
        }

        private List<listDocumentJson> listDocument(int idBoleta)
        {
            var listDocument = (from bol in db.ccee_BoletaDeDeposito
                               join cue in db.ccee_CuentaBancaria on bol.fk_CuentaBancaria equals cue.pk_CuentaBAncaria
                               join doc in db.ccee_DocContable on bol.BdD_NoBoletaDeDeposito equals doc.fk_BoletaDeposito
                               where bol.BdD_NoBoletaDeDeposito == idBoleta
                               select new listDocumentJson()
                               {
                                   idBoleta = bol.BdD_NoBoletaDeDeposito,
                                   codigo = cue.CtB_Codigo,
                                   noAccount = cue.CtB_NoCuenta,
                                   noDeposit = bol.BdD_NoDeposito,
                                   date = bol.BdD_Fecha,
                                   namebank = cue.CtB_Nombre,
                                   idDocument = doc.DCo_NoDocContable,
                                   format = cue.CtB_Formato
                               }).ToList();

            var modelDocument = listDocument
                .GroupBy(o => new
                {
                    idBoleta = o.idBoleta,
                    codigo = o.codigo,
                    noAccount = o.noAccount,
                    noDeposit = o.noDeposit,
                    date = o.date,
                    namebank = o.namebank,
                    format = o.format

                })
                .Select(g => new listDocumentJson
                {
                    idBoleta = g.Key.idBoleta,
                    codigo = g.Key.codigo,
                    noAccount = g.Key.noAccount,
                    noDeposit = g.Key.noDeposit,
                    date = g.Key.date,
                    namebank = g.Key.namebank,
                    format = g.Key.format,
                    idDocuments = g.Select(p => p.idDocument).ToList(),
                })
                .OrderBy(a => a.idBoleta)
                .ToList();

            return modelDocument;
        }


        private List<listEconomicJson> listEconomic(listDocumentJson docs)
        {
            List<listEconomicJson> listEco = new List<listEconomicJson>();
            foreach (int item in docs.idDocuments)
            {
                listEco.AddRange(getReferee(item));
                listEco.AddRange(getTimbre(item));
                listEco.AddRange(getOther(item));
            }
            return listEco;
        }


        private List<listEconomicJson> getReferee(int idDoc)
        {
            var listReferee = (from col in db.ccee_CargoEconomico_colegiado
                              where col.fk_DocContable == idDoc
                              select new listEconomicJson()
                              {
                                  idEconomic = col.CEcC_NoCargoEconomico,
                                  amount = col.CEcC_Monto,
                                  idTypeEconomic = col.fk_TipoCargoEconomico
                              }).ToList();
            return listReferee;
        }

        private List<listEconomicJson> getTimbre(int idDoc)
        {
            var listReferee = (from col in db.ccee_CargoEconomico_timbre
                               where col.fk_DocContable == idDoc
                               select new listEconomicJson()
                               {
                                   idEconomic = col.CEcT_NoCargoEconomico,
                                   amount = col.CEcT_Monto,
                                   idTypeEconomic = col.fk_TipoCargoEconomico
                               }).ToList();
            return listReferee;
        }

        private List<listEconomicJson> getOther(int idDoc)
        {
            var listReferee = (from col in db.ccee_CargoEconomico_otros
                               where col.fk_DocContable == idDoc
                               select new listEconomicJson()
                               {
                                   idEconomic = col.CEcO_NoCargoEconomico,
                                   amount = col.CEcO_Monto,
                                   idTypeEconomic = col.fk_TipoCargoEconomico
                               }).ToList();
            return listReferee;
        }

        private listDocumentJson listTupla(List<listEconomicJson> economic, List<listBreakdownAccountJson> breakdown, listDocumentJson docs)
        {
            decimal totalNewC = 0m;
            List<listEconomicJson> economicGroup = groupEconomic(economic);
            foreach (listBreakdownAccountJson objBreakdown in breakdown)
            {
                listEconomicJson lEconomic = economicGroup.Where(o => o.idTypeEconomic == objBreakdown.idTypeEconomic)
                                                .FirstOrDefault();
                if(lEconomic != null)
                {
                    if (objBreakdown.condition == null) objBreakdown.condition = "";
                    if (objBreakdown.condition.Equals("nuevoColegiado"))
                    {
                        totalNewC += ((lEconomic.total == null) ? 0m : lEconomic.total.Value);
                    }
                    else
                    {
                        switch (objBreakdown.codigo)
                        {
                            case Constant.Constant.cod_document_accouting_value_1:
                                docs.valor1 += Math.Round( ((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_2:
                                docs.valor2 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_3:
                                docs.valor3 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_4:
                                docs.valor4 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_5:
                                docs.valor5 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_6:
                                docs.valor6 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_7:
                                docs.valor7 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_8:
                                docs.valor8 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_9:
                                docs.valor9 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_10:
                                docs.valor10 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_11:
                                docs.valor11 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_12:
                                docs.valor12 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_13:
                                docs.valor13 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_14:
                                docs.valor14 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_15:
                                docs.valor15 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_16:
                                docs.valor16 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_17:
                                docs.valor17 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_18:
                                docs.valor18 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                            case Constant.Constant.cod_document_accouting_value_19:
                                docs.valor19 += Math.Round(((lEconomic.total == null) ? 0m : lEconomic.total.Value), 2);
                                break;
                        }
                    }

                    if (totalNewC > 0)
                    {
                        docs.valor16 = totalNewC;
                    }
                    docs.total = docs.valor1 + docs.valor2 + docs.valor3 + docs.valor4 + docs.valor5 + docs.valor6 +
                                 docs.valor7 + docs.valor8 + docs.valor9 + docs.valor10 + docs.valor11 + docs.valor12 +
                                 docs.valor13 + docs.valor14 + docs.valor15 + docs.valor16 + docs.valor17 + docs.valor18 +
                                 docs.valor19;
                }
                
            }

            return docs;

        }

        private List<listBreakdownAccountJson> breakdownAccount()
        {
            var tempDesglose = (from tip in db.ccee_TipoCargoEconomico
                               join par in db.ccee_PartidaContable on tip.ccee_PartidaContable_PCo_NoPartidaContable equals par.PCo_NoPartidaContable
                               where par.PCo_NoPartidaContable != 28
                               select new listBreakdownAccountJson()
                               {
                                   id = par.PCo_NoPartidaContable,
                                   codigo = par.PCo_Codigo,
                                   idTypeEconomic = tip.TCE_NoTipoCargoEconomico,
                                   condition = tip.TCE_CondicionCalculo
                               }).ToList();
            return tempDesglose;
        }

        private List<listEconomicJson> groupEconomic(List<listEconomicJson> economic)
        {
            var modelEconomic = economic
                .GroupBy(o => new
                {
                    idTypeEconomic = o.idTypeEconomic

                })
                .Select(g => new listEconomicJson
                {
                    idTypeEconomic = g.Key.idTypeEconomic,
                    total = g.Sum(o=>o.amount),
                })
                .OrderBy(a => a.idTypeEconomic)
                .ToList();
            return modelEconomic;
        }


    }
}
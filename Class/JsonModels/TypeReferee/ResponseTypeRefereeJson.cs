﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.TypeReferee
{
    public class ResponseTypeRefereeJson
    {
        public int idTypeReferee { get; set; }
        public string nombre { get; set; }
    }
}
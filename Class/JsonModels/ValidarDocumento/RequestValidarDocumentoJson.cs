﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.ValidarDocumento
{
    public class RequestValidarDocumentoJson
    {
        public int idDocument { get; set; }
        public string institution { get; set; }
    }
}
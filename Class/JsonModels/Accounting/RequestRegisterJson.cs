﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Accounting
{
    public class RequestRegisterJson
    {
        public int idR { get; set; }
        public string FNRef { get; set; }
        public string SNRef { get; set; }
        public string FSNRef { get; set; }
        public string SSNRef { get; set; }
        public string mailReferee { get; set; }
        public string mailRefereeC { get; set; }
        public string telR { get; set; }
        public string nitR { get; set; }
        public string cuiR { get; set; }
        public string datenac { get; set; }
        public string terminosycon { get; set; }
        public string rcaptcha { get; set; }
    }
}
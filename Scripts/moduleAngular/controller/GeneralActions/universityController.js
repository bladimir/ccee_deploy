﻿(function () {
    angular.module('generals')
        .controller('university', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listUniversity;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idUniversidadUpdate = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            
            $scope.init = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_UNIVERSITY)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listUniversity = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }


            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }


            $scope.getUniversity = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_UNIVERSITY)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listUniversity = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.addUniversity = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.universityname,
                    description: $scope.universitydescription
                }
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_UNIVERSITY, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getUniversity();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (uni) {
                $scope.idUniversidadUpdate = uni.id;
                $scope.universityname = uni.name;
                $scope.universitydescription = uni.description;
                $anchorScroll();
            }

            $scope.updateUniversity = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.universityname,
                    description: $scope.universitydescription
                }
                generalactionsFactory.actionPutFunction(CONSTANT.URL_API_UNIVERSITY, $scope.idUniversidadUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.getUniversity();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.deleteUniversity = function (uni) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionDeleteFunction(CONSTANT.URL_API_UNIVERSITY, uni.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getUniversity();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idUniversidadUpdate = 0;
                $scope.universityname = "";
                $scope.universitydescription = "";
            }


        }]);

})()
﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.AddDues;
using webcee.Class.Tools;
using webcee.Models;

namespace webcee.Controllers
{
    public class Automatic_EconomicController : ApiController
    {


        private static Logger logger = LogManager.GetCurrentClassLogger();
        //private CCEEEntities db = new CCEEEntities();
        public int init = 0;
        public int finish = 0;
        private const bool THERE_IS_PAY = true;
        private const bool THERE_IS_NOT_PAY = false;
        //private const string regexReferee = @"0|1|2|3|4|7|14|15|16";
        private const string regexTimbre = @"0|1|2|14|15|16";
        private const string regexPostumo = @"0|1|2|3|4|8|14|15|16";
        private const string regexVerifyStatus = @"1|4|7";

        private const string regexTimbresNew = @"^(3|4|5|6|8)?$"; // timbre
        private const string regexSeguroNew = @"^(5|6|7)?$"; // no postumo junto con timbre
        private const string regexRefereeNew = @"^(5|6|8)?$"; // colegiado

        private string status = "";

        private int idEconomic = 122;
        private int isRefereeOrTimbre = 0;

        public class refereeTemp{
            public int NoReferee { get; set; }
            public int? status { get; set; }
            public decimal? salario { get; set; }
            public int? active { get; set; }
            public DateTime fecha { get; set; }
        }


        // GET: api/Automatic_Economic
        //public string Get()
        //{


        //    string error = "";
        //    using (var transaction = db.Database.BeginTransaction())
        //    {

        //        db.Configuration.AutoDetectChangesEnabled = false;
        //        try
        //        {
                    
        //            DateTime time = DateTime.Today;
        //            int month = time.Month;
        //            int year = time.Year;
        //            System.Diagnostics.Debug.WriteLine(time);

        //            List<refereeTemp> listReferee = (from col in db.ccee_Colegiado
        //                                             select new refereeTemp()
        //                                             {
        //                                                 NoReferee = col.Col_NoColegiado,
        //                                                 status = col.fk_TipoColegiado,
        //                                                 salario = col.Col_Salario,
        //                                                 active = col.Col_Estatus,
        //                                                 fecha = DateTime.Now
        //                                             }).ToList();

        //            Referee tempReferee;
        //            foreach (refereeTemp item in listReferee)
        //            {
        //                string json = JsonConvert.SerializeObject(item);
        //                System.Diagnostics.Debug.WriteLine(json);

        //                using (var db2 = new CCEEEntities())
        //                {
        //                    try
        //                    {
        //                        tempReferee = new Referee(item.NoReferee, db2);
        //                        tempReferee.economic();

        //                        if (isPay(item.status, regexVerifyStatus))
        //                            tempReferee.chageStatus();

        //                        if (item.status == 3)
        //                        {
        //                            if (item.active != null)
        //                            {
        //                                //if (item.active == 1)
        //                                //{
        //                                //    tempReferee.chageStatus();
        //                                //}
        //                                tempReferee.chageStatus();
        //                            }
        //                        }


        //                        if (isPay(item.status, regexReferee))
        //                            tempReferee.setEconomicReferee(month, year);

        //                        if (isPay(item.status, regexTimbre))
        //                        {
        //                            tempReferee.setTimbre(month, year, item.salario, false);
        //                        }
        //                        else
        //                        {
        //                            if (isPay(item.status, regexPostumo))
        //                                tempReferee.setTimbre(month, year, item.salario, true);
        //                        }

                                
        //                        tempReferee = null;
        //                        db2.SaveChanges();
        //                    }
        //                    catch (Exception)
        //                    {
        //                        error += json;
        //                        throw;
        //                    }
        //                }

                        
        //            }

        //            db.SaveChanges();
        //            transaction.Commit();
        //            return error;
        //        }
        //        catch (Exception e)
        //        {
        //            System.Diagnostics.Debug.WriteLine(e);
        //            transaction.Rollback();
        //            return error;
        //        }
        //    }
        //}

        // GET: api/Automatic_Economic/5
        public string Get()
        {
            //var referees = db.ccee_Colegiado.Select(p=>p.Col_NoColegiado).ToList();
            /*var referees = (from col in db.ccee_Colegiado
                       select new
                       {
                           referee = col.Col_NoColegiado
                       }).OrderBy(p=>p.referee).ToList();
            string stringDate = "01/" + ((DateTime.Today.Month < 10) ? "0" + DateTime.Today.Month : "" +DateTime.Today.Month) + "/" + DateTime.Today.Year;
            DateTime date = Class.GeneralFunction.changeStringofDate(stringDate);
            for (int i = 0; i < referees.Count(); i++)
            {
                //int status = createPayReferee(referees[i].referee, 1, 1, date);
                logger.Info("Automatic_Economic|Colegiado| " + referees[i] + "-" + status + "|" + this.status);
                Debug.WriteLine("Automatic_Economic|Colegiado| " + referees[i] + "-" + status + "|" + this.status);
            }*/
            //getListRepeat();
            return "value";
        }

        [Route("api/Automatic_Economic/{init}/{last}")]
        public string Get(int init, int last)
        {
            this.init = init;
            this.finish = last;
            var th = new Thread(new ThreadStart(getThreadReferee));
            th.IsBackground = true;
            th.Start();


            //var referees = db.ccee_Colegiado.Select(p=>p.Col_NoColegiado).ToList();
            /*var referees = (from col in db.ccee_Colegiado
                            where col.Col_NoColegiado >= init && col.Col_NoColegiado <= last
                            select new
                            {
                                referee = col.Col_NoColegiado
                            }).OrderBy(p => p.referee).ToList();
            string stringDate = "01/" + ((DateTime.Today.Month < 10) ? "0" + DateTime.Today.Month : "" + DateTime.Today.Month) + "/" + DateTime.Today.Year;
            DateTime date = Class.GeneralFunction.changeStringofDate(stringDate);
            for (int i = 0; i < referees.Count(); i++)
            {
                int status = createPayReferee(referees[i].referee, 1, 1, date);
                logger.Info("Automatic_Economic|Colegiado| " + referees[i] + "-" + status + "|" + init + "-" + last + "|" + this.status);
                Debug.WriteLine("Automatic_Economic|Colegiado| " + referees[i] + "-" + status + "|" + init + "-" + last + "|" + this.status);
            }*/

            return "value";
        }

        public void getThreadReferee()
        {
            int init = this.init;
            int last = this.finish;
            Debug.WriteLine("iniciado" + init + " - " + last);
            using (var context = new CCEEEntities())
            {
                var referees = (from col in context.ccee_Colegiado
                                where col.Col_NoColegiado >= init && col.Col_NoColegiado <= last
                                select new
                                {
                                    referee = col.Col_NoColegiado
                                }).OrderBy(p => p.referee).ToList();
                string stringDate = "01/" + ((DateTime.Today.Month < 10) ? "0" + DateTime.Today.Month : "" + DateTime.Today.Month) + "/" + DateTime.Today.Year;
                //string stringDate = "01/07/2018";
                DateTime date = Class.GeneralFunction.changeStringofDate(stringDate);
                for (int i = 0; i < referees.Count(); i++)
                {
                    int status = createPayReferee(referees[i].referee, 1, 1, date);
                    logger.Info("Automatic_Economic|Colegiado| " + referees[i] + "-" + status + "|" + init + "-" + last + "|" + this.status);
                    Debug.WriteLine("Automatic_Economic|Colegiado| " + referees[i] + "-" + status + "|" + init + "-" + last + "|" + this.status);
                }
            }
            //var referees = db.ccee_Colegiado.Select(p=>p.Col_NoColegiado).ToList();
           
        }


        public void getListRepeat()
        {
            string stringDate = "01/07/2018";
            DateTime date = Class.GeneralFunction.changeStringofDate(stringDate);
            using (var context = new CCEEEntities())
            {
                var temp = (from eco in context.ccee_CargoEconomico_colegiado
                           where eco.fk_TipoCargoEconomico == idEconomic
                           && eco.CEcC_FechaGeneracion.Value.Month == date.Month
                           && eco.CEcC_FechaGeneracion.Value.Year == date.Year
                           select new
                           {
                               id = eco.ccee_NoColegiado,
                               dateG = eco.CEcC_FechaGeneracion,
                               referee = eco.fk_Colegiado,
                               pay = eco.fk_Pago,
                               doc = eco.fk_DocContable
                           }).ToList();




                string data = "";


            }

        }


        // GET: api/Automatic_Economic/5
        public string Get(int id)
        {
            getListRepeat();
            return "value";
        }

        // POST: api/Automatic_Economic
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Automatic_Economic/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Automatic_Economic/5
        public void Delete(int id)
        {
        }

       
        public bool isPay(int? status, string regex)
        {
            if (status == null) return false;
            Regex r = new Regex(regex, RegexOptions.IgnoreCase);
            Match mat = r.Match(status.ToString());
            return mat.Success;
        }



        public int createPayReferee(int idReferee, int countC, int countT, DateTime dateChange)
        {

            using (var db = new CCEEEntities())
            {


                using (var transaction = db.Database.BeginTransaction())
                {
                    status = "";

                    try
                    {
                        int idBase = Constant.type_economic_base;
                        int idPostumo = Constant.type_economic_postumo;
                        ccee_Colegiado referee = db.ccee_Colegiado.Find(idReferee);
                        status += "|" + "tipo colegiado: " + referee.fk_TipoColegiado;
                        if (countC > 0)
                        {
                            if (!getRegex(referee.fk_TipoColegiado, regexRefereeNew))
                            {
                                Nullable<DateTime> lastRefereee = null;
                                lastRefereee = (db.ccee_CargoEconomico_colegiado
                                                    .Where(o => o.fk_Colegiado == idReferee
                                                    && o.CEcC_FechaGeneracion.Value.Month == dateChange.Month 
                                                    && o.CEcC_FechaGeneracion.Value.Year == dateChange.Year)
                                                    .OrderByDescending(p => p.CEcC_FechaGeneracion)
                                                    .Select(q => q.CEcC_FechaGeneracion))
                                                    .FirstOrDefault();

                                


                                if (lastRefereee == null)
                                {
                                    lastRefereee = dateChange;
                                    lastRefereee = ((lastRefereee == null) ? DateTime.Now : lastRefereee);
                                    List<ccee_TipoCargoEconomico> listTipoCargos = generarMontoColegiado(db);
                                    ccee_CargoEconomico_colegiado tempEconomicoCol;
                                    for (int i = 0; i < countC; i++)
                                    {
                                        DateTime nextMount = lastRefereee.Value.AddMonths(i);
                                        for (int j = 0; j < listTipoCargos.Count; j++)
                                        {

                                            tempEconomicoCol = new ccee_CargoEconomico_colegiado()
                                            {
                                                CEcC_Monto = listTipoCargos[j].TCE_Valor,
                                                CEcC_FechaGeneracion = nextMount,
                                                fk_TipoCargoEconomico = listTipoCargos[j].TCE_NoTipoCargoEconomico,
                                                fk_Colegiado = idReferee,
                                                fk_Pago = null
                                            };
                                            if (dateChange != null)
                                            {
                                                tempEconomicoCol.CEcC_FechaGeneracion = dateChange;
                                            }
                                            if (!getRegex(referee.fk_TipoColegiado, regexRefereeNew))
                                            {
                                                db.ccee_CargoEconomico_colegiado.Add(tempEconomicoCol);
                                                status += "-" + "pago colegiado: 200";
                                            }

                                        }
                                    }
                                }

                            }
                        }

                        if (countT > 0)
                        {
                            Nullable<DateTime> lastTimbre = null;
                            lastTimbre = (db.ccee_CargoEconomico_timbre
                                                .Where(o => o.fk_Colegiado == idReferee
                                                && o.CEcT_FechaGeneracion.Value.Month == dateChange.Month
                                                && o.CEcT_FechaGeneracion.Value.Year == dateChange.Year)
                                                .OrderByDescending(p => p.CEcT_FechaGeneracion)
                                                .Select(q => q.CEcT_FechaGeneracion))
                                                .FirstOrDefault();
                            if (lastTimbre == null)
                            {

                                lastTimbre = ((lastTimbre == null) ? DateTime.Now : lastTimbre);
                                decimal valorP = db.ccee_TipoCargoEconomico.Find(idPostumo).TCE_Valor.Value;
                                decimal montoTimbreGen = GeneralFunction.generarMontoTimbre(idReferee, db);

                                ccee_CargoEconomico_timbre tempTim = new ccee_CargoEconomico_timbre();
                                ccee_CargoEconomico_timbre tempPostumo = new ccee_CargoEconomico_timbre();
                                for (int i = 0; i < countT; i++)
                                {
                                    DateTime nextMount = lastTimbre.Value.AddMonths(i + 1);
                                    if (!getRegex(referee.fk_TipoColegiado, regexTimbresNew))
                                    {
                                        tempTim = new ccee_CargoEconomico_timbre()
                                        {
                                            CEcT_Monto = montoTimbreGen,
                                            CEcT_FechaGeneracion = nextMount,
                                            fk_TipoCargoEconomico = idBase,
                                            fk_Colegiado = idReferee,
                                            fk_Pago = null
                                        };
                                    }

                                    if (!getRegex(referee.fk_TipoColegiado, regexSeguroNew))
                                    {
                                        tempPostumo = new ccee_CargoEconomico_timbre()
                                        {
                                            CEcT_Monto = valorP,
                                            CEcT_FechaGeneracion = nextMount,
                                            fk_TipoCargoEconomico = idPostumo,
                                            fk_Colegiado = idReferee,
                                            fk_Pago = null
                                        };
                                    }
                                    if (dateChange != null)
                                    {
                                        if (!getRegex(referee.fk_TipoColegiado, regexTimbresNew))
                                        {
                                            tempTim.CEcT_FechaGeneracion = dateChange;
                                        }
                                        if (!getRegex(referee.fk_TipoColegiado, regexSeguroNew))
                                        {
                                            tempPostumo.CEcT_FechaGeneracion = dateChange;
                                        }

                                    }

                                    if (!getRegex(referee.fk_TipoColegiado, regexTimbresNew))
                                    {
                                        db.ccee_CargoEconomico_timbre.Add(tempTim);
                                        status += "-" + "pago timbre: 200";
                                    }
                                    if (!getRegex(referee.fk_TipoColegiado, regexSeguroNew))
                                    {
                                        db.ccee_CargoEconomico_timbre.Add(tempPostumo);
                                        status += "-" + "pago postumo: 200";
                                    }


                                }
                            }


                        }

                        db.SaveChanges();
                        transaction.Commit();
                        //transaction.Rollback();
                        return 200;


                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        return 400;
                    }
                }
            }
        }


        private List<ccee_TipoCargoEconomico> generarMontoColegiado(CCEEEntities db)
        {
            string cole = GeneralFunction.getKeyTableVariable(db, Constant.table_var_colegiado);
            int idEconomicReferee = Convert.ToInt32(cole);
            ccee_TipoCargoEconomico tempTypeEco = (from tca in db.ccee_TipoCargoEconomico
                                                   where tca.TCE_NoTipoCargoEconomico == idEconomicReferee
                                                   select tca).Single();

            string calculation = tempTypeEco.TCE_CondicionCalculo;
            string[] arrayCalculation = calculation.Split(',');
            List<int> intCalculation = new List<int>();
            foreach (string item in arrayCalculation)
            {
                intCalculation.Add(Convert.ToInt32(item));
            }

            var listE = from tca in db.ccee_TipoCargoEconomico
                        where intCalculation.Contains(tca.TCE_NoTipoCargoEconomico)
                        select tca;
            return listE.ToList();

        }

        private bool getRegex(int typeReferee, string regex)
        {
            Regex r = new Regex(regex, RegexOptions.IgnoreCase);
            Match mat = r.Match(typeReferee.ToString());
            return (mat.Success);
        }


        

    }
}

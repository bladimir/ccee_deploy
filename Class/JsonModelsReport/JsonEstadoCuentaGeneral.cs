﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonEstadoCuentaGeneral
    {
        public int Month { get; set; }
        public string mes
        {
            get
            {
                CultureInfo culture;
                culture = CultureInfo.CreateSpecificCulture("es-GT");
                return culture.DateTimeFormat.GetMonthName(this.Month);
            }
        }
        public int anio { get; set; }
        private decimal? tempMontoC { get; set; }
        public decimal? montoC
        {
            get
            {
                return ((tempMontoC == null) ? 0 :  Math.Round(tempMontoC.Value, 2));
            }
            set { tempMontoC = value; }
        }

        private decimal? tempMontoT { get; set; }
        public decimal? montoT
        {
            get
            {
                return ((tempMontoT == null) ? 0 : Math.Round(tempMontoT.Value, 2));
            }
            set { tempMontoT = value; }
        }

        private decimal? tempMontoP { get; set; }
        public decimal? montoP
        {
            get
            {
                return ((tempMontoP == null) ? 0 : Math.Round(tempMontoP.Value, 2));
            }
            set { tempMontoP = value; }
        }
        private decimal? tempMontoD { get; set; }
        public decimal? montoD
        {
            get
            {
                return ((tempMontoD == null) ? 0 : Math.Round(tempMontoD.Value, 2));
            }
            set { tempMontoD = value; }
        }
        public DateTime? tempfechaPago { get; set; }
        public string fecha
        {
            get
            {
                return (tempfechaPago == null) ? "" : tempfechaPago.Value.ToString("dd/MM/yyyy");
            }
        }
        public string recibo { get; set; }
        public string url { get; set; }
        public decimal porcent { get; set; }
        public int? idRecibo { get; set; }
        public string nombre { get; set; }

        public DateTime? tempfechaReceipt { get; set; }
        public string fechaReceip
        {
            get
            {
                return (tempfechaReceipt == null) ? "" : tempfechaReceipt.Value.ToString("dd/MM/yyyy");
            }
        }
        public int rechazado { get; set; }
        public int? congelado { get; set; }
    }
}
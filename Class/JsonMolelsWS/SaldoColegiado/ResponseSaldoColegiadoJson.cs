﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonMolelsWS.SaldoColegiado
{
    public class ResponseSaldoColegiadoJson
    {
        public int codigo { get; set; }
        public string resultadoConsulta { get; set; }
        public string nombreCompleto { get; set; }
        public string fechaUltimoPagoCol { get; set; }
        public string fechaUltimoPagoTim { get; set; }
        public int noCuotasCol { get; set; }
        public int noCuotasTim { get; set; }
        public int estadoColegiado { get; set; }
        public decimal saldoColegiado { get; set; }
        public decimal saldoTimbre { get; set; }
        public decimal postumo { get; set; }
        public decimal mora { get; set; }
        public decimal total { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.TypeDocument;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_TypeDocumentController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_TypeDocument
        public IEnumerable<ResponseTypeDocumentJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                var tempTypeDocument = from doc in db.ccee_TipoDoc
                                     select new ResponseTypeDocumentJson()
                                     {
                                         id = doc.TDC_NoTipoDoc,
                                         name = doc.TDC_Nombre,
                                         description = doc.TDC_Descripcion,
                                         templateXML = doc.TDC_XmlPlantilla,
                                         consumable = doc.TDC_Consumible
                                     };
                Autorization.saveOperation(db, autorization, "Obteniendo tipo documento");
                return tempTypeDocument;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_TypeDocument/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_TypeDocument
        public IHttpActionResult Post(RequestTypeDocumentJson typeDocument)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_TipoDoc tempTypeDocument = new ccee_TipoDoc()
                {
                    TDC_Nombre = typeDocument.name,
                    TDC_Descripcion = typeDocument.description,
                    TDC_XmlPlantilla = typeDocument.templateXML,
                    TDC_Consumible = typeDocument.consumable
                };

                db.ccee_TipoDoc.Add(tempTypeDocument);
                Autorization.saveOperation(db, autorization, "Agregando tipo documento");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_TypeDocument/5
        public IHttpActionResult Put(int id, RequestTypeDocumentJson typeDocument)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_TipoDoc tempTypeDocument = db.ccee_TipoDoc.Find(id);

                if (tempTypeDocument == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempTypeDocument.TDC_Nombre = typeDocument.name;
                tempTypeDocument.TDC_Descripcion = typeDocument.description;
                tempTypeDocument.TDC_XmlPlantilla = typeDocument.templateXML;
                tempTypeDocument.TDC_Consumible = typeDocument.consumable;

                db.Entry(tempTypeDocument).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando tipo documento");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_TypeDocument/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_TipoDoc tempTypeDocument = db.ccee_TipoDoc.Find(id);
                if (tempTypeDocument == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_TipoDoc.Remove(tempTypeDocument);
                Autorization.saveOperation(db, autorization, "Eliminando tipo documento");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}

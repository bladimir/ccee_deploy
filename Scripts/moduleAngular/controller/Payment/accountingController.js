﻿(function () {
    angular.module('payment')
        .controller('accounting', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'paymentFactory', function ($scope, $http, CONSTANT, $anchorScroll, paymentFactory) {
            $scope.listAccounting;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idAccountingUpdate = 0;
            $scope.idTypeAccounting = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                paymentFactory.actionGetFunction(CONSTANT.URL_API_TYPEACCOUNTING)
                    .then(function (response) {
                        console.log(response);
                        $scope.hideLoadingFunction();
                        $scope.listTypeAccounting = response.data;
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
            }


            $scope.setAuth = function () {
                paymentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        paymentFactory.setAuthorization(response.data);
                        $scope.init();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }
           
            

            $scope.searchAccount = function () {
                $scope.idTypeAccounting = $scope.selectaccounttype.id;
                $scope.getAccounting($scope.idTypeAccounting);
            }

            $scope.getAccounting = function (id) {
                $scope.showLoadingFunction();
                paymentFactory.actionGetIntFunction(CONSTANT.URL_API_ACCOUNTING, id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listAccounting = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            

            $scope.addAccounting = function () {
                $scope.showLoadingFunction();
                var information = {
                    description: $scope.accountdescription,
                    action: $scope.accountaction,
                    idTypeAccount: $scope.idTypeAccounting,
                    key: $scope.accountkey,
                    name: $scope.accountname,
                    position: $scope.accountposition,
                    groupAccouting: $scope.accountgroup
                }

                console.log(information);
                paymentFactory.actionPostFunction(CONSTANT.URL_API_ACCOUNTING, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.cleanData();
                                $scope.getAccounting($scope.idTypeAccounting);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (type) {
                $scope.idAccountingUpdate = type.id;
                $scope.accountdescription = type.description;
                $scope.accountaction = type.action;
                $scope.accountkey = type.key;
                $scope.accountname = type.name;
                $scope.accountposition = type.position;
                $scope.accountgroup = type.groupAccouting;
                $anchorScroll();
            }

            $scope.updateAccounting = function () {
                $scope.showLoadingFunction();
                var information = {
                    description: $scope.accountdescription,
                    action: $scope.accountaction,
                    key: $scope.accountkey,
                    name: $scope.accountname,
                    position: $scope.accountposition,
                    groupAccouting: $scope.accountgroup
                }
                paymentFactory.actionPutFunction(CONSTANT.URL_API_ACCOUNTING, $scope.idAccountingUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.getAccounting($scope.idTypeAccounting);
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }


            $scope.deleteAccounting = function (type) {
                $scope.showLoadingFunction();
                paymentFactory.actionDeleteFunction(CONSTANT.URL_API_ACCOUNTING, type.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getAccounting($scope.idTypeAccounting);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idAccountingUpdate = 0;
                $scope.accountdescription = "";
                $scope.accountaction = "";
                $scope.accountkey = "";
                $scope.accountname = "";
                $scope.accountposition = null;
            }


        }]);

})()
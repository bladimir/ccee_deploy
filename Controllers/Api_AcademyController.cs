﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.JsonModels.Academy;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_AcademyController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Academy
        public IEnumerable<string> Get()
        {
            return null;
        }

        // GET: api/Api_Academy/5
        public IEnumerable<ResponseAcademyJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                var tempAcademy = from aca in db.ccee_Especialidad
                                  join esp in db.ccee_TipoEspecialidad on aca.pkfk_TipoEspecialidad equals esp.TEs_NoTipoEspecialidad
                                  join uni in db.ccee_Universidad on aca.pkfk_Universidad equals uni.Uni_NoUniversidad
                                  where aca.pkfk_Colegiado == id
                                  select new ResponseAcademyJson()
                                  {
                                      idSpecialty = esp.TEs_NoTipoEspecialidad,
                                      idUniversity = uni.Uni_NoUniversidad,
                                      specialty = esp.TEs_NombreTipoEsp,
                                      university = uni.Uni_NombreUniversidad,
                                      name = aca.Esp_NombreEspecialidad,
                                      title = aca.Esp_Titulo,
                                      date = aca.Esp_FechaObtuvo,
                                      evaluation = aca.Esp_Evaluacion,
                                      graduationSubject = aca.Esp_Tema_Graduacion,
                                      fullNameUniversity = uni.Uni_Descripcion
                                  };
                List<ResponseAcademyJson> listAcademy = tempAcademy.ToList();
                for (int i = 0; i < listAcademy.Count; i++)
                {
                    listAcademy[i].dateText = GeneralFunction.DateOfString(listAcademy[i].date);
                }
                //foreach (ResponseAcademyJson contAcademy in tempAcademy)
                //{
                //    contAcademy.dateText = GeneralFunction.DateOfString(contAcademy.date);
                //}
                Autorization.saveOperation(db, autorization, "Obteniendo Academia");
                return listAcademy;

            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_Academy
        public IHttpActionResult Post(RequestAcademyJson academy)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_Especialidad tempAcademy = new ccee_Especialidad()
                {
                    pkfk_Colegiado = academy.idReferee,
                    pkfk_TipoEspecialidad = academy.specialty,
                    pkfk_Universidad = academy.university,
                    Esp_NombreEspecialidad = academy.name,
                    Esp_Titulo = academy.title,
                    Esp_FechaObtuvo = academy.date,
                    Esp_Evaluacion = academy.evaluation,
                    Esp_Tema_Graduacion = academy.graduationSubject
                };

                db.ccee_Especialidad.Add(tempAcademy);
                Autorization.saveOperation(db, autorization, "Agregando Academia");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        [Route("api/Api_Academy/{id}/{esp}/{uni}")]
        // PUT: api/Api_Academy/5/5/5
        public IHttpActionResult Put(int id, int esp, int uni, RequestAcademyJson academy)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_Especialidad tempAcademy = db.ccee_Especialidad.Find(esp, uni, id);

                if (tempAcademy == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                
                tempAcademy.Esp_NombreEspecialidad = academy.name;
                tempAcademy.Esp_Titulo = academy.title;
                tempAcademy.Esp_Tema_Graduacion = academy.graduationSubject;
                tempAcademy.Esp_Evaluacion = academy.evaluation;
                if(academy.date != null)
                {
                    tempAcademy.Esp_FechaObtuvo = academy.date;
                }

                db.Entry(tempAcademy).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando Academia");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);

            }
            catch (Exception )
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        [Route("api/Api_Academy/{id}/{esp}/{uni}")]
        // DELETE: api/Api_Academy/5
        public IHttpActionResult Delete(int id, int esp, int uni)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_Especialidad tempAcademy = db.ccee_Especialidad.Find(esp, uni, id);

                if (tempAcademy == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_Especialidad.Remove(tempAcademy);
                Autorization.saveOperation(db, autorization, "Eliminando Academia");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }
    }
}

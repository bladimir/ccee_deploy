﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Prueba;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_PruebaController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();
        public List<PurebaJson> Get()
        {

            List<PurebaJson> pp = new List<PurebaJson>();
            var tt = db.ccee_Colegiado.ToList();
            foreach (ccee_Colegiado col in tt)
            {
                pp.Add(new PurebaJson() {
                    id = col.Col_NoColegiado,
                    nombre = col.Col_PrimerNombre
                });
            }

            return pp;
        }

    }
}

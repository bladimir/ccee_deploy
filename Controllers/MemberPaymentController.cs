﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using System.Web;
using webcee.Models;
using webcee.ViewModels;
using webcee.Class;
using webcee.Class.Constant;

namespace webcee.Controllers
{
    public class MemberPaymentController : Controller
    {
        private CCEEEntities db = new CCEEEntities();
        // GET: api/MemberPayment
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/MemberPayment/5
        public string Get(int id)
        {
            return "value";
        }

        public ActionResult MemberEconomic()
        {

            LoginRefreeModelView login = Session[Constant.session_user] as LoginRefreeModelView;
            if (GeneralFunction.permissionsRefereee(this, Session[Constant.session_user] as LoginRefreeModelView, Constant.operation_ejecutar_pago) == null) return RedirectToAction("TimbreUsuario", "Document");
            
            ViewBag.idCahier = Constant.member_payment_Cashier;
            ViewBag.idCash = Constant.member_payment_Caja;
            ViewBag.receipt = Constant.member_payment_NoRecibo;
            ViewBag.idReferee = login.referee;
            return View();
            }
        }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webcee.Class.JsonModels.Timbre;

namespace webcee.Class.JsonModels._Tools
{
    public class PostBodyTimbreJson
    {

        public decimal value { get; set; }
        public List<ResponseTimbreJson> content { get; set; }

    }
}
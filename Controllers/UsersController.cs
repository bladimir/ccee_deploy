﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webcee.Class;
using webcee.Class.Constant;
using webcee.Models;
using webcee.ViewModels;

namespace webcee.Controllers
{
    public class UsersController : Controller
    {
        private CCEEEntities dbConexion = new CCEEEntities();
        // GET: Users
        public ActionResult Index()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_usuario) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, "") == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            return View();
        }

        public ActionResult Roles(int? id)
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_rol) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult Files(int? id)
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_expediente) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult Operations(int? id)
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_expediente) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult UserCash(int? id)
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_asing_cajero) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult Profile()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_pagina_inicio) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idUser = login.User.Usu_NoUsuario;
            return View();
        }

        public string GetToken()
        {
            //if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_universidad) == null) return null;
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            string keyAuth = login.keyAutorization;
            return keyAuth;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbConexion.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace webcee.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ccee_Documento
    {
        public int Doc_NoDocumento { get; set; }
        public string Doc_Observacion { get; set; }
        public Nullable<System.DateTime> Doc_FechaHoraEmision { get; set; }
        public string Doc_Xml { get; set; }
        public string Doc_HashDoc { get; set; }
        public Nullable<System.DateTime> Doc_FechaHoraConsumo { get; set; }
        public int fk_TipoDoc { get; set; }
        public string DCo_PrintRePrint { get; set; }
        public Nullable<int> fk_CargoEconomicoOtros { get; set; }
        public string Doc_NoDocumentoOld { get; set; }
        public string Doc_ANombre { get; set; }
        public Nullable<System.DateTime> Doc_FechaFinal { get; set; }
        public Nullable<int> Doc_Estado { get; set; }
        public Nullable<int> fk_Cajero { get; set; }
        public Nullable<int> Doc_EstadoCol { get; set; }
        public Nullable<int> Doc_EstatusBase64 { get; set; }
        public string Doc_FileBase64 { get; set; }
    
        public virtual ccee_Cajero ccee_Cajero { get; set; }
        public virtual ccee_CargoEconomico_otros ccee_CargoEconomico_otros { get; set; }
        public virtual ccee_TipoDoc ccee_TipoDoc { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Timbre
{
    public class RequestTimbreJson
    {
        public int idNumberTimbre { get; set; }
        public int status { get; set; }
        public int value { get; set; }
        public int idPageTimbre { get; set; }
    }
}
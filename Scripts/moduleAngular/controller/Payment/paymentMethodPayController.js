﻿(function () {
    angular.module('payment')
        .controller('paymentMethodPay', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'paymentFactory', function ($scope, $http, CONSTANT, $anchorScroll, paymentFactory) {
            $scope.listData;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.idUpdate = 0;
            $scope.objectUpdate;
            $scope.viewData = false;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.setAuth = function () {
                paymentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        paymentFactory.setAuthorization(response.data);
                        $scope.get();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.get = function () {
                $scope.showLoadingFunction();
                paymentFactory.actionGetIntFunction(CONSTANT.URL_API_PAYMENTMETHODPAY, $scope.noDocument)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listData = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.showData = function (data) {
                $scope.objectUpdate = data;
                $scope.amount = data.amount;
                $scope.document = data.noDocument;
                $scope.noTrans = data.noTransaction;
                $scope.casher = data.idCasher;
                $scope.cash = data.nameCash;
                $scope.typeMethod = data.typePaymentMethod;
                $scope.bank = data.issuingBank;
                $scope.dateAmount = data.date;
                $scope.rejection = data.rejection;
                $scope.reservation = data.reservation;
                $scope.remarke = data.remarke;
                $scope.viewData = true;
            }

            $scope.cancelData = function () {
                $scope.viewData = false;
            }

            $scope.cleanData = function () {
                $scope.idUpdate = 0;
                $scope.description = "";
                $scope.typeMethod = "";
            }

            $scope.changeType = function (state) {
                if (state == 0) {
                    return "Efectivo";
                } else if (state == 1) {
                    return "Cheque";
                } else if (state == 2) {
                    return "Transferencia";
                } else if (state == 3) {
                    return "Tarjeta de credito";
                } else if (state == 4) {
                    return "Tarjeta de debito";
                } else if (state == 5) {
                    return "Lote institucional";
                } else {
                    return "Indefinido";
                }
            }
            
            $scope.changeReservation = function (state) {
                if (state == 0) {
                    return "Sin reserva";
                } else if (state == 1) {
                    return "En reserva";
                } else if (state == 2) {
                    return "Rechazado";
                } else {
                    return "No definido"
                }
            }

            $scope.changeRejection = function (state) {
                if (state == 0) {
                    return "No";
                } else if (state == 1) {
                    return "Si";
                } else {
                    return "No definido"
                }
            }


        }]);

})()
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.FileRequirement
{
    public class UploadFileRequirement
    {
        public int id { get; set; }
        public string name { get; set; }
        public int idFile { get; set; }
        public string nameFile { get; set; }
        public int required { get; set; }
    }
}
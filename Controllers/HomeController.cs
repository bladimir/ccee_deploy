﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.OpenCloseCash;
using webcee.Models;
using webcee.ViewModels;

namespace webcee.Controllers
{
    public class HomeController : Controller
    {

        private CCEEEntities dbConexion  = new CCEEEntities(); 
        // GET: Home
        public ActionResult Index()
        {

            //ccee_Usuario o = dbConexion.ccee_Usuario.Single(l => l.Usu_NoUsuario == 1);
            //ccee_Perfil_Operacion p = dbConexion.ccee_Perfil_Operacion.Single(k => k.ccee_Usuario == o);
            //var m = dbConexion.ccee_Perfil.Single(b => b.ccee_Perfil_Operacion == p);

            

            ViewBag.titulo = "";
            return View();
            //return View();
        }

        [HttpPost]
        public ActionResult Index(String email, String password)
        {
            LoginModelViews loginModelViews = new LoginModelViews();
            try
            {
                loginModelViews.User = dbConexion.ccee_Usuario.Single(d => d.Usu_Correo == email);
                if (loginModelViews.User != null)
                {
                    if (loginModelViews.User.Usu_Clave.Equals(GeneralFunction.MD5Encrypt(password)))
                    {
                        ccee_Usuario user = new ccee_Usuario()
                        {
                            Usu_Correo = loginModelViews.User.Usu_Correo,
                            Usu_Nombre = loginModelViews.User.Usu_Nombre,
                            Usu_NoUsuario = loginModelViews.User.Usu_NoUsuario
                        };

                        List<string> listIdOperations = new List<string>();
                        int idCashier = 0;
                        var listOperations = dbConexion.ccee_Perfil.ToList()
                            .Find(d => d.Per_NoPerfil == loginModelViews.User.Usu_Per_NoPerfil);

                        var lisOperation = from ope in dbConexion.ccee_Operacion
                                           join perope in dbConexion.ccee_Perfil_Operacion on ope.Ope_NoOperacion equals perope.PerOpe_NoOperacion
                                           join per in dbConexion.ccee_Perfil on perope.PerOpe_NoPerfil equals per.Per_NoPerfil
                                           where per.Per_NoPerfil == loginModelViews.User.Usu_Per_NoPerfil
                                           select ope;

                        foreach (ccee_Operacion oper in lisOperation)
                        {
                            listIdOperations.Add(oper.Ope_Nombre);
                        }

                        var listCashier = from cas in dbConexion.ccee_Cajero
                                          where cas.fk_Usuario == loginModelViews.User.Usu_NoUsuario
                                          select cas;

                        foreach (ccee_Cajero cashier in listCashier)
                        {
                            idCashier = cashier.Cjo_NoCajero;
                        }


                        Autorization autorization = new Autorization(dbConexion);
                        string key = autorization.createToken(user.Usu_NoUsuario, user.Usu_Nombre);

                        loginModelViews = new LoginModelViews() {
                            User = user,
                            Operations = listIdOperations,
                            idCashier = idCashier,
                            keyAutorization = key
                        };

                        HttpContext.Session.Add(Constant.session_user, loginModelViews);
                        HttpContext.Session.Timeout = 1080;


                        //var temp = Session["mio"];

                        //Session[Constant.session_user] = loginModelViews;
                        return RedirectToAction("Index", "Referre", null);
                    }
                    else
                    {
                        ViewBag.titulo = "No ingresado, correo o contraseña erronea";
                    }
                }
                else
                {
                    ViewBag.titulo = "El correo no existe";
                }
            }catch(Exception e)
            {
                ViewBag.titulo = "No se puedo establecer conexión, intentar de nuevo";
            }
            return View();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbConexion.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
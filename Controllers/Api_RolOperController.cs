﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.RolOperations;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_RolOperController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_RolOper
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_RolOper/5 el id es el de perfil o rol
        public IEnumerable<ResponseRolOperJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                var tempRolOper = from roloper in db.ccee_Perfil_Operacion
                                  join oper in db.ccee_Operacion on roloper.PerOpe_NoOperacion equals oper.Ope_NoOperacion
                                  where roloper.PerOpe_NoPerfil == id
                                  select new ResponseRolOperJson()
                                  {
                                      idOper = oper.Ope_NoOperacion,
                                      nameOper = oper.Ope_Nombre,
                                      idRol = id
                                  };
                Autorization.saveOperation(db, autorization, "Obteniendo rol operacion");
                return tempRolOper;
            }
            catch (Exception)
            {
                return null;
            }
            
        }

        // POST: api/Api_RolOper
        public IHttpActionResult Post(RequestRolOperJson roloper)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_Perfil_Operacion tempRolOper = new ccee_Perfil_Operacion()
                {
                    PerOpe_NoPerfil = roloper.idRol,
                    PerOpe_NoOperacion = roloper.idOper
                };

                db.ccee_Perfil_Operacion.Add(tempRolOper);
                Autorization.saveOperation(db, autorization, "Agregando rol operacion");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_RolOper/5
        public void Put(int id, [FromBody]string value)
        {
        }

        [Route("api/Api_RolOper/{id}/{ido}")]
        // DELETE: api/Api_RolOper/5
        public IHttpActionResult Delete(int id, int ido)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_Perfil_Operacion tempRolOper = db.ccee_Perfil_Operacion.Find(id, ido);
                if (tempRolOper == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_Perfil_Operacion.Remove(tempRolOper);
                Autorization.saveOperation(db, autorization, "Eliminando rol operacion");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

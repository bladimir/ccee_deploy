﻿(function () {
    angular.module('generals')
        .controller('typeReferee', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listTypeReferee;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.id = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_TYPEREFEREE)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listTypeReferee = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }


            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            


            $scope.getTypeReferee = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_TYPEREFEREE)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listTypeReferee = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.addTypeReferee = function (type) {
                $scope.showLoadingFunction();
                var information = {
                    description: type
                }
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_TYPEREFEREE, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getTypeReferee();
                                $scope.cleanData();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.deleteTypeReferee = function (type) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionDeleteFunction(CONSTANT.URL_API_TYPEREFEREE, type.idTypeReferee)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getTypeReferee();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (uni) {
                console.log(uni);
                $scope.id = uni.idTypeReferee;
                $scope.typedescription = uni.nombre;
                $anchorScroll();
            }

            $scope.update = function () {
                $scope.showLoadingFunction();
                var information = {
                    description: $scope.typedescription
                }
                generalactionsFactory.actionPutFunction(CONSTANT.URL_API_TYPEREFEREE, $scope.id, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.getTypeReferee();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.cleanData = function () {
                $scope.id = 0;
                $scope.typedescription = "";
            }

            
        }]);

})()
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using webcee.Models;

namespace webcee.Class.Class
{
    public class Referee
    {
        private int idReferee;
        private CCEEEntities db;
        private List<ccee_CargoEconomico_colegiado> economicReferee;
        private List<ccee_CargoEconomico_otros> economicOther;
        private List<ccee_CargoEconomico_timbre> economicTimbre;
        private ccee_Colegiado referee;
        public static int REFEREE_NO_PRINT_DOC = 0;
        public static int REFEREE_PRINT_DOC = 1;
        public static int REFEREE_PRINT_NEW = 2;
        private const string regexPrint = @"^(1|4|8|14|15)?$";
        private const string regexStatus = @"^(1|2|3|7|15)?$";

        public Referee(int idReferee, CCEEEntities db)
        {
            this.idReferee = idReferee;
            this.db = db;
            //economic();
            refereeInfo();
        }

        public void economic()
        {
            economicReferee = (db.ccee_CargoEconomico_colegiado
                                .Where(o => o.fk_Colegiado == idReferee)).ToList();
            economicTimbre = (db.ccee_CargoEconomico_timbre
                                .Where(o => o.fk_Colegiado == idReferee)).ToList();
            //economicOther = (db.ccee_CargoEconomico_otros
            //                    .Where(o => o.fk_Colegiado == idReferee)).ToList();
        }

        public string getName()
        {
            return referee.Col_PrimerNombre + " " + referee.Col_SegundoNombre + " " +
                referee.Col_PrimerApellido + " " + referee.Col_SegundoApellido;
        }

        private void refereeInfo()
        {
            referee = db.ccee_Colegiado.Find(idReferee);
        }

        public decimal getMountTimbrePay()
        {
            string cent = GeneralFunction.getKeyTableVariable(db, Constant.Constant.table_var_aprox_decimal);
            decimal dCent = Convert.ToDecimal(cent);
            decimal salario = ((referee.Col_Salario == null) ? 0 : referee.Col_Salario.Value);
            decimal amount = GeneralFunction.generarMontoTimbre(salario, dCent);
            return amount;
        }

        public decimal getPsotumo()
        {
            return db.ccee_TipoCargoEconomico.Find(5).TCE_Valor.Value;
        }

        public decimal getMountColegiadoPay()
        {
            decimal amount = 0;
            List<ccee_TipoCargoEconomico> listTipoCargos = generarMontoColegiado();
            for (int j = 0; j < listTipoCargos.Count; j++)
            {
                amount += listTipoCargos[j].TCE_Valor.Value;
            }
            return amount;
        }


        private List<ccee_TipoCargoEconomico> generarMontoColegiado()
        {
            string cole = GeneralFunction.getKeyTableVariable(db, Constant.Constant.table_var_colegiado);
            int idEconomicReferee = Convert.ToInt32(cole);
            ccee_TipoCargoEconomico tempTypeEco = (from tca in db.ccee_TipoCargoEconomico
                                                   where tca.TCE_NoTipoCargoEconomico == idEconomicReferee
                                                   select tca).Single();

            string calculation = tempTypeEco.TCE_CondicionCalculo;
            string[] arrayCalculation = calculation.Split(',');
            List<int> intCalculation = new List<int>();
            foreach (string item in arrayCalculation)
            {
                intCalculation.Add(Convert.ToInt32(item));
            }

            var listE = from tca in db.ccee_TipoCargoEconomico
                        where intCalculation.Contains(tca.TCE_NoTipoCargoEconomico)
                        select tca;
            return listE.ToList();

        }

        public bool isUpdateStatus()
        {
            int typeReferee = referee.fk_TipoColegiado;

            Regex r = new Regex(regexStatus, RegexOptions.IgnoreCase);
            Match mat = r.Match(typeReferee.ToString());
            return ((mat.Success));
            
        }

        public int isPrintEnable()
        {
            int typeReferee = referee.fk_TipoColegiado;
            
            if(typeReferee == 0 || typeReferee == 16)
            {
                return REFEREE_PRINT_NEW;
            }else if(typeReferee == 3 && referee.Col_Estatus == 1)
            {
                return REFEREE_PRINT_DOC;
            }else if(typeReferee == 6 && referee.Col_Estatus == 1)
            {
                return REFEREE_PRINT_DOC;
            }else if (typeReferee == 7 && referee.Col_Estatus == 1)
            {
                return REFEREE_PRINT_DOC;
            }
            else if (typeReferee == 14 && referee.Col_Estatus == 0)
            {
                return REFEREE_PRINT_DOC;
            }
            else if (typeReferee == 15 && referee.Col_Estatus == 0)
            {
                return REFEREE_PRINT_DOC;
            }

            if (referee.Col_Estatus == 0) return REFEREE_NO_PRINT_DOC;

            Regex r = new Regex(regexPrint, RegexOptions.IgnoreCase);
            Match mat = r.Match(typeReferee.ToString());
            return ((mat.Success) ? REFEREE_PRINT_DOC : REFEREE_NO_PRINT_DOC);

            //switch (typeReferee)
            //{
            //    case 0:
            //        return REFEREE_NO_PRINT_DOC;
            //    case 1:
            //        return REFEREE_PRINT_DOC;
            //    case 2:
            //        return REFEREE_NO_PRINT_DOC;
            //    case 3:
            //        return REFEREE_PRINT_DOC;
            //    case 4:
            //        return REFEREE_NO_PRINT_DOC;//VERIFICAR CUANDO SE ENVIA BIEN
            //    case 5:
            //        return REFEREE_NO_PRINT_DOC;
            //    case 6:
            //        return REFEREE_PRINT_DOC;
            //    case 7:
            //        return REFEREE_NO_PRINT_DOC;//VERIFICAR CUANDO SE ENVIA BIEN
            //    case 8:
            //        return REFEREE_NO_PRINT_DOC;//VERIFICAR CUANDO SE ENVIA BIEN
            //    case 14:
            //        return REFEREE_PRINT_DOC;
            //    case 15:
            //        return REFEREE_PRINT_DOC;
            //    default:
            //        return REFEREE_NO_PRINT_DOC;
            //}
        }

        public void setEconomicReferee(int month, int year)
        {
            var existPay = (from car in economicReferee
                            where car.CEcC_FechaGeneracion.Value.Year == year &&
                            car.CEcC_FechaGeneracion.Value.Month == month
                            select car).Count();

            if (existPay == 0)
            {
                List<ccee_TipoCargoEconomico> listTipoCargos = generarMontoColegiado();
                ccee_CargoEconomico_colegiado tempEconomicoCol;

                for (int j = 0; j < listTipoCargos.Count; j++)
                {

                    tempEconomicoCol = new ccee_CargoEconomico_colegiado()
                    {
                        CEcC_Monto = listTipoCargos[j].TCE_Valor,
                        CEcC_FechaGeneracion = DateTime.Parse(month + "/1/" + year),
                        fk_TipoCargoEconomico = listTipoCargos[j].TCE_NoTipoCargoEconomico,
                        fk_Colegiado = idReferee,
                        fk_Pago = null
                    };
                    db.ccee_CargoEconomico_colegiado.Add(tempEconomicoCol);

                }
            }
        }

        public void setTimbre(int month, int year, decimal? salario, bool isOnlyPostumo)
        {
            int idBase = Constant.Constant.type_economic_base;
            int idPostumo = Constant.Constant.type_economic_postumo;

            var existPay = (from car in economicTimbre
                            where car.CEcT_FechaGeneracion.Value.Year == year &&
                            car.CEcT_FechaGeneracion.Value.Month == month
                            select car).Count();

            if (existPay == 0)
            {
                decimal valorP = db.ccee_TipoCargoEconomico.Find(idPostumo).TCE_Valor.Value;
                decimal montoTimbreGen = GeneralFunction.generarMontoTimbre(idReferee, db);

                ccee_CargoEconomico_timbre tempTim;
                ccee_CargoEconomico_timbre tempPostumo;

                tempTim = new ccee_CargoEconomico_timbre()
                {
                    CEcT_Monto = ((isOnlyPostumo) ? 0 : montoTimbreGen),
                    CEcT_FechaGeneracion = DateTime.Parse(month + "/1/" + year),
                    fk_TipoCargoEconomico = idBase,
                    fk_Colegiado = idReferee,
                    fk_Pago = null
                };

                tempPostumo = new ccee_CargoEconomico_timbre()
                {
                    CEcT_Monto = valorP,
                    CEcT_FechaGeneracion = DateTime.Parse(month + "/1/" + year),
                    fk_TipoCargoEconomico = idPostumo,
                    fk_Colegiado = idReferee,
                    fk_Pago = null
                };
                db.ccee_CargoEconomico_timbre.Add(tempTim);
                db.ccee_CargoEconomico_timbre.Add(tempPostumo);
            }
        }

        public void chageStatus()
        {
            referee.Col_Estatus = ((isActivo()) ? 1 : 0 );
            if (referee.fk_TipoColegiado == 8)
            {
                referee.Col_Estatus = 1;
            }
            db.Entry(referee).State = EntityState.Modified;
        }

        public void defineStatus(int status)
        {
            referee.Col_Estatus = status;
            db.Entry(referee).State = EntityState.Modified;
        }

        public bool isActivo()
        {
            int monthsOfInactive = 3;
            bool active = false;
            var model = economicReferee
                .Where(p => p.fk_Pago == null && p.CEcC_FechaGeneracion < DateTime.Today)
                .GroupBy(o => new
                {
                    Month = o.CEcC_FechaGeneracion.Value.Month,
                    Year = o.CEcC_FechaGeneracion.Value.Year
                })
                .Select(g => new
                {
                    idReferee = g.Select(p=>p.ccee_NoColegiado),
                    Date = DateTime.Parse(g.Key.Month + "/1/" + g.Key.Year)
                })
                .ToList();

            if(referee.fk_TipoColegiado == 3)
            {
                return (model.Count() <= monthsOfInactive);
            }

            active = (model.Count() <= monthsOfInactive);

            var modelT = economicTimbre
                .Where(p => p.fk_Pago == null && p.CEcT_FechaGeneracion < DateTime.Today)
                .GroupBy(o => new
                {
                    Month = o.CEcT_FechaGeneracion.Value.Month,
                    Year = o.CEcT_FechaGeneracion.Value.Year
                })
                .Select(g => new
                {
                    idReferee = g.Select(p => p.ccee_NoColegiado),
                    Date = DateTime.Parse(g.Key.Month + "/1/" + g.Key.Year)
                })
                .ToList();

            active = ((modelT.Count() <= monthsOfInactive) && active);
            return active;
        }
        

    }
}
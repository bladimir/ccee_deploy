﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonFichaColegiadoDir
    {
        public int? Dir_TipoDireccion { get; set; }
        public string Dir_Caserio { get; set; }
        public string tipoDireccion { get
            {
                switch (Dir_TipoDireccion)
                {
                    case 1:
                        return "Residencia";
                    case 2:
                        return "Trabajo";
                    default:
                        return "No definida";
                }
            }
        }
    }
}
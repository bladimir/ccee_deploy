﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonDocumentosPendientes
    {
        public int Col_NoColegiado { get; set; }
        public string Col_PrimerApellido { get; set; }
        public string Col_SegundoApellido { get; set; }
        public string Col_PrimerNombre { get; set; }
        public string Col_SegundoNombre { get; set; }
        public string Uni_NombreUniversidad { get; set; }
        public string Sed_Direccion { get; set; }
        public string MyProperty { get; set; }
        public string Col_FechaColegiacion {
            get
            {
                return ((fechaColegiacion != null) ? fechaColegiacion.Value.ToString("dd/MM/yyyy") : "");
            }
        }
        public string Exp_NombreExpediente { get; set; }
        public string ERe_NombreRequisito { get; set; }
        public DateTime? fechaColegiacion { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels._Tools;
using webcee.Class.JsonModels.Timbre;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_TimbreController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Timbre
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_Timbre/5/5
        [Route("api/Api_Timbre/{cashier}/{count}/{idValuetimbre}")]
        public IEnumerable<ResponseTimbreJson> Get(int cashier, int count, int idValuetimbre)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                var tempTimbre = (from tim in db.ccee_Timbre
                                 join hoj in db.ccee_HojaTimbre on tim.fk_HojaTimbre equals hoj.HoT_NoHojaTimbre
                                 where hoj.fk_Cajero == cashier &&
                                        tim.fk_ValorTimbre == idValuetimbre &&
                                        tim.Tim_Estatus == Constant.timbre_status_enable
                                 orderby hoj.HoT_NumeroHoja, tim.Tim_NumeroTimbre ascending
                                 select new ResponseTimbreJson()
                                 {
                                     id = tim.Tim_NoTimbre,
                                     numberTimbre = tim.Tim_NumeroTimbre,
                                     numberPage = hoj.HoT_NumeroHoja
                                 }).Take(count);

                int max = tempTimbre.Count();

                if(max == count)
                {
                    Autorization.saveOperation(db, autorization, "Obteniendo timbres");
                    return tempTimbre;
                }
                return null;   
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_Timbre
        public IHttpActionResult Post(PostBodyTimbresJson timbre)
        {

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    string autorization = Request.Headers.Authorization.Parameter;
                    if (!Autorization.enableAutorization(db, autorization)) return null;
                    foreach (PostBodyTimbreJson tempTimbre in timbre.timbres)
                    {

                        decimal valueTimbre = tempTimbre.value;
                        if (tempTimbre.content != null)
                        {
                            foreach (ResponseTimbreJson tempTim in tempTimbre.content)
                            {
                                ccee_Timbre cceeTimbre = db.ccee_Timbre.Find(tempTim.id);
                                if (cceeTimbre == null)
                                {
                                    continue;
                                }

                                cceeTimbre.Tim_Estatus = Constant.timbre_status_sold;
                                cceeTimbre.Tim_Fecha = DateTime.Today;
                                cceeTimbre.fk_Pago = timbre.idPay;
                                cceeTimbre.Tim_Colegiado = timbre.referee;
                                db.Entry(cceeTimbre).State = EntityState.Modified;
                            }
                        }
                    }
                    db.SaveChanges();
                    Autorization.saveOperation(db, autorization, "Agregando timbre");
                    transaction.Commit();
                    return Ok(HttpStatusCode.OK);
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return Ok(HttpStatusCode.BadRequest);
                }

            }


        }

        // PUT: api/Api_Timbre/5
        public void Put(int id, PostBodyTimbreJson timbre)
        {
        }

        // DELETE: api/Api_Timbre/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

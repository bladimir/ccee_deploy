﻿(function () {

    var app = angular.module('event', []);

    app.controller("assing", ['$scope', '$http', 'CONSTANT', function ($scope, $http, CONSTANT) {
        $scope.events;
        $scope.subEvents;
        $scope.showSegment = 0;
        $scope.numeroColegiado = CONSTANT.REGEX_DIGIT;
        $scope.infoReferee = -1;
        $scope.idTempReferee;
        $scope.aggregateEvent = "";
        $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
        $scope.statusEvent = "";
        $scope.buttonStatusAssing = -1;

        //carga lista de eventos segun la fecha del dia presente
        $http.get(CONSTANT.URL_API_EVENTASSING)
                .success(function (data) {
                    console.log(data);
                    if (data != null) {
                        $scope.events = data;
                        $scope.showSegment = 1;

                    }
                })
                .error(function (data) {
                    console.log(data);
                });


        //carga todos los sub eventos segun el seleccionado
        $scope.getSubEvent = function () {
            console.log($scope.selectEvents.idEvent);
            $http.get(CONSTANT.URL_API_EVENTASSING + "/" + $scope.selectEvents.idEvent)
                .success(function (data) {
                    $scope.subEvents = data;
                    $scope.showSegment = 2;
                })
                .error(function (data) {
                    console.log(data);
                });
        }

        //Al seleccionar un sub evento este mostrara le form para buscar un colegiado
        $scope.showReffere = function () {
            $scope.showSegment = 3;
        }

        //obtiene el colegiado
        $scope.getReferre = function () {
            if ($scope.formreferre.$valid) {

                $scope.idTempReferee = $scope.idReferre;
                $scope.aggregateEvent = "";

                $http.get(CONSTANT.URL_API_EVENTASSING + "/" + $scope.idReferre + "/" + $scope.selectSubEvents.idEvent)
                .success(function (data) {
                    console.log(data);
                    if (data != null) {
                        $scope.inforefereeName = data.name;
                        $scope.inforefereeLastname = data.lastname;
                        $scope.inforefereeStatus = data.status;
                        $scope.buttonStatusAssing = data.statusAssign;
                        $scope.inforefereeStatusAssing = $scope.titleOfEvent(data.statusAssign);
                    } else {
                        $scope.cleanReferee();
                    }
                })
                .error(function (data) {
                    console.log("error");
                    console.log(data);
                });
            }

        }

        //Asignacion del colegiado si este no existe
        $scope.addAssign = function (status) {
            var informacion = {
                idReferre: $scope.idTempReferee,
                idSubEvent: $scope.selectSubEvents.idEvent,
                idStatus: status
            }

            $http.post(CONSTANT.URL_API_EVENTASSING, JSON.stringify(informacion),
                {
                    headers: {
                        'Content-Type': CONSTANT.HEADER_CONTENT_TYPE_JSON
                    }
                })
            .success(function (data) {
                console.log(data);
                if (data == 200) {
                    $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
                    $scope.aggregateEvent = $scope.titleOfEvent(status);
                }
            })
            .error(function (data) {
                console.log("error");
                console.log(data);
            });
        }

        //Cambio de stado de colegiado si este ya existe
        $scope.changeAssing = function (status) {

            var informacion = {
                idReferre: $scope.idTempReferee,
                idSubEvent: $scope.selectSubEvents.idEvent,
                idStatus: status
            }

            $http.put(CONSTANT.URL_API_EVENTASSING, JSON.stringify(informacion),
                {
                    headers: {
                        'Content-Type': CONSTANT.HEADER_CONTENT_TYPE_JSON
                    }
                })
            .success(function (data) {
                console.log(data);
                if (data == 200) {
                    $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
                    $scope.aggregateEvent = $scope.titleOfEvent(status);
                }
            })
            .error(function (data) {
                console.log("error");
                console.log(data);
            });
        }

        //Conforme al estado este saca el estado 
        $scope.titleOfEvent = function (status) {
            if (status == 1) {
                return "Inscrito";
            } else if (status == 2) {
                return "Asistente";
            } else if (status == 3) {
                return "Cancelado";
            } else if (status == 0) {
                return "No Asignado";
            } else if (status == -1) {
                return "";
            }
        }


        //Limpieza de los datos de colegiado
        $scope.cleanReferee = function () {
            idReferre = "";
            $scope.inforefereeName = "";
            $scope.inforefereeLastname = "";
            $scope.inforefereeStatus = -1;
            $scope.buttonStatusAssing = -1;
            $scope.inforefereeStatusAssing = $scope.titleOfEvent(-1);
            $scope.aggregateEvent = "";
        }

        //Minimiza la vista de la notificacion
        $scope.hideNotify = function () {
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.cleanReferee();
        }

    }])

    app.controller("viewEvents", ['$scope', '$http', '$window', 'CONSTANT', function ($scope, $http, $window, CONSTANT) {

        $scope.events;
        $scope.subEvents;
        $scope.tempassintents;
        $scope.assintents;
        $scope.showSegment = 0;
        $scope.dataPagination = -1;
        $scope.showPagination = true;


        $http.get(CONSTANT.URL_API_EVENTASSING)
                .success(function (data) {
                    console.log(data);
                    if (data != null) {
                        $scope.events = data;
                        $scope.showSegment = 1;
                    }
                })
                .error(function (data) {
                    console.log(data);
                });

        //carga todos los sub eventos segun el seleccionado
        $scope.getSubEvent = function () {
            console.log($scope.selectEvents.idEvent);
            $http.get(CONSTANT.URL_API_EVENTASSING + "/" + $scope.selectEvents.idEvent)
                .success(function (data) {
                    $scope.subEvents = data;
                    $scope.showSegment = 2;
                })
                .error(function (data) {
                    console.log(data);
                });
        }

        $scope.getListAssistent = function () {
            console.log($scope.selectSubEvents.idEvent);
            $http.get(CONSTANT.URL_API_EVENTASSISTENT + "/" + $scope.selectSubEvents.idEvent)
                .success(function (data) {
                    if (data != null) {
                        $scope.tempassintents = data;
                        $scope.dataPagination = -1;
                        $scope.paginationUp();
                        $scope.showSegment = 3;
                    }
                })
                .error(function (data) {
                    console.log(data);
                });
        }

        $scope.printDocument = function (idReferee, typeDocu) {
            $window.open('Print/' + idReferee.id + '/' + $scope.selectSubEvents.idEvent + '/' + typeDocu );
        }

        $scope.paginationUp = function () {
            $scope.dataPagination++;
            $scope.assintents = [];
            for (var i = ($scope.dataPagination * CONSTANT.PAGINATION_CANT) ; i < $scope.tempassintents.length && i < (($scope.dataPagination * CONSTANT.PAGINATION_CANT) + CONSTANT.PAGINATION_CANT) ; i++) {
                $scope.assintents.push($scope.tempassintents[i]);
            }
        }

        $scope.paginationDown = function () {
            $scope.dataPagination--;
            $scope.assintents = [];
            for (var i = ($scope.dataPagination * CONSTANT.PAGINATION_CANT) ; i < $scope.tempassintents.length && i < (($scope.dataPagination * CONSTANT.PAGINATION_CANT) + CONSTANT.PAGINATION_CANT) ; i++) {
                $scope.assintents.push($scope.tempassintents[i]);
            }
        }

        $scope.$watch('search', function (text) {

            if ($scope.tempassintents == null) return null;

            if (text == "") {
                $scope.dataPagination = -1;
                $scope.paginationUp();
                $scope.showPagination = true;
                return null;
            }

            $scope.showPagination = false;
            if (!CONSTANT.REGEX_DIGIT.test(text)) {
                $scope.assintents = [];
                for (var i = 0; i < $scope.tempassintents.length; i++) {
                    var re = new RegExp("" + text.toLowerCase() + "");
                    if (re.test($scope.tempassintents[i].name.toLowerCase() + " " + $scope.tempassintents[i].lastname.toLowerCase() )) {
                        $scope.assintents.push($scope.tempassintents[i]);
                    }
                }
            } else {
                $scope.assintents = [];
                for (var i = 0; i < $scope.tempassintents.length; i++) {
                    var re = new RegExp("" + text + "");
                    if (re.test($scope.tempassintents[i].id)) {
                        $scope.assintents.push($scope.tempassintents[i]);
                    }
                }
            }
        })
        

    }])

    app.controller("events", ['$scope', '$http', '$window', 'CONSTANT', function ($scope, $http, $window, CONSTANT) {

        $scope.events;
        $scope.docs;
        $scope.showStatusInfo = false;
        $scope.mensajeSubEvent = "";
        $scope.subEvents;

        console.log("log")
        $http.get(CONSTANT.URL_API_EVENT)
                .success(function (data) {
                    if (data != null) {
                        $scope.events = data;
                    }
                })
                .error(function (data) {
                    console.log(data);
                });

        $http.get(CONSTANT.URL_API_DOCEVENT)
                .success(function (data) {
                    if (data != null) {
                        $scope.docs = data;
                    }
                })
                .error(function (data) {
                    console.log(data);
                });


        $scope.showInfoEvent = function () {
            $scope.showStatusInfo = true;
            $scope.eventName = $scope.selectEvents.name;
            $scope.eventDesciption = $scope.selectEvents.description;
            $scope.eventDateI = $scope.selectEvents.initation;
            $scope.eventDateF = $scope.selectEvents.finalize;
            $scope.eventStatus = $scope.stateOfEvent($scope.selectEvents.status);
        }

        $scope.addEvent = function () {

            if ($scope.formaddevent.$valid) {

                var informacion = {
                    name: $scope.eventName,
                    description: $scope.eventDesciption,
                    initation: $scope.eventDateI,
                    finalize: $scope.eventDateF,
                    status: $scope.eventSelectStatus
                }


                $http.post(CONSTANT.URL_API_EVENT, JSON.stringify(informacion),
                    {
                        headers: {
                            'Content-Type': CONSTANT.HEADER_CONTENT_TYPE_JSON
                        }
                    })
                .success(function (data) {
                    console.log(data);
                    if (data == 200) {
                        $scope.cleanData();
                        $scope.rechargeEvent()
                    }
                })
                .error(function (data) {
                    console.log("error");
                    console.log(data);
                });


            }
        }

        $scope.addSubEvent = function () {

            if ($scope.formsubevent.$valid) {
                var informacion = {
                    name: $scope.subNombre,
                    description: $scope.subDescription,
                    events: $scope.selectEvents.id,
                    document: $scope.selectDocPrint.id,
                    statusPrint: $scope.docStatusPrint
                }


                $http.post(CONSTANT.URL_API_SUBEVENT, JSON.stringify(informacion),
                    {
                        headers: {
                            'Content-Type': CONSTANT.HEADER_CONTENT_TYPE_JSON
                        }
                    })
                .success(function (data) {
                    if (data == 200) {
                        $scope.cleanDataSub();
                        $scope.mensajeSubEvent = CONSTANT.MENSAJE_SUCCESS_SUB_EVENT;
                    } else {
                        $scope.mensajeSubEvent = "";
                    }
                })
                .error(function (data) {
                    console.log("error");
                    console.log(data);
                });
            }

        }


        $scope.getSubEvent = function () {
            console.log($scope.selectEvents.id);
            $http.get(CONSTANT.URL_API_EVENTASSING + "/" + $scope.selectEvents.id)
                .success(function (data) {
                    $scope.subEvents = data;
                })
                .error(function (data) {
                    console.log(data);
                });
        }


        $scope.getOnlySubEvent = function () {
            console.log($scope.selectEventsSubEdit);
            $http.get(CONSTANT.URL_API_SUBEVENT + "/" + $scope.selectEventsSubEdit.idEvent)
                .success(function (data) {
                    if (data != null) {
                        $scope.subNombre = data.name;
                        $scope.subDescription = data.description;
                    }
                })
                .error(function (data) {
                    console.log(data);
                });
        }



        $scope.changeEvent = function () {
            if ($scope.formaddevent.$valid && $scope.selectEvents != null) {

                

                var statusEvent = $scope.selectEvents.status;
                if ($scope.eventSelectStatus!=null) {
                    statusEvent = $scope.eventSelectStatus;
                }


                var informacion = {
                    name: $scope.eventName,
                    description: $scope.eventDesciption,
                    initation: $scope.eventDateI,
                    finalize: $scope.eventDateF,
                    status: statusEvent
                }
                
                $http.put(CONSTANT.URL_API_EVENT + "/" + $scope.selectEvents.id, JSON.stringify(informacion),
                    {
                        headers: {
                            'Content-Type': CONSTANT.HEADER_CONTENT_TYPE_JSON
                        }
                    })
                .success(function (data) {
                    if (data == 200) {
                        $scope.cleanData();
                        $scope.rechargeEvent();
                    } else {
                        $scope.mensajeSubEventEdit = "";
                    }
                })
                .error(function (data) {
                    console.log("error");
                    console.log(data);
                });
            }
        }



        $scope.changeSubEvent = function () {
            if ($scope.formsubevent.$valid) {
                var informacion = {
                    name: $scope.subNombre,
                    description: $scope.subDescription,
                    events: 0,
                    document: 0,
                    statusPrint: 0
                }


                $http.put(CONSTANT.URL_API_SUBEVENT + "/" + $scope.selectEventsSubEdit.idEvent, JSON.stringify(informacion),
                    {
                        headers: {
                            'Content-Type': CONSTANT.HEADER_CONTENT_TYPE_JSON
                        }
                    })
                .success(function (data) {
                    if (data == 200) {
                        $scope.cleanDataSub();
                        $scope.mensajeSubEvent = "Sub evento editado";
                    } else {
                        $scope.mensajeSubEvent = "";
                    }
                })
                .error(function (data) {
                    console.log("error");
                    console.log(data);
                });
            }
        }


        //Conforme al estado este saca el estado 
        $scope.stateOfEvent = function (status) {
            if (status == 1) {
                return "Inscripcion";
            } else if (status == 2) {
                return "En Proceso";
            } else if (status == 3) {
                return "Cancelado";
            } else if (status == 4) {
                return "Concluído";
            } else if (status == 0) {
                return "On Hold";
            } else if (status == -1) {
                return "";
            }
        }

        $scope.cleanData = function () {
            $scope.showStatusInfo = false;
            $scope.eventName = "";
            $scope.eventDesciption = "";
            $scope.eventDateI = "";
            $scope.eventDateF = "";
            $scope.eventStatus = "";
        }

        $scope.cleanDataSub = function () {
            $scope.subNombre = "";
            $scope.subDescription = "";
            $scope.selectEventsSubEdit();
        }

        $scope.rechargeEvent = function () {
            $http.get(CONSTANT.URL_API_EVENT)
                .success(function (data) {
                    if (data != null) {
                        $scope.events = data;
                    }
                })
                .error(function (data) {
                    console.log(data);
                });
        }



    }])


    app.controller("assingNoReferee", ['$scope', '$http', '$window', 'CONSTANT', function ($scope, $http, $window, CONSTANT) {

        $scope.events;
        $scope.subEvents;
        $scope.assintents;
        $scope.showSegment = 0;
        $scope.tempassintents;
        $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
        $scope.dataPagination = -1;
        $scope.showPagination = true;
        $scope.showInscription = true;
        $scope.idEditNoReferee = 0;

        $http.get(CONSTANT.URL_API_EVENTASSING)
                .success(function (data) {
                    console.log(data);
                    if (data != null) {
                        $scope.events = data;
                        $scope.showSegment = 1;
                    }
                })
                .error(function (data) {
                    console.log(data);
                });


        //carga todos los sub eventos segun el seleccionado
        $scope.getSubEvent = function () {
            $http.get(CONSTANT.URL_API_EVENTASSING + "/" + $scope.selectEvents.idEvent)
                .success(function (data) {
                    $scope.subEvents = data;
                    $scope.showSegment = 2;
                })
                .error(function (data) {
                    console.log(data);
                });
        }

        $scope.addNoReferee = function () {


            if ($scope.formnoreferee.$valid) {
                var informacion = {
                    name: $scope.noReferreName,
                    lastname: $scope.noReferreLastname,
                    nit: $scope.noReferreNit,
                    movil: $scope.noReferreMovil,
                    address: $scope.noReferreAddress,
                    remark: $scope.noReferreRemarke,
                    idSubEvent: $scope.selectSubEvents.idEvent,
                    idType: 1,
                    idNoReferee: 0
                }

                console.log(informacion);

                $http.post(CONSTANT.URL_API_NOREFEREEEVENT, JSON.stringify(informacion),
                    {
                        headers: {
                            'Content-Type': CONSTANT.HEADER_CONTENT_TYPE_JSON
                        }
                    })
                .success(function (data) {
                    console.log(data);
                    if (data == 200) {
                        $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
                        console.log(data);
                    } else {
                        $scope.mensajeSubEvent = "";
                    }
                })
                .error(function (data) {
                    console.log("error");
                    console.log(data);
                });


            }

        }

        $scope.getListAssistent = function () {
            console.log($scope.selectSubEvents.idEvent);
            $http.get(CONSTANT.URL_API_NOREFEREEEVENT + "/" + $scope.selectSubEvents.idEvent)
                .success(function (data) {
                    if (data != null) {
                        $scope.tempassintents = data;
                        $scope.dataPagination = -1;
                        $scope.paginationUp();
                        $scope.showSegment = 3;
                    }
                })
                .error(function (data) {
                    console.log(data);
                });
        }


        $scope.getDataNoReferee = function (refereee) {
            $scope.idEditNoReferee = refereee.id;
            $scope.noReferreName = refereee.name;
            $scope.noReferreLastname = refereee.lastname;
            $scope.noReferreNit = refereee.nit;
            $scope.noReferreMovil = refereee.movil;
            $scope.noReferreAddress = refereee.address;
            $scope.noReferreRemarke = refereee.remark;
            $scope.showInscription = true;
            $scope.assintents = [];
        }

        $scope.editDataNoReferee = function () {
            if ($scope.formnoreferee.$valid) {
                var informacion = {
                    name: $scope.noReferreName,
                    lastname: $scope.noReferreLastname,
                    nit: $scope.noReferreNit,
                    movil: $scope.noReferreMovil,
                    address: $scope.noReferreAddress,
                    remark: $scope.noReferreRemarke,
                    idSubEvent: 0,
                    idType: 1,
                    idNoReferee: 0
                }

                console.log(informacion);

                $http.put(CONSTANT.URL_API_NOREFEREEEVENT + "/" + $scope.idEditNoReferee, JSON.stringify(informacion),
                    {
                        headers: {
                            'Content-Type': CONSTANT.HEADER_CONTENT_TYPE_JSON
                        }
                    })
                .success(function (data) {
                    console.log(data);
                    if (data == 200) {
                        $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
                        $scope.tempassintents = null
                        console.log(data);
                    } else {
                        $scope.mensajeSubEvent = "";
                    }
                })
                .error(function (data) {
                    console.log("error");
                    console.log(data);
                });


            }
        }


        $scope.addNoRefereeList = function (refereee) {
            var informacion = {
                name: "",
                lastname: "",
                nit: "",
                movil: 0,
                address: "",
                remark: "",
                idSubEvent: $scope.selectSubEvents.idEvent,
                idType: 2,
                idNoReferee: refereee.id
            }

            console.log(informacion);

            $http.post(CONSTANT.URL_API_NOREFEREEEVENT, JSON.stringify(informacion),
                {
                    headers: {
                        'Content-Type': CONSTANT.HEADER_CONTENT_TYPE_JSON
                    }
                })
            .success(function (data) {
                console.log(data);
                if (data == 200) {
                    $scope.noReferreName = refereee.name;
                    $scope.noReferreLastname = refereee.lastname;
                    $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
                    console.log(data);
                } else {
                    $scope.mensajeSubEvent = "";
                }
            })
            .error(function (data) {
                console.log("error");
                console.log(data);
            });
        }


        $scope.searchNoReferre = function (text) {
            $scope.assintents = [];
            for (var i = 0; i < $scope.tempassintents.length; i++) {
                var re = new RegExp("" + text.toLowerCase() + "");
                if (re.test($scope.tempassintents[i].name.toLowerCase() + " " + $scope.verfyLastName($scope.tempassintents[i].lastname).toLowerCase() )) {
                    $scope.assintents.push($scope.tempassintents[i]);
                }
            }
        }


        $scope.verfyLastName = function (text) {
            if (text != null) {
                return text;
            } else {
                return "";
            }
        }

        $scope.$watch('searchNoReferee', function (text) {

            if (text == null) return null;
            if (text == "") {
                $scope.showInscription = true;
                $scope.assintents = [];
                return null;
            }

            $scope.showInscription = false;
            if ($scope.tempassintents == null) {
                $http.get(CONSTANT.URL_API_NOREFEREEEVENT )
                    .success(function (data) {
                        if (data != null) {
                            console.log(data);
                            $scope.tempassintents = data;
                            $scope.searchNoReferre(text);
                        }
                    })
                    .error(function (data) {
                        console.log(data);
                    });
            } else {
                $scope.searchNoReferre(text);
            }
        })

        $scope.$watch('search', function (text) {


            if ($scope.tempassintents == null) return null;

            if (text == "") {
                $scope.dataPagination = -1;
                $scope.paginationUp();
                $scope.showPagination = true;
                return null;
            }

            $scope.showPagination = false;
            if (!CONSTANT.REGEX_DIGIT.test(text)) {
                $scope.assintents = [];
                for (var i = 0; i < $scope.tempassintents.length; i++) {
                    var re = new RegExp("" + text.toLowerCase() + "");
                    if (re.test($scope.tempassintents[i].name.toLowerCase() + " " + $scope.verfyLastName($scope.tempassintents[i].lastname).toLowerCase() )) {
                        $scope.assintents.push($scope.tempassintents[i]);
                    }
                }
            } 
        })



        $scope.paginationUp = function () {
            $scope.dataPagination++;
            $scope.assintents = [];
            for (var i = ($scope.dataPagination * CONSTANT.PAGINATION_CANT) ; i < $scope.tempassintents.length && i < (($scope.dataPagination * CONSTANT.PAGINATION_CANT) + CONSTANT.PAGINATION_CANT) ; i++) {
                $scope.assintents.push($scope.tempassintents[i]);
            }
        }

        $scope.paginationDown = function () {
            $scope.dataPagination--;
            $scope.assintents = [];
            for (var i = ($scope.dataPagination * CONSTANT.PAGINATION_CANT) ; i < $scope.tempassintents.length && i < (($scope.dataPagination * CONSTANT.PAGINATION_CANT) + CONSTANT.PAGINATION_CANT) ; i++) {
                $scope.assintents.push($scope.tempassintents[i]);
            }
        }





        $scope.printDocument = function (idNoReferee, typeDocu) {
            $window.open('PrintNoReferee/' + idNoReferee.id + '/' + $scope.selectSubEvents.idEvent + '/' + typeDocu);
        }

        $scope.cleanReferee = function () {
            idReferre = "";
            $scope.noReferreName = "";
            $scope.noReferreLastname = "";
            $scope.noReferreNit = "";
            $scope.noReferreMovil = "";
            $scope.noReferreAddress = "";
            $scope.noReferreRemarke = "";
        }

        //Minimiza la vista de la notificacion
        $scope.hideNotify = function () {
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.cleanReferee();
        }

    }])


})();
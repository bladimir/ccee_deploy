﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.JsonModels.DepositSlit;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_DepositSlitController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_DepositSlit
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        public ResponseDepositSlitJson Get(int id)
        {
            try
            {
                var dep = (from bol in db.ccee_BoletaDeDeposito
                           where bol.BdD_NoBoletaDeDeposito == id
                           select new ResponseDepositSlitJson()
                           {
                               idCahier = bol.fk_Cajero.Value,
                               date = bol.BdD_Fecha.Value,
                               idBank = bol.fk_CuentaBancaria.Value,
                               boleta = bol.BdD_NoDeposito
                           }).FirstOrDefault();
                return dep;
            }
            catch (Exception)
            {

                return null;
            }
            
        }


        [Route("api/Api_DepositSlit/{date}/{idCashier}/{other}/{dateR}/{o}")]
        // GET: api/Api_DepositSlit/5 valores 5 boletas sin asignar
        public IEnumerable<ResponseDepositSlitNullJson> Get(string date, int idCashier, int other, string dateR, int o)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                var pLis = db.Database.SqlQuery<ResponseDepositSlitNullJson>(queryData(date, idCashier, dateR)).ToList();
                Autorization.saveOperation(db, autorization, "Obteniendo interfaz contable");
                return pLis;
            }
            catch (Exception)
            {
                return null;
            }
            
        }


        [Route("api/Api_DepositSlit/{idDepo}/{type}")]
        public IEnumerable<ResponseDepositSlitNullJson> Get(int idDepo, int type)
        {
            
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                var pLis = from dep in db.ccee_BoletaDeDeposito
                            join doc in db.ccee_DocContable on dep.BdD_NoBoletaDeDeposito equals doc.fk_BoletaDeposito
                            where dep.BdD_NoBoletaDeDeposito == idDepo
                            select new ResponseDepositSlitNullJson()
                            {
                                con = doc.DCo_NoDocContable,
                                DCo_NoRecibo = doc.DCo_NoRecibo,
                                nombre = doc.DCo_ANombre,
                                fk_BoletaDeposito = dep.BdD_NoBoletaDeDeposito
                            };
                Autorization.saveOperation(db, autorization, "Obteniendo interfaz contable");

                return pLis;
            }
            catch (Exception)
            {
                return null;
            }
            

        }


        [Route("api/Api_DepositSlit/{idC}/{idB}/{boleta}/{dateB}")]
        public int Get(int idC, int idB, string boleta, string dateB )
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return 0;
                DateTime? dateDeposit = GeneralFunction.changeStringofDateSimple(dateB);
                var idDep = (db.ccee_BoletaDeDeposito
                            .Where(o => o.BdD_NoDeposito.Equals(boleta))
                            .Where(p => p.fk_Cajero == idC)
                            .Where(q => q.fk_CuentaBancaria == idB)
                            .Where(r => r.BdD_Fecha == dateDeposit)
                            .Select(s=>s.BdD_NoBoletaDeDeposito))
                            .FirstOrDefault();
                Autorization.saveOperation(db, autorization, "Obteniendo interfaz contable");
                return idDep;

            }
            catch (Exception)
            {
                return 0;
            }
        }

        [Route("api/Api_DepositSlit/{date}/{idCashier}/{o}")]
        // GET: api/Api_DepositSlit/5
        public List<ResponseDepositSlitJson> Get(string date, int idCashier, int o)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                DateTime? dateDeposit = GeneralFunction.changeStringofDateSimple(date);
                var dep = (from bol in db.ccee_BoletaDeDeposito
                          where bol.fk_Cajero == idCashier &&
                          bol.BdD_Fecha.Value == dateDeposit  
                          select new ResponseDepositSlitJson()
                          {
                              idCahier = bol.fk_Cajero.Value,
                              date = bol.BdD_Fecha.Value,
                              idBank = bol.fk_CuentaBancaria.Value,
                              boleta = bol.BdD_NoDeposito
                          }).ToList();

                

                Autorization.saveOperation(db, autorization, "Obteniendo interfaz contable por cajero y fecha");
                return dep;
            }
            catch (Exception)
            {
                return null;
            }

        }


        // POST: api/Api_DepositSlit
        public int Post(RequestDepositSlitJson deposit)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return 0;
                DateTime? dateDeposit = GeneralFunction.changeStringofDateSimple(deposit.date);
                ccee_BoletaDeDeposito tempDeposit = (db.ccee_BoletaDeDeposito
                                                    .Where(o => o.BdD_NoDeposito.Equals(deposit.boleta))
                                                    .Where(p => p.fk_Cajero == deposit.idCahier)
                                                    .Where(q => q.fk_CuentaBancaria == deposit.idBank)
                                                    .Where(r => r.BdD_Fecha == dateDeposit))
                                                    .FirstOrDefault();

                Autorization.saveOperation(db, autorization, "Agregando interfaz contable");
                if (tempDeposit != null)
                {
                    return tempDeposit.BdD_NoBoletaDeDeposito;
                }else
                {
                    ccee_BoletaDeDeposito newDeposit = new ccee_BoletaDeDeposito();
                    newDeposit.BdD_NoDeposito = deposit.boleta;
                    newDeposit.BdD_Fecha = dateDeposit;
                    newDeposit.fk_Cajero = deposit.idCahier;
                    newDeposit.fk_CuentaBancaria = deposit.idBank;
                    db.ccee_BoletaDeDeposito.Add(newDeposit);
                    db.SaveChanges();
                    return newDeposit.BdD_NoBoletaDeDeposito;
                }
            }
            catch (Exception)
            {
                return 0;
            }


        }

        // PUT: api/Api_DepositSlit/5
        public IHttpActionResult Put(int id, RequestDepositSlitListJson listDocs)
        {

            using (var transaction = db.Database.BeginTransaction())
            {

                try
                {
                    string autorization = Request.Headers.Authorization.Parameter;
                    if (!Autorization.enableAutorization(db, autorization)) return null;
                    foreach (ResponseDepositSlitNullJson temNull in listDocs.listNulls)
                    {
                        ccee_DocContable tempDoc = db.ccee_DocContable.Find(temNull.con);
                        tempDoc.fk_BoletaDeposito = null;
                        db.Entry(tempDoc).State = EntityState.Modified;
                    }

                    foreach (ResponseDepositSlitNullJson tempNoNull in listDocs.listNoNulls)
                    {
                        ccee_DocContable tempDoc = db.ccee_DocContable.Find(tempNoNull.con);
                        tempDoc.fk_BoletaDeposito = id;
                        db.Entry(tempDoc).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    Autorization.saveOperation(db, autorization, "Actualizando interfaz contable");
                    transaction.Commit();
                    return Ok(HttpStatusCode.OK);
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return Ok(HttpStatusCode.BadRequest);
                }

            }
        }

        // DELETE: api/Api_DepositSlit/5
        public IHttpActionResult Delete(int id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {

                try
                {
                    string autorization = Request.Headers.Authorization.Parameter;
                    if (!Autorization.enableAutorization(db, autorization)) return null;
                    ccee_BoletaDeDeposito temDep = db.ccee_BoletaDeDeposito.Find(id);
                    List<ccee_DocContable> listDoc = (db.ccee_DocContable
                                                        .Where(o => o.fk_BoletaDeposito == temDep.BdD_NoBoletaDeDeposito))
                                                        .ToList();
                    foreach (ccee_DocContable tempDoc in listDoc)
                    {
                        tempDoc.fk_BoletaDeposito = null;
                        db.Entry(tempDoc).State = EntityState.Modified;
                    }

                    db.ccee_BoletaDeDeposito.Remove(temDep);
                    db.SaveChanges();
                    Autorization.saveOperation(db, autorization, "Eliminando interfaz contable");
                    transaction.Commit();
                    return Ok(HttpStatusCode.OK);
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return Ok(HttpStatusCode.BadRequest);
                }

            }
        }

        private string queryData(string date, int cashier, string dateR)
        {

            string query = "select lista.con, ccee_DocContable.DCo_ANombre as nombre, " +
                            "ccee_DocContable.DCo_NoRecibo, ccee_DocContable.fk_BoletaDeposito " +
                            "from ccee_DocContable, ( " +
	                            "select ccee_Pago.Pag_NoPago as pago, ccee_CargoEconomico_timbre.fk_DocContable as con " +
	                            "from ccee_CargoEconomico_timbre, ccee_Pago " +
	                            "where ccee_CargoEconomico_timbre.fk_Pago = ccee_Pago.Pag_NoPago " +
	                            "and Pag_Fecha >= '"+ date + "' " +
                                "and Pag_Fecha <= '" + dateR + "' " +
                                "and ccee_Pago.fk_Cajero = " +cashier+" " +
                                "UNION " +
	                            "select ccee_Pago.Pag_NoPago as pago, ccee_CargoEconomico_colegiado.fk_DocContable as con " +
	                            "from ccee_CargoEconomico_colegiado, ccee_Pago " +
	                            "where ccee_CargoEconomico_colegiado.fk_Pago = ccee_Pago.Pag_NoPago " +
                                "and Pag_Fecha >= '" + date + "' " +
                                "and Pag_Fecha <= '" + dateR + "' " +
                                "and ccee_Pago.fk_Cajero = " +cashier+" " +
	                            "UNION " +
	                            "select ccee_Pago.Pag_NoPago as pago, ccee_CargoEconomico_otros.fk_DocContable as con " +
	                            "from ccee_CargoEconomico_otros, ccee_Pago " +
	                            "where ccee_CargoEconomico_otros.fk_Pago = ccee_Pago.Pag_NoPago " +
                                "and Pag_Fecha >= '" + date + "' " +
                                "and Pag_Fecha <= '" + dateR + "' " +
                                "and ccee_Pago.fk_Cajero = " +cashier+" " +
                                ") as lista " +
                            "where ccee_DocContable.DCo_NoDocContable = lista.con " +
                            "and ccee_DocContable.fk_BoletaDeposito is null " +
                            "and ccee_DocContable.DCo_Eliminado is null ";

            return query;
        }
        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

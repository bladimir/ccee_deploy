﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.CloseCashRange
{
    public class RequestCloseCashRangeJson
    {
        public DateTime dateR { get; set; }
        public int idCashier { get; set; }
        public string direction { get; set; }
    }
}
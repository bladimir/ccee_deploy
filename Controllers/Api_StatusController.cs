﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_StatusController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();
        private const string regexVerifyStatus = @"^(1|4|7)?$";

        public class refereeTemp
        {
            public int NoReferee { get; set; }
            public int? status { get; set; }
            public decimal? salario { get; set; }
            public int? active { get; set; }
            public DateTime fecha { get; set; }
        }

        // GET: api/Api_Status
        public bool Get()
        {
            return true;
        }

        // GET: api/Api_Status/5
        public bool Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return false;

                var referee = (from col in db.ccee_Colegiado
                                   where col.Col_NoColegiado == id
                                   select new refereeTemp()
                                   {
                                       NoReferee = col.Col_NoColegiado,
                                       status = col.fk_TipoColegiado,
                                       salario = col.Col_Salario,
                                       active = col.Col_Estatus,
                                       fecha = DateTime.Now
                                   }).FirstOrDefault();
                if (referee == null) return false;

                Referee tempReferee;
                tempReferee = new Referee(referee.NoReferee, db);
                tempReferee.economic();
                if (isPay(referee.status, regexVerifyStatus))
                    tempReferee.chageStatus();

                if (referee.status == 3)
                {
                    if (referee.active != null)
                    {
                        //if (referee.active == 1)
                        //{
                        //    tempReferee.chageStatus();
                        //}
                        tempReferee.chageStatus();
                    }
                }

                if (referee.status == 8)
                {
                    tempReferee.chageStatus();
                }

                Autorization.saveOperation(db, autorization, "Actualizando Estatus de " + id);
                db.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        // POST: api/Api_Status
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_Status/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_Status/5
        public void Delete(int id)
        {
        }

        public bool isPay(int? status, string regex)
        {
            if (status == null) return false;
            Regex r = new Regex(regex, RegexOptions.IgnoreCase);
            Match mat = r.Match(status.ToString());
            return mat.Success;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public bool statusWithoutAuth(int id)
        {
            try
            {
                

                var referee = (from col in db.ccee_Colegiado
                               where col.Col_NoColegiado == id
                               select new refereeTemp()
                               {
                                   NoReferee = col.Col_NoColegiado,
                                   status = col.fk_TipoColegiado,
                                   salario = col.Col_Salario,
                                   active = col.Col_Estatus,
                                   fecha = DateTime.Now
                               }).FirstOrDefault();

                Referee tempReferee;
                tempReferee = new Referee(referee.NoReferee, db);
                tempReferee.economic();
                if (isPay(referee.status, regexVerifyStatus))
                    tempReferee.chageStatus();

                if (referee.status == 3)
                {
                    if (referee.active != null)
                    {
                        //if (referee.active == 1)
                        //{
                        //    tempReferee.chageStatus();
                        //}
                        tempReferee.chageStatus();
                    }
                }
                
                db.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }


    }
}

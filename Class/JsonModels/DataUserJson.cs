﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels
{
    public class DataUserJson
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string correo { get; set; }
        public Nullable<int> telefonoResidencial { get; set; }
        public Nullable<int> telefonoMovil { get; set; }
        public int idProfile { get; set; }
        public string profile { get; set; }
        public string question { get; set; }
        public string answer { get; set; }
        public string oldPass { get; set; }
        public string newPass { get; set; }
        public bool isAdmin { get; set; }
    }
}
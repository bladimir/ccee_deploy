﻿(function () {
    angular.module('generals')
        .controller('typeSpeciality', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listTypeEspecialty;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idTypeSpecialtyUpdate = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_TYPESPECIALTY)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listTypeEspecialty = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            
            $scope.getTypeEspecialty = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_TYPESPECIALTY)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listTypeEspecialty = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.addTypeEspecialty = function (type) {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.typeName,
                    degree: $scope.tyepDegree,
                    abbreviation: $scope.typeAbbreviation
                }
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_TYPESPECIALTY, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.cleanData();
                                $scope.getTypeEspecialty();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (type) {
                $scope.idTypeSpecialtyUpdate = type.id;
                $scope.typeName = type.name;
                $scope.tyepDegree = type.degree;
                $scope.typeAbbreviation = type.abbreviation;
                $anchorScroll();
            }

            $scope.updateTypeSpecialty = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.typeName,
                    degree: $scope.tyepDegree,
                    abbreviation: $scope.typeAbbreviation
                }
                generalactionsFactory.actionPutFunction(CONSTANT.URL_API_TYPESPECIALTY, $scope.idTypeSpecialtyUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.getTypeEspecialty();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }


            $scope.deleteTypeEspecialty = function (type) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionDeleteFunction(CONSTANT.URL_API_TYPESPECIALTY, type.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getTypeEspecialty();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idTypeSpecialtyUpdate = 0;
                $scope.typeName = "";
                $scope.tyepDegree = "";
                $scope.typeAbbreviation = "";
            }


        }]);

})()
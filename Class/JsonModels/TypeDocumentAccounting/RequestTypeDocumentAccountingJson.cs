﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.TypeDocumentAccounting
{
    public class RequestTypeDocumentAccountingJson
    {
        public string name { get; set; }
        public string description { get; set; }
        public string templateXML { get; set; }
    }
}
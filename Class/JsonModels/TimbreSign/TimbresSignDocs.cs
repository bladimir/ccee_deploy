﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.TimbreSign
{
    public class TimbresSignDocs
    {
        public int No { get; set; }
        public int page { get; set; }
        public decimal valor { get; set; }
        public int idTimbre { get; set; }
    }
}
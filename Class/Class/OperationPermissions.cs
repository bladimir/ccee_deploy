﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webcee.ViewModels;

namespace webcee.Class.Class
{
    public class OperationPermissions
    {
        private Controller c;
        private LoginModelViews login;

        public OperationPermissions(Controller c, LoginModelViews login)
        {
            this.c = c;
            this.login = login;
        }

        public string permissionViewPage(string operationView)
        {
            return ((permissionsUser(login.Operations, operationView)) ? "" : null);
        }

        public void defineViewBagPermissions()
        {
            c.ViewBag.varColegiado = permissionsUser(login.Operations, Constant.Constant.operation_var_global);

            c.ViewBag.nuevoColegido = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_colegiado);
            c.ViewBag.editarColegiado = permissionsUser(login.Operations, Constant.Constant.operation_editar_colegiado);
            c.ViewBag.eliminarColegiado = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_colegiado);
            c.ViewBag.nuevoDireccion = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_direccion);
            c.ViewBag.editarDireccion = permissionsUser(login.Operations, Constant.Constant.operation_editar_direccion);
            c.ViewBag.eliminarDireccion = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_direccion);
            c.ViewBag.buscarDireccion = permissionsUser(login.Operations, Constant.Constant.operation_buscar_direccion);
            c.ViewBag.nuevoTelefono = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_telefono);
            c.ViewBag.editarTelefono = permissionsUser(login.Operations, Constant.Constant.operation_editar_telefono);
            c.ViewBag.eliminarTelefono = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_telefono);
            c.ViewBag.buscarTelefono = permissionsUser(login.Operations, Constant.Constant.operation_buscar_telefono);
            c.ViewBag.nuevoMail = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_mail);
            c.ViewBag.editarMail = permissionsUser(login.Operations, Constant.Constant.operation_editar_mail);
            c.ViewBag.eliminarMail = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_mail);
            c.ViewBag.buscarMail = permissionsUser(login.Operations, Constant.Constant.operation_buscar_mail);
            c.ViewBag.nuevoBeneficiario = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_beneficiario);
            c.ViewBag.editarBeneficiario = permissionsUser(login.Operations, Constant.Constant.operation_editar_beneficiario);
            c.ViewBag.eliminarBeneficiario = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_beneficiario);
            c.ViewBag.buscarBeneficiario = permissionsUser(login.Operations, Constant.Constant.operation_buscar_beneficiario);
            c.ViewBag.nuevoHistorial = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_historial);
            c.ViewBag.editarHistorial = permissionsUser(login.Operations, Constant.Constant.operation_editar_historial);
            c.ViewBag.eliminarHistorial = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_historial);
            c.ViewBag.buscarHistorial = permissionsUser(login.Operations, Constant.Constant.operation_buscar_historial);
            c.ViewBag.nuevoAcademia = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_academia);
            c.ViewBag.editarAcademia = permissionsUser(login.Operations, Constant.Constant.operation_editar_academia);
            c.ViewBag.eliminarAcademia = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_academia);
            c.ViewBag.buscarAcademia = permissionsUser(login.Operations, Constant.Constant.operation_buscar_academia);
            c.ViewBag.nuevoTipoCol = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_tipo_col);
            c.ViewBag.editarTipoCol = permissionsUser(login.Operations, Constant.Constant.operation_editar_tipo_col);
            c.ViewBag.eliminarTipoCol = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_tipo_col);
            c.ViewBag.buscarTipoCol = permissionsUser(login.Operations, Constant.Constant.operation_buscar_tipo_col);
            c.ViewBag.nuevoTipoBen = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_tipo_ben);
            c.ViewBag.editarTipoBen = permissionsUser(login.Operations, Constant.Constant.operation_editar_tipo_ben);
            c.ViewBag.eliminarTipoBen = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_tipo_ben);
            c.ViewBag.buscarTipoBen = permissionsUser(login.Operations, Constant.Constant.operation_buscar_tipo_ben);
            c.ViewBag.nuevoTipoEsp = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_tipo_esp);
            c.ViewBag.editarTipoEsp = permissionsUser(login.Operations, Constant.Constant.operation_editar_tipo_esp);
            c.ViewBag.eliminarTipoEsp = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_tipo_esp);
            c.ViewBag.buscarTipoEsp = permissionsUser(login.Operations, Constant.Constant.operation_buscar_tipo_esp);
            c.ViewBag.nuevoUniversidad = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_universidad);
            c.ViewBag.editarUniversidad = permissionsUser(login.Operations, Constant.Constant.operation_editar_universidad);
            c.ViewBag.eliminarUniversidad = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_universidad);
            c.ViewBag.buscarUniversidad = permissionsUser(login.Operations, Constant.Constant.operation_buscar_universidad);
            c.ViewBag.nuevoLenguaje = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_lenguaje);
            c.ViewBag.editarLenguaje = permissionsUser(login.Operations, Constant.Constant.operation_editar_lenguaje);
            c.ViewBag.eliminarLenguaje = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_lenguaje);
            c.ViewBag.buscarLenguaje = permissionsUser(login.Operations, Constant.Constant.operation_buscar_lenguaje);
            c.ViewBag.nuevoExpediente = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_expediente);
            c.ViewBag.editarExpediente = permissionsUser(login.Operations, Constant.Constant.operation_editar_expediente);
            c.ViewBag.eliminarExpediente = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_expediente);
            c.ViewBag.buscarExpediente = permissionsUser(login.Operations, Constant.Constant.operation_buscar_expediente);
            c.ViewBag.nuevoExpedienteReq = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_expediente_req);
            c.ViewBag.editarExpedienteReq = permissionsUser(login.Operations, Constant.Constant.operation_editar_expediente_req);
            c.ViewBag.eliminarExpedienteReq = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_expediente_req);
            c.ViewBag.buscarExpedienteReq = permissionsUser(login.Operations, Constant.Constant.operation_buscar_expediente_req);
            c.ViewBag.nuevoPais = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_expediente_pais);
            c.ViewBag.editarPais = permissionsUser(login.Operations, Constant.Constant.operation_editar_expediente_pais);
            c.ViewBag.eliminarPais = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_expediente_pais);
            c.ViewBag.buscarPais = permissionsUser(login.Operations, Constant.Constant.operation_buscar_expediente_pais);
            c.ViewBag.nuevoDepartamento = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_expediente_departamento);
            c.ViewBag.editarDepartamento = permissionsUser(login.Operations, Constant.Constant.operation_editar_expediente_departamento);
            c.ViewBag.eliminarDepartamento = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_expediente_departamento);
            c.ViewBag.buscarDepartamento = permissionsUser(login.Operations, Constant.Constant.operation_buscar_expediente_departamento);
            c.ViewBag.nuevoMunicipio = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_expediente_municipio);
            c.ViewBag.editarMunicipio = permissionsUser(login.Operations, Constant.Constant.operation_editar_expediente_municipio);
            c.ViewBag.eliminarMunicipio = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_expediente_municipio);
            c.ViewBag.buscarMunicipio = permissionsUser(login.Operations, Constant.Constant.operation_buscar_expediente_municipio);
            c.ViewBag.nuevoSede = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_sede);
            c.ViewBag.editarSede = permissionsUser(login.Operations, Constant.Constant.operation_editar_sede);
            c.ViewBag.eliminarSede = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_sede);
            c.ViewBag.buscarSede = permissionsUser(login.Operations, Constant.Constant.operation_buscar_sede);
            c.ViewBag.nuevoDocGenerales = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_doc_generales);
            c.ViewBag.editarDocGenerales = permissionsUser(login.Operations, Constant.Constant.operation_editar_doc_generales);
            c.ViewBag.eliminarDocGenerales = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_doc_generales);
            c.ViewBag.buscarDocGenerales = permissionsUser(login.Operations, Constant.Constant.operation_buscar_doc_generales);
            c.ViewBag.nuevoDocContables = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_doc_contables);
            c.ViewBag.editarDocContables = permissionsUser(login.Operations, Constant.Constant.operation_editar_doc_contables);
            c.ViewBag.eliminarDocContables = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_doc_contables);
            c.ViewBag.buscarDocContables = permissionsUser(login.Operations, Constant.Constant.operation_buscar_doc_contables);
            c.ViewBag.nuevoTipoPartida = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_tipo_partida);
            c.ViewBag.editarTipoPartida = permissionsUser(login.Operations, Constant.Constant.operation_editar_tipo_partida);
            c.ViewBag.eliminarTipoPartida = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_tipo_partida);
            c.ViewBag.buscarTipoPartida = permissionsUser(login.Operations, Constant.Constant.operation_buscar_tipo_partida);
            c.ViewBag.nuevoPartidaCont = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_partida_cont);
            c.ViewBag.editarPartidaCont = permissionsUser(login.Operations, Constant.Constant.operation_editar_partida_cont);
            c.ViewBag.eliminarPartidaCont = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_partida_cont);
            c.ViewBag.buscarPartidaCont = permissionsUser(login.Operations, Constant.Constant.operation_buscar_partida_cont);
            c.ViewBag.nuevoBanco = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_banco);
            c.ViewBag.editarBanco = permissionsUser(login.Operations, Constant.Constant.operation_editar_banco);
            c.ViewBag.eliminarBanco = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_banco);
            c.ViewBag.buscarBanco = permissionsUser(login.Operations, Constant.Constant.operation_buscar_banco);
            c.ViewBag.nuevoMedioPago = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_medio_pago);
            c.ViewBag.editarMedioPago = permissionsUser(login.Operations, Constant.Constant.operation_editar_medio_pago);
            c.ViewBag.eliminarMedioPago = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_medio_pago);
            c.ViewBag.buscarMedioPago = permissionsUser(login.Operations, Constant.Constant.operation_buscar_medio_pago);
            c.ViewBag.nuevoCargoEcono = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_cargo_econo);
            c.ViewBag.editarCargoEcono = permissionsUser(login.Operations, Constant.Constant.operation_editar_cargo_econo);
            c.ViewBag.eliminarCargoEcono = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_cargo_econo);
            c.ViewBag.buscarCargoEcono = permissionsUser(login.Operations, Constant.Constant.operation_buscar_cargo_econo);
            c.ViewBag.nuevoCentroPago = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_centro_pago);
            c.ViewBag.editarCentroPago = permissionsUser(login.Operations, Constant.Constant.operation_editar_centro_pago);
            c.ViewBag.eliminarCentroPago = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_centro_pago);
            c.ViewBag.buscarCentroPago = permissionsUser(login.Operations, Constant.Constant.operation_buscar_centro_pago);
            c.ViewBag.nuevoCaja = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_caja);
            c.ViewBag.editarCaja = permissionsUser(login.Operations, Constant.Constant.operation_editar_caja);
            c.ViewBag.eliminarcaja = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_caja);
            c.ViewBag.buscarcaja = permissionsUser(login.Operations, Constant.Constant.operation_buscar_caja);
            c.ViewBag.nuevoCajero = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_cajero);
            c.ViewBag.editarCajero = permissionsUser(login.Operations, Constant.Constant.operation_editar_cajero);
            c.ViewBag.eliminarCajero = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_cajero);
            c.ViewBag.buscarCajero = permissionsUser(login.Operations, Constant.Constant.operation_buscar_cajero);
            c.ViewBag.nuevoAsingCajero = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_asing_cajero);
            c.ViewBag.editarAsingCajero = permissionsUser(login.Operations, Constant.Constant.operation_editar_asing_cajero);
            c.ViewBag.eliminarAsingCajero = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_asing_cajero);
            c.ViewBag.buscarAsingCajero = permissionsUser(login.Operations, Constant.Constant.operation_buscar_asing_cajero);
            c.ViewBag.nuevoBodega = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_bodega);
            c.ViewBag.editarBodega = permissionsUser(login.Operations, Constant.Constant.operation_editar_bodega);
            c.ViewBag.eliminarBodega = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_bodega);
            c.ViewBag.buscarBodega = permissionsUser(login.Operations, Constant.Constant.operation_buscar_bodega);
            c.ViewBag.nuevoValorTimb = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_valor_timb);
            c.ViewBag.editarValorTimb = permissionsUser(login.Operations, Constant.Constant.operation_editar_valor_timb);
            c.ViewBag.eliminarValorTimb = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_valor_timb);
            c.ViewBag.buscarValorTimb = permissionsUser(login.Operations, Constant.Constant.operation_buscar_valor_timb);
            c.ViewBag.nuevoUsuario = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_usuario);
            c.ViewBag.editarUsuario = permissionsUser(login.Operations, Constant.Constant.operation_editar_usuario);
            c.ViewBag.eliminarUsuario = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_usuario);
            c.ViewBag.buscarUsuario = permissionsUser(login.Operations, Constant.Constant.operation_buscar_usuario);
            c.ViewBag.nuevoRol = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_rol);
            c.ViewBag.editarRol = permissionsUser(login.Operations, Constant.Constant.operation_editar_rol);
            c.ViewBag.eliminarRol = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_rol);
            c.ViewBag.buscarRol = permissionsUser(login.Operations, Constant.Constant.operation_buscar_rol);
            c.ViewBag.nuevoCuentaBanc = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_cuenta_banc);
            c.ViewBag.editarCuentaBanc = permissionsUser(login.Operations, Constant.Constant.operation_editar_cuenta_banc);
            c.ViewBag.eliminarCuentaBanc = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_cuenta_banc);
            c.ViewBag.buscarCuentaBanc = permissionsUser(login.Operations, Constant.Constant.operation_buscar_cuenta_banc);

            c.ViewBag.nuevoNoColegiado = permissionsUser(login.Operations, Constant.Constant.operation_nuevo_no_colegiado);
            c.ViewBag.editarNoColegiado = permissionsUser(login.Operations, Constant.Constant.operation_editar_no_colegiado);
            c.ViewBag.eliminarNoColegiado = permissionsUser(login.Operations, Constant.Constant.operation_eliminar_no_colegiado);
            c.ViewBag.buscarNoColegiado = permissionsUser(login.Operations, Constant.Constant.operation_buscar_no_colegiado);

            c.ViewBag.buscarRecibo = permissionsUser(login.Operations, Constant.Constant.operation_buscar_recibo);
            c.ViewBag.editarRecibo = permissionsUser(login.Operations, Constant.Constant.operation_editar_recibo);
            c.ViewBag.imprimirRecibo = permissionsUser(login.Operations, Constant.Constant.operation_imprimir_recibo);
            c.ViewBag.revertirRecibo = permissionsUser(login.Operations, Constant.Constant.operation_revertir_recibo);
            
            c.ViewBag.editarCheque = permissionsUser(login.Operations, Constant.Constant.operation_editar_cheque);
            c.ViewBag.buscarCheque = permissionsUser(login.Operations, Constant.Constant.operation_buscar_cheque);
            c.ViewBag.agregarFCheque = permissionsUser(login.Operations, Constant.Constant.operation_agregar_f_cheque);

            c.ViewBag.editarEstadoTipoCol = permissionsUser(login.Operations, Constant.Constant.operation_editar_estado_tipo_col);
            c.ViewBag.editarMontoEcono = permissionsUser(login.Operations, Constant.Constant.operation_editar_monto_econo);

            c.ViewBag.ejecutarCaja = permissionsUser(login.Operations, Constant.Constant.operation_ejecutar_caja);
            c.ViewBag.ejecutarPago = permissionsUser(login.Operations, Constant.Constant.operation_ejecutar_pago);
            c.ViewBag.ejecutarTimbre = permissionsUser(login.Operations, Constant.Constant.operation_ejecutar_timbre);
            c.ViewBag.ejecutarCarga = permissionsUser(login.Operations, Constant.Constant.operation_ejecutar_carga);
            c.ViewBag.ejecutarBoleta = permissionsUser(login.Operations, Constant.Constant.operation_ejecutar_boleta);
            c.ViewBag.ejecutarRecibo = permissionsUser(login.Operations, Constant.Constant.operation_ejecutar_recibo);
            c.ViewBag.ejecutarCheque = permissionsUser(login.Operations, Constant.Constant.operation_ejecutar_cheque);
            c.ViewBag.ejecutarListadoDoc = permissionsUser(login.Operations, Constant.Constant.operation_ejecutar_listado_doc);
            c.ViewBag.ejecutarDocumentosRec = permissionsUser(login.Operations, Constant.Constant.operation_ejecutar_documentos_rec);
            c.ViewBag.ejecutarHistorialRecibo = permissionsUser(login.Operations, Constant.Constant.operation_ejecutar_historial_recibo);
            c.ViewBag.ejecutarFechaCerti = permissionsUser(login.Operations, Constant.Constant.operation_ejecutar_fecha_certi);
            c.ViewBag.ejecutarDiplomaNuevo = permissionsUser(login.Operations, Constant.Constant.operation_ejecutar_diploma_nuevo);
            c.ViewBag.ejecutarCambioTimbre = permissionsUser(login.Operations, Constant.Constant.operation_ejecutar_cambio_timbre);
            c.ViewBag.ejecutarInvalidarCert = permissionsUser(login.Operations, Constant.Constant.operation_ejecutar_invalidar_cert);
            c.ViewBag.ejecutarEliminarCarg = permissionsUser(login.Operations, Constant.Constant.operation_ejecutar_elimar_carg);

            c.ViewBag.reporteDesgloseN = permissionsUser(login.Operations, Constant.Constant.operation_reporte_desglose_n);
            c.ViewBag.reporteDesgloseC = permissionsUser(login.Operations, Constant.Constant.operation_reporte_desglose_c);
            c.ViewBag.reporteIngresos = permissionsUser(login.Operations, Constant.Constant.operation_reporte_ingresos);
            c.ViewBag.reporteConsolidados = permissionsUser(login.Operations, Constant.Constant.operation_reporte_consolidados);
            c.ViewBag.reporteCheques = permissionsUser(login.Operations, Constant.Constant.operation_reporte_cheques);
            c.ViewBag.reporteRecibos = permissionsUser(login.Operations, Constant.Constant.operation_reporte_recibos);
            c.ViewBag.reporteCorteCaja = permissionsUser(login.Operations, Constant.Constant.operation_reporte_corte_caja);
            c.ViewBag.reporteFicha = permissionsUser(login.Operations, Constant.Constant.operation_reporte_ficha);
            c.ViewBag.reporteSaldo = permissionsUser(login.Operations, Constant.Constant.operation_reporte_saldo);
            c.ViewBag.reporteEstado = permissionsUser(login.Operations, Constant.Constant.operation_reporte_estado);
            c.ViewBag.reporteDocumentos = permissionsUser(login.Operations, Constant.Constant.operation_reporte_documentos);
            c.ViewBag.reporteColegiado = permissionsUser(login.Operations, Constant.Constant.operation_reporte_colegiado);
            c.ViewBag.reporteListadoCol = permissionsUser(login.Operations, Constant.Constant.operation_reporte_listado_col);
            c.ViewBag.reporteCalidadEst = permissionsUser(login.Operations, Constant.Constant.operation_reporte_calidad_est);
            c.ViewBag.reporteEstadisticas = permissionsUser(login.Operations, Constant.Constant.operation_reporte_estadisticas);
            c.ViewBag.reportePadron = permissionsUser(login.Operations, Constant.Constant.operation_reporte_padron);
            c.ViewBag.reporteEtiqueta = permissionsUser(login.Operations, Constant.Constant.operation_reporte_etiqueta);
            c.ViewBag.reporteDirectorio = permissionsUser(login.Operations, Constant.Constant.operation_reporte_directorio);
            c.ViewBag.historicoAc = permissionsUser(login.Operations, Constant.Constant.operation_reporte_historico_ac);
            c.ViewBag.reporteFichaResumida = permissionsUser(login.Operations, Constant.Constant.operation_reporte_ficha_resumida);
            c.ViewBag.reporteTimbre = permissionsUser(login.Operations, Constant.Constant.operation_reporte_timbre);
            c.ViewBag.reporteAsamblea = permissionsUser(login.Operations, Constant.Constant.operation_reporte_asamblea);
            c.ViewBag.reporteTesis = permissionsUser(login.Operations, Constant.Constant.operation_reporte_tesis);
            c.ViewBag.reporteHistorialUsu = permissionsUser(login.Operations, Constant.Constant.operation_historial_usu);
            c.ViewBag.reporteTimbreBod = permissionsUser(login.Operations, Constant.Constant.operation_reporte_timbre_bod);
            c.ViewBag.reporteDiplomaNuevo = permissionsUser(login.Operations, Constant.Constant.operation_reporte_diploma_nuevo);
            c.ViewBag.reporteResumenInv = permissionsUser(login.Operations, Constant.Constant.operation_reporte_resumen_inv);
            c.ViewBag.reporteTimbreCaj = permissionsUser(login.Operations, Constant.Constant.operation_reporte_timbre_caj);
            c.ViewBag.reporteEntradaSali = permissionsUser(login.Operations, Constant.Constant.operation_reporte_entrada_sali);
            c.ViewBag.reporteCertificacion = permissionsUser(login.Operations, Constant.Constant.operation_reporte_certificacion);
            c.ViewBag.reporteCarnet = permissionsUser(login.Operations, Constant.Constant.operation_reporte_carnet);
            c.ViewBag.reporteCalculoActuario = permissionsUser(login.Operations, Constant.Constant.operation_reporte_calculo_actuario);

            c.ViewBag.reporteIngresoTimbreBod = permissionsUser(login.Operations, Constant.Constant.operation_reporte_ingreso_timbre_bod);
            c.ViewBag.reporteEntregaTimbreCaj = permissionsUser(login.Operations, Constant.Constant.operation_reporte_entrega_timbre_caj);
            c.ViewBag.reporteEntradasSalidas = permissionsUser(login.Operations, Constant.Constant.operation_reporte_entradas_salidas);
            c.ViewBag.reporteResumenInventarioTim = permissionsUser(login.Operations, Constant.Constant.operation_reporte_resumen_inventario_tim);
            c.ViewBag.reporteResumenCajaDia = permissionsUser(login.Operations, Constant.Constant.operation_reporte_resumen_caja_dia);

            //operation_reporte_ingreso_timbre_bod

            c.ViewBag.cambioFechaAcadem = permissionsUser(login.Operations, Constant.Constant.operation_cambiar_fecha_academ);

            c.ViewBag.verSaldo = permissionsUser(login.Operations, Constant.Constant.operation_ver_saldo);
            c.ViewBag.verReporteGeneral = permissionsUser(login.Operations, Constant.Constant.operation_ver_reporte_general);
            c.ViewBag.verEditarMedioPagoRecibo = permissionsUser(login.Operations, Constant.Constant.operation_editar_medio_pago_recibo);
            c.ViewBag.verConvenio = permissionsUser(login.Operations, Constant.Constant.operation_ver_convenio);
            c.ViewBag.verPlanilla = permissionsUser(login.Operations, Constant.Constant.operation_ver_planilla);
            
            c.ViewBag.actualizarTodosEstatus = permissionsUser(login.Operations, Constant.Constant.operation_actualizar_todos_estatus);

            c.ViewBag.newUsers = true;
            c.ViewBag.newEvent = true;
            c.ViewBag.cashier = (login.idCashier != 0);
            c.ViewBag.Name = login.User.Usu_Nombre;
        }

        private bool permissionsUser(List<string> listPermission, string permission)
        {
            foreach (string per in listPermission)
            {
                if (per.Equals(permission))
                {
                    return true;
                }
            }
            return false;
        }



    }
}
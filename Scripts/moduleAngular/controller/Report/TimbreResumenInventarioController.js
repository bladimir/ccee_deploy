﻿(function () {
    angular.module('reports')
        .controller('timbreResumenBodega', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'reportFactory', function ($scope, $http, CONSTANT, $anchorScroll, reportFactory) {
            $scope.direction;
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquaters;
            $scope.regexReferee = CONSTANT.REGEX_REFEREE;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.name = "";

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.identical = function (actual, expected) {
                return actual === parseInt(expected);
            }

            $scope.init = function (name) {
                $scope.name = name;
                console.log($scope.name);
                reportFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        reportFactory.setAuthorization(response.data);
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.generateList = function () {
                var textGetReport = "";
                console.log(($scope.selectHead != null) ? $scope.selectHead.id : "");
                textGetReport = "&cashi=";
                textGetReport += "&name=" + $scope.name;
                console.log(textGetReport);
                $scope.direction = "../../Reports/aspxReports/ResumenInventario.aspx?" + textGetReport;
            }

        }]);

})()
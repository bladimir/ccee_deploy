﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Economic
{
    public class ResponseEconomicRefereeJson
    {
        public int id { get; set; }
        public decimal amount { get; set; }
        public Nullable<DateTime> dateCreated { get; set; }
        public int idTypeEconomic { get; set; }
        public int idPay { get; set; }
        public int idReferee { get; set; }
        public int idNoReferee { get; set; }
        public string description { get; set; }
        public int enable { get; set; }
        public int? relationship { get; set; }
        public int isTimbre { get; set; }
        public string calcule { get; set; }
        public int positionOfDate { get; set; }
    }
}
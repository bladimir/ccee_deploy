﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels._Tools;
using webcee.Class.JsonModels.AccountingDocument;
using webcee.Class.JsonModels.Economic;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_AccountingDocumentController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_AccountingDocument
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_AccountingDocument/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_AccountingDocument
        public int Post(RequestAccountingDocumentJson accounting)
        {

            try
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        ccee_TipoDocContable tempDocCon =
                            db.ccee_TipoDocContable.Find(accounting.idTypeDocumentAccounting);
                        ccee_Colegiado tempCol =
                            db.ccee_Colegiado.Find(accounting.idReferee);
                        ccee_Cajero tempCaj =
                            db.ccee_Cajero.Find(accounting.idCashier);

                        if (tempDocCon == null || tempCol == null || tempCaj == null)
                        {
                            return 0;
                        }

                        string template = createReceipt(tempCol, tempCaj, accounting.remark,
                                                        tempDocCon, accounting.listEconomic, accounting.address);
                        ccee_DocContable tempDoc = new ccee_DocContable()
                        {
                            DCo_ANombre = accounting.titleName,
                            DCo_Direccion = accounting.address,
                            DCo_FechaHora = DateTime.Now,
                            DCo_Nit = accounting.nit,
                            DCo_Observacion = accounting.remark,
                            DCo_Xml = template,
                            DCo_HashDoc = "",
                            fk_TipoDocContable = accounting.idTypeDocumentAccounting
                        };
                        db.ccee_DocContable.Add(tempDoc);
                        db.SaveChanges();

                        template = template.Replace(Constant.html_pdf_remplace_No_document, tempDoc.DCo_NoDocContable.ToString());
                        string shaTemplate = convertSha1(template);
                        tempDoc.DCo_Xml = template;
                        tempDoc.DCo_HashDoc = shaTemplate;
                        db.Entry(tempDoc).State = EntityState.Modified;
                        db.SaveChanges();

                        transaction.Commit();

                        return tempDoc.DCo_NoDocContable;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        return 0;
                    }
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        // PUT: api/Api_AccountingDocument/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_AccountingDocument/5
        public void Delete(int id)
        {
        }


        private string convertSha1(string text)
        {
            HashAlgorithm sha1 = HashAlgorithm.Create("SHA1");
            Byte[] sha1Data = sha1.ComputeHash(UTF8Encoding.UTF8.GetBytes(text));
            return BitConverter.ToString(sha1Data).Replace("-", "");
        }


        private string createReceipt(ccee_Colegiado referre, ccee_Cajero cashier, string remarke, 
                                        ccee_TipoDocContable type, IEnumerable<ResponseEconomicJson> listEconomic, string address)
        {
            bool isCash = false;
            bool isOther = false;
            string cheque = "";
            string contentTypeEconomic = "";
            decimal totalEconomic = 0;
            string tupla = "";
            int idVerificaciorPay = 0;
            ccee_Usuario tempUser = db.ccee_Usuario.Find(cashier.fk_Usuario);

            listEconomic = listEconomic.OrderBy(o => o.idTypeEconomic);
            List<ResponseEconomicJson> listTempEconomic = new List<ResponseEconomicJson>();
            foreach (ResponseEconomicJson contEconomic in listEconomic)
            {

                int enableEco = contEconomic.enable;
                if(contEconomic.typeEconomic != 3)
                {
                    foreach (ResponseEconomicRefereeJson contEcoL in contEconomic.ListReferee)
                    {
                        ResponseEconomicJson tempRef = new ResponseEconomicJson();
                        tempRef.id = contEcoL.id;
                        tempRef.amount = contEcoL.amount;
                        tempRef.idTypeEconomic = contEcoL.idTypeEconomic;
                        tempRef.enable = enableEco;
                        tempRef.description = contEcoL.description;
                        listTempEconomic.Add(tempRef);
                    }
                }
                else
                {
                    listTempEconomic.Add(contEconomic);
                }
                
            }

            var select = from r in listTempEconomic
                     where r.enable == 1
                     group r by new
                     {
                         Category = r.idTypeEconomic
                     } into g
                     select new
                     {
                         Category = g.Key.Category,
                         Count = g.Count(),
                         Descripton = g.Select(y=>y.description),
                         Sumatory = g.Sum(x => x.amount)
                     };

            
            foreach (var item in select)
            {
                string nameTypeEconomic = item.Descripton.FirstOrDefault();
                tupla = Constant.html_pdf_remplace_tupla_type_economic.Replace(
                        Constant.html_pdf_remplace_type_economic, item.Count + " " + nameTypeEconomic);
                tupla = tupla.Replace(Constant.html_pdf_remplace_amount,
                        string.Format("{0:C2}", item.Sumatory));
                contentTypeEconomic += " " + tupla;
                totalEconomic += item.Sumatory;
            }

            //foreach (ResponseEconomicJson contEconomic in listEconomic)
            //{
            //    if (contEconomic.enable == 1)
            //    {
            //        if (valueTypeEconomic == 0)
            //        {
            //            valueTypeEconomic = contEconomic.idTypeEconomic;
            //            totalEconomic = contEconomic.amount;
            //            totalForTypeEconomic = contEconomic.amount;
            //            textTypeEconomic = contEconomic.description;
            //            idVerificaciorPay = contEconomic.id;
            //        }else
            //        {
            //            if (valueTypeEconomic == contEconomic.idTypeEconomic)
            //            {
            //                totalEconomic += contEconomic.amount;
            //                totalForTypeEconomic += contEconomic.amount;
            //            }
            //            else
            //            {
            //                tupla = Constant.html_pdf_remplace_tupla_type_economic.Replace(Constant.html_pdf_remplace_type_economic, textTypeEconomic);
            //                tupla = tupla.Replace(Constant.html_pdf_remplace_amount, totalForTypeEconomic.ToString());
            //                contentTypeEconomic += " " + tupla;

            //                valueTypeEconomic = contEconomic.idTypeEconomic;
            //                totalForTypeEconomic = contEconomic.amount;
            //                textTypeEconomic = contEconomic.description;
            //                totalEconomic += contEconomic.amount;
            //            }
            //        }
            //    }
            //}

            //tupla = Constant.html_pdf_remplace_tupla_type_economic.Replace(Constant.html_pdf_remplace_type_economic, textTypeEconomic);
            //tupla = tupla.Replace(Constant.html_pdf_remplace_amount, string.Format("{0:C2}", totalForTypeEconomic));
            //contentTypeEconomic += " " + tupla;

            var tempTypePay = from med in db.ccee_MedioPago
                              join pag in db.ccee_MedioPago_Pago on med.MedP_NoMedioPago equals pag.pkfk_MedioPago
                              join pgo in db.ccee_Pago on pag.pkfk_Pago equals pgo.Pag_NoPago
                              join eco in db.ccee_CargoEconomico on pgo.Pag_NoPago equals eco.fk_Pago
                              where eco.CEc_NoCargoEconomico == idVerificaciorPay
                              select new ModelAccountingDocument()
                              {
                                  medPayPay = pag,
                                  medPay = med                                  
                              };

            foreach (ModelAccountingDocument model in tempTypePay)
            {
                switch (model.medPay.MdP_TipoMedio)
                {
                    case 0:
                        isCash = true;
                        break;
                    case 1:
                        cheque += " Cheque: " + model.medPayPay.MPP_NoDocumento;
                        break;

                    default:
                        isOther = true;
                        break;
                 
                }
            }


            

            string templateHTML = type.TDC_XmlPlantilla;
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_date, GeneralFunction.dateNowFormatDateHour());
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_No_referree, referre.Col_NoColegiado.ToString());
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_name_referee, referre.Col_PrimerNombre + "  " + referre.Col_SegundoNombre + " " + referre.Col_PrimerApellido + " " + referre.Col_SegundoApellido );
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_remake, remarke);
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_type_economic_reciber, contentTypeEconomic);
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_total_letter, GeneralFunction.enletras(totalEconomic.ToString()));
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_total, string.Format("{0:C2}", totalEconomic));
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_cash, ((isCash) ? "X" : "O" ));
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_others, ((isOther) ? "X" : "O"));
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_cheque, cheque);
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_address, address);
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_cashier, tempUser.Usu_Nombre + " " + tempUser.Usu_Apellido);
            return templateHTML;
        }




        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

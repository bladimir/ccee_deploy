﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.Constant
{
    public class Constant
    {
        public const int referee_init_temp = 100000;

        public const string role_manager = "Administrador";
        public const string role_general = "General";
        public const string role_editor = "Editor";

        public const string session_user = "usuario";

        public const int status_code_error = 400;
        public const int status_code_user_repeat = 410;
        public const int status_code_success = 200;
        public const int status_code_user_not_found = 411;

        public const string operation_add_user = "Usuarios";
        public const string operation_see_user = "Colegiados";
        public const string operation_edit_event = "Eventos";

        public const string operation_var_global = "Var Global";

        public const string operation_pagina_inicio = "Pagina inicial";
        public const string operation_nuevo_colegiado = "Nuevo colegiado";
        public const string operation_editar_colegiado = "Editar colegiado";
        public const string operation_eliminar_colegiado = "Eliminar colegiado";
        public const string operation_nuevo_direccion = "Agregar direccion";
        public const string operation_editar_direccion = "Editar direccion";
        public const string operation_eliminar_direccion = "Eliminar direccion";
        public const string operation_buscar_direccion = "Buscar direccion";
        public const string operation_nuevo_telefono = "Agregar telefono";
        public const string operation_editar_telefono = "Editar telefono";
        public const string operation_eliminar_telefono = "Eliminar telefono";
        public const string operation_buscar_telefono = "Buscar telefono";
        public const string operation_nuevo_mail = "Agregar mail";
        public const string operation_editar_mail = "Editar mail";
        public const string operation_eliminar_mail = "Eliminar mail";
        public const string operation_buscar_mail = "Buscar mail";
        public const string operation_nuevo_beneficiario = "Agregar beneficiario";
        public const string operation_editar_beneficiario = "Editar benericiario";
        public const string operation_eliminar_beneficiario = "Eliminar beneficiario";
        public const string operation_buscar_beneficiario = "Buscar beneficiario";
        public const string operation_nuevo_historial = "Agregar historial";
        public const string operation_editar_historial = "Editar historial";
        public const string operation_eliminar_historial = "Eliminar historial";
        public const string operation_buscar_historial = "Buscar historial";
        public const string operation_nuevo_academia = "Agregar academia";
        public const string operation_editar_academia = "Editar academia";
        public const string operation_eliminar_academia = "Eliminar academia";
        public const string operation_buscar_academia = "Buscar academia";
        public const string operation_nuevo_tipo_col = "Nuevo tipo col";
        public const string operation_editar_tipo_col = "Editar tipo col";
        public const string operation_eliminar_tipo_col = "Eliminar tipo col";
        public const string operation_buscar_tipo_col = "Buscar tipo  col";
        public const string operation_nuevo_tipo_ben = "Nuevo tipo ben";
        public const string operation_editar_tipo_ben = "Editar tipo ben";
        public const string operation_eliminar_tipo_ben = "Eliminar tipo ben";
        public const string operation_buscar_tipo_ben = "Buscar tipo ben";
        public const string operation_nuevo_tipo_esp = "Nuevo tipo esp";
        public const string operation_editar_tipo_esp = "Editar tipo esp";
        public const string operation_eliminar_tipo_esp = "Eliminar tipo esp";
        public const string operation_buscar_tipo_esp = "Buscar tipo esp";
        public const string operation_nuevo_universidad = "Nuevo universidad";
        public const string operation_editar_universidad = "Editar universidad";
        public const string operation_eliminar_universidad = "Eliminar universidad";
        public const string operation_buscar_universidad = "Buscar universidad";
        public const string operation_nuevo_lenguaje = "Nuevo lenguaje";
        public const string operation_editar_lenguaje = "Editar lenguaje";
        public const string operation_eliminar_lenguaje = "Eliminar lenguaje";
        public const string operation_buscar_lenguaje = "Buscar lenguaje";
        public const string operation_nuevo_expediente = "Nuevo expediente";
        public const string operation_editar_expediente = "Editar expediente";
        public const string operation_eliminar_expediente = "Eliminar expediente";
        public const string operation_buscar_expediente = "Buscar expediente";
        public const string operation_nuevo_expediente_req = "Nuevo expediente req";
        public const string operation_editar_expediente_req = "Editar expediente req";
        public const string operation_eliminar_expediente_req = "Eliminar expediente req";
        public const string operation_buscar_expediente_req = "Buscar  expediente req";
        public const string operation_nuevo_expediente_pais = "Nuevo pais";
        public const string operation_editar_expediente_pais = "Editar pais";
        public const string operation_eliminar_expediente_pais = "Eliminar pais";
        public const string operation_buscar_expediente_pais = "Buscar pais";
        public const string operation_nuevo_expediente_departamento = "Nuevo departamento";
        public const string operation_editar_expediente_departamento = "Editar departamento";
        public const string operation_eliminar_expediente_departamento = "Eliminar departamento";
        public const string operation_buscar_expediente_departamento = "Buscar departamento";
        public const string operation_nuevo_expediente_municipio = "Nuevo municipio";
        public const string operation_editar_expediente_municipio = "Editar municipio";
        public const string operation_eliminar_expediente_municipio = "Eliminar municipio";
        public const string operation_buscar_expediente_municipio = "Buscar municipio";
        public const string operation_nuevo_sede = "Nuevo sede";
        public const string operation_editar_sede = "Editar sede";
        public const string operation_eliminar_sede = "Eliminar sede";
        public const string operation_buscar_sede = "Buscar sede";
        public const string operation_nuevo_doc_generales = "Nuevo doc generales";
        public const string operation_editar_doc_generales = "Editar doc generales";
        public const string operation_eliminar_doc_generales = "Eliminar doc generales";
        public const string operation_buscar_doc_generales = "Buscar doc generales";
        public const string operation_nuevo_doc_contables = "Nuevo doc contable";
        public const string operation_editar_doc_contables = "Editar doc contable";
        public const string operation_eliminar_doc_contables = "Eliminar doc contable";
        public const string operation_buscar_doc_contables = "Buscar doc contable";
        public const string operation_nuevo_tipo_partida = "Nuevo tipo partida";
        public const string operation_editar_tipo_partida = "Editar tipo partida";
        public const string operation_eliminar_tipo_partida = "Eliminar tipo partida";
        public const string operation_buscar_tipo_partida = "Buscar tipo partida";
        public const string operation_nuevo_partida_cont = "Nuevo partida cont";
        public const string operation_editar_partida_cont = "Editar partida cont";
        public const string operation_eliminar_partida_cont = "Eliminar partida cont";
        public const string operation_buscar_partida_cont = "Buscar partida cont";
        public const string operation_nuevo_banco = "Nuevo banco";
        public const string operation_editar_banco = "Editar banco";
        public const string operation_eliminar_banco = "Eliminar banco";
        public const string operation_buscar_banco = "Buscar banco";
        public const string operation_nuevo_medio_pago = "Nuevo medio pago";
        public const string operation_editar_medio_pago = "Editar medio pago";
        public const string operation_eliminar_medio_pago = "Eliminar medio pago";
        public const string operation_buscar_medio_pago = "Buscar medio pago";
        public const string operation_nuevo_cargo_econo = "Nuevo cargo econo";
        public const string operation_editar_cargo_econo = "Editar cargo econo";
        public const string operation_eliminar_cargo_econo = "Eliminar cargo econo";
        public const string operation_buscar_cargo_econo = "Buscar cargo econo";
        public const string operation_nuevo_centro_pago = "Nuevo centro pago";
        public const string operation_editar_centro_pago = "Editar centro pago";
        public const string operation_eliminar_centro_pago = "Eliminar centro pago";
        public const string operation_buscar_centro_pago = "Buscar centro pago";
        public const string operation_nuevo_caja = "Nuevo caja";
        public const string operation_editar_caja = "Editar caja";
        public const string operation_eliminar_caja = "Eliminar caja";
        public const string operation_buscar_caja = "Buscar caja";
        public const string operation_nuevo_cajero = "Nuevo cajero";
        public const string operation_editar_cajero = "Editar cajero";
        public const string operation_eliminar_cajero = "Eliminar cajero";
        public const string operation_buscar_cajero = "Buscar cajero";
        public const string operation_nuevo_asing_cajero = "Nuevo asing cajero";
        public const string operation_editar_asing_cajero = "Editar asing cajero";
        public const string operation_eliminar_asing_cajero = "Eliminar asing cajero";
        public const string operation_buscar_asing_cajero = "Buscar asign cajero";
        public const string operation_nuevo_bodega = "Nuevo bodega";
        public const string operation_editar_bodega = "Editar bodega";
        public const string operation_eliminar_bodega = "Eliminar bodega";
        public const string operation_buscar_bodega = "Buscar bodega";
        public const string operation_nuevo_valor_timb = "Nuevo valor timb";
        public const string operation_editar_valor_timb = "Editar valor timb";
        public const string operation_eliminar_valor_timb = "Eliminar valor timb";
        public const string operation_buscar_valor_timb = "Buscar valor timb";
        public const string operation_editar_monto_econo = "Editar monto econo";

        public const string operation_ejecutar_caja = "Ejecutar caja";
        public const string operation_ejecutar_pago = "Ejecutar pago";
        public const string operation_ejecutar_timbre = "Ejecutar timbre";
        public const string operation_ejecutar_carga = "Ejecutar carga";
        public const string operation_ejecutar_boleta = "Ejecutar boleta";
        public const string operation_ejecutar_recibo = "Ejecutar recibo";
        public const string operation_ejecutar_cheque = "Ejecutar cheque";
        public const string operation_ejecutar_cambio_timbre = "Ejecutar cambio timbre";
        public const string operation_ejecutar_listado_doc = "Ejecutar listado doc";
        public const string operation_ejecutar_documentos_rec = "Ejecutar documentos rec";
        public const string operation_ejecutar_historial_recibo = "Ejecutar historial recibo";
        public const string operation_ejecutar_diploma_nuevo = "Ejecutar diploma nuevo";
        public const string operation_ejecutar_fecha_certi = "Modificar fecha certi";
        public const string operation_ejecutar_invalidar_cert = "Ejecutar invalidar cert";
        public const string operation_ejecutar_elimar_carg = "Ejecutar eliminar carg";


        public const string operation_nuevo_usuario = "Nuevo usuario";
        public const string operation_editar_usuario = "Editar usuario";
        public const string operation_eliminar_usuario = "Eliminar usuario";
        public const string operation_buscar_usuario = "Buscar usuario";
        public const string operation_nuevo_rol = "Nuevo rol";
        public const string operation_editar_rol = "Editar rol";
        public const string operation_eliminar_rol = "Eliminar rol";
        public const string operation_buscar_rol = "Buscar rol";
        public const string operation_nuevo_cuenta_banc = "Nuevo cuenta banc";
        public const string operation_editar_cuenta_banc = "Editar cuenta banc";
        public const string operation_eliminar_cuenta_banc = "Eliminar cuenta banc";
        public const string operation_buscar_cuenta_banc = "Buscar cuenta banc";

        public const string operation_nuevo_no_colegiado = "Nuevo no colegiado";
        public const string operation_editar_no_colegiado = "Editar no colegiado";
        public const string operation_eliminar_no_colegiado = "Eliminar no colegiado";
        public const string operation_buscar_no_colegiado = "Buscar no colegiado";

        public const string operation_buscar_recibo = "Buscar recibo";
        public const string operation_imprimir_recibo = "Imprimir recibo";
        public const string operation_revertir_recibo = "Revertir recibo";
        public const string operation_editar_recibo = "Editar recibo";

        public const string operation_editar_cheque = "Editar cheque";
        public const string operation_buscar_cheque = "Buscar cheque";
        public const string operation_agregar_f_cheque = "Agregar f pag";

        public const string operation_editar_estado_tipo_col = "Editar estado tipo col";

        public const string operation_reporte_desglose_n = "Reporte desglose n";
        public const string operation_reporte_desglose_c = "Reporte desglose c";
        public const string operation_reporte_ingresos = "Reporte ingresos";
        public const string operation_reporte_consolidados = "Reporte consolidados";
        public const string operation_reporte_cheques = "Reporte cheques";
        public const string operation_reporte_recibos = "Reporte recibos";
        public const string operation_reporte_corte_caja = "Reporte corte caja";
        public const string operation_reporte_ficha = "Reporte ficha";
        public const string operation_reporte_saldo = "Reporte saldo";
        public const string operation_reporte_estado = "Reporte estado";
        public const string operation_reporte_documentos = "Reporte documentos";
        public const string operation_reporte_colegiado = "Reporte colegiado";
        public const string operation_reporte_listado_col = "Reporte listado col";
        public const string operation_reporte_calidad_est = "Reporte calidad est";
        public const string operation_reporte_estadisticas = "Reporte estadisticas";
        public const string operation_reporte_padron = "Reporte padron";
        public const string operation_reporte_etiqueta = "Reporte etiqueta";
        public const string operation_reporte_directorio = "Reporte directorio";
        public const string operation_reporte_historico_ac = "Reporte historico ac";
        public const string operation_reporte_ficha_resumida = "Reporte ficha res";
        public const string operation_reporte_timbre = "Reporte timbres";
        public const string operation_reporte_asamblea = "Reporte asamblea";
        public const string operation_reporte_tesis = "Reporte tesis";
        public const string operation_reporte_diploma_nuevo = "Reporte diploma nuevo";
        public const string operation_historial_usu = "Reporte historial usu";
        public const string operation_reporte_timbre_bod = "Reporte timbre bod";
        public const string operation_reporte_resumen_inv = "Reporte resumen inv";
        public const string operation_reporte_timbre_caj = "Reporte timbre caj";
        public const string operation_reporte_entrada_sali = "Reporte entrada sali";
        public const string operation_reporte_certificacion = "Reporte certificacion";
        public const string operation_reporte_carnet = "Reporte carnet";
        public const string operation_reporte_calculo_actuario = "Reporte calculo actuario";

        public const string operation_reporte_ingreso_timbre_bod = "Ingreso Timbre Bod";
        public const string operation_reporte_entrega_timbre_caj = "Entrega Timbre Caj";
        public const string operation_reporte_entradas_salidas = "Entradas Salidas";
        public const string operation_reporte_resumen_inventario_tim = "Resumen Inventario tim";
        public const string operation_reporte_resumen_caja_dia = "Resumen Caja Dia";

        public const string operation_ver_saldo = "Ver saldo";
        public const string operation_ver_reporte_general = "ver reportes genel";
        public const string operation_editar_medio_pago_recibo = "Editar medio pago recibo";
        public const string operation_ver_convenio = "Ver convenio";
        public const string operation_ver_planilla = "Ver planilla";
        public const string operation_cambiar_fecha_academ = "Cambio fecha academ";
        
        public const string operation_actualizar_todos_estatus = "Actualizar todos estatus";

        public const string http_content_header_json = "application/pdf";

        public const string html_pdf_remplace_name = "[NameAssisten]";

        public const string html_pdf_remplace_No_reciber = "[numerorecibo]";
        public const string html_pdf_remplace_date = "[fecha]";
        public const string html_pdf_remplace_No_referree = "[Nocolegiado]";
        public const string html_pdf_remplace_name_referee = "[nombrecolegiado]";
        public const string html_pdf_remplace_address = "[direccion]";
        public const string html_pdf_remplace_type_economic_reciber = "[tiposcargoeconomicorecibo]";
        public const string html_pdf_remplace_type_economic = "[tiposcargoeconomico]";
        public const string html_pdf_remplace_total_letter = "[totalletras]";
        public const string html_pdf_remplace_total = "[total]";
        public const string html_pdf_remplace_cashier = "[nombrecajero]";
        public const string html_pdf_remplace_No_document = "[numerodoc]";
        public const string html_pdf_remplace_remake = "[observaciones]";
        public const string html_pdf_remplace_print = "[impresion]";
        public const string html_pdf_remplace_cash = "[Efectivo]";
        public const string html_pdf_remplace_cheque = "[cheque]";
        public const string html_pdf_remplace_others = "[otros]";
        public const string html_pdf_remplace_amount = "[amount]";

        public const string html_pdf_remplace_date_birth = "[fechadenacimiento]";
        public const string html_pdf_remplace_sexo = "[sexo]";
        public const string html_pdf_remplace_dpi = "[dpi]";
        public const string html_pdf_remplace_nationality = "[nacionalidad]";
        public const string html_pdf_remplace_state_civil = "[estadocivil]";
        public const string html_pdf_remplace_no_account_referee = "[nocuentacolegiado]";
        public const string html_pdf_remplace_no_amount_referee = "[montocolegiado]";
        public const string html_pdf_remplace_no_account_timbre = "[nocuentatimbre]";
        public const string html_pdf_remplace_no_amount_timbre = "[montotimbre]";
        public const string html_pdf_remplace_no_amount_others = "[montootros]";
        public const string html_pdf_remplace_phone = "[telefono]";
        public const string html_pdf_remplace_title = "[titulo]";
        public const string html_pdf_remplace_university = "[universidad]";
        public const string html_pdf_remplace_list_address = "[listadireccion]";
        public const string html_pdf_remplace_list_phone = "[listatelefono]";
        public const string html_pdf_remplace_list_title = "[listatitulos]";
        public const string html_pdf_remplace_tupla_type_economic = "<div class=\"row\"><div  class=\"div10\"></div><div class=\"div70\"><p class=\"f4\">[tiposcargoeconomico]</p></div><div class=\"row\"><p class=\"f4C\">[amount]</p></div></div>";
        public const string html_pdf_remplace_tupla_direction = "<div class=\"row\"><p class=\"f2\">[direccion]</p></div>";
        public const string html_pdf_remplace_tupla_phone = "<div class=\"row\"><p class=\"f2\">[telefono]</p></div> ";
        public const string html_pdf_remplace_tupla_title = "<div class=\"div50\"><p class=\"f2\">[titulo]</p></div><div class=\"row\"><p class=\"f2\">[universidad]</p></div>";

        public const int type_document_pdf_gafete = 1;
        public const int type_document_pdf_diploma = 2;

        public const int state_event_for_print = 1;

        public const int state_event_inscription = 1;

        public const int state_sub_event_inscription = 1;
        public const int state_sub_event_assing = 2;
        public const int state_sub_event_cancel = 3;

        public const string url_dir_images = "~/images/";
        public const string url_dir_documents = "~/documents/";
        public const string url_dir_images_no_found = "sinfoto.jpg";
        public const string url_dir_images_inside_server_not_found = "~/Content/img/sinfoto.jpg";
        public const string url_dir_documents_angular = "documents/";
        public const string url_dir_print_doc = "/Payment/Print/";
        public const string url_dir_print_document = "/Payment/PrintDocument/";
        public const string url_dir_print_document_base_64 = "/Payment/PrintDocumentBase64/";

        public const int delivered_doc = 1;
        public const int notDelivered_doc = 0;

        public const string extension_image = @"^(.jpg|.jpeg|.png)$";

        public const string close_type_document = "Sin tipo";

        public const int id_System_Web = 1;

        public const int type_economic_offtime = 0;
        public const int type_economic_ontime = 1;

        public const int user_cash_get_list_no_assing = 0;
        public const int user_cash_get_list_assing = 1;

        public const string page_redirect_no_login_controller = "Home";
        public const string page_redirect_no_login_page = "Index";
        public const string page_redirect_no_permission_controller = "Referre";
        public const string page_redirect_no_permission_page = "Index";

        public const int text_query_is_number = 1;
        public const int text_query_is_text = 2;
        public const int text_query_is_like = 3;

        public const int timbre_status_enable = 1;
        public const int timbre_status_sold = 2;
        public const int timbre_status_out = 3;
        public const int timbre_status_use_referee = 5;

        public const int document_status_sign_proccess = 1;
        public const int document_status_sign_complete = 2;

        public const string table_var_aprox_decimal = "aproxDecimal";
        public const string table_var_ia = "Ia";
        public const string table_var_colegiado = "VarColegiado";
        public const string table_var_new_referee = "VarNuevoColegiado";
        public const string table_var_new_referee_diploma = "VarNuevoDiploma";
        public const string table_var_timbre = "VarTimbre";

        public const int type_economic_base = 130;
        public const int type_economic_postumo = 5;
        public const int type_economic_timbre_of_demand = 131;
        public const int type_economic_mora = 120;

        public const string url_server = "http://23.96.243.185:9011/Document/ValidarDocumento/";

        public const int salario_base = 8000;

        public const string varToken = "ColegioCCEE";

        public const int type_referee_convenio_pago = 14;

        public const string cod_document_accouting_value_1 = "41040101";
        public const string cod_document_accouting_value_2 = "41050101";
        public const string cod_document_accouting_value_3 = "41060101";
        public const string cod_document_accouting_value_4 = "21010101";
        public const string cod_document_accouting_value_5 = "41010101";
        public const string cod_document_accouting_value_6 = "41060101A";
        public const string cod_document_accouting_value_7 = "21010201";
        public const string cod_document_accouting_value_8 = "42010102";
        public const string cod_document_accouting_value_9 = "44010101";
        public const string cod_document_accouting_value_10 = "42010107";
        public const string cod_document_accouting_value_11 = "43010101";
        public const string cod_document_accouting_value_12 = "42010104";
        public const string cod_document_accouting_value_13 = "42010105";
        public const string cod_document_accouting_value_14 = "42010106";
        public const string cod_document_accouting_value_15 = "45010102";
        public const string cod_document_accouting_value_16 = "42010101";
        public const string cod_document_accouting_value_17 = "45010101";
        public const string cod_document_accouting_value_18 = "48020101";
        public const string cod_document_accouting_value_19 = "48020102";

        public const string member_page_redirect_login_controller = "Document";
        public const string member_page_redirect_login_page = "TimbreUsuario";
        public const string member_page_redirect_no_permission_controller = "UserProfile";
        public const string member_page_redirect_no_permission_page = "TimbreUsuario";

        public const int member_payment_Cashier = 3;
        public const int member_payment_Caja = 3;
        public const int member_payment_NoRecibo = 0;

    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_RefereeRevertController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_RefereeRevert
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_RefereeRevert/5
        public int Get(int id)
        {
            try
            {

                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return 0;


                var tempReferee = (from col in db.ccee_Colegiado
                                   where col.Col_Revertido == 1
                                   orderby col.Col_NoColegiado ascending
                                   select col.Col_NoColegiado).FirstOrDefault();
                                  


                Autorization.saveOperation(db, autorization, "Obteniendo Universidades");

                return tempReferee;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        // POST: api/Api_RefereeRevert
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_RefereeRevert/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_RefereeRevert/5
        public IHttpActionResult Delete(int id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    string autorization = Request.Headers.Authorization.Parameter;
                    if (!Autorization.enableAutorization(db, autorization)) return null;

                    ccee_Colegiado tempReferee = db.ccee_Colegiado.Find(id);
                    if(tempReferee == null)
                    {
                        transaction.Commit();
                        return Ok(HttpStatusCode.NotFound);
                    }


                    var listDocs = from doc in db.ccee_DocExpRequisito
                                   where doc.fk_Colegiado == id
                                   select doc;
                    foreach (var docs in listDocs)
                    {
                        db.ccee_DocExpRequisito.Remove(docs);
                    }

                    var listEsp = from esp in db.ccee_Especialidad
                                  where esp.pkfk_Colegiado == id
                                  select esp;

                    foreach (var esp in listEsp)
                    {
                        db.ccee_Especialidad.Remove(esp);
                    }

                    var listDir = from dir in db.ccee_Direccion
                                  where dir.fk_Colegiado == id
                                  select dir;

                    foreach (var dir in listDir)
                    {
                        var listDirFam = from dirf in db.ccee_DireccionFamiliar
                                         where dirf.fk_Direccion == dir.Dir_NoDireccion
                                         select dirf;

                        foreach (var dirf in listDirFam)
                        {
                            db.ccee_DireccionFamiliar.Remove(dirf);
                        }

                        db.ccee_Direccion.Remove(dir);

                    }

                    var listPhone = from tel in db.ccee_Telefono
                                    where tel.fk_Colegiado == id
                                    select tel;

                    foreach (var phone in listPhone)
                    {
                        db.ccee_Telefono.Remove(phone);
                    }

                    var listFam = from fam in db.ccee_Familiar
                                  where fam.fk_Colegiado == id
                                  select fam;

                    foreach (var fam in listFam)
                    {
                        db.ccee_Familiar.Remove(fam);
                    }

                    var listHis = from his in db.ccee_HistoricoLaboral
                                  where his.fk_Colegiado == id
                                  select his;

                    foreach (var his in listHis)
                    {
                        db.ccee_HistoricoLaboral.Remove(his);
                    }

                    var listMail = from mai in db.ccee_Mail
                                   where mai.fk_Colegiado == id
                                   select mai;

                    foreach (var mail in listMail)
                    {
                        db.ccee_Mail.Remove(mail);
                    }

                    tempReferee.Col_FechaColegiacion = null;
                    tempReferee.Col_PrimerNombre = "";
                    tempReferee.Col_SegundoNombre = "";
                    tempReferee.Col_TercerNombre = "";
                    tempReferee.Col_PrimerApellido = "";
                    tempReferee.Col_SegundoApellido = "";
                    tempReferee.Col_CasdaApellido = "";

                    tempReferee.Col_RegCedula = "";
                    tempReferee.Col_Registro = "";
                    tempReferee.Col_CedulaExtendida = "";
                    tempReferee.Col_Dpi = "";
                    tempReferee.Col_DpiExtendido = "";
                    tempReferee.Col_EstadoCivil = 1;
                    tempReferee.Col_Sexo = 0; 
                    tempReferee.Col_FechaNacimiento = null;

                    tempReferee.Col_Nit = "";
                    tempReferee.Col_Observaciones = "";
                    tempReferee.Col_Notificacion = "";
                    tempReferee.Col_FechaJuramentacion = null;
                    tempReferee.Col_Salario = 0;
                    tempReferee.fk_TipoColegiado = 1;

                    tempReferee.fk_Sede = null;
                    tempReferee.fk_SedeRegistrada = null;
                    tempReferee.Col_FechaFallecimiento = null;
                    tempReferee.Col_Revertido = 1;

                    


                    db.Entry(tempReferee).State = EntityState.Modified;
                    Autorization.saveOperation(db, autorization, "Actualizando colegiado " + tempReferee.Col_NoColegiado);
                    db.SaveChanges();


                    transaction.Commit();
                    return Ok(HttpStatusCode.OK);

                    //ccee_Colegiado tempCol = db.ccee_Colegiado.Find(id);
                    //if (tempCol == null)
                    //{
                    //    return Ok(HttpStatusCode.NotFound);
                    //}

                    //var lisEsp = from esp in db.ccee_Especialidad
                    //             where esp.pkfk_Colegiado == id
                    //             select esp;

                    //foreach (var esp in lisEsp)
                    //{
                    //    try
                    //    {
                    //        db.ccee_Especialidad.Remove(esp);
                    //    }
                    //    catch (Exception)
                    //    {

                    //        throw;
                    //    }
                    //}



                    //db.ccee_Colegiado.Remove(tempCol);

                    
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return Ok(HttpStatusCode.BadRequest);
                }
            }
        }





    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.TimbreChange;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_TimbreChangeController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_TimbreChange
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_TimbreChange/5
        public string Get(int id)
        {
            return "value";
        }

        [Route("api/Api_TimbreChange/{page}/{timbre}/{cashier}/{status}/{value}")]
        public RequestTimbreChangeJson Get(int page, int timbre, int cashier, bool status, int value)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                int statusTimbre = (status) ? Constant.timbre_status_enable : Constant.timbre_status_sold;
                RequestTimbreChangeJson temTimbre = null;

                if(cashier == 0)
                {
                    temTimbre = (from tim in db.ccee_Timbre
                                     join hoj in db.ccee_HojaTimbre on tim.fk_HojaTimbre equals hoj.HoT_NoHojaTimbre
                                     join val in db.ccee_ValorTimbre on tim.fk_ValorTimbre equals val.VaT_NoValorTimbre
                                     where tim.Tim_NumeroTimbre == timbre &&
                                     hoj.HoT_NumeroHoja == page &&
                                     tim.Tim_Estatus == statusTimbre && 
                                     val.VaT_NoValorTimbre == value
                                     select new RequestTimbreChangeJson()
                                     {
                                         id = tim.Tim_NoTimbre,
                                         numberTimbre = tim.Tim_NumeroTimbre,
                                         numberPage = hoj.HoT_NumeroHoja,
                                         valor = val.VaT_Denominacion
                                     }).FirstOrDefault();
                }else
                {
                    temTimbre = (from tim in db.ccee_Timbre
                                     join hoj in db.ccee_HojaTimbre on tim.fk_HojaTimbre equals hoj.HoT_NoHojaTimbre
                                     join val in db.ccee_ValorTimbre on tim.fk_ValorTimbre equals val.VaT_NoValorTimbre
                                     where tim.Tim_NumeroTimbre == timbre &&
                                     hoj.HoT_NumeroHoja == page && hoj.fk_Cajero == cashier &&
                                     tim.Tim_Estatus == statusTimbre &&
                                     val.VaT_NoValorTimbre == value
                                 select new RequestTimbreChangeJson()
                                     {
                                         id = tim.Tim_NoTimbre,
                                         numberTimbre = tim.Tim_NumeroTimbre,
                                         numberPage = hoj.HoT_NumeroHoja,
                                         valor = val.VaT_Denominacion
                                     }).FirstOrDefault();
                }


                Autorization.saveOperation(db, autorization, "Solicitud de timbre para cambio de timbre");
                return temTimbre;

            }
            catch (Exception)
            {
                return null;
            }
        }

        [Route("api/Api_TimbreChange/{page}/{timbreFist}/{timbreEnd}/{cashier}/{status}/{value}")]
        public IEnumerable<RequestTimbreChangeJson> Get(int page, int timbreFist, int timbreEnd, int cashier, bool status, int value)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                int statusTimbre = (status) ? Constant.timbre_status_enable : Constant.timbre_status_sold;
                IEnumerable<RequestTimbreChangeJson> temTimbre = null;

                if (cashier == 0)
                {
                    temTimbre = (from tim in db.ccee_Timbre
                                 join hoj in db.ccee_HojaTimbre on tim.fk_HojaTimbre equals hoj.HoT_NoHojaTimbre
                                 join val in db.ccee_ValorTimbre on tim.fk_ValorTimbre equals val.VaT_NoValorTimbre
                                 where tim.Tim_NumeroTimbre >= timbreFist &&
                                 tim.Tim_NumeroTimbre <= timbreEnd && 
                                 hoj.HoT_NumeroHoja == page &&
                                 
                                 val.VaT_NoValorTimbre == value
                                 select new RequestTimbreChangeJson()
                                 {
                                     id = tim.Tim_NoTimbre,
                                     numberTimbre = tim.Tim_NumeroTimbre,
                                     numberPage = hoj.HoT_NumeroHoja,
                                     valor = val.VaT_Denominacion
                                 });//tim.Tim_Estatus == statusTimbre && para que se verifique el estatus de busqueda
                }
                else
                {
                    temTimbre = (from tim in db.ccee_Timbre
                                 join hoj in db.ccee_HojaTimbre on tim.fk_HojaTimbre equals hoj.HoT_NoHojaTimbre
                                 join val in db.ccee_ValorTimbre on tim.fk_ValorTimbre equals val.VaT_NoValorTimbre
                                 where tim.Tim_NumeroTimbre >= timbreFist &&
                                 tim.Tim_NumeroTimbre <= timbreEnd &&
                                 hoj.HoT_NumeroHoja == page && hoj.fk_Cajero == cashier &&
                                 tim.Tim_Estatus == statusTimbre &&
                                 val.VaT_NoValorTimbre == value
                                 select new RequestTimbreChangeJson()
                                 {
                                     id = tim.Tim_NoTimbre,
                                     numberTimbre = tim.Tim_NumeroTimbre,
                                     numberPage = hoj.HoT_NumeroHoja,
                                     valor = val.VaT_Denominacion
                                 });
                }


                Autorization.saveOperation(db, autorization, "Solicitud de timbre para cambio de timbre");
                return temTimbre;

            }
            catch (Exception)
            {
                return null;
            }
        }


        // POST: api/Api_TimbreChange
        public IHttpActionResult Post(ResponseTimbreChangeJson timbres)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    string autorization = Request.Headers.Authorization.Parameter;
                    if (!Autorization.enableAutorization(db, autorization)) return null;

                    foreach (RequestTimbreChangeJson item in timbres.removeTimbre)
                    {
                        ccee_Timbre timbre = db.ccee_Timbre.Find(item.id);
                        if (timbre == null) continue;
                        timbre.Tim_Estatus = Constant.timbre_status_enable;
                        timbre.Tim_Fecha = null;
                        db.Entry(timbre).State = EntityState.Modified;

                    }

                    foreach (RequestTimbreChangeJson item in timbres.addTimbre)
                    {
                        ccee_Timbre timbre = db.ccee_Timbre.Find(item.id);
                        if (timbre == null) continue;
                        timbre.Tim_Estatus = Constant.timbre_status_sold;
                        timbre.Tim_Fecha = DateTime.Today;
                        db.Entry(timbre).State = EntityState.Modified;

                    }
                    db.SaveChanges();
                    Autorization.saveOperation(db, autorization, "Cambio de timbre");
                    transaction.Commit();
                    return Ok(HttpStatusCode.OK);

                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return Ok(HttpStatusCode.BadRequest);
                }
            }
        }

        // PUT: api/Api_TimbreChange/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_TimbreChange/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

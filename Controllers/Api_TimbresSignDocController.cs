﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.TimbreSign;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_TimbresSignDocController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();
        private int pageSize = 2;

        // GET: api/Api_TimbresSignDoc
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_TimbresSignDoc/5
        public IHttpActionResult Get(string id)
        {
            try
            {
                var tempDocument = new
                {
                    id = id
                };

                var client = new RestClient("http://104.248.227.154:4023");
                RestRequest restSend = HttpRequestRest.getRequestHttpPost(client, "api/timbre/find", tempDocument);
                var response = client.Execute(restSend);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string content = response.Content;
                    var info = new JavaScriptSerializer().Deserialize<ResponseStatusDocumentJson>(content);
                    byte[] bytes = Convert.FromBase64String(info.base64);

                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        /*HttpResponseMessage responseM = Request.CreateResponse();
                        using (WebClient webClient = new WebClient())
                        {
                            responseM.Headers.AcceptRanges.Add("bytes");
                            responseM.StatusCode = HttpStatusCode.OK;
                            //response.Content =  new StreamContent(fi.ReadStream());
                            responseM.Content = new StreamContent(ms);
                            responseM.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("render");
                            responseM.Content.Headers.ContentDisposition.FileName = "colegio_ccee.pdf";
                            responseM.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");//("application/octet-stream");
                            responseM.Content.Headers.ContentLength = ms.Length;

                        }
                        return responseM;*/
                        

                        HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                        {
                            Content = new StreamContent(ms)
                        };
                        httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                        {
                            FileName = "ccee.pdf"
                        };
                        httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

                        ResponseMessageResult responseMessageResult = ResponseMessage(httpResponseMessage);
                        return responseMessageResult;

                        //return File(ms, Constant.http_content_header_json);

                    }

                }
                

                return null;
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }

        // GET: api/Api_TimbresSignDoc/5/10
        [Route("api/Api_TimbresSignDoc/{id}/{page}")]
        public List<ResponseListtimbreSignJson> Get(int id, int page)
        {
            try
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    List<ResponseListtimbreSignJson> lisDocs = new List<ResponseListtimbreSignJson>();
                    var docs = db.ccee_DocumentoFirmado.Where(o => o.fk_Colegiado == id)
                                    .OrderBy(p => p.DocFir_NoDocumentoFirmado)
                                    .Skip((page - 1) * pageSize)
                                    .Take(pageSize).ToList();
                    foreach (var item in docs)
                    {

                        if (item.DocFir_Estatus == Constant.document_status_sign_proccess)
                        {
                            var tempDocument = new
                            {
                                id = item.DocFir_IdMongo
                            };

                            var client = new RestClient("http://159.65.44.11:4023");
                            RestRequest restSend = HttpRequestRest.getRequestHttpPost(client, "api/timbre/find", tempDocument);
                            var response = client.Execute(restSend);
                            if (response.StatusCode == HttpStatusCode.OK)
                            {
                                string content = response.Content;
                                var info = new JavaScriptSerializer().Deserialize<ResponseStatusDocumentJson>(content);
                                if (info.status == 1)
                                {
                                    item.DocFir_Estatus = Constant.document_status_sign_complete;
                                    db.Entry(item).State = EntityState.Modified;
                                }
                            }
                        }

                        lisDocs.Add(new ResponseListtimbreSignJson()
                        {
                            id = item.DocFir_NoDocumentoFirmado,
                            name = item.DocFir_Nombre,
                            date = item.DocFir_HoraFecha,
                            key = item.DocFir_IdMongo,
                            status = item.DocFir_Estatus
                        });

                    }
                    db.SaveChanges();
                    transaction.Commit();
                    return lisDocs;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_TimbresSignDoc
        public ResponseTimbreSingNotifyJson Post(RequestTimbreSignJson timbreSign)
        {

            try
            {
                var client = new RestClient("http://159.65.44.11:4023");
                RestRequest restSend = HttpRequestRest.getRequestHttpPost(client, "api/timbre", timbreSign);
                var response = client.Execute(restSend);
                
                string content = response.Content;
                if(response.StatusCode == HttpStatusCode.OK)
                {
                    var info = new JavaScriptSerializer().Deserialize<ResponseTimbreSingNotifyJson>(content);

                    using (var transaction = db.Database.BeginTransaction())
                    {
                        try
                        {
                            List<TimbresSignDocs> listTimbres = timbreSign.timbres;
                            foreach (TimbresSignDocs timbres in listTimbres)
                            {
                                ccee_Timbre tempTimbre = db.ccee_Timbre.Find(timbres.idTimbre);
                                if (tempTimbre == null)
                                {
                                    transaction.Rollback();
                                    return null;
                                }

                                tempTimbre.Tim_Estatus = Constant.timbre_status_use_referee;
                                db.Entry(tempTimbre).State = EntityState.Modified;
                            }

                            ccee_DocumentoFirmado tempDocumentSign = new ccee_DocumentoFirmado();
                            tempDocumentSign.DocFir_Nombre = timbreSign.name;
                            tempDocumentSign.DocFir_HoraFecha = DateTime.Now;
                            tempDocumentSign.DocFir_IdMongo = info.id;
                            tempDocumentSign.DocFir_Estatus = Constant.document_status_sign_proccess;
                            tempDocumentSign.fk_Colegiado = timbreSign.colegiado;
                            db.ccee_DocumentoFirmado.Add(tempDocumentSign);

                            db.SaveChanges();
                            transaction.Commit();
                            return info;
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            return null;
                        }
                        
                    }

                }else
                {
                    return null;
                }
                
            }
            catch (Exception)
            {
                return null;
            }

        }

        // PUT: api/Api_TimbresSignDoc/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_TimbresSignDoc/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

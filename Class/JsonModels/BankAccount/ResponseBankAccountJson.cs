﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.BankAccount
{
    public class ResponseBankAccountJson
    {
        public int id { get; set; }
        public string NoAccount { get; set; }
        public string codigo { get; set; }
        public string format { get; set; }
        public string name { get; set; }
        public string bank { get; set; }
    }
}
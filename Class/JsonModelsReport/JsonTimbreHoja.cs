﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonTimbreHoja
    {
        public int hoja { get; set; }
        public string fecha {
            get
            {
                return ((tempFecha == null) ? "" : tempFecha.Value.ToString("dd/MM/yyyy"));
            }
        }
        public DateTime? tempFecha { get; set; }
        public decimal valor { get; set; }
        public decimal timbre { get; set; }
    }
}
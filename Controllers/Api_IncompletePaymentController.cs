﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.IncompletePayment;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_IncompletePaymentController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_IncompletePayment
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_IncompletePayment/5
        [Route("api/Api_IncompletePayment/{dateInit}/{id}")]
        public List<ResponseIncompletePaymentJson> Get(string dateInit, int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                string query = "select sum(t.monto) as amount, t.pago as pay, t.col as referee " +
                                "from( " +
                                "select ccee_CargoEconomico_colegiado.CEcC_Monto as monto, ccee_CargoEconomico_colegiado.fk_TipoCargoEconomico as tipo, " +
                                "ccee_Pago.Pag_NoPago as pago, ccee_CargoEconomico_colegiado.fk_DocContable as con, ccee_CargoEconomico_colegiado.fk_Colegiado as col " +
                                "from ccee_CargoEconomico_colegiado, ccee_Pago " +
                                "where ccee_CargoEconomico_colegiado.fk_Pago = ccee_Pago.Pag_NoPago " +
                                "and ccee_Pago.Pag_Fecha = '[dateInit]' " +
                                "and ccee_Pago.fk_Cajero = [id] " +
                                "UNION ALL " +
                                "select ccee_CargoEconomico_timbre.CEcT_Monto as monto, ccee_CargoEconomico_timbre.fk_TipoCargoEconomico as tipo, ccee_Pago.Pag_NoPago as pago " +
                                ", ccee_CargoEconomico_timbre.fk_DocContable as con, ccee_CargoEconomico_timbre.fk_Colegiado as col " +
                                "from ccee_CargoEconomico_timbre, ccee_Pago " +
                                "where ccee_CargoEconomico_timbre.fk_Pago = ccee_Pago.Pag_NoPago " +
                                "and ccee_Pago.Pag_Fecha = '[dateInit]' " +
                                "and ccee_Pago.fk_Cajero = [id] " +
                                "UNION ALL " +
                                "select ccee_CargoEconomico_otros.CEcO_Monto as monto, ccee_CargoEconomico_otros.fk_TipoCargoEconomico as tipo, ccee_Pago.Pag_NoPago as pago " +
                                ", ccee_CargoEconomico_otros.fk_DocContable as con, ccee_CargoEconomico_otros.fk_Colegiado as col " +
                                "from ccee_CargoEconomico_otros, ccee_Pago " +
                                "where ccee_CargoEconomico_otros.fk_Pago = ccee_Pago.Pag_NoPago " +
                                "and ccee_Pago.Pag_Fecha = '[dateInit]' " +
                                "and ccee_Pago.fk_Cajero = [id] ) as t " +
                                "where t.con is null " +
                                "group by t.pago, t.col";
                query = query.Replace("[dateInit]", dateInit);
                query = query.Replace("[id]", Convert.ToString(id));
         
                var pLis = db.Database.SqlQuery<ResponseIncompletePaymentJson>(query).ToList();
                Autorization.saveOperation(db, autorization, "Obteniendo listado de pagos incompletos");
                return pLis;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_IncompletePayment
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_IncompletePayment/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_IncompletePayment/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.UserWS
{
    public class ResponseUserWSJson
    {
        public string authorization { get; set; }
        public int status { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels._Tools;
using webcee.Class.JsonModels.ListedDocuments;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_ListedDocumentsController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();
        // GET: api/Api_ListedDocuments
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_ListedDocuments/5
        public List<ModelListDocumentJson> Get(int id)
        {
            try
            {
                List<ModelListDocumentJson> listDocuments = new List<ModelListDocumentJson>();
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                Payment payment = new Payment(db);

                int typCertificateNew = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "VarCertificacionNuevoCol"));

                List<int?> listPay = payment.findIdPayWithDoc(id);
                if (listPay.Count() > 0)
                {
                    foreach (var item in listPay)
                    {
                        if (item == null) continue;
                        var tempEco = from eco in db.ccee_CargoEconomico_otros
                                      join tip in db.ccee_TipoCargoEconomico on eco.fk_TipoCargoEconomico equals tip.TCE_NoTipoCargoEconomico
                                      join doc in db.ccee_Documento on eco.CEcO_NoCargoEconomico equals doc.fk_CargoEconomicoOtros
                                      where eco.fk_Pago == item
                                      select new
                                      {
                                          TypeEconomic = tip.TCE_NoTipoCargoEconomico,
                                          Condicion = tip.TCE_CondicionCalculo,
                                          Descripcion = tip.TCE_Descripcion,
                                          NoDocumento = doc.Doc_NoDocumento
                                      };

                        foreach (var itemEco in tempEco)
                        {
                            //Cambio de estado para el objeto de nuevo colegiado
                            if (itemEco.TypeEconomic == typCertificateNew)
                            {
                                listDocuments.Add(new ModelListDocumentJson()
                                {
                                    idDocument = itemEco.NoDocumento,
                                    direccion = Constant.url_dir_print_document + itemEco.NoDocumento + "/" + 1,
                                    title = itemEco.Descripcion
                                });
                            }

                            switch (itemEco.Condicion)
                            {
                                case "VarCertificacionCol":
                                    listDocuments.Add(new ModelListDocumentJson()
                                    {
                                        idDocument = itemEco.NoDocumento,
                                        direccion = Constant.url_dir_print_document + itemEco.NoDocumento + "/" + 1,
                                        title = itemEco.Descripcion
                                    });
                                    break;
                                case "VarCertificacionColOdl":
                                    listDocuments.Add(new ModelListDocumentJson()
                                    {
                                        idDocument = itemEco.NoDocumento,
                                        direccion = Constant.url_dir_print_document + itemEco.NoDocumento + "/" + 1,
                                        title = itemEco.Descripcion
                                    });
                                    break;
                                case "VarCertificacionColFecha":
                                    listDocuments.Add(new ModelListDocumentJson()
                                    {
                                        idDocument = itemEco.NoDocumento,
                                        direccion = Constant.url_dir_print_document + itemEco.NoDocumento + "/" + 1,
                                        title = itemEco.Descripcion
                                    });
                                    break;
                            }
                        }
                    }
                }
                return listDocuments;

            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_ListedDocuments
        public List<ResponseListedDocuments> Post(RequestListedDocuments list)
        {

            string autorization = Request.Headers.Authorization.Parameter;
            if (!Autorization.enableAutorization(db, autorization)) return null;

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {

                    List<ResponseListedDocuments> docs = new List<ResponseListedDocuments>();
                    List<int> listReferee = createListReferee(list.range);

                    ccee_Cajero tempCaj = db.ccee_Cajero.Find(list.idCashier);
                    ccee_Usuario tempUser = db.ccee_Usuario.Find(tempCaj.fk_Usuario);
                    string nameUser = tempUser.Usu_Nombre + " " + tempUser.Usu_Apellido;

                    if (list.typeDocument == 1)
                    {
                        
                        int economicVariable = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "VarCargoVariable"));
                        var idEconomic = db.ccee_CargoEconomico_otros
                                    .Where(p => p.fk_Pago == list.idPay &&
                                            p.fk_TipoCargoEconomico == economicVariable)
                                    .Select(o => o.CEcO_NoCargoEconomico)
                                    .FirstOrDefault();

                        if (idEconomic == 0) return null;

                        ccee_TipoDoc typeDoc = db.ccee_TipoDoc.Find(3);

                        foreach (var referee in listReferee)
                        {
                            ccee_Colegiado tempReferee = db.ccee_Colegiado.Find(referee);

                            if (tempReferee == null) continue;

                            int idDocu = 0;

                            string nameReferee = tempReferee.Col_PrimerNombre + " " + tempReferee.Col_SegundoNombre + " " +
                                                   tempReferee.Col_TercerNombre + " " +
                                                tempReferee.Col_PrimerApellido + " " + tempReferee.Col_SegundoApellido + " " + 
                                                tempReferee.Col_CasdaApellido;

                            PrintDocNotAccount printNot = new PrintDocNotAccount(db, referee);
                            idDocu = printNot.createDocCertification(typeDoc.TDC_NoTipoDoc, idEconomic,
                                                                    nameReferee, referee.ToString(),
                                                                    typeDoc.TDC_XmlPlantilla,
                                                                    typeDoc.TDC_XmlPlantilla,
                                                                    referee, tempReferee.Col_FechaColegiacion, null, null, null, list.idCashier);
                            docs.Add(new ResponseListedDocuments()
                            {
                                address = Constant.url_dir_print_document + idDocu + "/" + 1,
                                idDocument = idDocu,
                                idReferee = referee
                            });
                        }
                        db.SaveChanges();
                        Autorization.saveOperation(db, autorization, "Obteniendo Listado Documentos");
                        transaction.Commit();
                        
                    }
                    else if (list.typeDocument == 2)
                    {
                        string diplomaReferee = GeneralFunction.getKeyTableVariable(db, Constant.table_var_new_referee_diploma);
                        int idDiplomaReferee = Convert.ToInt32(diplomaReferee);
                        ccee_TipoDoc tipo = db.ccee_TipoDoc.Find(6);

                        foreach (var referee in listReferee)
                        {
                            ccee_Colegiado cole = db.ccee_Colegiado.Find(referee);
                            if (cole == null) continue;

                            int idEconomic = (db.ccee_CargoEconomico_otros.Where(o => o.fk_TipoCargoEconomico == idDiplomaReferee)
                                                                    .Where(p => p.fk_Colegiado == referee)
                                                                    .Where(q => q.fk_Pago != null)
                                                                    .Select(r => r.CEcO_NoCargoEconomico))
                                                                    .FirstOrDefault();
                            if (idEconomic != 0)
                            {
                                ccee_Documento doc = (db.ccee_Documento
                                                        .Where(o => o.fk_CargoEconomicoOtros == idEconomic))
                                                        .FirstOrDefault();

                                if (doc == null)
                                {
                                    
                                    //ccee_Colegiado cole = db.ccee_Colegiado.Find(referee);
                                    string template = tipo.TDC_XmlPlantilla;
                                    string name = cole.Col_PrimerNombre + " " + cole.Col_SegundoNombre + " " + cole.Col_PrimerApellido + " " + cole.Col_SegundoApellido;

                                    template = template.Replace(Constant.html_pdf_remplace_No_referree, name);

                                    ccee_Documento tempDoc = new ccee_Documento();
                                    tempDoc.Doc_Observacion = name;
                                    tempDoc.Doc_FechaHoraEmision = DateTime.Now;
                                    tempDoc.Doc_Xml = template;
                                    tempDoc.Doc_HashDoc = "";
                                    tempDoc.fk_TipoDoc = tipo.TDC_NoTipoDoc;
                                    tempDoc.fk_CargoEconomicoOtros = idEconomic;

                                    db.ccee_Documento.Add(tempDoc);
                                    db.SaveChanges();

                                    docs.Add(new ResponseListedDocuments()
                                    {
                                        address = "/Payment/PrintDocument/" + tempDoc.Doc_NoDocumento + "/" + 2,
                                        idDocument = tempDoc.Doc_NoDocumento,
                                        idReferee = referee
                                    });
                                    
                                }
                                else
                                {
                                    docs.Add(new ResponseListedDocuments()
                                    {
                                        address = "/Payment/PrintDocument/" + doc.Doc_NoDocumento + "/" + 2,
                                        idDocument = doc.Doc_NoDocumento,
                                        idReferee = referee
                                    });
                                }
                            }
                        }
                        db.SaveChanges();
                        Autorization.saveOperation(db, autorization, "Obteniendo Listado Documentos");
                        transaction.Commit();

                    }
                    return docs;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }

        }

        // PUT: api/Api_ListedDocuments/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_ListedDocuments/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private List<int> createListReferee(string texto)
        {
            List<int> listReferee = new List<int>();
            string[] divText = texto.Split(',');
            for (int i = 0; i < divText.Length; i++)
            {
                string[] divRangeText = divText[i].Split('-');
                if (divRangeText.Length > 1)
                {
                    int initRange = Convert.ToInt32(divRangeText[0]);
                    int finishRange = Convert.ToInt32(divRangeText[1]);

                    var tempCol = from col in db.ccee_Colegiado
                                  where col.Col_NoColegiado >= initRange &&
                                  col.Col_NoColegiado <= finishRange
                                  select new
                                  {
                                      NoCol = col.Col_NoColegiado
                                  };

                    foreach (var item in tempCol)
                    {
                        listReferee.Add(item.NoCol);
                    }

                    //for (int j = initRange; j <= finishRange; j++)
                    //{
                    //    listReferee.Add(j);
                    //}
                }
                else
                {
                    listReferee.Add(Convert.ToInt32(divRangeText[0]));
                }
            }
            return listReferee;
        }


    }
}

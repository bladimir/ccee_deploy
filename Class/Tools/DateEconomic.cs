﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.Tools
{
    public class DateEconomic
    {


        public static int getMonth(DateTime last, DateTime now)
        {
            int countMonth = (last.Month - now.Month) + 12 * (last.Year - now.Year);
            return (countMonth <= 0) ? Math.Abs(countMonth) : 0;
        }

        public static int getYear(DateTime last, DateTime now)
        {
            return  (now.Year - last.Year);
        }

        public static DateTime islessDate(DateTime firstDate, DateTime secoundDate)
        {
            
            if (firstDate.Year < secoundDate.Year)
            {
                return firstDate;
            }else if(firstDate.Year == secoundDate.Year)
            {
                if (firstDate.Month < secoundDate.Month)
                {
                    return firstDate;
                }
                return secoundDate;
            }
            return secoundDate;

        }

    }
}
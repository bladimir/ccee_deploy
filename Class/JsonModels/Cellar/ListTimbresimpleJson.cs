﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Cellar
{
    public class ListTimbresimpleJson
    {
        public int noTimbre { get; set; }
        public int numberTimbre { get; set; }
        public int statusTimbre { get; set; }
    }
}
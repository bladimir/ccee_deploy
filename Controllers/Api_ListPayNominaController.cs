﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModelsReport;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_ListPayNominaController : ApiController
    {
        // GET: api/Api_ListPayNomina
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_ListPayNomina/5
        public List<JsonEstadoCuentaGeneral> Get(int id)
        {
            string query = remplazeQueryGen( id);
            decimal porcent = 0;
            using (var context = new CCEEEntities())
            {
                porcent = Convert.ToDecimal(GeneralFunction.getKeyTableVariable(context, "Porcentaje"));
                List<JsonTempEstadoCuentaC> pLis = context.Database.SqlQuery<JsonTempEstadoCuentaC>(query).ToList();
                
                List<JsonEstadoCuentaGeneral> listOtros = new List<JsonEstadoCuentaGeneral>();
                foreach (JsonTempEstadoCuentaC item in pLis)
                {
                    listOtros.Add(new JsonEstadoCuentaGeneral()
                    {
                        anio = item.fecha.Year,
                        Month = item.fecha.Month,
                        montoC = item.montoC,
                        montoT = item.montoT,
                        montoP = item.montoP,
                        tempfechaPago = item.fechaP,
                        recibo = item.recibos,
                        idRecibo = item.doc,
                        nombre = item.nombre,
                        url = Constant.url_dir_print_doc + item.doc,
                        porcent = (porcent / 100),
                        rechazado = item.rechazado,
                        congelado = item.congelado
                    });
                }



                return listOtros.OrderBy(a => a.anio)
                    .ThenBy(b => b.Month).ToList();
            }
        }

        // POST: api/Api_ListPayNomina
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_ListPayNomina/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_ListPayNomina/5
        public void Delete(int id)
        {
        }

        private string remplazeQueryGen(int idColegiado)
        {
            string query = "SELECT DISTINCT tab.fecha as fecha, tab.pago as pago, " +
                            "tab.C1 as montoC, tab.T1 as montoT, tab.P1 as montoP, doc , ccee_DocContable.DCo_NoRecibo as recibos, " +
                            "ccee_Pago.Pag_Fecha fechaP, ccee_DocContable.DCo_ANombre as nombre,  ccee_MedioPago_Pago_C.MPP_Rechazado as rechazado, ccee_Pago.Pag_Congelado as congelado   " +
                            "FROM( " +
                            "SELECT " +
                                 "fecha, " +
                                 "pago, " +
                                 "doc, " +
                                "COALESCE([120], 0) as C1, " +
                                "COALESCE([130], 0) as T1, " +
                                "COALESCE([5], 0) as P1 " +
                            "FROM " +
                                "( " +
                                    "select ccee_CargoEconomico_timbre.fk_Pago as pago " +
                                            ", ccee_CargoEconomico_timbre.fk_Colegiado as col, ccee_CargoEconomico_timbre.fk_TipoCargoEconomico as tipo " +
                                            ", ccee_CargoEconomico_timbre.CEcT_FechaGeneracion as fecha, ccee_CargoEconomico_timbre.CEcT_Monto as monto " +
                                            ", ccee_CargoEconomico_timbre.fk_DocContable as doc " +
                                    "from ccee_CargoEconomico_timbre " +
                                    "where ccee_CargoEconomico_timbre.fk_Colegiado = [colegiado] " +
                                ") as p " +
                            "PIVOT " +
                                "( " +
                                    "SUM(monto) FOR tipo IN([120], [130], [5]) " +
                                ") AS pPivot " +
                            ") as tab " +
                            ", ccee_DocContable, ccee_Pago, ccee_MedioPago_Pago_C " +
                            "WHERE tab.doc = ccee_DocContable.DCo_NoDocContable " +
                            "and tab.pago = ccee_Pago.Pag_NoPago " +
                            "and ccee_Pago.Pag_NoPago = ccee_MedioPago_Pago_C.fk_Pago " +
                            "and ccee_MedioPago_Pago_C.MPP_EsNomina  is not null ";
            query = query.Replace("[colegiado]", idColegiado.ToString());
            return query;
        }


    }
}

﻿(function () {
    angular.module('referee')
        .controller('addReferee', ['$scope', '$http', '$location', 'CONSTANT', '$anchorScroll', 'refereeFactory', function ($scope, $http, $location, CONSTANT, $anchorScroll, refereeFactory) {
            $scope.listTypeReferee = [];
            $scope.listHeadquarters;
            $scope.regexNit = CONSTANT.REGEX_NIT;
            $scope.regexDate = CONSTANT.REGEX_FECHA;
            $scope.regexDPI = CONSTANT.REGEX_DPI;
            $scope.formatDate = CONSTANT.FORMAT_DATE;
            $scope.numbers = CONSTANT.REGEX_DIGIT;
            $scope.regexAmount = CONSTANT.REGEX_AMOUNT;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.tempRefereeNumber = 0;
            $scope.refereeNumberRevert = 0;
            $scope.refereeNumberTemp = 0;
            $scope.showRevert = false;
            $scope.refereeRemark = "Fotostática Título [ ]\nFormulario Beneficiarios [ ]\nOtros [ ]\n";

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.setAuth = function () {
                refereeFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    refereeFactory.setAuthorization(response.data);
                    console.log(refereeFactory.getAuthorization());
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.init = function () {
                var tempIdNewReferee = 16;
                var tempIdNoIncorporado = 7;
                var listRefereeTemp;

                refereeFactory.actionGetFunction(CONSTANT.URL_API_TYPEREFEREE)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        listRefereeTemp = response.data;
                        for (var i = 0; i < listRefereeTemp.length; i++) {
                            if (tempIdNewReferee == listRefereeTemp[i].idTypeReferee ||
                                    tempIdNoIncorporado == listRefereeTemp[i].idTypeReferee) {
                                $scope.listTypeReferee.push(listRefereeTemp[i]);
                            }
                        }

                        //$scope.listTypeReferee = response.data;
                        //if ($scope.listTypeReferee != null) {
                        //    $scope.refereeTypeReferee = $scope.listTypeReferee[$scope.listTypeReferee.length - 1];
                        //} 
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });


                refereeFactory.actionGetFunction(CONSTANT.URL_API_REFEREENEW)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.refereeNumber = response.data;
                        $scope.tempRefereeNumber = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });

                refereeFactory.actionGetFunction(CONSTANT.URL_API_HEADQUARTERS)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listHeadquarters = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });

                refereeFactory.actionGetIntFunction(CONSTANT.URL_GET_REFEREEREVERT, 1)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.refereeNumberRevert = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });

                refereeFactory.actionGetIntFunction(CONSTANT.URL_REFEREETEMP, 1)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.refereeNumberTemp = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });

            }

            


            $scope.addReferee = function () {
                $scope.showLoadingFunction();
                var information = {
                    NoColegiado: $scope.refereeNumber,
                    FechaColegiacion: $scope.refereeDateReferee,

                    PrimerNombre: $scope.refereeFistName,
                    SegundoNombre: $scope.refereeSecoundName,
                    TercerNombre: $scope.refereeThird,
                    PrimerApellido: $scope.refereeFirstLastName,
                    SegundoApellido: $scope.refereeSecoundLastName,
                    CasdaApellido: $scope.refereeMarriedLastName,
                    
                    RegCedula: $scope.refereeNumberRegCedula,
                    Registro: $scope.refereeNumberOrdenCedula,
                    CedulaExtendida: $scope.refereeExtCedula,
                    Dpi: $scope.refereeNumberDPI,
                    DpiExtendido: $scope.refereeExtDPI,

                    EstadoCivil: $scope.refereeCivilState,
                    Sexo: $scope.refereeSex,
                    FechaNacimiento: $scope.refereeDateBirth,
                    Nit: $scope.refereeNit,
                    Observaciones: $scope.refereeRemark,
                    Notificacion: $scope.refereeNotify,
                    Estatus: 0,
                    FechaJuramentacion: $scope.refereeSweReferee,
                    fechaGraduacion: $scope.refereeDateGradu,

                    idTipoColegiacion: (($scope.refereeTypeReferee == null) ? 0 : $scope.refereeTypeReferee.idTypeReferee),
                    idSede: (($scope.selectHeadquar == null) ? null : $scope.selectHeadquar.id),
                    idSedeActual: (($scope.selectHeadquarReg == null) ? null : $scope.selectHeadquarReg.id),
                    salario: $scope.refereeSalary,
                    placeBirth: $scope.placeBirthReferee,
                    nacionality: $scope.nacionalityReferee

                }

                refereeFactory.actionPostFunction(CONSTANT.URL_API_REFEREE, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.changeFiles();
                            } else if (response.data == 409) {
                                $scope.refereeNumber = "Numero ya utilizado";
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });

            }

            $scope.revertNumber = function () {
                $scope.showRevert = true;
                $scope.refereeNumber = $scope.refereeNumberRevert;
            }

            $scope.revertTemp = function () {
                $scope.showRevert = false;
                $scope.refereeNumber = $scope.refereeNumberTemp;
            }

            $scope.revertNumberCancel = function () {
                $scope.showRevert = false;
                $scope.refereeNumber = $scope.tempRefereeNumber;
            }

            $scope.changeFiles = function () {
                window.location.href = '/Referre/FileReferee/' + $scope.refereeNumber;
            }

            $scope.updateReferee = function () {
                $scope.showLoadingFunction();
                var gender = "";
                $scope.statusUpdate = "";
                console.log($scope.dateSweReferee);

                var informacion = {

                    FechaColegiacion: $scope.refereeDateReferee,

                    PrimerNombre: $scope.refereeFistName,
                    SegundoNombre: $scope.refereeSecoundName,
                    TercerNombre: $scope.refereeThird,
                    PrimerApellido: $scope.refereeFirstLastName,
                    SegundoApellido: $scope.refereeSecoundLastName,
                    CasdaApellido: $scope.refereeMarriedLastName,

                    RegCedula: $scope.refereeNumberRegCedula,
                    Registro: $scope.refereeNumberOrdenCedula,
                    CedulaExtendida: $scope.refereeExtCedula,
                    Dpi: $scope.refereeNumberDPI,
                    DpiExtendido: $scope.refereeExtDPI,

                    EstadoCivil: $scope.refereeCivilState,
                    Sexo: $scope.refereeSex,
                    FechaNacimiento: $scope.refereeDateBirth,
                    Nit: $scope.refereeNit,
                    Observaciones: $scope.refereeRemark,
                    Notificacion: $scope.refereeNotify,
                    Estatus: 0,
                    FechaJuramentacion: $scope.refereeSweReferee,
                    idTipoColegiacion: (($scope.refereeTypeReferee == null) ? 0 : $scope.refereeTypeReferee.idTypeReferee),
                    idSede: $scope.selectHeadquar.id,
                    idSedeActual: $scope.selectHeadquarReg.id,
                    salario: $scope.refereeSalary,
                    placeBirth: $scope.placeBirthReferee,
                    nacionality: $scope.nacionalityReferee

                }

                console.log(informacion);

                refereeFactory.actionPutFunction(CONSTANT.URL_API_REFEREE, $scope.refereeNumberRevert, informacion)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                window.location.href = '/Referre/Index';
                            } else {
                                $scope.mensajeSubEvent = "";
                            }

                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });

            }
            

        }]);

})()
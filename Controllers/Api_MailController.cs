﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.Mail;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_MailController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();


        // GET: api/Api_Mail
        public IEnumerable<string> Get()
        {
            return null;
        }

        // GET: api/Api_Mail/5
        public IEnumerable<ResponseMailJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var contentMail = from mail in db.ccee_Mail
                                  where mail.fk_Colegiado == id
                                  select new ResponseMailJson()
                                  {
                                      id = mail.Mai_NoMail,
                                      mail = mail.Mai_MailDir
                                  };
                Autorization.saveOperation(db, autorization, "Obteniendo correos");
                return contentMail;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_Mail
        public void Post(RequestMailJson mail)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return;

                ccee_Mail tempMail = new ccee_Mail()
                {
                    Mai_MailDir = mail.mail,
                    fk_Colegiado = mail.idReferee
                };

                db.ccee_Mail.Add(tempMail);
                Autorization.saveOperation(db, autorization, "Agregando correo");
                db.SaveChanges();
                
            }
            catch (Exception)
            {

            }

        }

        // PUT: api/Api_Mail/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_Mail/5
        public IHttpActionResult Delete(int id)
        {
            string autorization = Request.Headers.Authorization.Parameter;
            if (!Autorization.enableAutorization(db, autorization)) return null;

            ccee_Mail tempMail = db.ccee_Mail.Find(id);
            if(tempMail == null)
            {
                return Ok(HttpStatusCode.NotFound);
            }
            try
            {
                db.ccee_Mail.Remove(tempMail);
                Autorization.saveOperation(db, autorization, "Eliminando correo");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonEstadoCuetaInfoCol
    {
        public string nombre { get; set; }
        public int colegiado { get; set; }
        public DateTime? tempsaldoCole { get; set; }
        public string saldoCole {
            get
            {
                return (tempsaldoCole == null) ? "" : tempsaldoCole.Value.ToString("dd/MM/yyyy");
            }
        }
        public DateTime? tempsaldoTimbre { get; set; }
        public string saldoTimbre {
            get
            {
                return (tempsaldoTimbre == null) ? "" : tempsaldoTimbre.Value.ToString("dd/MM/yyyy");
            }
        }
        public int edad { get; set; }
        public string activo { get; set; }
    }
}
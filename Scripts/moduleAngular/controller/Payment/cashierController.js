﻿(function () {
    angular.module('payment')
        .controller('cashier', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'paymentFactory', function ($scope, $http, CONSTANT, $anchorScroll, paymentFactory) {
            $scope.listData;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idUpdate = 0;
            $scope.objectUpdate;
            $scope.regexHour = CONSTANT.REGEX_HOUR;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                paymentFactory.actionGetFunction(CONSTANT.URL_API_CASHIER)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listData = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.setAuth = function () {
                paymentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        paymentFactory.setAuthorization(response.data);
                        $scope.init();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.get = function () {
                $scope.showLoadingFunction();
                paymentFactory.actionGetFunction(CONSTANT.URL_API_CASHIER)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listData = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.add = function () {
                $scope.showLoadingFunction();
                var information = {
                    active: $scope.cashierselectstate,
                    finalHour: $scope.cashierfinalHour,
                    initHour: $scope.cashierinitHour,
                    name: $scope.cashiername
                }

                console.log(information);
                paymentFactory.actionPostFunction(CONSTANT.URL_API_CASHIER, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.cleanData();
                                $scope.get();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (list) {
                $scope.idUpdate = list.id;
                $scope.objectUpdate = list;
                $scope.cashierstate = list.active;
                $scope.cashierinitHour = list.initHour;
                $scope.cashierfinalHour = list.finalHour;
                $scope.cashiername = list.name;
                $anchorScroll();
            }

            $scope.update = function () {
                $scope.showLoadingFunction();
                var information = {
                    active: ($scope.cashierselectstate == null) ? $scope.objectUpdate.active : $scope.cashierselectstate,
                    finalHour: $scope.cashierfinalHour,
                    initHour: $scope.cashierinitHour,
                    name: $scope.cashiername
                }
                paymentFactory.actionPutFunction(CONSTANT.URL_API_CASHIER, $scope.idUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.get();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }


            $scope.delete = function (list) {
                $scope.showLoadingFunction();
                paymentFactory.actionDeleteFunction(CONSTANT.URL_API_CASHIER, list.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.get();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idUpdate = 0;
                $scope.cashierstate = "";
                $scope.cashierinitHour = "";
                $scope.cashierfinalHour = "";
                $scope.cashiername = "";
            }

            $scope.changeActive = function (state) {
                if (state == 1) {
                    return "Activo";
                } else if (state == 0) {
                    return "Inactivo";
                } else {
                    return "Indefinido";
                }
            }


        }]);

})()
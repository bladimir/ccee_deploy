﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.RefereeReceipt
{
    public class ResponseRefereeReceiptJson
    {
        public int id { get; set; }
        public string NoReceipt { get; set; }
        public DateTime? dateCreate { get; set; }
        public string ToName { get; set; }
        public string url { get; set; }
        public int? reject { get; set; }
        public int? delete { get; set; }
    }
}
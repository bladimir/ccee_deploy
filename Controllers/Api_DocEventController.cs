﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.JsonModels.DocumentEvent;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_DocEventController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_DocEvent
        public List<ResponseDocEventJson> Get()
        {
            try
            {
                List<ResponseDocEventJson> docEventList = new List<ResponseDocEventJson>();
                var tempDoc = db.ccee_DocEvento;
                foreach (ccee_DocEvento conteDoc in tempDoc)
                {
                    docEventList.Add(new ResponseDocEventJson() {
                        id = conteDoc.DoE_NoDocEvento,
                        name = conteDoc.DoE_DocNombre
                    });
                }
                return docEventList;
            }
            catch
            {
                return null;
            }
            
        }

        // GET: api/Api_DocEvent/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_DocEvent
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_DocEvent/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_DocEvent/5
        public void Delete(int id)
        {
        }
    }
}

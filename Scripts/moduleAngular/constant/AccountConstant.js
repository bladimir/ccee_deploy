﻿(function () {
    angular.module('account')
        .constant('CONSTANTE', {
            REGEX_EMAIL: /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/,
            REGEX_PASS: /^[a-zA-Z0-9]{5,12}$/,
            REGEX_TELEFONO: /^[0-9]{8}$/,
            REGEX_NUMEROS: /\d+/,
            REGEX_FECHA: /^(0?[1-9]|[12][0-9]|3[01])[\/](0?[1-9]|1[012])[\/](19|20)\d{2}$/,
            REGEX_MAYUSCULA: /[A-Z]+/,
            REGEX_NIT: /^[0-9]{5,9}-([0-9]|[a-zA-Z])$/,
            CARACTERES_MINIMOS: 5,
            CARACTERES_MAXIMOS: 12,
            MENSAJE_NUMEROS_ERROR: 'al menos un digito',
            MENSAJE_MAYUSCULA_ERROR: 'al menos una mayuscula',
            MENSAJE_CARACTERES_ERROR: 'cinco caracteres min',
            MENSAJE_CARACTERES_MAX_ERROR: 'no superar los 12 caracteres',
            URL_GET_TOKEN: '/Users/GetToken',
            REGEX_VALIDAR: /^\d+/,
            STATUS_CODE_ERROR: 400,
            STATUS_CODE_USER_REPEAT: 410,
            STATUS_CODE_SUCCESS: 200,
            STATUS_CODE_USER_NOT_FOUND: 411,
            NOTIFY_HIDE_CSS: 'emergente1 notify-event-content',
            NOTIFY_SHOW_CSS: 'emergente2 notify-event-content'
        });

})();
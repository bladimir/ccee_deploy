﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.RecoverReceipt
{
    public class ResponseRecoverReceiptJson
    {
        public int pay { get; set; }
        public int referee { get; set; }
        public decimal amount { get; set; }
        public int cashier { get; set; }
        public DateTime? datepay { get; set; }
        public int document { get; set; }
    }
}
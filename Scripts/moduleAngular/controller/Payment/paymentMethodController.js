﻿(function () {
    angular.module('payment')
        .controller('paymentMethod', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'paymentFactory', function ($scope, $http, CONSTANT, $anchorScroll, paymentFactory) {
            $scope.listData;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.idUpdate = 0;
            $scope.objectUpdate;
            $scope.listIssuingBank;
            $scope.idIssuingBank;
            $scope.listTypeMethod = [];

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            //paymentFactory.actionGetFunction(CONSTANT.URL_API_ISSUINGBANK)
            //    .then(function (response) {
            //        $scope.hideLoadingFunction();
            //        console.log(response);
            //        $scope.listIssuingBank = response.data;
            //    }, function (error) {
            //        $scope.hideLoadingFunction();
            //        console.log(error);
            //    });


            $scope.getIssuingBank = function(){
                $scope.idIssuingBank = $scope.selectIssuingBank.id;
                $scope.get($scope.idIssuingBank);
            }

            


            $scope.setAuth = function () {
                paymentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        paymentFactory.setAuthorization(response.data);
                        $scope.get();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.get = function () {
                $scope.createListTypeMethod();
                $scope.showLoadingFunction();
                paymentFactory.actionGetFunction(CONSTANT.URL_API_PAYMENTMETHOD)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listData = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.add = function () {
                $scope.showLoadingFunction();
                var information = {
                    typeMethod: (($scope.selectTypeMethod != null) ? $scope.selectTypeMethod.id : -1),
                    description: $scope.description,
                    idIssuingBank: 40
                }

                console.log(information);
                paymentFactory.actionPostFunction(CONSTANT.URL_API_PAYMENTMETHOD, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.cleanData();
                                $scope.get($scope.idIssuingBank);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (list) {
                $scope.idUpdate = list.id;
                $scope.objectUpdate = list;
                $scope.description = list.description;
                $scope.typeMethod = list.typeMethod;
                $anchorScroll();
            }

            $scope.update = function () {
                $scope.showLoadingFunction();
                var information = {
                    typeMethod: (($scope.selectTypeMethod != null) ? $scope.selectTypeMethod.id : $scope.typeMethod),
                    description: $scope.description
                }
                paymentFactory.actionPutFunction(CONSTANT.URL_API_PAYMENTMETHOD, $scope.idUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.get($scope.idIssuingBank);
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }


            $scope.delete = function (list) {
                $scope.showLoadingFunction();
                paymentFactory.actionDeleteFunction(CONSTANT.URL_API_PAYMENTMETHOD, list.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.get($scope.idIssuingBank);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idUpdate = 0;
                $scope.description = "";
                $scope.typeMethod = "";
            }

            $scope.changeType = function (state) {
                if (state == 0) {
                    return "Efectivo";
                } else if (state == 1) {
                    return "Cheque";
                } else if (state == 2) {
                    return "Transferencia";
                } else if (state == 3) {
                    return "Tarjeta de credito";
                } else if (state == 4) {
                    return "Tarjeta de debito";
                } else if (state == 5) {
                    return "Lote institucional";
                } else {
                    return "Indefinido";
                }
            }

            $scope.createListTypeMethod = function () {
                var cash = { id: 0, text: "Efectivo" };
                var check = { id: 1, text: "Cheque" };
                var transfer = { id: 2, text: "Transferencia" };
                var creditCard = { id: 3, text: "Tarjeta de credito" };
                var debitCard = { id: 4, text: "Tarjeta de debito" };
                var lot = { id: 5, text: "Lote institucional" };
                $scope.listTypeMethod = [];
                $scope.listTypeMethod.push(cash);
                $scope.listTypeMethod.push(check);
                $scope.listTypeMethod.push(transfer);
                $scope.listTypeMethod.push(creditCard);
                $scope.listTypeMethod.push(debitCard);
                $scope.listTypeMethod.push(lot);
            }


        }]);

})()
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webcee.Class.JsonModels.Economic;

namespace webcee.Class.JsonModels.RejectedCheck
{
    public class ResponseRejectedCheckJson
    {
        public int MPP_NoMedioPP { get; set; }
        public Nullable<decimal> MPP_Monto { get; set; }
        public string MPP_NoTransaccion { get; set; }
        public string MPP_NoDocumento { get; set; }
        public Nullable<int> MPP_Reserva { get; set; }
        public Nullable<int> MPP_Rechazado { get; set; }
        public Nullable<System.DateTime> MPP_FechaHora { get; set; }
        public int fk_MedioPago { get; set; }
        public int? fk_Pago { get; set; }
        public int fk_Banco { get; set; }
        public string MPP_Observacion { get; set; }
        public int? payFrozen { get; set; }
        public List<ResponseEconomicRefereeJson> ecomonics { get; set; }
    }
}
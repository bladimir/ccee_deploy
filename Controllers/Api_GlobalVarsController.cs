﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.GlobalVars;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_GlobalVarsController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();
        // GET: api/Api_GlobalVars
        public List<ResponseGlobalVarsJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempGlobal = (from glo in db.ccee_Variables
                                 select new ResponseGlobalVarsJson()
                                 {
                                     id = glo.VAR_NoVariables,
                                     key = glo.Var_Clave,
                                     value = glo.Var_Valor
                                 }).ToList();

                Autorization.saveOperation(db, autorization, "Obteniendo Variables Globales");

                return tempGlobal;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_GlobalVars/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_GlobalVars
        public IHttpActionResult Post(RequestGlobalVarsJson global)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Variables tempGlobal = new ccee_Variables()
                {
                    Var_Clave = global.key,
                    Var_Valor = global.value
                };

                db.ccee_Variables.Add(tempGlobal);

                Autorization.saveOperation(db, autorization, "Agregando Variables Globales");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_GlobalVars/5
        public IHttpActionResult Put(int id, RequestGlobalVarsJson global)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Variables tempGlobal = db.ccee_Variables.Find(id);

                if (tempGlobal == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempGlobal.Var_Clave = global.key;
                tempGlobal.Var_Valor = global.value;

                db.Entry(tempGlobal).State = EntityState.Modified;

                Autorization.saveOperation(db, autorization, "Actualizando Variables Globales");

                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_GlobalVars/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Variables tempGlobal = db.ccee_Variables.Find(id);
                if (tempGlobal == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_Variables.Remove(tempGlobal);

                Autorization.saveOperation(db, autorization, "Eliminando Variables Globales");

                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.TempReporte;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_TempReporteController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_TempReporte
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_TempReporte/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_TempReporte
        public int Post(RequestTempReporteJson tempReport)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return 0;

                ccee_TempReporte tempTempReport = new ccee_TempReporte()
                {
                    TemRep_Listado = tempReport.datos
                };

                db.ccee_TempReporte.Add(tempTempReport);
                db.SaveChanges();
                int postTempReport = tempTempReport.TemRep_NoTempReporte;


                Autorization.saveOperation(db, autorization, "Agregando listado para reportes");
                db.SaveChanges();

                return postTempReport;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        // PUT: api/Api_TempReporte/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_TempReporte/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

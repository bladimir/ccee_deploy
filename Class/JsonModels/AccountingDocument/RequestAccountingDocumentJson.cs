﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webcee.Class.JsonModels.Economic;

namespace webcee.Class.JsonModels.AccountingDocument
{
    public class RequestAccountingDocumentJson
    {
        public bool isReferee { get; set; }
        public string remark { get; set; }
        public string date { get; set; }
        public string titleName { get; set; }
        public string nit { get; set; }
        public string address { get; set; }
        public int idCashier { get; set; }
        public int idReferee { get; set; }
        public int idNotReferee { get; set; }
        public int idPay { get; set; }
        public int idTypeDocumentAccounting { get; set; }
        public string NoDocumentCol { get; set; }
        public int NoDocumentOther { get; set; }
        public string listIdDocs { get; set; }
        public DateTime? dateTempCol { get; set; }
        public DateTime? dateTempTim { get; set; }
        public IEnumerable<ResponseEconomicJson> listEconomic { get; set; }
        public int idDocument { get; set; }
    }
}
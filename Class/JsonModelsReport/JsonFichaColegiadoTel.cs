﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonFichaColegiadoTel
    {
        public int Tel_NoTelefono { get; set; }
        public string Tel_NumTelefono { get; set; }
        public string telefono { get
            {
                return "(" + Tel_CodigoPais + ") " + Tel_NumTelefono;
            }
        }
        public int? Tel_TipoTelefono { get; set; }
        public string tipoTelefono { get
            {
                switch (Tel_TipoTelefono)
                {
                    case 1:
                        return "Teléfono casa";
                    case 2:
                        return "Teléfono oficina";
                    case 3:
                        return "Fax oficina";
                    case 4:
                        return "Celular";
                    case 5:
                        return "Otro";
                    default:
                        return "";
                }
            }
        }
        public int? Tel_CodigoPais { get; set; }
    }
}
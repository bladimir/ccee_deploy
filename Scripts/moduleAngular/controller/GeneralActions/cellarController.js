﻿(function () {
    angular.module('generals')
        .controller('cellar', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listCellar;
            $scope.listCashier;
            $scope.listCellarAssing;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idUpdate = 0;
            $scope.regexNumber = CONSTANT.REGEX_DIGIT;
            $scope.regexAmount = CONSTANT.REGEX_AMOUNT;
            $scope.regexPages = CONSTANT.REGEX_PAGES;
            $scope.regexSearchPAge = CONSTANT.REGEX_SEARCH_PAGE;
            $scope.numberPageText;
            $scope.showPage = 0;
            $scope.listTimbreSend = [];

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                //generalactionsFactory.actionGetFunction(CONSTANT.URL_API_CELLAR)
                //.then(function (response) {
                //    $scope.hideLoadingFunction();
                //    console.log(response);
                //    $scope.listCellar = response.data;
                //}, function (error) {
                //    $scope.hideLoadingFunction();
                //    console.log(error);
                //});

                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_CASHIER)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listCashier = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });

                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_VALUETIMBRE)
                    .then(function (response) {
                        console.log(response);
                        $scope.listT = response.data;
                    }, function (error) {
                        console.log(error);
                    });
            }



            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            
            $scope.getCellar = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_CELLAR, $scope.searchnumberPage)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listCellar = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getTimbreAssing = function () {
                $scope.showLoadingFunction();
                var amount = (($scope.selectValueFind == null) ? "0" : $scope.selectValueFind.denomination);
                var cashier = (($scope.selectCashierFind == null) ? "0" : $scope.selectCashierFind.id);
                
                var date = "";
                if ($scope.dateFind != null) {
                    date = ($scope.dateFind.getFullYear() + "-" + ($scope.dateFind.getMonth() + 1) + "-" + $scope.dateFind.getDate());
                    if ($scope.dateFFind != null) {
                        date += "_" + ($scope.dateFFind.getFullYear() + "-" + ($scope.dateFFind.getMonth() + 1) + "-" + $scope.dateFFind.getDate());
                    } else {
                        date = "0";
                    }
                } else {
                    date = "0";
                }

                page = (($scope.pageFind == null || $scope.pageFind == "") ? "0" : $scope.pageFind);

                generalactionsFactory.actionGetGetGetGetIntFunction(CONSTANT.URL_API_CELLARPAGE, cashier, amount, date, page)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listCellarAssing = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.addCellar = function (cashier) {
                $scope.showLoadingFunction();
                var information = {
                    numberPage: $scope.numberPage,
                    initRange: $scope.numberInit,
                    finishRange: $scope.numberFinal,
                    value: $scope.selectValue.denomination,
                    idValue: $scope.selectValue.id,
                    idCashier: cashier
                }
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_CELLAR, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getCellar();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (cou) {
                $scope.idUpdate = cou.id;
                $scope.numberPageText = cou.numberPage;
                $anchorScroll();
            }

            $scope.updateCellar = function () {
                $scope.showLoadingFunction();
                $scope.isAsign = false;
                var information = {
                    idCashier: $scope.selectCashierUpdate.id,
                    numberPage: $scope.pagesTimbreUpdate,
                    value: $scope.selectValueUpdate.denomination,
                    idValue: $scope.selectValueUpdate.id
                }
                console.log(information);
                generalactionsFactory.actionPutFunction(CONSTANT.URL_API_CELLAR, 0, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.isAsign = true;
                            $scope.cleanData();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.deleteCellar = function (cell, amount) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionDeleteFunctionSpecial(CONSTANT.URL_API_CELLAR, cell, amount.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.pagesTimbreDelete = "";
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.updateTimbre = function (type, timbre, object) {
                $scope.showLoadingFunction();
                var information = {
                    id: timbre
                }
                console.log(information);
                generalactionsFactory.actionPutFunction(CONSTANT.URL_API_CELLARPAGE, type, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            if (type == 1) {
                                object.nameCashier = "";
                                $scope.getCellar();
                            } else if (type == 2) {
                                object.statusTimbre = 3;
                            }
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
            }


            $scope.viewTimbre = function (listPage) {
                $scope.showPage = 2;
                $scope.listTimbre = listPage.timbres;
                $scope.numberPageTimbre = listPage.numberPage;
                $anchorScroll();
            }


            $scope.createListTimbre = function (cashier) {
                ///$scope.listTimbreSend
                var splitTimbre = $scope.numberPage.split("-");
                console.log(splitTimbre);
                if (splitTimbre.length == 2) {
                    var var1 = parseInt(splitTimbre[0]);
                    var var2 = parseInt(splitTimbre[1]);
                    for (var i = var1; i <= var2; i++) {
                        var information = {
                            numberPage: i,
                            initRange: $scope.numberInit,
                            finishRange: $scope.numberFinal,
                            value: $scope.selectValue.denomination,
                            idValue: $scope.selectValue.id,
                            idCashier: cashier,
                            status: "Pendiente"
                        }
                        $scope.listTimbreSend.push(information);
                    }

                } else if (splitTimbre.length == 1) {
                    var var1 = parseInt(splitTimbre[0]);
                    var information = {
                        numberPage: var1,
                        initRange: $scope.numberInit,
                        finishRange: $scope.numberFinal,
                        value: $scope.selectValue.denomination,
                        idValue: $scope.selectValue.id,
                        idCashier: cashier,
                        status: "Pendiente"
                    }
                    $scope.listTimbreSend.push(information);
                }

            }

            $scope.addListCellar = function (position) {
                console.log(position);
                if (position >= $scope.listTimbreSend.length) return;
                var information = $scope.listTimbreSend[position];
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_CELLAR, information)
                        .then(function (response) {
                            if (response.data == 200) {
                                $scope.listTimbreSend[position].status = "Completado";
                            } else {
                                $scope.listTimbreSend[position].status = "Problema al cargarlo";
                            }
                            position = position + 1;
                            $scope.addListCellar(position);
                        }, function (error) {
                            $scope.listTimbreSend[position].status = "Problema al cargarlo";
                            position = position + 1;
                            $scope.addListCellar(position);
                        });
            }

            $scope.cleanData = function () {
                $scope.idUpdate = 0;
                $scope.numberPageText = "";
            }

            $scope.clearFind = function () {
                $scope.selectValueFind = null;
                $scope.selectCashierFind = null;
                $scope.dateFind = null;
                $scope.dateFFind = null;
                $scope.pageFind = null;
            }

            $scope.clearList = function () {
                $scope.listTimbreSend = [];
            }

            $scope.startPage = function () {
                $scope.numberInit = 1;
                $scope.numberFinal = 40;
            }

            $scope.changeReport = function () {
                window.location.href = '/Reports/TimbreBodega';
            }

            $scope.statusTimbre = function (status) {
                switch (status) {
                    case 1:
                        return "Disponible";
                        break;
                    case 2:
                        return "No Disponible";
                        break;
                    case 3:
                        return "De baja";
                        break;
                    default:
                        return "Sin definir";
                        break;
                }
            }


        }]);

})()
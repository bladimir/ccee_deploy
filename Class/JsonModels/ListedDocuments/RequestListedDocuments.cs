﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.ListedDocuments
{
    public class RequestListedDocuments
    {
        public string range { get; set; }
        public int typeDocument { get; set; }
        public int idPay { get; set; }
        public int idCashier { get; set; }
    }
}
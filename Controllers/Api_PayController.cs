﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Net;
using System.Web.Http;
using webcee.Class.JsonModels._Tools;
using webcee.Class.JsonModels.Economic;
using webcee.Class.JsonModels.PaymentMethod_Pay;
using webcee.Models;
using System.Linq;
using webcee.Class.Class;
using webcee.Class.Constant;

namespace webcee.Controllers
{
    public class Api_PayController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Pay
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_Pay/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_Pay
        public int Post(PostBodyEconomicJson pay)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return 0;
                Autorization.saveOperation(db, autorization, "Pago de colegiado");
                var tempPost = this.generatePay(pay, true);
                return tempPost;
            }
            catch (Exception)
            {
                return 0;
            }

            //Ok(HttpStatusCode.BadRequest);
        }

        // PUT: api/Api_Pay/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_Pay/5
        public void Delete(int id)
        {
        }

        public int generatePay(PostBodyEconomicJson pay, bool temp)
        {



            foreach (RequestPaymentMethod_PayJson paymentMethod in pay.jsonContentMethod)
            {
                if (paymentMethod.idPaymentMethod >= 46 && paymentMethod.idPaymentMethod <= 48)
                {
                    var tempExist = (from med in db.ccee_MedioPago_Pago_C
                                     where med.MPP_NoTransaccion.Equals(paymentMethod.noTransaction)
                                     && med.fk_Banco == paymentMethod.idBank
                                     select med.MPP_NoMedioPP).ToList();
                    if (tempExist.Count() > 0)
                    {
                        return -2;
                    }
                }
            }


            if (pay.id == 1)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        ccee_CargoEconomico_timbre tempTimbreAprox = null;
                        ccee_TipoCargoEconomico typeMora = db.ccee_TipoCargoEconomico
                            .Where(o => o.TCE_CondicionCalculo.Equals("esMora")).FirstOrDefault();
                        ccee_Pago tempPay = new ccee_Pago()
                        {
                            Pag_Monto = pay.amount,
                            Pag_Observacion = pay.remake,
                            Pag_Fecha = DateTime.Today,
                            fk_Caja = pay.idCash,
                            fk_Cajero = pay.idCashier
                        };

                        db.ccee_Pago.Add(tempPay);
                        db.SaveChanges();

                        foreach (ResponseEconomicJson contEconomi in pay.jsonContent)
                        {

                            if (contEconomi.enable == 1)
                            {

                                if (contEconomi.typeEconomic == 1)
                                {
                                    foreach (ResponseEconomicRefereeJson conteEcoReferee in contEconomi.ListReferee)
                                    {
                                        ccee_CargoEconomico_colegiado tempEcoReferee = db.ccee_CargoEconomico_colegiado.Find(conteEcoReferee.id);
                                        tempEcoReferee.fk_Pago = tempPay.Pag_NoPago;
                                        db.Entry(tempEcoReferee).State = EntityState.Modified;
                                    }
                                }
                                else if (contEconomi.typeEconomic == 2)
                                {

                                    if (contEconomi.idTypeEconomic == typeMora.TCE_NoTipoCargoEconomico)
                                    {
                                        ccee_CargoEconomico_timbre tempEcoTimbre = new ccee_CargoEconomico_timbre();
                                        tempEcoTimbre.CEcT_Monto = contEconomi.amount;
                                        tempEcoTimbre.CEcT_FechaGeneracion = contEconomi.dateCreated;
                                        tempEcoTimbre.fk_Colegiado = contEconomi.idReferee;
                                        tempEcoTimbre.fk_TipoCargoEconomico = contEconomi.idTypeEconomic;
                                        tempEcoTimbre.fk_Pago = tempPay.Pag_NoPago;
                                        db.ccee_CargoEconomico_timbre.Add(tempEcoTimbre);
                                    }
                                    else
                                    {
                                        ccee_CargoEconomico_timbre tempEcoTimbre = db.ccee_CargoEconomico_timbre.Find(contEconomi.id);

                                        if (tempEcoTimbre.fk_TipoCargoEconomico == Constant.type_economic_base)
                                        {
                                            tempTimbreAprox = tempEcoTimbre;
                                        }
                                        tempEcoTimbre.fk_Pago = tempPay.Pag_NoPago;
                                        db.Entry(tempEcoTimbre).State = EntityState.Modified;
                                    }

                                }
                                else if (contEconomi.typeEconomic == 3)
                                {
                                    ccee_CargoEconomico_otros tempEcoOther = db.ccee_CargoEconomico_otros.Find(contEconomi.id);
                                    tempEcoOther.fk_Pago = tempPay.Pag_NoPago;
                                    db.Entry(tempEcoOther).State = EntityState.Modified;
                                }
                                else if (contEconomi.typeEconomic == 4)
                                {
                                    if (tempTimbreAprox == null)
                                    {
                                        ccee_CargoEconomico_otros tempEcoOther = new ccee_CargoEconomico_otros();
                                        tempEcoOther.CEcO_Monto = contEconomi.amount;
                                        tempEcoOther.CEcO_FechaGeneracion = DateTime.Now;
                                        tempEcoOther.fk_Colegiado = contEconomi.idReferee;
                                        tempEcoOther.fk_TipoCargoEconomico = contEconomi.idTypeEconomic;
                                        tempEcoOther.fk_Pago = tempPay.Pag_NoPago;
                                        tempEcoOther.fk_NoColegiado = contEconomi.idNoReferee;
                                        db.ccee_CargoEconomico_otros.Add(tempEcoOther);
                                    }
                                    else
                                    {
                                        tempTimbreAprox.CEcT_Monto = tempTimbreAprox.CEcT_Monto + contEconomi.amount;
                                        db.Entry(tempTimbreAprox).State = EntityState.Modified;
                                    }




                                }

                            }

                        }
                        ccee_MedioPago_Pago_C tempPaymentMethodPay;
                        if (pay.jsonContentMethod == null)
                        {
                            return -1;
                        }
                        else if (pay.jsonContentMethod.Count() <= 0)
                        {
                            return -1;
                        }

                        foreach (RequestPaymentMethod_PayJson paymentMethod in pay.jsonContentMethod)
                        {
                            tempPaymentMethodPay = new ccee_MedioPago_Pago_C()
                            {
                                MPP_Monto = paymentMethod.amount,
                                MPP_NoTransaccion = paymentMethod.noTransaction,
                                MPP_NoDocumento = paymentMethod.noDocument,
                                MPP_Observacion = paymentMethod.remarke,
                                MPP_Reserva = paymentMethod.reservation,
                                MPP_Rechazado = paymentMethod.rejection,
                                MPP_FechaHora = DateTime.Now,
                                fk_MedioPago = paymentMethod.idPaymentMethod,
                                fk_Pago = tempPay.Pag_NoPago,
                                fk_Banco = paymentMethod.idBank
                            };
                            db.ccee_MedioPago_Pago_C.Add(tempPaymentMethodPay);
                            tempPaymentMethodPay = null;
                        }
                        db.SaveChanges();
                        transaction.Commit();
                        //transaction.Rollback();
                        return tempPay.Pag_NoPago;
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        return 0;
                    }
                }
            }
            else if (pay.id == 2)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        ccee_TipoCargoEconomico typeMora = db.ccee_TipoCargoEconomico
                            .Where(o => o.TCE_CondicionCalculo.Equals("esMora")).FirstOrDefault();
                        ccee_Pago tempPay = new ccee_Pago()
                        {
                            Pag_Monto = pay.amount,
                            Pag_Observacion = pay.remake,
                            Pag_Fecha = DateTime.Today,
                            fk_Caja = pay.idCash,
                            fk_Cajero = pay.idCashier
                        };

                        db.ccee_Pago.Add(tempPay);
                        db.SaveChanges();
                        foreach (ResponseEconomicJson contEconomi in pay.jsonContent)
                        {
                            if (contEconomi.enable == 1)
                            {
                                if (contEconomi.typeEconomic == 3)
                                {
                                    ccee_CargoEconomico_otros tempEcoOther = db.ccee_CargoEconomico_otros.Find(contEconomi.id);
                                    tempEcoOther.fk_Pago = tempPay.Pag_NoPago;
                                    db.Entry(tempEcoOther).State = EntityState.Modified;
                                }
                                else if (contEconomi.typeEconomic == 4)
                                {
                                    ccee_CargoEconomico_otros tempEcoOther = new ccee_CargoEconomico_otros();
                                    tempEcoOther.CEcO_Monto = contEconomi.amount;
                                    tempEcoOther.CEcO_FechaGeneracion = DateTime.Now;
                                    tempEcoOther.fk_Colegiado = contEconomi.idReferee;
                                    tempEcoOther.fk_TipoCargoEconomico = contEconomi.idTypeEconomic;
                                    tempEcoOther.fk_Pago = tempPay.Pag_NoPago;
                                    tempEcoOther.fk_NoColegiado = contEconomi.idNoReferee;
                                    db.ccee_CargoEconomico_otros.Add(tempEcoOther);
                                }
                            }
                        }
                        db.SaveChanges();

                        foreach (RequestPaymentMethod_PayJson paymentMethod in pay.jsonContentMethod)
                        {
                            ccee_MedioPago_Pago_C tempPaymentMethodPay = new ccee_MedioPago_Pago_C()
                            {
                                MPP_Monto = paymentMethod.amount,
                                MPP_NoTransaccion = paymentMethod.noTransaction,
                                MPP_NoDocumento = paymentMethod.noDocument,
                                MPP_Observacion = paymentMethod.remarke,
                                MPP_Reserva = paymentMethod.reservation,
                                MPP_Rechazado = paymentMethod.rejection,
                                MPP_FechaHora = DateTime.Now,
                                fk_MedioPago = paymentMethod.idPaymentMethod,
                                fk_Pago = tempPay.Pag_NoPago,
                                fk_Banco = paymentMethod.idBank
                            };
                            db.ccee_MedioPago_Pago_C.Add(tempPaymentMethodPay);
                        }
                        db.SaveChanges();
                        transaction.Commit();
                        return tempPay.Pag_NoPago;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        return 0;
                    }
                }
            }
            return 0;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Constant;
using webcee.Class.JsonModelsReport;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_ListPayOtherController : ApiController
    {
        // GET: api/Api_ListPayOther
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_ListPayOther/5
        public List<JsonEstadoCuentaColegiado> Get(int id)
        {
            string query = remplazeQuery("ccee_CargoEconomico_otros", "O", id);
            string query2 = remplazeQueryWihtoutPay("ccee_CargoEconomico_otros", "O", id);
            using (var context = new CCEEEntities())
            {
                List<JsonTempEstadoCuentaC> pLis = context.Database.SqlQuery<JsonTempEstadoCuentaC>(query).ToList();
                var tempLis = context.Database.SqlQuery<JsonTempEstadoCuentaC>(query2).ToList();
                pLis.AddRange(tempLis);
                List<JsonEstadoCuentaColegiado> listOtros = new List<JsonEstadoCuentaColegiado>();
                foreach (JsonTempEstadoCuentaC item in pLis)
                {
                    listOtros.Add(new JsonEstadoCuentaColegiado()
                    {
                        anio = item.fecha.Year,
                        Month = item.fecha.Month,
                        tempfechaPago = item.fechaP,
                        monto = item.monto,
                        tipoPago = item.descripcion,
                        recibo = item.recibos,
                        idRecibo = item.idRecibo,
                        url = Constant.url_dir_print_doc + item.idRecibo,
                        count = 1,
                        multiplicacion = 1 * item.monto,
                        rechazado = item.rechazado,
                        congelado = item.congelado
                    });
                }

                

                return listOtros.OrderBy(a => a.anio)
                    .ThenBy(b => b.Month).ToList();
            }
        }
    

        // POST: api/Api_ListPayOther
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_ListPayOther/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_ListPayOther/5
        public void Delete(int id)
        {
        }

        private string remplazeQuery(string cargoEconomico, string varCargo, int idColegiado)
        {
            string query = @"select distinct colegiado.cargo, colegiado.pago, colegiado.col, 
                            ccee_TipoCargoEconomico.TCE_Descripcion as descripcion, colegiado.monto, ccee_Pago.Pag_Fecha as fechaP, colegiado.fecha 
                            , ccee_DocContable.DCo_NoRecibo as recibos,  ccee_DocContable.DCo_NoDocContable as idRecibo 
                            , ccee_MedioPago_Pago_C.MPP_Rechazado as rechazado, ccee_Pago.Pag_Congelado as congelado 
                            from ccee_TipoCargoEconomico, ccee_Pago, ccee_DocContable, ccee_MedioPago_Pago_C,  ( 
                            select [cargo].CEc[cargoSel]_NoCargoEconomico as cargo, [cargo].fk_Pago as pago 
                                    , [cargo].fk_Colegiado as col, [cargo].fk_TipoCargoEconomico as tipo 
                                    , [cargo].CEc[cargoSel]_FechaGeneracion as fecha, [cargo].CEc[cargoSel]_Monto as monto 
                                    , [cargo].fk_DocContable as contable 
                            from [cargo] 
                            where [cargo].fk_Colegiado = [colegiado]
                            ) as colegiado 
                            WHERE colegiado.tipo = ccee_TipoCargoEconomico.TCE_NoTipoCargoEconomico 
                            and colegiado.pago = ccee_Pago.Pag_NoPago 
                            and colegiado.contable = ccee_DocContable.DCo_NoDocContable  
                            and ccee_Pago.Pag_NoPago = ccee_MedioPago_Pago_C.fk_Pago ";

            query = query.Replace("[cargo]", cargoEconomico);
            query = query.Replace("[cargoSel]", varCargo);
            query = query.Replace("[colegiado]", idColegiado.ToString());

            return query;
        }

        private string remplazeQueryWihtoutPay(string cargoEconomico, string varCargo, int idColegiado)
        {
            string query = "select colegiado.cargo, colegiado.col, " +
                            "ccee_TipoCargoEconomico.TCE_Descripcion as descripcion, colegiado.monto, colegiado.fecha " +
                            " " +
                            "from ccee_TipoCargoEconomico,  ( " +
                            "select [cargo].CEc[cargoSel]_NoCargoEconomico as cargo, [cargo].fk_Pago as pago " +
                                    ", [cargo].fk_Colegiado as col, [cargo].fk_TipoCargoEconomico as tipo " +
                                    ", [cargo].CEc[cargoSel]_FechaGeneracion as fecha, [cargo].CEc[cargoSel]_Monto as monto " +
                                    ", [cargo].fk_DocContable as contable " +
                            "from [cargo] " +
                            "where [cargo].fk_Colegiado = [colegiado] " +
                            "AND [cargo].fk_Pago is null " +
                            ") as colegiado " +
                            "WHERE colegiado.tipo = ccee_TipoCargoEconomico.TCE_NoTipoCargoEconomico ";

            query = query.Replace("[cargo]", cargoEconomico);
            query = query.Replace("[cargoSel]", varCargo);
            query = query.Replace("[colegiado]", idColegiado.ToString());

            return query;
        }


        private List<JsonEstadoCuentaColegiado> orderMonth(List<JsonTempEstadoCuentaC> listValue, string title)
        {
            var model = listValue
                    .GroupBy(o => new
                    {
                        Month = o.fecha.Month,
                        Year = o.fecha.Year,
                        Receipt = o.recibos
                    })
                    .Select(g => new JsonEstadoCuentaColegiado
                    {
                        Month = g.Key.Month,
                        anio = g.Key.Year,
                        monto = g.Sum(s => s.monto),
                        tempfechaPago = g.Select(p => p.fechaP).FirstOrDefault(),
                        tipoPago = title,
                        recibo = g.Key.Receipt,
                        idRecibo = g.Select(p => p.idRecibo).FirstOrDefault(),
                        count = g.Count(),
                        url = Constant.url_dir_print_doc + g.Select(p => p.idRecibo).FirstOrDefault()
                    })
                    .OrderBy(a => a.anio)
                    .ThenBy(b => b.Month)
                    .ToList();
            return model;
        }


    }
}

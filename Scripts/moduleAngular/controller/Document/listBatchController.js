﻿(function () {
    angular.module('document')
        .controller('listBatch', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'documentFactory', 'Upload', function ($scope, $http, CONSTANT, $anchorScroll, documentFactory, Upload) {

            $scope.listData = [];
            $scope.listNomina = [];
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.visibleCount = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }


            $scope.init = function () {
                $scope.getCashier();
            }

            $scope.setAuth = function () {
                documentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        documentFactory.setAuthorization(response.data);
                        $scope.init();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.getCashier = function () {
                documentFactory.actionGetFunction(CONSTANT.URL_API_CASHIER)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listCashier = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getBatch = function () {
                documentFactory.actionGetIntFunction(CONSTANT.URL_API_BATCH, $scope.selectCashier.id)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listBatch = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

        }]);

})()
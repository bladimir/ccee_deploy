﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.Balance;
using webcee.Class.JsonMolelsWS;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_BalanceController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Balance
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_Balance/5
        public ResponseBalanceJson Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                var response = proccessBalance(id);
                Autorization.saveOperation(db, autorization, "Obteniendo balance colegiado");
                return response;
            }
            catch (Exception)
            {
                return null;
            }
            
        }

        // POST: api/Api_Balance
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_Balance/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_Balance/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private IEnumerable<ModelAnioMesJson> obtenerCantidadCargosTimbre(List<ccee_CargoEconomico_timbre> listCuotaColegiadoTotal)
        {
            //DateTime dt = DateTime.Now.AddMonths(-1);
            //TimeSpan ttt = dt.TimeOfDay;
            //var tt = listCuotaColegiadoTotal.Where(o=>o.CEcC_FechaGeneracion.Value.TimeOfDay <= ttt);

            var model = listCuotaColegiadoTotal
                .GroupBy(o => new
                {
                    Month = o.CEcT_FechaGeneracion.Value.Month,
                    Year = o.CEcT_FechaGeneracion.Value.Year
                })
                .Select(g => new ModelAnioMesJson
                {
                    Month = g.Key.Month,
                    Year = g.Key.Year,
                    Total = g.Count()
                })
                .OrderByDescending(a => a.Year)
                .ThenByDescending(a => a.Month)
                .ToList();

            IEnumerable<ModelAnioMesJson> tempAnioMes = model;


            return model;

        }

        public ResponseBalanceJson proccessBalance(int id)
        {
            try
            {
                

                int minTime = 3;
                string IaValue = GeneralFunction.getKeyTableVariable(db, Constant.table_var_ia);
                string cent = GeneralFunction.getKeyTableVariable(db, Constant.table_var_aprox_decimal);
                ResponseBalanceJson resBalance = new ResponseBalanceJson();

                if (IaValue == null) return null;
                double Ia = Convert.ToDouble(IaValue);
                decimal dCent = Convert.ToDecimal(cent);

                List<ccee_CargoEconomico_colegiado> listBalanceC = (db.ccee_CargoEconomico_colegiado
                                                                    .Where(p => p.fk_Colegiado == id))
                                                                    .ToList();

                resBalance.lastReferee = listBalanceC.Where(p => p.fk_Pago != null)
                                          .OrderByDescending(o => o.CEcC_FechaGeneracion)
                                          .Select(q => q.CEcC_FechaGeneracion)
                                          .FirstOrDefault();

                var temC = (listBalanceC
                                .Where(o => o.fk_Pago != null)
                                .OrderByDescending(p => p.CEcC_FechaGeneracion))
                                .FirstOrDefault();

                resBalance.dateReferee = (temC == null) ? null : temC.CEcC_FechaGeneracion;


                listBalanceC = (listBalanceC
                                .Where(o => o.fk_Pago == null))
                                .ToList();

                var model = listBalanceC
                .GroupBy(o => new
                {
                    Month = o.CEcC_FechaGeneracion.Value.Month,
                    Year = o.CEcC_FechaGeneracion.Value.Year,
                    Day = o.CEcC_FechaGeneracion.Value.Day
                })
                .Select(g => new ModelAnioMesJson
                {
                    Month = g.Key.Month,
                    Year = g.Key.Year,
                    Total = g.Count()
                })
                .OrderByDescending(a => a.Year)
                .ThenByDescending(a => a.Month)
                .ToList();

                resBalance.countReferee = model.Count();

                resBalance.referee = listBalanceC.Sum(o => o.CEcC_Monto).Value;


                List<ccee_CargoEconomico_timbre> listBalanceT = (db.ccee_CargoEconomico_timbre
                                                                    .Where(p => p.fk_Colegiado == id))
                                                                    .ToList();

                var temT = (listBalanceT
                                .Where(o => o.fk_Pago != null && o.fk_TipoCargoEconomico == Constant.type_economic_base)
                                .OrderByDescending(p => p.CEcT_FechaGeneracion))
                                .FirstOrDefault();

                resBalance.dateTimbre = (temT == null) ? null : temT.CEcT_FechaGeneracion;

                temT = (listBalanceT
                                .Where(o => o.fk_Pago != null && o.fk_TipoCargoEconomico == Constant.type_economic_postumo)
                                .OrderByDescending(p => p.CEcT_FechaGeneracion))
                                .FirstOrDefault();

                resBalance.datePostumo = (temT == null) ? null : temT.CEcT_FechaGeneracion;

                resBalance.lastTimbre = listBalanceT.Where(p => p.fk_Pago != null)
                                          .OrderByDescending(o => o.CEcT_FechaGeneracion)
                                          .Select(q => q.CEcT_FechaGeneracion)
                                          .FirstOrDefault();

                listBalanceT = (listBalanceT
                                .Where(o => o.fk_Pago == null)
                                .OrderBy(m => m.CEcT_FechaGeneracion)).ToList();

                model = listBalanceT
                .GroupBy(o => new
                {
                    Month = o.CEcT_FechaGeneracion.Value.Month,
                    Year = o.CEcT_FechaGeneracion.Value.Year,
                    Day = o.CEcT_FechaGeneracion.Value.Day
                })
                .Select(g => new ModelAnioMesJson
                {
                    Month = g.Key.Month,
                    Year = g.Key.Year,
                    Day = g.Key.Day,
                    Total = g.Count()
                })
                .OrderBy(a => a.Year)
                .ThenBy(a => a.Month)
                .ToList();


                int initList = 0;
                for (int i = 0; i < model.Count; i++)
                {

                    ModelAnioMesJson tempAnioMes = model[i];
                    decimal sumPostumoTimbre = 0.0m;

                    initList = ((i == 0) ? 0 : initList + model[i - 1].Total);
                    for (int j = 0; j < tempAnioMes.Total; j++)
                    {
                        ccee_CargoEconomico_timbre unitEcoRef = listBalanceT[initList + j];
                        sumPostumoTimbre += unitEcoRef.CEcT_Monto.Value;
                    }

                    if (model.Count > minTime)
                    {
                        GeneralFunction generalFun = new GeneralFunction();
                        DateTime date = Convert.ToDateTime(tempAnioMes.Month + "/" + 1 + "/" + tempAnioMes.Year);
                        resBalance.mora += generalFun.amountMora(sumPostumoTimbre, generalFun.monthDifference(DateTime.Now, date), Ia);
                    }

                }

                resBalance.mora = GeneralFunction.aproxDecimal(resBalance.mora, dCent);

                model = listBalanceT
                .Where(p => p.fk_TipoCargoEconomico == Constant.type_economic_base)
                .GroupBy(o => new
                {
                    Month = o.CEcT_FechaGeneracion.Value.Month,
                    Year = o.CEcT_FechaGeneracion.Value.Year,
                    Day = o.CEcT_FechaGeneracion.Value.Day
                })
                .Select(g => new ModelAnioMesJson
                {
                    Month = g.Key.Month,
                    Year = g.Key.Year,
                    Day = g.Key.Day,
                    Total = g.Count()
                })
                .OrderByDescending(a => a.Year)
                .ThenByDescending(a => a.Month)
                .ThenByDescending(a => a.Day)
                .ToList();

                resBalance.countTimbre = model.Count();
                //resBalance.dateTimbre = model.FirstOrDefault().Day + " / " + model.FirstOrDefault().Month + " / " + model.FirstOrDefault().Year;
                resBalance.timbre = listBalanceT.Where(p => p.fk_TipoCargoEconomico == Constant.type_economic_base)
                                                .Sum(o => o.CEcT_Monto).Value;

                model = listBalanceT
                .Where(p => p.fk_TipoCargoEconomico == Constant.type_economic_postumo)
                .GroupBy(o => new
                {
                    Month = o.CEcT_FechaGeneracion.Value.Month,
                    Year = o.CEcT_FechaGeneracion.Value.Year
                })
                .Select(g => new ModelAnioMesJson
                {
                    Month = g.Key.Month,
                    Year = g.Key.Year,
                    Total = g.Count()
                })
                .OrderByDescending(a => a.Year)
                .ThenByDescending(a => a.Month)
                .ToList();

                resBalance.countPostumo = model.Count();
                //resBalance.datePostumo = model.FirstOrDefault().Day + " / " + model.FirstOrDefault().Month + " / " + model.FirstOrDefault().Year;
                resBalance.postumo = listBalanceT.Where(p => p.fk_TipoCargoEconomico == Constant.type_economic_postumo)
                                                    .Sum(o => o.CEcT_Monto).Value;


                List<ccee_CargoEconomico_otros> listBalanceO = (db.ccee_CargoEconomico_otros.Where(p => p.fk_Colegiado == id)
                                                        .Where(o => o.fk_Pago == null)).ToList();


                model = listBalanceO
                .GroupBy(o => new
                {
                    Month = o.CEcO_FechaGeneracion.Value.Month,
                    Year = o.CEcO_FechaGeneracion.Value.Year
                })
                .Select(g => new ModelAnioMesJson
                {
                    Month = g.Key.Month,
                    Year = g.Key.Year,
                    Total = g.Count()
                })
                .OrderByDescending(a => a.Year)
                .ThenByDescending(a => a.Month)
                .ToList();


                resBalance.countOther = model.Count();
                resBalance.other = listBalanceO.Sum(o => o.CEcO_Monto).Value;
                return resBalance;
            }
            catch (Exception)
            {
                return null;
            }
        }


    }
}

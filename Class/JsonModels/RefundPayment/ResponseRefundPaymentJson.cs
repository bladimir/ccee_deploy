﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.RefundPayment
{
    public class ResponseRefundPaymentJson
    {
        public int id { get; set; }
        public DateTime? datePay { get; set; }
        public decimal? total { get; set; }
        public string document { get; set; }
        public int? idPay { get; set; }
        public decimal? diference { get; set; }
        public List<ResponseRefundPaymentListJson> list { get; set; }
    }
}
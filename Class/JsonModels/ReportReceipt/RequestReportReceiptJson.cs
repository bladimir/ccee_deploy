﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.ReportReceipt
{
    public class RequestReportReceiptJson
    {
        public string NoReceipt { get; set; }
        public int idNotReferee { get; set; }
    }
}
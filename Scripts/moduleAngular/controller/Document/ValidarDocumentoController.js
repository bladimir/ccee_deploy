﻿(function () {
    angular.module('document')
        .controller('validarDocumento', ['$scope', '$http', '$anchorScroll', 'documentFactory', function ($scope, $http, $anchorScroll, documentFactory) {

            $scope.listData;
            $scope.showDelete = false;
            $scope.showNotify = 'emergente1 notify-event-content';
            $scope.idUpdate = 0;
            $scope.objectUpdate;
            $scope.dateCon = null;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = 'emergente2 notify-event-content';
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = 'emergente1 notify-event-content';
            }

            $scope.init = function (idDocument) {
                $scope.numberDoc = idDocument;
            }
            
            $scope.searchData = function () {
                $scope.number = "";
                $scope.dateEmit = "";
                $scope.dateFinish = "";
                $scope.name = "";
                $scope.institution = "";
                $scope.idUpdate = 0;
                $scope.showLoadingFunction();
                documentFactory.actionGetIntFunction("/api/Api_ValidarDocumento", $scope.numberDoc)
                    .then(function (response) {
                        console.log(response);
                        if (response.data != null) {
                            $scope.number = response.data.number;
                            $scope.dateEmit = response.data.dateEmit;
                            $scope.dateFinish = response.data.dateFinish;
                            $scope.name = response.data.name;
                            $scope.institution = response.data.institution;
                            $scope.idUpdate = response.data.number;
                            $scope.dateCon = response.data.dateConsumption;
                        }
                        $scope.hideLoadingFunction();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.update = function () {
                $scope.showLoadingFunction();
                var information = {
                    idDocument: $scope.idUpdate,
                    institution: $scope.intitution,
                    rcaptch: $scope.rcaptcha
                }
                documentFactory.actionPutFunction("/api/Api_ValidarDocumento", $scope.idUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        $scope.textSuccess = response.data;
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.invalidate = function () {
                $scope.showLoadingFunction();
                documentFactory.actionDeleteFunction("/api/Api_ValidarDocumento", $scope.idUpdate)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        $scope.textSuccess = response.data;
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.cleanData = function () {
                $scope.numberDoc = "";
                $scope.intitution = "";
            }

        }]);
})()
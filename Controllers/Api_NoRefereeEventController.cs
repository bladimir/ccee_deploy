﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Constant;
using webcee.Class.JsonModels.NoReferee;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_NoRefereeEventController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        public IEnumerable<ResponseNoRefereeAssingEventJson> Get()
        {
            try
            {
                List<ResponseNoRefereeAssingEventJson> listNoReferee = new List<ResponseNoRefereeAssingEventJson>();
                var temNoReferee = db.ccee_NoColegiado.ToList();
                foreach (ccee_NoColegiado conteNoReferee in temNoReferee)
                {
                    listNoReferee.Add(new ResponseNoRefereeAssingEventJson()
                    {

                        id = conteNoReferee.NoC_NoNoColegiado,
                        name = conteNoReferee.NoC_Nombre,
                        lastname = conteNoReferee.NoC_Apellido,
                        nit = conteNoReferee.NoC_Nit,
                        movil = conteNoReferee.NoC_NumTelefono,
                        address = conteNoReferee.NoC_Direccion,
                        remark = conteNoReferee.NoC_Observacion

                    });
                }

                return listNoReferee;
            }
            catch(Exception)
            {
                return null;
            }
        }

        public IEnumerable<ResponseNoRefereeAssingEventJson> Get(int id)
        {
            try
            {
                List<ResponseNoRefereeAssingEventJson> listNoReferee = new List<ResponseNoRefereeAssingEventJson>();
                var temNoReferee = from col in db.ccee_NoColegiado
                                   join subcol in db.ccee_SubEventoNoColegiado on col.NoC_NoNoColegiado equals subcol.pkfk_NoColegiado
                                   where subcol.pkfk_SubEvento == id && subcol.SEN_Estatus == Constant.state_sub_event_assing
                                   select col;
                foreach (ccee_NoColegiado conteNoReferee in temNoReferee)
                {
                    listNoReferee.Add(new ResponseNoRefereeAssingEventJson()
                    {

                        id = conteNoReferee.NoC_NoNoColegiado,
                        name = conteNoReferee.NoC_Nombre,
                        lastname = conteNoReferee.NoC_Apellido

                    });
                }

                return listNoReferee;
            }
            catch
            {
                return null;
            }
        }


        // POST: api/Api_NoRefereeEvent
        public IHttpActionResult Post(RequestNoRefereeEventJson noReferee)
        {

            if (noReferee.idType == 1)
            {



                ccee_NoColegiado tempNoReferee = new ccee_NoColegiado()
                {
                    NoC_Nombre = noReferee.name,
                    NoC_Apellido = noReferee.lastname,
                    NoC_Nit = noReferee.nit,
                    NoC_NumTelefono = noReferee.movil,
                    NoC_Direccion = noReferee.address,
                    NoC_Observacion = noReferee.remark
                };

                try
                {

                    db.ccee_NoColegiado.Add(tempNoReferee);
                    db.SaveChanges();

                    ccee_SubEventoNoColegiado temSubEvent = new ccee_SubEventoNoColegiado()
                    {
                        pkfk_SubEvento = noReferee.idSubEvent,
                        pkfk_NoColegiado = tempNoReferee.NoC_NoNoColegiado,
                        SEN_Estatus = 2
                    };

                    db.ccee_SubEventoNoColegiado.Add(temSubEvent);
                    db.SaveChanges();
                    return Ok(200);

                }
                catch
                {
                    return Ok(400);
                }
            }else if (noReferee.idType == 2)
            {

                try
                {

                    ccee_SubEventoNoColegiado temSubEvent = new ccee_SubEventoNoColegiado()
                    {
                        pkfk_SubEvento = noReferee.idSubEvent,
                        pkfk_NoColegiado = noReferee.idNoReferee,
                        SEN_Estatus = 2
                    };

                    db.ccee_SubEventoNoColegiado.Add(temSubEvent);
                    db.SaveChanges();

                    return Ok(200);
                }
                catch
                {
                    return Ok(400);
                }

            }


            return Ok(400);
        }


        public IHttpActionResult Put(int id, RequestNoRefereeEventJson noReferee)
        {
            try
            {

                ccee_NoColegiado tempNoReferee = db.ccee_NoColegiado.Find(id);
                tempNoReferee.NoC_Nombre = noReferee.name;
                tempNoReferee.NoC_Apellido = noReferee.lastname;
                tempNoReferee.NoC_Nit = noReferee.nit;
                tempNoReferee.NoC_NumTelefono = noReferee.movil;
                tempNoReferee.NoC_Direccion = noReferee.address;
                tempNoReferee.NoC_Observacion = noReferee.remark;
                
                db.Entry(tempNoReferee).State = EntityState.Modified;
                db.SaveChanges();
                return Ok(Constant.status_code_success);

            }
            catch
            {
                return Ok(Constant.status_code_error);
            }
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

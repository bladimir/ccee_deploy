﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.Departament;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_DepartamentController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Departament
        public IEnumerable<ResponseDepartamentJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempDepartament = from dep in db.ccee_Departamento
                                      orderby dep.Dep_NombreDep ascending
                               select new ResponseDepartamentJson()
                               {
                                   id = dep.Dep_NoDepartamento,
                                   name = dep.Dep_NombreDep
                               };
                Autorization.saveOperation(db, autorization, "Obteniendo departamentos");
                return tempDepartament;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_Departament/5
        public IEnumerable<ResponseDepartamentJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempDepartament = from dep in db.ccee_Departamento
                                      where dep.fk_Pais == id
                                      orderby dep.Dep_NombreDep ascending
                                      select new ResponseDepartamentJson()
                                      {
                                          id = dep.Dep_NoDepartamento,
                                          name = dep.Dep_NombreDep
                                      };
                Autorization.saveOperation(db, autorization, "Obteniendo departamentos por pais");
                return tempDepartament;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_Departament
        public IHttpActionResult Post(RequestDepartamentJson departament)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Departamento tempDepartament = new ccee_Departamento()
                {
                    Dep_NombreDep = departament.name,
                    fk_Pais = departament.idCountry
                };

                db.ccee_Departamento.Add(tempDepartament);
                Autorization.saveOperation(db, autorization, "Agregando departamentos");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        // PUT: api/Api_Departament/5
        public IHttpActionResult Put(int id, RequestDepartamentJson departament)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Departamento tempDepartament = db.ccee_Departamento.Find(id);

                if (tempDepartament == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempDepartament.Dep_NombreDep = departament.name;

                db.Entry(tempDepartament).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando departamentos");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_Departament/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Departamento tempDepartament = db.ccee_Departamento.Find(id);
                if (tempDepartament == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_Departamento.Remove(tempDepartament);
                Autorization.saveOperation(db, autorization, "Eliminando departamentos");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}

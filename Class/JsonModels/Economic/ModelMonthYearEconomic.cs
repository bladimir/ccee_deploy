﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Economic
{
    public class ModelMonthYearEconomic
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public string MonthName
        {
            get
            {
                CultureInfo culture;
                culture = CultureInfo.CreateSpecificCulture("es-GT");
                return culture.DateTimeFormat.GetMonthName(this.Month);
            }
        }
        public int Total { get; set; }
    }
}
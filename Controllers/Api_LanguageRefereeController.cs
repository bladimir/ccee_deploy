﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.LanguageReferee;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_LanguageRefereeController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_LanguageReferee
        public IEnumerable<ResponseLanguageRefereeJson> Get()
        {
            return null;
        }

        // GET: api/Api_LanguageReferee/5
        public IEnumerable<ResponseLanguageRefereeJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempLangReferee = from lan in db.ccee_Idioma
                                      join laref in db.ccee_ColegiadoIdioma on lan.Idi_NoIdioma equals laref.pkfk_Idioma
                                      where laref.pkfk_Colegiado == id
                                      select new ResponseLanguageRefereeJson()
                                      {
                                          nameLanguage = lan.Idi_Descripcion,
                                          idLanguage = lan.Idi_NoIdioma,
                                          idReferee = laref.pkfk_Colegiado
                                      };
                Autorization.saveOperation(db, autorization, "Obteniendo lenguaje por colegiado");
                return tempLangReferee;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_LanguageReferee
        public IHttpActionResult Post(RequestLanguageRefereeJson languageReferee)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_ColegiadoIdioma tempLanguageRefe = new ccee_ColegiadoIdioma()
                {
                    pkfk_Colegiado = languageReferee.idReferee,
                    pkfk_Idioma = languageReferee.idLanguage
                };


                db.ccee_ColegiadoIdioma.Add(tempLanguageRefe);
                Autorization.saveOperation(db, autorization, "Agregando lenguaje por colegiado");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        // PUT: api/Api_LanguageReferee/5
        public void Put(int id, [FromBody]string value)
        {
        }

        [Route("api/Api_LanguageReferee/{id}/{lan}/")]
        // DELETE: api/Api_LanguageReferee/5
        public IHttpActionResult Delete(int id, int lan)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_ColegiadoIdioma tempLanguageRefe = db.ccee_ColegiadoIdioma.Find(id, lan);

                if (tempLanguageRefe == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_ColegiadoIdioma.Remove(tempLanguageRefe);
                Autorization.saveOperation(db, autorization, "Eliminando lenguaje por colegiado");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿(function () {
    angular.module('reports')
        .controller('estadoCuentaRecibo', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'reportFactory', function ($scope, $http, CONSTANT, $anchorScroll, reportFactory) {
            $scope.direction;
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquaters;
            $scope.regexReferee = CONSTANT.REGEX_REFEREE;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }


            $scope.generateList = function () {
                var textGetReport = "";
                console.log(($scope.selectHead != null) ? $scope.selectHead.id : "");
                textGetReport = "&selc=" + $scope.selecC +
                                "&selt=" + $scope.selecT +
                                "&refe=" + $scope.inRefe +
                                "&selo=" + $scope.selecO;
                console.log(textGetReport);
                $scope.direction = "../../Reports/aspxReports/EstadoCuentaConReciboColegiadoAspx.aspx?" + textGetReport;
            }

        }]);

})()
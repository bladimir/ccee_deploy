﻿(function () {
    angular.module('referee')
        .controller('searchReferee', ['$scope', '$http', '$location', 'CONSTANT', '$anchorScroll', 'refereeFactory', function ($scope, $http, $location, CONSTANT, $anchorScroll, refereeFactory) {
            
            $scope.numbers = CONSTANT.REGEX_DIGIT;
            $scope.regexNit = CONSTANT.REGEX_NIT;
            $scope.regexPhone = CONSTANT.REGEX_PHONE;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.info = "";
            $scope.listData;
            $scope.idUpdate = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.setAuth = function () {
                refereeFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    refereeFactory.setAuthorization(response.data);
                    console.log(refereeFactory.getAuthorization());
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.init = function () {
                
            }

            $scope.getCol = function () {
                var first = (($scope.firstName == "" || $scope.firstName == null) ? "0" : $scope.firstName);
                var secound = (($scope.secoundName == "" || $scope.secoundName == null) ? "0" : $scope.secoundName);
                var last = (($scope.lastName == "" || $scope.lastName == null) ? "0" : $scope.lastName);
                var lastSecound = (($scope.lastSecoundName == "" || $scope.lastSecoundName == null) ? "0" : $scope.lastSecoundName);

                var third = (($scope.thirdName == "" || $scope.thirdName == null) ? "0" : $scope.thirdName);
                var maried = (($scope.lastMariedName == "" || $scope.lastMariedName == null) ? "0" : $scope.lastMariedName);

                $scope.showLoadingFunction();
                refereeFactory.actionGetGetGetGetGetGetIntFunction(CONSTANT.URL_API_REFEREE, first, secound, third, last, lastSecound, maried)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listData = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            

            $scope.changeReferee = function (idReferee) {
                window.location.href = '/Referre/Index/#?id=' + idReferee;
            }


        }]);

})()
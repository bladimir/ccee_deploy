﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.JsonModels._Tools;
using webcee.Class.JsonModels.Cellar;
using webcee.Class.JsonModels.Timbre;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_CellarController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Cellar
        public IEnumerable<ResponseCellarJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempCellar = (from cel in db.ccee_HojaTimbre
                                 join tim in db.ccee_Timbre on cel.HoT_NoHojaTimbre equals tim.fk_HojaTimbre
                                 join val in db.ccee_ValorTimbre on tim.fk_ValorTimbre equals val.VaT_NoValorTimbre
                                 where cel.fk_Cajero == null
                                 select new ResponseCellarJson()
                                 {
                                     id = cel.HoT_NoHojaTimbre,
                                     numberPage = cel.HoT_NumeroHoja,
                                     amount = val.VaT_Denominacion,
                                     textAmount = val.VaT_Nombre
                                 }).ToList();

                var select = from r in tempCellar
                             group r by new
                             {
                                 Pages = r.numberPage,
                                 Id = r.id,

                             } into g
                             select new ResponseCellarJson()
                             {
                                 numberPage = g.Key.Pages,
                                 id = g.Key.Id,
                                 amount = g.Select(p=>p.amount).FirstOrDefault(),
                                 textAmount = g.Select(o=>o.textAmount).FirstOrDefault()
                             };
                Autorization.saveOperation(db, autorization, "Obteniendo hoja timbre");

                return select;
            }
            catch (Exception)
            {
                return null;
            }
        }


        // GET: api/Api_Cellar/5
        public IEnumerable<ResponseCellarJson> Get(string id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempCellar = (from cel in db.ccee_HojaTimbre
                                  join tim in db.ccee_Timbre on cel.HoT_NoHojaTimbre equals tim.fk_HojaTimbre
                                  join val in db.ccee_ValorTimbre on tim.fk_ValorTimbre equals val.VaT_NoValorTimbre
                                  where cel.fk_Cajero == null
                                  select new ResponseCellarJson()
                                  {
                                      id = cel.HoT_NoHojaTimbre,
                                      numberPage = cel.HoT_NumeroHoja,
                                      textNumberPage = cel.HoT_NumeroHoja.ToString(),
                                      amount = val.VaT_Denominacion,
                                      textAmount = val.VaT_Nombre
                                  }).ToList();

                var select = from r in tempCellar
                             group r by new
                             {
                                 Pages = r.numberPage,
                                 Id = r.id,

                             } into g
                             select new ResponseCellarJson()
                             {
                                 numberPage = g.Key.Pages,
                                 id = g.Key.Id,
                                 amount = g.Select(p => p.amount).FirstOrDefault(),
                                 textAmount = g.Select(o => o.textAmount).FirstOrDefault(),
                                 textNumberPage = g.Select(q => q.textNumberPage).FirstOrDefault()
                             };

                var selectT = select.Where(o => o.textNumberPage.Contains(id));
                Autorization.saveOperation(db, autorization, "Obteniendo hoja timbre");

                return selectT;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_Cellar
        public IHttpActionResult Post(RequestCellarJson cellar)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        List<int> pageList = GeneralFunction.getIntsOfArray(cellar.numberPage);
                        foreach (int page in pageList)
                        {

                            ccee_HojaTimbre pageTimbre = new ccee_HojaTimbre()
                            {
                                HoT_NumeroHoja = page,
                                HoT_FechaCreacion = DateTime.Today,
                                HoT_TempValortimbre = cellar.idValue
                            };

                            db.ccee_HojaTimbre.Add(pageTimbre);
                            db.SaveChanges();


                            for (int i = cellar.initRange; i <= cellar.finishRange; i++)
                            {
                                ccee_Timbre timbre = new ccee_Timbre()
                                {
                                    Tim_NumeroTimbre = i,
                                    Tim_Estatus = 1,
                                    Tim_Valor = cellar.value,
                                    fk_HojaTimbre = pageTimbre.HoT_NoHojaTimbre,
                                    fk_ValorTimbre = cellar.idValue

                                };
                                db.ccee_Timbre.Add(timbre);
                            }
                            Autorization.saveOperation(db, autorization, "Agregando hoja timbre");
                            db.SaveChanges();
                        }
                        
                        transaction.Commit();
                        return Ok(HttpStatusCode.OK);
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        return Ok(HttpStatusCode.BadRequest);
                    }
                    

                }

            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        // PUT: api/Api_Cellar/5
        public IHttpActionResult Put(int id, RequestCellarJson cellar)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                List<int> pageList = GeneralFunction.getIntsOfArray(cellar.numberPage);
                foreach (var page in pageList)
                {

                    var tempPages = db.ccee_HojaTimbre.Where(o => o.fk_Cajero == null &&
                                                o.HoT_NumeroHoja == page && 
                                                o.HoT_TempValortimbre == cellar.idValue);
                    foreach (ccee_HojaTimbre tempPage in tempPages)
                    {
                        tempPage.fk_Cajero = cellar.idCashier;
                        tempPage.HoT_FechaAsignacion = DateTime.Today;
                        db.Entry(tempPage).State = EntityState.Modified;
                    }

                }
                db.SaveChanges();
                Autorization.saveOperation(db, autorization, "Actualizando hoja timbre");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);

            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        [Route("api/Api_Cellar/{id}/{amount}")]
        // DELETE: api/Api_Cellar/5
        public IHttpActionResult Delete(string id, int amount)
        {

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {

                
                    string autorization = Request.Headers.Authorization.Parameter;
                    if (!Autorization.enableAutorization(db, autorization)) return null;

                    List<int> pageList = GeneralFunction.getIntsOfArray(id);
                    foreach (var page in pageList)
                    {

                        /*var tempPages = db.ccee_HojaTimbre.Where(o=>o.fk_Cajero == null &&
                                                    o.HoT_NumeroHoja == page ).ToList();*/

                        var tem = (from hoj in db.ccee_HojaTimbre
                                   join tim in db.ccee_Timbre on hoj.HoT_NoHojaTimbre equals tim.fk_HojaTimbre
                                   join val in db.ccee_ValorTimbre on tim.fk_ValorTimbre equals val.VaT_NoValorTimbre
                                   where hoj.fk_Cajero == null && hoj.HoT_NumeroHoja == page && val.VaT_NoValorTimbre == amount
                                   select new
                                   {
                                       idPage = hoj.HoT_NoHojaTimbre
                                   }).ToList();

                        var tempPages = tem
                                        .GroupBy(o => new {   IdPage = o.idPage })
                                        .Select(g => new { idPage = g.Key.IdPage })
                                        .OrderBy(a => a.idPage)
                                        .ToList();

                        int postitioPrint = 0;
                        foreach (var tempPage in tempPages)
                        {
                            ccee_HojaTimbre pageTimbre = db.ccee_HojaTimbre.Find(tempPage.idPage);
                            var tempTimbre = db.ccee_Timbre.Where(o => o.fk_HojaTimbre == tempPage.idPage);

                            db.ccee_Timbre.RemoveRange(tempTimbre);
                            db.ccee_HojaTimbre.Remove(pageTimbre);
                            db.SaveChanges();
                            Debug.WriteLine("Posicion " + postitioPrint + " -- " + tempPage.idPage);

                        }

                        /*foreach (var tempPage in tempPages)
                        {
                            int idPage = tempPage.HoT_NoHojaTimbre;
                            var tempTimbre = db.ccee_Timbre.Where(o => o.fk_HojaTimbre == idPage);
                            db.ccee_Timbre.RemoveRange(tempTimbre);
                            db.ccee_HojaTimbre.Remove(tempPage);
                            db.SaveChanges();
                        }*/

                    }


                    Autorization.saveOperation(db, autorization, "Eiminando hojas timbres" + id);
                    db.SaveChanges();
                    transaction.Commit();
                    return Ok(HttpStatusCode.OK);

                }
                catch (Exception)
                {
                    return Ok(HttpStatusCode.BadRequest);
                }
            }

        }
        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonResumenPago
    {
        public string text { get; set; }
        public decimal valor { get; set; }
    }
}
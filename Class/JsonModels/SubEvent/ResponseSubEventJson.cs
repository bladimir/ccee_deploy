﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.SubEvent
{
    public class ResponseSubEventJson
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
}
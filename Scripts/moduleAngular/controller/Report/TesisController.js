﻿(function () {
    angular.module('reports')
        .controller('tesis', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'reportFactory', function ($scope, $http, CONSTANT, $anchorScroll, reportFactory) {
            $scope.direction;
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquaters;
            $scope.regexReferee = CONSTANT.REGEX_REFEREE;
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.loadmasive = 0;
            $scope.idloadmasive = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        reportFactory.setAuthorization(response.data);
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.generateList = function () {
                var textGetReport = "";
                console.log($scope.checkboxModel);
                console.log(($scope.selectHead != null) ? $scope.selectHead.id : "");
                textGetReport = "referee=" + $scope.inReferee +
                                "&title=" + $scope.inTitle +
                                "&masiva=" + $scope.loadmasive +
                                "&idmasiva=" + $scope.idloadmasive +

                                "&isTesis=" + $scope.checkboxModel;
                console.log(textGetReport);
                $scope.direction = "../../Reports/aspxReports/tesisAspx.aspx?" + textGetReport;
            }

            $scope.define = function () {
                $scope.showLoadingFunction();
                var information = {
                    datos: $scope.fileContent.replace(/\r\n/g, ',')
                }
                console.log(information);
                reportFactory.actionPostFunction(CONSTANT.URL_GET_TEMP_REPORT, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            console.log(response);
                            if (response.data != 0) {
                                $scope.idloadmasive = response.data;
                                $scope.loadmasive = 1;
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

        }]);

})()
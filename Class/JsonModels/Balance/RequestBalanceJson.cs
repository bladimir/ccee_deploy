﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Balance
{
    public class RequestBalanceJson
    {
        public decimal referee { get; set; }
        public decimal timbre { get; set; }
        public decimal other { get; set; }
    }
}
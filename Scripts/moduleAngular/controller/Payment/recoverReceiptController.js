﻿(function () {
    angular.module('payment')
        .controller('recoverReceipt', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'paymentFactory', '$window', '$location', function ($scope, $http, CONSTANT, $anchorScroll, paymentFactory, $window, $location) {
            var idReferee = $location.search().id;
            $scope.listT;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.idTypeAccountingUpdate = 0;
            $scope.recover = null;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                $scope.idReferee = idReferee;
            }

            $scope.getRecoverReceipt = function () {
                console.log($scope.idReferee);
                $scope.showLoadingFunction();
                paymentFactory.actionGetIntFunction(CONSTANT.URL_API_RECOVERRECEIPT, $scope.idReferee)
                .then(function (response) {
                    $scope.listT = response.data;
                    $scope.hideLoadingFunction();
                    console.log($scope.listT);
                }, function (error) {
                    console.log(error);
                    $scope.hideLoadingFunction();
                });
            }

            $scope.setRecover = function (rec) {
                $scope.recover = rec;
            }


            $scope.addReceipt = function (idType, idCahier) {


                $scope.showLoadingFunction();
                var information = {
                    titleName: "",
                    address: "Guatemala",
                    nit: "",
                    remark: "",
                    idCashier: $scope.recover.cashier,
                    idReferee: $scope.recover.referee,
                    idPay: $scope.recover.pay,
                    idTypeDocumentAccounting: 1,
                    NoDocumentCol: $scope.idReceipt,
                    NoDocumentOther: null,
                    listIdDocs: null,
                    dateTempCol: null,
                    dateTempTim: null,
                    isReferee: true
                }

                //$scope.recovery = $base64.encode(JSON.stringify(information));

                console.log(information);
                paymentFactory.actionPostFunction("/Referre/PrintFileReferee", information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data != null) {
                                console.log(response);
                                $scope.listDocumentPrint = response.data;
                                $scope.showListDocs = true;
                            }

                        }, function (error) {
                            console.log(error);
                            $scope.error = "Problemas de conectividad, intentar de nuevo";
                            $scope.hideLoadingFunction();
                        });
            }


        }]);

})()
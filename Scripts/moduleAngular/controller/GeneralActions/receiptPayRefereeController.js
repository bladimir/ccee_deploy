﻿(function () {
    angular.module('generals')
        .controller('receiptPayReferee', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listReceipt;
            $scope.listEconomic;
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.showEconomic = 0;
            $scope.viewSearch = 0;
            $scope.listReceiptCSV = [];
            $scope.listReceiptTimbreCSV = [];
            $scope.listReceiptNominaCSV = [];
            $scope.completeNameReferee = "";
            $scope.idreferee = "";

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
            }




            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getInfoReferee = function (idReferee) {
                $scope.statusUpdate = "";
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_REFEREE, idReferee)
                    .then(function (response) {
                        var data = response.data;
                        if (data != null) {
                            $scope.completeNameReferee = data.PrimerNombre + " " + data.SegundoNombre + " "
                                                         + data.TercerNombre + " " + data.PrimerApellido + " " + data.SegundoApellido + " " + data.CasdaApellido;
                            $scope.idreferee = idReferee;
                            console.log(data);
                        }
                    }, function (error) {
                        console.log(data);
                    });
            }

            $scope.getReceipt = function () {
                $scope.cleanData();
                $scope.showLoadingFunction();
                $scope.getInfoReferee($scope.idReferee);
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_LISTPAY, $scope.idReferee)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listReceipt = response.data;
                        var sum = 0;
                        for (var i = 0; i < $scope.listReceipt.length; i++) {
                            var cont = {
                                pay: (($scope.listReceipt[i].recibo != null && $scope.listReceipt[i].recibo != '') ? "Pagado" : "No pagado"),
                                month: $scope.listReceipt[i].mes,
                                year: $scope.listReceipt[i].anio,
                                mount: $scope.listReceipt[i].monto,
                                receipt: $scope.listReceipt[i].recibo,
                                dates: $scope.listReceipt[i].fechaReceip
                            }
                            sum += $scope.listReceipt[i].monto;
                            $scope.listReceiptCSV.push(cont);
                        }

                        var cont = {
                            pay: "Total",
                            month: "",
                            year: "",
                            mount: sum,
                            receipt: ""
                        }
                        $scope.listReceiptCSV.push(cont);

                        cont = {
                            monto: sum,
                            mes: "="
                        }
                        $scope.listReceipt.push(cont);


                        $scope.viewSearch = 0;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getReceiptTimbre = function () {
                $scope.cleanData();
                $scope.showLoadingFunction();
                $scope.getInfoReferee($scope.idReferee);
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_LISTPAYTIMBRE, $scope.idReferee)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listReceiptTimbre = response.data;
                        $scope.showEconomic = 2;
                        $scope.viewSearch = 2;

                        var sumB = 0;
                        var sumS = 0;
                        var sumM = 0;
                        var sumD = 0;
                        for (var i = 0; i < $scope.listReceiptTimbre.length; i++) {
                            var cont = {
                                pay: (($scope.listReceiptTimbre[i].recibo != null && $scope.listReceiptTimbre[i].recibo != '') ? "Pagado" : "No pagado"),
                                month: $scope.listReceiptTimbre[i].mes,
                                year: $scope.listReceiptTimbre[i].anio,
                                mountB: $scope.listReceiptTimbre[i].montoT,
                                mountS: $scope.listReceiptTimbre[i].montoP,
                                mountM: $scope.listReceiptTimbre[i].montoC,
                                mountD: $scope.listReceiptTimbre[i].montoD,
                                percent: $scope.listReceiptTimbre[i].porcent * 100,
                                receipt: $scope.listReceiptTimbre[i].recibo,
                                dates: $scope.listReceiptTimbre[i].fechaReceip
                            }
                            sumB += $scope.listReceiptTimbre[i].montoT;
                            sumS += $scope.listReceiptTimbre[i].montoP;
                            sumM += $scope.listReceiptTimbre[i].montoC;
                            sumD += $scope.listReceiptTimbre[i].montoD;
                            $scope.listReceiptTimbreCSV.push(cont);
                        }

                        var cont = {
                            pay: "Total",
                            month: "",
                            year: "",
                            mountB: sumB,
                            mountS: sumS,
                            mountM: sumM,
                            mountD: sumD,
                            percent: "",
                            receipt: ""
                        }
                        $scope.listReceiptTimbreCSV.push(cont);

                        cont = {
                            montoT: sumB,
                            montoP: sumS,
                            montoC: sumM,
                            montoD: sumD,
                            mes: "="
                        }
                        $scope.listReceiptTimbre.push(cont);


                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getReceiptOther = function () {
                $scope.cleanData();
                $scope.showLoadingFunction();
                $scope.getInfoReferee($scope.idReferee);
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_LISTPAYOTHER, $scope.idReferee)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listReceiptTimbre = response.data;
                        $scope.showEconomic = 3;
                        $scope.viewSearch = 3;

                        var sum = 0;
                        for (var i = 0; i < $scope.listReceiptTimbre.length; i++) {
                            var cont = {
                                pay: (($scope.listReceiptTimbre[i].recibo != null && $scope.listReceiptTimbre[i].recibo != '') ? "Pagado" : "No pagado"),
                                month: $scope.listReceiptTimbre[i].mes,
                                year: $scope.listReceiptTimbre[i].anio,
                                description: $scope.listReceiptTimbre[i].tipoPago,
                                count: $scope.listReceiptTimbre[i].count,
                                mount: $scope.listReceiptTimbre[i].monto,
                                multiplicacion: $scope.listReceiptTimbre[i].multiplicacion,
                                receipt: $scope.listReceiptTimbre[i].recibo
                            }
                            sum += $scope.listReceiptTimbre[i].multiplicacion;
                            $scope.listReceiptTimbreCSV.push(cont);
                        }

                        var cont = {
                            pay: "Total",
                            month: "",
                            year: "",
                            description: "",
                            count: "",
                            mount: "",
                            multiplicacion: sum,
                            receipt: ""
                        }
                        $scope.listReceiptTimbreCSV.push(cont);

                        cont = {
                            multiplicacion: sum,
                            mes: "="
                        }
                        $scope.listReceiptTimbre.push(cont);



                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getReceiptNomina = function () {
                $scope.cleanData();
                $scope.showLoadingFunction();
                $scope.getInfoReferee($scope.idReferee);
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_LISTPAYNOMINA, $scope.idReferee)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listReceiptNomina = response.data;
                        $scope.showEconomic = 4;
                        $scope.viewSearch = 4;
                        

                        var sumB = 0;
                        var sumS = 0;
                        var sumM = 0;
                        for (var i = 0; i < $scope.listReceiptNomina.length; i++) {
                            var cont = {
                                pay: (($scope.listReceiptNomina[i].recibo != null && $scope.listReceiptNomina[i].recibo != '') ? "Pagado" : "No pagado"),
                                month: $scope.listReceiptNomina[i].mes,
                                year: $scope.listReceiptNomina[i].anio,
                                mountB: $scope.listReceiptNomina[i].montoT,
                                mountS: $scope.listReceiptNomina[i].montoP,
                                mountM: $scope.listReceiptNomina[i].montoC,
                                percent: $scope.listReceiptNomina[i].porcent * 100,
                                base: $scope.listReceiptNomina[i].montoT * 100,
                                institution: $scope.listReceiptNomina[i].nombre,
                                receipt: $scope.listReceiptNomina[i].recibo
                            }
                            sumB += $scope.listReceiptNomina[i].montoT;
                            sumS += $scope.listReceiptNomina[i].montoP;
                            sumM += $scope.listReceiptNomina[i].montoC;
                            $scope.listReceiptNominaCSV.push(cont);
                        }

                        var cont = {
                            pay: "Total",
                            month: "",
                            year: "",
                            mountB: sumB,
                            mountS: sumS,
                            mountM: sumM,
                            percent: sumB + sumS + sumM
                        }
                        $scope.listReceiptNominaCSV.push(cont);

                        cont = {
                            montoT: sumB,
                            montoP: sumS,
                            montoC: sumM,
                            percent: sumB + sumS + sumM,
                            mes: "=",
                            fecha: "Total"
                        }
                        $scope.listReceiptNomina.push(cont);


                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.getEconomic = function (recipt) {

                $scope.showLoadingFunction();
                generalactionsFactory.actionGetGetIntFunction(CONSTANT.URL_API_REFEREERECEIPT, $scope.idReferee, recipt.idRecibo)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listEconomic = response.data;
                        $scope.showEconomic = 1;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }



            $scope.cleanData = function () {
                $scope.listReceipt = null;
                $scope.listEconomic = null;
                $scope.listReceiptCSV = [];
                $scope.listReceiptTimbreCSV = [];
                $scope.showEconomic = 0;
            }

            $scope.textTotal = function (list) {

                if (list.mes == "=") {
                    return "Total";
                }
                else {
                    return ((list.recibo != null && list.recibo != '') ? "Pagado" : "No pagado")
                }
            }


        }]);

})()
﻿(function () {
    angular.module('reports')
        .controller('corteCajaRango', ['$scope', '$http', 'CONSTANT', '$anchorScroll', '$window', 'reportFactory', function ($scope, $http, CONSTANT, $anchorScroll, $window, reportFactory) {
            $scope.direction;
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquaters;
            $scope.listReports;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.identical = function (actual, expected) {
                return actual === parseInt(expected);
            }

            $scope.init = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        reportFactory.setAuthorization(response.data);
                        $scope.initF();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.initF = function () {
                $scope.getCashier();
            }


            $scope.getCashier = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_CASHIER)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listCashier = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getDataSelect = function (list) {
                if (list == null) return "";
                var len = list.length;

                var listTotal = "";
                for (var i = 0; i < len; i++) {
                    listTotal += list[i].id + ((len > 0 && i < len - 1) ? "," : "");
                }
                return listTotal;
            }

            $scope.getList = function () {
                var dateI = $scope.inDateI.getFullYear() + "-" + ($scope.inDateI.getMonth() + 1) + "-" + $scope.inDateI.getDate();
                var dateF = $scope.inDateF.getFullYear() + "-" + ($scope.inDateF.getMonth() + 1) + "-" + $scope.inDateF.getDate();
                var cashier = $scope.getDataSelect($scope.inCashi);
                reportFactory.actionGetGetGetIntFunction(CONSTANT.URL_API_CLOSECASHRANGE, dateI, dateF, cashier)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response.data);
                        $scope.listReports = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.getNameCashier = function (idCashier) {
                var len = $scope.listCashier.length;
                for (var i = 0; i < len; i++) {
                    if ($scope.listCashier[i].id == idCashier) {
                        return $scope.listCashier[i].name;
                    }
                }
            }

            $scope.viewReport = function (dir) {
                $window.open("/Reports/CorteCaja/" + dir);
            }

            

        }]);

})()
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace webcee.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ccee_AperturaCierre
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ccee_AperturaCierre()
        {
            this.ccee_BilletesDen = new HashSet<ccee_BilletesDen>();
        }
    
        public int ApC_NoAperturaCierre { get; set; }
        public string ApC_Hora { get; set; }
        public Nullable<System.DateTime> ApC_Fecha { get; set; }
        public Nullable<decimal> ApC_MontoEfectivo { get; set; }
        public Nullable<decimal> ApC_MontoDocumento { get; set; }
        public Nullable<decimal> ApC_MontoTransaccionElectronica { get; set; }
        public Nullable<decimal> ApC_Total { get; set; }
        public Nullable<int> ApC_AbreCierra { get; set; }
        public int fk_Caja { get; set; }
        public Nullable<int> fk_Cajero { get; set; }
        public Nullable<int> ApC_NoRecibo { get; set; }
    
        public virtual ccee_Caja ccee_Caja { get; set; }
        public virtual ccee_Cajero ccee_Cajero { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ccee_BilletesDen> ccee_BilletesDen { get; set; }
    }
}

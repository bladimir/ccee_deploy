﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace webcee.Class.Class
{
    public class DocumentPdf
    {

        public static MemoryStream GetPDFCertificado(string pHTML, int idDocument, string doc)
        {

            var bytes = Encoding.UTF8.GetBytes(pHTML);

            using (var input = new MemoryStream(bytes))
            {
                try
                {
                    var output = new MemoryStream();
                    var document = new Document(new Rectangle(PageSize.LETTER.Width, PageSize.LETTER.Height), 5, 5, 5, 5);
                    var writer = PdfWriter.GetInstance(document, output);
                    byte[] img = CreateQR.createQR(doc);
                    var imgPdf = Image.GetInstance(img);

                    imgPdf.BorderWidth = 15;
                    
                    float percentage = 0.0f;
                    percentage = 50 / imgPdf.Width;
                    imgPdf.ScalePercent(percentage * 100);
                    //float posx = (float)((PageSize.LETTER.Width - imgPdf.ScaledWidth) - (PageSize.LETTER.Width * 0.1));
                    //float posy = (float)((PageSize.LETTER.Height - imgPdf.ScaledHeight) - (PageSize.LETTER.Height * 0.1));
                    imgPdf.Alignment = Element.ALIGN_RIGHT;
                    //imgPdf.SetAbsolutePosition(posx, posy);
                    imgPdf.IndentationRight = 70;
                    writer.CloseStream = false;
                    document.Open();
                    var xmlWorker = XMLWorkerHelper.GetInstance();
                    xmlWorker.ParseXHtml(writer, document, input, Encoding.UTF8);
                    document.Add(imgPdf);
                    document.Close();
                    output.Position = 0;
                    return output;

                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

    }
}
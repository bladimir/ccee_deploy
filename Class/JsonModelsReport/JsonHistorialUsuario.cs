﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonHistorialUsuario
    {
        public string nombre { get; set; }
        public string operacion { get; set; }
        public DateTime? fechaTemp { get; set; }
        public string fecha {
            get
            {
                return ((fechaTemp == null) ? "" : fechaTemp.Value.ToString("dd/MM/yyyy"));
            }
        }
    }
}
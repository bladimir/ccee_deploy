﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.AddressFamily;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_AddressFamilyController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_AddressFamily
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_AddressFamily/5
        public IEnumerable<ResponseAddressFamilyJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempDirFam = from dirf in db.ccee_DireccionFamiliar
                                join addr in db.ccee_Direccion on dirf.fk_Direccion equals addr.Dir_NoDireccion
                                where dirf.fk_Familiar == id
                                select new ResponseAddressFamilyJson()
                                {
                                    idAddressFamily = dirf.DirFam_NoDireccionFamiliar,
                                    street  = addr.Dir_CalleAvenida,
                                    number = addr.Dir_Numero,
                                    zone = addr.Dir_Zona,
                                    colony = addr.Dir_Colonia
                                };
                Autorization.saveOperation(db, autorization, "Obteniendo direcciones beneficiarios");
                return tempDirFam;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_AddressFamily
        public IHttpActionResult Post(RequestAddressFamilyJson addressFamily)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_DireccionFamiliar tempAddresFamily = new ccee_DireccionFamiliar()
                {
                    fk_Direccion = addressFamily.idAddress,
                    fk_Familiar = addressFamily.idFamily
                };

                db.ccee_DireccionFamiliar.Add(tempAddresFamily);
                Autorization.saveOperation(db, autorization, "Agregando direcciones beneficiarios");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
                
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        // PUT: api/Api_AddressFamily/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_AddressFamily/5
        public IHttpActionResult Delete(int id)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_DireccionFamiliar tempDirFam = db.ccee_DireccionFamiliar.Find(id);

                if(tempDirFam == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_DireccionFamiliar.Remove(tempDirFam);
                Autorization.saveOperation(db, autorization, "Eliminando direcciones beneficiarios: " + id);
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

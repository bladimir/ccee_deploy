﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.TypeEconomic
{
    public class ResponseTypeEconomicJson
    {
        public int id { get; set; }
        public string description { get; set; }
        public Nullable<int> debit { get; set; }
        public Nullable<decimal> percent { get; set; }
        public Nullable<decimal> valor { get; set; }
        public Nullable<int> periodicity { get; set; }
        public string calculationCondition { get; set; }
        public int idaccounting { get; set; }
        public Nullable<DateTime> initDate { get; set; }
        public Nullable<DateTime> finalyDate { get; set; }
        public int? status { get; set; }
        public string nameAccouting { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonEntradaSalida
    {
        public DateTime? asignacion { get; set; }
        public string dia
        {
            get
            {
                return (asignacion == null) ? "" : asignacion.Value.ToString("dd/MM/yyyy");
            }
        }
        public decimal denominacion { get; set; }
        public int serie { get; set; }
        public string series { get; set; }
        public int timbre { get; set; }
        public decimal valor { get; set; }
        public int pliegos { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModelsReport;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_ListPayTimbreController : ApiController
    {
        // GET: api/Api_ListPayTimbre
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_ListPayTimbre/5
        public List<JsonEstadoCuentaGeneral> Get(int id)
        {
            string query = remplazeQueryGen(id);
            string query2 = remplazeQueryGen2(id);
            using (var context = new CCEEEntities())
            {
                List<JsonTempEstadoCuentaC> pLis = context.Database.SqlQuery<JsonTempEstadoCuentaC>(query).ToList();
                var tempPList = context.Database.SqlQuery<JsonTempEstadoCuentaC>(query2).ToList();
                pLis.AddRange(tempPList);
                return orderMonthGen(pLis);
            }


            
        }

        // POST: api/Api_ListPayTimbre
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_ListPayTimbre/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_ListPayTimbre/5
        public void Delete(int id)
        {
        }

        private string remplazeQueryGen(int idColegiado)
        {
            string query = @"SELECT DISTINCT tab.fecha as fecha, tab.pago as pago, 
                            tab.C1 as montoC, tab.T1 as montoT, tab.P1 as montoP, doc , ccee_DocContable.DCo_NoRecibo as recibos, 
                            tab.D1 as montoD, ccee_DocContable.DCo_FechaHora as fechaR,  ccee_MedioPago_Pago_C.MPP_Rechazado as rechazado 
                            , ccee_Pago.Pag_Congelado as congelado
                            FROM( 
                            SELECT 
                                 fecha, 
                                 pago, 
                                 doc, 
                                COALESCE([120], 0) as C1, 
                                COALESCE([130], 0) as T1, 
                                COALESCE([5], 0) as P1, 
                                COALESCE([131], 0) as D1 
                            FROM 
                                ( 
                                    select ccee_CargoEconomico_timbre.fk_Pago as pago 
                                            , ccee_CargoEconomico_timbre.fk_Colegiado as col, ccee_CargoEconomico_timbre.fk_TipoCargoEconomico as tipo 
                                            , ccee_CargoEconomico_timbre.CEcT_FechaGeneracion as fecha, ccee_CargoEconomico_timbre.CEcT_Monto as monto 
                                            , ccee_CargoEconomico_timbre.fk_DocContable as doc 
                                    from ccee_CargoEconomico_timbre 
                                    where ccee_CargoEconomico_timbre.fk_Colegiado = [colegiado] 
                                    UNION ALL 
                                    select ccee_CargoEconomico_otros.fk_Pago as pago 
                                    , ccee_CargoEconomico_otros.fk_Colegiado as col, ccee_CargoEconomico_otros.fk_TipoCargoEconomico as tipo 
                                    , ccee_CargoEconomico_otros.CEcO_FechaGeneracion as fecha, ccee_CargoEconomico_otros.CEcO_Monto as monto 
                                    , ccee_CargoEconomico_otros.fk_DocContable as doc 
                                    from ccee_CargoEconomico_otros 
                                    where ccee_CargoEconomico_otros.fk_Colegiado = [colegiado] and ccee_CargoEconomico_otros.fk_TipoCargoEconomico = 131 
                                ) as p 
                            PIVOT 
                                ( 
                                    SUM(monto) FOR tipo IN([120], [130], [5], [131]) 
                                ) AS pPivot 
                            ) as tab 
                            , ccee_DocContable, ccee_MedioPago_Pago_C, ccee_Pago  
                            WHERE tab.doc = ccee_DocContable.DCo_NoDocContable 
                            and ccee_Pago.Pag_NoPago = tab.pago
                            and tab.pago = ccee_MedioPago_Pago_C.fk_Pago ";
            query = query.Replace("[colegiado]", idColegiado.ToString());
            return query;
        }

        private string remplazeQueryGen2(int idColegiado)
        {
            string query = @"SELECT tab.fecha as fecha, tab.pago as pago, 
                            tab.C1 as montoC, tab.T1 as montoT, tab.P1 as montoP, doc 
                            FROM( 
                            SELECT 
                                 fecha, 
                                 pago, 
                                 doc, 
                                COALESCE([120], 0) as C1, 
                                COALESCE([130], 0) as T1, 
                                COALESCE([5], 0) as P1 
                           FROM 
                                ( 
                                    select ccee_CargoEconomico_timbre.fk_Pago as pago 
                                            , ccee_CargoEconomico_timbre.fk_Colegiado as col, ccee_CargoEconomico_timbre.fk_TipoCargoEconomico as tipo 
                                            , ccee_CargoEconomico_timbre.CEcT_FechaGeneracion as fecha, ccee_CargoEconomico_timbre.CEcT_Monto as monto 
                                            , ccee_CargoEconomico_timbre.fk_DocContable as doc 
                                    from ccee_CargoEconomico_timbre 
                                    where ccee_CargoEconomico_timbre.fk_Colegiado = [colegiado] 
                                    and ccee_CargoEconomico_timbre.fk_Pago is null 
                                ) as p 
                            PIVOT 
                                ( 
                                    SUM(monto) FOR tipo IN([120], [130], [5]) 
                                ) AS pPivot 
                            ) as tab ";
            query = query.Replace("[colegiado]", idColegiado.ToString());
            return query;
        }

        private List<JsonEstadoCuentaGeneral> orderMonthGen(List<JsonTempEstadoCuentaC> listValue)
        {
            decimal porcent = 0;
            using (var context = new CCEEEntities())
            {
                porcent = Convert.ToDecimal(GeneralFunction.getKeyTableVariable(context, "Porcentaje"));
            }
            var model = listValue
                    .GroupBy(o => new
                    {
                        Month = o.fecha.Month,
                        Year = o.fecha.Year,
                        Doc = o.doc
                    })
                    .Select(g => new JsonEstadoCuentaGeneral
                    {
                        Month = g.Key.Month,
                        anio = g.Key.Year,
                        montoC = g.Sum(s => s.montoC),
                        montoT = g.Sum(s => s.montoT),
                        montoP = g.Sum(s => s.montoP),
                        montoD = g.Sum(s => s.montoD),
                        tempfechaPago = g.Select(p => p.fechaP).FirstOrDefault(),
                        recibo = g.Select(p => p.recibos).FirstOrDefault(),
                        idRecibo = g.Key.Doc,
                        tempfechaReceipt = g.Select(p=>p.fechaR).FirstOrDefault(),
                        url = Constant.url_dir_print_doc + g.Select(p => p.doc).FirstOrDefault(),
                        porcent = porcent,
                        rechazado = g.Max(p=>p.rechazado),
                        congelado = g.Max(p=>p.congelado)
                    })
                    .OrderBy(a => a.anio)
                    .ThenBy(a => a.Month)
                    .ToList();
            return model;
        }


    }
}

﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using webcee.Models;
using webcee.ViewModels;
using webcee.Class.Class;
using NLog;
using webcee.Class.JsonModels._Tools;

namespace webcee.Class
{
    public class GeneralFunction
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static string MD5Encrypt(string texto)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(texto));
            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }



        public static MemoryStream GetPDFGafete(string pHTML)
        {
            
            var bytes = Encoding.UTF8.GetBytes(pHTML);

            using (var input = new MemoryStream(bytes))
            {
                try
                {
                    var output = new MemoryStream(); // this MemoryStream is closed by FileStreamResult

                    var document = new Document(new Rectangle(PageSize.LETTER.Width / 2, PageSize.LETTER.Height / 2), 5, 5, 5, 5);
                    var writer = PdfWriter.GetInstance(document, output);
                    writer.CloseStream = false;
                    document.Open();

                    var xmlWorker = XMLWorkerHelper.GetInstance();
                    xmlWorker.ParseXHtml(writer, document, input, Encoding.UTF8);
                    document.Close();
                    output.Position = 0;
                    return output;

                }
                catch (Exception)
                {
                    return null;
                }
            }
        }


        public static MemoryStream GetPDFRecibo(string pHTML)
        {
            
            var bytes = Encoding.UTF8.GetBytes(pHTML);

            using (var input = new MemoryStream(bytes))
            {
                try
                {

                    var css = "body{font-family: Lucida;}";
                    FontFactory.Register(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "cour.ttf"), "Lucida");

                    var output = new MemoryStream();    

                    var document = new Document(new Rectangle(PageSize.LETTER.Width, PageSize.LETTER.Height / 2), 1, 1, 1, 1);
                    document.AddTitle("Recibo Colegiado");
                    var writer = PdfWriter.GetInstance(document, output);
                    writer.CloseStream = false;
                    document.Open();

                    var xmlWorker = XMLWorkerHelper.GetInstance();
                    

                    using (var msCSS = new MemoryStream(Encoding.UTF8.GetBytes(css)))
                    {

                        xmlWorker.ParseXHtml(writer, document, input, msCSS, Encoding.UTF8, FontFactory.FontImp);
                    }
                    //xmlWorker.ParseXHtml(writer, document, input, Encoding.UTF8);
                    document.Close();
                    output.Position = 0;
                    return output;

                }
                catch (Exception)
                {
                    return null;
                }
            }
        }



        public static MemoryStream GetPDFLetter(string pHTML)
        {

            var bytes = Encoding.UTF8.GetBytes(pHTML);

            using (var input = new MemoryStream(bytes))
            {
                try
                {
                    var output = new MemoryStream();
                    var document = new Document(new Rectangle(PageSize.LETTER.Width, PageSize.LETTER.Height), 5, 5, 5, 5);
                    var writer = PdfWriter.GetInstance(document, output);
                    writer.CloseStream = false;
                    document.Open();
                    var xmlWorker = XMLWorkerHelper.GetInstance();
                    xmlWorker.ParseXHtml(writer, document, input, Encoding.UTF8);
                    document.Close();
                    output.Position = 0;
                    return output;

                }
                catch (Exception)
                {
                    return null;
                }
            }
        }


        public static Font GetFont(string fontName, string filename)
        {
            if (!FontFactory.IsRegistered(fontName))
            {
                var fontPath = Environment.GetEnvironmentVariable("SystemRoot") + "\\fonts\\" + filename;
                FontFactory.Register(fontPath);
            }
            return FontFactory.GetFont(fontName, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        }

        public static MemoryStream GetPDFDiploma(string pHTML)
        {
            //byte[] bPDF = null;

            //MemoryStream ms = new MemoryStream();
            //TextReader txtReader = new StringReader(pHTML);
            //Document doc = new Document(new Rectangle(PageSize.LEGAL.Width, PageSize.LEGAL.Height), 5, 5, 5, 5);

            //PdfWriter oPdfWriter = PdfWriter.GetInstance(doc, System.Web.HttpContext.Current.Response.OutputStream);

            //HTMLWorker htmlWorker = new HTMLWorker(doc);


            //doc.Open();
            //htmlWorker.StartDocument();
            //htmlWorker.Parse(txtReader);
            //htmlWorker.EndDocument();
            //htmlWorker.Close();
            //doc.Close();
            //bPDF = ms.ToArray();

            

            var bytes = Encoding.UTF8.GetBytes(pHTML);

            using (var input = new MemoryStream(bytes))
            {
                try
                {
                    var output = new MemoryStream(); // this MemoryStream is closed by FileStreamResult

                    var document = new Document(new Rectangle(PageSize.LEGAL.Width, PageSize.LEGAL.Height), 5, 5, 5, 5);
                    var writer = PdfWriter.GetInstance(document, output);
                    //var img = Image.GetInstance()
                    writer.CloseStream = false;
                    document.Open();

                    var xmlWorker = XMLWorkerHelper.GetInstance();
                    xmlWorker.ParseXHtml(writer, document, input, Encoding.UTF8);
                    document.Close();
                    output.Position = 0;
                    return output;

                }
                catch(Exception )
                {
                    return null;
                }


                
            }

        }

        public static DateTime changeStringofDate(string formatDate)
        {
            if (formatDate == null || formatDate.Equals("")) return DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
            try
            {
                return DateTime.ParseExact(formatDate, "dd/MM/yyyy", null);
            }
            catch
            {
                return DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
            }
        }

        public static string changeDateWithCero(int valueDate)
        {
            return (valueDate < 10) ? "0" + valueDate : "" + valueDate;
        }

        public static string DateOfString(DateTime? dateTime)
        {
            if (dateTime == null) return "";
            string dateConvert = changeDateWithCero(dateTime.Value.Day)
                                        + "/" + changeDateWithCero(dateTime.Value.Month)
                                        + "/" + dateTime.Value.Year;
            return dateConvert;
        }

        public static string DateOfStringWS(DateTime? dateTime)
        {
            if (dateTime == null) return "";
            string dateConvert = dateTime.Value.Year
                                        + "" + changeDateWithCero(dateTime.Value.Month)
                                        + "" + changeDateWithCero(dateTime.Value.Day);
            return dateConvert;
        }


        public static bool isValidExtension(string extension)
        {
            Regex regex = new Regex(Constant.Constant.extension_image, RegexOptions.IgnoreCase);
            Match match = regex.Match(extension);
            return (match.Success) ? true : false;
        }



        public static string permissions(Controller c, LoginModelViews login, string viewRol)
        {
            if (login == null) return null;
            OperationPermissions operation = new OperationPermissions(c, login);
            if (operation.permissionViewPage(viewRol) == null) return null;
            operation.defineViewBagPermissions();
            return "";
        }

        public static string permissionsRefereee(Controller c, LoginRefreeModelView login, string viewRol)
        {
            if (login == null) return null;
            return "";
        }

        public static string generateQuery(string variable, string texto, int type )
        {
            if (texto == null || texto.Equals("undefined") || texto.Equals("")) return " ";

            bool fistQuerry = false;
            string textQuerry = " and ( ";
            string[] divText = texto.Split(',');
            for (int i = 0; i < divText.Length; i++)
            {
                string[] divRangeText = divText[i].Split('-');
                if (divRangeText.Length > 1)
                {
                    if (fistQuerry) { textQuerry += " or "; }
                    fistQuerry = true;
                    if(type == Constant.Constant.text_query_is_like)
                    {
                        textQuerry += " ( " + variable + " LIKE " + convertTypeQuery(divRangeText[0], type) + " or "+ variable +" LIKE " + convertTypeQuery(divRangeText[1], type) + " ) ";
                    }
                    else
                    {
                        textQuerry += " ( " + variable + " >= " + convertTypeQuery(divRangeText[0], type) + " and " + variable + " <= " + convertTypeQuery(divRangeText[1], type) + " ) ";
                    }
                }
                else
                {
                    if (fistQuerry) { textQuerry += " or "; }
                    fistQuerry = true;

                    if(type == Constant.Constant.text_query_is_like)
                    {
                        textQuerry += " " + variable + " LIKE " + convertTypeQuery(divRangeText[0], type) + " ";
                    }
                    else
                    {
                        textQuerry += " " + variable + " = " + convertTypeQuery(divRangeText[0], type) + " ";
                    }
                }
            }
            return textQuerry + " ) ";

        }

        public static List<int> getIntsOfArray(string text)
        {
            List<int> listValues = new List<int>();
            if (text == null || text.Equals("undefined") || text.Equals("")) return listValues;
            
            string[] divText = text.Split(',');
            for (int i = 0; i < divText.Length; i++)
            {
                string[] divRangeText = divText[i].Split('-');
                if (divRangeText.Length > 1)
                {
                    int initL = Convert.ToInt32(divRangeText[0]);
                    int endL = Convert.ToInt32(divRangeText[1]);
                    for (int j = initL; j <= endL; j++)
                    {
                        listValues.Add(j);
                    }
                    
                }
                else
                {
                    listValues.Add(Convert.ToInt32(divRangeText[0]));
                }
            }
            return listValues;
        }

        private static string convertTypeQuery(string text, int type)
        {
            switch (type)
            {
                case Constant.Constant.text_query_is_number:
                    return text;
                case Constant.Constant.text_query_is_text:
                    return "'" + text + "'";
                case Constant.Constant.text_query_is_like:
                    return "'%" + text + "%'";
            }
            return "";
        }

        public static string TypeAddress(int idTypeAddress)
        {
            switch (idTypeAddress)
            {
                case 1:
                    return "Residencia";

                case 2:
                    return "Trabajo";

                case 3:
                    return "Notificacion";
                case 4:
                    return "Correspondencia";

                default:
                    return "No Definido";
            }
        }


        public static int getDiffMonths(DateTime end, DateTime init)
        {
            int countMonth = (end.Month - init.Month) + (12 * (end.Year - init.Year));
            return (countMonth <= 0) ? Math.Abs(countMonth) : countMonth;
        }

        public static Nullable<DateTime> changeStringofDateWS(string formatDate)
        {
            if (formatDate == null || formatDate.Equals("")) return null;
            try
            {
                return DateTime.ParseExact(formatDate, "yyyyMMdd-HHmmss", null);
            }
            catch
            {
                return null;
            }
        }

        public static Nullable<DateTime> changeStringofDateSimple(string formatDate)
        {
            if (formatDate == null || formatDate.Equals("")) return null;
            try
            {
                return DateTime.ParseExact(formatDate, "yyyy-MM-dd", null);
            }
            catch
            {
                try
                {
                    return DateTime.ParseExact(formatDate, "yyyy-M-d", null);
                }
                catch (Exception)
                {

                    return null;
                }

            }
        }

        public static string dateNowFormatDateHour()
        {
            
            return DateTime.Now.ToString("dd-MM-yyyy h:mm");
        }

        public static string dateNowFormatDateHourPrint()
        {
            return DateTime.Now.ToString("yyyyMMdd:hhmmss");
        }

        public static string dateFormatDateOfDate(Nullable<DateTime> dateTime)
        {
            return ((dateTime == null) ? "" : dateTime.Value.ToString("dd-MM-yyyy"));
        }

        public static string getKeyTableVariable(CCEEEntities db, string key)
        {
            try
            {
                string value = (from var in db.ccee_Variables
                            where var.Var_Clave == key
                            select var.Var_Valor).Single();
                return value;
            }
            catch (Exception)
            {

                return null;
            }
        }


        public static string enletras(string num)

        {

            string res, dec = "";
            Int64 entero;
            int decimales;
            double nro;
            try
            {
                nro = Convert.ToDouble(num);
            }
            catch
            {
                return "";
            }

            entero = Convert.ToInt64(Math.Truncate(nro));
            decimales = Convert.ToInt32(Math.Round((nro - entero) * 100, 2));
            if(decimales == 100 || decimales == 0)
            {
                dec = " EXACTOS";
            }
            else if (decimales > 0)
            {
                dec = " CON " + decimales.ToString() + "/100";
            }

            res = toText(Convert.ToDouble(entero)) + dec;
            return res;

        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }


        public decimal amountMora(decimal sumTimbre, int count, double Ia)
        {

            if (count == 0) return 0.0m;

            logger.Info("General Function|Suma Timbre|" + sumTimbre);
            logger.Info("General Function|Conteo timbre| " + count);
            double Im = Ia / 12;
            double amountTotal = 0.0;
            double convertSumTimbre = Convert.ToDouble(sumTimbre);

            amountTotal = 1 + Im;
            amountTotal = Math.Pow(Convert.ToDouble(amountTotal), count);
            amountTotal = convertSumTimbre * amountTotal - convertSumTimbre;

            decimal round = Convert.ToDecimal(Math.Round(amountTotal, 2));
            logger.Info("General Function|Total Mora| " + round);
            return round;
        }

        public int monthDifference(DateTime lValue, DateTime rValue)
        {
            int diference = Math.Abs((lValue.Month - rValue.Month) + (12 * (lValue.Year - rValue.Year)));
            logger.Info("General Function|Total Mora| " + diference);
            return diference;
        }

        public static decimal aproxDecimal(decimal amountTotal, decimal cent)
        {
            string[] amountDiv = Convert.ToString(amountTotal).Split('.');
            if(amountDiv.Count() <= 1)
            {
                return amountTotal;
            }
            decimal tempMonto = Convert.ToDecimal(amountDiv[0]);
            decimal tempMontoT = Convert.ToDecimal("0." + amountDiv[1]);
            decimal multipli = Math.Ceiling(tempMontoT / cent);
            tempMonto = tempMonto + (multipli * cent);
            return Convert.ToDecimal(tempMonto);
        }


        public static decimal generarMontoTimbre(int colegiado, CCEEEntities db)
        {
            decimal salarioBase = 8000;
            decimal monto = 0;
            decimal aprox = 0.25m;
            decimal porcent = Convert.ToDecimal(GeneralFunction.getKeyTableVariable(db, "Porcentaje"));

            ccee_Colegiado tempCol = (from col in db.ccee_Colegiado
                                      where col.Col_NoColegiado == colegiado
                                      select col).Single();


            if (tempCol.Col_Salario.Value <= salarioBase)
            {
                monto = salarioBase * (decimal)porcent;
            }
            else
            {
                decimal tempMonto = Math.Round(tempCol.Col_Salario.Value * (decimal)0.01, 2);
                string[] amountDiv = Convert.ToString(tempMonto).Split('.');

                if (amountDiv.Count() <= 1)
                {
                    return tempMonto;
                }
                decimal tempMontoT = Convert.ToDecimal("0." + amountDiv[1]);
                tempMontoT = tempMontoT % aprox;
                if(tempMontoT != 0)
                {
                    monto = tempMonto + (aprox - tempMontoT);
                }else
                {
                    monto = tempMonto;
                }
            }
            return monto;
        }

        public static decimal generarMontoTimbre(decimal salario, decimal cent)
        {
            decimal salarioBase = Constant.Constant.salario_base;
            decimal monto = 0;
            
            if (salario <= salarioBase)
            {
                monto = salarioBase * (decimal)0.01;
            }
            else
            {
                decimal tempMonto = Math.Round(salario * (decimal)0.01, 2);
                monto = aproxDecimal(tempMonto, cent);
            }
            return monto;
        }

        public static List<string> getListToStringComa(string dataComa)
        {
            string[] listColegiados = dataComa.Split(',');


            string data = "";
            List<string> listOfColegiados = new List<string>();
            for (int i = 1; i <= listColegiados.Length; i++)
            {
                string onliColegiado = listColegiados[i - 1];
                if (onliColegiado != null && !onliColegiado.Equals(""))
                    data += listColegiados[i - 1];
                if ((i % 100) == 0)
                {
                    listOfColegiados.Add(data);
                    data = "";
                }
                else
                {
                    if (onliColegiado != null && !onliColegiado.Equals(""))
                        data += ",";
                }
            }

            if (!data.Equals(""))
            {
                string last = Convert.ToString(data[data.Length - 1]);
                if (last.Equals(","))
                {
                    data = data.Substring(0, data.Length - 1);
                }
                listOfColegiados.Add(data);
                data = "";
            }
            return listOfColegiados;
        }

        private static string toText(double value)
        {
            string Num2Text = "";
            value = Math.Truncate(value);
            if (value == 0) Num2Text = "CERO";
            else if (value == 1) Num2Text = "UNO";
            else if (value == 2) Num2Text = "DOS";
            else if (value == 3) Num2Text = "TRES";
            else if (value == 4) Num2Text = "CUATRO";
            else if (value == 5) Num2Text = "CINCO";
            else if (value == 6) Num2Text = "SEIS";
            else if (value == 7) Num2Text = "SIETE";
            else if (value == 8) Num2Text = "OCHO";
            else if (value == 9) Num2Text = "NUEVE";
            else if (value == 10) Num2Text = "DIEZ";
            else if (value == 11) Num2Text = "ONCE";
            else if (value == 12) Num2Text = "DOCE";
            else if (value == 13) Num2Text = "TRECE";
            else if (value == 14) Num2Text = "CATORCE";
            else if (value == 15) Num2Text = "QUINCE";
            else if (value < 20) Num2Text = "DIECI" + toText(value - 10);
            else if (value == 20) Num2Text = "VEINTE";
            else if (value < 30) Num2Text = "VEINTI" + toText(value - 20);
            else if (value == 30) Num2Text = "TREINTA";
            else if (value == 40) Num2Text = "CUARENTA";
            else if (value == 50) Num2Text = "CINCUENTA";
            else if (value == 60) Num2Text = "SESENTA";
            else if (value == 70) Num2Text = "SETENTA";
            else if (value == 80) Num2Text = "OCHENTA";
            else if (value == 90) Num2Text = "NOVENTA";
            else if (value < 100) Num2Text = toText(Math.Truncate(value / 10) * 10) + " Y " + toText(value % 10);

            else if (value == 100) Num2Text = "CIEN";
            else if (value < 200) Num2Text = "CIENTO " + toText(value - 100);
            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) Num2Text = toText(Math.Truncate(value / 100)) + "CIENTOS";

            else if (value == 500) Num2Text = "QUINIENTOS";
            else if (value == 700) Num2Text = "SETECIENTOS";
            else if (value == 900) Num2Text = "NOVECIENTOS";
            else if (value < 1000) Num2Text = toText(Math.Truncate(value / 100) * 100) + " " + toText(value % 100);

            else if (value == 1000) Num2Text = "MIL";
            else if (value < 2000) Num2Text = "MIL " + toText(value % 1000);
            else if (value < 1000000)

            {
                Num2Text = toText(Math.Truncate(value / 1000)) + " MIL";
                if ((value % 1000) > 0) Num2Text = Num2Text + " " + toText(value % 1000);
            }

            else if (value == 1000000) Num2Text = "UN MILLON";
            else if (value < 2000000) Num2Text = "UN MILLON " + toText(value % 1000000);
            else if (value < 1000000000000)
            {
                Num2Text = toText(Math.Truncate(value / 1000000)) + " MILLONES ";
                if ((value - Math.Truncate(value / 1000000) * 1000000) > 0) Num2Text = Num2Text + " " + toText(value - Math.Truncate(value / 1000000) * 1000000);
            }

            else if (value == 1000000000000) Num2Text = "UN BILLON";
            else if (value < 2000000000000) Num2Text = "UN BILLON " + toText(value - Math.Truncate(value / 1000000000000) * 1000000000000);
            else
            {
                Num2Text = toText(Math.Truncate(value / 1000000000000)) + " BILLONES";
                if ((value - Math.Truncate(value / 1000000000000) * 1000000000000) > 0) Num2Text = Num2Text + " " + toText(value - Math.Truncate(value / 1000000000000) * 1000000000000);

            }

            return Num2Text;

        }

    }
}
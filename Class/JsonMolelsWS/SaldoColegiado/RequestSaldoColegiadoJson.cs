﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonMolelsWS.SaldoColegiado
{
    public class RequestSaldoColegiadoJson
    {
        public int noColegiado { get; set; }
        public int noCuotasColegiado { get; set; }
        public int noCoutasTimbre { get; set; }
        public decimal montoTotal { get; set; }
        public decimal efectivo { get; set; }
        public decimal cheques { get; set; }
        public string usuarioPago { get; set; }
        public int agencia { get; set; }
        public string fechaPago { get; set; }
        public string horaPago { get; set; }
        public int boleta_colegiado { get; set; }
        public int boleta_Tim { get; set; }
        public int idCajero { get; set; }
        public int idCaja { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.TimbreChange
{
    public class ResponseTimbreChangeJson
    {
        public int cashier { get; set; }
        public List<RequestTimbreChangeJson> removeTimbre { get; set; }
        public List<RequestTimbreChangeJson> addTimbre { get; set; }
    }
}
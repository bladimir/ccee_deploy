﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Bill
{
    public class DenominationBillJson
    {
        public int count { get; set; }
        public decimal tempValue { get; set; }
        public decimal value {
            get
            {
                return Math.Round(tempValue, 2);
            }
            set { tempValue = value; }
        }
        public int type { get; set; }
    }
}
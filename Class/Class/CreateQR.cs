﻿using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace webcee.Class.Class
{
    public class CreateQR
    {

        public static byte[] createQR(string idDoc)
        {
            string dir = Constant.Constant.url_server + GeneralFunction.Base64Encode(idDoc);
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(dir, QRCodeGenerator.ECCLevel.Q);
            BitmapByteQRCode qrCode = new BitmapByteQRCode(qrCodeData);
            byte[] qrCodeImage = qrCode.GetGraphic(20);
            return qrCodeImage;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.DocAccountOld
{
    public class ResponseDocAccountOldJson
    {
        public string link { get; set; }
        public int status { get; set; }
    }
}
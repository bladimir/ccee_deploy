﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.PaymentCenter
{
    public class RequestPaymentCenterJson
    {
        public string name { get; set; }
        public int idHeadquarter { get; set; }
    }
}
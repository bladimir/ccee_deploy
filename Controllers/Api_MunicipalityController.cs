﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.Municipality;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_MunicipalityController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();


        // GET: api/Api_Municipality
        public IEnumerable<ResponseMunicipalityJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempMunicipality = from mun in db.ccee_Municipio
                                       orderby mun.Mun_NombreMun ascending
                                      select new ResponseMunicipalityJson()
                                      {
                                          id = mun.Mun_NoMunicipio,
                                          name = mun.Mun_NombreMun
                                      };
                Autorization.saveOperation(db, autorization, "Obteniendo municipios");
                return tempMunicipality;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_Municipality/5
        public IEnumerable<ResponseMunicipalityJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempMunicipality = from mun in db.ccee_Municipio
                                       where mun.fk_Departamento == id
                                       orderby mun.Mun_NombreMun ascending
                                       select new ResponseMunicipalityJson()
                                       {
                                           id = mun.Mun_NoMunicipio,
                                           name = mun.Mun_NombreMun
                                       };
                Autorization.saveOperation(db, autorization, "Obteniendo municipios por departamento");
                return tempMunicipality;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_Municipality
        public IHttpActionResult Post(RequestMunicipalityJson municipality)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Municipio tempMunicipality = new ccee_Municipio()
                {
                    Mun_NombreMun = municipality.name,
                    fk_Departamento = municipality.idDepartament
                };

                db.ccee_Municipio.Add(tempMunicipality);
                Autorization.saveOperation(db, autorization, "Agregando municipio");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_Municipality/5
        public IHttpActionResult Put(int id, RequestMunicipalityJson municipality)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Municipio tempMunicipality = db.ccee_Municipio.Find(id);

                if (tempMunicipality == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempMunicipality.Mun_NombreMun = municipality.name;

                db.Entry(tempMunicipality).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando municipio");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_Municipality/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Municipio tempMunicipality = db.ccee_Municipio.Find(id);
                if (tempMunicipality == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_Municipio.Remove(tempMunicipality);
                Autorization.saveOperation(db, autorization, "Eliminando municipio");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}

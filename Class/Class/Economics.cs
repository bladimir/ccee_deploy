﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webcee.Class.JsonModels.Economic;
using webcee.Models;

namespace webcee.Class.Class
{
    public class Economics
    {
        private CCEEEntities db;

        private int idPay;
        private List<ResponseEconomicRefereeJson> listEcoReferee =
            new List<ResponseEconomicRefereeJson>();

        private List<ResponseEconomicRefereeJson> listEcoTimbre =
            new List<ResponseEconomicRefereeJson>();

        private List<ResponseEconomicRefereeJson> listEcoOther =
            new List<ResponseEconomicRefereeJson>();


        public Economics(int idPay, CCEEEntities db)
        {
            this.db = db;
            this.idPay = idPay;
            createListEconomicReferee(this.idPay);
            createListEconomicTimbre(this.idPay);
            createListEconomicOther(this.idPay);
        }


        public int? statusPay()
        {
            ccee_Pago tempPay = db.ccee_Pago.Find(this.idPay);
            return tempPay.Pag_Congelado;
        }

        public List<ResponseEconomicRefereeJson> getListReferee()
        {
            return unionList(listEcoReferee, -2, "Cuota Colegiado");
        }

        private void createListEconomicReferee(int idPay)
        {
            listEcoReferee = (from lis in db.ccee_CargoEconomico_colegiado
                              join tip in db.ccee_TipoCargoEconomico on lis.fk_TipoCargoEconomico equals tip.TCE_NoTipoCargoEconomico
                              where lis.fk_Pago == idPay
                              select new ResponseEconomicRefereeJson()
                              {
                                  id = lis.CEcC_NoCargoEconomico,
                                  amount = lis.CEcC_Monto.Value,
                                  idTypeEconomic = lis.fk_TipoCargoEconomico,
                                  description = tip.TCE_Descripcion,
                                  calcule = tip.TCE_CondicionCalculo,
                                  dateCreated = lis.CEcC_FechaGeneracion

                              }).ToList();

        }

        public List<ResponseEconomicRefereeJson> getListTimbre()
        {
            return unionList(listEcoTimbre, -2, "Cuota Colegiado");
        }

        private void createListEconomicTimbre(int idPay)
        {
            listEcoTimbre = (from lis in db.ccee_CargoEconomico_timbre
                             join tip in db.ccee_TipoCargoEconomico on lis.fk_TipoCargoEconomico equals tip.TCE_NoTipoCargoEconomico
                             where lis.fk_Pago == idPay
                             select new ResponseEconomicRefereeJson()
                             {
                                 id = lis.CEcT_NoCargoEconomico,
                                 amount = lis.CEcT_Monto.Value,
                                 idTypeEconomic = lis.fk_TipoCargoEconomico,
                                 description = tip.TCE_Descripcion,
                                 calcule = tip.TCE_CondicionCalculo,
                                 dateCreated = lis.CEcT_FechaGeneracion

                             }).ToList();
        }

        public List<ResponseEconomicRefereeJson> getListOther()
        {
            return listEcoOther;
        }

        private void createListEconomicOther(int idPay)
        {
            listEcoOther = (from lis in db.ccee_CargoEconomico_otros
                            join tip in db.ccee_TipoCargoEconomico on lis.fk_TipoCargoEconomico equals tip.TCE_NoTipoCargoEconomico
                            where lis.fk_Pago == idPay
                            select new ResponseEconomicRefereeJson()
                            {
                                id = lis.CEcO_NoCargoEconomico,
                                amount = lis.CEcO_Monto.Value,
                                idTypeEconomic = lis.fk_TipoCargoEconomico,
                                description = tip.TCE_Descripcion,
                                calcule = tip.TCE_CondicionCalculo,
                                dateCreated = lis.CEcO_FechaGeneracion

                            }).ToList();
        }


        


        public List<ResponseEconomicRefereeJson> unionList(List<ResponseEconomicRefereeJson> listEco, int typeEco, string title)
        {
            if (listEco.Count <= 0) return listEco;

            var model = listEco
                .GroupBy(o => new
                {
                    Month = o.dateCreated.Value.Month,
                    Year = o.dateCreated.Value.Year
                })
                .Select(g => new ResponseEconomicRefereeJson
                {
                    idTypeEconomic = typeEco,
                    description = title,
                    amount = g.Sum(p => p.amount)
                })
                .ToList();

            return model;
        }


    }
}
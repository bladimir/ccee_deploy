﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.PaymentMethod_Pay;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_PaymentMethodPayController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_PaymentMethodPay
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_PaymentMethodPay/5
        public IEnumerable<ResponsePaymentMethod_PayJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                var tempPayMethodP = from pmp in db.ccee_MedioPago_Pago
                                     join pag in db.ccee_Pago on pmp.pkfk_Pago equals pag.Pag_NoPago
                                     join med in db.ccee_MedioPago on pmp.pkfk_MedioPago equals med.MedP_NoMedioPago
                                     join caj in db.ccee_Caja on pag.fk_Caja equals caj.Cja_NoCaja
                                     join cje in db.ccee_Cajero on pag.fk_Cajero equals cje.Cjo_NoCajero
                                     join ban in db.ccee_BancoOEmisor on med.fk_BancoEmisor equals ban.BanE_NoBancoEmisor
                                     where pmp.MPP_NoDocumento == (""+ id)
                                     select new ResponsePaymentMethod_PayJson()
                                     {
                                         idPay = pmp.pkfk_Pago,
                                         idPaymentMethod = pmp.pkfk_MedioPago,
                                         amount = pmp.MPP_Monto,
                                         noDocument = pmp.MPP_NoDocumento,
                                         noTransaction = pmp.MPP_NoTransaccion,
                                         rejection = pmp.MPP_Rechazado,
                                         reservation = pmp.MPP_Reserva,
                                         remarke = pmp.MPP_Observacion,
                                         date = pmp.MPP_FechaHora,
                                         nameCash = caj.Cja_Nombre,
                                         idCasher = cje.Cjo_NoCajero,
                                         typePaymentMethod = med.MdP_TipoMedio,
                                         issuingBank = ban.BcE_NombreBancoEmisor
                                     };
                Autorization.saveOperation(db, autorization, "Obteniendo metodos de pago");
                return tempPayMethodP;

            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_PaymentMethodPay
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_PaymentMethodPay/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_PaymentMethodPay/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

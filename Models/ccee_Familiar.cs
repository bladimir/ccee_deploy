//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace webcee.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ccee_Familiar
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ccee_Familiar()
        {
            this.ccee_DireccionFamiliar = new HashSet<ccee_DireccionFamiliar>();
            this.ccee_Telefono = new HashSet<ccee_Telefono>();
        }
    
        public int Fam_NoFamiliar { get; set; }
        public string Fam_PrimerNombre { get; set; }
        public string Fam_SegundoNombre { get; set; }
        public string Fam_TercerNombre { get; set; }
        public string Fam_PrimerApellido { get; set; }
        public string Fam_SegundoApellido { get; set; }
        public string Fam_CasdaApellido { get; set; }
        public int fk_TipoBeneficiario { get; set; }
        public int fk_Colegiado { get; set; }
        public Nullable<int> familiar_old { get; set; }
        public Nullable<int> Fam_Porcentaje { get; set; }
        public string Fam_Observaciones { get; set; }
        public string Fam_Dpi { get; set; }
    
        public virtual ccee_Colegiado ccee_Colegiado { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ccee_DireccionFamiliar> ccee_DireccionFamiliar { get; set; }
        public virtual ccee_TipoBeneficiario ccee_TipoBeneficiario { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ccee_Telefono> ccee_Telefono { get; set; }
    }
}

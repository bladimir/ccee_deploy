﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webcee.Models;

namespace webcee.Class.JsonModels._Tools
{
    public class ModelAccountingDocument
    {
        public ccee_MedioPago_Pago medPayPay { get; set; }
        public ccee_MedioPago medPay { get; set; }
    }
}
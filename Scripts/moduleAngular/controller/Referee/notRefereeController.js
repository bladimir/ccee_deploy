﻿(function () {
    angular.module('referee')
        .controller('notReferee', ['$scope', '$http', '$location', 'CONSTANT', '$anchorScroll', 'refereeFactory', function ($scope, $http, $location, CONSTANT, $anchorScroll, refereeFactory) {
            
            $scope.numbers = CONSTANT.REGEX_DIGIT;
            $scope.regexNit = CONSTANT.REGEX_NIT;
            $scope.regexPhone = CONSTANT.REGEX_PHONE;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.info = "";
            $scope.listData;
            $scope.idUpdate = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                $scope.get();
            }

            $scope.setAuth = function () {
                refereeFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    refereeFactory.setAuthorization(response.data);
                    console.log(refereeFactory.getAuthorization());
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.get = function () {
                $scope.showLoadingFunction();
                refereeFactory.actionGetFunction(CONSTANT.URL_API_NOTREFEREE)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listData = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.add = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.name,
                    lastname: $scope.lastname,
                    address: $scope.address,
                    nit: $scope.nit,
                    movil: $scope.phone,
                    remark: $scope.remake,
                    isAfiliado: (($scope.remake == true)? 1 : 0) 
                }
                console.log(information);
                refereeFactory.actionPostFunction(CONSTANT.URL_API_NOTREFEREE, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.cleanData();
                                $scope.get();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (type) {
                $scope.idUpdate = type.id;
                $scope.name = type.name;
                $scope.lastname = type.lastname;
                $scope.nit = type.nit;
                $scope.phone = type.movil;
                $scope.address = type.address;
                $scope.remake = type.remake;
                $scope.afiliado = ((type.afiliado == 1) ? true : false);
                $anchorScroll();
            }

            $scope.update = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.name,
                    lastname: $scope.lastname,
                    address: $scope.address,
                    nit: $scope.nit,
                    movil: $scope.phone,
                    remark: $scope.remake,
                    isAfiliado: (($scope.afiliado == true) ? 1 : 0)
                }
                refereeFactory.actionPutFunction(CONSTANT.URL_API_NOTREFEREE, $scope.idUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.get();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }


            $scope.delete = function (type) {
                $scope.showLoadingFunction();
                refereeFactory.actionDeleteFunction(CONSTANT.URL_API_NOTREFEREE, type.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.get();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idUpdate = 0;
                $scope.name = "";
                $scope.lastname = "";
                $scope.nit = "";
                $scope.phone = "";
                $scope.address = "";
                $scope.remake = "";
                $scope.afiliado = false;
            }
            



            $scope.changeEconomic = function (notReferee) {
                window.location.href = '/Payment/EconomicNotReferee/#?id=' + notReferee.id;
            }


        }]);

})()
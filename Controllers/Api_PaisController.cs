﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.Pais;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_PaisController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Pais
        public IEnumerable<ResponsePaisJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempPais = from pai in db.ccee_Pais
                               orderby pai.Pai_NombrePais ascending
                                     select new ResponsePaisJson()
                                     {
                                         id = pai.Pai_NoPais,
                                         name = pai.Pai_NombrePais
                                     };
                Autorization.saveOperation(db, autorization, "Obteniendo pais");
                return tempPais;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_Pais/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_Pais
        public IHttpActionResult Post(RequestPaisJson country)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Pais tempCountry = new ccee_Pais()
                {
                    Pai_NombrePais = country.name
                };

                db.ccee_Pais.Add(tempCountry);
                Autorization.saveOperation(db, autorization, "Agregando pais");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_Pais/5
        public IHttpActionResult Put(int id, RequestPaisJson country)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Pais tempCountry = db.ccee_Pais.Find(id);

                if (tempCountry == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempCountry.Pai_NombrePais = country.name;

                db.Entry(tempCountry).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando pais");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_Pais/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Pais tempCountry = db.ccee_Pais.Find(id);
                if (tempCountry == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_Pais.Remove(tempCountry);
                Autorization.saveOperation(db, autorization, "Eliminando pais");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}

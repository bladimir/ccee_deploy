﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.University;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_UniversityController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_University
        public IEnumerable<ResponseUniversityJson> Get()
        {
            try
            {

                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;


                var tempUniversity = from uni in db.ccee_Universidad
                                     select new ResponseUniversityJson()
                                     {
                                         id = uni.Uni_NoUniversidad,
                                         name =  uni.Uni_NombreUniversidad,
                                         description = uni.Uni_Descripcion
                                     };

                Autorization.saveOperation(db, autorization, "Obteniendo Universidades");

                return tempUniversity;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_University/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_University
        public IHttpActionResult Post(RequestUniversityJson university)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Universidad tempUniversity = new ccee_Universidad()
                {
                    Uni_NombreUniversidad = university.name,
                    Uni_Descripcion = university.description
                };

                db.ccee_Universidad.Add(tempUniversity);

                Autorization.saveOperation(db, autorization, "Agregando Universidades");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_University/5
        public IHttpActionResult Put(int id, RequestUniversityJson university)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Universidad tempUniversity = db.ccee_Universidad.Find(id);

                if (tempUniversity == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempUniversity.Uni_NombreUniversidad = university.name;
                tempUniversity.Uni_Descripcion = university.description;

                db.Entry(tempUniversity).State = EntityState.Modified;

                Autorization.saveOperation(db, autorization, "Actualizando Universidades");

                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_University/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Universidad tempUniversity = db.ccee_Universidad.Find(id);
                if (tempUniversity == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_Universidad.Remove(tempUniversity);

                Autorization.saveOperation(db, autorization, "Eliminando Universidades");

                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

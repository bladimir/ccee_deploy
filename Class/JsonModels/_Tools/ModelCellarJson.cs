﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels._Tools
{
    public class ModelCellarJson
    {
        public int page { get; set; }
        public int endPage { get; set; }
        public bool isRange { get; set; }
    }
}
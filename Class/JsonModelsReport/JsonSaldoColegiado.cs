﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonSaldoColegiado
    {
        public int noColegiado { get; set; }
        public string nombre { get; set; }
        public string uColegiado {
            get
            {
                return (tempuColegiado == null) ? "" : tempuColegiado.Value.ToString("MM/yyyy");
            }
        }
        public DateTime? tempuColegiado { get; set; }
        public string uTimbre {
            get
            {
                return (tempuTimbre == null) ? "" : tempuTimbre.Value.ToString("MM/yyyy");
            }
        }
        public DateTime? tempuTimbre { get; set; }

        public decimal tempSColegiado { get; set; }
        public decimal sColegiado {
            get
            {
                return Math.Round(tempSColegiado, 2);
            }
            set { tempSColegiado = value; }
        }

        public decimal tempSTimbre { get; set; }
        public decimal sTimbre {
            get
            {
                return Math.Round(tempSTimbre, 2);
            }
            set { tempSTimbre = value; }
        }

        public decimal tempSOtros { get; set; }
        public decimal sOtros {
            get
            {
                return Math.Round(tempSOtros, 2);
            }
            set { tempSOtros = value; }
        }

        public decimal tempSMora { get; set; }
        public decimal sMora {
            get
            {
                return Math.Round(tempSMora, 2);
            }
            set { tempSMora = value; }
        }
        public int cColegiado { get; set; }
        public int cTimbre { get; set; }

        public decimal tempTotal { get; set; }
        public decimal total {
            get
            {
                return Math.Round(tempTotal, 2);
            }
            set { tempTotal = value; }
        }
        public string activo { get; set; }
    }
}
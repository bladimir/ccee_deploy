﻿(function () {
    angular.module('payment')
        .controller('receipt', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'paymentFactory', '$window', function ($scope, $http, CONSTANT, $anchorScroll, paymentFactory, $window) {
            $scope.listTypeAccounting;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.idTypeAccountingUpdate = 0;
            $scope.idReceipt = 0;
            $scope.statusReceipt = "";
            $scope.idPay = 0;
            $scope.listMethod = [];
            $scope.aumontoPaymentMethod = 0;
            $scope.listPaymentMethod;
            $scope.listPaymentMethodPay = [];
            var NoRefereeSearch = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.setAuth = function () {
                paymentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        paymentFactory.setAuthorization(response.data);
                        $scope.getIssuingbank();
                        $scope.getPaymentMethod();
                        $scope.getIdPayMethodCash();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.getIssuingbank = function () {
                paymentFactory.actionGetFunction(CONSTANT.URL_API_ISSUINGBANK)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listBank = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getPaymentMethod = function () {
                $scope.showLoadingFunction();
                paymentFactory.actionGetIntFunction(CONSTANT.URL_API_PAYMENTMETHOD, 40)
                .then(function (response) {
                    $scope.listPaymentMethod = response.data;
                    $scope.hideLoadingFunction();
                }, function (error) {
                    console.log(error);
                    $scope.hideLoadingFunction();
                });
            }

            $scope.getReceipt = function () {
                $scope.idReceipt = 0;
                $scope.showLoadingFunction();
                paymentFactory.actionGetIntFunction(CONSTANT.URL_API_RECEIPT, $scope.numberreceipt)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        NoRefereeSearch = $scope.numberreceipt;
                        console.log(response);
                        $scope.listDataReceipt = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getOnlyReceipt = function (receipt) {
                $scope.idReceipt = 0;
                $scope.showLoadingFunction();
                paymentFactory.actionGetGetIntFunction(CONSTANT.URL_API_RECEIPT, receipt.id, 0)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        if (response.data != null) {
                            $scope.nameReceipt = response.data.name;
                            $scope.dateReceipt = response.data.date;
                            $scope.idReceipt = response.data.id;
                            $scope.dirReceipt = response.data.direction;
                            $scope.revert = response.data.textDelete;
                            $scope.idRever = 0;
                        }
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            //URL_API_REFUNDPAYMENT

            $scope.updateReciber = function () {
                $scope.statusReceipt = "";
                $scope.showLoadingFunction();
                var information = {
                    noReciber: $scope.newreceipt,
                    dateReciber: $scope.datereceipt
                }
                paymentFactory.actionPutFunction(CONSTANT.URL_API_RECEIPT, $scope.idReceipt, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.statusReceipt = "Completado";
                            $window.location.reload();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.statusReceipt = "Error de conexion";
                    });

            }

            $scope.deletePay = function (type) {
                $scope.showLoadingFunction();
                paymentFactory.actionDeleteTwoFunction(CONSTANT.URL_API_RECEIPT, $scope.idReceipt, type)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.idReceipt = 0;
                                $scope.clearData();
                            } else if (response.data == 404) {
                                $scope.statusReceipt = "Pagos ya revertidos";
                            } else if (response.data == 304) {
                                $scope.statusReceipt = "Proceso de eliminacion de pagos tuvo problema";
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                            $scope.statusReceipt = "Problemas de servidor";
                        });
            }

            $scope.getMethodPay = function () {
                $scope.idRever = 2;
                $scope.showLoadingFunction();
                paymentFactory.actionGetGetIntFunction(CONSTANT.URL_API_REFUNDPAYMENT, $scope.idReceipt, 0)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        if (response.data != null) {
                            $scope.idPay = response.data.idPay;
                            $scope.listMethod = response.data.list;
                            $scope.aumontoPaymentMethod = response.data.diference;
                        }
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.deleteMethod = function (uni) {
                $scope.showLoadingFunction();
                paymentFactory.actionDeleteFunction(CONSTANT.URL_API_REFUNDPAYMENT, uni.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getMethodPay();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.getIdPayMethodCash = function () {
                paymentFactory.actionGetIntFunction(CONSTANT.URL_API_VARS, "CCEEMedioPago")
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    $scope.idPayMethodCash = response.data;
                    console.log(response.data);
                }, function (error) {
                    console.log(error);
                    $scope.hideLoadingFunction();
                });
            }

            $scope.addMethodPay = function () {
                $scope.showLoadingFunction();
                var objectCheck = {
                    fk_Pago: $scope.idPay
                }

                var information = {
                    objectCheck: objectCheck,
                    jsonContentMethod: $scope.listPaymentMethodPay
                }
                console.log(information);
                paymentFactory.actionPostFunction(CONSTANT.URL_API_REFUNDPAYMENT, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data != null) {
                                console.log(response.data);
                                $scope.getMethodPay();
                                $scope.listPaymentMethodPay = [];
                            }

                        }, function (error) {
                            console.log(error);
                            $scope.idPay = 0;
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.addListMethodCash = function () {
                if ($scope.amountpayMethod <= $scope.aumontoPaymentMethod && $scope.aumontoPaymentMethod > 0) {
                    var paymentMethod = {
                        amount: parseFloat($scope.amountpayMethod),
                        noTransaction: 0,
                        noDocument: 0,
                        reservation: 0,
                        rejection: 0,
                        remarke: "",
                        idBank: 40,
                        idPaymentMethod: $scope.idPayMethodCash
                    };
                    $scope.listPaymentMethodPay.push(paymentMethod);
                    $scope.aumontoPaymentMethod = parseFloat($scope.aumontoPaymentMethod) - parseFloat($scope.amountpayMethod);
                    $scope.amountpayMethod = "";
                    $scope.numberpayTrans = "";
                    $scope.numberpayDoc = "";
                    $scope.alertamountPayment = "";
                } else {
                    $scope.alertamountPayment = ": El monto no es permitido";
                }
            }

            $scope.addListMethod = function () {
                var idMethodPay = 0;
                var len = $scope.listPaymentMethod.length;
                for (var i = 0; i < len; i++) {
                    var type = $scope.listPaymentMethod[i].typeMethod;
                    if (type == $scope.radioPaymentMethod) {
                        idMethodPay = $scope.listPaymentMethod[i].id;
                        break;
                    }
                }

                console.log(idMethodPay);

                if ($scope.amountpayMethod <= $scope.aumontoPaymentMethod && $scope.aumontoPaymentMethod > 0) {
                    var paymentMethod = {
                        amount: parseFloat($scope.amountpayMethod),
                        noTransaction: $scope.numberpayTrans,
                        noDocument: $scope.numberpayDoc,
                        reservation: $scope.selectReservation,
                        rejection: 0,
                        remarke: $scope.remakepayMothod,
                        idBank: $scope.selectIssuingBank.id,
                        idPaymentMethod: idMethodPay
                    };
                    $scope.listPaymentMethodPay.push(paymentMethod);
                    $scope.aumontoPaymentMethod = parseFloat($scope.aumontoPaymentMethod) - parseFloat($scope.amountpayMethod);
                    $scope.amountpayMethod = "";
                    $scope.numberpayTrans = "";
                    $scope.numberpayDoc = "";
                    $scope.alertamountPayment = "";
                    $scope.viewBank = false;
                } else {
                    $scope.alertamountPayment = ": El monto no es permitido";
                }
            }

            $scope.deleteListMethod = function (data) {
                var pos = $scope.listPaymentMethodPay.indexOf(data);
                console.log(data);
                $scope.aumontoPaymentMethod = parseFloat($scope.aumontoPaymentMethod) + data.amount;
                $scope.listPaymentMethodPay.splice(pos, 1);
            }

            $scope.reCreateReceipt = function () {
                $scope.showLoadingFunction();
                paymentFactory.actionGetGetIntFunction(CONSTANT.URL_API_RECOVERRECEIPT, $scope.idReceipt, 0)
                        .then(function (response) {
                            if (response.data.length > 0) {
                                var dataRecover = response.data[0];
                                var information = {
                                    titleName: "",
                                    address: "Guatemala",
                                    nit: "",
                                    remark: "",
                                    idCashier: dataRecover.cashier,
                                    idReferee: dataRecover.referee,
                                    idPay: dataRecover.pay,
                                    idTypeDocumentAccounting: 1,
                                    NoDocumentCol: NoRefereeSearch,
                                    NoDocumentOther: null,
                                    listIdDocs: null,
                                    dateTempCol: null,
                                    dateTempTim: null,
                                    isReferee: true,
                                    idDocument: dataRecover.document
                                }
                                console.log(information);
                                paymentFactory.actionPostFunction("/Referre/RePrintFileReferee", information)
                                    .then(function (response) {
                                        $scope.hideLoadingFunction();
                                        if (response.data != null) {
                                            console.log(response);
                                            $window.location.reload();
                                        }

                                    }, function (error) {
                                        console.log(error);
                                        $scope.error = "Problemas de conectividad, intentar de nuevo";
                                        $scope.hideLoadingFunction();
                                    });
                            }


                            if (response.data == 200) {
                                console.log(response.data)
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.clearData = function () {
                $scope.idRever = 0;
                $scope.numberreceipt = "";
            }


        }]);

})()
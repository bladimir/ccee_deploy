﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Timbre
{
    public class ResponseTimbreJson
    {
        public int id { get; set; }
        public int? numberTimbre { get; set; }
        public int? status { get; set; }
        public int? value { get; set; }
        public int idPageTimbre { get; set; }
        public int? numberPage { get; set; }
        public int? idPay { get; set; }
    }
}
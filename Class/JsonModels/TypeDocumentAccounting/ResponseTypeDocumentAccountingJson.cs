﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.TypeDocumentAccounting
{
    public class ResponseTypeDocumentAccountingJson
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string templateXML { get; set; }
    }
}
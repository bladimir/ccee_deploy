﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Batch
{
    public class ResponseBatchJson
    {
        public int id { get; set; }
        public int idDocAccount { get; set; }
        public string name { get; set; }
        public string noReceipt { get; set; }
        public DateTime? date { get; set; }
        public int? delete { get; set; }
        public string textDelete
        {
            get
            {
                return ((delete == null || delete == 0) ? "No revertido" : "Revertido");
            }
        }
    }
}
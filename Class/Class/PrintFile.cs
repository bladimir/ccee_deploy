﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using webcee.Class.JsonModels.Economic;
using webcee.Models;

namespace webcee.Class.Class
{
    public class PrintFile
    {

        private class orderEconomic
        {
            public string tuplaHtml { get; set; }
            public decimal totalTupla { get; set; }
        }

        public class methodPay
        {
            public int idMethod { get; set; }
            public string document { get; set; }
            public string banco { get; set; }
            public string transfer { get; set; }
        }

        private int idDoc;
        private CCEEEntities db;
        private int idPay;
        private List<ResponseEconomicRefereeJson> listEcoReferee = 
            new List<ResponseEconomicRefereeJson>();

        private List<ResponseEconomicRefereeJson> listEcoTimbre =
            new List<ResponseEconomicRefereeJson>();

        private List<ResponseEconomicRefereeJson> listEcoOther =
            new List<ResponseEconomicRefereeJson>();

        private List<methodPay> listMethod = new List<methodPay>();

        public PrintFile(int idPay, CCEEEntities db)
        {
            this.db = db;
            this.idPay = idPay;
            createListEconomicReferee(this.idPay);
            createListEconomicTimbre(this.idPay);
            createListEconomicOther(this.idPay);
            createListMethod(this.idPay);
        }

        

        public int getIdDo()
        {
            return idDoc;
        }

        public List<ResponseEconomicRefereeJson> getListOther()
        {
            int key = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "VarCertificacionNuevoCol"));

            List<ResponseEconomicRefereeJson> tempEcoOther = new List<ResponseEconomicRefereeJson>();
            tempEcoOther.AddRange(listEcoOther);

            for (int i = 0; i < tempEcoOther.Count(); i++)
            {
                if(tempEcoOther[i].idTypeEconomic == key)
                {
                    tempEcoOther[i].calcule = "VarCertificacionCol";
                }
            }

            return tempEcoOther;
        }

        private void createListEconomicReferee(int idPay)
        {
            listEcoReferee = (from lis in db.ccee_CargoEconomico_colegiado
                              join tip in db.ccee_TipoCargoEconomico on lis.fk_TipoCargoEconomico equals tip.TCE_NoTipoCargoEconomico
                              where lis.fk_Pago == idPay
                              select new ResponseEconomicRefereeJson()
                              {
                                  id = lis.CEcC_NoCargoEconomico,
                                  amount = lis.CEcC_Monto.Value,
                                  idTypeEconomic = lis.fk_TipoCargoEconomico,
                                  description = tip.TCE_Descripcion,
                                  calcule = tip.TCE_CondicionCalculo,
                                  dateCreated = lis.CEcC_FechaGeneracion

                              }).ToList();
                             
        }

        private void createListEconomicTimbre(int idPay)
        {
            listEcoTimbre = (from lis in db.ccee_CargoEconomico_timbre
                             join tip in db.ccee_TipoCargoEconomico on lis.fk_TipoCargoEconomico equals tip.TCE_NoTipoCargoEconomico
                             where lis.fk_Pago == idPay
                             select new ResponseEconomicRefereeJson()
                             {
                                 id = lis.CEcT_NoCargoEconomico,
                                 amount = lis.CEcT_Monto.Value,
                                 idTypeEconomic = lis.fk_TipoCargoEconomico,
                                 description = tip.TCE_Descripcion,
                                 calcule = tip.TCE_CondicionCalculo,
                                 dateCreated = lis.CEcT_FechaGeneracion

                             }).ToList();
        }

        private void createListEconomicOther(int idPay)
        {
            listEcoOther = (from lis in db.ccee_CargoEconomico_otros
                            join tip in db.ccee_TipoCargoEconomico on lis.fk_TipoCargoEconomico equals tip.TCE_NoTipoCargoEconomico
                            where lis.fk_Pago == idPay
                            select new ResponseEconomicRefereeJson()
                            {
                                id = lis.CEcO_NoCargoEconomico,
                                amount = lis.CEcO_Monto.Value,
                                idTypeEconomic = lis.fk_TipoCargoEconomico,
                                description = tip.TCE_Descripcion,
                                calcule = tip.TCE_CondicionCalculo,
                                dateCreated = lis.CEcO_FechaGeneracion

                            }).ToList();
        }


        


        public bool thereIsPayReferee()
        {
            int countReferee = listEcoReferee.Count();
            countReferee += listEcoTimbre.Count();
            return (countReferee > 0);
        }

        public bool thereIsOther()
        {
            int countOther = listEcoOther.Count();
            return (countOther > 0);
        }


        private void createListMethod(int idPay)
        {
            listMethod = (from met in db.ccee_MedioPago_Pago_C
                             join pag in db.ccee_Pago on met.fk_Pago equals pag.Pag_NoPago
                             join med in db.ccee_MedioPago on met.fk_MedioPago equals med.MedP_NoMedioPago
                             join ban in db.ccee_BancoOEmisor on met.fk_Banco equals ban.BanE_NoBancoEmisor
                             where pag.Pag_NoPago == idPay
                             select new methodPay
                             {
                                 idMethod = med.MdP_TipoMedio.Value,
                                 document = met.MPP_NoDocumento,
                                 banco = ban.BcE_NombreBancoEmisor,
                                 transfer = met.MPP_NoTransaccion

                             }).ToList();
        }

        private void createListMethod(List<methodPay> listMethod)
        {
            this.listMethod = listMethod; 
        }




        public string createDocumentReceipt(int idTypeDoc, string NoReferee, string nameReferee, 
                                            string address, string nit, string remark, 
                                            string NoDocument, string nameUser, int idCashier)
        {

            string template = "";
            List<ResponseEconomicRefereeJson> tempList = new List<ResponseEconomicRefereeJson>();
            List<orderEconomic> tempOrder = new List<orderEconomic>();

            ccee_TipoDocContable tempDocCon = db.ccee_TipoDocContable.Find(idTypeDoc);
            address = (address == null) ? "" : address;

            //tempList.AddRange(unionList(listEcoReferee, -9999, "Cuota Colegio (Q.22.00)"));
            

            List<ResponseEconomicRefereeJson> tempListTTimbre = new List<ResponseEconomicRefereeJson>();
            List<ResponseEconomicRefereeJson> tempListTPostumo = new List<ResponseEconomicRefereeJson>();
            List<ResponseEconomicRefereeJson> tempListTOther = new List<ResponseEconomicRefereeJson>();
            for (int i = 0; i < listEcoTimbre.Count; i++)
            {
                if(listEcoTimbre[i].idTypeEconomic == Constant.Constant.type_economic_base)
                {
                    listEcoTimbre[i].idTypeEconomic = listEcoTimbre[i].idTypeEconomic * -1;
                    tempListTTimbre.Add(listEcoTimbre[i]);
                }
                else if (listEcoTimbre[i].idTypeEconomic == Constant.Constant.type_economic_postumo)
                {
                    listEcoTimbre[i].idTypeEconomic = listEcoTimbre[i].idTypeEconomic * -1;
                    tempListTPostumo.Add(listEcoTimbre[i]);
                }
                else
                {
                    listEcoTimbre[i].idTypeEconomic = listEcoTimbre[i].idTypeEconomic * -1;
                    tempListTOther.Add(listEcoTimbre[i]);
                }
                
            }
            listEcoReferee = listEcoReferee.OrderBy(o => o.idTypeEconomic).ToList();
            tempListTTimbre = tempListTTimbre.OrderBy(o => o.idTypeEconomic).ToList();
            tempListTPostumo = tempListTPostumo.OrderBy(o => o.idTypeEconomic).ToList();
            tempListTOther = tempListTOther.OrderBy(o => o.idTypeEconomic).ToList();
            listEcoOther = listEcoOther.OrderBy(o => o.idTypeEconomic).ToList();

            tempOrder.Add(getTuplasHtml(unionList(listEcoReferee, -9999, "Cuota Colegio (Q.22.00)")));
            tempOrder.Add(getTuplasHtmlOut(tempListTTimbre));
            tempOrder.Add(getTuplasHtml(tempListTPostumo));
            tempOrder.Add(getTuplasHtmlOut(tempListTOther));

            //tempList.AddRange(listEcoTimbre);
            //tempOrder.Add(getTuplasHtml(unionList(listEcoReferee, -9999, "Cuota Colegio (Q.22.00)")));

            tempOrder.Add(getTuplasHtmlOut(unionListOther(listEcoOther, -1, "Nuevo Colegiado")));

            template = createReceiptReferee(tempOrder, tempDocCon.TDC_XmlPlantilla,
                                            NoReferee, nameReferee, remark, address, nameUser, this.listMethod);
            ccee_DocContable tempDoc = new ccee_DocContable()
            {
                DCo_ANombre = nameReferee,
                DCo_Direccion = address,
                DCo_FechaHora = DateTime.Now,
                DCo_Nit = nit,
                DCo_Observacion = remark,
                DCo_Xml = template,
                DCo_HashDoc = "",
                DCo_NoRecibo = NoDocument.ToString(),
                fk_TipoDocContable = tempDocCon.TDC_NoTipoDocContable,
                fk_Cajero = idCashier
            };
            db.ccee_DocContable.Add(tempDoc);
            db.SaveChanges();
            idDoc = tempDoc.DCo_NoDocContable;
            return Constant.Constant.url_dir_print_doc + idDoc;
        }


        private string createReceiptReferee(List<orderEconomic> listEconomic, 
                                            string templateHTML, string NoReferee, string totalName,
                                            string remake, string address, string nameUser,
                                            List<methodPay> listMethod)
        {
            bool isCash = false;
            bool isOther = false;
            string cheque = "";
            string contentTypeEconomic = "";
            decimal totalEconomic = 0;
            string numberOther = "";

            int idTypeCash = Convert.ToInt32(db.ccee_Variables
                                            .Where(o => o.Var_Clave.Equals("esTipoEfectivo"))
                                            .Select(p => p.Var_Valor).FirstOrDefault());

            int idTypeCheque = Convert.ToInt32(db.ccee_Variables
                                            .Where(o => o.Var_Clave.Equals("esTipoCheque"))
                                            .Select(p => p.Var_Valor).FirstOrDefault());

            orderEconomic tempOrderEconomic = new orderEconomic();



            for (int i = 0; i < listEconomic.Count(); i++)
            {
                tempOrderEconomic.tuplaHtml += " " + listEconomic[i].tuplaHtml;
                tempOrderEconomic.totalTupla += listEconomic[i].totalTupla;
            }

            
            contentTypeEconomic += " " + tempOrderEconomic.tuplaHtml;
            totalEconomic += tempOrderEconomic.totalTupla;

            foreach (methodPay method in listMethod)
            {
                if(method.idMethod == idTypeCash)
                {
                    isCash = true;
                }
                else if(method.idMethod == idTypeCheque)
                {
                    cheque += " Cheque: " + method.document + " ";
                    numberOther = method.banco;
                }
                else
                {
                    cheque += " Bol: " + method.transfer + " ";
                    numberOther += method.banco + " ";
                    isOther = true;
                }
            }
            
            
            //templateHTML = templateHTML.Replace(Constant.Constant.html_pdf_remplace_date, GeneralFunction.dateNowFormatDateHour());
            templateHTML = templateHTML.Replace(Constant.Constant.html_pdf_remplace_No_referree, NoReferee);
            templateHTML = templateHTML.Replace(Constant.Constant.html_pdf_remplace_name_referee, totalName);
            templateHTML = templateHTML.Replace(Constant.Constant.html_pdf_remplace_remake, remake);
            templateHTML = templateHTML.Replace(Constant.Constant.html_pdf_remplace_type_economic_reciber, contentTypeEconomic);
            templateHTML = templateHTML.Replace(Constant.Constant.html_pdf_remplace_total_letter, GeneralFunction.enletras(totalEconomic.ToString()));
            templateHTML = templateHTML.Replace(Constant.Constant.html_pdf_remplace_total, totalEconomic.ToString("C", CultureInfo.CreateSpecificCulture("es-GT")));
            templateHTML = templateHTML.Replace(Constant.Constant.html_pdf_remplace_cash, ((isCash) ? "X" : "O"));
            templateHTML = templateHTML.Replace(Constant.Constant.html_pdf_remplace_others, numberOther);
            templateHTML = templateHTML.Replace(Constant.Constant.html_pdf_remplace_cheque, cheque);
            templateHTML = templateHTML.Replace(Constant.Constant.html_pdf_remplace_address, address);
            templateHTML = templateHTML.Replace(Constant.Constant.html_pdf_remplace_cashier, nameUser);
            return templateHTML;
        }

        public string updateDocumentReceipt(int idTypeDoc, string NoReferee, string nameReferee,
                                            string address, string nit, string remark,
                                            string NoDocument, string nameUser, int idCashier, int idReceipt)
        {
            
            string template = "";
            List<ResponseEconomicRefereeJson> tempList = new List<ResponseEconomicRefereeJson>();
            List<orderEconomic> tempOrder = new List<orderEconomic>();

            ccee_TipoDocContable tempDocCon = db.ccee_TipoDocContable.Find(idTypeDoc);
            address = (address == null) ? "" : address;

            //tempList.AddRange(unionList(listEcoReferee, -9999, "Cuota Colegio (Q.22.00)"));


            List<ResponseEconomicRefereeJson> tempListTTimbre = new List<ResponseEconomicRefereeJson>();
            List<ResponseEconomicRefereeJson> tempListTPostumo = new List<ResponseEconomicRefereeJson>();
            List<ResponseEconomicRefereeJson> tempListTOther = new List<ResponseEconomicRefereeJson>();
            for (int i = 0; i < listEcoTimbre.Count; i++)
            {
                if (listEcoTimbre[i].idTypeEconomic == Constant.Constant.type_economic_base)
                {
                    listEcoTimbre[i].idTypeEconomic = listEcoTimbre[i].idTypeEconomic * -1;
                    tempListTTimbre.Add(listEcoTimbre[i]);
                }
                else if (listEcoTimbre[i].idTypeEconomic == Constant.Constant.type_economic_postumo)
                {
                    listEcoTimbre[i].idTypeEconomic = listEcoTimbre[i].idTypeEconomic * -1;
                    tempListTPostumo.Add(listEcoTimbre[i]);
                }
                else
                {
                    listEcoTimbre[i].idTypeEconomic = listEcoTimbre[i].idTypeEconomic * -1;
                    tempListTOther.Add(listEcoTimbre[i]);
                }

            }
            listEcoReferee = listEcoReferee.OrderBy(o => o.idTypeEconomic).ToList();
            tempListTTimbre = tempListTTimbre.OrderBy(o => o.idTypeEconomic).ToList();
            tempListTPostumo = tempListTPostumo.OrderBy(o => o.idTypeEconomic).ToList();
            tempListTOther = tempListTOther.OrderBy(o => o.idTypeEconomic).ToList();
            listEcoOther = listEcoOther.OrderBy(o => o.idTypeEconomic).ToList();

            tempOrder.Add(getTuplasHtml(unionList(listEcoReferee, -9999, "Cuota Colegio (Q.22.00)")));
            tempOrder.Add(getTuplasHtmlOut(tempListTTimbre));
            tempOrder.Add(getTuplasHtml(tempListTPostumo));
            tempOrder.Add(getTuplasHtmlOut(tempListTOther));

            //tempList.AddRange(listEcoTimbre);
            //tempOrder.Add(getTuplasHtml(unionList(listEcoReferee, -9999, "Cuota Colegio (Q.22.00)")));

            tempOrder.Add(getTuplasHtmlOut(unionListOther(listEcoOther, -1, "Nuevo Colegiado")));

            template = createReceiptReferee(tempOrder, tempDocCon.TDC_XmlPlantilla,
                                            NoReferee, nameReferee, remark, address, nameUser, this.listMethod);

            ccee_DocContable tempDoc = db.ccee_DocContable.Find(idReceipt);
            tempDoc.DCo_Xml = template;
            db.Entry(tempDoc).State = EntityState.Modified;

            //ccee_DocContable tempDoc = db.ccee_DocContable.Find(idReceipt);

            /*ccee_DocContable tempDoc = new ccee_DocContable()
            {
                DCo_ANombre = nameReferee,
                DCo_Direccion = address,
                DCo_FechaHora = DateTime.Now,
                DCo_Nit = nit,
                DCo_Observacion = remark,
                DCo_Xml = template,
                DCo_HashDoc = "",
                DCo_NoRecibo = NoDocument.ToString(),
                fk_TipoDocContable = tempDocCon.TDC_NoTipoDocContable,
                fk_Cajero = idCashier
            };
            db.ccee_DocContable.Add(tempDoc);*/
            db.SaveChanges();
            idDoc = tempDoc.DCo_NoDocContable;
            return Constant.Constant.url_dir_print_doc + idDoc;
        }


        private orderEconomic getTuplasHtml(List<ResponseEconomicRefereeJson> listEconomic)
        {
            CultureInfo culture;
            culture = CultureInfo.CreateSpecificCulture("es-GT");
            orderEconomic orderEconomic = new orderEconomic();
            string contentTupla = "";
            decimal totalTupla = 0m;
            


            var select = from economic in listEconomic
                         group economic by new
                         {
                             Category = economic.idTypeEconomic,
                             Amount = economic.amount
                         } into g
                         select new
                         {
                             Amount = g.Key.Amount,
                             category = g.Key.Category,
                             count = g.Count(),
                             descripton = g.Select(y => y.description),
                             sumatory = g.Sum(x => x.amount),
                             date = g.Select(o => o.dateCreated).ToList()
                         };

            foreach (var item in select)
            {
                string tupla = "";
                string nameTypeEconomic = item.descripton.FirstOrDefault();
                if (item.category == -9999 || item.category == (Constant.Constant.type_economic_base * -1)
                    || item.category == (Constant.Constant.type_economic_postumo * -1))
                {

                    string datePrint = "";
                    DateTime dateInit;
                    DateTime dateFinish = new DateTime();

                    for (int i = 0; i < item.date.Count(); i++)
                    {

                        dateInit = item.date[i].Value;
                        

                        bool statusRange = true;
                        int countRange = 0;
                        while (statusRange)
                        {
                            if (i + 1 < item.date.Count())
                            {


                                int monthFist = item.date[i].Value.Month;
                                int monthLast = item.date[i + 1].Value.Month;
                                if ((monthFist + 1 == monthLast) || (monthFist == 12 && monthLast == 1))
                                {
                                    dateFinish = item.date[i+1].Value;
                                    countRange++;
                                }
                                else
                                {
                                    statusRange = false;
                                }
                                i++;
                            }else
                            {
                                statusRange = false;
                                i++;
                            }
                        }
                        i--;
                        if(countRange == 0)
                        {
                            datePrint += " de " + dateInit.ToString("MMM", culture) + " " 
                                + dateInit.Year + ", ";
                        }else
                        {
                            datePrint += " de " + dateInit.ToString("MMM", culture)
                            + " " + dateInit.Year + " a " + dateFinish.ToString("MMM", culture)
                            + " " + dateFinish.Year + ",";
                        }
                        
                    }

                    
                    nameTypeEconomic += datePrint;

                }
                else if (item.category == Constant.Constant.type_economic_timbre_of_demand)
                {
                    DateTime dateNow = DateTime.Today;
                    nameTypeEconomic = "Cuota timbre por la cantidad de Q." + Math.Round(item.sumatory, 2) + " en " +
                                    dateNow.ToString("MMMM", culture) + "/" + dateNow.Year;
                }


                tupla = Constant.Constant.html_pdf_remplace_tupla_type_economic.Replace(
                        Constant.Constant.html_pdf_remplace_type_economic, item.count + " " + nameTypeEconomic);
                tupla = tupla.Replace(Constant.Constant.html_pdf_remplace_amount, item.sumatory.ToString("C", CultureInfo.CreateSpecificCulture("es-GT")));
                contentTupla += " " + tupla;
                totalTupla += item.sumatory;
            }


            //----------------------------------------------------

            //foreach (var item in select)
            //{
            //    string tupla = "";
            //    string nameTypeEconomic = item.descripton.FirstOrDefault();
            //    if (item.category == -9999 || item.category == (Constant.Constant.type_economic_base * -1 ) 
            //        || item.category == (Constant.Constant.type_economic_postumo * -1))
            //    {
            //        string datePrint = "";
            //        if (item.dateMin.Equals(item.dateMax))
            //        {
            //            datePrint = " de " + item.dateMin.Value.ToString("MMMM", culture) + " " + item.dateMin.Value.Year;
            //        }
            //        else
            //        {
            //            datePrint = " de " + item.dateMin.Value.ToString("MMMM", culture)
            //                + " " + item.dateMin.Value.Year + " a " + item.dateMax.Value.ToString("MMMM", culture)
            //                + " " + item.dateMax.Value.Year;
            //        }
            //        nameTypeEconomic += datePrint;

            //    }else if (item.category == Constant.Constant.type_economic_timbre_of_demand)
            //    {
            //        DateTime dateNow = DateTime.Today;
            //        nameTypeEconomic = "Cuota timbre por la cantidad de Q." + Math.Round(item.sumatory, 2) + " en " + 
            //                        dateNow.ToString("MMMM", culture) + "/" + dateNow.Year;
            //    }


            //    tupla = Constant.Constant.html_pdf_remplace_tupla_type_economic.Replace(
            //            Constant.Constant.html_pdf_remplace_type_economic, item.count + " " + nameTypeEconomic);
            //    tupla = tupla.Replace(Constant.Constant.html_pdf_remplace_amount, item.sumatory.ToString("C", CultureInfo.CreateSpecificCulture("es-GT")));
            //    contentTupla += " " + tupla;
            //    totalTupla += item.sumatory;
            //}
            orderEconomic.tuplaHtml = contentTupla;
            orderEconomic.totalTupla = totalTupla;
            return  orderEconomic;
        }

        private orderEconomic getTuplasHtmlOut(List<ResponseEconomicRefereeJson> listEconomic)
        {
            CultureInfo culture;
            culture = CultureInfo.CreateSpecificCulture("es-GT");
            orderEconomic orderEconomic = new orderEconomic();
            string contentTupla = "";
            decimal totalTupla = 0m;



            var select = (from economic in listEconomic
                         group economic by new
                         {
                             Category = economic.idTypeEconomic
                         } into g
                         select new
                         {
                             category = g.Key.Category,
                             count = g.Count(),
                             descripton = g.Select(y => y.description),
                             sumatory = g.Sum(x => x.amount),
                             date = g.Select(o => o.dateCreated).ToList(),
                             dateMin = g.Max(p => p.dateCreated),
                             dateMax = g.Min(p => p.dateCreated)
                         }).ToList();
            //select = select.OrderBy(p => p.date);

            foreach (var item in select)
            {
                var listDates = item.date.ToList();
                if (item.date.Count() > 0)
                {
                    if(item.date[0] != null)
                        listDates = item.date.OrderBy(p => p.Value).ToList();
                }
                string tupla = "";
                string nameTypeEconomic = item.descripton.FirstOrDefault();
                if (item.category == -9999 || item.category == (Constant.Constant.type_economic_base * -1)
                    || item.category == (Constant.Constant.type_economic_postumo * -1))
                {

                    string datePrint = "";
                    DateTime dateInit;
                    DateTime dateFinish = new DateTime();

                    for (int i = 0; i < listDates.Count(); i++)
                    {

                        dateInit = listDates[i].Value;


                        bool statusRange = true;
                        int countRange = 0;
                        while (statusRange)
                        {
                            if (i + 1 < listDates.Count())
                            {


                                int monthFist = listDates[i].Value.Month;
                                int monthLast = listDates[i + 1].Value.Month;
                                if ((monthFist + 1 == monthLast) || (monthFist == 12 && monthLast == 1))
                                {
                                    dateFinish = listDates[i + 1].Value;
                                    countRange++;
                                }
                                else
                                {
                                    statusRange = false;
                                }
                                i++;
                            }
                            else
                            {
                                statusRange = false;
                                i++;
                            }
                        }
                        i--;
                        if (countRange == 0)
                        {
                            datePrint += " de " + dateInit.ToString("MMM", culture) + " "
                                + dateInit.Year + ", ";
                        }
                        else
                        {
                            datePrint += " de " + dateInit.ToString("MMM", culture)
                            + " " + dateInit.Year + " a " + dateFinish.ToString("MMM", culture)
                            + " " + dateFinish.Year + ",";
                        }

                    }


                    nameTypeEconomic += datePrint;

                }
                else if (item.category == Constant.Constant.type_economic_timbre_of_demand)
                {
                    DateTime dateNow = DateTime.Today;
                    nameTypeEconomic = "Cuota timbre por la cantidad de Q." + Math.Round(item.sumatory, 2) + " en " +
                                    dateNow.ToString("MMMM", culture) + "/" + dateNow.Year;
                }


                tupla = Constant.Constant.html_pdf_remplace_tupla_type_economic.Replace(
                        Constant.Constant.html_pdf_remplace_type_economic, item.count + " " + nameTypeEconomic);
                tupla = tupla.Replace(Constant.Constant.html_pdf_remplace_amount, item.sumatory.ToString("C", CultureInfo.CreateSpecificCulture("es-GT")));
                contentTupla += " " + tupla;
                totalTupla += item.sumatory;
            }
            orderEconomic.tuplaHtml = contentTupla;
            orderEconomic.totalTupla = totalTupla;
            return orderEconomic;
        }


        public void setReceiptToEconomic(int idReceipt)
        {
            foreach (ResponseEconomicRefereeJson economic in listEcoReferee)
            {
                ccee_CargoEconomico_colegiado tempCol = db.ccee_CargoEconomico_colegiado.Find(economic.id);
                tempCol.fk_DocContable = idReceipt;
                db.Entry(tempCol).State = EntityState.Modified;
            }
            foreach (ResponseEconomicRefereeJson economic in listEcoTimbre)
            {
                ccee_CargoEconomico_timbre tempTimbre = db.ccee_CargoEconomico_timbre.Find(economic.id);
                tempTimbre.fk_DocContable = idReceipt;
                db.Entry(tempTimbre).State = EntityState.Modified;
            }
            foreach (ResponseEconomicRefereeJson economic in listEcoOther)
            {
                ccee_CargoEconomico_otros tempOther = db.ccee_CargoEconomico_otros.Find(economic.id);
                tempOther.fk_DocContable = idReceipt;
                db.Entry(tempOther).State = EntityState.Modified;
            }
        }


        public int getCountDoc(string calcule)
        {
            return listEcoOther.Where(p => p.calcule == calcule)
                                    .Count();
        }


        public List<ResponseEconomicRefereeJson> unionList(List<ResponseEconomicRefereeJson> listEco, int typeEco, string title)
        {
            var model = listEco
                .GroupBy(o => new
                {
                    Month = o.dateCreated.Value.Month,
                    Year = o.dateCreated.Value.Year
                })
                .Select(g => new ResponseEconomicRefereeJson
                {
                    idTypeEconomic = typeEco,
                    description = title,
                    amount = g.Sum(p=>p.amount),
                    dateCreated = g.Select(o=>o.dateCreated).FirstOrDefault()
                })
                .OrderBy(a => a.dateCreated)
                .ToList();
            
            return model;
        }


        public List<ResponseEconomicRefereeJson> unionListOther(List<ResponseEconomicRefereeJson> listEco, int typeEco, string title)
        {
            
            ResponseEconomicRefereeJson tempEcoReferee = null;
            List<ResponseEconomicRefereeJson> tempOther = new List<ResponseEconomicRefereeJson>();
            string newReferee = GeneralFunction.getKeyTableVariable(db, Constant.Constant.table_var_new_referee);
            int idNewReferee = Convert.ToInt32(newReferee);
            ccee_TipoCargoEconomico newTypeEco = (from tca in db.ccee_TipoCargoEconomico
                                                  where tca.TCE_NoTipoCargoEconomico == idNewReferee
                                                  select tca).Single();

            string calculation = newTypeEco.TCE_CondicionCalculo;
            string[] arrayCalculation = calculation.Split(',');

            for (int i = 0; i < listEco.Count(); i++)
            {
                var ecoRef = listEco[i];
                bool find = false;
                for (int j = 0; j < arrayCalculation.Count(); j++)
                {
                    if(ecoRef.idTypeEconomic == Convert.ToInt32( arrayCalculation[j]))
                    {
                        if(tempEcoReferee == null)
                        {
                            tempEcoReferee = new ResponseEconomicRefereeJson();
                            tempEcoReferee.amount = 0;
                        }
                        tempEcoReferee.amount += ecoRef.amount;
                        find = true;
                    }
                }
                if (!find)
                {
                    tempOther.Add(ecoRef);
                }
            }

            if(tempEcoReferee != null)
            {
                tempEcoReferee.description = title;
                tempEcoReferee.idTypeEconomic = typeEco;
                tempOther.Add(tempEcoReferee);
            }

            return tempOther;
        }

        public List<ResponseEconomicRefereeJson> orderOfDate(List<ResponseEconomicRefereeJson> tempListForDate)
        {
            //List<ResponseEconomicRefereeJson> tempListForDate = unionList(listEcoReferee, -9999, "Cuota Colegio (Q.22.00)");
            int rangeDate = 1;
            for (int i = 0; i < tempListForDate.Count(); i++)
            {
                if (i < tempListForDate.Count() - 1)
                {
                    int monthFist = tempListForDate[i].dateCreated.Value.Month;
                    int monthLast = tempListForDate[i + 1].dateCreated.Value.Month;
                    if ((monthFist + 1 == monthLast) || (monthFist == 12 && monthLast == 1))
                    {
                        tempListForDate[i].positionOfDate = rangeDate;
                        tempListForDate[i + 1].positionOfDate = rangeDate;
                    }
                    else
                    {
                        rangeDate++;
                        tempListForDate[i + 1].positionOfDate = rangeDate;
                    }
                }
            }
            return tempListForDate;
        }




        // ambito para la creacion del documento e insertarlo en uno ya creado
        // Mejorar este
        public string createDocumentReceipt2(int idTypeDoc, string NoReferee, string nameReferee,
                                            string address, string nit, string remark,
                                            string NoDocument, string nameUser, int idDocAccount)
        {

            //string template = "";
            //List<ResponseEconomicRefereeJson> tempList = new List<ResponseEconomicRefereeJson>();
            //ccee_TipoDocContable tempDocCon = db.ccee_TipoDocContable.Find(idTypeDoc);
            //address = (address == null) ? "" : address;

            //tempList.AddRange(unionList(listEcoReferee, -9999, "Cuota Colegio (Q.22.00)"));
            ////tempList.AddRange(unionList(listEcoTimbre, -1, "Cuota Timbre"));
            //for (int i = 0; i < listEcoTimbre.Count; i++)
            //{
            //    listEcoTimbre[i].idTypeEconomic = listEcoTimbre[i].idTypeEconomic * -1;
            //}
            //tempList.AddRange(listEcoTimbre);
            //tempList.AddRange(unionListOther(listEcoOther, -1, "Nuevo Colegiado"));

            //template = createReceiptReferee(null, tempDocCon.TDC_XmlPlantilla,
            //                                NoReferee, nameReferee, remark, address, nameUser, this.listMethod);


            //ccee_DocContable tempDoc = db.ccee_DocContable.Find(idDocAccount);
            //tempDoc.DCo_Xml = template;
            //db.Entry(tempDoc).State = EntityState.Modified;
            //db.SaveChanges();

            //idDoc = tempDoc.DCo_NoDocContable;
            //return Constant.Constant.url_dir_print_doc + idDoc;


            string template = "";
            List<ResponseEconomicRefereeJson> tempList = new List<ResponseEconomicRefereeJson>();
            List<orderEconomic> tempOrder = new List<orderEconomic>();

            ccee_TipoDocContable tempDocCon = db.ccee_TipoDocContable.Find(idTypeDoc);
            address = (address == null) ? "" : address;

            //tempList.AddRange(unionList(listEcoReferee, -9999, "Cuota Colegio (Q.22.00)"));


            List<ResponseEconomicRefereeJson> tempListTTimbre = new List<ResponseEconomicRefereeJson>();
            List<ResponseEconomicRefereeJson> tempListTPostumo = new List<ResponseEconomicRefereeJson>();
            List<ResponseEconomicRefereeJson> tempListTOther = new List<ResponseEconomicRefereeJson>();
            for (int i = 0; i < listEcoTimbre.Count; i++)
            {
                if (listEcoTimbre[i].idTypeEconomic == Constant.Constant.type_economic_base)
                {
                    listEcoTimbre[i].idTypeEconomic = listEcoTimbre[i].idTypeEconomic * -1;
                    tempListTTimbre.Add(listEcoTimbre[i]);
                }
                else if (listEcoTimbre[i].idTypeEconomic == Constant.Constant.type_economic_postumo)
                {
                    listEcoTimbre[i].idTypeEconomic = listEcoTimbre[i].idTypeEconomic * -1;
                    tempListTPostumo.Add(listEcoTimbre[i]);
                }
                else
                {
                    listEcoTimbre[i].idTypeEconomic = listEcoTimbre[i].idTypeEconomic * -1;
                    tempListTOther.Add(listEcoTimbre[i]);
                }

            }
            listEcoReferee = listEcoReferee.OrderBy(o => o.idTypeEconomic).ToList();
            tempListTTimbre = tempListTTimbre.OrderBy(o => o.idTypeEconomic).ToList();
            tempListTPostumo = tempListTPostumo.OrderBy(o => o.idTypeEconomic).ToList();
            tempListTOther = tempListTOther.OrderBy(o => o.idTypeEconomic).ToList();
            listEcoOther = listEcoOther.OrderBy(o => o.idTypeEconomic).ToList();

            tempOrder.Add(getTuplasHtml(unionList(listEcoReferee, -9999, "Cuota Colegio (Q.22.00)")));
            tempOrder.Add(getTuplasHtmlOut(tempListTTimbre));
            tempOrder.Add(getTuplasHtml(tempListTPostumo));
            tempOrder.Add(getTuplasHtmlOut(tempListTOther));

            //tempList.AddRange(listEcoTimbre);
            //tempOrder.Add(getTuplasHtml(unionList(listEcoReferee, -9999, "Cuota Colegio (Q.22.00)")));

            tempOrder.Add(getTuplasHtmlOut(unionListOther(listEcoOther, -1, "Nuevo Colegiado")));

            template = createReceiptReferee(tempOrder, tempDocCon.TDC_XmlPlantilla,
                                            NoReferee, nameReferee, remark, address, nameUser, this.listMethod);
            ccee_DocContable tempDoc = db.ccee_DocContable.Find(idDocAccount);
            tempDoc.DCo_Xml = template;
            db.Entry(tempDoc).State = EntityState.Modified;
            db.SaveChanges();
            idDoc = tempDoc.DCo_NoDocContable;
            return Constant.Constant.url_dir_print_doc + idDoc;

        }

        //finalizacion del ambito




    }
}
﻿(function () {
    angular.module('generals')
        .controller('departament', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listCountry;
            $scope.listDepartament;
            $scope.idCountry = 0;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idDepartamentUpdate = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_COUNTRY)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listCountry = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }
            


            $scope.getCountry = function () {
                $scope.idCountry = $scope.selectCountry.id;
                $scope.getDepartament($scope.idCountry);
            }


            $scope.getDepartament = function (id) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_DEPARTAMENT, id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listDepartament = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.addDepartament = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.departamentname,
                    idCountry: $scope.idCountry
                }
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_DEPARTAMENT, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getDepartament($scope.idCountry);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (dep) {
                $scope.idDepartamentUpdate = dep.id;
                $scope.departamentname = dep.name;
                $anchorScroll();
            }

            $scope.updateDepartament = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.departamentname,
                    idCountry: $scope.idCountry
                }
                generalactionsFactory.actionPutFunction(CONSTANT.URL_API_DEPARTAMENT, $scope.idDepartamentUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.getDepartament($scope.idCountry);
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.deleteDepartament = function (dep) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionDeleteFunction(CONSTANT.URL_API_DEPARTAMENT, dep.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getDepartament($scope.idCountry);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idDepartamentUpdate = 0;
                $scope.departamentname = "";
            }


        }]);

})()
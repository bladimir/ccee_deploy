﻿(function () {
    angular.module('referee')
        .factory('refereeFactoryDB', ['CONSTANT', function (CONSTANT) {
            var idReferee = 0;
            var refereeFactoryDB = {};

            refereeFactoryDB.setIdReferee = function (id) {
                idReferee = id;
            };

            refereeFactoryDB.getIdReferee = function (id) {
                return idReferee;
            };

            return refereeFactoryDB;
        }]);

})()
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Cellar
{
    public class ResponseCellarJson
    {
        public int id { get; set; }
        public int? numberPage { get; set; }
        public string textNumberPage { get; set; }
        public decimal? amount { get; set; }
        public string textAmount { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.ListGeneral
{
    public class RequestListGeneralJson
    {
        public string idUniversity { get; set; }
        public string idTypeReferee { get; set; }
        public string idSede { get; set; }
        public string idSedeReg { get; set; }
        public string idTypeSpecialisti { get; set; }
        public string status { get; set; }
        public int orderSearch { get; set; }
        public string edad { get; set; }
        public string year { get; set; }
        public string academy { get; set; }
        public string referees { get; set; }
        public string typeAcademi { get; set; }
        public bool groupReferee { get; set; }
        public string departament { get; set; }
        public string typeDir { get; set; }
        public int loadMasive { get; set; }

        public bool check1 { get; set; }
        public bool check2 { get; set; }
        public bool check3 { get; set; }
        public bool check4 { get; set; }
        public bool check5 { get; set; }
        public bool check6 { get; set; }
        public bool check7 { get; set; }
        public bool check8 { get; set; }
        public bool check9 { get; set; }
        public bool check10 { get; set; }
        public bool check11 { get; set; }
        public bool check12 { get; set; }
        public bool check13 { get; set; }
        public bool check14 { get; set; }
        public bool check15 { get; set; }
        public bool check16 { get; set; }
        public bool check17 { get; set; }
        public bool check18 { get; set; }
        public bool check19 { get; set; }
        public bool check20 { get; set; }
        public bool check36 { get; set; }
        public bool check42 { get; set; }
    }
}
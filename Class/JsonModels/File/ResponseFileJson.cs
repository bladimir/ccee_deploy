﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.File
{
    public class ResponseFileJson
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
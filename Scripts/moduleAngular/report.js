﻿(function () {

    var app = angular.module('reports', ['ngMaterial', 'ngSanitize', 'ngCsv']);

    app.config(function ($mdThemingProvider) {

        $mdThemingProvider.definePalette('amazingPaletteName', {
            '50': 'ffffff',
            '100': 'F1441C',
            '200': 'ef9a9a',
            '300': 'F1441C',
            '400': 'ef5350',
            '500': '00B60E',
            '600': 'e53935',
            '700': 'd32f2f',
            '800': 'c62828',
            '900': '000000',
            'A100': 'ffffff',
            'A200': '000000',
            'A400': 'ff1744',
            'A700': 'd50000',
            'contrastDefaultColor': 'dark',    // whether, by default, text (contrast)
            // on this palette should be dark or light

            'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
             '200', '300', '400', 'A100'],
            'contrastLightColors': undefined    // could also specify this if default was 'dark'
        });

        $mdThemingProvider.theme('default')
        .backgroundPalette('amazingPaletteName')
        .primaryPalette('orange');
    });

    app.directive('fileReader', function () {
        return {
            scope: {
                fileReader: "="
            },
            link: function (scope, element) {
                $(element).on('change', function (changeEvent) {
                    var files = changeEvent.target.files;
                    if (files.length) {
                        var r = new FileReader();
                        r.onload = function (e) {
                            var contents = e.target.result;
                            scope.$apply(function () {
                                scope.fileReader = contents;
                                scope.testing = contents;
                            });
                        };

                        r.readAsText(files[0]);
                    }
                });
            }
        };
    });


})();
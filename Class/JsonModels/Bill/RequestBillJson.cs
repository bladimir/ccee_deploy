﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Bill
{
    public class RequestBillJson
    {
        public DateTime date { get; set; }
        public List<DenominationBillJson> list { get; set; }
    }
}
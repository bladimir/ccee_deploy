﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.BankAccount;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_BackAccountController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_BackAccount
        public IEnumerable<ResponseBankAccountJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var bank = from cue in db.ccee_CuentaBancaria
                           join ban in db.ccee_BancoOEmisor on cue.fk_BancoEmisor equals ban.BanE_NoBancoEmisor
                           select new ResponseBankAccountJson()
                           {
                               id = cue.pk_CuentaBAncaria,
                               name = cue.CtB_Nombre,
                               format = cue.CtB_Formato,
                               codigo = cue.CtB_Codigo,
                               bank = ban.BcE_NombreBancoEmisor,
                               NoAccount = cue.CtB_NoCuenta
                           };
                Autorization.saveOperation(db, autorization, "Obteniendo cuenta bancarias");
                return bank;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_BackAccount/5
        public IEnumerable<ResponseBankAccountJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var bank = from cue in db.ccee_CuentaBancaria
                           join ban in db.ccee_BancoOEmisor on cue.fk_BancoEmisor equals ban.BanE_NoBancoEmisor
                           where cue.fk_BancoEmisor == id
                           select new ResponseBankAccountJson()
                           {
                               id = cue.pk_CuentaBAncaria,
                               name = cue.CtB_Nombre,
                               format = cue.CtB_Formato,
                               codigo = cue.CtB_Codigo,
                               bank = ban.BcE_NombreBancoEmisor,
                               NoAccount = cue.CtB_NoCuenta
                           };
                Autorization.saveOperation(db, autorization, "Obteniendo cuenta bancaria");
                return bank;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_BackAccount
        public IHttpActionResult Post(RequestBankAccountJson bankAccount)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_CuentaBancaria tempAccount = new ccee_CuentaBancaria()
                {
                    CtB_Nombre = bankAccount.name,
                    CtB_Codigo = bankAccount.codigo,
                    CtB_Formato = bankAccount.format,
                    CtB_NoCuenta = bankAccount.NoAccount,
                    fk_BancoEmisor = bankAccount.idBank
                };

                db.ccee_CuentaBancaria.Add(tempAccount);
                Autorization.saveOperation(db, autorization, "Agregando cuenta bancaria");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_BackAccount/5
        public IHttpActionResult Put(int id, RequestBankAccountJson bankAccount)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_CuentaBancaria tempAccount = db.ccee_CuentaBancaria.Find(id);

                if (tempAccount == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempAccount.CtB_Nombre = bankAccount.name;
                tempAccount.CtB_Codigo = bankAccount.codigo;
                tempAccount.CtB_Formato = bankAccount.format;
                tempAccount.CtB_NoCuenta = bankAccount.NoAccount;

                db.Entry(tempAccount).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando cuenta bancaria");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_BackAccount/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_CuentaBancaria tempAccount = db.ccee_CuentaBancaria.Find(id);
                if (tempAccount == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_CuentaBancaria.Remove(tempAccount);
                Autorization.saveOperation(db, autorization, "Eliminando cuenta bancaria");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}

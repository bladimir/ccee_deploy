﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonEtiqueta
    {
        public int Col_NoColegiado { get; set; }
        public string Uni_NombreUniversidad { get; set; }
        public string TES_GradoAcademico { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public int sexo { get; set; }
    }
}
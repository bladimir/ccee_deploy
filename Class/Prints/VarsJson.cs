﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.Prints
{
    public class VarsJson
    {
        public string tag { get; set; }
        public string content { get; set; }
        public int isRow { get; set; }
        public string textRow { get; set; }
    }
}
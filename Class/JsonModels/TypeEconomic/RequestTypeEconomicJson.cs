﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.TypeEconomic
{
    public class RequestTypeEconomicJson
    {
        public string description { get; set; }
        public int debit { get; set; }
        public decimal percent { get; set; }
        public decimal valor { get; set; }
        public int periodicity { get; set; }
        public string calculationCondition { get; set; }
        public int idaccounting { get; set; }
        public Nullable<DateTime> initDate { get; set; }
        public Nullable<DateTime> finalyDate { get; set; }
        public int status { get; set; }
    }
}
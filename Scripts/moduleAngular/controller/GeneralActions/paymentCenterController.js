﻿(function () {
    angular.module('generals')
        .controller('paymentCenter', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquarter;
            $scope.listPaymentCenter;
            $scope.idHeadquarter = 0;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idPaymentCenterUpdate = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_COUNTRY)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listCountry = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getPayment = function () {
                $scope.idHeadquarter = $scope.selectHeadquart.id;
                $scope.getPaymentCenter($scope.idHeadquarter);
            }


            $scope.getDepartament = function () {
                if ($scope.selectCountry == null) return null;
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_DEPARTAMENT, $scope.selectCountry.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listDepartament = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getMunicipality = function () {
                if ($scope.selectDepartament == null) return null;
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_MUNICIPALITY, $scope.selectDepartament.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listMunicipality = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getHeadquar = function () {
                if ($scope.selectMunicipality == null) return null;
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_HEADQUARTERS, $scope.selectMunicipality.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listHeadquarter = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getPaymentCenter = function (id) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_PAYMENTCENTER, id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listPaymentCenter = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.addPaymentCenter = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.paymentname,
                    idHeadquarter: $scope.idHeadquarter
                }
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_PAYMENTCENTER, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getPaymentCenter($scope.idHeadquarter);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (pay) {
                $scope.idPaymentCenterUpdate = pay.id;
                $scope.paymentname = pay.name;
                $anchorScroll();
            }

            $scope.updatePaymentCenter = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.paymentname
                }
                generalactionsFactory.actionPutFunction(CONSTANT.URL_API_PAYMENTCENTER, $scope.idPaymentCenterUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.getPaymentCenter($scope.idHeadquarter);
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.deletePaymentCenter = function (pay) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionDeleteFunction(CONSTANT.URL_API_PAYMENTCENTER, pay.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getPaymentCenter($scope.idHeadquarter);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idPaymentCenterUpdate = 0;
                $scope.paymentname = "";
            }



        }]);

})()
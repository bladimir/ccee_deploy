﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.DepositSlit
{
    public class ResponseDepositSlitNullJson
    {
        public int? con { get; set; }
        public string nombre { get; set; }
        public string DCo_NoRecibo { get; set; }
        public int? fk_BoletaDeposito { get; set; }
    }
}
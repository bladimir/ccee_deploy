﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Constant;
using webcee.Class.JsonModels.SubEvent;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_SubEventController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        public ResponseSubEventJson Get(int id)
        {
            try
            {
                ccee_SubEvento tempSubEvent = db.ccee_SubEvento.Find(id);
                
                return new ResponseSubEventJson() {
                    id = tempSubEvent.SEv_NoSubEvento,
                    name = tempSubEvent.SEv_NombreSubEvento,
                    description = tempSubEvent.SEv_Descripcion
                };
            }
            catch
            {
                return null;
            }
        }


        // POST: api/Api_SubEvent
        public IHttpActionResult Post(RequestSubEventJson subEvent)
        {

            ccee_SubEvento tempSubEvent = new ccee_SubEvento()
            {
                SEv_NombreSubEvento = subEvent.name,
                SEv_Descripcion = subEvent.description,
                fk_Evento = subEvent.events
            };

            try
            {
                db.ccee_SubEvento.Add(tempSubEvent);
                db.SaveChanges();

                ccee_SubEventoDoc tempSubEventDoc = new ccee_SubEventoDoc()
                {
                    pkfk_DocEvento = subEvent.document,
                    pkfk_SubEvento = tempSubEvent.SEv_NoSubEvento,
                    SED_Impreso = subEvent.statusPrint
                };

                db.ccee_SubEventoDoc.Add(tempSubEventDoc);
                db.SaveChanges();

                return Ok(Constant.status_code_success);
            }
            catch
            {
                return Ok(Constant.status_code_error);
            }
            
        }


        public IHttpActionResult Put(int id, RequestSubEventJson subEvent)
        {
            try
            {
                ccee_SubEvento tempSubEvent = db.ccee_SubEvento.Find(id);
                if (tempSubEvent != null)
                {
                    tempSubEvent.SEv_NombreSubEvento = subEvent.name;
                    tempSubEvent.SEv_Descripcion = subEvent.description;
                    db.Entry(tempSubEvent).State = EntityState.Modified;
                    db.SaveChanges();
                    return Ok(Constant.status_code_success);
                }
                else
                {
                    return Ok(Constant.status_code_error);
                }
            }
            catch
            {
                return Ok(Constant.status_code_error);
            }
            
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



    }
}

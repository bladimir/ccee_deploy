﻿(function () {
    angular.module('generals')
        .controller('openClose', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listCash;
            $scope.listOpenCloseCash;
            $scope.listPaymentMethodPay;
            $scope.listHeadquarter;
            $scope.listPaymentCenter;
            $scope.listSelectState;
            $scope.idPaymentCenter = 0;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idCashUpdate = 0;
            $scope.objectCashUpdate;
            $scope.idCashier;
            $scope.regexAmount = CONSTANT.REGEX_AMOUNT;
            $scope.regexDigit = CONSTANT.REGEX_DIGIT;
            $scope.showCash = false;
            $scope.showStatus = 0;
            $scope.idCash = 0;
            $scope.idCashier = 0;
            $scope.statusRequerid = 0;
            $scope.idCashRequerid = 0;
            $scope.subTotal = 0.0;
            $scope.enableTotal = false;



            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.headquartes = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_HEADQUARTERS)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listHeadquarter = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.setAuth = function (id) {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.getInit(id);
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            

            //generalactionsFactory.actionGetFunction(CONSTANT.URL_API_OPENCLOSECASH)
            //    .then(function (response) {
            //        $scope.hideLoadingFunction();
            //        console.log(response);
            //        $scope.listOpenCloseCash = response.data;
            //    }, function (error) {
            //        $scope.hideLoadingFunction();
            //        console.log(error);
            //    });

            

            $scope.getPaymentCenter = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_PAYMENTCENTER, $scope.amountSelectHeadquarter.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listPaymentCenter = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getCash = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_CASH, $scope.amountSelectPaymentCenter.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listCash = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.getInit = function (id) {
                $scope.idCashier = id;
                $scope.get($scope.idCashier);
                $scope.headquartes();
            }

            $scope.get = function (id) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_OPENCLOSECASH, id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        $scope.defineSelectState(response.data);
                        console.log(response.data);
                        //
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.aceptOpenClose = function (id, idCash) {
                var select = $scope.amountSelectStateOpen.id;
                if (select == 3) {
                    $scope.idCash = idCash;
                    $scope.idCashier = id;
                    $scope.showStatus = 1;
                } else {
                    $scope.add(id,idCash, select)
                }
            }


            $scope.add = function (id, idCash, idSelect) {
                if ($scope.amountSelectStateOpen == null) {
                    if ($scope.showCash == true) return;
                }
                $scope.showLoadingFunction();
                var information = {
                    amountCash: parseFloat($scope.amountCashOpen),
                    amountDoc: parseFloat(0.0),
                    amountTrans: parseFloat(0.0),
                    total: parseFloat(0.0),
                    openClose: $scope.amountSelectStateOpen.id,
                    idCash: (($scope.showCash == false) ? idCash : $scope.amountSelectCash.id),
                    idCashier: id,
                    receipt: $scope.receiptOpen
                }
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_OPENCLOSECASH, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.get($scope.idCashier);
                                $scope.cleanData();
                                $scope.changeIndex();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.addCloseCash = function (id, idCash) {
                var information = {
                    amountCash: parseFloat($scope.amountCashOpen),
                    amountDoc: parseFloat($scope.amountDocOpen),
                    amountTrans: parseFloat($scope.amountTransOpen),
                    total: parseFloat($scope.amountTotalOpen),
                    openClose: $scope.amountSelectStateOpen.id,
                    idCash: (($scope.idCashRequerid > 0) ? $scope.idCashRequerid : idCash),
                    idCashier: id,
                    receipt: $scope.receiptOpen,
                    bills: $scope.getBills()
                }
                $scope.showLoadingFunction();
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_OPENCLOSECASH, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.changeReport($scope.amountcash.date, id);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }


            $scope.sumValues = function () {
                $scope.subTotal = 0.0;
                $scope.subTotal += $scope.amountcash.cent1 * 0.01;
                $scope.subTotal += $scope.amountcash.cent5 * 0.05;
                $scope.subTotal += $scope.amountcash.cent10 * 0.10;
                $scope.subTotal += $scope.amountcash.cent25 * 0.25;
                $scope.subTotal += $scope.amountcash.cent50 * 0.50;
                $scope.subTotal += $scope.amountcash.cent100 * 1.00;
                $scope.subTotal += $scope.amountcash.bill1 * 1.00;
                $scope.subTotal += $scope.amountcash.bill5 * 5.00;
                $scope.subTotal += $scope.amountcash.bill10 * 10.00;
                $scope.subTotal += $scope.amountcash.bill20 * 20.00;
                $scope.subTotal += $scope.amountcash.bill50 * 50.00;
                $scope.subTotal += $scope.amountcash.bill100 * 100.00;
                $scope.subTotal += $scope.amountcash.bill200 * 200.00;

                $scope.subTotal = Math.round($scope.subTotal * 100) / 100;

                if ($scope.subTotal == $scope.amountCashOpen) {
                    $scope.enableTotal = true;
                } else {
                    $scope.enableTotal = false;
                }

            }

            $scope.getBills = function () {
                var bills = [];
                bills.push({ count: $scope.amountcash.cent1, value: 0.01, type: 0 });
                bills.push({ count: $scope.amountcash.cent5, value: 0.05, type: 0 });
                bills.push({ count: $scope.amountcash.cent10, value: 0.10, type: 0 });
                bills.push({ count: $scope.amountcash.cent25, value: 0.25, type: 0 });
                bills.push({ count: $scope.amountcash.cent50, value: 0.50, type: 0 });
                bills.push({ count: $scope.amountcash.cent100, value: 1.00, type: 0 });
                bills.push({ count: $scope.amountcash.bill1, value: 1.00, type: 1 });
                bills.push({ count: $scope.amountcash.bill5, value: 5.00, type: 1 });
                bills.push({ count: $scope.amountcash.bill10, value: 10.00, type: 1 });
                bills.push({ count: $scope.amountcash.bill20, value: 20.00, type: 1 });
                bills.push({ count: $scope.amountcash.bill50, value: 50.00, type: 1 });
                bills.push({ count: $scope.amountcash.bill100, value: 100.00, type: 1 });
                bills.push({ count: $scope.amountcash.bill200, value: 200.00, type: 1 });
                var dataBill = {
                    date: $scope.amountcash.date,
                    list: bills
                }
                return dataBill;
            }



            //$scope.defineCash = function (id) {
            //    $scope.showLoadingFunction();
            //    generalactionsFactory.actionGetIntFunction("/GeneralActions/OpenCloseCashDefineCash", id)
            //        .then(function (response) {
            //            $scope.hideLoadingFunction();
            //            console.log(response);
            //            $scope.changeIndex();
            //        }, function (error) {
            //            $scope.hideLoadingFunction();
            //            console.log(error);
            //        })
            //}

            $scope.showDataUpdate = function (pay) {
                $scope.idCashUpdate = pay.id;
                $scope.objectCashUpdate = pay;
                $scope.cashname = pay.name;
                $scope.cashactive = pay.activo;
                $anchorScroll();
            }


            $scope.cleanData = function () {
                $scope.idCashUpdate = 0;
                $scope.amountCashOpen = "";
                $scope.amountDocOpen = "";
                $scope.amountTransOpen = "";
                $scope.amountTotalOpen = "";
            }

            $scope.selectOpenClose = function (type) {
                if (type == 0) {
                    return "Abre";
                } else if (type == 1) {
                    return "Cierre";
                } else {
                    return "No definido";
                }
            }

            $scope.defineSelectState = function (list) {
                $scope.listSelectState = [];
                console.log(list);
                if (list != null) {
                    var openCash = { id: 0, name: 'Apertura' };
                    var openTCash = { id: 2, name: 'Apertura Temp' };
                    var closeCash = { id: 3, name: 'Cierre' };
                    $scope.showCash = false;

                    switch (list.openClose) {
                        case 0:
                            $scope.listSelectState.push(closeCash);
                            break;
                        case 1:
                            $scope.listSelectState.push(openTCash);
                            $scope.listSelectState.push(closeCash);
                            break;
                        case 2:
                            $scope.listSelectState.push(closeCash);
                            break;
                        case 3:
                            $scope.listSelectState.push(openCash);
                            break;
                        case 4:
                            $scope.listSelectState.push(closeCash);
                            $scope.idCashRequerid = list.idCash;
                            break;
                        default:
                    }
                }
            }

            $scope.changeIndex = function () {
                window.location.href = '/Referre/Index';
            }

            $scope.changeReport = function (date, idCashier) {
                var textDate = (date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate())
                window.location.href = '/Reports/CorteCaja/#?date=' + textDate + '&cashier=' + idCashier;
                //window.location.href = '/Reports/CorteCaja/' + textDate + "/" + idCashier;
            }

            $scope.showLocations = function () {
                if ($scope.amountSelectStateOpen == null) return;
                var idSelect = $scope.amountSelectStateOpen.id;
                switch (idSelect) {
                    case 0:
                        $scope.showCash = true;
                        break;
                    case 1:
                        $scope.showCash = false;
                        break;
                    case 2:
                        $scope.showCash = true;
                        break;
                    case 3:
                        $scope.showCash = false;
                        break;
                    default:

                }
            }


        }]);

})()
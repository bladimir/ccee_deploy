﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Phone
{
    public class RequestPhoneJson
    {

        public string number { get; set; }
        public int cod { get; set; }
        public int idReferee { get; set; }
        public int idFamily { get; set; }
        public int type { get; set; }
    }
}
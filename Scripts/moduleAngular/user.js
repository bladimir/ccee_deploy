﻿(function () {

    var app = angular.module('user', []);

    app.controller("listUser", ['$scope', '$http', '$location', '$anchorScroll', 'CONSTANTE', 'userFactory', function ($scope, $http, $location, $anchorScroll, CONSTANTE, userFactory) {
        var funciones = this;
        $scope.isVisibleCreate = false;
        $scope.usersList;
        $scope.profiles;
        $scope.newUser;
        $scope.errorPassword;
        $scope.regexPhone = CONSTANTE.REGEX_TELEFONO;
        $scope.regexEmail = CONSTANTE.REGEX_EMAIL;
        $scope.statusError = "";
        $scope.viewEdit = 0;
        $scope.viewDelete = true;
        $scope.autorization = "";

        $scope.setAuth = function () {
            userFactory.actionGetFunction(CONSTANTE.URL_GET_TOKEN)
                .then(function (response) {
                    userFactory.setAuthorization(response.data);
                    $scope.autorization = response.data;
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
        }

        $scope.init = function () {
            //carga del listado principal de usuarios
            $http.get(CONSTANTE.URL_API_USER, {
                headers: { 'Authorization': 'Basic ' + $scope.autorization }
            })
                    .success(function (data) {
                        $scope.usersList = data;
                    })
                    .error(function (data) {
                        console.log(data);
                    });

            //carga del listado de perfiles en el sistema
            $http.get(CONSTANTE.URL_API_PROFILE, {
                headers: { 'Authorization': 'Basic ' + $scope.autorization }
            })
                    .success(function (data) {
                        $scope.profiles = data;
                    })
                    .error(function (data) {
                        console.log(data);
                    });
        }

        

        //Maneja la vista de crear usuarios 
        $scope.setVisibleCreate = function () {
            $scope.isVisibleCreate = !$scope.isVisibleCreate;
        }

        //Crea el usuario en el sistema validando el form
        $scope.createUser = function (userData, form) {
            if (form.$valid) {
                
                var informacion = {
                    Usu_Nombre: $scope.firstname + " " + (($scope.secundname == null) ? "" : $scope.secundname),
                    Usu_Apellido: $scope.threename + " " + (($scope.fourname == null) ? "" : $scope.fourname),
                    Usu_Correo: $scope.email,
                    Usu_TelMovil: $scope.movil,
                    Usu_TelResidencial: $scope.phone,
                    Usu_Clave: $scope.password,
                    Usu_PreguntaClave: $scope.question,
                    Usu_RespuestaClave: $scope.answer,
                    Usu_Per_NoPerfil: $scope.selectedName.id
                }

                console.log(informacion);

                $http.post(CONSTANTE.URL_API_USER, JSON.stringify(informacion),
                    {
                        headers: {
                            'Content-Type': CONSTANTE.HEADER_CONTENT_TYPE_JSON,
                            'Authorization': 'Basic ' + $scope.autorization
                        }
                    })
                    .success(function (data) {
                        if (data == CONSTANTE.STATUS_CODE_SUCCESS) {
                            $scope.statusError = "";
                            $scope.setVisibleCreate();
                            funciones.listViewUser();
                            funciones.cleanFormCreate(userData);
                        } else {
                            funciones.mensajeError(data);
                        }
                    })
                    .error(function (data) {
                        console.log('Error: ' + data);
                    });


            }
        }

        //Mostrar los campos de edicion para que el usuario los pueda modificar
        $scope.showEdit = function(user){
            $anchorScroll();
            $scope.viewEdit = user.id;
            $scope.nameEdit = user.nombre;
            $scope.lastnameEdit = user.apellido;
            $scope.profileType = user.profile;
            $scope.phoneEdit = user.telefonoResidencial;
            $scope.movilEdit = user.telefonoMovil;
        }

        $scope.updateEditUser = function () {
            var informacion = {
                nombre: $scope.nameEdit,
                apellido: $scope.lastnameEdit,
                telefonoMovil: $scope.movilEdit,
                telefonoResidencial: $scope.phoneEdit,
                idProfile: ($scope.profileSelectEdit == null) ? 0 : $scope.profileSelectEdit.id,
                isAdmin: true,
                newPass: $scope.passEdit,

                Usu_Correo: "",
                Usu_Clave:"",
                Usu_PreguntaClave: "",
                Usu_RespuestaClave: "",

            }

            console.log(informacion);

            $http.put(CONSTANTE.URL_API_USER + "/" + $scope.viewEdit, JSON.stringify(informacion),
                {
                    headers: {
                        'Content-Type': CONSTANTE.HEADER_CONTENT_TYPE_JSON,
                        'Authorization': 'Basic ' + $scope.autorization
                    }
                })
                .success(function (data) {
                    if (data == CONSTANTE.STATUS_CODE_SUCCESS) {
                        $scope.viewEdit = 0;
                        $scope.passEdit = null;
                        funciones.listViewUser();

                    } else {
                        
                    }
                })
                .error(function (data) {
                    console.log('Error');
                    console.log(data);
                });
        }

        $scope.hideEdit = function () {
            $scope.viewEdit = 0;
        }


        //
        $scope.edit = function (editUser, form) {
            console.log(editUser.firstname);
        }



        //para  mostrar los botones de eliminar asi no se le da por error
        $scope.beforeDelete = function () {
            $scope.viewDelete = !$scope.viewDelete
        }

        //Elimina el usuario del sistema
        $scope.delete = function (idUser) {

            console.log(idUser.id);

            $http.delete(CONSTANTE.URL_API_USER + "/" + idUser.id, {
                headers: { 'Authorization': 'Basic ' + $scope.autorization }
            })
                .success(function (data) {
                    if (data == CONSTANTE.STATUS_CODE_SUCCESS) {
                        $scope.statusError = "";
                        funciones.listViewUser();
                    }
                })
                .error(function (data) {
                    console.log(data);
                });
        }


        //Funcion para la recarga de los usuarios despues de haber creado o eliminado
        this.listViewUser = function(){
            $http.get(CONSTANTE.URL_API_USER, {
                headers: { 'Authorization': 'Basic ' + $scope.autorization }
            })
                .success(function (data) {
                    console.log(data);
                    $scope.usersList = data;
                })
                .error(function (data) {
                    console.log(data);
                });
        }

        //Manejo de los errores de status code que envia el Api
        this.mensajeError = function (noError) {
            if (noError == CONSTANTE.STATUS_CODE_ERROR) {
                $scope.statusError = CONSTANTE.MENSAJE_STATUS_CODE_ERROR;

            } else if (noError == CONSTANTE.STATUS_CODE_USER_REPEAT) {
                $scope.statusError = CONSTANTE.MENSAJE_STATUS_CODE_USER_REPEAT;

            } else if (noError == CONSTANTE.STATUS_CODE_USER_NOT_FOUND) {
                $scope.statusError = CONSTANTE.MENSAJE_STATUS_CODE_USER_NOT_FOUND;

            }
        }

        //Verifica que el password sea el correcto por las validaciones que se dan
        $scope.$watch('newUser.password', function (text) {

            if (text != null) {
                $scope.infoPass = true;
                $scope.errorPassword = '';

                if (funciones.isValidCaracteresMinimos(text)) $scope.errorPassword += '';
                else $scope.errorPassword += ','+CONSTANTE.MENSAJE_CARACTERES_ERROR;


                if (funciones.isContieneDigito(text)) $scope.errorPassword += '';
                else $scope.errorPassword += ',' + CONSTANTE.MENSAJE_NUMEROS_ERROR;

                if (funciones.isContieneMayusculas(text)) $scope.errorPassword += '';
                else $scope.errorPassword += ',' + CONSTANTE.MENSAJE_MAYUSCULA_ERROR;

                if (funciones.isValidCaracteresMaximos(text)) $scope.errorPassword += '';
                else $scope.errorPassword += ',' + CONSTANTE.MENSAJE_CARACTERES_MAX_ERROR;

                //if ($scope.nuevo.caracteres == '' && $scope.nuevo.digitos == '' && $scope.nuevo.mayusculas == '' && $scope.nuevo.maximo == '')
                //    $scope.infoPass = false;
                if ($scope.errorPassword != '') $scope.errorPassword = "La contraseña no contiene: " + $scope.errorPassword;
                else $scope.errorPassword = "";


            } else {

            }

        })

        //limpia la pantalla de los campos del create
        this.cleanFormCreate = function (userData) {
            $scope.firstname = null;
            $scope.secundname = null;
            $scope.threename = null;
            $scope.fourname = null;
            $scope.email = null;
            $scope.phone = null;
            $scope.movil = null;
            $scope.password = null;
            $scope.repassword = null;
            $scope.question = null;
            $scope.answer = null;
            $scope.create.$setPristine();
            $scope.create.$setValidity();
            $scope.create.$setUntouched();
        }


        //Verifica y muestra mensaje que el pasword no es igual en el repetir password
        $scope.$watch('newUser.repassword', function (text) {
            if (text!=null) {
                if ($scope.newUser.password !== text) {
                    $scope.errorRePassword = "Contraseñas no parecidas";
                } else {
                    $scope.errorRePassword = "";
                }
            }
        })

        //Congunto de validaciones para el password
        this.isValidCaracteresMinimos = function (texto) {
            return texto.length >= CONSTANTE.CARACTERES_MINIMOS;
        }

        this.isValidCaracteresMaximos = function (texto) {
            return texto.length <= CONSTANTE.CARACTERES_MAXIMOS;
        }

        this.isContieneDigito = function (texto) {
            return CONSTANTE.REGEX_NUMEROS.test(texto);
        }

        this.isContieneMayusculas = function (texto) {
            return CONSTANTE.REGEX_MAYUSCULA.test(texto);
        }

        this.isFecha = function (texto) {
            return CONSTANTE.REGEX_FECHA.test(texto);
        }
        //Fin del conjunto de validaciones

    }])

    app.controller("editUser", ['$scope', '$http', 'CONSTANTE', function ($scope, $http, CONSTANTE) {
        $scope.idUser = "2";


        $http.get(CONSTANTE.URL_API_USER + "/" + $scope.idUser, {
            headers: { 'Authorization': 'Basic ' + $scope.autorization }
        })
            .success(function (data) {
                console.log(data);
            })
            .error(function (data) {
                console.log(data);
            });


    }])

})();
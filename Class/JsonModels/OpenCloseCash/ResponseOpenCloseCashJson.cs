﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.OpenCloseCash
{
    public class ResponseOpenCloseCashJson
    {
        public int id { get; set; }
        public Nullable<DateTime> date { get; set; }
        public decimal? amountCash { get; set; }
        public decimal? amountDoc { get; set; }
        public decimal? amountTrans { get; set; }
        public decimal? total { get; set; }
        public int? openClose { get; set; }
        public int idCasher { get; set; }
        public int idCash { get; set; }
    }
}
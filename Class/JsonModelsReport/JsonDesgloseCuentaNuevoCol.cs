﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonDesgloseCuentaNuevoCol
    {
        public int cuenta { get; set; }
        public string descripcion { get; set; }
        private decimal tempValor { get; set; }
        private decimal tempTotal { get; set; }
        public decimal valor {
            get
            {
                return Math.Round(tempValor, 2);
            }
            set { tempValor = value; }
        }
        public decimal total
        {
            get
            {
                return Math.Round(tempTotal, 2);
            }
            set { tempTotal = value; }
        }
        

    }
}
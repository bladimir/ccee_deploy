﻿/**** PARA USARLO DECLARAR ESTA VARIABLE ****
 var moduleName = 'document';
/********************************************/
    angular.module(moduleName)
     .controller('ModalCtrl',
         function ($scope, $rootScope, $uibModal, $log) {

             $rootScope.$on("CallModelMethod", function (evt, data) {
                 return $scope.open(null, data);
             });

             $scope.open = function (size, data) {
                 var modalInstance = $uibModal.open({
                     templateUrl: 'myModalContent.html',
                     controller: 'ModalInstanceCtrl',
                     size: size,
                     resolve: {
                         dataObject: data
                     }
                 });

                 modalInstance.result.then(function () {
                     if (data.callBackMethodName != null) {
                         $scope.$emit(data.callBackMethodName, { datos: data.datos });
                     }
                 }, function () {
                     //$log.info('Modal dismissed at: ' + new Date());
                 });
             };
         });

    angular.module(moduleName)
        .controller('ModalInstanceCtrl',
            function ($scope, $uibModalInstance, $log, dataObject) {

                $scope.modalTitle = dataObject.modalTitle;
                $scope.modalContent = dataObject.modalContent;
                $scope.modalOkButtonTitle = dataObject.modalOkButtonTitle;
                $scope.modalCancelButtonTitle = dataObject.modalCancelButtonTitle;
                $scope.showCancelButton = dataObject.modalCancelShow;

                $scope.okButton = function () {
                    $uibModalInstance.close('Mensaje OK');
                };

                $scope.cancelButton = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            });

    /*******************************
    * Modal Control Example
    *******************************
    angular.module('document')
    .controller('ModalCtrl',
        function ($scope, $uibModal, $log) {

            $scope.items = ['item1', 'item2', 'item3'];
        
            $scope.open = function (size) {

                var modalInstance = $uibModal.open({
                    templateUrl: 'myModalContent.html',
                    controller: 'ModalInstanceCtrl',
                    size: size,
                    resolve: {
                        items: function () {
                            return $scope.items;
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };
    });

    angular.module('document')
        .controller('ModalInstanceCtrl',
            function ($scope, $uibModalInstance, $log, items) {

        $scope.items = items;
        $scope.selected = {
            item: $scope.items[0]
        };

        $scope.ok = function () {
            $uibModalInstance.close($scope.selected.item);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });
    ******************************************/

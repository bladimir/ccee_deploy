﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.Economic;
using webcee.Class.JsonModels.RejectedCheck;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_RejectedCheckController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_RejectedCheck
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        public ResponseRejectedCheckJson Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ResponseRejectedCheckJson payMethod = (from pay in db.ccee_MedioPago_Pago_C
                                                       where pay.MPP_NoMedioPP == id
                                                       select new ResponseRejectedCheckJson()
                                                       {
                                                           MPP_NoMedioPP = pay.MPP_NoMedioPP,
                                                           MPP_Monto = pay.MPP_Monto,
                                                           MPP_NoTransaccion = pay.MPP_NoTransaccion,
                                                           MPP_NoDocumento = pay.MPP_NoDocumento,
                                                           MPP_Reserva = pay.MPP_Reserva,
                                                           MPP_Rechazado = pay.MPP_Rechazado,
                                                           MPP_FechaHora = pay.MPP_FechaHora,
                                                           fk_MedioPago = pay.fk_MedioPago,
                                                           fk_Pago = pay.fk_Pago,
                                                           fk_Banco = pay.fk_Banco,
                                                           MPP_Observacion = pay.MPP_Observacion
                                                       }).FirstOrDefault();

                if (payMethod != null)
                {
                    if (payMethod.fk_Pago != null)
                    {
                        Economics economics = new Economics(payMethod.fk_Pago.Value, db);
                        payMethod.ecomonics = new List<ResponseEconomicRefereeJson>();
                        payMethod.ecomonics.AddRange(economics.getListReferee());
                        payMethod.ecomonics.AddRange(economics.getListTimbre());
                        payMethod.ecomonics.AddRange(economics.getListOther());
                        payMethod.payFrozen = economics.statusPay();
                    }
                }
                Autorization.saveOperation(db, autorization, "Obteniendo cheque");

                return payMethod;


            }
            catch (Exception)
            {
                return null;
            }
        }

        [Route("api/Api_RejectedCheck/{noCheck}/{idBack}")]
        // GET: api/Api_RejectedCheck/5/5
        public List<ResponseRejectedCheckJson> Get(string noCheck, int idBack)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var payMethod = ( from pay in db.ccee_MedioPago_Pago_C
                                                    where pay.fk_Banco == idBack
                                                    && pay.MPP_NoDocumento == noCheck
                                                    select new ResponseRejectedCheckJson()
                                                    {
                                                        MPP_NoMedioPP = pay.MPP_NoMedioPP,
                                                        MPP_Monto = pay.MPP_Monto,
                                                        MPP_NoTransaccion = pay.MPP_NoTransaccion,
                                                        MPP_NoDocumento = pay.MPP_NoDocumento,
                                                        MPP_Reserva = pay.MPP_Reserva,
                                                        MPP_Rechazado = pay.MPP_Rechazado,
                                                        MPP_FechaHora = pay.MPP_FechaHora,
                                                        fk_MedioPago = pay.fk_MedioPago,
                                                        fk_Pago = pay.fk_Pago,
                                                        fk_Banco = pay.fk_Banco,
                                                        MPP_Observacion = pay.MPP_Observacion
                                                    }).ToList();
                Autorization.saveOperation(db, autorization, "Obteniendo cheques por documento");

                return payMethod;


            }
            catch (Exception)
            {
                return null;
            }
            
        }

        // POST: api/Api_RejectedCheck
        public IHttpActionResult Post(RequestRejectedCheckJson check)
        {

            
            return Ok(HttpStatusCode.OK);
        }

        // PUT: api/Api_RejectedCheck/5
        public IHttpActionResult Put(int id, RequestRejectedCheckJson check)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    string autorization = Request.Headers.Authorization.Parameter;
                    if (!Autorization.enableAutorization(db, autorization)) return null;

                    ccee_MedioPago_Pago_C medio = db.ccee_MedioPago_Pago_C.Find(check.objectCheck.MPP_NoMedioPP);
                    if (medio == null)
                    {
                        return Ok(HttpStatusCode.BadRequest);
                    }

                    if (check.type == 0)
                    {
                        medio.MPP_Reserva = 0;
                        medio.MPP_Rechazado = 0;
                        db.Entry(medio).State = EntityState.Modified;
                    }
                    else if (check.type == 1)
                    {
                        medio.MPP_Reserva = 1;
                        medio.MPP_Rechazado = 0;
                        db.Entry(medio).State = EntityState.Modified;
                    }
                    else if (check.type == 2)
                    {
                        ccee_Pago tempPay = db.ccee_Pago.Find(check.objectCheck.fk_Pago);
                        if (tempPay == null)
                        {
                            transaction.Rollback();
                            return Ok(HttpStatusCode.BadRequest);
                        }

                        tempPay.Pag_Congelado = 1;
                        medio.MPP_Reserva = 2;
                        medio.MPP_Rechazado = 1;

                        db.Entry(tempPay).State = EntityState.Modified;
                        db.Entry(medio).State = EntityState.Modified;
                    }

                    db.SaveChanges();
                    Autorization.saveOperation(db, autorization, "Actualizando estado de cheque");
                    transaction.Commit();
                    return Ok(HttpStatusCode.OK);


                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return Ok(HttpStatusCode.BadRequest);
                }
            }
        }

        // DELETE: api/Api_RejectedCheck/5
        public void Delete(int id)
        {
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

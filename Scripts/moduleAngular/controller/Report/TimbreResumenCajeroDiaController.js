﻿(function () {
    angular.module('reports')
        .controller('timbreResumenCajeroDia', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'reportFactory', function ($scope, $http, CONSTANT, $anchorScroll, reportFactory) {
            $scope.direction;
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquaters;
            $scope.regexReferee = CONSTANT.REGEX_REFEREE;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.name = "";

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.identical = function (actual, expected) {
                return actual === parseInt(expected);
            }

            $scope.init = function (name) {
                $scope.name = name;
                console.log($scope.name);
                reportFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        reportFactory.setAuthorization(response.data);
                        $scope.initF();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.initF = function () {
                $scope.getCashier();
            }


            $scope.getCashier = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_CASHIER)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listCashier = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.numberOfCero = function (value) {
                return ((value < 9) ? "0" + value : value)
            }

            $scope.generateList = function () {
                var textGetReport = "";
                textGetReport = "&cashi="  + $scope.inCashi.id;
                textGetReport += "&name=" + $scope.name;
                textGetReport += "&dateI=" + ($scope.inDateI.getFullYear() + "-" + $scope.numberOfCero($scope.inDateI.getMonth() + 1) + "-" + $scope.numberOfCero($scope.inDateI.getDate())) +
                                "&dateIT=" + ($scope.numberOfCero($scope.inDateI.getDate()) + "/" + $scope.numberOfCero($scope.inDateI.getMonth() + 1) + "/" + $scope.inDateI.getFullYear() + " Cajero: " + $scope.inCashi.name) +
                                "&dateII=" + ($scope.inDateI.getDate() + "/" + ($scope.inDateI.getMonth() + 1) + "/" + $scope.inDateI.getFullYear());
                console.log(textGetReport);
                $scope.direction = "../../Reports/aspxReports/TimbreResumenCajeroDiaAspx.aspx?" + textGetReport;
            }

        }]);

})()
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Accounting
{
    public class RequestAccountingJson
    {
        public string description { get; set; }
        public string action { get; set; }
        public int idTypeAccount { get; set; }
        public string key { get; set; }
        public string name { get; set; }
        public int position { get; set; }
        public int groupAccouting { get; set; }
    }
}
﻿(function () {
    angular.module('user')
        .controller('role', ['$scope', '$http', 'CONSTANTE', '$anchorScroll', 'userFactory', function ($scope, $http, CONSTANTE, $anchorScroll, userFactory) {
            $scope.listRoles;
            $scope.listOperations;
            $scope.listRolOperations;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANTE.NOTIFY_SHOW_CSS;
            $scope.idRolUpdate = 0;
            $scope.idRolOperation = 0;
            $scope.showOperations = false;


            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANTE.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANTE.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                userFactory.actionGetFunction(CONSTANTE.URL_API_ROLE)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listRoles = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });

                userFactory.actionGetFunction(CONSTANTE.URL_API_OPERATION)
                    .then(function (response) {
                        console.log(response);
                        $scope.listOperations = response.data;
                    }, function (error) {
                        console.log(error);
                    });
            }


            $scope.setAuth = function () {
                userFactory.actionGetFunction(CONSTANTE.URL_GET_TOKEN)
                    .then(function (response) {
                        userFactory.setAuthorization(response.data);
                        $scope.init();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.getRol = function () {
                $scope.showLoadingFunction();
                userFactory.actionGetFunction(CONSTANTE.URL_API_ROLE)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listRoles = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.addRol = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.rolName
                }
                console.log(information);
                userFactory.actionPostFunction(CONSTANTE.URL_API_ROLE, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getRol();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }


            $scope.showDataUpdate = function (type) {
                $scope.idRolUpdate = type.id;
                $scope.rolName = type.name;
            }

            $scope.updateRol= function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.rolName
                }
                userFactory.actionPutFunction(CONSTANTE.URL_API_ROLE, $scope.idRolUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.getRol();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }


            $scope.deleteRol = function (type) {
                $scope.showLoadingFunction();
                userFactory.actionDeleteFunction(CONSTANTE.URL_API_ROLE, type.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getRol();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }


            $scope.getRolOper = function (id) {
                $scope.showLoadingFunction();
                userFactory.actionGetIntFunction(CONSTANTE.URL_API_ROLOPERATION, id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listRolOperations = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.addRolOper = function () {
                $scope.showLoadingFunction();
                var information = {
                    idOper: $scope.selectOperations.id,
                    idRol: $scope.idRolOperation
                }
                console.log(information);
                userFactory.actionPostFunction(CONSTANTE.URL_API_ROLOPERATION, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getRolOper($scope.idRolOperation);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.deleteRolOper = function (type) {
                $scope.showLoadingFunction();
                userFactory.actionDeleteTwoFunction(CONSTANTE.URL_API_ROLOPERATION, type.idRol, type.idOper)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getRolOper($scope.idRolOperation);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idRolUpdate = 0;
                $scope.rolName = "";
            }

            $scope.selectOperation = function (type) {
                $scope.idRolOperation = type.id;
                $scope.nameRolOperation = type.name;
                $scope.getRolOper(type.id);
                $scope.showOperations = true;
            }


        }]);

})()
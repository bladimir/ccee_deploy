﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.ListGeneral
{
    public class ResponseListGeneralJson
    {
        public int? Col_NoColegiado { get; set; }
        public string Col_PrimerApellido { get; set; }
        public string Col_SegundoApellido { get; set; }
        public string Col_CasdaApellido { get; set; }
        public string Col_PrimerNombre { get; set; }
        public string Col_SegundoNombre { get; set; }
        public string Col_TercerNombre { get; set; }
        public string nacimiento { get; set; }
        public DateTime? Col_FechaNacimiento { get; set; }
        public DateTime? Col_FechaColegiacion { get; set; }
        public string activo { get; set; }
        public string sexo { get; set; }
        public int? fk_Sede { get; set; }
        public string fk_Sede_Nombre { get; set; }
        public int? fk_SedeRegistrada { get; set; }
        public string fk_SedeRegistrada_Nombre { get; set; }
        public string Tco_Descripcion { get; set; }
        public string estadoCivil { get; set; }
        public string Dir_TipoDireccion { get; set; }
        public string direccion { get; set; }
        public string TEs_NombreTipoEsp { get; set; }
        public string Uni_NombreUniversidad { get; set; }
        public string Esp_NombreEspecialidad { get; set; }
        public string Esp_Tema_Graduacion { get; set; }
        public string Esp_Evaluacion { get; set; }
        public string phones { get; set; }
        public string mails { get; set; }
        public DateTime? Esp_FechaObtuvo { get; set; }
        public string Uni_NombreUniversidadabre { get; set; }
        public string Col_Dpi { get; set; }
        public DateTime? Col_FechaFallecimiento { get; set; }
        public DateTime? Col_FechaJuramentacion { get; set; }
        public DateTime? Col_jubilado { get; set; }
        public int edad { get; set; }
        public string Col_Nacionalidad { get; set; }
        public string Col_DpiExtendido { get; set; }
        public string Col_Experiencia_en { get; set; }
        public string TES_Abreviacion { get; set; }
        public string idiomas { get; set; }
        public string Esp_Titulo { get; set; }
    }
}
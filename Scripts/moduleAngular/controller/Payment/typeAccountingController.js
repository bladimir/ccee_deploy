﻿(function () {
    angular.module('payment')
        .controller('typeAccounting', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'paymentFactory', function ($scope, $http, CONSTANT, $anchorScroll, paymentFactory) {
            $scope.listTypeAccounting;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idTypeAccountingUpdate = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                paymentFactory.actionGetFunction(CONSTANT.URL_API_TYPEACCOUNTING)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listTypeAccounting = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.setAuth = function () {
                paymentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        paymentFactory.setAuthorization(response.data);
                        $scope.init();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.getTypeAccounting = function () {
                $scope.showLoadingFunction();
                paymentFactory.actionGetFunction(CONSTANT.URL_API_TYPEACCOUNTING)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listTypeAccounting = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.addTypeAccounting = function () {
                $scope.showLoadingFunction();
                var information = {
                    description: $scope.accountdescription,
                }

                console.log(information);
                paymentFactory.actionPostFunction(CONSTANT.URL_API_TYPEACCOUNTING, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.cleanData();
                                $scope.getTypeAccounting();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (type) {
                $scope.idTypeAccountingUpdate = type.id;
                $scope.accountdescription = type.description;
                $anchorScroll();
            }

            $scope.updateTypeAccounting = function () {
                $scope.showLoadingFunction();
                var information = {
                    description: $scope.accountdescription
                }
                paymentFactory.actionPutFunction(CONSTANT.URL_API_TYPEACCOUNTING, $scope.idTypeAccountingUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.getTypeAccounting();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }


            $scope.deleteTypeAccounting = function (type) {
                $scope.showLoadingFunction();
                paymentFactory.actionDeleteFunction(CONSTANT.URL_API_TYPEACCOUNTING, type.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getTypeAccounting();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idTypeAccountingUpdate = 0;
                $scope.accountdescription = "";
            }


        }]);

})()
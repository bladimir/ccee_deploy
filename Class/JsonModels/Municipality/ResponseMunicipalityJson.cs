﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Municipality
{
    public class ResponseMunicipalityJson
    {
        public int id { get; set; }
        public string name { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Academy
{
    public class RequestAcademyJson
    {
        public int specialty { get; set; }
        public int university { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public int idReferee { get; set; }
        public DateTime? date { get; set; }
        public string evaluation { get; set; }
        public string graduationSubject { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.DepositSlit
{
    public class RequestDepositSlitListJson
    {
        public List<ResponseDepositSlitNullJson> listNulls { get; set; }
        public List<ResponseDepositSlitNullJson> listNoNulls { get; set; }
    }
}
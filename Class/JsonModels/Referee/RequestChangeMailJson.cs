﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Referee
{
    public class RequestChangeMailJson
    {
        public int idReferee { get; set; }
        public string newPassword { get; set; }
    }
}
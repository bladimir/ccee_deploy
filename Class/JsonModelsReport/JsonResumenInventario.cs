﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonResumenInventario
    {
        public int idDenominacion { get; set; }
        public int pliegos { get; set; }
        public int unidades { get; set; }
        public decimal denominaciones { get; set; }
        public decimal totalTimbre { get; set; }
        public string serie { get; set; }
        public int sueltos { get; set; }
        public decimal totalSum
        {
            get
            {
                return unidades * denominaciones;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.PaymentMethod;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_PaymentMethodController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_PaymentMethod
        public IEnumerable<ResponsePaymentMethodJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempPaymentMethod = from med in db.ccee_MedioPago
                                  select new ResponsePaymentMethodJson()
                                  {
                                      id = med.MedP_NoMedioPago,
                                      description = med.MdP_Descripcion,
                                      typeMethod = med.MdP_TipoMedio
                                  };
                Autorization.saveOperation(db, autorization, "Obteniendo medio de pago");
                return tempPaymentMethod;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_PaymentMethod/5
        public IEnumerable<ResponsePaymentMethodJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempPaymentMethod = from med in db.ccee_MedioPago
                                        where med.fk_BancoEmisor == id
                                        select new ResponsePaymentMethodJson()
                                        {
                                            id = med.MedP_NoMedioPago,
                                            description = med.MdP_Descripcion,
                                            typeMethod = med.MdP_TipoMedio
                                        };
                Autorization.saveOperation(db, autorization, "Obteniendo medio de pago por banco");
                return tempPaymentMethod;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_PaymentMethod
        public IHttpActionResult Post(RequestPaymentMethodJson paymentMethod)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_MedioPago tempPaymentMethod = new ccee_MedioPago()
                {
                    MdP_TipoMedio = paymentMethod.typeMethod,
                    MdP_Descripcion = paymentMethod.description,
                    fk_BancoEmisor = paymentMethod.idIssuingBank
                };

                db.ccee_MedioPago.Add(tempPaymentMethod);
                Autorization.saveOperation(db, autorization, "Agregando medio de pago");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_PaymentMethod/5
        public IHttpActionResult Put(int id, RequestPaymentMethodJson paymentMethod)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_MedioPago tempPaymentMethod = db.ccee_MedioPago.Find(id);

                if (tempPaymentMethod == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempPaymentMethod.MdP_TipoMedio = paymentMethod.typeMethod;
                tempPaymentMethod.MdP_Descripcion = paymentMethod.description;

                db.Entry(tempPaymentMethod).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando medio de pago");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_PaymentMethod/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_MedioPago tempPaymentMethod = db.ccee_MedioPago.Find(id);
                if (tempPaymentMethod == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_MedioPago.Remove(tempPaymentMethod);
                Autorization.saveOperation(db, autorization, "Eliminando medio de pago");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}

﻿(function () {
    angular.module('payment')
        .controller('issuingBank', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'paymentFactory', function ($scope, $http, CONSTANT, $anchorScroll, paymentFactory) {
            $scope.listData;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idUpdate = 0;
            $scope.objectUpdate;
            $scope.REGEX_PHONE = CONSTANT.REGEX_PHONE;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                paymentFactory.actionGetFunction(CONSTANT.URL_API_ISSUINGBANK)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listData = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.setAuth = function () {
                paymentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        paymentFactory.setAuthorization(response.data);
                        $scope.init();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            


            $scope.get = function () {
                $scope.showLoadingFunction();
                paymentFactory.actionGetFunction(CONSTANT.URL_API_ISSUINGBANK)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listData = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.add = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.issuingname,
                    address: $scope.issuingaddress,
                    phone: $scope.issuingphone,
                    service: $scope.issuingservice,
                    typeInstitution: $scope.issuingintitutionselect
                }
                console.log(information);
                paymentFactory.actionPostFunction(CONSTANT.URL_API_ISSUINGBANK, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.get();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (data) {
                $scope.objectUpdate = data;
                $scope.idUpdate = data.id;
                $scope.issuingname = data.name;
                $scope.issuingaddress = data.address;
                $scope.issuingphone = data.phone;
                $scope.issuingservice = data.service;
                $scope.issuingintitution = data.typeInstitution;
                $anchorScroll();
            }

            $scope.update = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.issuingname,
                    address: $scope.issuingaddress,
                    phone: $scope.issuingphone,
                    service: $scope.issuingservice,
                    typeInstitution: (($scope.issuingintitutionselect == null) ? $scope.objectUpdate.typeInstitution : $scope.issuingintitutionselect)
                }
                paymentFactory.actionPutFunction(CONSTANT.URL_API_ISSUINGBANK, $scope.idUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.get();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.delete = function (data) {
                $scope.showLoadingFunction();
                paymentFactory.actionDeleteFunction(CONSTANT.URL_API_ISSUINGBANK, data.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.get();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idUpdate = 0;
                $scope.issuingname = "";
                $scope.issuingaddress = "";
                $scope.issuingphone = "";
                $scope.issuingservice = "";
                $scope.issuingintitution = "";
            }

            $scope.defineInstitution = function (data) {
                if (data == 0) {
                    return "Banco";
                } else if (data == 1) {
                    return "Tarjeta";
                } else if (data == 2) {
                    return "Cooperativa";
                } else if (data == 3) {
                    return "Otro";
                } else {
                    return "No definido";
                }
            }


        }]);

})()
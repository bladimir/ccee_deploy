﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.FileRequirement
{
    public class RequestFileRequirementJson
    {
        public string name { get; set; }
        public Nullable<int> required { get; set; }
        public int idFile { get; set; }
        public int? initFile { get; set; }
    }
}
﻿(function () {
    angular.module('reports')
        .controller('timbreIngresoBodega', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'reportFactory', function ($scope, $http, CONSTANT, $anchorScroll, reportFactory) {
            $scope.direction;
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquaters;
            $scope.regexReferee = CONSTANT.REGEX_REFEREE;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.name = "";

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.identical = function (actual, expected) {
                return actual === parseInt(expected);
            }

            $scope.init = function (name) {
                $scope.name = name;
                console.log($scope.name);
                reportFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        reportFactory.setAuthorization(response.data);
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.numberOfCero = function (value) {
                return ((value < 9) ? "0" + value : value)
            }

            $scope.generateList = function () {
                var textGetReport = "";
                console.log(($scope.selectHead != null) ? $scope.selectHead.id : "");
                textGetReport = "&cashi=";
                textGetReport += "&name=" + $scope.name;
                textGetReport += "&dateI=" + ($scope.inDateI.getFullYear() + "-" + $scope.numberOfCero($scope.inDateI.getMonth() + 1) + "-" + $scope.numberOfCero($scope.inDateI.getDate())) +
                                 "&dateF=" + ($scope.inDateF.getFullYear() + "-" + $scope.numberOfCero($scope.inDateF.getMonth() + 1) + "-" + $scope.numberOfCero($scope.inDateF.getDate())) +
                                "&dateII=" + ($scope.inDateI.getDate() + "/" + ($scope.inDateI.getMonth() + 1) + "/" + $scope.inDateI.getFullYear());
                console.log(textGetReport);
                $scope.direction = "../../Reports/aspxReports/TimbreIngresoBodega.aspx?" + textGetReport;
            }

        }]);

})()
/// <autosync enabled="true" />
/// <reference path="angular.min.js" />
/// <reference path="angular/angular.min.js" />
/// <reference path="angular-animate.js" />
/// <reference path="angular-animate/angular-animate.js" />
/// <reference path="angular-aria.js" />
/// <reference path="angular-aria/angular-aria.js" />
/// <reference path="angular-base64.min.js" />
/// <reference path="angular-base64-upload.min.js" />
/// <reference path="angular-cookies.js" />
/// <reference path="angular-loader.js" />
/// <reference path="angular-material/angular-material.js" />
/// <reference path="angular-material/angular-material-mocks.js" />
/// <reference path="angular-message-format.js" />
/// <reference path="angular-messages.js" />
/// <reference path="angular-mocks.js" />
/// <reference path="angular-parse-ext.js" />
/// <reference path="angular-recaptcha.min.js" />
/// <reference path="angular-resource.js" />
/// <reference path="angular-route.js" />
/// <reference path="angular-sanitize.js" />
/// <reference path="angular-scenario.js" />
/// <reference path="angular-touch.js" />
/// <reference path="bootstrap.js" />
/// <reference path="bootstrap-datetimepicker.js" />
/// <reference path="bootstrap-select.js" />
/// <reference path="FileAPI.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="moduleAngular/account.js" />
/// <reference path="moduleAngular/app.js" />
/// <reference path="moduleAngular/constant/AccountConstant.js" />
/// <reference path="moduleAngular/constant/DocumentConstant.js" />
/// <reference path="moduleAngular/constant/EventConstant.js" />
/// <reference path="moduleAngular/constant/GeneralActionsConstant.js" />
/// <reference path="moduleAngular/constant/PaymentConstant.js" />
/// <reference path="moduleAngular/constant/RefereeConstant.js" />
/// <reference path="moduleAngular/constant/ReportConstant.js" />
/// <reference path="moduleAngular/constant/UserConstant.js" />
/// <reference path="moduleangular/controller/document/inboxtimbrecontroller.js" />
/// <reference path="moduleangular/controller/document/listbatchcontroller.js" />
/// <reference path="moduleAngular/controller/Document/typeDocumentAccountingController.js" />
/// <reference path="moduleAngular/controller/Document/typeDocumentController.js" />
/// <reference path="moduleAngular/controller/Document/uploadNominaController.js" />
/// <reference path="moduleangular/controller/document/usercertificacioncontroller.js" />
/// <reference path="moduleangular/controller/document/userpcontroller.js" />
/// <reference path="moduleangular/controller/document/usertimbrecontroller.js" />
/// <reference path="moduleAngular/controller/Document/ValidarDocumentoController.js" />
/// <reference path="moduleangular/controller/generalactions/banckaccountscontroller.js" />
/// <reference path="moduleAngular/controller/GeneralActions/cashController.js" />
/// <reference path="moduleAngular/controller/GeneralActions/cellarController.js" />
/// <reference path="moduleAngular/controller/GeneralActions/countryController.js" />
/// <reference path="moduleAngular/controller/GeneralActions/departamentController.js" />
/// <reference path="moduleangular/controller/generalactions/diplomanewrefereecontroller.js" />
/// <reference path="moduleangular/controller/generalactions/documentforreceiptcontroller.js" />
/// <reference path="moduleangular/controller/generalactions/globalvarscontroller.js" />
/// <reference path="moduleAngular/controller/GeneralActions/headquartersController.js" />
/// <reference path="moduleAngular/controller/GeneralActions/languageController.js" />
/// <reference path="moduleangular/controller/generalactions/listeddocumentscontroller.js" />
/// <reference path="moduleAngular/controller/GeneralActions/municipalityController.js" />
/// <reference path="moduleAngular/controller/GeneralActions/openCloseCashController.js" />
/// <reference path="moduleAngular/controller/GeneralActions/paymentCenterController.js" />
/// <reference path="moduleangular/controller/generalactions/receiptpayrefereecontroller.js" />
/// <reference path="moduleangular/controller/generalactions/refereereceiptcontroller.js" />
/// <reference path="moduleAngular/controller/GeneralActions/rejectedCheckController.js" />
/// <reference path="moduleangular/controller/generalactions/testdocumentcontroller.js" />
/// <reference path="moduleangular/controller/generalactions/timbreschangecontroller.js" />
/// <reference path="moduleAngular/controller/GeneralActions/timbresOfDemandController.js" />
/// <reference path="moduleAngular/controller/GeneralActions/typeBeneficiaryController.js" />
/// <reference path="moduleAngular/controller/GeneralActions/typeReferee.js" />
/// <reference path="moduleAngular/controller/GeneralActions/typeSpecialityController.js" />
/// <reference path="moduleAngular/controller/GeneralActions/universityController.js" />
/// <reference path="moduleangular/controller/generalactions/valuetimbrecontroller.js" />
/// <reference path="moduleAngular/controller/Payment/accountingController.js" />
/// <reference path="moduleAngular/controller/Payment/cashierController.js" />
/// <reference path="moduleAngular/controller/Payment/depositSlipController.js" />
/// <reference path="moduleAngular/controller/Payment/economicController.js" />
/// <reference path="moduleangular/controller/payment/economicnotreferee.js" />
/// <reference path="moduleangular/controller/payment/incompletepaymentcontroller.js" />
/// <reference path="moduleAngular/controller/Payment/issuingBankController.js" />
/// <reference path="moduleAngular/controller/Payment/memberEconomicController.js" />
/// <reference path="moduleAngular/controller/Payment/payController.js" />
/// <reference path="moduleAngular/controller/Payment/paymentMethodController.js" />
/// <reference path="moduleAngular/controller/Payment/paymentMethodPayController.js" />
/// <reference path="moduleAngular/controller/Payment/receiptController.js" />
/// <reference path="moduleangular/controller/payment/recoverreceiptcontroller.js" />
/// <reference path="moduleAngular/controller/Payment/typeAccountingController.js" />
/// <reference path="moduleAngular/controller/Payment/typeEconomicController.js" />
/// <reference path="moduleAngular/controller/Referee/addReferee.js" />
/// <reference path="moduleangular/controller/referee/changeallrefereecontroller.js" />
/// <reference path="moduleangular/controller/referee/eliminarrefereecontroller.js" />
/// <reference path="moduleAngular/controller/Referee/fileReferee.js" />
/// <reference path="moduleangular/controller/referee/notrefereecontroller.js" />
/// <reference path="moduleangular/controller/referee/searchrefereecontroller.js" />
/// <reference path="moduleangular/controller/report/asambleacontroller.js" />
/// <reference path="moduleangular/controller/report/calculoactuariocontroller.js" />
/// <reference path="moduleAngular/controller/Report/CalidadEstatusSaldoController.js" />
/// <reference path="moduleAngular/controller/Report/calidadyEstatusSaldosController.js" />
/// <reference path="moduleangular/controller/report/carnetcontroller.js" />
/// <reference path="moduleangular/controller/report/certificacioncontroller.js" />
/// <reference path="moduleAngular/controller/Report/ChequesRechazadosController.js" />
/// <reference path="moduleAngular/controller/Report/ConsolidadoDeudaController.js" />
/// <reference path="moduleAngular/controller/Report/CorteCajaController.js" />
/// <reference path="moduleAngular/controller/Report/CorteCajaRangoController.js" />
/// <reference path="moduleAngular/controller/Report/DesgloseCuentaNColegiadoController.js" />
/// <reference path="moduleAngular/controller/Report/DesgloseCuentasController.js" />
/// <reference path="moduleangular/controller/report/directoriocontroller.js" />
/// <reference path="moduleAngular/controller/Report/documentosPendientesController.js" />
/// <reference path="moduleAngular/controller/Report/EstadisticaPieController.js" />
/// <reference path="moduleAngular/controller/Report/EstadoCuentaColegiadoController.js" />
/// <reference path="moduleangular/controller/report/estadocuentaconrecibocolcontroller.js" />
/// <reference path="moduleAngular/controller/Report/EtiquetaController.js" />
/// <reference path="moduleAngular/controller/Report/FichaColegiadoController.js" />
/// <reference path="moduleangular/controller/report/ficharesumidacontroller.js" />
/// <reference path="moduleAngular/controller/Report/graphicsController.js" />
/// <reference path="moduleangular/controller/report/historialusuariocontroller.js" />
/// <reference path="moduleangular/controller/report/historicoactivocontroller.js" />
/// <reference path="moduleAngular/controller/Report/ListadoGeneralColController.js" />
/// <reference path="moduleangular/controller/report/listadogeneralcolfiltrocontroller.js" />
/// <reference path="moduleAngular/controller/Report/listRefereeController.js" />
/// <reference path="moduleAngular/controller/Report/PadronAsambleaController.js" />
/// <reference path="moduleAngular/controller/Report/RecibosOperadosController.js" />
/// <reference path="moduleAngular/controller/Report/ReporteColegiado.js" />
/// <reference path="moduleAngular/controller/Report/ReporteIngresosController.js" />
/// <reference path="moduleAngular/controller/Report/SaldoColegiadoController.js" />
/// <reference path="moduleangular/controller/report/tesiscontroller.js" />
/// <reference path="moduleangular/controller/report/timbrebodegacontroller.js" />
/// <reference path="moduleangular/controller/report/timbrecontroller.js" />
/// <reference path="moduleangular/controller/report/timbreentregabodegacajcontroller.js" />
/// <reference path="moduleangular/controller/report/timbreingresobodegacontroller.js" />
/// <reference path="moduleangular/controller/report/timbreresumencajerodiacontroller.js" />
/// <reference path="moduleangular/controller/report/timbreresumeninventariocontroller.js" />
/// <reference path="moduleangular/controller/report/timbrescajerocontroller.js" />
/// <reference path="moduleangular/controller/report/timbresentradasalidacontroller.js" />
/// <reference path="moduleAngular/controller/User/fileController.js" />
/// <reference path="moduleangular/controller/user/profilecontroller.js" />
/// <reference path="moduleAngular/controller/User/roleController.js" />
/// <reference path="moduleAngular/controller/User/userCashController.js" />
/// <reference path="moduleAngular/document.js" />
/// <reference path="moduleAngular/event.js" />
/// <reference path="moduleAngular/fatories/Document/documentFactory.js" />
/// <reference path="moduleAngular/fatories/GeneralActions/generalactionsFactory.js" />
/// <reference path="moduleAngular/fatories/Payment/comunicationFactory.js" />
/// <reference path="moduleAngular/fatories/Payment/paymentFactory.js" />
/// <reference path="moduleAngular/fatories/Referee/refereeFactory.js" />
/// <reference path="moduleAngular/fatories/Referee/refereeFactoryDB.js" />
/// <reference path="moduleAngular/fatories/Report/reportFactory.js" />
/// <reference path="moduleAngular/fatories/Report/reportFunctionFactory.js" />
/// <reference path="moduleAngular/fatories/User/userFactory.js" />
/// <reference path="moduleAngular/generalactions.js" />
/// <reference path="moduleangular/modals/documentmodal.js" />
/// <reference path="moduleAngular/payment.js" />
/// <reference path="moduleAngular/referee.js" />
/// <reference path="moduleAngular/report.js" />
/// <reference path="moduleAngular/user.js" />
/// <reference path="moduleScripts/interfaceDatePiker.js" />
/// <reference path="moment.js" />
/// <reference path="moment-with-locales.js" />
/// <reference path="ngclipboard.min.js" />
/// <reference path="ng-file-upload.js" />
/// <reference path="ng-file-upload-all.js" />
/// <reference path="ng-file-upload-shim.js" />
/// <reference path="powerbi.js" />
/// <reference path="respond.js" />
/// <reference path="ui-bootstrap-tpls-1.3.3.js" />

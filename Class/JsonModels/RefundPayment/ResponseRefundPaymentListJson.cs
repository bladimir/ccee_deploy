﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.RefundPayment
{
    public class ResponseRefundPaymentListJson
    {
        public int id { get; set; }
        public string document { get; set; }
        public string transaction { get; set; }
        public decimal? amount { get; set; }
        public string description { get; set; }
        public string bank { get; set; }
    }
}
﻿(function () {
    angular.module('referee')
        .controller('fileReferee', ['$scope', '$http', '$location', 'CONSTANT', '$anchorScroll', '$window', 'refereeFactory', function ($scope, $http, $location, CONSTANT, $anchorScroll, $window, refereeFactory) {

            var idReferee = $location.search().id;
            $scope.listFile;
            $scope.listOperations;
            $scope.listFileRequirement;
            $scope.showDelete = false;
            $scope.showDeleteRequire = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idFileUpdate = 0;
            $scope.idFileRequireFile = 0;
            $scope.idFileRequireFileUpdate = 0;
            console.log("id Referee: " + idReferee);
            $scope.showDocReq = 0;

            
            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.setAuth = function () {
                refereeFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    refereeFactory.setAuthorization(response.data);
                    console.log(refereeFactory.getAuthorization());
                    $scope.getFiles();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            
            $scope.getFiles = function () {
                $scope.showLoadingFunction();
                refereeFactory.actionGetFunction(CONSTANT.URL_API_FILE)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listFile = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getDocExp = function (idDoc) {
                $scope.showLoadingFunction();
                refereeFactory.actionGetGetIntFunction(CONSTANT.URL_API_DOCFILE, idReferee, idDoc)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listFileExp = response.data;
                        $scope.showDocReq = 1;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.donwloadFile = function (dir, external) {
                if (external == null) {
                    $window.open('/' + dir);
                } else {
                    $window.open(dir);
                }
            }



            $scope.editFile = function (idDoc) {
                $window.open('/Referre/EditFiles/' + idReferee + '/' + idDoc);
            }


        }]);

})()
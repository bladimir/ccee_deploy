﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonDesgloseCuentaPartida
    {
        public int tipo { get; set; }
        public string nombreTipo { get; set; }
        public string descripcion { get; set; }
        public string condicion { get; set; }
        public string cuenta { get; set; }
        public int? posicion { get; set; }
        public int? agrupar { get; set; }
        public int id { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.Cash;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_CashController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Cash
        public IEnumerable<ResponseCashJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempCash = from caj in db.ccee_Caja
                                  select new ResponseCashJson()
                                  {
                                      id = caj.Cja_NoCaja,
                                      name = caj.Cja_Nombre,
                                      activo = caj.Cja_Activo
                                  };
                Autorization.saveOperation(db, autorization, "Obteniendo cajas");
                return tempCash;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_Cash/5
        public IEnumerable<ResponseCashJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempCash = from caj in db.ccee_Caja
                                  where caj.fk_CentroPago == id
                                  select new ResponseCashJson()
                                  {
                                      id = caj.Cja_NoCaja,
                                      name = caj.Cja_Nombre,
                                      activo = caj.Cja_Activo
                                  };
                Autorization.saveOperation(db, autorization, "Obteniendo caja");
                return tempCash;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_Cash
        public IHttpActionResult Post(RequestCashJson cash)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Caja tempCash = new ccee_Caja()
                {
                    Cja_Nombre = cash.name,
                    Cja_Activo = cash.activo,
                    fk_CentroPago = cash.fkPaymentCenter
                };

                db.ccee_Caja.Add(tempCash);
                Autorization.saveOperation(db, autorization, "Agregando caja");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_Cash/5
        public IHttpActionResult Put(int id, RequestCashJson cash)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Caja tempCash = db.ccee_Caja.Find(id);

                if (tempCash == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempCash.Cja_Nombre = cash.name;
                tempCash.Cja_Activo = cash.activo;

                db.Entry(tempCash).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando caja");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_Cash/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Caja tempCash = db.ccee_Caja.Find(id);
                if (tempCash == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_Caja.Remove(tempCash);
                Autorization.saveOperation(db, autorization, "Eliminando caja");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}

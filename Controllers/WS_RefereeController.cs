﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Models;

namespace webcee.Controllers
{
    public class WS_RefereeController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        public class temInfo
        {
            public string primerNombre { get; set; }
            public string segundoNombre { get; set; }
            public string tercerNombre { get; set; }
            public string primerApellido { get; set; }
            public string segundoApellido { get; set; }
            public string apellidoCasada { get; set; }
            public string correo { get; set; }
            public string calidad { get; set; }
            public string profesion { get; set; }
        }

        // GET: api/WS_Referee
        public IEnumerable<string> Get()
        {
            
            return new string[] { "value1", "value2" };
        }

        // GET: api/WS_Referee/5
        public temInfo Get(int id)
        {
            try
            {
                var referee = (from col in db.ccee_Colegiado
                               join tip in db.ccee_TipoColegiado on col.fk_TipoColegiado equals tip.Tco_NoTipoColegiado
                               where col.Col_NoColegiado == id
                               select new
                               {
                                   fistName = col.Col_PrimerNombre,
                                   secondName = col.Col_SegundoNombre,
                                   fistLastName = col.Col_PrimerApellido,
                                   secondLastName = col.Col_SegundoApellido,
                                   thirdName = col.Col_TercerNombre,
                                   marrigName = col.Col_CasdaApellido,
                                   calidad = tip.Tco_Descripcion,
                                   status = col.Col_Estatus
                               }).FirstOrDefault();
                if(referee == null)
                {
                    return null;
                }

                var email = (from mai in db.ccee_Mail
                             where mai.fk_Colegiado == id
                             select new {
                                 email = mai.Mai_MailDir
                             }).FirstOrDefault();

                var prof = (from esp in db.ccee_Especialidad
                            join tes in db.ccee_TipoEspecialidad on esp.pkfk_TipoEspecialidad equals tes.TEs_NoTipoEspecialidad
                            where esp.pkfk_Colegiado == id
                            select new
                            {
                                especiality = tes.TEs_NombreTipoEsp
                            }).FirstOrDefault();

                temInfo tem = new temInfo();
                tem.primerNombre = referee.fistName;
                tem.segundoNombre = referee.secondName;
                tem.tercerNombre = referee.thirdName;
                tem.primerApellido = referee.fistLastName;
                tem.segundoApellido = referee.secondLastName;
                tem.apellidoCasada = referee.marrigName;
                tem.correo = ((email == null) ? "" : email.email);
                tem.calidad = ((referee.status == 1) ? "Activo" : "Inactivo");
                tem.profesion = ((prof == null) ? "" : prof.especiality);
                return tem;


            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/WS_Referee
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/WS_Referee/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/WS_Referee/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿(function () {
    angular.module('generals')
        .controller('banckAccount', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listBank;
            $scope.list;
            $scope.idBanck = 0;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idUpdate = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_ISSUINGBANK)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listBank = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }


            $scope.getBankAccount = function () {
                $scope.idBanck = $scope.selectBank.id;
                $scope.get($scope.idBanck);
            }


            $scope.get = function (id) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_BANKACCOUNT, id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.list = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.add = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.nameBankAccount,
                    codigo: $scope.codBankAccount,
                    format: $scope.formatBankAccount,
                    NoAccount: $scope.numberBankAccount,
                    idBank: $scope.idBanck
                }
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_BANKACCOUNT, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.get($scope.idBanck);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (data) {
                $scope.idUpdate = data.id;
                $scope.numberBankAccount = data.NoAccount;
                $scope.codBankAccount = data.codigo;
                $scope.formatBankAccount = data.format;
                $scope.nameBankAccount = data.name;
                $anchorScroll();
            }

            $scope.update = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.nameBankAccount,
                    codigo: $scope.codBankAccount,
                    format: $scope.formatBankAccount,
                    NoAccount: $scope.numberBankAccount
                }
                generalactionsFactory.actionPutFunction(CONSTANT.URL_API_BANKACCOUNT, $scope.idUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.get($scope.idBanck);
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.delete = function (mun) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionDeleteFunction(CONSTANT.URL_API_BANKACCOUNT, mun.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.get($scope.idBanck);
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idUpdate = 0;
                $scope.numberBankAccount = "";
                $scope.codBankAccount = "";
                $scope.formatBankAccount = "";
                $scope.nameBankAccount = "";
            }


        }]);

})()
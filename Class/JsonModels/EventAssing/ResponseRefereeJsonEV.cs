﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.EventAssing
{
    public class ResponseRefereeJsonEV
    {
        public string name { get; set; }
        public string lastname { get; set; }
        public int? status { get; set; }
        public int statusAssign { get; set; }
    }
}
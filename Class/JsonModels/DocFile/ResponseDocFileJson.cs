﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.DocFile
{
    public class ResponseDocFileJson
    {
        public int id { get; set; }
        public string nameFile { get; set; }
        public string nameFileRequirement { get; set; }
        public int stateFileRequirement { get; set; }
        public string document { get; set; }
        public int stateDocument { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.DocFile;
using webcee.Models;
using webcee.ViewModels;

namespace webcee.Controllers
{
    public class Api_DocFileController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_DocFile
        public IEnumerable<ResponseDocFileJson> Get()
        {
            return null;
        }

        // GET: api/Api_DocFile/5
        public IEnumerable<ResponseDocFileJson> Get(int id)
        {
            string autorization = Request.Headers.Authorization.Parameter;
            if (!Autorization.enableAutorization(db, autorization)) return null;

            var tempDocFile = from doc in db.ccee_ExpedienteRequisito
                              join exp in db.ccee_Expediente on doc.fk_Expediente equals exp.Exp_NoExpediente
                              select new ResponseDocFileJson()
                              {
                                  id = doc.ERe_NoExpedienteRequisito,
                                  nameFile = exp.Exp_NombreExpediente,
                                  nameFileRequirement = doc.ERe_NombreRequisito,
                                  stateFileRequirement = doc.ERe_Obligatorio.Value
                              };
            List<ResponseDocFileJson> listFile = tempDocFile.ToList();

            for (int i = 0; i < listFile.Count; i++)
            {
                var listDocEx = from exp in db.ccee_DocExpRequisito
                                where exp.fk_ExpReq == listFile[i].id && exp.fk_Colegiado == id
                                select exp;

                foreach(ccee_DocExpRequisito temp in listDocEx)
                {
                    listFile[i].document = temp.DER_NombreDocuemento;
                    listFile[i].stateDocument = temp.DER_Entregado.Value;
                }

            }
            Autorization.saveOperation(db, autorization, "Obteniendo documentos expediente");

            return listFile;
        }

        [Route("api/Api_DocFile/{idReferee}/{idDoc}")]
        public IEnumerable<FileDocumentModelView> Get(int idReferee, int idDoc)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                List<FileDocumentModelView> listFile = (from doc in db.ccee_ExpedienteRequisito
                                  join exp in db.ccee_Expediente on doc.fk_Expediente equals exp.Exp_NoExpediente
                                  where exp.Exp_NoExpediente == idDoc
                                  select new FileDocumentModelView()
                                  {
                                      id = doc.ERe_NoExpedienteRequisito,
                                      nameFile = exp.Exp_NombreExpediente,
                                      nameFileRequirement = doc.ERe_NombreRequisito,
                                      stateFileRequirement = doc.ERe_Obligatorio.Value
                                  }).ToList();

                for (int i = 0; i < listFile.Count; i++)
                {
                    listFile[i].stateFileRequirementText = convertStatusFile(listFile[i].stateFileRequirement);
                    int fkExp = listFile[i].id;
                    var listDocEx = from exp in db.ccee_DocExpRequisito
                                    where exp.fk_ExpReq == fkExp && exp.fk_Colegiado == idReferee
                                    select exp;

                    if (listDocEx != null)
                    {

                        foreach (ccee_DocExpRequisito temp in listDocEx)
                        {
                            if(temp.DER_Externo == null)
                            {
                                listFile[i].document = (temp.DER_NombreDocuemento == null || temp.DER_NombreDocuemento == "") ? "" : Constant.url_dir_documents_angular + temp.DER_NombreDocuemento;
                            }else
                            {
                                listFile[i].document = (temp.DER_NombreDocuemento == null || temp.DER_NombreDocuemento == "") ? "" : temp.DER_NombreDocuemento;
                            }
                            
                            listFile[i].stateDocument = temp.DER_Documento;
                            listFile[i].external = temp.DER_Externo;
                        }
                    }

                }
                Autorization.saveOperation(db, autorization, "Obteniendo documentos expedientes por col.");

                return listFile;

            }
            catch (Exception)
            {
                return null;
            }
            

        }

        // POST: api/Api_DocFile
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_DocFile/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_DocFile/5
        public void Delete(int id)
        {
        }

        private string convertStatusFile(int idType)
        {
            if (idType == 1)
            {
                return "Obligatorio";
            }
            else if (idType == 2)
            {
                return "Opcional";
            }
            else
            {
                return "";
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_VarsController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Vars
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_Vars/5
        public string Get(string id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                string value = db.ccee_Variables.Where(p => p.Var_Clave.Equals(id))
                                .Select(o=>o.Var_Valor)
                                .FirstOrDefault();
                Autorization.saveOperation(db, autorization, "Obteniendo variable");
                return value;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_Vars
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_Vars/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_Vars/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.Voucher;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_VoucherController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Voucher
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_Voucher/5
        public string Get(int id)
        {
            return "value";
        }

        [Route("api/Api_Voucher/{voucher}/{bank}")]
        public ResponseVoucherJson Get(string voucher, int bank)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ResponseVoucherJson response = new ResponseVoucherJson();


                var pLis = (from dep in db.ccee_BoletaDeDeposito
                           join cue in db.ccee_CuentaBancaria on dep.fk_CuentaBancaria equals cue.pk_CuentaBAncaria
                           join ban in db.ccee_BancoOEmisor on cue.fk_BancoEmisor equals ban.BanE_NoBancoEmisor
                           where dep.BdD_NoDeposito == voucher && ban.BanE_NoBancoEmisor == bank
                           select new ResponseVoucherJson()
                           {
                               id = dep.BdD_NoBoletaDeDeposito
                           }).Count();

                if(pLis == 0)
                {
                    response.status = "No procesado";
                }else
                {
                    response.status = "Procesado";
                }


                Autorization.saveOperation(db, autorization, "Obteniendo interfaz contable");

                return response;
            }
            catch (Exception)
            {
                return null;
            }


        }

        // POST: api/Api_Voucher
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_Voucher/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_Voucher/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.NoReferee
{
    public class RequestNoRefereeJson
    {
        public int id { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string nit { get; set; }
        public int? movil { get; set; }
        public string address { get; set; }
        public string remark { get; set; }
        public int isAfiliado { get; set; }
    }
}
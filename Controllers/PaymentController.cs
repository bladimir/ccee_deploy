﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.DocumentAccouting;
using webcee.Class.JsonSing;
using webcee.Models;
using webcee.ViewModels;

namespace webcee.Controllers
{
    public class PaymentController : Controller
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: Payment
        public ActionResult Index()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, "") == null) return RedirectToAction("Index", "Home");
            return View();
        }

        public ActionResult TypeEconomic()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_cargo_econo) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult Pay(int id)
        {
            ViewBag.idReferee = id;
            return View();
        }

        public ActionResult Economic()
        {
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_pago) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            @ViewBag.idCahier = login.idCashier;
            @ViewBag.idCash = login.idCash;
            var openClose = (from ope in db.ccee_AperturaCierre
                            join caj in db.ccee_Cajero on ope.fk_Cajero equals caj.Cjo_NoCajero
                            where ope.ApC_NoAperturaCierre == login.idOpenClose
                            select new
                            {
                                timeI = caj.Cjo_HoraInicia,
                                timeF = caj.Cjo_HoraFin,
                                document = ope.ApC_NoRecibo
                            }).FirstOrDefault();

            DateTime now = DateTime.Now;
            if(openClose.timeF != null && openClose.timeI != null)
            {
                string[] parseTimeI = openClose.timeI.Split(':');
                int timeIHour;
                string[] parseTimeF = openClose.timeF.Split(':');
                int timeFHour;
                try
                {
                    timeIHour = Convert.ToInt32(parseTimeI[0]);
                    timeFHour = Convert.ToInt32(parseTimeF[0]);
                    int timeHour = now.Hour;
                    //int timeHour = 10;
                    if(!(timeIHour <= timeHour && timeFHour >= timeHour))
                    {
                        return RedirectToAction("Index", "Referre");
                    }
                }
                catch (Exception)
                {
                    return RedirectToAction("Index", "Referre");
                }
                
            }
            @ViewBag.receipt = openClose.document;
            
            return View();
        }


        public ActionResult EconomicNotReferee()
        {
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_pago) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            @ViewBag.idCahier = login.idCashier;
            @ViewBag.idCash = login.idCash;
            var openClose = (from ope in db.ccee_AperturaCierre
                             join caj in db.ccee_Cajero on ope.fk_Cajero equals caj.Cjo_NoCajero
                             where ope.ApC_NoAperturaCierre == login.idOpenClose
                             select new
                             {
                                 timeI = caj.Cjo_HoraInicia,
                                 timeF = caj.Cjo_HoraFin,
                                 document = ope.ApC_NoRecibo
                             }).FirstOrDefault();

            DateTime now = DateTime.Now;
            if (openClose.timeF != null && openClose.timeI != null)
            {
                string[] parseTimeI = openClose.timeI.Split(':');
                int timeIHour;
                string[] parseTimeF = openClose.timeF.Split(':');
                int timeFHour;
                try
                {
                    timeIHour = Convert.ToInt32(parseTimeI[0]);
                    timeFHour = Convert.ToInt32(parseTimeF[0]);
                    int timeHour = now.Hour;
                    //int timeHour = 10;
                    if (!(timeIHour <= timeHour && timeFHour >= timeHour))
                    {
                        return RedirectToAction("Index", "Referre");
                    }
                }
                catch (Exception)
                {
                    return RedirectToAction("Index", "Referre");
                }

            }
            @ViewBag.receipt = openClose.document;

            return View();
        }


        public ActionResult TypeAccounting()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_tipo_partida) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult Accounting()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_partida_cont) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult IssuingBanck()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_banco) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult Cashier()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_cajero) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult PaymentMethod()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_medio_pago) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult PaymentMethodPay()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, "") == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            return View();
        }

        public ActionResult ReportReceipt()
        {
            //if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, "") == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            return View();
        }

        public ActionResult RecoverReceipt()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_pago) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult IncompletePayment()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_pago) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            return View();
        }

        public ActionResult Print(int? id)
        {
            //if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, "") == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            try
            {

                if(id == null)
                {
                    return View();
                }

                ccee_DocContable doc = db.ccee_DocContable.Find(id);
                if (doc == null)
                {
                    return View();
                }

                if (doc.DCo_PrintRePrint == null)
                {
                    doc.DCo_PrintRePrint = "I";
                }else
                {
                    doc.DCo_PrintRePrint = "R";
                }
                string html = doc.DCo_Xml;
                string printDate = doc.DCo_PrintRePrint + "-" + GeneralFunction.dateNowFormatDateHourPrint();
                html = html.Replace(Constant.html_pdf_remplace_print, printDate);
                html = html.Replace(Constant.html_pdf_remplace_date, doc.DCo_FechaHora.Value.ToString("dd-MM-yyyy h:mm"));
                html = html.Replace(Constant.html_pdf_remplace_No_document, "" + doc.DCo_NoDocContable);
                html = html.Replace(Constant.html_pdf_remplace_No_reciber, doc.DCo_NoRecibo);
                MemoryStream memory = GeneralFunction.GetPDFRecibo(html);
                db.Entry(doc).State = EntityState.Modified;
                db.SaveChanges();
                return File(memory, Constant.http_content_header_json);
            }
            catch (Exception)
            {
                return View();
            }
        }


        public ActionResult PrintDocument(int? id, int? ids)
        {
            //if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, "") == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            try
            {

                if (id == null)
                {
                    return View();
                }

                ccee_Documento doc = db.ccee_Documento.Find(id);
                if (doc == null)
                {
                    return View();
                }

                if (doc.DCo_PrintRePrint == null)
                {
                    doc.DCo_PrintRePrint = "I";
                }
                else
                {
                    doc.DCo_PrintRePrint = "R";
                }
                string html = doc.Doc_Xml;
                string verify = "";
                if (doc.Doc_HashDoc != null && doc.Doc_HashDoc.Count() > 12)
                {
                    verify = doc.Doc_HashDoc.Substring(0, 12) + doc.Doc_NoDocumento;
                }

                string printDate = doc.DCo_PrintRePrint + "-" + GeneralFunction.dateNowFormatDateHourPrint();
                html = html.Replace(Constant.html_pdf_remplace_print, printDate);
                html = html.Replace(Constant.html_pdf_remplace_No_document, "" + doc.Doc_NoDocumento);
                html = html.Replace("[hashkeyverify]", verify);
                MemoryStream memory = new MemoryStream();
                switch (ids.Value)
                {
                    case 1:
                        //memory = GeneralFunction.GetPDFLetter(html);
                        memory = DocumentPdf.GetPDFCertificado(html, id.Value, verify);
                        break;
                    case 2:
                        memory = GeneralFunction.GetPDFDiploma(html);
                        break;
                    default:
                        break;
                }



                db.Entry(doc).State = EntityState.Modified;
                db.SaveChanges();
                //var inputAsString = Convert.ToBase64String(memory.ToArray());
                //Console.WriteLine(inputAsString);

                return File(memory, Constant.http_content_header_json);
            }
            catch (Exception)
            {
                return View();
            }
        }

        public ActionResult PrintDocumentBase64(int? id, int? ids)
        {
            //if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, "") == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            try
            {
                var client = new RestClient("http://159.65.44.11:4023");

                if (id == null)
                {
                    return View();
                }

                ccee_Documento doc = db.ccee_Documento.Find(id);
                if (doc == null)
                {
                    return View();
                }

                if (doc.DCo_PrintRePrint == null)
                {
                    doc.DCo_PrintRePrint = "I";
                }
                else
                {
                    doc.DCo_PrintRePrint = "R";
                }
                string html = doc.Doc_Xml;
                string verify = "";
                if (doc.Doc_HashDoc != null && doc.Doc_HashDoc.Count() > 12)
                {
                    verify = doc.Doc_HashDoc.Substring(0, 12) + doc.Doc_NoDocumento;
                }

                string printDate = doc.DCo_PrintRePrint + "-" + GeneralFunction.dateNowFormatDateHourPrint();
                html = html.Replace(Constant.html_pdf_remplace_print, printDate);
                html = html.Replace(Constant.html_pdf_remplace_No_document, "" + doc.Doc_NoDocumento);
                html = html.Replace("[hashkeyverify]", verify);
                MemoryStream memory = new MemoryStream();
                switch (ids.Value)
                {
                    case 1:
                        //memory = GeneralFunction.GetPDFLetter(html);
                        memory = DocumentPdf.GetPDFCertificado(html, id.Value, verify);
                        break;
                    case 2:
                        memory = GeneralFunction.GetPDFDiploma(html);
                        break;
                    default:
                        break;
                }



                db.Entry(doc).State = EntityState.Modified;
                db.SaveChanges();
                var inputAsString = Convert.ToBase64String(memory.ToArray());
                //Console.WriteLine(inputAsString);
                var json = new
                {
                    idIntitution = 1,
                    base64 = inputAsString,
                    posx = 400,
                    posy = 350,
                    posxF = 520,
                    posyF = 400,
                    page = 1,
                    img = true
                };
                RestRequest restSend = HttpRequestRest.getRequestHttpPost(client, "api/certification", json);

                var response = client.Execute(restSend);
                string content = response.Content;
                var example1Model = new JavaScriptSerializer().Deserialize<ResponseCertification>(content);

                bool findDocument = false;
                while (!findDocument)
                {
                    var jsonId = new { id = example1Model.id };
                    Thread.Sleep(3000);
                    restSend = HttpRequestRest.getRequestHttpPost(client, "api/certification/find", jsonId);
                    response = client.Execute(restSend);
                    var findDoc = new JavaScriptSerializer().Deserialize<ResponseFindCertification>(response.Content);
                    if(findDoc.status == 1)
                    {
                        findDocument = true;
                        inputAsString = findDoc.base64;
                    }


                }

                byte[] imageBytes = Convert.FromBase64String(inputAsString);
                MemoryStream ms = new MemoryStream(imageBytes, 0,
                  imageBytes.Length);
                return File(ms, Constant.http_content_header_json);
                //restSend = HttpRequestRest.getRequestHttpPost(client, "api/certification/find", json);


                //return inputAsString;
            }
            catch (Exception)
            {
                return View();
            }
        }


        public string PruebaHTTP()
        {
            var client = new RestClient("http://159.65.44.11:4023");
            var request = new RestRequest("api/certification/find", Method.POST);

            
            var json = new
            {
                id = "sopjig9X1dRKvPP95tHMMUKzR5EtL9"
            };

            // Json to post.
            string jsonToSend = new JavaScriptSerializer().Serialize(json);
            request.AddHeader("Content-Type", "application/json; charset=utf-8");
            request.AddJsonBody(json);
            
            request.RequestFormat = DataFormat.Json;

            //var resonses = client.Execute(request);
            try
            {
                var response = client.Execute(request);
                var content = response.Content;
                
                Debug.WriteLine(content);

                //client.ExecuteAsync(request, response =>
                //{
                //    if (response.StatusCode == HttpStatusCode.OK)
                //    {
                //        Console.WriteLine(response);
                //        Debug.WriteLine(response);
                //    }
                //    else
                //    {
                //        Console.WriteLine(response);
                //        Debug.WriteLine(response);
                //    }
                //});

                Debug.WriteLine("My.......");
            }
            catch (Exception error)
            {
                // Log
            }

            return "";
        }

        public ActionResult DepositSlip()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_boleta) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            return View();
        }

        public ActionResult Receipt()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_recibo) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        
        
        public FileContentResult DocumentAccouting(int id)
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_boleta) == null) return null;
            DocumentAccouting docAcc = new DocumentAccouting(db);
            List<listDocumentJson> listDocs = docAcc.createFile(id);

            int numeroItems = listDocs.Count();
            StringWriter sw = new StringWriter();
            using (sw)
            {
                sw.Write("CODIGO BANCO (TABLA)" + "\t" +
                                 "NUMERO CUENTA BANCARIA (TABLA)" + "\t" +
                                 "CODIGO DOCUMENTO (TABLA)" + "\t" +
                                 "NUMERO DOCUMENTO" + "\t" +
                                 "FECHA DOCUMENTO" + "\t" +
                                 "A NOMBRE DE" + "\t" +
                                 "MOTIVO" + "\t" +
                                 "CENTRO COSTO (TABLA)" + "\t" +
                                 "NUMERO PA" + "\t" +
                                 "NEGOCIABLE" + "\t" +
                                 "MONTO EN DOLARES" + "\t" +
                                 "TIPO DE CAMBIO" + "\t" +
                                 "MONTO EN QUETZALES" + "\t" +
                                 "FORMATO CONTABLE (TABLA)" + "\t" +
                                 "DEPARTAMENTO (TABLA)" + "\t" +
                                 "VALOR 1" + "\t" +
                                 "VALOR 2" + "\t" +
                                 "VALOR 3" + "\t" +
                                 "VALOR 4" + "\t" +
                                 "VALOR 5" + "\t" +
                                 "VALOR 6" + "\t" +
                                 "VALOR 7" + "\t" +
                                 "VALOR 8" + "\t" +
                                 "VALOR 9" + "\t" +
                                 "VALOR 10" + "\t" +
                                 "VALOR 11" + "\t" +
                                 "VALOR 12" + "\t" +
                                 "VALOR 13" + "\t" +
                                 "VALOR 14" + "\t" +
                                 "VALOR 15" + "\t" +
                                 "VALOR 16" + "\t" +
                                 "VALOR 17" + "\t" +
                                 "VALOR 18" + "\t" +
                                 "VALOR 19" + "\t" +
                                 "VALOR 20" + "" + "");
                sw.Write(sw.NewLine);
                foreach (listDocumentJson doc in listDocs)
                {
                    sw.Write(doc.codigo + "\t" +
                                 doc.noAccount + "\t" +
                                 doc.codDocument + "\t" +
                                 doc.noDeposit + "\t" +
                                 ((doc.date == null)? "" : doc.date.Value.ToString("dd/MM/yyyy")) + "\t" +
                                 doc.namebank + "\t" +
                                 doc.motivo + "\t" +
                                 doc.centerCost + "\t" +
                                 doc.numbrePa + "\t" +
                                 doc.negociable + "\t" +
                                 doc.amountDolar + "\t" +
                                 doc.typeChange + "\t" +
                                 doc.total + "\t" +
                                 doc.format + "\t" +
                                 doc.departamento + "\t" +
                                 doc.valor1 + "\t" +
                                 doc.valor2 + "\t" +
                                 doc.valor3 + "\t" +
                                 doc.valor4 + "\t" +
                                 doc.valor5 + "\t" +
                                 doc.valor6 + "\t" +
                                 doc.valor7 + "\t" +
                                 doc.valor8 + "\t" +
                                 doc.valor9 + "\t" +
                                 doc.valor10 + "\t" +
                                 doc.valor11 + "\t" +
                                 doc.valor12 + "\t" +
                                 doc.valor13 + "\t" +
                                 doc.valor14 + "\t" +
                                 doc.valor15 + "\t" +
                                 doc.valor16 + "\t" +
                                 doc.valor17 + "\t" +
                                 doc.valor18 + "\t" +
                                 doc.valor19 + "\t" +
                                 doc.valor20 + 
                                 ((numeroItems > 0 )? "" : "" ));
                }

                

            }
            string contenido = sw.ToString();
            string NombreArchivo = "DOCUMENTOS BANCOS 20 VALORES";
            string ExtensionArchivo = "txt";
            return File(new System.Text.UTF8Encoding().GetBytes(contenido), "text/" + ExtensionArchivo, NombreArchivo + "." + ExtensionArchivo);
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Balance
{
    public class ResponseBalanceJson
    {
        public int countReferee { get; set; }
        public decimal referee { get; set; }
        public DateTime? dateReferee { get; set; }
        public int countTimbre { get; set; }
        public decimal timbre { get; set; }
        public DateTime? dateTimbre { get; set; }
        public int countPostumo { get; set; }
        public decimal postumo { get; set; }
        public DateTime? datePostumo { get; set; }
        public int countOther { get; set; }
        public decimal other { get; set; }
        public decimal total { get; set; }
        public decimal mora { get; set; }
        public DateTime? lastReferee { get; set; }
        public DateTime? lastTimbre { get; set; }
    }
}
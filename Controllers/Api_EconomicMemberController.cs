﻿using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.Economic;
using webcee.Class.JsonMolelsWS;
using webcee.Class.Tools;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_EconomicMemberController : ApiController
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_EconomicMember/5
        public IEnumerable<ResponseEconomicJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                Autorization.saveOperation(db, autorization, "Obteniendo cargos economicos");
                return listEconomic(id);
            }
            catch (Exception)
            {
                return null;
            }
        }

        [Route("api/Api_EconomicMember/{afiliado}/{id}")]
        public IEnumerable<ResponseEconomicJson> Get(int afiliado, int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                Autorization.saveOperation(db, autorization, "Obteniendo cargos economicos no col.");
                return listEconomicNotReferr(id);
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_Economic/id=type
        public ResponseEconomicJson Post(RequestEconomicJson economic)
        {
            int TypeEconomicTimbresDemand = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "VarTimbreDemanda"));
            int TypeEconomicVariable = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "VarCargoVariable"));
            int TypeEconomicConvenio = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "VarCargoConvenio"));
            int TypeEconomicVariableTP = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "VarCargoVariableTP"));
            decimal valorEconomic = 0;
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_TipoCargoEconomico tempCargo = db.ccee_TipoCargoEconomico.Find(economic.idTypeEconomic);
                if(tempCargo == null)
                {
                    return null;
                }

                if (tempCargo.TCE_NoTipoCargoEconomico == TypeEconomicTimbresDemand 
                    || tempCargo.TCE_NoTipoCargoEconomico == TypeEconomicVariable
                    || tempCargo.TCE_NoTipoCargoEconomico == TypeEconomicConvenio
                    || tempCargo.TCE_NoTipoCargoEconomico == TypeEconomicVariableTP)
                {
                    valorEconomic = economic.amount;
                }else
                {
                    valorEconomic = getValueValor(tempCargo.TCE_Valor.Value, tempCargo.TCE_Porcentaje.Value);
                }


                DateTime? time = DateTime.Today;
                if (economic.isDemand == 1)
                {
                    var tempEconomicTimbre = (from eco in db.ccee_CargoEconomico_timbre_shopping_cart
                                              join typ in db.ccee_TipoCargoEconomico on eco.fk_TipoCargoEconomico equals typ.TCE_NoTipoCargoEconomico
                                              where eco.fk_Colegiado == economic.idReferee && eco.fk_Pago != null
                                                      // && eco.CEcT_FechaGeneracion <= DateTime.Now
                                              orderby eco.CEcT_FechaGeneracion descending
                                              select eco).FirstOrDefault();

                    if(time != null)
                        time = tempEconomicTimbre.CEcT_FechaGeneracion;


                }

                ccee_CargoEconomico_otros_shopping_cart tempEco = new ccee_CargoEconomico_otros_shopping_cart();
                tempEco.CEcO_Monto = valorEconomic;
                tempEco.fk_Colegiado = economic.idReferee;
                tempEco.fk_NoColegiado = economic.idNoReferee;
                tempEco.fk_TipoCargoEconomico = economic.idTypeEconomic;
                tempEco.CEcO_FechaGeneracion = time;

                
                db.ccee_CargoEconomico_otros_shopping_cart.Add(tempEco);
                Autorization.saveOperation(db, autorization, "Agregando cargos economicos");
                db.SaveChanges();

                ResponseEconomicJson responseEco = new ResponseEconomicJson();
                responseEco.id = tempEco.CEcO_NoCargoEconomico;
                responseEco.amount = tempEco.CEcO_Monto.Value;
                responseEco.idReferee = tempEco.fk_Colegiado;
                responseEco.idNoReferee = tempEco.fk_NoColegiado;
                responseEco.description = tempCargo.TCE_Descripcion;
                responseEco.idTypeEconomic = tempEco.fk_TipoCargoEconomico;
                responseEco.conditionVAlue = tempCargo.TCE_CondicionCalculo;
                responseEco.dateCreated = tempEco.CEcO_FechaGeneracion;
                responseEco.typeEconomic = 3;
                
                return responseEco;
            }
            catch (Exception)
            {
                return null;
            }

        }
        

        // PUT: api/Api_Economic/5
        public IHttpActionResult Put(int id, RequestEconomicJson economic)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                if (economic.idType == 1)
                {
                    ccee_CargoEconomico_colegiado_shopping_cart temEco = db.ccee_CargoEconomico_colegiado_shopping_cart.Find(id);
                    if (temEco == null)
                    {
                        return Ok(HttpStatusCode.NotFound);
                    }

                    temEco.CEcC_Monto = economic.amount;
                    Autorization.saveOperation(db, autorization, "Modificado cargos economicos");
                    db.Entry(temEco).State = EntityState.Modified;
                    db.SaveChanges();
                    return Ok(HttpStatusCode.OK);
                }
                else if (economic.idType == 2)
                {
                    ccee_CargoEconomico_timbre_shopping_cart temEco = db.ccee_CargoEconomico_timbre_shopping_cart.Find(id);
                    if (temEco == null)
                    {
                        return Ok(HttpStatusCode.NotFound);
                    }

                    temEco.CEcT_Monto = economic.amount;
                    Autorization.saveOperation(db, autorization, "Modificado cargos economicos");
                    db.Entry(temEco).State = EntityState.Modified;
                    db.SaveChanges();
                    return Ok(HttpStatusCode.OK);
                }
                else
                {
                    ccee_CargoEconomico_otros_shopping_cart temEco = db.ccee_CargoEconomico_otros_shopping_cart.Find(id);
                    if (temEco == null)
                    {
                        return Ok(HttpStatusCode.NotFound);
                    }

                    temEco.CEcO_Monto = economic.amount;
                    Autorization.saveOperation(db, autorization, "Modificado cargos economicos");
                    db.Entry(temEco).State = EntityState.Modified;
                    db.SaveChanges();
                    return Ok(HttpStatusCode.OK);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        // DELETE: api/Api_Economic/5
        [Route("api/Api_EconomicMember/{id}/{idType}")]
        public IHttpActionResult Delete(int id, int idType)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                if(idType == 1)
                {
                    ccee_CargoEconomico_colegiado_shopping_cart temEco = db.ccee_CargoEconomico_colegiado_shopping_cart.Find(id);
                    if (temEco == null)
                    {
                        return Ok(HttpStatusCode.NotFound);
                    }

                    db.ccee_CargoEconomico_colegiado_shopping_cart.Remove(temEco);
                    Autorization.saveOperation(db, autorization, "Eliminando cargos economicos");
                    db.SaveChanges();
                    return Ok(HttpStatusCode.OK);
                }else if (idType == 2)
                {
                    ccee_CargoEconomico_timbre_shopping_cart temEco = db.ccee_CargoEconomico_timbre_shopping_cart.Find(id);
                    if (temEco == null)
                    {
                        return Ok(HttpStatusCode.NotFound);
                    }

                    db.ccee_CargoEconomico_timbre_shopping_cart.Remove(temEco);
                    Autorization.saveOperation(db, autorization, "Eliminando cargos economicos");
                    db.SaveChanges();
                    return Ok(HttpStatusCode.OK);
                }
                else
                {
                    ccee_CargoEconomico_otros_shopping_cart temEco = db.ccee_CargoEconomico_otros_shopping_cart.Find(id);
                    if (temEco == null)
                    {
                        return Ok(HttpStatusCode.NotFound);
                    }

                    db.ccee_CargoEconomico_otros_shopping_cart.Remove(temEco);
                    Autorization.saveOperation(db, autorization, "Eliminando cargos economicos");
                    db.SaveChanges();
                    return Ok(HttpStatusCode.OK);
                }

            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }


        private int getCantEconomic(int periodicity, DateTime economic, DateTime dateChange )
        {
            int cantEconomic = 0;
            if (periodicity == 1)
            {
                cantEconomic = DateEconomic.getMonth(economic, dateChange);

            }
            else if (periodicity == 2)
            {
                cantEconomic = DateEconomic.getYear(economic, dateChange);
            }
            return cantEconomic;
        }

        private decimal getValueValor(decimal value, decimal percent)
        {
            if(percent != 0)
            {
                return decimal.Round(value * (100 / percent));
            }
            return value;
        }


        public IEnumerable<ResponseEconomicJson> listEconomic(int id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    int minTime = 3;
                    string IaValue = GeneralFunction.getKeyTableVariable(db, "Ia");
                    List<int> listIdTimbre = db.ccee_TipoCargoEconomico.Where(p => p.TCE_CondicionCalculo == "esTimbre")
                                                                .Select(o => o.TCE_NoTipoCargoEconomico)
                                                                .ToList();
                    CultureInfo culture;
                    culture = CultureInfo.CreateSpecificCulture("es-GT");

                    if (IaValue == null) return null;
                    double Ia = Convert.ToDouble(IaValue);

                    //Obtener el referee
                    var referee = db.ccee_Colegiado.Find(id);
                    logger.Info("Economic|Colegiado| " + referee.Col_NoColegiado);

                    //db.GenerarInteresTimbre(id, idTypeEconomic);
                    List<ResponseEconomicJson> listEconomicTotal = new List<ResponseEconomicJson>();

                    //Generar Economico de Colegiado
                    List<ResponseEconomicRefereeJson> tempEconomicReferee = (from eco in db.ccee_CargoEconomico_colegiado_shopping_cart
                                                                             join typ in db.ccee_TipoCargoEconomico on eco.fk_TipoCargoEconomico equals typ.TCE_NoTipoCargoEconomico
                                       where eco.fk_Colegiado == id && eco.fk_Pago == null
                                       orderby eco.CEcC_FechaGeneracion ascending
                                       select new ResponseEconomicRefereeJson()
                                       {
                                           id = eco.CEcC_NoCargoEconomico,
                                           amount = eco.CEcC_Monto.Value,
                                           idReferee = eco.fk_Colegiado.Value,
                                           idTypeEconomic = eco.fk_TipoCargoEconomico,
                                           description = typ.TCE_Descripcion,
                                           dateCreated = eco.CEcC_FechaGeneracion

                                       }).ToList();
                    int initList = 0;
                    List<ModelMonthYearEconomic> listMonthReferee = mounthListReferee(tempEconomicReferee.ToList()).ToList();
                    ResponseEconomicJson tempResponseEco;
                    ModelMonthYearEconomic tempAnioMes;
                    for (int i = 0; i < listMonthReferee.Count; i++)
                    {
                        tempAnioMes = listMonthReferee[i];
                        tempResponseEco = new ResponseEconomicJson();
                        tempResponseEco.description = "Colegiado " + tempAnioMes.MonthName + " del " + tempAnioMes.Year;
                        tempResponseEco.typeEconomic = 1;
                        tempResponseEco.amount = 0.0m;

                        List<ResponseEconomicRefereeJson> tempEcoRef = new List<ResponseEconomicRefereeJson>();

                        initList = ((i == 0)? 0 : initList + listMonthReferee[i-1].Total);
                        for (int j = 0; j < tempAnioMes.Total; j++)
                        {
                            ResponseEconomicRefereeJson unitEcoRef = tempEconomicReferee[initList + j];
                            tempResponseEco.amount += unitEcoRef.amount;
                            tempEcoRef.Add(unitEcoRef);
                        }
                        tempResponseEco.ListReferee = tempEcoRef;
                        listEconomicTotal.Add(tempResponseEco);
                        tempAnioMes = null;
                    }

                    //Generar economico timbre
                    List<ResponseEconomicRefereeJson> tempEconomicTimbre = (from eco in db.ccee_CargoEconomico_timbre_shopping_cart
                                                                            join typ in db.ccee_TipoCargoEconomico on eco.fk_TipoCargoEconomico equals typ.TCE_NoTipoCargoEconomico
                                                                             where eco.fk_Colegiado == id && eco.fk_Pago == null
                                                                                   // && eco.CEcT_FechaGeneracion <= DateTime.Now
                                                                             orderby eco.CEcT_FechaGeneracion ascending
                                                                             select new ResponseEconomicRefereeJson()
                                                                             {
                                                                                 id = eco.CEcT_NoCargoEconomico,
                                                                                 amount = eco.CEcT_Monto.Value,
                                                                                 idReferee = eco.fk_Colegiado.Value,
                                                                                 idTypeEconomic = eco.fk_TipoCargoEconomico,
                                                                                 description = typ.TCE_Descripcion,
                                                                                 dateCreated = eco.CEcT_FechaGeneracion

                                                                             }).ToList();
                    //tempEconomicTimbre = tempEconomicTimbre.OrderByDescending(p => p.idTypeEconomic).ToList();

                    foreach (ResponseEconomicRefereeJson respEco in tempEconomicTimbre)
                    {
                        tempResponseEco = new ResponseEconomicJson();
                        DateTime date = Convert.ToDateTime(respEco.dateCreated.Value.Month + "/" + 1 + "/" + respEco.dateCreated.Value.Year);
                        string monthName = culture.DateTimeFormat.GetMonthName(respEco.dateCreated.Value.Month);
                        tempResponseEco.description = respEco.description + " " + monthName + " del " + respEco.dateCreated.Value.Year;
                        tempResponseEco.typeEconomic = 2;
                        tempResponseEco.amount = respEco.amount;
                        tempResponseEco.id = respEco.id;
                        tempResponseEco.idReferee = respEco.idReferee;
                        tempResponseEco.idTypeEconomic = respEco.idTypeEconomic;
                        tempResponseEco.dateCreated = respEco.dateCreated;
                        tempResponseEco.order = 2;
                        tempResponseEco.orderDate = date;
                        for (int k = 0; k < listIdTimbre.Count; k++)
                        {
                            if (listIdTimbre[k] == tempResponseEco.idTypeEconomic)
                            {
                                tempResponseEco.isTimbre = 1;
                                tempResponseEco.order = 1;
                            }
                        }
                        listEconomicTotal.Add(tempResponseEco);
                    }

                    listMonthReferee = mounthListReferee(tempEconomicTimbre.ToList()).ToList();
                    initList = 0;
                    tempAnioMes = null;
                    for (int i = 0; i < listMonthReferee.Count; i++)
                    {
                        tempAnioMes = listMonthReferee[i];
                        decimal sumPostumoTimbre = 0.0m;
                        initList = ((i == 0) ? 0 : initList + listMonthReferee[i - 1].Total);
                        for (int j = 0; j < tempAnioMes.Total; j++)
                        {
                            ResponseEconomicRefereeJson unitEcoRef = tempEconomicTimbre[initList + j];
                            sumPostumoTimbre += unitEcoRef.amount;
                        }

                        DateTime isToday = DateTime.Now;
                        if (listMonthReferee.Count > minTime && referee.fk_TipoColegiado != Constant.type_referee_convenio_pago)
                        {
                            GeneralFunction generalFun = new GeneralFunction();
                            DateTime? date = Convert.ToDateTime(tempAnioMes.Month + "/" + 1 + "/" + tempAnioMes.Year);
                            logger.Info("Economic|Fecha| " + date.Value.ToString());
                            ResponseEconomicRefereeJson resMora = new ResponseEconomicRefereeJson();
                            int diference = generalFun.monthDifference(isToday, date.Value);
                            decimal amountMora = generalFun.amountMora(sumPostumoTimbre, diference, Ia);
                            tempResponseEco = new ResponseEconomicJson();
                            tempResponseEco.description = "Cuota Extraordinaria " + tempAnioMes.MonthName;
                            tempResponseEco.typeEconomic = 2;
                            tempResponseEco.amount = amountMora;
                            tempResponseEco.idReferee = id;
                            tempResponseEco.idTypeEconomic = 120;
                            tempResponseEco.dateCreated = date;
                            tempResponseEco.order = 3;
                            tempResponseEco.orderDate = date;
                            listEconomicTotal.Add(tempResponseEco);
                            date = null;
                        }
                        tempAnioMes = null;
                    }

                    
                    //Generar economico timbre
                    tempEconomicTimbre = (from eco in db.ccee_CargoEconomico_timbre_shopping_cart
                                          join typ in db.ccee_TipoCargoEconomico on eco.fk_TipoCargoEconomico equals typ.TCE_NoTipoCargoEconomico
                                        where eco.fk_Colegiado == id && eco.fk_Pago == null
                                              && eco.CEcT_FechaGeneracion >  DateTime.Now
                                        orderby eco.CEcT_FechaGeneracion ascending
                                        select new ResponseEconomicRefereeJson()
                                        {
                                            id = eco.CEcT_NoCargoEconomico,
                                            amount = eco.CEcT_Monto.Value,
                                            idReferee = eco.fk_Colegiado.Value,
                                            idTypeEconomic = eco.fk_TipoCargoEconomico,
                                            description = typ.TCE_Descripcion,
                                            dateCreated = eco.CEcT_FechaGeneracion

                                        }).ToList();

                    foreach (ResponseEconomicRefereeJson respEco in tempEconomicTimbre)
                    {
                        tempResponseEco = new ResponseEconomicJson();
                        DateTime date = Convert.ToDateTime(respEco.dateCreated.Value.Month + "/" + 1 + "/" + respEco.dateCreated.Value.Year);
                        string monthName = culture.DateTimeFormat.GetMonthName(respEco.dateCreated.Value.Month);
                        tempResponseEco.description = respEco.description + " " + monthName + " del " + respEco.dateCreated.Value.Year;
                        tempResponseEco.typeEconomic = 2;
                        tempResponseEco.amount = respEco.amount;
                        tempResponseEco.id = respEco.id;
                        tempResponseEco.idReferee = respEco.idReferee;
                        tempResponseEco.idTypeEconomic = respEco.idTypeEconomic;
                        tempResponseEco.dateCreated = respEco.dateCreated;
                        tempResponseEco.order = 2;
                        tempResponseEco.orderDate = date;
                        for (int k = 0; k < listIdTimbre.Count; k++)
                        {
                            if (listIdTimbre[k] == tempResponseEco.idTypeEconomic)
                            {
                                tempResponseEco.isTimbre = 1;
                                tempResponseEco.order = 1;
                            }
                        }
                        listEconomicTotal.Add(tempResponseEco);
                    }
                    
                    // Generar cargos economicos de otro tipo
                    List<ResponseEconomicJson> tempEconomicOther = (from eco in db.ccee_CargoEconomico_otros_shopping_cart
                                                                    join typ in db.ccee_TipoCargoEconomico on eco.fk_TipoCargoEconomico equals typ.TCE_NoTipoCargoEconomico
                                                                            where eco.fk_Colegiado == id && eco.fk_Pago == null && eco.CEcO_FechaGeneracion != null
                                                                            orderby eco.CEcO_FechaGeneracion ascending
                                                                            select new ResponseEconomicJson()
                                                                            {
                                                                                id = eco.CEcO_NoCargoEconomico,
                                                                                amount = eco.CEcO_Monto.Value,
                                                                                idReferee = eco.fk_Colegiado.Value,
                                                                                idTypeEconomic = eco.fk_TipoCargoEconomico,
                                                                                description = typ.TCE_Descripcion,
                                                                                conditionVAlue = typ.TCE_CondicionCalculo,
                                                                                dateCreated = eco.CEcO_FechaGeneracion,
                                                                                typeEconomic = 3
                                                                            }).ToList();
                    foreach (ResponseEconomicJson tempEco in tempEconomicOther)
                    {
                        listEconomicTotal.Add(tempEco);
                    }


                    //Cargo economico de mora
                    int idTypeEcoCuadre = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "VarCuadrePago"));
                    ccee_TipoCargoEconomico tempType = db.ccee_TipoCargoEconomico.Find(idTypeEcoCuadre);
                    ResponseEconomicJson responseCuadre = new ResponseEconomicJson();
                    responseCuadre.typeEconomic = 4;
                    responseCuadre.description = tempType.TCE_Descripcion;
                    responseCuadre.idReferee = id; 
                    responseCuadre.idNoReferee = null; 
                    responseCuadre.idTypeEconomic = tempType.TCE_NoTipoCargoEconomico;

                    listEconomicTotal.Add(responseCuadre);
                    
                    transaction.Commit();  
                    return listEconomicTotal;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return null;
                }

            }
                
        }

        public IEnumerable<ResponseEconomicJson> listEconomicNotReferr(int id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {

                try
                {
                    List<ResponseEconomicJson> listEconomicTotal = new List<ResponseEconomicJson>();

                    // Generar cargos economicos de otro tipo
                    List<ResponseEconomicJson> tempEconomicOther = (from eco in db.ccee_CargoEconomico_otros_shopping_cart
                                                                    join typ in db.ccee_TipoCargoEconomico on eco.fk_TipoCargoEconomico equals typ.TCE_NoTipoCargoEconomico
                                                                    where eco.fk_NoColegiado == id && eco.fk_Pago == null && eco.CEcO_FechaGeneracion != null
                                                                    orderby eco.CEcO_FechaGeneracion ascending
                                                                    select new ResponseEconomicJson()
                                                                    {
                                                                        id = eco.CEcO_NoCargoEconomico,
                                                                        amount = eco.CEcO_Monto.Value,
                                                                        idReferee = eco.fk_Colegiado.Value,
                                                                        idNoReferee = eco.fk_NoColegiado,
                                                                        idTypeEconomic = eco.fk_TipoCargoEconomico,
                                                                        description = typ.TCE_Descripcion,
                                                                        conditionVAlue = typ.TCE_CondicionCalculo,
                                                                        dateCreated = eco.CEcO_FechaGeneracion,
                                                                        typeEconomic = 3
                                                                    }).ToList();
                    listEconomicTotal.AddRange(tempEconomicOther);

                    //Cargo economico de mora
                    int idTypeEcoCuadre = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "VarCuadrePago"));
                    ccee_TipoCargoEconomico tempType = db.ccee_TipoCargoEconomico.Find(idTypeEcoCuadre);
                    ResponseEconomicJson responseCuadre = new ResponseEconomicJson();
                    responseCuadre.typeEconomic = 4;
                    responseCuadre.description = tempType.TCE_Descripcion;
                    responseCuadre.idReferee = null;
                    responseCuadre.idNoReferee = id;
                    responseCuadre.idTypeEconomic = tempType.TCE_NoTipoCargoEconomico;
                    
                    listEconomicTotal.Add(responseCuadre);

                    transaction.Commit();
                    return listEconomicTotal;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return null;
                }

            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private IEnumerable<ModelMonthYearEconomic> mounthListReferee(List<ResponseEconomicRefereeJson> listCuotaColegiadoTotal)
        {
            
            var model = listCuotaColegiadoTotal
                .GroupBy(o => new
                {
                    Month = o.dateCreated.Value.Month,
                    Year = o.dateCreated.Value.Year
                })
                .Select(g => new ModelMonthYearEconomic
                {
                    Month = g.Key.Month,
                    Year = g.Key.Year,
                    Total = g.Count(),
                })
                .OrderBy(a => a.Year)
                .ThenBy(a => a.Month)
                .ToList();

            IEnumerable<ModelMonthYearEconomic> tempAnioMes = model;
            return model;

        }


    }
}

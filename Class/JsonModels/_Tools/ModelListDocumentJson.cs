﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels._Tools
{
    public class ModelListDocumentJson
    {
        public int idDocument { get; set; }
        public string direccion { get; set; }
        public string title { get; set; }
        public string hash { get; set; }
        public string direccionBase64 { get; set; }
        public Boolean vigente { get; set; }
        public string fechaEmision { get; set; }
        public string aNombre { get; set; }
    }
}
﻿(function () {
    angular.module('document')
        .controller('uploadNomina', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'documentFactory', 'Upload', function ($scope, $http, CONSTANT, $anchorScroll, documentFactory, Upload) {
            
            $scope.listData = [];
            $scope.listNomina = [];
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.regexAmount = CONSTANT.REGEX_AMOUNT_NEG;
            $scope.visibleCount = 0;
            $scope.listExtras = [];
            $scope.totalReferee = 0;
            $scope.totalPostumo = 0;
            $scope.totalTimbre = 0;
            $scope.finalAmount = 0;
            $scope.countList = 0;
            $scope.tempListData = [];
            $scope.dateFormat = "";
            $scope.idBatch = 0;
            $scope.idCashier = 0;
            $scope.idDocAccount = 0;
            $scope.listPaymentMethod = [];
            $scope.listPaymentMethodPay = [];
            $scope.listDataTypeDoc;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.submit = function () {
                if ($scope.file) {
                    $scope.uploadFile($scope.file);
                }
            };

            $scope.init = function () {
                $scope.getNoReferee();
                $scope.getIssuingBank();
                $scope.getPaymentMethod();
                $scope.getIdPayMethodCash();
            }

            $scope.setAuth = function (idCashier) {
                $scope.idCashier = idCashier;
                documentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        documentFactory.setAuthorization(response.data);
                        $scope.init();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.getNoReferee = function () {
                documentFactory.actionGetIntFunction(CONSTANT.URL_API_NOREFEREE, 1)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listNomina = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getIssuingBank = function () {
                $scope.showLoadingFunction();
                documentFactory.actionGetFunction(CONSTANT.URL_API_ISSUINGBANK)
                .then(function (response) {
                    console.log(response);
                    $scope.listIssuingBank = response.data;
                    $scope.hideLoadingFunction();
                }, function (error) {
                    console.log(error);
                    $scope.hideLoadingFunction();
                });
            }

            $scope.getPaymentMethod = function () {
                $scope.showLoadingFunction();
                documentFactory.actionGetIntFunction(CONSTANT.URL_API_PAYMENTMETHOD, 40)
                .then(function (response) {
                    $scope.listPaymentMethod = response.data;
                    $scope.hideLoadingFunction();
                }, function (error) {
                    console.log(error);
                    $scope.hideLoadingFunction();
                });
            }

            $scope.getIdPayMethodCash = function () {
                documentFactory.actionGetIntFunction(CONSTANT.URL_API_VARS, "CCEEMedioPago")
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    $scope.idPayMethodCash = response.data;
                    console.log(response.data);
                }, function (error) {
                    console.log(error);
                    $scope.hideLoadingFunction();
                });
            }

            $scope.saveBatch = function (cashier) {
                $scope.showLoadingFunction();
                var information = {
                    idCashier: cashier
                }
                console.log(information);
                documentFactory.actionPostFunction(CONSTANT.URL_API_BATCH, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            console.log(response.data);
                            if (response.data != null) {
                                $scope.idBatch = response.data.id;
                                $scope.idDocAccount = response.data.idDocAccount;
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }


            $scope.payNomina = function (cashier, cash, receip) {
                $scope.tempListData = [];
                if ($scope.countList < $scope.listData.length) {
                    $scope.tempListData.push($scope.listData[$scope.countList]);
                    $scope.payNominaIndividual(cashier, cash, receip, $scope.tempListData);
                    $scope.countList++;
                }
            }


            $scope.payNominaIndividual = function (cashier, cash, receip, objectPay) {
                //$scope.showLoadingFunction();
                objectPay[0].statusSync = 1;
                console.log(objectPay);
                var idReceip = receip + 1;
                var information = {
                    NoColegiado: $scope.selectNoReferre,
                    idCash: cash,
                    idCashier: cashier,
                    document: idReceip,
                    idType: $scope.radioPaymentMethod,
                    NoDocument: (($scope.radioPaymentMethod == "1") ? $scope.numberpayDoc : ""),
                    idBank: (($scope.radioPaymentMethod == "1") ?  $scope.selectIssuingBank.id : 0),
                    enableUpdate: $scope.enableUpdate,
                    negatives: $scope.negative,
                    idTypeDocument: parseInt($scope.selectIdType),
                    data: objectPay,
                    idBatch: $scope.idBatch,
                    idDocAccount: $scope.idDocAccount,
                    jsonContentMethod: $scope.listPaymentMethodPay,
                    dateOfPay: $scope.deathTextReferee,
                    textOfEconomic: $scope.textRemark
                }
                console.log(information);
                documentFactory.actionPostFunction(CONSTANT.URL_API_NOMINA, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            console.log(response.data);
                            objectPay[0].statusSync = 2;

                            if (response.data.status == 400) {
                                objectPay[0].statusSync = 3;
                            }

                            if ($scope.countList < $scope.listData.length) {
                                $scope.tempListData = [];
                                $scope.tempListData.push($scope.listData[$scope.countList]);
                                $scope.payNominaIndividual(cashier, cash, receip, $scope.tempListData);
                                $scope.countList++;
                            }

                            //if (response.data.status == 200) {
                            //    $scope.uploadTotal = response.data.total;
                            //    $scope.uploadAproxi = response.data.extra;
                            //    $scope.uploadReciber = response.data.recibo;
                            //    $scope.uploadTimbre = response.data.timbres;
                            //    $scope.listDataLis = response.data.listEco;
                            //    $scope.uploadReciberList = response.data.listDoc;
                            //    $scope.uploadErro = response.data.textError;
                            //    $scope.setReceipt(idReceip);
                            //    console.log($scope.uploadReciberList);
                            //} else {
                            //    $scope.uploadErro = response.data.textError;
                            //}
                            //$scope.visibleCount = 1;

                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }


            $scope.getReciberNomina = function (cashier, cash, receip) {
                $scope.showLoadingFunction();
                var idReceip = receip + 1;
                var information = {
                    NoColegiado: $scope.selectNoReferre,
                    idCash: cash,
                    idCashier: cashier,
                    document: idReceip,
                    idType: $scope.radioPaymentMethod,
                    NoDocument: (($scope.radioPaymentMethod == "1") ? $scope.numberpayDoc : ""),
                    idBank: (($scope.radioPaymentMethod == "1") ? $scope.selectIssuingBank.id : 0),
                    enableUpdate: $scope.enableUpdate,
                    negatives: $scope.negative,
                    idTypeDocument: parseInt($scope.selectIdType),
                    idBatch: $scope.idBatch,
                    idDocAccount: $scope.idDocAccount,
                    jsonContentMethod: $scope.listPaymentMethodPay,
                    dateOfPay: $scope.deathTextReferee,
                    textOfEconomic: $scope.textRemark
                }
                console.log(information); //dateOfPay: $scope.deathTextReferee
                documentFactory.actionPutFunction(CONSTANT.URL_API_NOMINA, $scope.idBatch, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            console.log(response.data);

                            

                            if (response.data.status == 200) {
                                $scope.uploadTotal = response.data.total;
                                $scope.uploadAproxi = response.data.extra;
                                $scope.uploadReciber = response.data.recibo;
                                $scope.uploadTimbre = response.data.timbres;
                                $scope.listDataLis = response.data.listEco;
                                $scope.uploadReciberList = response.data.listDoc;
                                $scope.uploadErro = response.data.textError;
                                $scope.setReceipt(idReceip);
                                console.log($scope.uploadReciberList);
                            } else {
                                $scope.uploadErro = response.data.textError;
                            }
                            $scope.visibleCount = 1;

                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }



            $scope.setReceipt = function (idReceip) {
                $scope.showLoadingFunction();
                console.log(idReceip);
                var information = {
                    NoReceipt: idReceip
                }
                documentFactory.actionPostFunction("/Referre/incrementReceipPublic", information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();

                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            
            $scope.uploadFile = function (file) {
                $scope.showLoadingFunction();
                Upload.upload({
                    url: '/Document/UploadNominaData',
                    method: 'POST',
                    file: file,
                    data: { file: file, 'username': 'bladimir', 'targetPath': '/' }
                }).then(function (resp) {
                    $scope.hideLoadingFunction();
                    $scope.listData = resp.data;
                    console.log(resp);
                    console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                    $scope.sumAmount();
                    $scope.saveBatch($scope.idCashier);

                }, function (resp) {
                    console.log('Error status: ' + resp.status);
                }, function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
            }

            $scope.sumAmount = function () {
                $scope.totalReferee = 0;
                $scope.totalPostumo = 0;
                $scope.totalTimbre = 0;
                var len = $scope.listData.length;
                for (var i = 0; i < len; i++) {
                    $scope.totalReferee += $scope.listData[i].cuotaColegiado;
                    $scope.totalPostumo += $scope.listData[i].seguroPostumo;
                    $scope.totalTimbre += $scope.listData[i].cuotaTimbre;
                }
                var total = $scope.totalReferee + $scope.totalPostumo + $scope.totalTimbre;
                if ($scope.finalAmount == total) {
                    $scope.textNotify = "Los montos si cuadran";
                } else {
                    $scope.textNotify = "Los montos no cuadran";
                }

                $scope.textAmounts =  total;
                


            }

            
            $scope.addListMethodZero = function () {
                    var paymentMethod = {
                        amount: 0,
                        noTransaction: 0,
                        noDocument: 0,
                        reservation: 0,
                        rejection: 0,
                        remarke: "",
                        idBank: 40,
                        idPaymentMethod: $scope.idPayMethodCash
                    };
                    $scope.listPaymentMethodPay.push(paymentMethod);
                    $scope.aumontoPaymentMethod = parseFloat($scope.aumontoPaymentMethod) - parseFloat($scope.amountpayMethod);
                    $scope.amountpayMethod = 0;
                    $scope.numberpayTrans = "";
                    $scope.numberpayDoc = "";
                    $scope.alertamountPayment = "";
                }

                $scope.addListMethodCash = function () {
                    var paymentMethod = {
                        amount: parseFloat($scope.amountpayMethod),
                        noTransaction: 0,
                        noDocument: 0,
                        reservation: 0,
                        rejection: 0,
                        remarke: "",
                        idBank: 40,
                        idPaymentMethod: $scope.idPayMethodCash
                    };
                    $scope.listPaymentMethodPay.push(paymentMethod);
                    $scope.aumontoPaymentMethod = $scope.subTwoDecimal($scope.aumontoPaymentMethod, $scope.amountpayMethod);
                    $scope.amountpayMethod = 0;
                    $scope.numberpayTrans = "";
                    $scope.numberpayDoc = "";
                    $scope.alertamountPayment = "";
                }

                $scope.addListMethod = function () {
                    var idMethodPay = 0;
                    var len = $scope.listPaymentMethod.length;
                    for (var i = 0; i < len; i++) {
                        var type = $scope.listPaymentMethod[i].typeMethod;
                        if (type == $scope.radioPaymentMethod) {
                            idMethodPay = $scope.listPaymentMethod[i].id;
                            break;
                        }
                    }

                    console.log(idMethodPay);


                    var paymentMethod = {
                        amount: parseFloat($scope.amountpayMethod),
                        noTransaction: $scope.numberpayTrans,
                        noDocument: $scope.numberpayDoc,
                        reservation: 1,
                        rejection: 0,
                        remarke: $scope.remakepayMothod,
                        idBank: $scope.selectIssuingBank.id,
                        idPaymentMethod: idMethodPay
                    };
                    console.log(paymentMethod);
                    $scope.listPaymentMethodPay.push(paymentMethod);
                    $scope.aumontoPaymentMethod = parseFloat($scope.aumontoPaymentMethod) - parseFloat($scope.amountpayMethod);
                    $scope.amountpayMethod = 0;
                    $scope.numberpayTrans = "";
                    $scope.numberpayDoc = "";
                    $scope.alertamountPayment = "";
                    $scope.viewBank = false;
                }



                $scope.deleteListMethod = function (data) {
                    var pos = $scope.listPaymentMethodPay.indexOf(data);
                    console.log(data);
                    $scope.aumontoPaymentMethod = $scope.sumTwoDecimal($scope.aumontoPaymentMethod, data.amount);
                    $scope.listPaymentMethodPay.splice(pos, 1);
                }


                $scope.sumTwoDecimal = function (first, secound) {
                    var Total = parseFloat(first) + parseFloat(secound);
                    return parseFloat(Total.toFixed(2));
                }

                $scope.subTwoDecimal = function (first, secound) {
                    var Total = parseFloat(first) - parseFloat(secound);
                    return parseFloat(Total.toFixed(2));
                }

            $scope.typeError = function (type) {
                switch (type) {
                    case 1:
                        return "medio";
                        break;
                    case 2:
                        return "grave";
                        break;
                    default:
                        return "";
                }
            }

            $scope.getStatusSyn = function (type) {
                switch (type) {
                    case 0:
                        return "No sincronizado";
                        break;
                    case 1:
                        return "Sincronizado";
                        break;
                    case 2:
                        return "Completado";
                        break;
                    case 3:
                        return "Error en el dato";
                        break;
                    default:
                        return "Sin conocimiento";
                }
            }




        }]);

})()
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.UserCash
{
    public class RequestUserCashJson
    {
        public int idUser { get; set; }
        public string nameUser { get; set; }
    }
}
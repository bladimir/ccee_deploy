﻿using System.Collections.Generic;
using webcee.Class.JsonModels.Economic;
using webcee.Class.JsonModels.PaymentMethod_Pay;

namespace webcee.Class.JsonModels._Tools
{
    public class PostBodyEconomicJson
    {
        public decimal amount { get; set; }
        public string remake { get; set; }
        public int idCashier { get; set; }
        public int idCash { get; set; }
        public int id { get; set; }
        public int idReferee { get; set; }
        public int idNotReferee { get; set; }
        public IEnumerable<ResponseEconomicJson> jsonContent { get; set; }
        public IEnumerable<RequestPaymentMethod_PayJson> jsonContentMethod { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.ValueTimbre;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_ValueTimbreController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_ValueTimbre
        public IEnumerable<ResponseValueTimbreJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempValue = from val in db.ccee_ValorTimbre
                                     select new ResponseValueTimbreJson()
                                     {
                                         id = val.VaT_NoValorTimbre,
                                         name = val.VaT_Nombre,
                                         denomination = val.VaT_Denominacion
                                     };
                Autorization.saveOperation(db, autorization, "Obteniendo valor timbre");
                return tempValue;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_ValueTimbre/5
        public IEnumerable<ResponseValueTimbreJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempValue = from val in db.ccee_ValorTimbre
                                join tim in db.ccee_Timbre on val.VaT_NoValorTimbre equals tim.fk_ValorTimbre
                                join hoj in db.ccee_HojaTimbre on tim.fk_HojaTimbre equals hoj.HoT_NoHojaTimbre
                                where tim.Tim_Estatus == Constant.timbre_status_enable && 
                                hoj.fk_Cajero == id
                                select new ResponseValueTimbreJson()
                                {
                                    id = val.VaT_NoValorTimbre,
                                    name = val.VaT_Nombre,
                                    denomination = val.VaT_Denominacion
                                };

                tempValue = tempValue
                    .GroupBy(o => new
                    {
                        Val = o.id,
                    })
                    .Select(g => new ResponseValueTimbreJson()
                    {
                        id = g.Key.Val,
                        name = g.Select(p=>p.name).FirstOrDefault(),
                        denomination = g.Select(p=>p.denomination).FirstOrDefault()
                    })
                    .OrderBy(a => a.id);


                Autorization.saveOperation(db, autorization, "Obteniendo valor timbre");
                return tempValue;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_ValueTimbre
        public IHttpActionResult Post(RequestValueTimbreJson valueTimbre)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_ValorTimbre tempValueTimbre = new ccee_ValorTimbre()
                {
                    VaT_Nombre = valueTimbre.name,
                    VaT_Denominacion = valueTimbre.denomination
                };

                db.ccee_ValorTimbre.Add(tempValueTimbre);
                Autorization.saveOperation(db, autorization, "Agregando valor timbre");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_ValueTimbre/5
        public IHttpActionResult Put(int id, RequestValueTimbreJson valueTimbre)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_ValorTimbre tempValueTimbre = db.ccee_ValorTimbre.Find(id);

                if (tempValueTimbre == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempValueTimbre.VaT_Nombre = valueTimbre.name;
                tempValueTimbre.VaT_Denominacion = valueTimbre.denomination;

                db.Entry(tempValueTimbre).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando valor timbre");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_ValueTimbre/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_ValorTimbre tempValueTimbre = db.ccee_ValorTimbre.Find(id);
                if (tempValueTimbre == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_ValorTimbre.Remove(tempValueTimbre);
                Autorization.saveOperation(db, autorization, "Eliminando valor timbre");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

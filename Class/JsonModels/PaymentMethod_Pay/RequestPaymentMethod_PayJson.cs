﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.PaymentMethod_Pay
{
    public class RequestPaymentMethod_PayJson
    {
        public int idPaymentMethod { get; set; }
        public int idBank { get; set; }
        public decimal amount { get; set; }
        public string noTransaction { get; set; }
        public string noDocument { get; set; }
        public int reservation { get; set; }
        public int rejection { get; set; }
        public string remarke { get; set; }
    }
}
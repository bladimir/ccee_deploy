﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonHistorialActivoInfo
    {
        public string nombre { get; set; }
        public string noColegiado { get; set; }
        public string fecha
        {
            get
            {
                return ((tempFecha == null) ? "" : tempFecha.ToString("dd/MM/yyyy"));
            }
        }
        public DateTime tempFecha { get; set; }
        public string edad { get; set; }
        public string mesRestante { get; set; }
        public string cuotasFaltantes { get; set; }
        public string mesesActivos { get; set; }
        public string montoPagado { get; set; }
        public string montoTotal { get; set; }
    }
}
﻿using Microsoft.PowerBI.Api.V1;
using Microsoft.PowerBI.Api.V1.Models;
using Microsoft.PowerBI.Security;
using Microsoft.Rest;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webcee.Class;
using webcee.Class.Constant;
using webcee.Class.Globals;
using webcee.Models;
using webcee.ViewModels;

namespace webcee.Controllers
{
    public class ReportsController : Controller
    {

        private string workspaceCollection;
        private string workspaceId;
        private string reportId;
        private readonly string accessKey;
        private readonly string apiUrl;
        private CCEEEntities db = new CCEEEntities();


        public ReportsController()
        {
            this.accessKey = ConfigurationManager.AppSettings["powerbi:AccessKey"];
            this.apiUrl = ConfigurationManager.AppSettings["powerbi:ApiUrl"];
        }


        public ActionResult ListReferre()
        {
            //if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews) == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            return View();
        }

        public ActionResult DocumentosPendientes()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_documentos) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            return View();
        }

        public ActionResult CalidadyEstatusSaldos()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_calidad_est) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult PadronAsambleas()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_padron) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            return View();
        }

        public ActionResult Etiquetas()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_etiqueta) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            return View();
        }

        public ActionResult Error()
        {
            //if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, "") == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_login_controller);
            return View();
        }

        public ActionResult ListadoGeneralCol()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_listado_col) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            return View();
        }

        public ActionResult ListadoGeneralColFiltro()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_listado_col) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            ViewBag.nameCashier = login.User.Usu_Nombre + " " + login.User.Usu_Apellido;
            return View();
        }

        public ActionResult CalidadEstatusSaldo()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_calidad_est) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult ReporteColegiado()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_colegiado) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            return View();
        }

        public ActionResult FichaColegiado()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_ficha) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult SaldoColegiado()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_saldo) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult ReporteIngreso()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_ingresos) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult ConsolidadoDeudas()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_consolidados) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult CalculoActuario()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_calculo_actuario) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult DesgloseCuentaNColegiado()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_desglose_n) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            return View();
        }

        public ActionResult RecibosOperados()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_recibos) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            return View();
        }

        public ActionResult ChequesRechazados()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_cheques) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            return View();
        }

        public ActionResult DesgloseCuentas()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_desglose_c) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            return View();
        }

        public ActionResult EstadoCuentaColegiado()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_estado) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult EstadoCuentaConReciboColegiado()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_estado) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult EstadisticaPie()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_estadisticas) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult CorteCaja()
        {
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_ejecutar_caja) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult CorteCajaCaja()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_caja) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }


        public ActionResult CorteCajaRango()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_corte_caja) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            return View();
        }

        public ActionResult HistoricoActivo()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_historico_ac) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult FichaResumida()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_ficha_resumida) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult Directorio()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_directorio) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            return View();
        }

        public ActionResult Timbres()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_timbre) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            return View();
        }

        public ActionResult TimbreBodega()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_timbre_bod) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.nameCashier = login.User.Usu_Nombre + " " + login.User.Usu_Apellido;
            return View();
        }

        public ActionResult TimbreIngresoBodega()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_ingreso_timbre_bod) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.nameCashier = login.User.Usu_Nombre + " " + login.User.Usu_Apellido;
            return View();
        }

        public ActionResult TimbreEntregaBodegaCaj()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_entrega_timbre_caj) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.nameCashier = login.User.Usu_Nombre + " " + login.User.Usu_Apellido;
            return View();
        }

        public ActionResult TimbreResumenCajeroDia()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_resumen_caja_dia) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.nameCashier = login.User.Usu_Nombre + " " + login.User.Usu_Apellido;
            return View();
        }

        public ActionResult Asamblea()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_asamblea) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult Tesis()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_tesis) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult TimbresResumenInventario()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_resumen_inventario_tim) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            ViewBag.nameCashier = login.User.Usu_Nombre + " " + login.User.Usu_Apellido;
            return View();
        }

        public ActionResult TimbresEntradaSalida()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_entradas_salidas) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            ViewBag.nameCashier = login.User.Usu_Nombre + " " + login.User.Usu_Apellido;
            return View();
        }

        public ActionResult TimbresCajero()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_timbre_caj) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idCashier = login.idCashier;
            ViewBag.nameCashier = login.User.Usu_Nombre + " " + login.User.Usu_Apellido;
            return View();
        }

        public ActionResult Certificaciones()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_certificacion) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult Carnet()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_reporte_carnet) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult HistorialUsuario()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_historial_usu) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ViewBag.idUser = login.User.Usu_NoUsuario;
            return View();
        }

        public ActionResult PruebaData()
        {
            //if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, "") == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_login_controller);

            Mathos.Parser.MathParser parser = new Mathos.Parser.MathParser();

            string expr = "(x+(2*x)/(1-x))"; // the expression

            decimal result = 0; // the storage of the result

            parser.LocalVariables.Add("x", 41); // 41 is the value of x

            result = parser.Parse(expr); // parsing

            Console.WriteLine(result); // 38.95

            return View();
        }


        // GET: Reports
        public ActionResult Graphics()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, "") == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            return View();
        }

        public ActionResult Reports()
        {
            using (var client = this.CreatePowerBIClient())
            {
                var reportsResponse = client.Reports.GetReports("ccee-embedded", "7dca6810-f11e-4f46-93ca-c358710283b0");

                List<Report> listado = reportsResponse.Value.ToList();
                


                return View();
            }
        }


        public async System.Threading.Tasks.Task<ActionResult> ReportGraph(int id)
        {

            
            try
            {
                ccee_Grafica graph = this.getGraphic(id);

                if (graph == null) return RedirectToAction("Graphics");

                reportId = graph.GRA_ReportId;
                workspaceCollection = graph.GRA_WorkspaceCollection;
                workspaceId = graph.GRA_WorkspaceId;

                using (var client = this.CreatePowerBIClient())
                {
                    var reportsResponse = await client.Reports.GetReportsAsync(this.workspaceCollection, this.workspaceId);
                    var report = reportsResponse.Value.FirstOrDefault(r => r.Id == this.reportId);
                    var embedToken = PowerBIToken.CreateReportEmbedToken(this.workspaceCollection, this.workspaceId, report.Id);

                    var viewModel = new ReportViewModel
                    {
                        Report = report,
                        AccessToken = embedToken.Generate(this.accessKey)
                    };

                    return View(viewModel);
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Graphics");
            }
            
        }


        private IPowerBIClient CreatePowerBIClient()
        {
            var credentials = new TokenCredentials(accessKey, "AppKey");
            var client = new PowerBIClient(credentials)
            {
                BaseUri = new Uri(apiUrl)
            };

            return client;
        }

        private ccee_Grafica getGraphic(int id)
        {
            try
            {
                ccee_Grafica tempGraph = db.ccee_Grafica.Find(id);
                return tempGraph;
            }
            catch (Exception)
            {
                return null;
            }
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
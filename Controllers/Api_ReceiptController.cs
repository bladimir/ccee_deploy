﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.Receipt;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_ReceiptController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Receipt
        public IEnumerable<ResponseReceiptJson> Get()
        {
            return null;
        }

        // GET: api/Api_Receipt/5
        public IEnumerable<ResponseReceiptJson> Get(string id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                string[] listDiv = id.Split('$');
                

                if (listDiv.Count() > 1)
                {
                    string data = listDiv[1];

                    var temDoc = from doc in db.ccee_DocContable
                                 where doc.DCo_NoRecibo.Contains(data) 
                                 orderby doc.DCo_FechaHora descending
                                 select new ResponseReceiptJson()
                                 {
                                     id = doc.DCo_NoDocContable,
                                     name = doc.DCo_ANombre,
                                     date = doc.DCo_FechaHora,
                                     direction = Constant.url_dir_print_doc + doc.DCo_NoDocContable,
                                     delete = doc.DCo_Eliminado
                                 };
                    Autorization.saveOperation(db, autorization, "Obteniendo recibos");

                    return temDoc;

                }
                else 
                {
                    var temDoc = from doc in db.ccee_DocContable
                                 where doc.DCo_NoRecibo == id
                                 orderby doc.DCo_FechaHora descending
                                 select new ResponseReceiptJson()
                                 {
                                     id = doc.DCo_NoDocContable,
                                     name = doc.DCo_ANombre,
                                     date = doc.DCo_FechaHora,
                                     direction = Constant.url_dir_print_doc + doc.DCo_NoDocContable,
                                     delete = doc.DCo_Eliminado
                                 };
                    Autorization.saveOperation(db, autorization, "Obteniendo recibos");

                    return temDoc;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        [Route("api/Api_Receipt/{id}/{idReceipt}")]
        public ResponseReceiptJson Get(int id, string idReceipt)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                var temDoc = (from doc in db.ccee_DocContable
                              where doc.DCo_NoDocContable == id
                              select new ResponseReceiptJson()
                              {
                                  id = doc.DCo_NoDocContable,
                                  name = doc.DCo_ANombre,
                                  date = doc.DCo_FechaHora,
                                  direction = Constant.url_dir_print_doc + doc.DCo_NoDocContable,
                                  delete = doc.DCo_Eliminado
                              }).FirstOrDefault();
                Autorization.saveOperation(db, autorization, "Obteniendo recibo");

                return temDoc;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_Receipt
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_Receipt/5
        public IHttpActionResult Put(int id, RequestReceiptJson receipt)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {

                    string autorization = Request.Headers.Authorization.Parameter;
                    if (!Autorization.enableAutorization(db, autorization)) return null;
                    ccee_DocContable doc = db.ccee_DocContable.Find(id);

                    if (receipt.noReciber != null)
                    {
                        doc.DCo_NoRecibo = receipt.noReciber;
                    }

                    if (receipt.dateReciber != null)
                    {
                        DateTime dateTemp = receipt.dateReciber.Value.Date;
                        doc.DCo_FechaHora = receipt.dateReciber.Value.Date;
                        Payment payment = new Payment(db);
                        List<int?> listPay = payment.findIdPayWithDoc(id);
                        if (listPay.Count() > 0)
                        {
                            for (int i = 0; i < listPay.Count(); i++)
                            {
                                if (listPay[i] == null) continue;
                                ccee_Pago tempPay = db.ccee_Pago.Find(listPay[i]);
                                tempPay.Pag_Fecha = receipt.dateReciber.Value.Date;
                                db.Entry(tempPay).State = EntityState.Modified;
                            }
                        }
                    }

                    db.Entry(doc).State = EntityState.Modified;
                    Autorization.saveOperation(db, autorization, "Actualizando recibo");
                    db.SaveChanges();
                    transaction.Commit();
                    return Ok(HttpStatusCode.OK);
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return Ok(HttpStatusCode.BadRequest);
                }
            }
        }

        // DELETE: api/Api_Receipt/5
        //public IHttpActionResult Delete(int id)
        //{
        //    using (var transaction = db.Database.BeginTransaction())
        //    {
        //        try
        //        {
        //            string autorization = Request.Headers.Authorization.Parameter;
        //            if (!Autorization.enableAutorization(db, autorization)) return null;
        //            Payment payment = new Payment(db);

        //            //List<ccee_Documento> tempDocument;
        //            //List<ccee_CargoEconomico_otros> tempOther;
        //            //ccee_Documento doc;
        //            List<int?> listPay = payment.findIdPayWithDoc(id);
        //            if(listPay.Count() > 0)
        //            {
        //                bool statusReverse = true;
        //                foreach (int idPay in listPay)
        //                {
        //                    statusReverse = payment.reversePayment(idPay);
        //                    if (statusReverse == false) break;
        //                }
                        
        //                if (statusReverse) {

        //                    ccee_DocContable tempDoc = db.ccee_DocContable.Find(id);
        //                    tempDoc.DCo_Eliminado = 1;
        //                    db.Entry(tempDoc).State = EntityState.Modified;

        //                    ccee_Batch tempBatch = db.ccee_Batch.Where(p => p.fk_DocContable == id)
        //                                                .FirstOrDefault();
        //                    if(tempBatch != null)
        //                    {
        //                        tempBatch.Bat_Eliminado = 1;
        //                        db.Entry(tempBatch).State = EntityState.Modified;
        //                    }
                            
                            
        //                    //Revertir timbre
        //                    foreach (int idPay in listPay)
        //                    {
        //                        var timbres = from tim in db.ccee_Timbre
        //                                      where tim.fk_Pago == idPay
        //                                      select new
        //                                      {
        //                                          id = tim.Tim_NoTimbre,
        //                                          idPay = tim.fk_Pago
        //                                      };
        //                        foreach (var timbre in timbres)
        //                        {
        //                            ccee_Timbre tempTimbre = db.ccee_Timbre.Find(timbre.id);
        //                            tempTimbre.fk_Pago = null;
        //                            tempTimbre.Tim_Estatus = Constant.timbre_status_enable;
        //                            db.Entry(tempDoc).State = EntityState.Modified;
        //                        }

        //                        var tempOther = db.ccee_CargoEconomico_otros.Where(p => p.fk_Pago == idPay).ToList();
        //                        //var tempOther = (from ecoO in db.ccee_CargoEconomico_otros
        //                        //             where ecoO.fk_DocContable == id
        //                        //             select ecoO).ToList();

        //                        foreach (var other in tempOther)
        //                        {
        //                            //tempDocument = db.ccee_Documento
        //                            //    .Where(p => p.fk_CargoEconomicoOtros == other.CEcO_NoCargoEconomico).ToList();

        //                            var tempDocument = (from docO in db.ccee_Documento
        //                                            where docO.fk_CargoEconomicoOtros == other.CEcO_NoCargoEconomico
        //                                            select docO).ToList();

        //                            ccee_Documento doc;
        //                            foreach (var document in tempDocument)
        //                            {
        //                                doc = db.ccee_Documento.Find(document.Doc_NoDocumento);
                                        
        //                                if (doc == null)
        //                                {
        //                                    continue;
        //                                }

        //                                doc.Doc_FechaHoraConsumo = DateTime.Now;
        //                                doc.Doc_Observacion = "Invalidado por CCEE";
        //                                doc.Doc_Estado = 2;
        //                                db.Entry(doc).State = EntityState.Modified;

        //                                doc = null;
        //                            }
        //                            tempDocument = null;
        //                        }
        //                        tempOther = null;



        //                    }
                            
        //                    db.SaveChanges();
        //                    Autorization.saveOperation(db, autorization, "Eliminando recibo");
        //                    transaction.Commit();
        //                    return Ok(HttpStatusCode.OK);
        //                }
        //                transaction.Rollback();
        //                return Ok(HttpStatusCode.NotModified);
        //            }
                    
        //            transaction.Rollback();
        //            return Ok(HttpStatusCode.NotFound);

        //        }
        //        catch (Exception)
        //        {
        //            transaction.Rollback();
        //            return Ok(HttpStatusCode.BadRequest);
        //        }
        //    }
        //}


        // DELETE: api/Api_Receipt/5
        [Route("api/Api_Receipt/{id}/{status}")]
        public IHttpActionResult Delete(int id, int status)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    string autorization = Request.Headers.Authorization.Parameter;
                    if (!Autorization.enableAutorization(db, autorization)) return null;
                    Payment payment = new Payment(db);
                    ccee_DocContable tempDoc = db.ccee_DocContable.Find(id);
                    if (tempDoc != null)
                    {


                        //List<ccee_Documento> tempDocument;
                        //List<ccee_CargoEconomico_otros> tempOther;
                        //ccee_Documento doc;
                        List<int?> listPay = payment.findIdPayWithDoc(id);
                        listPay = listPay.Where(p => p != null).ToList();
                        if (listPay.Count() > 0)
                        {

                            bool statusReverse = true;
                            foreach (int? idPay in listPay)
                            {
                                if (idPay == null) continue;

                                var tempOther = (from ecoO in db.ccee_CargoEconomico_otros
                                                 where ecoO.fk_Pago == idPay
                                                 select new
                                                 {
                                                     id = ecoO.CEcO_NoCargoEconomico
                                                 });



                                foreach (var other in tempOther)
                                {
                                    //tempDocument = db.ccee_Documento
                                    //    .Where(p => p.fk_CargoEconomicoOtros == other.CEcO_NoCargoEconomico).ToList();

                                    var tempDocument = (from docO in db.ccee_Documento
                                                        where docO.fk_CargoEconomicoOtros == other.id
                                                        select docO).ToList();

                                    ccee_Documento doc;
                                    foreach (var document in tempDocument)
                                    {
                                        doc = db.ccee_Documento.Find(document.Doc_NoDocumento);

                                        if (doc == null)
                                        {
                                            continue;
                                        }

                                        doc.Doc_FechaHoraConsumo = DateTime.Now;
                                        doc.Doc_Observacion = "Invalidado por CCEE";
                                        doc.Doc_Estado = 2;
                                        db.Entry(doc).State = EntityState.Modified;

                                        doc = null;
                                    }
                                    tempDocument = null;
                                }
                                tempOther = null;

                                List<ccee_CargoEconomico_timbre> listTimbre = (db.ccee_CargoEconomico_timbre
                                                                .Where(q => q.fk_Pago == idPay &&
                                                                q.fk_TipoCargoEconomico == Constant.type_economic_mora)).ToList();


                                statusReverse = payment.reversePayment(idPay.Value);
                                if (statusReverse == false) break;
                                db.ccee_CargoEconomico_timbre.RemoveRange(listTimbre);
                                Debug.WriteLine("F idPay: " + idPay);
                            }

                            if (statusReverse)
                            {

                                //ccee_DocContable tempDoc = db.ccee_DocContable.Find(id);
                                tempDoc.DCo_Eliminado = status;
                                if (status == 2)
                                {
                                    tempDoc.DCo_NoRecibo = tempDoc.DCo_NoRecibo + "_A";
                                }
                                db.Entry(tempDoc).State = EntityState.Modified;

                                ccee_Batch tempBatch = db.ccee_Batch.Where(p => p.fk_DocContable == id)
                                                            .FirstOrDefault();
                                if (tempBatch != null)
                                {
                                    tempBatch.Bat_Eliminado = 1;
                                    db.Entry(tempBatch).State = EntityState.Modified;
                                }


                                //Revertir timbre
                                foreach (int idPay in listPay)
                                {
                                    var timbres = from tim in db.ccee_Timbre
                                                  where tim.fk_Pago == idPay
                                                  select new
                                                  {
                                                      id = tim.Tim_NoTimbre,
                                                      idPay = tim.fk_Pago
                                                  };
                                    foreach (var timbre in timbres)
                                    {
                                        ccee_Timbre tempTimbre = db.ccee_Timbre.Find(timbre.id);
                                        tempTimbre.fk_Pago = null;
                                        tempTimbre.Tim_Estatus = Constant.timbre_status_enable;
                                        db.Entry(tempDoc).State = EntityState.Modified;
                                    }
                                    


                                }
                                // revertir timbres por batch

                                var timbresBatch = from tim in db.ccee_Timbre
                                                   where tim.fk_Nomina == id
                                                   select new
                                                   {
                                                       id = tim.Tim_NoTimbre,
                                                       idPay = tim.fk_Pago
                                                   };
                                if (timbresBatch != null)
                                {
                                    foreach (var timbre in timbresBatch)
                                    {
                                        ccee_Timbre tempTimbre = db.ccee_Timbre.Find(timbre.id);
                                        tempTimbre.fk_Pago = null;
                                        tempTimbre.Tim_Estatus = Constant.timbre_status_enable;
                                        db.Entry(tempDoc).State = EntityState.Modified;
                                    }
                                }
                                

                                db.SaveChanges();
                                Autorization.saveOperation(db, autorization, "Eliminando recibo");

                                if (!statusReverse)
                                {
                                    transaction.Rollback();
                                    return Ok(HttpStatusCode.NotModified);
                                }
                                transaction.Commit();
                                //transaction.Rollback();
                                return Ok(HttpStatusCode.OK);
                            }
                            transaction.Rollback();
                            return Ok(HttpStatusCode.NotModified);
                        }
                        else //Cuando los recibos no tienen pagos 
                        {
                            tempDoc.DCo_Eliminado = status;
                            if (status == 2)
                            {
                                tempDoc.DCo_NoRecibo = tempDoc.DCo_NoRecibo + "_A";
                            }
                            db.Entry(tempDoc).State = EntityState.Modified;

                            ccee_Batch tempBatch = db.ccee_Batch.Where(p => p.fk_DocContable == id)
                                                        .FirstOrDefault();
                            if (tempBatch != null)
                            {
                                tempBatch.Bat_Eliminado = 1;
                                db.Entry(tempBatch).State = EntityState.Modified;
                            }
                            db.SaveChanges();
                            Autorization.saveOperation(db, autorization, "Eliminando recibo");
                            transaction.Commit();
                            return Ok(HttpStatusCode.OK);
                        }

                        
                    }else
                    {
                        transaction.Rollback();
                        return Ok(HttpStatusCode.NotFound);
                    }
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return Ok(HttpStatusCode.BadRequest);
                }
            }
        }




        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

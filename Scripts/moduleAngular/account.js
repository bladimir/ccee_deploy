﻿(function () {

    var app = angular.module('account', ['vcRecaptcha', 'ngMaterial']);

    app.controller("reset", ['$scope', '$http',  '$anchorScroll', 'CONSTANTE', '$window', function ($scope, $http, $anchorScroll, CONSTANTE, $window) {
        
        $scope.showLoadingFunction = function () {
            $anchorScroll();
            $scope.showNotify = CONSTANTE.NOTIFY_SHOW_CSS;
        }

        $scope.hideLoadingFunction = function () {
            $scope.showNotify = CONSTANTE.NOTIFY_HIDE_CSS;
        }

        var funciones = this;
        $scope.regexNit = CONSTANTE.REGEX_NIT;
        $scope.regexMail = CONSTANTE.REGEX_EMAIL;
        $scope.regexPhone = CONSTANTE.REGEX_PHONE;
        $scope.regexPhoneState = CONSTANTE.REGEX_STATEPHONE;
        $scope.showNotify = 'emergente1 notify-event-content';

        $scope.MsjRegister = false;
        $scope.ErrorRegister = false;



        $scope.showQuestion = true;
        $scope.showError = false;
        $scope.showError1 = false;
        $scope.showPassword = false;
        $scope.textQuestion = "";
        $scope.errorRepeatPass = "";
        $scope.errorPassword = "";
        $scope.regexEmail = CONSTANTE.REGEX_EMAIL;
        $scope.email = "";

        $scope.onInit = function () {
            $scope.hideLoadingFunction();
        }

        $scope.cbExpiration = function () {
            // reset the 'response' object that is on scope
            $scope.rcaptcha = null;
        };

        $scope.Registrar = function () {
            if (
                $scope.idReferee == null ||
                $scope.FNRef == null ||
                $scope.FSNRef == null ||
                $scope.mailReferee == null ||
                $scope.mailRefereeC == null ||
                $scope.telR == null ||
                $scope.nitR == null ||
                $scope.datenac == null ||
                $scope.terminosycon == null ||
                $scope.rcaptcha == null
                ) {
                $scope.ErrorRegister = true;
            } else if (
                $scope.idReferee != null &&
                $scope.FNRef != null &&
                $scope.FSNRef != null &&
                $scope.mailReferee != null &&
                $scope.mailRefereeC != null &&
                $scope.telR != null &&
                $scope.nitR != null &&
                $scope.datenac != null &&
                $scope.terminosycon != null &&
                $scope.rcaptcha != null
                ) {
                console.log('Correcto!');
                if ($scope.mailReferee == $scope.mailRefereeC) {
                    $scope.showLoadingFunction();
                    var informacion = {
                        idR: $scope.idReferee,
                        FNRef: $scope.FNRef,
                        SNRef: $scope.SNRef,
                        FSNRef: $scope.FSNRef,
                        SSNRef: $scope.SSNRef,
                        mailReferee: $scope.mailReferee,
                        mailRefereeC: $scope.mailRefereeC,
                        telR: $scope.telR,
                        nitR: $scope.nitR,
                        cuiR: $scope.cuiR,
                        datenac: $scope.datenac,
                        terminosycon: $scope.terminosycon,
                        rcaptcha: $scope.rcaptcha
                    }
                    $scope.showLoadingFunction();
                    $http.post('/api/Registrar', JSON.stringify(informacion),
                        {
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        })
                        .success(function (data) {
                            if (data.Result) {
                                $scope.hideLoadingFunction();
                                $scope.MsjRegister = true;
                                $scope.ErrorMsj = data.Mensaje;//"Registro exitoso, verifique su bandeja de entrada para obtener la clave temporal de acceso a la plataforma virtual.";
                            } else {
                                $scope.hideLoadingFunction();
                                $scope.MsjRegister = true;
                                $scope.ErrorMsj = data.Mensaje;
                            }

                        })
                        .error(function (data) {
                            $scope.hideLoadingFunction();
                            console.log("Error");
                        });




                } else {
                    $scope.MsjRegister = true;
                    $scope.ErrorMsj = "Correos electronicos ingresados no coinciden, por favor verifique.";
                }
                $scope.ErrorRegister = false;
            }
        };



        $scope.submit1 = function () {
            if ($scope.idReferee == null || $scope.mailReferee == null
                || $scope.rcaptcha == null
                ) {
                $scope.showError = true;
            } else if ($scope.idReferee != null && $scope.mailReferee != null
                && $scope.rcaptcha != null
                ) {
                $scope.showError = false;
                var informacion = {
                    idRef: $scope.idReferee,
                    mail: $scope.mailReferee
                }
                $scope.showLoadingFunction();
                $http.post('/api/RestorePassword', JSON.stringify(informacion),
                        {
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        })
                        .success(function (data) {
                            if (data.Result) {
                                $scope.hideLoadingFunction();
                                $scope.showError1 = true;
                                $scope.ErrorMsj = "Clave temporal enviada correctamente, por favor revise su bandeja de entrada.";
                            } else {
                                $scope.hideLoadingFunction();
                                $scope.showError1 = true;
                                $scope.ErrorMsj = data.Mensaje;
                            }

                        })
                        .error(function (data) {
                            console.log("Error");
                            $scope.hideLoadingFunction();
                        });

            }
        };

        $scope.getQuestion = function (form, email) {
            var informacion = {
                type: "question",
                email: email.email
            }

            $http.post('/api/Api_Account', JSON.stringify(informacion),
                        {
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        })
                        .success(function (data) {
                            console.log(data);
                            if (data != 400) {
                                $scope.email = email.email;
                                $scope.textQuestion = "¿ "+data+" ?";
                                $scope.changeQuestion();
                            }
                        })
                        .error(function (data) {
                            console.log('Error');
                            console.log(data);
                        });
        }

        $scope.changeQuestion = function () {
            $scope.showQuestion = !$scope.showQuestion;
        }

        $scope.confirmQuestion = function (form, answer, email) {
            var informacion = {
                type: "answer",
                email: email,
                answer: answer.answer
            }
            console.log(informacion);
            $http.post('/api/Api_Account', JSON.stringify(informacion),
                        {
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        })
                        .success(function (data) {
                            console.log(data);
                            if (data == 200) {
                                $scope.showPassword = true;
                            }
                        })
                        .error(function (data) {
                            console.log('Error');
                            console.log(data);
                        });
        }


        $scope.changePassword = function (form, pass, email) {
            console.log(pass);
            var informacion = {
                email: email,
                pass: pass.password
            }
            console.log(informacion);

            $http.put('/api/Api_Account', JSON.stringify(informacion),
                        {
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        })
                        .success(function (data) {
                            console.log(data);
                            if (data == 200) {
                                window.location.href = '/Home/Index';
                            }
                        })
                        .error(function (data) {
                            console.log('Error');
                            console.log(data);
                        });
        }

        $scope.$watch('password.repeat', function (text) {
            if (text != null) {
                if ($scope.password.password !== text) {
                    $scope.errorRepeatPass = "Contraseñas no parecidas";
                } else {
                    $scope.errorRepeatPass = "";
                }
            }
        })

        $scope.$watch('password.password', function (text) {

            if (text != null) {
                $scope.infoPass = true;
                $scope.errorPassword = '';

                if (funciones.isValidCaracteresMinimos(text)) $scope.errorPassword += '';
                else $scope.errorPassword += ',' + CONSTANTE.MENSAJE_CARACTERES_ERROR;


                if (funciones.isContieneDigito(text)) $scope.errorPassword += '';
                else $scope.errorPassword += ',' + CONSTANTE.MENSAJE_NUMEROS_ERROR;

                if (funciones.isContieneMayusculas(text)) $scope.errorPassword += '';
                else $scope.errorPassword += ',' + CONSTANTE.MENSAJE_MAYUSCULA_ERROR;

                if (funciones.isValidCaracteresMaximos(text)) $scope.errorPassword += '';
                else $scope.errorPassword += ',' + CONSTANTE.MENSAJE_CARACTERES_MAX_ERROR;

                //if ($scope.nuevo.caracteres == '' && $scope.nuevo.digitos == '' && $scope.nuevo.mayusculas == '' && $scope.nuevo.maximo == '')
                //    $scope.infoPass = false;
                if ($scope.errorPassword != '') $scope.errorPassword = "La contraseña no contiene: " + $scope.errorPassword;
                else $scope.errorPassword = "";


            } else {

            }

        })

        //Congunto de validaciones para el password
        this.isValidCaracteresMinimos = function (texto) {
            return texto.length >= CONSTANTE.CARACTERES_MINIMOS;
        }

        this.isValidCaracteresMaximos = function (texto) {
            return texto.length <= CONSTANTE.CARACTERES_MAXIMOS;
        }

        this.isContieneDigito = function (texto) {
            return CONSTANTE.REGEX_NUMEROS.test(texto);
        }

        this.isContieneMayusculas = function (texto) {
            return CONSTANTE.REGEX_MAYUSCULA.test(texto);
        }

        this.isFecha = function (texto) {
            return CONSTANTE.REGEX_FECHA.test(texto);
        }
        //Fin del conjunto de validaciones


    }])



})();
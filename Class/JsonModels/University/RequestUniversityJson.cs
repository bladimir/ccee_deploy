﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.University
{
    public class RequestUniversityJson
    {
        public string name { get; set; }
        public String description { get; set; }
    }
}
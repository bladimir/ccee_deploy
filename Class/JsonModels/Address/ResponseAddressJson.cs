﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Address
{
    public class ResponseAddressJson
    {
        public int idAddress { get; set; }
        public int status { get; set; }
        public string street { get; set; }
        public string number { get; set; }
        public string zone { get; set; }
        public string colony { get; set; }
        public string caserio { get; set; }
        public string barrio { get; set; }
        public int? typeAddress { get; set; }
        public string municipality { get; set; }
        public string departament { get; set; }
        public string country { get; set; }
        public int? notification { get; set; }

    }
}
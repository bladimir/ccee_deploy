﻿(function () {
    angular.module('generals')
        .controller('refereeReceipt', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listReceipt;
            $scope.listEconomic;
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.showEconomic = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
            }


            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getReceipt = function () {
                $scope.cleanData();
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_REFEREERECEIPT, $scope.idReferee)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listReceipt = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }
            
            $scope.getEconomic = function (recipt) {

                $scope.showLoadingFunction();
                generalactionsFactory.actionGetGetIntFunction(CONSTANT.URL_API_REFEREERECEIPT, $scope.idReferee, recipt.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listEconomic = response.data;
                        $scope.showEconomic = 1;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            

            $scope.cleanData = function () {
                $scope.listReceipt = null;
                $scope.listEconomic = null;
                $scope.showEconomic = 0;
            }


        }]);

})()
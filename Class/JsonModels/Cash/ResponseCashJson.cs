﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Cash
{
    public class ResponseCashJson
    {
        public int id { get; set; }
        public string name { get; set; }
        public string activo { get; set; }
    }
}
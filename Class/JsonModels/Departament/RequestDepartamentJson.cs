﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Departament
{
    public class RequestDepartamentJson
    {
        public string name { get; set; }
        public int idCountry { get; set; }
    }
}
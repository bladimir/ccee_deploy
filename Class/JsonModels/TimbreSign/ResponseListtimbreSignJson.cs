﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.TimbreSign
{
    public class ResponseListtimbreSignJson
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime? date { get; set; }
        public string key { get; set; }
        public int? status { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.DocumentAccouting
{
    public class listDocumentJson
    {
        public int idBoleta { get; set; }
        public string codigo { get; set; }
        public string noAccount { get; set; }
        public string noDeposit { get; set; }
        public DateTime? date { get; set; }
        public string namebank { get; set; }
        public string format { get; set; }
        public decimal total { get; set; }
        public decimal valor1 { get; set; }
        public decimal valor2 { get; set; }
        public decimal valor3 { get; set; }
        public decimal valor4 { get; set; }
        public decimal valor5 { get; set; }
        public decimal valor6 { get; set; }
        public decimal valor7 { get; set; }
        public decimal valor8 { get; set; }
        public decimal valor9 { get; set; }
        public decimal valor10 { get; set; }
        public decimal valor11 { get; set; }
        public decimal valor12 { get; set; }
        public decimal valor13 { get; set; }
        public decimal valor14 { get; set; }
        public decimal valor15 { get; set; }
        public decimal valor16 { get; set; }
        public decimal valor17 { get; set; }
        public decimal valor18 { get; set; }
        public decimal valor19 { get; set; }
        public decimal valor20 { get; set; }

        //Constantes
        public string codDocument { get { return "DEP"; } }
        public string motivo { get { return "DEPOSITOS DEL DIA"; } }
        public string centerCost { get { return "1201"; } }
        public string numbrePa { get { return ""; } }
        public string negociable { get { return "N"; } }
        public string amountDolar { get { return "0"; } }
        public string typeChange { get { return "1"; } }
        public string departamento { get { return ""; } }


        //De utilidad
        public int idDocument { get; set; }
        public List<int> idDocuments { get; set; }
    }
}
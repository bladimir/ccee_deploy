﻿(function () {
    angular.module('document')
        .controller('typeDocument', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'documentFactory', function ($scope, $http, CONSTANT, $anchorScroll, documentFactory) {
            $scope.listData;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idUpdate = 0;
            $scope.objectUpdate;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                documentFactory.actionGetFunction(CONSTANT.URL_API_TYPEDOCUMENT)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listData = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.setAuth = function () {
                documentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        documentFactory.setAuthorization(response.data);
                        $scope.init();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            


            $scope.get = function () {
                $scope.showLoadingFunction();
                documentFactory.actionGetFunction(CONSTANT.URL_API_TYPEDOCUMENT)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listData = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.add = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.typename,
                    description: $scope.typedescription,
                    templateXML: $scope.typetemplate,
                    consumable: $scope.typeconsumableselect
                }
                console.log(information);
                documentFactory.actionPostFunction(CONSTANT.URL_API_TYPEDOCUMENT, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.get();
                                $scope.cleanData();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (data) {
                $scope.objectUpdate = data;
                $scope.idUpdate = data.id;
                $scope.typename = data.name;
                $scope.typedescription = data.description;
                $scope.typetemplate = data.templateXML;
                $scope.typeconsumable = data.consumable;
                $anchorScroll();
            }

            $scope.update = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: $scope.typename,
                    description: $scope.typedescription,
                    templateXML: $scope.typetemplate,
                    consumable: (($scope.typeconsumableselect == null) ? $scope.objectUpdate.consumable : $scope.typeconsumableselect )
                }
                documentFactory.actionPutFunction(CONSTANT.URL_API_TYPEDOCUMENT, $scope.idUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.get();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.delete = function (data) {
                $scope.showLoadingFunction();
                documentFactory.actionDeleteFunction(CONSTANT.URL_API_TYPEDOCUMENT, data.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.get();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idUpdate = 0;
                $scope.typename = "";
                $scope.typedescription = "";
                $scope.typetemplate = "";
                $scope.typeconsumable = "";
            }

            $scope.defineConsumable = function (data) {
                if (data == 0) {
                    return "Si es consumible";
                } else if (data == 1) {
                    return "No es consumible";
                } else {
                    return "No definido";
                }
            }


        }]);

})()
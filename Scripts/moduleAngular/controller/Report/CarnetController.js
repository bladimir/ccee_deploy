﻿(function () {
    angular.module('reports')
        .controller('carnet', ['$scope', '$http', 'CONSTANT', '$anchorScroll', '$location', 'reportFactory', function ($scope, $http, CONSTANT, $anchorScroll, $location, reportFactory) {
            var idReferee = $location.search().id;
            $scope.direction;
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquaters;
            $scope.regexReferee = CONSTANT.REGEX_REFEREE;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.inReferee = idReferee;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.generateList = function () {
                var textGetReport = "";
                console.log(($scope.selectHead != null) ? $scope.selectHead.id : "");
                textGetReport = "&colegiado=" + $scope.inReferee;
                console.log(textGetReport);
                $scope.direction = "../../Reports/aspxReports/CarnetAspx.aspx?" + textGetReport;
            }

        }]);

})()
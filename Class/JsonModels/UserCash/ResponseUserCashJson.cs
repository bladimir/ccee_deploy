﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.UserCash
{
    public class ResponseUserCashJson
    {
        public int id { get; set; }
        public int? active { get; set; }
        public string initHour { get; set; }
        public string finalHour { get; set; }
        public int idUser { get; set; }
        public string name { get; set; }
        public string nameCashier { get; set; }
    }
}
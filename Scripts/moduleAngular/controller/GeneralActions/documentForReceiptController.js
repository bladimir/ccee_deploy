﻿(function () {
    angular.module('generals')
        .controller('documentForReceipt', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listUniversity;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idUniversidadUpdate = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    generalactionsFactory.setAuthorization(response.data);
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getReceipt = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_RECEIPT, $scope.numberreceipt)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listDataReceipt = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getDocs = function (idDocCont) {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_GET_LISTENEDDOCUMENTS, idDocCont)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listDataDoc = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }



        }]);

})()
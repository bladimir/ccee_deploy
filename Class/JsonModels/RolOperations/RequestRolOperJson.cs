﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.RolOperations
{
    public class RequestRolOperJson
    {
        public int idOper { get; set; }
        public int idRol { get; set; }
    }
}
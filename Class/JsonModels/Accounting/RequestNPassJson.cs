﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace webcee.Class.JsonModels.Accounting
{
    public class RequestNPassJson
    {
        public int idRef { get; set; }
        public string mail { get; set; }
    }
}
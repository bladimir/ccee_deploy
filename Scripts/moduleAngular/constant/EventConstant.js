﻿(function () {
    angular.module('event')
        .constant('CONSTANT', {
            REGEX_DIGIT: /^\d+$/,
            NOTIFY_HIDE_CSS: 'emergente1 notify-event-content',
            NOTIFY_SHOW_CSS: 'emergente2 notify-event-content',
            URL_API_EVENTASSING: '/api/Api_EventAssing',
            URL_API_EVENT: '/api/Api_Event',
            URL_API_SUBEVENT: '/api/Api_SubEvent',
            URL_API_EVENTASSISTENT: '/api/Api_EventAssistant',
            URL_API_NOREFEREEEVENT: "/api/Api_NoRefereeEvent",
            URL_API_DOCEVENT: "/api/Api_DocEvent",
            URL_GET_TOKEN: '/Users/GetToken',
            HEADER_CONTENT_TYPE_JSON: 'application/json',
            MENSAJE_SUCCESS_SUB_EVENT: "Sub Evento Guardado",
            PAGINATION_CANT: 10

        });

})();
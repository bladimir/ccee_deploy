﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.Phone;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_PhoneController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Phone
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_Phone/5
        public IEnumerable<ResponsePhoneJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempPhone = from tel in db.ccee_Telefono
                                where tel.fk_Colegiado == id
                                select new ResponsePhoneJson
                                {
                                    id = tel.Tel_NoTelefono,
                                    state = tel.Tel_CodigoPais,
                                    number = tel.Tel_NumTelefono,
                                    idfamily = tel.fk_Familiar,
                                    type = tel.Tel_TipoTelefono
                                };
                Autorization.saveOperation(db, autorization, "Obteniendo telefonos");
                return tempPhone;
            }
            catch (Exception)
            {
                return null;
            }
            
        }

        // GET: api/Api_Phone/5
        [Route("api/Api_Phone/{id}/{idM}")]
        public IEnumerable<ResponsePhoneJson> Get(int id, int idM)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempPhone = from tel in db.ccee_Telefono
                                where tel.fk_Colegiado == id && tel.fk_Familiar == idM
                                select new ResponsePhoneJson
                                {
                                    id = tel.Tel_NoTelefono,
                                    state = tel.Tel_CodigoPais,
                                    number = tel.Tel_NumTelefono
                                };
                Autorization.saveOperation(db, autorization, "Obteniendo telefonos de beneficiario");
                return tempPhone;
            }
            catch (Exception)
            {
                return null;
            }

        }

        // POST: api/Api_Phone
        public IHttpActionResult Post(RequestPhoneJson phone)
        {
            int? idFamily = phone.idFamily;
            if (idFamily == 0)
            {
                idFamily = null;
            }

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Telefono newPhone = new ccee_Telefono()
                {
                    Tel_CodigoPais = phone.cod,
                    Tel_NumTelefono = phone.number,
                    fk_Colegiado = phone.idReferee,
                    fk_Familiar= idFamily,
                    Tel_TipoTelefono = phone.type
                };

                db.ccee_Telefono.Add(newPhone);
                Autorization.saveOperation(db, autorization, "Agregando telefono");
                db.SaveChanges();
                return Ok(Constant.status_code_success);
            }
            catch (Exception)
            {
                return Ok(Constant.status_code_error);
            }

        }

        // PUT: api/Api_Phone/5
        public IHttpActionResult Put(int id, RequestPhoneJson phone)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Telefono tempPhone = db.ccee_Telefono.Find(id);
                if(tempPhone == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                if(phone.idFamily == 0)
                {
                    tempPhone.fk_Familiar = null;
                }else
                {
                    tempPhone.fk_Familiar = phone.idFamily;
                }

                db.Entry(tempPhone).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando telefono");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
            

        }

        // DELETE: api/Api_Phone/5
        public IHttpActionResult Delete(int id)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Telefono tempPhone = db.ccee_Telefono.Find(id);
                if(tempPhone == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_Telefono.Remove(tempPhone);
                Autorization.saveOperation(db, autorization, "Eliminando telefono");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);

            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

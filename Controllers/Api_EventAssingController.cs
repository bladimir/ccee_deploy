﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Constant;
using webcee.Class.JsonModels.EventAssing;
using webcee.Models;
using webcee.ViewModels;

namespace webcee.Controllers
{
    public class Api_EventAssingController : ApiController
    {

        private CCEEEntities db =  new CCEEEntities();

        // GET: api/Api_Event
        public IEnumerable<EventModelViews> Get()
        {
            return listEvent();
        }

        // GET: api/Api_Event/5
        public IEnumerable<EventModelViews> Get(int id)
        {
            return subListEvent(id);
        }

        [Route("api/Api_EventAssing/{idReferre}/{idSubEvent}")]
        public ResponseRefereeJsonEV Get(int idReferre, int idSubEvent) //int? id, int? ids
        {
            try
            {
                int stateAssign = 0;
                ccee_Colegiado ceColegiado = db.ccee_Colegiado.Single(p => p.Col_NoColegiado == idReferre);

                var ceSubCol = db.ccee_SubEventoColegiado
                               .Where(p => p.pkfk_Colegiado == idReferre)
                               .Where(q => q.pkfk_SubEvento == idSubEvent);


                foreach (ccee_SubEventoColegiado ctnSubCole in ceSubCol)
                {
                    stateAssign = ctnSubCole.SEC_Estatus;
                }


                return new ResponseRefereeJsonEV()
                {
                    name = ceColegiado.Col_PrimerNombre + " " + ceColegiado.Col_SegundoNombre + " " + ceColegiado.Col_TercerNombre,
                    lastname = ceColegiado.Col_PrimerApellido + " " + ceColegiado.Col_SegundoApellido,
                    status = ceColegiado.Col_Estatus,
                    statusAssign = stateAssign
                };
            }
            catch (Exception )
            {
                return null;
            }
        }
        

        public IHttpActionResult Post(RequestAssignJsonEV assign)
        {
            ccee_SubEventoColegiado ceSubCol = new ccee_SubEventoColegiado()
            {
                pkfk_Colegiado = assign.idReferre,
                pkfk_SubEvento = assign.idSubEvent,
                SEC_Estatus = assign.idStatus
            };
            db.ccee_SubEventoColegiado.Add(ceSubCol);
            try
            {
                db.SaveChanges();
                return Ok(Constant.status_code_success);
            }
            catch
            {
                return Ok(Constant.status_code_error);
            }
        }


        // PUT: api/Api_Event/5
        public IHttpActionResult Put(RequestAssignJsonEV assign)
        {
            ccee_SubEventoColegiado ceContSubCol = null;
            var ceSubCol = db.ccee_SubEventoColegiado
                               .Where(p => p.pkfk_Colegiado == assign.idReferre)
                               .Where(q => q.pkfk_SubEvento == assign.idSubEvent);


            foreach (ccee_SubEventoColegiado ctnSubCole in ceSubCol)
            {
                ceContSubCol = ctnSubCole;
            }

            if (ceContSubCol != null)
            {
                ceContSubCol.SEC_Estatus = assign.idStatus;

                db.Entry(ceContSubCol).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    return Ok(Constant.status_code_success);
                }
                catch
                {
                    return Ok(Constant.status_code_error);
                }
            }
            return Ok(Constant.status_code_error);
        }

        // DELETE: api/Api_Event/5
        public void Delete(int id)
        {
        }

        private List<EventModelViews> listEvent()
        {
            
            List<EventModelViews> contentEvent =  new List<EventModelViews>();
            DateTime dateT = DateTime.Now;
            var events = db.ccee_Evento
                        .Where(p => p.Eve_Estatus == Constant.state_event_inscription)
                        .Where(o => o.Eve_Inicio <= dateT)
                        .Where(q => q.Eve_Fin >= dateT);

            try
            {
                foreach (ccee_Evento ccEvent in events)
                {
                    contentEvent.Add(new EventModelViews()
                    {
                        idEvent = ccEvent.Eve_NoEvento,
                        name = ccEvent.Eve_NombreEvento
                    });
                }
                return contentEvent;
            }
            catch
            {
                return null;
            }

            
        }


        private List<EventModelViews> subListEvent(int valor)
        {
            List<EventModelViews> contentEvent = new List<EventModelViews>();
            var subEvent = db.ccee_SubEvento
                            .Where(p => p.fk_Evento == valor);
            foreach (ccee_SubEvento ccSubEvent in subEvent)
            {
                contentEvent.Add(new EventModelViews()
                {
                    idEvent = ccSubEvent.SEv_NoSubEvento,
                    name = ccSubEvent.SEv_NombreSubEvento
                });
            }
            return contentEvent;
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}

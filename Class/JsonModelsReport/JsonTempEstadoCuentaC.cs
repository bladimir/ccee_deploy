﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonTempEstadoCuentaC
    {
        public int cargo { get; set; }
        public int? pago { get; set; }
        public int col { get; set; }
        public string descripcion { get; set; }
        public decimal? monto { get; set; }
        public DateTime fechaP { get; set; }
        public DateTime fecha { get; set; }
        public string recibos { get; set; }

        public decimal? montoC { get; set; }
        public decimal? montoT { get; set; }
        public decimal? montoP { get; set; }
        public decimal? montoD { get; set; }
        public int? idRecibo { get; set; }
        public int? doc { get; set; }
        public string nombre { get; set; }

        public DateTime? fechaR { get; set; }
        public int rechazado { get; set; }
        public int? congelado { get; set; }
    }
}
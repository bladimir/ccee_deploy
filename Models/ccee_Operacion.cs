//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace webcee.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ccee_Operacion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ccee_Operacion()
        {
            this.ccee_Perfil_Operacion = new HashSet<ccee_Perfil_Operacion>();
        }
    
        public int Ope_NoOperacion { get; set; }
        public string Ope_Nombre { get; set; }
        public int Ope_Sis_NoSistema { get; set; }
    
        public virtual ccee_Sistema ccee_Sistema { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ccee_Perfil_Operacion> ccee_Perfil_Operacion { get; set; }
    }
}

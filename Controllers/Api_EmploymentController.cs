﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.JsonModels.Employment;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_EmploymentController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Employment
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_Employment/5
        public IEnumerable<ResponseEmploymentJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var listEmployment = from emp in db.ccee_HistoricoLaboral
                                     where emp.fk_Colegiado == id
                                     select new ResponseEmploymentJson()
                                     {
                                         id = emp.HLa_NoHistoricoLaboral,
                                         datestart = emp.HLa_FechaInicio.Value,
                                         datefinish = emp.HLa_FechaFin.Value,
                                         empleador = emp.HLa_Empleador,
                                         address = emp.HLa_Direccion,
                                         phone = emp.HLa_Telefono.Value,
                                         puesto = emp.HLa_Puesto,
                                         actual = emp.HLa_Actual,
                                         salario = emp.HLa_Salario
                                     };
                if(listEmployment == null)
                {
                    return null;
                }

                List<ResponseEmploymentJson> convertListEmployment = listEmployment.ToList();
                for (int i = 0; i < convertListEmployment.Count; i++)
                {
                    convertListEmployment[i].datefinishText = GeneralFunction.DateOfString(convertListEmployment[i].datefinish);
                    convertListEmployment[i].datestartText = GeneralFunction.DateOfString(convertListEmployment[i].datestart);
                }
                Autorization.saveOperation(db, autorization, "Obteniendo historial laboral");
                return convertListEmployment;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_Employment
        public IHttpActionResult Post(RequestEmplymentJson employment)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_HistoricoLaboral tempEmployment = new ccee_HistoricoLaboral()
                {
                    HLa_FechaFin = employment.datefinish,
                    HLa_FechaInicio = employment.datestart,
                    HLa_Direccion = employment.address,
                    HLa_Empleador = employment.empleador,
                    HLa_Puesto = employment.puesto,
                    HLa_Telefono = employment.phone,
                    fk_Colegiado = employment.idReferee,
                    HLa_Actual = Convert.ToInt32(employment.actual),
                    HLa_Salario = employment.salario
                };

                db.ccee_HistoricoLaboral.Add(tempEmployment);
                Autorization.saveOperation(db, autorization, "Agregando historial laboral");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);

            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        // PUT: api/Api_Employment/5
        public IHttpActionResult Put(int id, RequestEmplymentJson employment)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_HistoricoLaboral tempEmployment = db.ccee_HistoricoLaboral.Find(id);
                if (tempEmployment == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempEmployment.HLa_FechaInicio = ((employment.datestart != null)? employment.datestart: tempEmployment.HLa_FechaInicio);
                tempEmployment.HLa_FechaFin = ((employment.datefinish != null) ? employment.datefinish : tempEmployment.HLa_FechaFin);
                tempEmployment.HLa_Direccion = employment.address;
                tempEmployment.HLa_Empleador = employment.empleador;
                tempEmployment.HLa_Puesto = employment.puesto;
                tempEmployment.HLa_Telefono = employment.phone;
                tempEmployment.HLa_Salario = employment.salario;
                tempEmployment.HLa_Actual = Convert.ToInt32(employment.actual);

                db.Entry(tempEmployment).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando historial laboral");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);

            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        // DELETE: api/Api_Employment/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_HistoricoLaboral tempEmploy = db.ccee_HistoricoLaboral.Find(id);
                if(tempEmploy == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_HistoricoLaboral.Remove(tempEmploy);
                Autorization.saveOperation(db, autorization, "Eliminando historial laboral");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

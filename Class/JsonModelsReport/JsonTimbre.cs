﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonTimbre
    {
        public int idDenominacion { get; set; }
        public decimal denominacion { get; set; }
        public int cantidad { get; set; }
        public decimal total { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.LanguageReferee
{
    public class ResponseLanguageRefereeJson
    {
        public int idReferee { get; set; }
        public int idLanguage { get; set; }
        public string nameLanguage { get; set; }
    }
}
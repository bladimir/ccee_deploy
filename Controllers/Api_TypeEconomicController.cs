﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.TypeEconomic;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_TypeEconomicController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_TypeEconomic
        public IEnumerable<ResponseTypeEconomicJson> Get()
        {
            string autorization = Request.Headers.Authorization.Parameter;
            if (!Autorization.enableAutorization(db, autorization)) return null;
            var tempEcono = from teco in db.ccee_TipoCargoEconomico
                            join par in db.ccee_PartidaContable on teco.ccee_PartidaContable_PCo_NoPartidaContable equals par.PCo_NoPartidaContable
                            
                            select new ResponseTypeEconomicJson()
                            {
                                id = teco.TCE_NoTipoCargoEconomico,
                                valor = teco.TCE_Valor,
                                description = teco.TCE_Descripcion,
                                percent = teco.TCE_Porcentaje,
                                debit = teco.TCE_DebitoCredito,
                                calculationCondition = teco.TCE_CondicionCalculo,
                                periodicity = teco.TCE_Periodicidad,
                                finalyDate = teco.TCE_FechaFinalizacion,
                                initDate = teco.TCE_FechaInicio,
                                idaccounting = teco.ccee_PartidaContable_PCo_NoPartidaContable,
                                status = teco.TCE_Estatus,
                                nameAccouting = par.PCo_Descripcion
                            };
            Autorization.saveOperation(db, autorization, "Obteniendo tipo cargo economico");
            return tempEcono;
        }
        
        // GET: api/Api_TypeEconomic/5
        public IEnumerable<ResponseTypeEconomicJson> Get(int id)
        {
            string autorization = Request.Headers.Authorization.Parameter;
            if (!Autorization.enableAutorization(db, autorization)) return null;
            if (id == Constant.type_economic_offtime)
            {
                var tempEcono = from teco in db.ccee_TipoCargoEconomico
                                where teco.TCE_Periodicidad == 0 && teco.TCE_Estatus == 1
                                orderby teco.TCE_Descripcion ascending
                                select new ResponseTypeEconomicJson()
                                {
                                    id = teco.TCE_NoTipoCargoEconomico,
                                    valor = teco.TCE_Valor.Value,
                                    percent = teco.TCE_Porcentaje.Value,
                                    description = teco.TCE_Descripcion
                                };
                Autorization.saveOperation(db, autorization, "Obteniendo tipo cargo economico fuera de periodo");
                return tempEcono;
            }else if(id == Constant.type_economic_ontime)
            {
                var tempEcono = from teco in db.ccee_TipoCargoEconomico
                                where teco.TCE_Periodicidad != 0
                                orderby teco.TCE_Descripcion ascending
                                select new ResponseTypeEconomicJson()
                                {
                                    id = teco.TCE_NoTipoCargoEconomico,
                                    valor = teco.TCE_Valor.Value,
                                    percent = teco.TCE_Porcentaje.Value,
                                    description = teco.TCE_Descripcion
                                };
                Autorization.saveOperation(db, autorization, "Obteniendo tipo cargo economico en periodo");
                return tempEcono;
            }
            return null;
        }

        // POST: api/Api_TypeEconomic
        public IHttpActionResult Post(RequestTypeEconomicJson typeEconomic)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_TipoCargoEconomico tempTypeEco = new ccee_TipoCargoEconomico()
                {
                    TCE_Descripcion = typeEconomic.description,
                    TCE_DebitoCredito = typeEconomic.debit,
                    TCE_Porcentaje = typeEconomic.percent,
                    TCE_Valor = typeEconomic.valor,
                    TCE_Periodicidad = typeEconomic.periodicity,
                    TCE_CondicionCalculo = typeEconomic.calculationCondition,
                    TCE_FechaInicio = typeEconomic.initDate,
                    TCE_FechaFinalizacion = typeEconomic.finalyDate,
                    TCE_Estatus = typeEconomic.status,
                    ccee_PartidaContable_PCo_NoPartidaContable = typeEconomic.idaccounting
                };

                db.ccee_TipoCargoEconomico.Add(tempTypeEco);
                Autorization.saveOperation(db, autorization, "Agregando tipo cargo economico");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);   
            }
        }

        // PUT: api/Api_TypeEconomic/5
        public IHttpActionResult Put(int id, RequestTypeEconomicJson typeEconomic)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_TipoCargoEconomico tempTypeEconomic = db.ccee_TipoCargoEconomico.Find(id);

                if (tempTypeEconomic == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempTypeEconomic.TCE_Descripcion = typeEconomic.description;
                tempTypeEconomic.TCE_DebitoCredito = typeEconomic.debit;
                tempTypeEconomic.TCE_Porcentaje = typeEconomic.percent;
                tempTypeEconomic.TCE_Valor = typeEconomic.valor;
                tempTypeEconomic.TCE_Periodicidad = typeEconomic.periodicity;
                tempTypeEconomic.TCE_CondicionCalculo = typeEconomic.calculationCondition;
                tempTypeEconomic.TCE_FechaInicio = typeEconomic.initDate;
                tempTypeEconomic.TCE_FechaFinalizacion = typeEconomic.finalyDate;
                tempTypeEconomic.TCE_Estatus = typeEconomic.status;
                tempTypeEconomic.ccee_PartidaContable_PCo_NoPartidaContable = typeEconomic.idaccounting;
                
                db.Entry(tempTypeEconomic).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando tipo cargo economico");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_TypeEconomic/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_TipoCargoEconomico tempTypeEconomic = db.ccee_TipoCargoEconomico.Find(id);
                if (tempTypeEconomic == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_TipoCargoEconomico.Remove(tempTypeEconomic);
                Autorization.saveOperation(db, autorization, "Eliminando tipo cargo economico");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

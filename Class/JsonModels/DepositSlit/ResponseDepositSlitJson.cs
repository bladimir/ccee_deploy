﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.DepositSlit
{
    public class ResponseDepositSlitJson
    {
        public int idCahier { get; set; }
        public int idBank { get; set; }
        public DateTime date { get; set; }
        public string boleta { get; set; }
    }
}
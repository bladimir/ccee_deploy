﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonFichaColegiadoTitu
    {
        public string Expr1 { get; set; }
        public string TES_GradoAcademico { get; set; }
        public string Uni_NombreUniversidad { get; set; }
    }
}
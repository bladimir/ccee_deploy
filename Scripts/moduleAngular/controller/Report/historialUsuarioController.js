﻿(function () {
    angular.module('reports')
        .controller('historialUsuario', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'reportFactory', function ($scope, $http, CONSTANT, $anchorScroll, reportFactory) {
            $scope.direction;
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquaters;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.identical = function (actual, expected) {
                return actual === parseInt(expected);
            }

            $scope.init = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        reportFactory.setAuthorization(response.data);
                        $scope.initF();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.initF = function () {
                $scope.getCashier();
            }


            $scope.getCashier = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_USER)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listCashier = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }


            $scope.getDataSelect = function (list) {
                if (list == null) return "";
                var len = list.length;

                var listTotal = "";
                for (var i = 0; i < len; i++) {
                    listTotal += list[i].id + ((len > 0 && i < len - 1) ? "," : "");
                }
                return listTotal;
            }

            $scope.getDataSelectNames = function (list) {
                if (list == null) return "";
                var len = list.length;

                var listTotal = "";
                for (var i = 0; i < len; i++) {
                    listTotal += list[i].name + ((len > 0 && i < len - 1) ? "," : "");
                }
                return listTotal;
            }



            $scope.generateList = function () {
                var textGetReport = "";
                textGetReport = "&dateI=" + ($scope.inDateI.getFullYear() + "-" + ($scope.inDateI.getMonth() + 1) + "-" + $scope.inDateI.getDate()) +
                                "&dateF=" + ($scope.inDateF.getFullYear() + "-" + ($scope.inDateF.getMonth() + 1) + "-" + $scope.inDateF.getDate()) +
                                "&cashi=" + $scope.getDataSelect($scope.inCashi) +

                                "&cashiN=" + $scope.getDataSelectNames($scope.inCashi);
                console.log(textGetReport);
                $scope.direction = "../../Reports/aspxReports/HistorialUsuarioAspx.aspx?" + textGetReport;
            }

        }]);

})()
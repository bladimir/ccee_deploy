﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonCalidadEstatusSaldo
    {
        public int Col_NoColegiado { get; set; }
        public String Col_PrimerApellido { get; set; }
        public String Col_SegundoApellido { get; set; }
        public String Col_CasdaApellido { get; set; }
        public String Col_PrimerNombre { get; set; }
        public String Col_SegundoNombre { get; set; }
        public String Col_TercerNombre { get; set; }
        public String Uni_NombreUniversidad { get; set; }
        public String Sed_Direccion { get; set; }
        public String activo { get; set; }
        private decimal tempSaldoC { get; set; }
        public decimal saldoC {
            get
            {
                return Math.Round(tempSaldoC, 2);
            }
            set { tempSaldoC = value; }
        }
        private decimal tempSaldoT { get; set; }
        public decimal saldoT {
            get
            {
                return Math.Round(tempSaldoT, 2);
            }
            set { tempSaldoT = value; }
        }
        private decimal tempSaldoS { get; set; }
        public decimal saldoS
        {
            get
            {
                return Math.Round(tempSaldoS, 2);
            }
            set { tempSaldoS = value; }
        }
        public DateTime? fechaTempC { get; set; }
        public DateTime? fechaTempT { get; set; }
        public DateTime? fechaTempS { get; set; }
        public string titulo { get; set; }
        public string fechaC {
            get {
                return (fechaTempC == null) ? "" : fechaTempC.Value.ToString("dd/MM/yyyy"); 
            }
        }
        public string fechaT
        {
            get
            {
                return (fechaTempT == null) ? "" : fechaTempT.Value.ToString("dd/MM/yyyy");
            }
        }
        public string fechaS
        {
            get
            {
                return (fechaTempS == null) ? "" : fechaTempS.Value.ToString("dd/MM/yyyy");
            }
        }
        public string email { get; set; }
        public string telefono { get; set; }

    }
}
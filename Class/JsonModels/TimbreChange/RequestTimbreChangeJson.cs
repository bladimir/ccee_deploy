﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.TimbreChange
{
    public class RequestTimbreChangeJson
    {
        public int id { get; set; }
        public int? numberPage { get; set; }
        public int? numberTimbre { get; set; }
        public decimal? valor { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.JsonModels.UserWS;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_UserWSController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_UserWS
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_UserWS/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_UserWS
        public ResponseUserWSJson Post(RequestUserWSJson user)
        {
            ResponseUserWSJson tempUser = new ResponseUserWSJson();

            try
            {
                ccee_Usuario tUs = db.ccee_Usuario
                                    .Where(p => p.Usu_Correo == user.email)
                                    .FirstOrDefault();
                if(tUs != null)
                {

                    if (tUs.Usu_Clave.Equals(GeneralFunction.MD5Encrypt(user.pass)))
                    {

                        Autorization autorization = new Autorization(db);
                        string key = autorization.createToken(tUs.Usu_NoUsuario, tUs.Usu_Nombre);
                        tempUser.authorization = key;
                        tempUser.status = 200; // OK
                        return tempUser;

                    }
                    else
                    {
                        tempUser.authorization = "";
                        tempUser.status = 409; // Conflict
                        return tempUser;
                    }

                }else
                {
                    tempUser.authorization = "";
                    tempUser.status = 404; // NotFound
                    return tempUser;
                }
                
            }
            catch (Exception)
            {
                tempUser.authorization = "";
                tempUser.status = 400;
                return tempUser;
            }
        }

        // PUT: api/Api_UserWS/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_UserWS/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

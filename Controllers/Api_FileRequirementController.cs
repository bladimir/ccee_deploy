﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.FileRequirement;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_FileRequirementController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_FileRequirement
        public IEnumerable<ResponseFileRequirementJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                var tempReq = from req in db.ccee_ExpedienteRequisito
                              select new ResponseFileRequirementJson()
                              {
                                  id = req.ERe_NoExpedienteRequisito,
                                  name = req.ERe_NombreRequisito,
                                  required = req.ERe_Obligatorio.Value,
                                  initFile = req.ERe_Inicio
                              };
                Autorization.saveOperation(db, autorization, "Obteniendo requisito expedinte");

                return tempReq;

            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_FileRequirement/5
        public IEnumerable<ResponseFileRequirementJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                var tempReq = from req in db.ccee_ExpedienteRequisito
                              where req.fk_Expediente == id
                              select new ResponseFileRequirementJson()
                              {
                                  id = req.ERe_NoExpedienteRequisito,
                                  name = req.ERe_NombreRequisito,
                                  required = req.ERe_Obligatorio.Value,
                                  initFile = req.ERe_Inicio
                              };
                Autorization.saveOperation(db, autorization, "Obteniendo requisito expedinte");

                return tempReq;

            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_FileRequirement
        public IHttpActionResult Post(RequestFileRequirementJson fileRequirement)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_ExpedienteRequisito tempFileRequirement = new ccee_ExpedienteRequisito()
                {
                    ERe_NombreRequisito = fileRequirement.name,
                    ERe_Obligatorio = fileRequirement.required,
                    ERe_Inicio = fileRequirement.initFile,
                    fk_Expediente = fileRequirement.idFile
                };

                db.ccee_ExpedienteRequisito.Add(tempFileRequirement);
                Autorization.saveOperation(db, autorization, "Agregando requisito expedinte");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_FileRequirement/5
        public IHttpActionResult Put(int id, RequestFileRequirementJson fileRequirement)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_ExpedienteRequisito tempFileRequirement = db.ccee_ExpedienteRequisito.Find(id);

                if (tempFileRequirement == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempFileRequirement.ERe_NombreRequisito = fileRequirement.name;
                tempFileRequirement.ERe_Obligatorio = (fileRequirement.required == null) ? tempFileRequirement.ERe_Obligatorio : fileRequirement.required;
                tempFileRequirement.ERe_Inicio = fileRequirement.initFile;

                db.Entry(tempFileRequirement).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando requisito expedinte");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_FileRequirement/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_ExpedienteRequisito tempFileRequirement = db.ccee_ExpedienteRequisito.Find(id);
                if (tempFileRequirement == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_ExpedienteRequisito.Remove(tempFileRequirement);
                Autorization.saveOperation(db, autorization, "Eliminando requisito expedinte");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Family
{
    public class RequestFamilyJson
    {
        public string firstName { get; set; }
        public string secoundName { get; set; }
        public string thirdName { get; set; }
        public string firstLastName { get; set; }
        public string secoundLastName { get; set; }
        public string marriedName { get; set; }
        public int typeBeneficiary { get; set; }
        public int idReferee { get; set; }
        public int? percentage { get; set; }
        public string remark { get; set; }
        public string dpi { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.ValueTimbre
{
    public class RequestValueTimbreJson
    {
        public string name { get; set; }
        public decimal? denomination { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonCorteCajaGeneral
    {
        public string nombre { get; set; }
        public DateTime fecha { get; set; }
        private decimal tempTotal { get; set; }
        public decimal total {
            get
            {
                return Math.Round(tempTotal, 2);
            }
            set { tempTotal = value; }
        }
        public string cierreApertura { get; set; }
    }
}
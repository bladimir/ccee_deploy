﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.GlobalVars
{
    public class RequestGlobalVarsJson
    {
        public string key { get; set; }
        public string value { get; set; }
    }
}
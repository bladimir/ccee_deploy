﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.JsonModels.Graphic;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_GraphicsController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Graphics
        public IEnumerable<ResponseGraphicJson> Get()
        {
            try
            {

                var tempGraphic = from gra in db.ccee_Grafica
                                  select new ResponseGraphicJson()
                                  {
                                      id = gra.GRA_NoGrafica,
                                      description = gra.GRA_Descripcion,
                                      reportId = gra.GRA_ReportId,
                                      workspaceCollection = gra.GRA_WorkspaceCollection,
                                      workspaceId = gra.GRA_WorkspaceId
                                  };
                return tempGraphic;

            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_Graphics/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_Graphics
        public IHttpActionResult Post(RequestGraphicJson graphic)
        {

            try
            {
                ccee_Grafica tempGraphic = new ccee_Grafica()
                {
                    GRA_Descripcion = graphic.description,
                    GRA_ReportId = graphic.reportId,
                    GRA_WorkspaceId = graphic.workspaceId,
                    GRA_WorkspaceCollection = graphic.workspaceCollection
                };

                db.ccee_Grafica.Add(tempGraphic);
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        // PUT: api/Api_Graphics/5
        public IHttpActionResult Put(int id, RequestGraphicJson graphic)
        {
            try
            {
                ccee_Grafica tempGraphic = db.ccee_Grafica.Find(id);

                if (tempGraphic == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempGraphic.GRA_Descripcion = graphic.description;
                tempGraphic.GRA_ReportId = graphic.reportId;
                tempGraphic.GRA_WorkspaceId = graphic.workspaceId;
                tempGraphic.GRA_WorkspaceCollection = graphic.workspaceCollection;

                db.Entry(tempGraphic).State = EntityState.Modified;
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_Graphics/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                ccee_Grafica tempGraphic = db.ccee_Grafica.Find(id);
                if (tempGraphic == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_Grafica.Remove(tempGraphic);
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

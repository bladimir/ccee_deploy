﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.Batch;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_BatchController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Batch
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_Batch/5
        public List<ResponseBatchJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempBatch = (from bat in db.ccee_Batch
                                join caj in db.ccee_Cajero on bat.fk_Cajero equals caj.Cjo_NoCajero
                                join rec in db.ccee_DocContable on bat.fk_DocContable equals rec.DCo_NoDocContable
                                where bat.fk_Cajero == id
                                select new ResponseBatchJson()
                                {
                                    id = bat.Bat_NoBathc,
                                    name = caj.Cjo_Name,
                                    noReceipt = rec.DCo_NoRecibo,
                                    date = rec.DCo_FechaHora,
                                    delete = rec.DCo_Eliminado
                                }).ToList();
                Autorization.saveOperation(db, autorization, "Obteniendo listado batch");
                return tempBatch;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_Batch
        public ResponseBatchJson Post(RequestBatchJson value)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ResponseBatchJson resBatch = new ResponseBatchJson();

                ccee_Batch tempBatch = new ccee_Batch()
                {
                    fk_Cajero = value.idCashier
                };

                db.ccee_Batch.Add(tempBatch);
                db.SaveChanges();

                string typeDocAccouting = db.ccee_Variables.Where(o => o.Var_Clave.Equals("VarReciboCol")).Select(p => p.Var_Valor).FirstOrDefault();
                int typeDocAccoutingInt = Convert.ToInt32(typeDocAccouting);
                ccee_TipoDocContable tempDocCon =
                    db.ccee_TipoDocContable.Find(typeDocAccoutingInt);
                ccee_DocContable tempDoc = new ccee_DocContable()
                {
                    DCo_ANombre = "Temp",
                    DCo_NoRecibo = "TBat@" + tempBatch.Bat_NoBathc,
                    fk_TipoDocContable = tempDocCon.TDC_NoTipoDocContable
                };

                db.ccee_DocContable.Add(tempDoc);
                db.SaveChanges();

                Autorization.saveOperation(db, autorization, "Agregando Batch");
                

                resBatch.id = tempBatch.Bat_NoBathc;
                resBatch.idDocAccount = tempDoc.DCo_NoDocContable;

                return resBatch;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // PUT: api/Api_Batch/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_Batch/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

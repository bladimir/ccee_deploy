﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.Rol;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_RolController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_Rol
        public IEnumerable<ResponseRolJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                var tempRol = from rol in db.ccee_Perfil
                              select new ResponseRolJson()
                              {
                                  id = rol.Per_NoPerfil,
                                  name = rol.Per_Nombre
                              };
                Autorization.saveOperation(db, autorization, "Obteniendo rol");
                return tempRol;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_Rol/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_Rol
        public IHttpActionResult Post(RequestRolJson rol)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_Perfil tempRol = new ccee_Perfil()
                {
                    Per_Nombre = rol.name,
                    Per_Sis_NoSistema = Constant.id_System_Web
                };

                db.ccee_Perfil.Add(tempRol);
                Autorization.saveOperation(db, autorization, "Agregando rol");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_Rol/5
        public IHttpActionResult Put(int id, RequestRolJson rol)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_Perfil tempRol = db.ccee_Perfil.Find(id);

                if (tempRol == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempRol.Per_Nombre = rol.name;

                db.Entry(tempRol).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando rol");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        // DELETE: api/Api_Rol/5
        public IHttpActionResult Delete(int id)
        {

            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;
                ccee_Perfil tempRol = db.ccee_Perfil.Find(id);
                if (tempRol == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_Perfil.Remove(tempRol);
                Autorization.saveOperation(db, autorization, "Eliminando rol");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.RejectedCheck
{
    public class RequestRejectedCheckJson
    {
        public int type { get; set; }
        public ResponseRejectedCheckJson objectCheck { get; set; }
    }
}
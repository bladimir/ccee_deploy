﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace webcee.Class.Class
{
    public class HttpRequestRest
    {

        public static RestRequest getRequestHttpPost(RestClient client, string url, object json)
        {
            RestRequest request = new RestRequest(url, Method.POST);
            string jsonToSend = new JavaScriptSerializer().Serialize(json);
            request.AddHeader("Content-Type", "application/json; charset=utf-8");
            request.AddJsonBody(json);
            request.RequestFormat = DataFormat.Json;
            return request;
        }

    }
}
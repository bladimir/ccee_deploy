﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.RolOperations
{
    public class ResponseRolOperJson
    {
        public int idOper { get; set; }
        public string nameOper { get; set; }
        public int idRol { get; set; }
        public string nameRol { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.NoReferee
{
    public class ResponseNoRefereeJson
    {
        public int id { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string totalName { get {
                return name + " " + lastname;
            }
        }
        public string nit { get; set; }
        public int? movil { get; set; }
        public string address { get; set; }
        public string remark { get; set; }
        public int? isAfiliado { get; set; }
        public string isAfiliadoText { get {

                return ((isAfiliado == 1) ? "Afiliado" : "");

            }
        }
    }
}
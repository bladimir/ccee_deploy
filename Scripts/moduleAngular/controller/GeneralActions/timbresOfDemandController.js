﻿(function () {
    angular.module('generals')
        .controller('timbres', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listCellar;
            $scope.listCashier;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.idUpdate = 0;
            $scope.regexNumber = CONSTANT.REGEX_DIGIT;
            $scope.listCash;
            $scope.numberPageText;
            $scope.listTimbre = [];
            $scope.enableTimbre = false;
            $scope.totalPaymentTimbre = 0;
            $scope.amountSelectTimbre = 0;
            $scope.showPayTimbre = false;
            $scope.statePay = 0;
            $scope.TypeEconomicTimbresDemand = 131;
            $scope.listEconomic = [];
            $scope.aumontoPaymentMethod;
            $scope.listPaymentMethodPay = [];
            $scope.listPaymentMethod = [];
            $scope.listDocumentPrint;

            $scope.butonDocs = true;
            $scope.Nodocument = 0;
            $scope.itemsTimbre = [];
            $scope.visiblePay = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.defineDocument = function (doc) {
                $scope.Nodocument = doc;
                console.log($scope.Nodocument);
            }

            $scope.setAuth = function (idCashier) {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.initFunction(idCashier);
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.addEconomic = function () {
                $scope.showLoadingFunction();
                var information = {
                    idTypeEconomic: $scope.TypeEconomicTimbresDemand,
                    idReferee: $scope.referee,
                    amount: $scope.amountSelectTimbre,
                    isDemand: 1
                }
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_ECONOMIC, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data != null) {
                                $scope.statePay = 2;
                                response.data.enable = 1;
                                $scope.getIdPayMethodCash();
                                $scope.listEconomic.push(response.data);
                                $scope.viewPay();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.getIssuingBank = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_ISSUINGBANK)
                .then(function (response) {
                    $scope.listIssuingBank = response.data;
                    $scope.hideLoadingFunction();
                }, function (error) {
                    console.log(error);
                    $scope.hideLoadingFunction();
                });
            }

            $scope.getPaymentMethod = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_PAYMENTMETHOD, 40)
                .then(function (response) {
                    $scope.listPaymentMethod = response.data;
                    $scope.hideLoadingFunction();
                }, function (error) {
                    console.log(error);
                    $scope.hideLoadingFunction();
                });
            }

            $scope.getPay = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_ECONOMIC, $scope.referee)
                .then(function (response) {
                    $scope.listEconomic = response.data;
                    $scope.hideLoadingFunction();
                }, function (error) {
                    console.log(error);
                    $scope.hideLoadingFunction();
                });
            }


            $scope.viewPay = function () {
                $scope.aumontoPaymentMethod = $scope.amountSelectTimbre;
                $scope.getCash();
                $scope.getIssuingBank();
                $scope.getIdPayMethodCash();
                $scope.getPaymentMethod();
                console.log($scope.listEconomic);
            }


            $scope.addListMethodCash = function () {
                if ($scope.amountpayMethod <= $scope.aumontoPaymentMethod && $scope.aumontoPaymentMethod > 0) {
                    var paymentMethod = {
                        amount: parseFloat($scope.amountpayMethod),
                        noTransaction: 0,
                        noDocument: 0,
                        reservation: 0,
                        rejection: 0,
                        remarke: "",
                        idBank: 40,
                        idPaymentMethod: $scope.idPayMethodCash
                    };
                    $scope.listPaymentMethodPay.push(paymentMethod);
                    $scope.aumontoPaymentMethod = parseFloat($scope.aumontoPaymentMethod) - parseFloat($scope.amountpayMethod);
                    $scope.amountpayMethod = "";
                    $scope.numberpayTrans = "";
                    $scope.numberpayDoc = "";
                    $scope.alertamountPayment = "";
                } else {
                    $scope.alertamountPayment = ": El monto no es permitido";
                }
            }

            $scope.addListMethod = function () {
                var idMethodPay = 0;
                var len = $scope.listPaymentMethod.length;
                for (var i = 0; i < len; i++) {
                    var type = $scope.listPaymentMethod[i].typeMethod;
                    if (type == $scope.radioPaymentMethod) {
                        idMethodPay = $scope.listPaymentMethod[i].id;
                        break;
                    }
                }

                console.log(idMethodPay);

                if ($scope.amountpayMethod <= $scope.aumontoPaymentMethod && $scope.aumontoPaymentMethod > 0) {
                    var paymentMethod = {
                        amount: parseFloat($scope.amountpayMethod),
                        noTransaction: $scope.numberpayTrans,
                        noDocument: $scope.numberpayDoc,
                        reservation: 1,
                        rejection: 0,
                        remarke: $scope.remakepayMothod,
                        idBank: $scope.selectIssuingBank.id,
                        idPaymentMethod: idMethodPay
                    };
                    $scope.listPaymentMethodPay.push(paymentMethod);
                    $scope.aumontoPaymentMethod = parseFloat($scope.aumontoPaymentMethod) - parseFloat($scope.amountpayMethod);
                    $scope.amountpayMethod = "";
                    $scope.numberpayTrans = "";
                    $scope.numberpayDoc = "";
                    $scope.alertamountPayment = "";
                    $scope.viewBank = false;
                } else {
                    $scope.alertamountPayment = ": El monto no es permitido";
                }
            }

            $scope.deleteListMethod = function (data) {
                var pos = $scope.listPaymentMethodPay.indexOf(data);
                console.log(data);
                $scope.aumontoPaymentMethod = parseFloat($scope.aumontoPaymentMethod) + data.amount;
                $scope.listPaymentMethodPay.splice(pos, 1);
            }

            

            $scope.addPay = function (idCashier, idCash) {
                var len = $scope.listEconomic;
                var pay;

                $scope.showLoadingFunction();
                var information = {
                    amount: $scope.amountSelectTimbre,
                    remake: $scope.remakepay,
                    idCashier: idCashier,
                    idCash: idCash,
                    id: 1,
                    idReferee: $scope.referee,
                    jsonContent: $scope.listEconomic,
                    jsonContentMethod: $scope.listPaymentMethodPay
                };

                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_PAY, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data > 0) {
                                console.log(response.data);
                                $scope.idPayment = response.data;
                                $scope.statePay = 3;
                            }

                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.addTimbres = function () {
                $scope.showLoadingFunction();
                var information = {
                    name: "Pagos Timbre",
                    idPay: $scope.idPayment,
                    timbres: $scope.listTimbre,
                    referee: $scope.referee
                }
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_TIMBRE, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.correlative();
                                $scope.visiblePay = 3;
                            }

                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.getCash = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_CASH)
                .then(function (response) {
                    $scope.listCash = response.data;
                }, function (error) {
                    console.log(error);
                });
            }

            $scope.initFunction = function (idCashier) {

                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_VALUETIMBRE, idCashier)
                .then(function (response) {
                    console.log(response);
                    $scope.itemsTimbre = response.data;
                }, function (error) {
                    console.log(error);
                });

            }


            $scope.getTimbres = function (idCahier, countContent, idValue, vIndex) {
                    if (countContent == null || countContent == "") {
                        $scope.itemsTimbre[vIndex].textDemand = "";
                        $scope.itemsTimbre[vIndex].isDisable = 0;
                        $scope.updateListTimbre($scope.itemsTimbre[vIndex].denomination, null);
                        return null
                    };

                    $scope.itemsTimbre[vIndex].textDemand = "Cargando..";
                    console.log(countContent);

                generalactionsFactory.actionGetGetGetIntFunction(CONSTANT.URL_API_TIMBRE, idCahier, countContent, idValue)
                    .then(function (response) {
                        if (response.status == 200) {
                            $scope.itemsTimbre[vIndex].textDemand = $scope.showTextTimbres(response.data);
                            $scope.itemsTimbre[vIndex].isDisable = ((response.data == null) ? 0 : 1);
                            $scope.updateListTimbre($scope.itemsTimbre[vIndex].denomination, response.data);
                        }
                        console.log(response);
                    }, function (error) {
                        console.log(error);
                    });
                
            }


            $scope.updateListTimbre = function (value, timbre) {
                var len = $scope.listTimbre.length;
                var exist = false;
                for (var i = 0; i < len; i++) {
                    if ($scope.listTimbre[i].value == value) {
                        $scope.listTimbre[i].content = timbre;
                        exist = true;
                        break;
                    }
                }

                if (!exist) {
                    $scope.listTimbre.push({ value: value, content: timbre });
                }
                $scope.amountSelect();
            }

            $scope.amountSelect = function () {
                var len = $scope.listTimbre.length;
                var sum = 0;
                for (var i = 0; i < len; i++) {
                    var value = $scope.listTimbre[i].value;
                    var lengthContent = 0;
                    if ($scope.listTimbre[i].content != null) lengthContent = $scope.listTimbre[i].content.length;
                    sum += value * lengthContent;
                }
                $scope.amountSelectTimbre = sum;
            }



            $scope.showTextTimbres = function (data) {
                console.log(data);
                var text = "";
                if (data == null) {
                    text = text + "Timbres insuficientes";
                } else {
                    var len = data.length;
                    console.log(len)
                    for (var i = 0; i < len; i++) {
                        text = text + "T)" + data[i].numberPage + "-" + data[i].numberTimbre + " ";
                    }
                }
                console.log(text);
                return text;
            }

            $scope.getIdPayMethodCash = function () {
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_VARS, "CCEEMedioPago")
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    $scope.idPayMethodCash = response.data;
                    console.log("Medio de pago");
                    console.log(response.data);
                }, function (error) {
                    console.log(error);
                    $scope.hideLoadingFunction();
                });
            }



            $scope.getInfoReferee = function () {
                $scope.statusUpdate = "";
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_REFEREE, $scope.referee)
                    .then(function (response) {
                        var data = response.data;
                        if (data != null) {
                            $scope.completeNameReferee = data.PrimerNombre + " " + data.SegundoNombre + " "
                                                         + data.TercerNombre + " " + data.PrimerApellido + " " + data.SegundoApellido + " " + data.CasdaApellido;
                            $scope.statePay = 1;
                            $scope.hideLoadingFunction();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });


                }


            $scope.getBalanceReferee = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_BALANCE, $scope.referee)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        if(response.data != null){
                            var total = response.data.countReferee + response.data.countTimbre;
                            if (total == 0) {
                                $scope.getInfoReferee();
                            } else {
                                $scope.completeNameReferee = "Colegiado no esta solvente";
                            }
                        }
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.addReceipt = function (idType, idCahier) {
                    

                $scope.showLoadingFunction();
                var information = {
                    titleName: "",
                    address: "Guatemala",
                    nit: "",
                    remark: "",
                    idCashier: idCahier,
                    idReferee: $scope.referee,
                    idPay: $scope.idPayment,
                    idTypeDocumentAccounting: 1,
                    NoDocumentCol: $scope.reciColegiado,
                    NoDocumentOther: $scope.reciOther,
                    listEconomic: $scope.listEconomic,
                    isReferee: true
                }

                console.log(information);
                generalactionsFactory.actionPostFunction("/Referre/PrintFileReferee", information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data != null) {
                                console.log(response);
                                $scope.listDocumentPrint = response.data;

                            }

                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.correlative = function () {
                console.log($scope.Nodocument);
                var len = $scope.listEconomic.length;
                var enableReColegiado = true;
                var enableReOther = false;
                //for (var i = 0; i < len ; i++) {
                //    var enable = $scope.listEconomic[i].enable;
                //    var idType = $scope.listEconomic[i].typeEconomic;
                //    if (enable == 1 && (idType == 2 || idType == 1 || idType == 3)) {
                //        enableReColegiado = true;
                //    }

                //    if (enable == 1 && idType == 3) {
                //        enableReOther = true;
                //    }
                //}


                if (enableReColegiado) {
                    $scope.Nodocument += 1;
                    $scope.reciColegiado = $scope.Nodocument;
                } else {
                    $scope.reciColegiado = 0;
                }
            }

            

            $scope.reloadPay = function () {
                window.location.href = '/Referre/Index';
            }

            $scope.changeType = function (state) {
                if (state == 0) {
                    return "Efectivo";
                } else if (state == 1) {
                    return "Cheque";
                } else if (state == 2) {
                    return "Transferencia";
                } else if (state == 3) {
                    return "Tarjeta de credito";
                } else if (state == 4) {
                    return "Tarjeta de debito";
                } else if (state == 5) {
                    return "Lote institucional";
                } else {
                    return "Indefinido";
                }
            }

            $scope.changeReservation = function (state) {
                if (state == 0) {
                    return "Sin reserva";
                } else if (state == 1) {
                    return "En reserva";
                } else if (state == 2) {
                    return "Rechazado";
                } else {
                    return "No definido"
                }
            }

            $scope.changeRejection = function (state) {
                if (state == 0) {
                    return "No";
                } else if (state == 1) {
                    return "Si";
                } else {
                    return "No definido"
                }
            }

        }]);

})()
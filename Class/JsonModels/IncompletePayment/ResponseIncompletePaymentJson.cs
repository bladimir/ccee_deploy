﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.IncompletePayment
{
    public class ResponseIncompletePaymentJson
    {
        public decimal? amount { get; set; }
        public int pay { get; set; }
        public int referee { get; set; }

    }
}
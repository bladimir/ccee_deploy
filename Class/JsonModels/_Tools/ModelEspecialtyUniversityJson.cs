﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webcee.Models;

namespace webcee.Class.JsonModels._Tools
{
    public class ModelEspecialtyUniversityJson
    {
        public ccee_Especialidad especialty { get; set; }
        public ccee_Universidad university { get; set; }
    }
}
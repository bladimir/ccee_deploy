﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels._Tools;
using webcee.Class.JsonModels.AccountingDocument;
using webcee.Class.JsonModels.Economic;
using webcee.Class.JsonModels.FileRequirement;
using webcee.Class.JsonModels.OpenCloseCash;
using webcee.Models;
using webcee.ViewModels;

namespace webcee.Controllers
{
    public class ReferreController : Controller
    {

        private CCEEEntities db = new CCEEEntities();
        private static int idReferee = 0;
        private static List<UploadFileRequirement> listFiles;
        private static int positionFiles = 0;
        private static int idDocumentRequirement;
        private const string regexVerifyStatus = @"^(1|4|7)?$";
        public class refereeTemp
        {
            public int NoReferee { get; set; }
            public int? status { get; set; }
            public decimal? salario { get; set; }
            public int? active { get; set; }
            public DateTime fecha { get; set; }
        }

        // GET: Referre
        public ActionResult Index()
        {
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            if (GeneralFunction.permissions(this, login, Constant.operation_pagina_inicio) == null) return RedirectToAction("Index", "Home");
            int[] cashierOpen = getCash(login.idCashier);
            if(cashierOpen == null)
            {
                login.idCash = 0;
                login.idOpenClose = 0;
            }else
            {
                login.idCash = cashierOpen[0];
                login.idOpenClose = cashierOpen[1];
            }
            Session[Constant.session_user] = login;
            ViewBag.stateCash = getStateCash(login.idCashier);
            int countReferee = db.ccee_Colegiado.Count();
            ViewBag.countReferee = countReferee;
            return View();
        }

        
        public ActionResult NewReferee()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_nuevo_colegiado) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult DeleteReferee()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_eliminar_colegiado) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        public ActionResult NotReferee()
        {
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            if (GeneralFunction.permissions(this, login, Constant.operation_pagina_inicio) == null) return RedirectToAction("Index", "Home");
            int[] cashierOpen = getCash(login.idCashier);
            if (cashierOpen == null)
            {
                login.idCash = 0;
                login.idOpenClose = 0;
            }
            else
            {
                login.idCash = cashierOpen[0];
                login.idOpenClose = cashierOpen[1];
            }
            Session[Constant.session_user] = login;
            ViewBag.stateCash = getStateCash(login.idCashier);
            return View();
        }

        public ActionResult SearchReferee()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_buscar_no_colegiado) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }

        [HttpGet]
        public ActionResult FileReferee(int? id)
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_nuevo_colegiado) == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);

            positionFiles = -1;

            if (id == null) return RedirectToAction("Index", "Referre", null);
            idReferee = id.Value;

            var list = from req in db.ccee_ExpedienteRequisito
                       join fil in db.ccee_Expediente on req.fk_Expediente equals fil.Exp_NoExpediente
                       where req.ERe_Inicio == 1
                        select new UploadFileRequirement()
                        {
                            id = req.ERe_NoExpedienteRequisito,
                            name = req.ERe_NombreRequisito,
                            idFile = req.fk_Expediente,
                            nameFile = fil.Exp_NombreExpediente,
                            required = req.ERe_Obligatorio.Value,
                            
                            
                        };
            listFiles = list.ToList();

            incrementListFile();

            //
            FileModelView fileModel = getFileModelView(positionFiles);
            if(fileModel == null)
            {
                RedirectToAction("Index", "Referre", null);
            }

            return View(fileModel);
        }

        [HttpPost]
        public ActionResult FileReferee(FileModelView file)
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_nuevo_colegiado) == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);

            string typeDocument = "Upload documento";
            
            foreach (string upload in Request.Files)
            {
                try
                {
                    string filenamereferee = "";

                    if (Request.Files[upload].ContentLength != 0) {

                        string extension = Path.GetExtension(Request.Files[upload].FileName);
                        string pathToSave = Server.MapPath(Constant.url_dir_documents);
                        filenamereferee = idReferee + "_" + listFiles[positionFiles].id + Path.GetExtension(Request.Files[upload].FileName);
                        Request.Files[upload].SaveAs(Path.Combine(pathToSave, filenamereferee));

                    }else
                    {
                        if (listFiles[positionFiles].required == 1)
                        {
                            ViewBag.message = "El archivo es obligatorio";
                            return View(getFileModelView(positionFiles));
                        }
                    }
                    
                    ccee_DocExpRequisito tempExp = existFileReferee(listFiles[positionFiles].id, idReferee);
                    if(tempExp == null)
                    {

                        tempExp = new ccee_DocExpRequisito()
                        {
                            DER_Documento = typeDocument,
                            DER_NombreDocuemento = filenamereferee,
                            DER_Entregado = Constant.delivered_doc,
                            fk_Colegiado = idReferee,
                            fk_ExpReq = listFiles[positionFiles].id,
                            DER_Externo = null
                        };

                        db.ccee_DocExpRequisito.Add(tempExp);
                        db.SaveChanges();

                    }else
                    {
                        tempExp.DER_Documento = typeDocument;
                        tempExp.DER_NombreDocuemento = filenamereferee;
                        tempExp.DER_Entregado = Constant.delivered_doc;
                        tempExp.DER_Externo = null;
                        db.Entry(tempExp).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    
                    incrementListFile();
                    if (positionFiles >= listFiles.Count) return RedirectToAction("Index", "Referre", null);
                    return View(getFileModelView(positionFiles));

                }
                catch (Exception)
                {
                    ViewBag.message = "Archivo no cargado, intentar de nuevo";
                    return View(getFileModelView(positionFiles));
                }

            }




            FileModelView tempModel = new FileModelView()
            {
                nameFile = listFiles[positionFiles].nameFile,
                nameFileRequirement = listFiles[positionFiles].name
            };

            incrementListFile();
            return View();
        }


        public ActionResult Close()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_pagina_inicio) == null) return RedirectToAction("Index", "Home");
            Session[Constant.session_user] = null;
            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult CloseColegiado()
        {
            try
            {
                if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_pagina_inicio) == null) return RedirectToAction("TimbreUsuario", "Document");
                Session[Constant.session_user] = null;
            }
            catch (Exception) {
            }
            return RedirectToAction("TimbreUsuario", "Document", null);
        }

        [HttpGet]
        public ActionResult Photo(int? id)
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_pagina_inicio) == null) return RedirectToAction("Index", "Home");
            if (id == null) return View(new PhotoModelView() { idUsuario = idReferee, pathPhoto = Constant.url_dir_images_no_found });
            idReferee = id.Value;
            string contentPath = db.ccee_Colegiado.Find(idReferee).Col_Foto;
            contentPath = (contentPath == null || contentPath.Equals("")) ? Constant.url_dir_images_no_found : contentPath;
            contentPath = Constant.url_dir_images + contentPath;
            ViewBag.status = true;
            return View(new PhotoModelView() { idUsuario = idReferee, pathPhoto = contentPath });
        }

        [HttpPost]
        public ActionResult Photo(PhotoModelView id)
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_pagina_inicio) == null) return RedirectToAction("Index", "Home");
            foreach (string upload in Request.Files)
            {
                try
                {
                    
                    if (Request.Files[upload].ContentLength == 0) continue;
                    
                    string extension = Path.GetExtension(Request.Files[upload].FileName);
                    if (GeneralFunction.isValidExtension(extension))
                    {

                        string pathToSave = Server.MapPath(Constant.url_dir_images);
                        string filenamereferee = idReferee + Path.GetExtension(Request.Files[upload].FileName);
                        Request.Files[upload].SaveAs(Path.Combine(pathToSave, filenamereferee));

                        ccee_Colegiado tempCole = db.ccee_Colegiado.Find(idReferee);
                        tempCole.Col_Foto = filenamereferee;

                        db.Entry(tempCole).State = EntityState.Modified;
                        db.SaveChanges();

                        ViewBag.message = "Imagen Cargada";
                        ViewBag.status = false;
                        return View(new PhotoModelView() { idUsuario = idReferee, pathPhoto = Constant.url_dir_images + filenamereferee });
                    }else
                    {
                        ViewBag.message = "Extension de imagen no valida solo jpg, png o jpeg";
                        ViewBag.status = true;
                        return View(new PhotoModelView() { idUsuario = idReferee, pathPhoto = Constant.url_dir_images + Constant.url_dir_images_no_found });
                    }
                    
                }
                catch (Exception)
                {
                    ViewBag.message = "Imagen no cargada, intentar de nuevo";
                    ViewBag.status = true;
                    return View(new PhotoModelView() { idUsuario = idReferee, pathPhoto = Constant.url_dir_images + Constant.url_dir_images_no_found });
                }
                
            }
            return View();
        }


        [HttpGet]
        public ActionResult Firm(int? id)
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_pagina_inicio) == null) return RedirectToAction("Index", "Home");
            if (id == null) return View(new PhotoModelView() { idUsuario = idReferee, pathPhoto = Constant.url_dir_images_no_found });
            idReferee = id.Value;
            string contentPath = db.ccee_Colegiado.Find(idReferee).Col_FotoFirma;
            contentPath = (contentPath == null || contentPath.Equals("")) ? Constant.url_dir_images_no_found : contentPath;
            contentPath = Constant.url_dir_images + contentPath;
            ViewBag.status = true;
            return View(new PhotoModelView() { idUsuario = idReferee, pathPhoto = contentPath });
        }

        [HttpPost]
        public ActionResult Firm(PhotoModelView id)
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_pagina_inicio) == null) return RedirectToAction("Index", "Home");
            foreach (string upload in Request.Files)
            {
                try
                {

                    if (Request.Files[upload].ContentLength == 0) continue;

                    string extension = Path.GetExtension(Request.Files[upload].FileName);
                    if (GeneralFunction.isValidExtension(extension))
                    {

                        string pathToSave = Server.MapPath(Constant.url_dir_images);
                        string filenamereferee = idReferee + "_firma" + Path.GetExtension(Request.Files[upload].FileName);
                        Request.Files[upload].SaveAs(Path.Combine(pathToSave, filenamereferee));

                        ccee_Colegiado tempCole = db.ccee_Colegiado.Find(idReferee);
                        tempCole.Col_FotoFirma = filenamereferee;

                        db.Entry(tempCole).State = EntityState.Modified;
                        db.SaveChanges();

                        ViewBag.message = "Imagen Cargada";
                        ViewBag.status = false;
                        return View(new PhotoModelView() { idUsuario = idReferee, pathPhoto = Constant.url_dir_images + filenamereferee });
                    }
                    else
                    {
                        ViewBag.message = "Extension de imagen no valida solo jpg, png o jpeg";
                        ViewBag.status = true;
                        return View(new PhotoModelView() { idUsuario = idReferee, pathPhoto = Constant.url_dir_images + Constant.url_dir_images_no_found });
                    }

                }
                catch (Exception)
                {
                    ViewBag.message = "Imagen no cargada, intentar de nuevo";
                    ViewBag.status = true;
                    return View(new PhotoModelView() { idUsuario = idReferee, pathPhoto = Constant.url_dir_images + Constant.url_dir_images_no_found });
                }

            }
            return View();
        }


        public ActionResult Files(int? id)
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_pagina_inicio) == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            //if (id == null) return RedirectToAction("Index", "Referre", null);

            try
            {


                //var tempDocFile = from doc in db.ccee_ExpedienteRequisito
                //                  join exp in db.ccee_Expediente on doc.fk_Expediente equals exp.Exp_NoExpediente
                //                  select new FileDocumentModelView()
                //                  {
                //                      id = doc.ERe_NoExpedienteRequisito,
                //                      idReferee = id.Value,
                //                      nameFile = exp.Exp_NombreExpediente,
                //                      nameFileRequirement = doc.ERe_NombreRequisito,
                //                      stateFileRequirement = doc.ERe_Obligatorio.Value
                //                  };

                //List<FileDocumentModelView> listFile = tempDocFile.ToList();

                //for (int i = 0; i < listFile.Count; i++)
                //{
                //    listFile[i].stateFileRequirementText = convertStatusFile(listFile[i].stateFileRequirement);
                //    int fkExp = listFile[i].id;
                //    var listDocEx = from exp in db.ccee_DocExpRequisito
                //                    where exp.fk_ExpReq == fkExp && exp.fk_Colegiado == id
                //                    select exp;

                //    if (listDocEx != null)
                //    {

                //        foreach (ccee_DocExpRequisito temp in listDocEx)
                //        {
                //            listFile[i].document = (temp.DER_NombreDocuemento == null || temp.DER_NombreDocuemento == "") ? "" : Constant.url_dir_documents + temp.DER_NombreDocuemento;
                //            listFile[i].stateDocument = temp.DER_Documento;
                //        }
                //    }

                //}

                //return View(listFile);
                return View();
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Referre", null);
            }
        }

        public ActionResult EditFiles(int? id, int? ids)
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_pagina_inicio) == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            if (id == null || ids == null) return RedirectToAction("Index", "Referre", null);

            idReferee = id.Value;
            idDocumentRequirement = ids.Value;
            ViewBag.showData = false;
            return View();
        }

        [HttpPost]
        public ActionResult EditFiles()
        {

            string typeDocument = "Upload documento";
            

            foreach (string upload in Request.Files)
            {
                try
                {
                    ccee_DocExpRequisito tempExp = existFileReferee(idDocumentRequirement, idReferee);

                    
                    if(tempExp != null)
                    {
                        if (Request.Files[upload].ContentLength == 0)
                        {
                            if (!typeDocument.Equals(Constant.close_type_document))
                            {
                                tempExp.DER_Documento = typeDocument;
                                tempExp.DER_Externo = null;
                                db.Entry(tempExp).State = EntityState.Modified;
                                db.SaveChanges();
                                ViewBag.message = "El archivo se ha cargado correctamente";
                                ViewBag.showData = true;
                                return View();
                            }
                            else
                            {
                                ViewBag.showData = false;
                                ViewBag.message = "No se han ingresados datos";
                                return View();
                            }
                        }
                    }

                    string filenamereferee = "";
                    string extension = Path.GetExtension(Request.Files[upload].FileName);
                    string pathToSave = Server.MapPath(Constant.url_dir_documents);
                    filenamereferee = idReferee + "_" + idDocumentRequirement + Path.GetExtension(Request.Files[upload].FileName);
                    Request.Files[upload].SaveAs(Path.Combine(pathToSave, filenamereferee));

                    
                    if (tempExp == null)
                    {
                        try
                        {
                            tempExp = new ccee_DocExpRequisito()
                            {
                                DER_Documento = typeDocument,
                                DER_NombreDocuemento = filenamereferee,
                                DER_Entregado = Constant.delivered_doc,
                                fk_Colegiado = idReferee,
                                fk_ExpReq = idDocumentRequirement
                            };

                            db.ccee_DocExpRequisito.Add(tempExp);
                            db.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            ViewBag.showData = false;
                            ViewBag.message = "Problemas en el sistema vuelva a cargar los datos" + e;
                            return View();
                        }

                    }
                    else
                    {
                        try
                        {
                            tempExp.DER_Documento = (typeDocument.Equals(Constant.close_type_document))? tempExp.DER_Documento : typeDocument;
                            tempExp.DER_NombreDocuemento = filenamereferee;
                            tempExp.DER_Entregado = Constant.delivered_doc;
                            tempExp.DER_Externo = null;
                            db.Entry(tempExp).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            ViewBag.showData = false;
                            ViewBag.message = "Problemas en el sistema vuelva a cargar los datos "+ e;
                            return View();
                        }
                    }


                    ViewBag.message = "El archivo se ha cargado correctamente";
                    ViewBag.showData = true;
                    return View();

                }
                catch (Exception e)
                {
                    ViewBag.showData = false;
                    ViewBag.message = "Problemas en el sistema vuelva a cargar los datos " +e;
                    return View();
                }

            }

            ViewBag.message = "El archivo se ha cargado correctamente";
            ViewBag.showData = true;
            return View();
        }
        public ActionResult BackEditFiles()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_pagina_inicio) == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            return RedirectToAction("Files", "Referre", new { id = idReferee });
        }

        public ActionResult PrintFileReferee(int? id, int? ids)
        {
            //if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews) == null) return RedirectToAction("Index", "Home");
            if(id == null || ids == null)
            {
                return View();
            }
            
            return null;

            
        }

        [HttpGet]
        public string CreateDiploma(int id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {

                try
                {
                    string diplomaReferee = GeneralFunction.getKeyTableVariable(db, Constant.table_var_new_referee_diploma);
                    int idDiplomaReferee = Convert.ToInt32(diplomaReferee);

                    int idEconomic = (db.ccee_CargoEconomico_otros.Where(o => o.fk_TipoCargoEconomico == idDiplomaReferee)
                                                            .Where(p => p.fk_Colegiado == id)
                                                            .Where(q => q.fk_Pago != null)
                                                            .Select(r => r.CEcO_NoCargoEconomico))
                                                            .FirstOrDefault();
                    if (idEconomic != 0)
                    {
                        ccee_Documento doc = (db.ccee_Documento
                                                .Where(o => o.fk_CargoEconomicoOtros == idEconomic))
                                                .FirstOrDefault();

                        if (doc == null)
                        {
                            ccee_TipoDoc tipo = db.ccee_TipoDoc.Find(6);
                            ccee_Colegiado cole = db.ccee_Colegiado.Find(id);
                            string template = tipo.TDC_XmlPlantilla;
                            string name = cole.Col_PrimerNombre + " " + cole.Col_SegundoNombre + " " + cole.Col_PrimerApellido + " " + cole.Col_SegundoApellido;

                            template = template.Replace(Constant.html_pdf_remplace_No_referree, name);
                            
                            ccee_Documento tempDoc = new ccee_Documento();
                            tempDoc.Doc_Observacion = name;
                            tempDoc.Doc_FechaHoraEmision = DateTime.Now;
                            tempDoc.Doc_Xml = template;
                            tempDoc.Doc_HashDoc = "";
                            tempDoc.fk_TipoDoc = tipo.TDC_NoTipoDoc;
                            tempDoc.fk_CargoEconomicoOtros = idEconomic;

                            db.ccee_Documento.Add(tempDoc);
                            db.SaveChanges();
                            transaction.Commit();
                            return "/Payment/PrintDocument/" + tempDoc.Doc_NoDocumento + "/" + 2;

                        }
                        else
                        {
                            return "/Payment/PrintDocument/" + doc.Doc_NoDocumento + "/" + 2;
                        }
                    }

                    return "";
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return "";
                }
            }

        }

        [HttpGet]
        //[Route("/Referre/GetFilesReferee/{id}")]
        public string GetFilesReferee(int? id)
        {
            int colegiado = id.Value;
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    List<ModelListDocumentJson> listDocuments = new List<ModelListDocumentJson>();
                    DateTime dateToday = DateTime.Today;

                    var listaDoc = (from docs in db.ccee_Documento
                                join cargo in db.ccee_CargoEconomico_otros on docs.fk_CargoEconomicoOtros equals cargo.CEcO_NoCargoEconomico
                                where cargo.fk_Colegiado == colegiado
                                orderby docs.Doc_FechaHoraEmision descending
                                select new
                                {
                                    idDocu = docs.Doc_NoDocumento,
                                    hash = docs.Doc_HashDoc,
                                    fecha = docs.Doc_FechaHoraEmision,
                                    nombre = docs.Doc_ANombre
                                }).ToList();

                    if (listaDoc != null)
                    {
                        System.Diagnostics.Debug.WriteLine("PLISC:" + listaDoc.Count());
                        foreach (var doc in listaDoc)
                        {
                            Boolean vigencia = false;

                            if (doc.fecha.Value != null) {
                                TimeSpan value = dateToday.Subtract(doc.fecha.Value);
                                // verifica 3 meses
                                double diasDiferencia = value.TotalDays;
                                if (diasDiferencia < 90) {
                                    vigencia = true;
                                }
                            }
                            if (!vigencia) {
                               continue;
                            }

                            listDocuments.Add(new ModelListDocumentJson()
                            {
                                idDocument = doc.idDocu,
                                direccion = Constant.url_dir_print_document + doc.idDocu + "/" + 1,
                                direccionBase64 = Constant.url_dir_print_document + doc.idDocu + "/" + 1,
                                // direccion = Constant.url_dir_print_document_base_64 + doc.idDocu + "/" + 1,
                                // direccionBase64 = Constant.url_dir_print_document_base_64 + doc.idDocu + "/" + 1,
                                title = "Certificacion dig.",
                                hash = doc.hash,
                                vigente = vigencia,
                                fechaEmision = doc.fecha.Value.ToString(),
                                aNombre = doc.nombre
                            });
                        }
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("LISTA es null");

                    }


                    return new JavaScriptSerializer().Serialize(listDocuments);
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        [HttpPost]
        public string PrintFileReferee(RequestAccountingDocumentJson accounting)
        {

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    List<ModelListDocumentJson> listDocuments = new List<ModelListDocumentJson>();

                    string typeDocAccouting = db.ccee_Variables
                                                .Where(o => o.Var_Clave.Equals("VarReciboCol"))
                                                .Select(p => p.Var_Valor).FirstOrDefault();
                    int typeNewReferee = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "estadoNuevoColegiado"));
                    int typePendiente = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "estadoPendiente"));
                    int typCertificateNew = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "VarCertificacionNuevoCol"));

                    int idTypeDocAccount = Convert.ToInt32(typeDocAccouting);


                    ccee_Cajero tempCaj = db.ccee_Cajero.Find(accounting.idCashier);
                    ccee_Usuario tempUser = db.ccee_Usuario.Find(tempCaj.fk_Usuario);
                    string nameUser = tempUser.Usu_Nombre + " " + tempUser.Usu_Apellido;

                    string documentColString = Convert.ToString(accounting.NoDocumentCol);
                    List<int> listContable = (from cont in db.ccee_DocContable
                                                          where cont.DCo_NoRecibo == documentColString &&
                                                          cont.DCo_Eliminado != 2 && cont.DCo_NoRecibo != "0"
                                                          select cont.DCo_NoDocContable).ToList();
                    if (listContable.Count() > 0)
                    {
                        return "1";
                    }
                    
                    if (accounting.isReferee)
                    {
                        ccee_Colegiado tempCol = db.ccee_Colegiado.Find(accounting.idReferee);
                        string nameReferee = tempCol.Col_PrimerNombre + " " + tempCol.Col_SegundoNombre + " " + tempCol.Col_TercerNombre + " " +
                                            tempCol.Col_PrimerApellido + " " + tempCol.Col_SegundoApellido + " " + tempCol.Col_CasdaApellido;
                        string levelReferee = ((tempCol.Col_Sexo == 1) ? "Licda. " : "Lic. ");

                        string address = (from dir in db.ccee_Direccion
                                          where dir.fk_Colegiado == accounting.idReferee && dir.Dir_Notificacion == 1
                                          select dir.Dir_CalleAvenida + " " + dir.Dir_Zona).FirstOrDefault();
                        string noReferee = ((tempCol.fk_TipoColegiado == typeNewReferee || tempCol.fk_TipoColegiado == typePendiente) ? "----" : tempCol.Col_NoColegiado.ToString());

                        PrintFile printFile = new PrintFile(accounting.idPay, db);
                        string docReferee = printFile.createDocumentReceipt(idTypeDocAccount, noReferee,
                                                            (levelReferee + nameReferee), address, tempCol.Col_Nit,
                                                            accounting.remark, accounting.NoDocumentCol,
                                                            nameUser, accounting.idCashier);

                        listDocuments.Add(new ModelListDocumentJson()
                        {
                            idDocument = 0,
                            direccion = docReferee,
                            title = "Recibo"
                        });
                        printFile.setReceiptToEconomic(printFile.getIdDo());
                        incrementReceipCash(accounting.NoDocumentCol);

                        var referee = (from col in db.ccee_Colegiado
                                       where col.Col_NoColegiado == accounting.idReferee
                                       select new refereeTemp()
                                       {
                                           NoReferee = col.Col_NoColegiado,
                                           status = col.fk_TipoColegiado,
                                           salario = col.Col_Salario,
                                           active = col.Col_Estatus,
                                           fecha = DateTime.Now
                                       }).FirstOrDefault();

                        Referee tempReferee;
                        tempReferee = new Referee(accounting.idReferee, db);
                        tempReferee.economic();
                        //if (tempReferee.isUpdateStatus())
                        //{
                            
                        //    if (isPay(referee.status, regexVerifyStatus))
                        //        tempReferee.chageStatus();

                        //    if (referee.status == 3)
                        //    {
                        //        if (referee.active != null)
                        //        {
                        //            if (referee.active == 1)
                        //            {
                        //                tempReferee.chageStatus();
                        //            }
                        //        }
                        //    }
                        //}


                        if (isPay(referee.status, regexVerifyStatus))
                            tempReferee.chageStatus();

                        if (referee.status == 3)
                        {
                            if (referee.active != null)
                            {
                                //if (referee.active == 1)
                                //{
                                //    tempReferee.chageStatus();
                                //}
                                tempReferee.chageStatus();
                            }
                        }
                        
                        /*if(tempCol.fk_TipoColegiado == 16)
                        {
                            tempCol.fk_TipoColegiado = 1;
                            db.Entry(tempCol).State = EntityState.Modified;
                        }*/

                        if (tempCol.fk_TipoColegiado == 8)
                        {
                            tempCol.Col_Estatus = 1;
                            db.Entry(tempCol).State = EntityState.Modified;
                        }


                        db.SaveChanges();
                       PrintDocNotAccount printNot = new PrintDocNotAccount(db, accounting.idReferee);
                        
                        List<ResponseEconomicRefereeJson> listEcoOther = printFile.getListOther();
                        int correlativeOld = 0;
                        string[] lisIdOldDoc = ((accounting.listIdDocs == null) ? null : accounting.listIdDocs.Split(','));
                        foreach (ResponseEconomicRefereeJson economicOther in listEcoOther)
                        {
                            string idOldDoc = null;
                            
                            if(lisIdOldDoc != null)
                            {
                                if(correlativeOld < lisIdOldDoc.Count())
                                {
                                    idOldDoc = lisIdOldDoc[correlativeOld];
                                }
                            }

                            //Cambio de estado para el objeto de nuevo colegiado
                            if(economicOther.idTypeEconomic == typCertificateNew)
                            {
                                economicOther.calcule = "VarCertificacionCol";
                            }

                            string key = economicOther.calcule;
                            if(key != null)
                            {
                                string value = (db.ccee_Variables.Where(o => o.Var_Clave == key)
                                                                .Select(p => p.Var_Valor))
                                                                .FirstOrDefault();
                                if(value != null)
                                {
                                    try
                                    {
                                        int valueTypeDocument = Convert.ToInt32(value);
                                        if (valueTypeDocument > 0)
                                        {
                                            ccee_TipoDoc typeDoc = db.ccee_TipoDoc.Find(valueTypeDocument);
                                            if (typeDoc != null)
                                            {

                                                

                                                int idDocu = 0;

                                                if (economicOther.calcule.Equals("VarCertificacionCol") || economicOther.calcule.Equals("VarCertificacionColOdl"))
                                                {
                                                    Referee refe = new Referee(tempCol.Col_NoColegiado, db);
                                                    int isPrint = refe.isPrintEnable();
                                                    if (isPrint == Referee.REFEREE_PRINT_DOC || isPrint == Referee.REFEREE_PRINT_NEW)
                                                    {

                                                        idDocu = printNot.createDocCertification(typeDoc.TDC_NoTipoDoc, economicOther.id,
                                                                                            nameReferee, tempCol.Col_NoColegiado.ToString(),
                                                                                            typeDoc.TDC_XmlPlantilla,
                                                                                            nameUser, accounting.idReferee, tempCol.Col_FechaColegiacion,
                                                                                            idOldDoc, accounting.dateTempCol, accounting.dateTempTim,
                                                                                            accounting.idCashier);
                                                        if (isPrint == Referee.REFEREE_PRINT_NEW)
                                                        {
                                                            listDocuments.Add(new ModelListDocumentJson()
                                                            {
                                                                idDocument = 0,
                                                                direccion = "",
                                                                title = "Imprimir en documentos por recibo"
                                                            });
                                                        }
                                                        else
                                                        {

                                                            ccee_Documento doc = db.ccee_Documento.Find(idDocu);
                                                            listDocuments.Add(new ModelListDocumentJson()
                                                            {
                                                                idDocument = idDocu,
                                                                direccion = Constant.url_dir_print_document + idDocu + "/" + 1,
                                                                direccionBase64 = Constant.url_dir_print_document_base_64 + idDocu + "/" + 1,
                                                                title = economicOther.description,
                                                                hash = doc.Doc_HashDoc
                                                            });
                                                        }

                                                    }
                                                    else
                                                    {
                                                        listDocuments.Add(new ModelListDocumentJson()
                                                        {
                                                            idDocument = 0,
                                                            direccion = "",
                                                            title = "No generado por estatus"
                                                        });
                                                    }
                                                }
                                                
                                                
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        
                                    }
                                }
                            }
                        }

                        if (tempCol.fk_TipoColegiado == 16)
                        {
                            tempCol.fk_TipoColegiado = 1;
                            db.Entry(tempCol).State = EntityState.Modified;
                        }
                        db.SaveChanges();

                    }
                    else
                    {
                        ccee_NoColegiado tempCol = db.ccee_NoColegiado.Find(accounting.idNotReferee);
                        string nameReferee = tempCol.NoC_Nombre + " " + tempCol.NoC_Apellido;
                        string address = tempCol.NoC_Direccion;


                        PrintFile printFile = new PrintFile(accounting.idPay, db);
                        string docReferee = printFile.createDocumentReceipt(idTypeDocAccount, "--" +tempCol.NoC_NoNoColegiado.ToString() + "--",
                                                            nameReferee, address, tempCol.NoC_Nit,
                                                            accounting.remark, accounting.NoDocumentCol,
                                                            nameUser, accounting.idCashier);

                        listDocuments.Add(new ModelListDocumentJson()
                        {
                            idDocument = 0,
                            direccion = docReferee,
                            title = "Recibo"
                        });

                        printFile.setReceiptToEconomic(printFile.getIdDo());
                        incrementReceipCash(accounting.NoDocumentCol);
                    }

                    listDocuments.Add(new ModelListDocumentJson()
                    {
                        idDocument = 0,
                        direccion = "/Reports/aspxReports/ResumenReciboPagoAspx.aspx?idPay=" + accounting.idPay,
                        title = "Resumen"
                    });

                    db.SaveChanges();
                    transaction.Commit();
                    //transaction.Rollback();
                    return new JavaScriptSerializer().Serialize(listDocuments);

                }
                catch (Exception ex)
                {
                    int p = 0;
                }
            }

            return "";
        }

        public string RePrintFileReferee(RequestAccountingDocumentJson accounting)
        {

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    List<ModelListDocumentJson> listDocuments = new List<ModelListDocumentJson>();

                    string typeDocAccouting = db.ccee_Variables
                                                .Where(o => o.Var_Clave.Equals("VarReciboCol"))
                                                .Select(p => p.Var_Valor).FirstOrDefault();
                    int typeNewReferee = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "estadoNuevoColegiado"));
                    int typePendiente = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "estadoPendiente"));
                    int typCertificateNew = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "VarCertificacionNuevoCol"));

                    int idTypeDocAccount = Convert.ToInt32(typeDocAccouting);


                    ccee_Cajero tempCaj = db.ccee_Cajero.Find(accounting.idCashier);
                    ccee_Usuario tempUser = db.ccee_Usuario.Find(tempCaj.fk_Usuario);
                    string nameUser = tempUser.Usu_Nombre + " " + tempUser.Usu_Apellido;

                    string documentColString = Convert.ToString(accounting.NoDocumentCol);
                    List<int> listContable = (from cont in db.ccee_DocContable
                                              where cont.DCo_NoRecibo == documentColString &&
                                              cont.DCo_Eliminado != 2 && cont.DCo_NoRecibo != "0"
                                              select cont.DCo_NoDocContable).ToList();
                    if (listContable.Count() <= 0)
                    {
                        return "1";
                    }

                    if (accounting.isReferee)
                    {
                        ccee_Colegiado tempCol = db.ccee_Colegiado.Find(accounting.idReferee);
                        string nameReferee = tempCol.Col_PrimerNombre + " " + tempCol.Col_SegundoNombre + " " + tempCol.Col_TercerNombre + " " +
                                            tempCol.Col_PrimerApellido + " " + tempCol.Col_SegundoApellido + " " + tempCol.Col_CasdaApellido;
                        string levelReferee = ((tempCol.Col_Sexo == 1) ? "Licda. " : "Lic. ");

                        string address = (from dir in db.ccee_Direccion
                                          where dir.fk_Colegiado == accounting.idReferee && dir.Dir_Notificacion == 1
                                          select dir.Dir_CalleAvenida + " " + dir.Dir_Zona).FirstOrDefault();
                        string noReferee = ((tempCol.fk_TipoColegiado == typeNewReferee || tempCol.fk_TipoColegiado == typePendiente) ? "----" : tempCol.Col_NoColegiado.ToString());

                        PrintFile printFile = new PrintFile(accounting.idPay, db);
                        string docReferee = printFile.updateDocumentReceipt(idTypeDocAccount, noReferee,
                                                            (levelReferee + nameReferee), address, tempCol.Col_Nit,
                                                            accounting.remark, accounting.NoDocumentCol,
                                                            nameUser, accounting.idCashier, accounting.idDocument);

                        listDocuments.Add(new ModelListDocumentJson()
                        {
                            idDocument = 0,
                            direccion = docReferee,
                            title = "Recibo"
                        });
                        printFile.setReceiptToEconomic(printFile.getIdDo());

                        
                        db.SaveChanges();

                    }
                    

                    db.SaveChanges();
                    transaction.Commit();
                    //transaction.Rollback();
                    return new JavaScriptSerializer().Serialize(listDocuments);

                }
                catch (Exception)
                {
                    int p = 0;
                }
            }

            return "";
        }

        private void incrementReceipCash(string NoReceipt)
        {
            try
            {
                LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
                ccee_AperturaCierre cash = db.ccee_AperturaCierre.Find(login.idOpenClose);
                int number = 0;
                bool success = Int32.TryParse(NoReceipt, out number);
                cash.ApC_NoRecibo = number;
                db.Entry(cash).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
            }
        }

        [HttpPost]
        public int incrementReceipPublic(int NoReceipt)
        {
            LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
            ccee_AperturaCierre cash = db.ccee_AperturaCierre.Find(login.idOpenClose);
            cash.ApC_NoRecibo = NoReceipt;
            db.Entry(cash).State = EntityState.Modified;
            db.SaveChanges();
            return 0;
        }


        //Print general documents
        //[HttpPost]
        //public string PrintFileReferee(RequestAccountingDocumentJson accounting)
        //{
        //    //if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews) == null) return RedirectToAction("Index", "Home");

        //    try
        //    {

        //        using (var transaction = db.Database.BeginTransaction())
        //        {

        //            try
        //            {
        //                List<ModelListDocumentJson> listDocuments = new List<ModelListDocumentJson>();
        //                string typeDocAccouting = db.ccee_Variables.Where(o => o.Var_Clave.Equals("VarReciboCol")).Select(p => p.Var_Valor).FirstOrDefault();
        //                int typeDocAccoutingInt = Convert.ToInt32(typeDocAccouting);
        //                ccee_TipoDocContable tempDocCon =
        //                    db.ccee_TipoDocContable.Find(typeDocAccoutingInt);
        //                ccee_Colegiado tempCol =
        //                    db.ccee_Colegiado.Find(accounting.idReferee);
        //                ccee_Cajero tempCaj =
        //                    db.ccee_Cajero.Find(accounting.idCashier);
        //                List<ResponseEconomicJson> economic = accounting.listEconomic.ToList();

        //                int countPayReferee = (economic.Where(p => p.typeEconomic < 3)
        //                                                .Where(o => o.enable == 1)).Count();
        //                int countPayOthers = (economic.Where(p => p.typeEconomic == 3)
        //                                                .Where(o => o.enable == 1)).Count();
        //                accounting.titleName = tempCol.Col_PrimerNombre + " " + tempCol.Col_SegundoNombre + " " + tempCol.Col_PrimerApellido + " " + tempCol.Col_SegundoApellido;
        //                accounting.nit = tempCol.Col_Nit;

        //                if (countPayReferee > 0)
        //                {
        //                    IEnumerable<ResponseEconomicJson> listEco = economic.Where(p => p.typeEconomic < 3);
        //                    int idDoc = createDocumentAccunting(tempCol, tempCaj, accounting, tempDocCon, listEco, accounting.idPay, accounting.NoDocumentCol);
        //                    listDocuments.Add(new ModelListDocumentJson()
        //                    {
        //                        idDocument = idDoc,
        //                        direccion = "/Payment/Print/" + idDoc,
        //                        title = "Recibo cargos colegiado"
        //                    });

        //                    var listCole = db.ccee_CargoEconomico_colegiado.Where(p => p.fk_Pago == accounting.idPay);
        //                    foreach (ccee_CargoEconomico_colegiado referee in listCole)
        //                    {
        //                        referee.fk_DocContable = idDoc;
        //                        db.Entry(referee).State = EntityState.Modified;
        //                    }

        //                    var listTim = db.ccee_CargoEconomico_timbre.Where(p => p.fk_Pago == accounting.idPay);
        //                    foreach (ccee_CargoEconomico_timbre timbre in listTim)
        //                    {
        //                        timbre.fk_DocContable = idDoc;
        //                        db.Entry(timbre).State = EntityState.Modified;
        //                    }
        //                }

        //                if (countPayOthers > 0)
        //                {
        //                    IEnumerable<ResponseEconomicJson> listEco = economic.Where(p => p.typeEconomic == 3).Where(o => o.enable == 1);
        //                    int idDoc = createDocumentAccunting(tempCol, tempCaj, accounting, tempDocCon, listEco, accounting.idPay, accounting.NoDocumentOther);
        //                    listDocuments.Add(new ModelListDocumentJson()
        //                    {
        //                        idDocument = idDoc,
        //                        direccion = Constant.url_dir_print_doc + idDoc,
        //                        title = "Recibo otros cargos"
        //                    });

        //                    var listOther = db.ccee_CargoEconomico_otros.Where(p => p.fk_Pago == accounting.idPay);
        //                    foreach (ccee_CargoEconomico_otros other in listOther)
        //                    {
        //                        other.fk_DocContable = idDoc;
        //                        db.Entry(other).State = EntityState.Modified;
        //                    }

        //                    foreach (ResponseEconomicJson listEconomic in listEco)
        //                    {
        //                        string key = listEconomic.conditionVAlue;
        //                        if (key != null)
        //                        {
        //                            string value = (db.ccee_Variables.Where(o => o.Var_Clave == key)
        //                                                            .Select(p => p.Var_Valor))
        //                                                            .FirstOrDefault();
        //                            if (value != null)
        //                            {

        //                                try
        //                                {
        //                                    int valueTypeDocument = Convert.ToInt32(value);
        //                                    if (valueTypeDocument > 0)
        //                                    {
        //                                        ccee_TipoDoc typeDoc = db.ccee_TipoDoc.Find(valueTypeDocument);
        //                                        if (typeDoc != null)
        //                                        {
        //                                            int idDocu = createDocumentReferee(tempCol, typeDoc, listEconomic, tempCaj);
        //                                            listDocuments.Add(new ModelListDocumentJson()
        //                                            {
        //                                                idDocument = idDocu,
        //                                                direccion = Constant.url_dir_print_document + idDocu + "/" + 1,
        //                                                title = listEconomic.description
        //                                            });
        //                                        }
        //                                    }
        //                                }
        //                                catch (Exception)
        //                                {

        //                                }

        //                            }
        //                        }

        //                    }
        //                }

        //                //Cambiar el correlatico del documento
        //                LoginModelViews login = Session[Constant.session_user] as LoginModelViews;
        //                ccee_AperturaCierre cash = db.ccee_AperturaCierre.Find(login.idOpenClose);
        //                cash.ApC_NoRecibo = ((accounting.NoDocumentCol > accounting.NoDocumentOther) ? accounting.NoDocumentCol : accounting.NoDocumentOther);
        //                db.Entry(cash).State = EntityState.Modified;

        //                db.SaveChanges();
        //                transaction.Commit();
        //                return new JavaScriptSerializer().Serialize(listDocuments);
        //            }
        //            catch (Exception)
        //            {
        //                transaction.Rollback();
        //                return null;
        //            }

        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}







        public ActionResult ChangeAllReferee()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_actualizar_todos_estatus) == null) return RedirectToAction(Constant.page_redirect_no_permission_page, Constant.page_redirect_no_permission_controller);
            return View();
        }





        public ActionResult PrintFileReferees(int? id, int? ids)
        {
            //if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews) == null) return RedirectToAction("Index", "Home");
            if (id == null || ids == null)
            {
                return View();
            }

            int referee = id.Value;
            int typeDocument = ids.Value;
            ccee_TipoDoc tipo  = db.ccee_TipoDoc.Find(typeDocument);

            string html = tipo.TDC_XmlPlantilla;
            MemoryStream memory = GeneralFunction.GetPDFLetter(html);
            return File(memory, Constant.http_content_header_json);



        }



        public ccee_DocExpRequisito existFileReferee(int fileReq, int referree)
        {
            try
            {
                var tempDoc = from doc in db.ccee_DocExpRequisito
                              where doc.fk_ExpReq == fileReq && doc.fk_Colegiado == referree
                              select doc;
                return tempDoc.FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        
        private string convertStatusFile(int idType)
        {
            if(idType == 1)
            {
                return "Obligatorio";
            }else if(idType == 2)
            {
                return "Opcional";
            }else
            {
                return "";
            }
        }

        private string convertTypeDocument(int idType)
        {
            if (idType == 1)
            {
                return "Stream";
            }else if(idType == 2)
            {
                return "Upload documento";
            }else if(idType == 3)
            {
                return "Scan";
            }else
            {
                return "";
            }
        }

        private FileModelView getFileModelView(int posFiles)
        {
            if(posFiles == 0)
            {
                return null;
            }


            return new FileModelView()
            {
                nameFile = listFiles[posFiles].nameFile,
                nameFileRequirement = listFiles[posFiles].name
            };
        }

        private void incrementListFile()
        {
            positionFiles += 1;
        }

        private bool getStateCash(int id)
        {
            if(id != 0)
            {
                var tempOpenClose = from apc in db.ccee_AperturaCierre
                                    where apc.fk_Cajero == id && apc.ApC_Fecha >= DateTime.Today
                                    orderby apc.ApC_Fecha descending
                                    select new ResponseOpenCloseCashJson()
                                    {
                                        id = apc.ApC_NoAperturaCierre,
                                        amountCash = apc.ApC_MontoEfectivo,
                                        amountDoc = apc.ApC_MontoDocumento,
                                        amountTrans = apc.ApC_MontoTransaccionElectronica,
                                        openClose = apc.ApC_AbreCierra,
                                        total = apc.ApC_Total,
                                        date = apc.ApC_Fecha,
                                        idCash = apc.fk_Caja
                                    };
                foreach (ResponseOpenCloseCashJson openClose in tempOpenClose)
                {
                    if(openClose.openClose == 0 || openClose.openClose == 2)
                    {
                        return true;
                    }else
                    {
                        return false;
                    }
                }
                return false;
            }
            
            return false;
        }

        
        private int[] getCash(int id)
        {
            int[] cashierOpen = new int[2];

            var listOpenClose = from apc in db.ccee_AperturaCierre
                                where apc.fk_Cajero == id && apc.ApC_Fecha >= DateTime.Today
                                orderby apc.ApC_Fecha descending
                                select new ResponseOpenCloseCashJson()
                                {
                                    id = apc.ApC_NoAperturaCierre,
                                    amountCash = apc.ApC_MontoEfectivo,
                                    amountDoc = apc.ApC_MontoDocumento,
                                    amountTrans = apc.ApC_MontoTransaccionElectronica,
                                    openClose = apc.ApC_AbreCierra,
                                    total = apc.ApC_Total,
                                    date = apc.ApC_Fecha,
                                    idCash = apc.fk_Caja
                                };
            foreach (ResponseOpenCloseCashJson contOpenClose in listOpenClose)
            {
                if (contOpenClose.openClose == 0 || contOpenClose.openClose == 2)
                {
                    cashierOpen[0] = contOpenClose.idCash;
                    cashierOpen[1] = contOpenClose.id;
                    return cashierOpen;
                }else
                {
                    return null;
                }
            }
            return null;
        }

        public bool isPay(int? status, string regex)
        {
            if (status == null) return false;
            Regex r = new Regex(regex, RegexOptions.IgnoreCase);
            Match mat = r.Match(status.ToString());
            return mat.Success;
        }


    }
}
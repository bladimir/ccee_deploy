﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonTotales
    {
        private decimal? tempTotal { get; set; }
        public decimal? Total {
            get
            {
                return ((tempTotal == null) ? 0 :  Math.Round(tempTotal.Value, 2));
            }
            set { tempTotal = value; }
        }

    }
}
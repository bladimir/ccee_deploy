﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webcee.Class;
using webcee.Class.Constant;
using webcee.Models;
using webcee.ViewModels;

namespace webcee.Controllers
{
    public class EventController : Controller
    {

        private CCEEEntities db = new CCEEEntities();
        private static ccee_DocEvento tempDocPring;

        // GET: Event
        public ActionResult Index()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_pagina_inicio) == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            return View();
        }


        public ActionResult EventAssing()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_pagina_inicio) == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            return View();
        }


        public ActionResult AddEvent()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_pagina_inicio) == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            return View();
        }


        public ActionResult EventAssingNoReferee()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_pagina_inicio) == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            return View();
        }


        public ActionResult EventAssingNoRefereeList()
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_pagina_inicio) == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            return View();
        }


        public ActionResult Print(int? id, int? ids, int? idop)
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, Constant.operation_pagina_inicio) == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            if (id == null && ids == null && idop == null) return View();
            

            try
            {

                ccee_DocEvento tempDoc = getDocument(ids.Value, idop.Value);
                string docPrintHtml = tempDoc.DoE_Text;

                ccee_Colegiado temReferee = db.ccee_Colegiado.Find(id);
                if(idop.Value == 1)
                {
                    docPrintHtml = changeInfoGafete(tempDoc, temReferee, docPrintHtml);
                }
                else
                {
                    docPrintHtml = changeInfo(tempDoc, temReferee, docPrintHtml);
                }
                
                
                if (tempDoc.DoE_NoTipo == Constant.type_document_pdf_gafete)
                {
                    return File(GeneralFunction.GetPDFGafete(docPrintHtml), Constant.http_content_header_json);
                }
                else if(tempDoc.DoE_NoTipo == Constant.type_document_pdf_diploma)
                {
                    
                    return File(GeneralFunction.GetPDFDiploma(docPrintHtml), Constant.http_content_header_json);
                }

                return View();
            }
            catch
            {
                return View();
            }
            
            
        }


        public ActionResult PrintNoReferee(int? id, int? ids, int? idop)
        {
            if (GeneralFunction.permissions(this, Session[Constant.session_user] as LoginModelViews, "") == null) return RedirectToAction(Constant.page_redirect_no_login_page, Constant.page_redirect_no_login_controller);
            if (id == null && ids == null && idop == null) return View();
            ccee_DocEvento tempDoc = getDocument(ids.Value, idop.Value);
            string docPrintHtml = tempDoc.DoE_Text;

            try
            {
                ccee_NoColegiado temNoReferee = db.ccee_NoColegiado.Find(id);

                if (idop.Value == 1)
                {
                    docPrintHtml = changeInfoGafeteNoReferee(tempDoc, temNoReferee, docPrintHtml);
                }
                else
                {
                    docPrintHtml = changeInfoNoReferee(tempDoc, temNoReferee, docPrintHtml);
                }
                

                if (tempDoc.DoE_NoTipo == Constant.type_document_pdf_gafete)
                {
                    return File(GeneralFunction.GetPDFGafete(docPrintHtml), Constant.http_content_header_json);
                }
                else if (tempDoc.DoE_NoTipo == Constant.type_document_pdf_diploma)
                {
                    return File(GeneralFunction.GetPDFDiploma(docPrintHtml), Constant.http_content_header_json);
                }

                return View();
            }
            catch
            {
                return View();
            }
            
        }


        //Funciones internas
        private ccee_DocEvento getDocument(int idSubEvento, int idDocType)
        {
            
            if (tempDocPring == null)
            {
                tempDocPring = searchDocument(idSubEvento, idDocType);
            }

            if(tempDocPring.DoE_NoTipo != idDocType)
            {
                tempDocPring = searchDocument(idSubEvento, idDocType);
            }

            return tempDocPring;

        }

        private ccee_DocEvento searchDocument(int idSubEvento, int idDocType)
        {
            var contentPrint = from doc in db.ccee_DocEvento
                               join subdoc in db.ccee_SubEventoDoc on doc.DoE_NoDocEvento equals subdoc.pkfk_DocEvento
                               where subdoc.pkfk_SubEvento == idSubEvento && doc.DoE_NoTipo == idDocType
                               select doc;

            foreach (ccee_DocEvento even in contentPrint)
            {
                return even;
            }
            return null;
        }


        private string changeInfo(ccee_DocEvento events, ccee_Colegiado coleg, string info)
        {
            if(events.DoE_Nombre.Value == Constant.state_event_for_print)
            {
                string name = "";
                if (coleg.Col_Sexo.Equals("Y"))
                {
                    name = "Lic. ";
                }
                else if (coleg.Col_Sexo.Equals("N"))
                {
                    name = "Licda. ";
                }

                name += coleg.Col_PrimerNombre + " " + coleg.Col_SegundoNombre
                                + " " + coleg.Col_PrimerApellido + " " + coleg.Col_SegundoApellido;
                info = info.Replace(Constant.html_pdf_remplace_name, name);
            }

            return info;
        }

        private string changeInfoGafete(ccee_DocEvento events, ccee_Colegiado coleg, string info)
        {
            if (events.DoE_Nombre.Value == Constant.state_event_for_print)
            {
                string name = "";

                name = coleg.Col_PrimerNombre 
                                + "<br />" + coleg.Col_PrimerApellido ;
                info = info.Replace(Constant.html_pdf_remplace_name, name);
            }

            return info;
        }

        private string changeInfoNoReferee(ccee_DocEvento events, ccee_NoColegiado noNoleg, string info)
        {
            if (events.DoE_Nombre.Value == Constant.state_event_for_print)
            {
                string name = noNoleg.NoC_Nombre + " " + noNoleg.NoC_Apellido ;
                info = info.Replace(Constant.html_pdf_remplace_name, name);
            }

            return info;
        }

        private string changeInfoGafeteNoReferee(ccee_DocEvento events, ccee_NoColegiado noNoleg, string info)
        {
            if (events.DoE_Nombre.Value == Constant.state_event_for_print)
            {
                string name = noNoleg.NoC_Nombre + "<br />" + noNoleg.NoC_Apellido;
                info = info.Replace(Constant.html_pdf_remplace_name, name);
            }

            return info;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
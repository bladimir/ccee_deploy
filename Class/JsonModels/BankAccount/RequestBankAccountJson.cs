﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.BankAccount
{
    public class RequestBankAccountJson
    {
        public string NoAccount { get; set; }
        public string codigo { get; set; }
        public string format { get; set; }
        public string name { get; set; }
        public int idBank { get; set; }
    }
}
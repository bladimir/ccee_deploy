﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_RefereeNewController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_RefereeNew
        public int Get()
        {
            string autorization = Request.Headers.Authorization.Parameter;
            if (!Autorization.enableAutorization(db, autorization)) return 0;

            var tempRefree = db.ccee_Colegiado.Where(p=>p.Col_NoColegiado < Constant.referee_init_temp)
                                    .Max(p => p.Col_NoColegiado);

            Autorization.saveOperation(db, autorization, "Obteniendo conteo colegiado");
            return tempRefree + 1;
        }

        // GET: api/Api_RefereeNew/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_RefereeNew
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_RefereeNew/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_RefereeNew/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

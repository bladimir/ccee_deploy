﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonResumenInventarioDate
    {
        public decimal? total { get; set; }
        public int month { get; set; }
        public int year { get; set; }
        public string fecha
        {
            get
            {
                CultureInfo culture;
                culture = CultureInfo.CreateSpecificCulture("es-GT");
                 
                return culture.DateTimeFormat.GetMonthName(this.month)
                            + " del " + year;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.PaymentMethod_Pay;
using webcee.Class.JsonModels.RefundPayment;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_RefundPaymentController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_RefundPayment
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_RefundPayment/5
        public List<ResponseRefundPaymentJson> Get(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                List<ResponseRefundPaymentJson> temRefund = new List<ResponseRefundPaymentJson>();
                var listC = from eco in db.ccee_CargoEconomico_colegiado
                            join pay in db.ccee_Pago on eco.fk_Pago equals pay.Pag_NoPago
                            join doc in db.ccee_DocContable on eco.fk_DocContable equals doc.DCo_NoDocContable
                            where eco.fk_Colegiado == id && pay.Pag_Congelado == 1
                            select new ResponseRefundPaymentJson()
                            {
                                id = pay.Pag_NoPago,
                                datePay = pay.Pag_Fecha,
                                total = pay.Pag_Monto,
                                document = doc.DCo_NoRecibo
                            };

                var listT = from eco in db.ccee_CargoEconomico_timbre
                            join pay in db.ccee_Pago on eco.fk_Pago equals pay.Pag_NoPago
                            join doc in db.ccee_DocContable on eco.fk_DocContable equals doc.DCo_NoDocContable
                            where eco.fk_Colegiado == id && pay.Pag_Congelado == 1
                            select new ResponseRefundPaymentJson()
                            {
                                id = pay.Pag_NoPago,
                                datePay = pay.Pag_Fecha,
                                total = pay.Pag_Monto,
                                document = doc.DCo_NoRecibo
                            };

                var listO = from eco in db.ccee_CargoEconomico_otros
                            join pay in db.ccee_Pago on eco.fk_Pago equals pay.Pag_NoPago
                            join doc in db.ccee_DocContable on eco.fk_DocContable equals doc.DCo_NoDocContable
                            where eco.fk_Colegiado == id && pay.Pag_Congelado == 1
                            select new ResponseRefundPaymentJson()
                            {
                                id = pay.Pag_NoPago,
                                datePay = pay.Pag_Fecha,
                                total = pay.Pag_Monto,
                                document = doc.DCo_NoRecibo
                            };

                temRefund.AddRange(listT);
                temRefund.AddRange(listC);
                temRefund.AddRange(listO);

                temRefund = temRefund
                .GroupBy(o => new
                {
                    Id = o.id,
                    DatePay = o.datePay,
                    Total = o.total,
                    Doc = o.document
                })
                .Select(g => new ResponseRefundPaymentJson
                {
                    id = g.Key.Id,
                    datePay = g.Key.DatePay,
                    total = g.Key.Total,
                    document = g.Key.Doc
                })
                .OrderByDescending(p=>p.datePay)
                .ToList();
                Autorization.saveOperation(db, autorization, "Obteniendo pagos congelados");
                return temRefund;

            }
            catch (Exception)
            {
                return null;
            }
        }

        [Route("api/Api_RefundPayment/{id}/{ids}")]
        public ResponseRefundPaymentJson Get(int id, int ids)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                try
                {
                    int? idPay = null;
                    idPay = (from eco in db.ccee_CargoEconomico_colegiado
                                where eco.fk_DocContable == id
                                select eco.fk_Pago).FirstOrDefault();

                    if (idPay == null)
                    {
                        idPay = (from eco in db.ccee_CargoEconomico_timbre
                                    where eco.fk_DocContable == id
                                    select eco.fk_Pago).FirstOrDefault();
                    }

                    if (idPay == null)
                    {
                        idPay = (from eco in db.ccee_CargoEconomico_otros
                                    where eco.fk_DocContable == id
                                    select eco.fk_Pago).FirstOrDefault();
                    }

                    if (idPay != null)
                    {

                        var totalPay = db.ccee_Pago.Where(p => p.Pag_NoPago == idPay)
                                        .Select(o => o.Pag_Monto).Sum();
                                        

                        List<ResponseRefundPaymentListJson> econimic = (from mec in db.ccee_MedioPago_Pago_C
                                       join med in db.ccee_MedioPago on mec.fk_MedioPago equals med.MedP_NoMedioPago
                                       join ban in db.ccee_BancoOEmisor on mec.fk_Banco equals ban.BanE_NoBancoEmisor
                                       where mec.fk_Pago == idPay
                                       select new ResponseRefundPaymentListJson()
                                       {
                                           id = mec.MPP_NoMedioPP,
                                           document = mec.MPP_NoDocumento,
                                           transaction = mec.MPP_NoTransaccion,
                                           amount = mec.MPP_Monto,
                                           description = med.MdP_Descripcion,
                                           bank = ban.BcE_NombreBancoEmisor
                                       }).ToList();

                        var total = totalPay - econimic.Sum(o => o.amount);

                        ResponseRefundPaymentJson respose = new ResponseRefundPaymentJson();
                        respose.idPay = idPay;
                        respose.list = econimic;
                        respose.diference = total;

                        db.SaveChanges();
                        Autorization.saveOperation(db, autorization, "Agregando medio de pago a pago congelado");
                        transaction.Commit();
                        return respose;
                        
                    }

                    return null;
                    
                }
                catch (Exception)
                {
                    transaction.Rollback();   
                }
            }
                return null;
        }

        // POST: api/Api_RefundPayment
        public IHttpActionResult Post(RequestRefundPaymentJson refund)
        {
            using (var transaction = db.Database.BeginTransaction())
            {

                try
                {
                    string autorization = Request.Headers.Authorization.Parameter;
                    if (!Autorization.enableAutorization(db, autorization)) return null;

                    ccee_Pago tempPay = db.ccee_Pago.Find(refund.objectCheck.fk_Pago);
                    if(tempPay == null)
                    {
                        transaction.Rollback();
                        return Ok(HttpStatusCode.NotFound);
                    }

                    tempPay.Pag_Congelado = null;
                    db.Entry(tempPay).State = EntityState.Modified;

                    foreach (RequestPaymentMethod_PayJson paymentMethod in refund.jsonContentMethod)
                    {
                        ccee_MedioPago_Pago_C tempPaymentMethodPay = new ccee_MedioPago_Pago_C()
                        {
                            MPP_Monto = paymentMethod.amount,
                            MPP_NoTransaccion = paymentMethod.noTransaction,
                            MPP_NoDocumento = paymentMethod.noDocument,
                            MPP_Observacion = paymentMethod.remarke,
                            MPP_Reserva = paymentMethod.reservation,
                            MPP_Rechazado = paymentMethod.rejection,
                            MPP_FechaHora = DateTime.Now,
                            fk_MedioPago = paymentMethod.idPaymentMethod,
                            fk_Pago = tempPay.Pag_NoPago,
                            fk_Banco = paymentMethod.idBank
                        };
                        db.ccee_MedioPago_Pago_C.Add(tempPaymentMethodPay);
                    }
                    db.SaveChanges();
                    Autorization.saveOperation(db, autorization, "Agregando medio de pago a pago congelado");
                    transaction.Commit();
                    return Ok(HttpStatusCode.Accepted);

                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return Ok(HttpStatusCode.BadRequest);
                }

            }
        }

        // PUT: api/Api_RefundPayment/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_RefundPayment/5
        public IHttpActionResult Delete(int id)
        {
            try
            {

                try
                {
                    string autorization = Request.Headers.Authorization.Parameter;
                    if (!Autorization.enableAutorization(db, autorization)) return null;

                    ccee_MedioPago_Pago_C tempMethod = db.ccee_MedioPago_Pago_C.Find(id);
                    if (tempMethod == null)
                    {
                        return Ok(HttpStatusCode.NotFound);
                    }

                    db.ccee_MedioPago_Pago_C.Remove(tempMethod);

                    Autorization.saveOperation(db, autorization, "Eliminando Medio de pago pago");

                    db.SaveChanges();
                    return Ok(HttpStatusCode.OK);
                }
                catch (Exception)
                {
                    return Ok(HttpStatusCode.BadRequest);
                }

            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.JsonModels.ValidarDocumento;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_ValidarDocumentoController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_ValidarDocumento
        public IEnumerable<string> Get()
        {
            return new string[] { "" };
        }

        // GET: api/Api_ValidarDocumento/5
        public RespoonseValidarDocumentoJson Get(string id)
        {
            try
            {
                int idDocument = Convert.ToInt32(id.Substring(12));

                var document = (from doc in db.ccee_Documento
                               where doc.Doc_NoDocumento == idDocument
                                select new RespoonseValidarDocumentoJson()
                               {
                                   number = doc.Doc_NoDocumento,
                                   dateEmit = doc.Doc_FechaHoraEmision,
                                   dateFinish = doc.Doc_FechaFinal,
                                   name = doc.Doc_ANombre,
                                   institution = doc.Doc_Observacion,
                                   dateConsumption = doc.Doc_FechaHoraConsumo
                               }).FirstOrDefault();

                return document;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // POST: api/Api_ValidarDocumento
        public void Post(RequestValidarDocumentoJson document)
        {
        }

        // PUT: api/Api_ValidarDocumento/5
        public string Put(int id, RequestValidarDocumentoJson document)
        {
            try
            {

                ccee_Documento doc = db.ccee_Documento.Find(id);
                if (doc == null)
                {
                    return "Documento no encontrado";
                }

                if(doc.Doc_FechaHoraConsumo != null)
                {
                    string documento = doc.Doc_Observacion;
                    return "El documento ya se consumió por: " + documento;
                }

                doc.Doc_FechaHoraConsumo = DateTime.Now;
                doc.Doc_Observacion = document.institution;
                doc.Doc_Estado = 1;
                db.Entry(doc).State = EntityState.Modified;
                db.SaveChanges();

                return "Documento validado con éxito por: " + document.institution;
            }
            catch (Exception)
            {
                return "Problemas con el servidor por favor intentar de nuevo";
            }
        }

        // DELETE: api/Api_ValidarDocumento/5
        public string Delete(int id)
        {
            try
            {
                ccee_Documento doc = db.ccee_Documento.Find(id);
                if (doc == null)
                {
                    return "Documento no encontrado";
                }

                doc.Doc_FechaHoraConsumo = DateTime.Now;
                doc.Doc_Observacion = "Invalidado por CCEE";
                doc.Doc_Estado = 2;
                db.Entry(doc).State = EntityState.Modified;
                db.SaveChanges();

                return "Documento invalidado ";
            }
            catch (Exception)
            {
                return "Problemas con el servidor por favor intentar de nuevo";
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

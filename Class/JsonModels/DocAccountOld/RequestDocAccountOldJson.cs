﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.DocAccountOld
{
    public class RequestDocAccountOldJson
    {
        public int idPay { get; set; }
        public int idTypeDoc { get; set; }
        public int NoReferee { get; set; }
        public string nameReferee { get; set; }
        public string addressReferee { get; set; }
        public string nitReferee { get; set; }
        public string remarkDoc { get; set; }
        public string nameUserCashier { get; set; }
        public int idDocAccount { get; set; }
    }
}
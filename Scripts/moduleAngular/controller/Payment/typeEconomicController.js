﻿(function () {
    angular.module('payment')
        .controller('typeEconomic', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'paymentFactory', function ($scope, $http, CONSTANT, $anchorScroll, paymentFactory) {
            $scope.listTypeEconomic;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idTypeEconomicUpdate = 0;
            $scope.objectTypeEconomic;
            $scope.listTypeAccounting;
            $scope.listAccounting;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                paymentFactory.actionGetFunction(CONSTANT.URL_API_TYPEECONOMIC)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listTypeEconomic = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });

                paymentFactory.actionGetFunction(CONSTANT.URL_API_TYPEACCOUNTING)
                    .then(function (response) {
                        console.log(response);
                        $scope.hideLoadingFunction();
                        $scope.listTypeAccounting = response.data;
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });
            }


            $scope.setAuth = function () {
                paymentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        paymentFactory.setAuthorization(response.data);
                        $scope.init();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.getTypeEconomic = function () {
                $scope.showLoadingFunction();
                paymentFactory.actionGetFunction(CONSTANT.URL_API_TYPEECONOMIC)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listTypeEconomic = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.searchAccount = function () {
                $scope.idTypeAccounting = $scope.selectaccounttype.id;
                $scope.getAccounting($scope.idTypeAccounting);
            }

            $scope.getAccounting = function (id) {
                $scope.showLoadingFunction();
                paymentFactory.actionGetIntFunction(CONSTANT.URL_API_ACCOUNTING, id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listAccounting = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.addTypeEconomic = function () {
                var statusT = (($scope.enableeconomic == true) ? 1 : 0);
                $scope.showLoadingFunction();
                var information = {
                    description: $scope.descriptioneconimic,
                    debit: $scope.typedebiteconimic,
                    percent: $scope.percenteconimic,
                    valor: $scope.valoreconimic,
                    periodicity: $scope.periodicityeconomic,
                    calculationCondition: $scope.calculationeconomic,
                    idaccounting: $scope.selectaccount.id,
                    initDate: $scope.initeconomic,
                    finalyDate: $scope.finaleconomic,
                    status: statusT
                }

                console.log(information);
                paymentFactory.actionPostFunction(CONSTANT.URL_API_TYPEECONOMIC, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.cleanData();
                                $scope.getTypeEconomic();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (type) {
                $scope.objectTypeEconomic = type;
                $scope.idTypeEconomicUpdate = type.id;
                $scope.descriptioneconimic = type.description;
                $scope.typedebiteconimictext = $scope.debitConvert(type.debit);
                $scope.percenteconimic = type.percent;
                $scope.valoreconimic = type.valor;
                $scope.periodicityeconomictext = $scope.periodicityConvert(type.periodicity);
                $scope.calculationeconomic = type.calculationCondition;
                $scope.initeconomictext = type.initDate;
                $scope.finaleconomictext = type.finalyDate;
                $scope.enableeconomic = ((type.status == 1) ? true : false);
                $scope.accounteconomic = type.nameAccouting;
                $anchorScroll();
            }

            $scope.updateTypeEconomic = function () {
                var statusT = (($scope.enableeconomic == true) ? 1 : 0);
                $scope.showLoadingFunction();
                var information = {
                    description: $scope.descriptioneconimic,
                    debit: (($scope.typedebiteconimic == null) ? $scope.objectTypeEconomic.debit : $scope.typedebiteconimic),
                    percent: $scope.percenteconimic,
                    valor: $scope.valoreconimic,
                    periodicity: (($scope.periodicityeconomic == null) ? $scope.objectTypeEconomic.periodicity : $scope.periodicityeconomic),
                    calculationCondition: $scope.calculationeconomic,
                    idaccounting: (($scope.selectaccount == null) ? $scope.objectTypeEconomic.idaccounting : $scope.selectaccount.id),
                    initDate: (($scope.initeconomic == null) ? $scope.objectTypeEconomic.initDate : $scope.initeconomic),
                    finalyDate: (($scope.finaleconomic == null) ? $scope.objectTypeEconomic.finalyDate : $scope.finaleconomic),
                    status: statusT
                }
                paymentFactory.actionPutFunction(CONSTANT.URL_API_TYPEECONOMIC, $scope.idTypeEconomicUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.getTypeEconomic();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }


            $scope.deleteTypeEspecialty = function (type) {
                $scope.showLoadingFunction();
                paymentFactory.actionDeleteFunction(CONSTANT.URL_API_TYPEECONOMIC, type.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getTypeEconomic();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idTypeEconomicUpdate = 0;
                $scope.objectTypeEconomic = null;
                $scope.descriptioneconimic = "";
                $scope.typedebiteconimictext = "";
                $scope.percenteconimic = "";
                $scope.valoreconimic = "";
                $scope.periodicityeconomictext = "";
                $scope.calculationeconomic = "";
                $scope.initeconomictext = "";
                $scope.finaleconomictext = "";
            }

            $scope.debitConvert = function (value) {
                if (value == 0) {
                    return "Debito";
                }else if(value == 1){
                    return "Credito"
                } else {
                    return "Sin definir"
                }
            }

            $scope.periodicityConvert = function (value) {
                if (value == 0) {
                    return "Por demanda";
                } else if (value == 1) {
                    return "Mes"
                } else if (value == 2) {
                    return "Año"
                } else {
                    return "Sin definir"
                }
            }


        }]);

})()
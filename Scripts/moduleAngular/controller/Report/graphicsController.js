﻿(function () {
    angular.module('reports')
        .controller('graphics', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'reportFactory', function ($scope, $http, CONSTANT, $anchorScroll, reportFactory) {
            $scope.listGraph;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idGraphUpdate = 0;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            reportFactory.actionGetFunction(CONSTANT.URL_API_GRAPHIC)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listGraph = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });


            $scope.getGraph = function () {
                $scope.showLoadingFunction();
                reportFactory.actionGetFunction(CONSTANT.URL_API_GRAPHIC)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listGraph = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.addGraph = function () {
                $scope.showLoadingFunction();
                var information = {
                    description: $scope.graphdescription,
                    reportId: $scope.graphreportid,
                    workspaceCollection: $scope.graphcollection,
                    workspaceId: $scope.graphworkspace
                }
                console.log(information);
                reportFactory.actionPostFunction(CONSTANT.URL_API_GRAPHIC, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getGraph();
                                $scope.cleanData();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.showDataUpdate = function (gra) {
                $scope.idGraphUpdate = gra.id;
                $scope.graphdescription = gra.description;
                $scope.graphreportid = gra.reportId;
                $scope.graphcollection = gra.workspaceCollection;
                $scope.graphworkspace = gra.workspaceId;
                $anchorScroll();
            }

            $scope.updateGraph = function () {
                $scope.showLoadingFunction();
                var information = {
                    description: $scope.graphdescription,
                    reportId: $scope.graphreportid,
                    workspaceCollection: $scope.graphcollection,
                    workspaceId: $scope.graphworkspace
                }
                reportFactory.actionPutFunction(CONSTANT.URL_API_GRAPHIC, $scope.idGraphUpdate, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.cleanData();
                            $scope.getGraph();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }

            $scope.deleteGraph = function (gra) {
                $scope.showLoadingFunction();
                reportFactory.actionDeleteFunction(CONSTANT.URL_API_GRAPHIC, gra.id)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.getGraph();
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.cleanData = function () {
                $scope.idGraphUpdate = 0;
                $scope.graphdescription = "";
                $scope.graphreportid = "";
                $scope.graphcollection = "";
                $scope.graphworkspace = "";
            }

            $scope.viewGraphic = function (gra) {
                window.location.href = '/Reports/ReportGraph/' + gra.id;
            }


        }]);

})()
﻿(function () {
    angular.module('payment')
        .controller('incompletePayment', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'paymentFactory', '$window', function ($scope, $http, CONSTANT, $anchorScroll, paymentFactory, $window) {
            $scope.listT;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.idTypeAccountingUpdate = 0;
            $scope.recover = null;
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquaters;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                paymentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        paymentFactory.setAuthorization(response.data);
                        $scope.initF();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.initF = function () {
                $scope.getCashier();
            }

            

            $scope.getCashier = function () {
                paymentFactory.actionGetFunction(CONSTANT.URL_API_CASHIER)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listCashier = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getRecoverReceipt = function () {
                console.log($scope.idReferee);
                $scope.showLoadingFunction();
                paymentFactory.actionGetIntFunction(CONSTANT.URL_API_RECOVERRECEIPT, $scope.idReferee)
                .then(function (response) {
                    $scope.listT = response.data;
                    $scope.hideLoadingFunction();
                }, function (error) {
                    console.log(error);
                    $scope.hideLoadingFunction();
                });
            }

            $scope.search = function () {
                var dateInitInfo = ($scope.inDateI.getFullYear() + "-" + ($scope.inDateI.getMonth() + 1) + "-" + $scope.inDateI.getDate());
                var idCashier = $scope.inCashi.id;
                console.log(idCashier);
                $scope.showLoadingFunction();
                paymentFactory.actionGetGetIntFunction(CONSTANT.URL_API_INCOMPLETEPAYMENT, dateInitInfo, idCashier)
                .then(function (response) {
                    $scope.listIncomplete = response.data;
                    $scope.hideLoadingFunction();
                    console.log($scope.listIncomplete);
                }, function (error) {
                    console.log(error);
                    $scope.hideLoadingFunction();
                });

            }


            $scope.setRecover = function (rec) {
                window.location.href = '/Payment/RecoverReceipt/#?id=' + rec.referee;
            }


        }]);

})()
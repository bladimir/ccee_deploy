﻿(function () {
    angular.module('referee')
        .controller('deleteReferee', ['$scope', '$http', '$location', 'CONSTANT', '$anchorScroll', 'refereeFactory', function ($scope, $http, $location, CONSTANT, $anchorScroll, refereeFactory) {
            $scope.idReferee = 0;
            $scope.numbers = CONSTANT.REGEX_DIGIT;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            $scope.info = "";

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.defineDelete = function () {
                $scope.isdelete = true;
                $scope.idReferee = $scope.referee;
                $scope.info = "";
            }

            $scope.setAuth = function () {
                refereeFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    refereeFactory.setAuthorization(response.data);
                    console.log(refereeFactory.getAuthorization());
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.delete = function () {
                $scope.showLoadingFunction();
                refereeFactory.actionDeleteFunction(CONSTANT.URL_API_REFEREE, $scope.idReferee)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.info = "El colegiado fue eliminado satisfactoriamente";
                                $scope.isdelete = false;
                            } else if (response.data == 404) {
                                $scope.info = "El colegiado no existe";
                                $scope.isdelete = false;
                            } else {
                                $scope.info = "Problema con la eliminación, posiblemente tiene datos en dirección, telefonos, etc.";
                                $scope.isdelete = false;
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }

            $scope.reverse = function () {
                $scope.showLoadingFunction();
                refereeFactory.actionDeleteFunction(CONSTANT.URL_GET_REFEREEREVERT, $scope.referee)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.changeReceipt();
                            } else if (response.data == 404) {
                                $scope.info = "El colegiado no existe";
                                $scope.isdelete = false;
                            } else {
                                $scope.info = "Problema con la eliminación, posiblemente tiene datos en dirección, telefonos, etc.";
                                $scope.isdelete = false;
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                        });
            }



            $scope.changeFiles = function () {
                window.location.href = '/Referre/FileReferee/' + $scope.refereeNumber;
            }

            $scope.changeReceipt = function () {
                window.location.href = '/Payment/Receipt';
            }


        }]);

})()
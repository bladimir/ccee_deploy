﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.AddressFamily
{
    public class RequestAddressFamilyJson
    {
        public int idFamily { get; set; }
        public int idAddress { get; set; }
    }
}
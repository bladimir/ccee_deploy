﻿(function () {
    angular.module('reports')
        .controller('chequesRechazados', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'reportFactory', function ($scope, $http, CONSTANT, $anchorScroll, reportFactory) {
            $scope.direction;
            $scope.listCountry;
            $scope.listDepartament;
            $scope.listMunicipality;
            $scope.listHeadquaters;
            $scope.regexReferee = CONSTANT.REGEX_REFEREE;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.inDateI = new Date(2000, 0, 1);
            $scope.inDateF = new Date(2030, 0, 1);

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.identical = function (actual, expected) {
                return actual === parseInt(expected);
            }

            $scope.init = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        reportFactory.setAuthorization(response.data);
                        $scope.initF();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.initF = function () {
                $scope.getHeadquarter();
                $scope.getCashier();
                $scope.getCash();
            }

            $scope.getHeadquarter = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_HEADQUARTERS)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listHeadquarter = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getCashier = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_CASHIER)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listCashier = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getCash = function () {
                reportFactory.actionGetFunction(CONSTANT.URL_API_CASH)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listCash = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getDataSelect = function (list) {
                if (list == null) return "";
                var len = list.length;

                var listTotal = "";
                for (var i = 0; i < len; i++) {
                    listTotal += list[i].id + ((len > 0 && i < len - 1) ? "," : "");
                }
                return listTotal;
            }

            $scope.getDataSelectNames = function (list) {
                if (list == null) return "";
                var len = list.length;

                var listTotal = "";
                for (var i = 0; i < len; i++) {
                    listTotal += list[i].name + ((len > 0 && i < len - 1) ? "," : "");
                }
                return listTotal;
            }

            $scope.getDataSelectNamesHead = function (list) {
                if (list == null) return "";
                var len = list.length;

                var listTotal = "";
                for (var i = 0; i < len; i++) {
                    listTotal += list[i].address + ((len > 0 && i < len - 1) ? "," : "");
                }
                return listTotal;
            }


            $scope.generateList = function () {
                var textGetReport = "";
                console.log(($scope.selectHead != null) ? $scope.selectHead.id : "");
                textGetReport = "&dateI=" + ($scope.inDateI.getFullYear() + "-" + ($scope.inDateI.getMonth() + 1) + "-" + $scope.inDateI.getDate()) +
                                "&dateF=" + ($scope.inDateF.getFullYear() + "-" + ($scope.inDateF.getMonth() + 1) + "-" + $scope.inDateF.getDate()) +
                                "&cash=" + $scope.getDataSelect($scope.inCash) +
                                "&rech=" + $scope.inRec +
                                "&cashi=" + $scope.getDataSelect($scope.inCashi) +
                                "&refe=" + $scope.inRefe +
                                "&head=" + $scope.getDataSelect($scope.inHead) +
                                "&froz=" + $scope.inFro +

                                "&cashiN=" + $scope.getDataSelectNames($scope.inCashi) +
                                "&cashN=" + $scope.getDataSelectNames($scope.inCash) +
                                "&headN=" + $scope.getDataSelectNamesHead($scope.inHead);
                console.log(textGetReport);
                $scope.direction = "../../Reports/aspxReports/ChequesRechazadosAspx.aspx?" + textGetReport;
            }

        }]);

})()
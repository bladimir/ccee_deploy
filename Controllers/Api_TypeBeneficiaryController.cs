﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.TypeBeneficiary;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_TypeBeneficiaryController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_TypeBeneficiary
        public IEnumerable<ResponseTypeBeneficiaryJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var listBeneficiary = from tbene in db.ccee_TipoBeneficiario
                                      select new ResponseTypeBeneficiaryJson()
                                      {
                                          id = tbene.TBe_NoTipoBeneficiario,
                                          name = tbene.TBe_NombreTipo,
                                          description = tbene.TBe_DescripcionTipo
                                      };
                Autorization.saveOperation(db, autorization, "Obteniendo tipo beneficiario");
                return listBeneficiary;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_TypeBeneficiary/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_TypeBeneficiary
        public IHttpActionResult Post(RequestTypeBeneficiaryJson typeBeneficiary)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_TipoBeneficiario tempTypeBeneficiary = new ccee_TipoBeneficiario()
                {
                    TBe_NombreTipo = typeBeneficiary.name,
                    TBe_DescripcionTipo = typeBeneficiary.description
                };

                db.ccee_TipoBeneficiario.Add(tempTypeBeneficiary);
                Autorization.saveOperation(db, autorization, "Agregando tipo beneficiario");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_TypeBeneficiary/5
        public IHttpActionResult Put(int id, RequestTypeBeneficiaryJson typeBeneficiary)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_TipoBeneficiario tempTypeBeneficiary = db.ccee_TipoBeneficiario.Find(id);

                if (tempTypeBeneficiary == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempTypeBeneficiary.TBe_NombreTipo = typeBeneficiary.name;
                tempTypeBeneficiary.TBe_DescripcionTipo = typeBeneficiary.description;

                db.Entry(tempTypeBeneficiary).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando tipo beneficiario");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_TypeBeneficiary/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_TipoBeneficiario tempTypeBeneficiary = db.ccee_TipoBeneficiario.Find(id);
                if (tempTypeBeneficiary == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_TipoBeneficiario.Remove(tempTypeBeneficiary);
                Autorization.saveOperation(db, autorization, "Eliminando tipo beneficiario");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

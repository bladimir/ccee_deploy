﻿(function () {
    angular.module('user')
        .factory('userFactory', ['$http', 'CONSTANTE', function ($http, CONSTANTE) {

            var refereeFactory = {};
            var authorization = "";

            refereeFactory.setAuthorization = function (id) {
                authorization = id;
            };

            refereeFactory.getAuthorization = function () {
                return authorization;
            };

            refereeFactory.actionGetFunction = function (dirr) {
                return $http.get(dirr, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            };

            refereeFactory.actionGetIntFunction = function (dirr, parameter) {
                return $http.get(dirr + "/" + parameter, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            refereeFactory.actionGetGetIntFunction = function (dirr, parameter, secoundparameter) {
                return $http.get(dirr + "/" + parameter + "/" + secoundparameter, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            refereeFactory.actionPostFunction = function (dirr, infojson) {
                return $http.post(dirr, JSON.stringify(infojson),
                    {
                        headers: {
                            'Content-Type': CONSTANTE.HEADER_CONTENT_TYPE_JSON,
                            'Authorization': 'Basic ' + authorization
                        }
                    });
            }

            refereeFactory.actionPutFunction = function (dirr, id, infojson) {
                return $http.put(dirr + "/" + id, JSON.stringify(infojson),
                    {
                        headers: {
                            'Content-Type': CONSTANTE.HEADER_CONTENT_TYPE_JSON,
                            'Authorization': 'Basic ' + authorization
                        }
                    });
            }

            refereeFactory.actionDeleteFunction = function (dirr, parameter) {
                return $http.delete(dirr + "/" + parameter, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            refereeFactory.actionDeleteTwoFunction = function (dirr, parameter, parameterTwo) {
                return $http.delete(dirr + "/" + parameter + "/" + parameterTwo, {
                    headers: { 'Authorization': 'Basic ' + authorization }
                });
            }

            return refereeFactory;
        }]);

})()
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace webcee.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ccee_BancoOEmisor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ccee_BancoOEmisor()
        {
            this.ccee_CuentaBancaria = new HashSet<ccee_CuentaBancaria>();
            this.ccee_MedioPago_Pago = new HashSet<ccee_MedioPago_Pago>();
            this.ccee_MedioPago_Pago_C = new HashSet<ccee_MedioPago_Pago_C>();
        }
    
        public int BanE_NoBancoEmisor { get; set; }
        public string BcE_NombreBancoEmisor { get; set; }
        public string BcE_Direccion { get; set; }
        public string BcE_Telefono { get; set; }
        public Nullable<int> BcE_TipoInstitucion { get; set; }
        public string BcE_Servicio { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ccee_CuentaBancaria> ccee_CuentaBancaria { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ccee_MedioPago_Pago> ccee_MedioPago_Pago { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ccee_MedioPago_Pago_C> ccee_MedioPago_Pago_C { get; set; }
    }
}

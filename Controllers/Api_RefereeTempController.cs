﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_RefereeTempController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_RefereeTemp
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_RefereeTemp/5
        public int Get(int id)
        {
            try
            {

                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return 0;

                int valueReferee = Constant.referee_init_temp;
                var tempReferee = db.ccee_Colegiado.Where(p => p.Col_NoColegiado >= Constant.referee_init_temp).ToList();


                if(tempReferee.Count == 0)
                {
                    return valueReferee;
                }

                valueReferee = tempReferee.Max(p => p.Col_NoColegiado);

                if (valueReferee <= 0)
                {
                    return valueReferee;
                }

                Autorization.saveOperation(db, autorization, "Obteniendo Universidades");

                return valueReferee + 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        // POST: api/Api_RefereeTemp
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api_RefereeTemp/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_RefereeTemp/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

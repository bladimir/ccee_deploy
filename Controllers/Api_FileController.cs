﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class.Class;
using webcee.Class.JsonModels.File;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_FileController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();

        // GET: api/Api_File
        public IEnumerable<ResponseFileJson> Get()
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                var tempFile = from exp in db.ccee_Expediente
                               select new ResponseFileJson()
                               {
                                   id = exp.Exp_NoExpediente,
                                   name = exp.Exp_NombreExpediente
                               };
                Autorization.saveOperation(db, autorization, "Obteniendo expedientes");
                return tempFile;

            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: api/Api_File/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_File
        public IHttpActionResult Post(RequestFileJson file)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Expediente tempFile = new ccee_Expediente()
                {
                    Exp_NombreExpediente = file.name
                };

                db.ccee_Expediente.Add(tempFile);
                Autorization.saveOperation(db, autorization, "Agregando expediente");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Api_File/5
        public IHttpActionResult Put(int id, RequestFileJson file)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Expediente tempFile = db.ccee_Expediente.Find(id);

                if (tempFile == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                tempFile.Exp_NombreExpediente = file.name;

                db.Entry(tempFile).State = EntityState.Modified;
                Autorization.saveOperation(db, autorization, "Actualizando expediente");
                db.SaveChanges();

                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Api_File/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                string autorization = Request.Headers.Authorization.Parameter;
                if (!Autorization.enableAutorization(db, autorization)) return null;

                ccee_Expediente tempFile = db.ccee_Expediente.Find(id);
                if (tempFile == null)
                {
                    return Ok(HttpStatusCode.NotFound);
                }

                db.ccee_Expediente.Remove(tempFile);
                Autorization.saveOperation(db, autorization, "Eliminando expediente");
                db.SaveChanges();
                return Ok(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}

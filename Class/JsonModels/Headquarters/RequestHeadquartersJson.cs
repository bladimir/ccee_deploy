﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Headquarters
{
    public class RequestHeadquartersJson
    {
        public string address { get; set; }
        public string phone { get; set; }
        public string coordinates { get; set; }
        public int idMunicipality { get; set; }
    }
}
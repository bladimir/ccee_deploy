﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels._Tools;
using webcee.Class.JsonModels.Economic;
using webcee.Class.JsonModels.PaymentMethod_Pay;
using webcee.Class.JsonModels.UploadNomina;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_PayNominaController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();
        

        // GET: api/Api_PayNomina
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api_PayNomina/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Api_PayNomina
        //public ResponseUploadNominaPayJson Post(RequestUploadNominaJson nomina)
        //{

        //    try
        //    {
        //        int idPaytimbre = 130;
        //        int idPayDemand = 131;
        //        int idTypeEcoCuadre = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "VarCuadrePago"));
        //        string cent = GeneralFunction.getKeyTableVariable(db, Constant.table_var_aprox_decimal);
        //        decimal dCent = Convert.ToDecimal(cent);
        //        string infoData = "";
        //        using (var transaction = db.Database.BeginTransaction())
        //        {
        //            try
        //            {
        //                string autorization = Request.Headers.Authorization.Parameter;
        //                if (!Autorization.enableAutorization(db, autorization)) return null;
        //                ccee_TipoCargoEconomico typeMora = db.ccee_TipoCargoEconomico
        //                    .Where(o => o.TCE_CondicionCalculo.Equals("esMora")).FirstOrDefault();

        //                List<ResponseEconomicJson> idEcomomics = new List<ResponseEconomicJson>();
        //                List<ResponseEconomicJson> idEcomomicsC = new List<ResponseEconomicJson>();
        //                List<ResponseEconomicJson> idEcomomicsT = new List<ResponseEconomicJson>();
        //                List<ResponseEconomicJson> idEcomomicsO = new List<ResponseEconomicJson>();
        //                List<ResponseEconomicJson> idEcomomicsCTemp;
        //                List<ResponseEconomicJson> idEcomomicsTTemp;
        //                List<ResponseEconomicJson> idEcomomicsOTemp;
        //                List<JsonDataResponse> listDocument = new List<JsonDataResponse>();
        //                List<ResponseUploadNominaJson> errorNomina = new List<ResponseUploadNominaJson>();

        //                ccee_Cajero cahier = db.ccee_Cajero.Find(nomina.idCashier);


        //                string typeDocAccouting = db.ccee_Variables.Where(o => o.Var_Clave.Equals("VarReciboCol")).Select(p => p.Var_Valor).FirstOrDefault();
        //                int typeDocAccoutingInt = Convert.ToInt32(typeDocAccouting);
        //                ccee_TipoDocContable tempDocCon =
        //                    db.ccee_TipoDocContable.Find(typeDocAccoutingInt);
        //                ccee_DocContable tempDoc = new ccee_DocContable()
        //                {
        //                    DCo_ANombre = nomina.NoColegiado.name,
        //                    fk_TipoDocContable = tempDocCon.TDC_NoTipoDocContable
        //                };
        //                db.ccee_DocContable.Add(tempDoc);
        //                db.SaveChanges();


        //                for (int i = 0; i < nomina.data.Count; i++)
        //                {
        //                    Debug.WriteLine("Numero de fila: " + i);
        //                    ResponseUploadNominaJson tempUpload = nomina.data[i];
        //                    bool statusPay = false;
        //                    decimal aomuntTotal = 0;

        //                    idEcomomicsCTemp = new List<ResponseEconomicJson>();
        //                    idEcomomicsTTemp = new List<ResponseEconomicJson>();
        //                    idEcomomicsOTemp = new List<ResponseEconomicJson>();

        //                    ccee_DocContable tempDocCol = new ccee_DocContable();
        //                    if (nomina.idTypeDocument > 0)
        //                    {
        //                        tempDocCol.fk_TipoDocContable = tempDocCon.TDC_NoTipoDocContable;

        //                        if (nomina.idTypeDocument == 1)
        //                        {
        //                            tempDocCol.DCo_ANombre = nomina.NoColegiado.name;
        //                        }
        //                        else if (nomina.idTypeDocument == 2)
        //                        {
        //                            tempDocCol.DCo_ANombre = tempUpload.nombre;
        //                        }
        //                        db.ccee_DocContable.Add(tempDocCol);
        //                        db.SaveChanges();
        //                    }

        //                    ccee_Pago tempPay = new ccee_Pago()
        //                    {
        //                        Pag_Monto = 0,
        //                        Pag_Observacion = "Pago en nomina",
        //                        Pag_Fecha = DateTime.Today,
        //                        fk_Caja = nomina.idCash,
        //                        fk_Cajero = nomina.idCashier
        //                    };
        //                    db.ccee_Pago.Add(tempPay);
        //                    db.SaveChanges();

        //                    ccee_Colegiado referee = db.ccee_Colegiado.Find(tempUpload.colegiado);

        //                    Debug.WriteLine("Colegiado: " + tempUpload.colegiado);
        //                    if (referee == null) {
        //                        aomuntTotal = tempUpload.cuotaTimbre + tempUpload.cuotaColegiado + tempUpload.seguroPostumo;
        //                        ccee_DocContable tempDocCol2 = new ccee_DocContable();
        //                        tempDocCol2.fk_TipoDocContable = tempDocCon.TDC_NoTipoDocContable;
        //                        tempDocCol2.DCo_ANombre = tempUpload.nombre;
        //                        db.ccee_DocContable.Add(tempDocCol2);
        //                        ccee_NoColegiado tempNotReferee = new ccee_NoColegiado()
        //                        {
        //                            NoC_Nombre = tempUpload.nombre,
        //                            NoC_Apellido = "No colegiado por Nomina",
        //                            NoC_Direccion = "Guatemala",
        //                            NoC_Nit = "",
        //                            NoC_NumTelefono = 55555555,
        //                            NoC_Observacion = "No colegiado por Nomina",
        //                            NoC_Afiliado = 0
        //                        };
        //                        db.ccee_NoColegiado.Add(tempNotReferee);
        //                        db.SaveChanges();

        //                        ccee_CargoEconomico_otros tempEcoOther = new ccee_CargoEconomico_otros();
        //                        tempEcoOther.CEcO_Monto = tempUpload.cuotaColegiado + tempUpload.cuotaTimbre + tempUpload.seguroPostumo;
        //                        tempEcoOther.CEcO_FechaGeneracion = Convert.ToDateTime(tempUpload.mes + "/" + 1 + "/" + tempUpload.anio);
        //                        tempEcoOther.fk_DocContable = tempDocCol2.DCo_NoDocContable;
        //                        tempEcoOther.fk_NoColegiado = tempNotReferee.NoC_NoNoColegiado;
        //                        tempEcoOther.fk_TipoCargoEconomico = 157;
        //                        tempEcoOther.fk_Pago = tempPay.Pag_NoPago;
        //                        db.ccee_CargoEconomico_otros.Add(tempEcoOther);
        //                        db.SaveChanges();
        //                        ResponseEconomicJson noRefereeOther = new ResponseEconomicJson()
        //                        {
        //                            id = tempEcoOther.CEcO_NoCargoEconomico,
        //                            amount = tempEcoOther.CEcO_Monto.Value,
        //                            idTypeEconomic = tempEcoOther.fk_TipoCargoEconomico,
        //                            idNoReferee = tempEcoOther.fk_NoColegiado.Value,
        //                            enable = 1,
        //                            description = "Cuota por planilla NO COLEGIADO",
        //                            dateCreated = tempEcoOther.CEcO_FechaGeneracion
        //                        };

        //                        idEcomomicsOTemp.Add(noRefereeOther);

        //                        int idDoc = createDocumentAccunting(nomina.NoColegiado.id, cahier,
        //                            nomina.NoColegiado.name + " " + nomina.NoColegiado.lastname, nomina.NoColegiado.address,
        //                            nomina.NoColegiado.nit, "", tempDocCon, idEcomomicsCTemp, idEcomomicsTTemp, idEcomomicsOTemp,
        //                            nomina.document, tempDocCol2, nomina.idType, nomina.NoDocument);
        //                        infoData += " (Se agrego un no colegiado Doc: " + nomina.document + ")";
        //                        nomina.document = "" + (Convert.ToInt32(nomina.document) + 1);
        //                        listDocument.Add(new JsonDataResponse() {
        //                            url = "/Payment/Print/" + idDoc,
        //                            text = "Recibo " + "A@" + (Convert.ToInt32(nomina.document) - 1)
        //                        });

        //                        ccee_MedioPago_Pago_C tempPaymentMethodPay = new ccee_MedioPago_Pago_C()
        //                        {
        //                            MPP_Monto = aomuntTotal,
        //                            MPP_NoTransaccion = "",
        //                            MPP_NoDocumento = "",
        //                            MPP_Observacion = "Pago de nomina",
        //                            MPP_Reserva = 0,
        //                            MPP_Rechazado = 0,
        //                            MPP_FechaHora = DateTime.Now,
        //                            fk_MedioPago = 44,
        //                            fk_Pago = tempPay.Pag_NoPago,
        //                            fk_Banco = 40,
        //                            MPP_Nomina = 1
        //                        };
        //                        if (nomina.idType == "1")
        //                        {
        //                            tempPaymentMethodPay.fk_Banco = nomina.idBank;
        //                            tempPaymentMethodPay.MPP_NoDocumento = nomina.NoDocument;
        //                            tempPaymentMethodPay.fk_MedioPago = 45;
        //                        }

        //                        db.ccee_MedioPago_Pago_C.Add(tempPaymentMethodPay);

        //                        statusPay = false;
        //                    }

        //                    aomuntTotal = 0;

        //                    if (tempUpload.cuotaColegiado > 0 && referee != null)
        //                    {

        //                        List<ccee_CargoEconomico_colegiado> colegiado = (db.ccee_CargoEconomico_colegiado
        //                                                                    .Where(q => q.fk_Colegiado == tempUpload.colegiado))
        //                                                                    .ToList();

        //                        List<ccee_CargoEconomico_colegiado> colegiadoOnly = (colegiado.Where(p => p.CEcC_FechaGeneracion.Value.Month == tempUpload.mes
        //                                                       && p.CEcC_FechaGeneracion.Value.Year == tempUpload.anio)).ToList();

        //                        if(colegiadoOnly.Count == 0) // cargo econimico colegiado si este no existe
        //                        {

        //                            List < ccee_TipoCargoEconomico > tEconomic = generarMontoColegiado();
        //                            for (int j = 0; j < tEconomic.Count; j++)
        //                            {
        //                                int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
        //                                ResponseEconomicJson temp = economicReferee(tempUpload.mes, tempUpload.anio, tempUpload.colegiado, tempPay.Pag_NoPago,
        //                                    doc, tEconomic[j]);
        //                                aomuntTotal += temp.amount;
        //                                idEcomomicsC.Add(temp);
        //                                idEcomomicsCTemp.Add(temp);
        //                                statusPay = true;
        //                            }

        //                        }else
        //                        {
        //                            foreach (ccee_CargoEconomico_colegiado cEco in colegiadoOnly)
        //                            {
        //                                if (cEco.fk_Pago == null) // Si el pago si existe pero el no se ha pagado
        //                                {
        //                                    int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
        //                                    ResponseEconomicJson temp = economicRefereeUpdate(doc, tempPay.Pag_NoPago, cEco, false, 0, false);
        //                                    aomuntTotal += temp.amount;
        //                                    idEcomomicsC.Add(temp);
        //                                    idEcomomicsCTemp.Add(temp);
        //                                    statusPay = true;
        //                                }
        //                                else
        //                                { // Si se pago pero se necesita cargarlo en algun cargo
        //                                    DateTime datePay;
        //                                    List<ccee_CargoEconomico_colegiado> colegiadoTemp = null;
        //                                    colegiadoTemp = colegiado.Where(p => p.fk_Pago == null && p.fk_TipoCargoEconomico == cEco.fk_TipoCargoEconomico)
        //                                        .OrderBy(o => o.CEcC_FechaGeneracion).ToList();

        //                                    if(colegiadoTemp.Count == 0) // si no existe ningun pago al que se le puede cargar
        //                                    {
        //                                        ccee_TipoCargoEconomico cTCET = db.ccee_TipoCargoEconomico.Find(cEco.fk_TipoCargoEconomico);
        //                                        ccee_CargoEconomico_colegiado cECOT = colegiado.OrderByDescending(p => p.CEcC_FechaGeneracion).FirstOrDefault();
        //                                        datePay = cECOT.CEcC_FechaGeneracion.Value.AddMonths(1);
        //                                        int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
        //                                        ResponseEconomicJson temp = economicReferee(datePay.Month, datePay.Year, tempUpload.colegiado, tempPay.Pag_NoPago,
        //                                            doc, cTCET);
        //                                        aomuntTotal += temp.amount;
        //                                        idEcomomicsC.Add(temp);
        //                                        idEcomomicsCTemp.Add(temp);
        //                                        statusPay = true;
        //                                    }
        //                                    else // Si hay algun pago al que hay que cargrlo
        //                                    {
        //                                        ccee_CargoEconomico_colegiado cECOT = colegiadoTemp.FirstOrDefault();
        //                                        int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
        //                                        ResponseEconomicJson temp = economicRefereeUpdate(doc, tempPay.Pag_NoPago, cECOT, false, 0, false);
        //                                        aomuntTotal += temp.amount;
        //                                        idEcomomicsC.Add(temp);
        //                                        idEcomomicsCTemp.Add(temp);
        //                                        statusPay = true;
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else if (tempUpload.cuotaColegiado <= 0 && nomina.negatives && referee != null) // Si el pago viene negativo colegiado
        //                    {
        //                        List<ccee_CargoEconomico_colegiado> colegiado = (db.ccee_CargoEconomico_colegiado
        //                                                                   .Where(q => q.fk_Colegiado == tempUpload.colegiado))
        //                                                                   .ToList();

        //                        List<ccee_CargoEconomico_colegiado> colegiadoOnly = (colegiado.Where(p => p.CEcC_FechaGeneracion.Value.Month == tempUpload.mes
        //                                                       && p.CEcC_FechaGeneracion.Value.Year == tempUpload.anio)).ToList();

        //                        if(colegiadoOnly.Count != 0) // Si si existe el pago
        //                        {
        //                            List<ccee_TipoCargoEconomico> tEconomic = generarMontoColegiado();
        //                            decimal? tempSum = tEconomic.Sum(p => p.TCE_Valor);
        //                            if(tempSum == (tempUpload.cuotaColegiado * -1)) // Si el monto es igual se anula el pago
        //                            {
        //                                foreach (ccee_CargoEconomico_colegiado cEco in colegiadoOnly)
        //                                {
        //                                    ResponseEconomicJson temp = economicRefereeUpdate(null, tempPay.Pag_NoPago, cEco, false, 0, true);
        //                                    aomuntTotal += temp.amount;
        //                                }
        //                                infoData += " (Se elimino pago Colegiado de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio +  ")";
        //                            }
        //                            else // Si no es igual no se anula el pago
        //                            {
        //                                foreach (var economic in tEconomic)
        //                                {
        //                                    if (economic.TCE_CondicionCalculo == null) continue;
        //                                    if (economic.TCE_CondicionCalculo.Equals("varAdministracion"))
        //                                    {
        //                                        foreach (ccee_CargoEconomico_colegiado cEco in colegiadoOnly)
        //                                        {
        //                                            if(cEco.fk_TipoCargoEconomico == economic.TCE_NoTipoCargoEconomico)
        //                                            {
        //                                                ResponseEconomicJson temp = economicRefereeUpdate(cEco.fk_DocContable, cEco.fk_Pago, cEco, true, tempUpload.cuotaColegiado, false);
        //                                                aomuntTotal += temp.amount;
        //                                                infoData += " (Se sustituyo pago Colegiado de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio + ")";
        //                                            }

        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        else
        //                        {
        //                            infoData += " (No existe pago Colegiado de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio + ")";
        //                        }
        //                    }

        //                    //Listado Global para manejo de una solo llamada a base de datos
        //                    List<ccee_CargoEconomico_timbre> listTimbre = (db.ccee_CargoEconomico_timbre
        //                                                                .Where(q => q.fk_Colegiado == tempUpload.colegiado)).ToList();

        //                    if (tempUpload.cuotaTimbre > 0 && referee != null) // Si hay algun pago de timbre que procesar
        //                    {


        //                        ccee_CargoEconomico_timbre timbre = (listTimbre
        //                                                                .Where(p => p.CEcT_FechaGeneracion.Value.Month == tempUpload.mes
        //                                                                        &&  p.CEcT_FechaGeneracion.Value.Year == tempUpload.anio 
        //                                                                        && p.fk_TipoCargoEconomico == idPaytimbre))
        //                                                                .FirstOrDefault();

        //                        if (timbre == null) // si no existe el pago
        //                        {

        //                            if (tempUpload.cuotaTimbre < 80) //si no existe y es menor a 80
        //                            {
        //                                ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(idPayDemand);
        //                                int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
        //                                ResponseEconomicJson temp = economicOther(tempUpload.cuotaTimbre, tempUpload.mes, tempUpload.anio, 
        //                                    doc, tempUpload.colegiado, tempPay.Pag_NoPago, temType);
        //                                aomuntTotal += temp.amount;
        //                                idEcomomicsO.Add(temp);
        //                                idEcomomicsOTemp.Add(temp);
        //                                statusPay = true;
        //                            }
        //                            else
        //                            {
        //                                ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(idPaytimbre);
        //                                int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
        //                                ResponseEconomicJson temp = economicTimbre(tempUpload.cuotaTimbre, tempUpload.mes, tempUpload.anio, doc, 
        //                                    tempUpload.colegiado, tempPay.Pag_NoPago, nomina.enableUpdate, temType);
        //                                aomuntTotal += temp.amount;
        //                                idEcomomicsT.Add(temp);
        //                                idEcomomicsTTemp.Add(temp);
        //                                statusPay = true;
        //                            }
        //                        }
        //                        else // Si el cargo si existe
        //                        {
        //                            if(timbre.fk_Pago == null) // No se ha pagado el cargo
        //                            {

        //                                if (tempUpload.cuotaTimbre < 80) // El cargo es menor a 80 creamos uno
        //                                {
        //                                    ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(idPayDemand);
        //                                    int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
        //                                    var lastPayT = listTimbre.Where(o=>o.fk_Pago != null).OrderByDescending(p => p.CEcT_FechaGeneracion).FirstOrDefault();

        //                                    ResponseEconomicJson temp = economicOther(tempUpload.cuotaTimbre, lastPayT.CEcT_FechaGeneracion.Value.Month, lastPayT.CEcT_FechaGeneracion.Value.Year,
        //                                        doc, tempUpload.colegiado, tempPay.Pag_NoPago, temType);
        //                                    aomuntTotal += temp.amount;
        //                                    idEcomomicsO.Add(temp);
        //                                    idEcomomicsOTemp.Add(temp);
        //                                    statusPay = true;
        //                                }
        //                                else // se actualiza el pago si es mayor a 80
        //                                {
        //                                    ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(timbre.fk_TipoCargoEconomico);
        //                                    int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
        //                                    ResponseEconomicJson temp = economicTimbreUpdate(tempUpload.cuotaTimbre,tempPay.Pag_NoPago, doc, nomina.enableUpdate, tempUpload.colegiado, temType, timbre, false);
        //                                    aomuntTotal += temp.amount;
        //                                    idEcomomicsT.Add(temp);
        //                                    idEcomomicsTTemp.Add(temp);
        //                                    statusPay = true;
        //                                }
        //                            }else // Si no es nulo se crea otro pago
        //                            {
        //                                ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(idPayDemand);
        //                                int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
        //                                ResponseEconomicJson temp = economicOther(tempUpload.cuotaTimbre, tempUpload.mes, tempUpload.anio,
        //                                    doc, tempUpload.colegiado, tempPay.Pag_NoPago, temType);
        //                                aomuntTotal += temp.amount;
        //                                idEcomomicsO.Add(temp);
        //                                idEcomomicsOTemp.Add(temp);
        //                                statusPay = true;
        //                            }
        //                        }
        //                    }else if(tempUpload.cuotaTimbre <= 0 && nomina.negatives && referee != null) // Si el pago viene negativo timbre
        //                    {
        //                        ccee_CargoEconomico_timbre timbre = (listTimbre
        //                                                                .Where(p => p.CEcT_FechaGeneracion.Value.Month == tempUpload.mes
        //                                                                        && p.CEcT_FechaGeneracion.Value.Year == tempUpload.anio
        //                                                                        && p.fk_TipoCargoEconomico == idPaytimbre))
        //                                                                .FirstOrDefault();
        //                        if (timbre != null)
        //                        {

        //                            if (timbre.CEcT_Monto == (tempUpload.cuotaTimbre * -1)) // Si el pago es igual se anula
        //                            {
        //                                timbre.fk_Pago = null;
        //                                timbre.fk_DocContable = null;
        //                                db.Entry(timbre).State = EntityState.Modified;
        //                                infoData += " (Se elimino pago Timbre de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio + ")";
        //                            }
        //                            else // si el pago es menor o mayor este se modifica
        //                            {
        //                                timbre.CEcT_Monto += tempUpload.cuotaTimbre;
        //                                db.Entry(timbre).State = EntityState.Modified;
        //                                aomuntTotal += timbre.CEcT_Monto.Value;
        //                                infoData += " (Se sustituyo Timbre pago de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio + ")";
        //                            }
        //                        }
        //                        else
        //                        {
        //                            infoData += " (No existe pago Timbre de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio + ")";
        //                        }
        //                    }

        //                    if (tempUpload.seguroPostumo > 0 && referee != null)
        //                    {
        //                        ccee_CargoEconomico_timbre postumo = (listTimbre
        //                                                                .Where(p => p.CEcT_FechaGeneracion.Value.Month == tempUpload.mes
        //                                                                && p.CEcT_FechaGeneracion.Value.Year == tempUpload.anio
        //                                                                && p.fk_TipoCargoEconomico == 5))
        //                                                                .FirstOrDefault();
        //                        if (postumo == null)
        //                        {
        //                            ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(5);
        //                            int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
        //                            ResponseEconomicJson temp = economicTimbre(temType.TCE_Valor.Value, tempUpload.mes, tempUpload.anio, doc,
        //                                tempUpload.colegiado, tempPay.Pag_NoPago, false, temType);
        //                            aomuntTotal += temp.amount;
        //                            idEcomomicsT.Add(temp);
        //                            idEcomomicsTTemp.Add(temp);
        //                            statusPay = true;
        //                        }
        //                        else
        //                        {
        //                            if(postumo.fk_Pago == null) // Si si existe el cargo economico pero no se ha pagado
        //                            {
        //                                ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(postumo.fk_TipoCargoEconomico);
        //                                int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
        //                                ResponseEconomicJson temp = economicTimbreUpdate(tempUpload.seguroPostumo, tempPay.Pag_NoPago, doc, nomina.enableUpdate, tempUpload.colegiado, temType, postumo, false);
        //                                aomuntTotal += temp.amount;
        //                                idEcomomicsT.Add(temp);
        //                                idEcomomicsTTemp.Add(temp);
        //                                statusPay = true;

        //                            }else
        //                            {


        //                                DateTime datePay;
        //                                List<ccee_CargoEconomico_timbre> postumoTemp = null;
        //                                postumoTemp = listTimbre.Where(p => p.fk_Pago == null && p.fk_TipoCargoEconomico == postumo.fk_TipoCargoEconomico)
        //                                    .OrderBy(o => o.CEcT_FechaGeneracion).ToList();

        //                                if (postumoTemp.Count == 0) // si no existe ningun pago al que se le puede cargar
        //                                {
        //                                    ccee_TipoCargoEconomico cTCET = db.ccee_TipoCargoEconomico.Find(postumo.fk_TipoCargoEconomico);
        //                                    ccee_CargoEconomico_timbre cECOT = listTimbre.OrderByDescending(o => o.CEcT_FechaGeneracion).ToList().FirstOrDefault();
        //                                    datePay = cECOT.CEcT_FechaGeneracion.Value.AddMonths(1);
        //                                    int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
        //                                    ResponseEconomicJson temp = economicTimbre(cTCET.TCE_Valor.Value, datePay.Month, datePay.Year, doc,
        //                                                tempUpload.colegiado, tempPay.Pag_NoPago, false, cTCET);
        //                                    aomuntTotal += temp.amount;
        //                                    idEcomomicsT.Add(temp);
        //                                    idEcomomicsTTemp.Add(temp);
        //                                    statusPay = true;
        //                                }
        //                                else // Si hay algun pago al que hay que cargrlo
        //                                {
        //                                    ccee_TipoCargoEconomico cTCET = db.ccee_TipoCargoEconomico.Find(postumo.fk_TipoCargoEconomico);
        //                                    ccee_CargoEconomico_timbre cECOT = postumoTemp.FirstOrDefault();
        //                                    int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
        //                                    ResponseEconomicJson temp = economicTimbreUpdate(tempUpload.seguroPostumo, tempPay.Pag_NoPago, doc, nomina.enableUpdate, tempUpload.colegiado, cTCET, postumo, false);
        //                                    aomuntTotal += temp.amount;
        //                                    idEcomomicsT.Add(temp);
        //                                    idEcomomicsTTemp.Add(temp);
        //                                    statusPay = true;
        //                                }


        //                            }

        //                        }
        //                    }else if(tempUpload.seguroPostumo <= 0 && nomina.negatives && referee != null) // si el pago de postumo no esta pagado
        //                    {
        //                        ccee_CargoEconomico_timbre postumo = (listTimbre
        //                                                                .Where(p => p.CEcT_FechaGeneracion.Value.Month == tempUpload.mes
        //                                                                && p.CEcT_FechaGeneracion.Value.Year == tempUpload.anio
        //                                                                && p.fk_TipoCargoEconomico == 5))
        //                                                                .FirstOrDefault();

        //                        if (postumo != null)
        //                        {

        //                            if (postumo.CEcT_Monto == (tempUpload.seguroPostumo * -1)) // Si el pago es igual se anula
        //                            {
        //                                postumo.fk_Pago = null;
        //                                postumo.fk_DocContable = null;
        //                                db.Entry(postumo).State = EntityState.Modified;
        //                                infoData += " (Se elimino pago Postumo de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio + ")";
        //                            }
        //                            else // si el pago es menor o mayor este se modifica
        //                            {
        //                                postumo.CEcT_Monto += tempUpload.seguroPostumo;
        //                                db.Entry(postumo).State = EntityState.Modified;
        //                                aomuntTotal += postumo.CEcT_Monto.Value;
        //                                infoData += " (Se sustituyo pago Postumo de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio + ")";
        //                            }
        //                        }else
        //                        {
        //                            infoData += " (No existe pago Postumo de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio + ")";
        //                        }
        //                    }

        //                    if (statusPay)
        //                    {
        //                        ccee_MedioPago_Pago_C tempPaymentMethodPay = new ccee_MedioPago_Pago_C()
        //                        {
        //                            MPP_Monto = aomuntTotal,
        //                            MPP_NoTransaccion = "",
        //                            MPP_NoDocumento = "",
        //                            MPP_Observacion = "Pago de nomina",
        //                            MPP_Reserva = 0,
        //                            MPP_Rechazado = 0,
        //                            MPP_FechaHora = DateTime.Now,
        //                            fk_MedioPago = 44,
        //                            fk_Pago = tempPay.Pag_NoPago,
        //                            fk_Banco = 40,
        //                            MPP_Nomina = 1
        //                        };
        //                        if(nomina.idType == "1")
        //                        {
        //                            tempPaymentMethodPay.fk_Banco = nomina.idBank;
        //                            tempPaymentMethodPay.MPP_NoDocumento = nomina.NoDocument;
        //                            tempPaymentMethodPay.fk_MedioPago = 45;
        //                        }
        //                        tempPay.Pag_Monto = aomuntTotal;
        //                        db.ccee_MedioPago_Pago_C.Add(tempPaymentMethodPay);
        //                        Debug.WriteLine("Monto Total: " + aomuntTotal);
        //                        Debug.WriteLine("Numero de pago: " + tempPay.Pag_NoPago);
        //                        db.SaveChanges();
        //                    }else
        //                    {
        //                        errorNomina.Add(tempUpload);
        //                        db.ccee_Pago.Remove(tempPay);
        //                        db.SaveChanges();
        //                    }


        //                    //Generador de recibo por colegiado o no colegiado
        //                    if (nomina.idTypeDocument == 1 && referee != null && statusPay)
        //                    {
        //                        int idDoc = createDocumentAccunting(nomina.NoColegiado.id, cahier,
        //                            nomina.NoColegiado.name + " " + nomina.NoColegiado.lastname, nomina.NoColegiado.address,
        //                            nomina.NoColegiado.nit, "", tempDocCon, idEcomomicsCTemp, idEcomomicsTTemp, idEcomomicsOTemp, 
        //                            nomina.document, tempDocCol, nomina.idType, nomina.NoDocument);
        //                        nomina.document = "" + (Convert.ToInt32(nomina.document) + 1);
        //                        listDocument.Add(new JsonDataResponse()
        //                        {
        //                            url = "/Payment/Print/" + idDoc,
        //                            text = "Recibo " + "A@" + (Convert.ToInt32(nomina.document) - 1)
        //                        });

        //                    }
        //                    else if(nomina.idTypeDocument == 2 && referee != null && statusPay)
        //                    {
        //                        //ccee_Colegiado temCol = db.ccee_Colegiado.Find(tempUpload.colegiado);

        //                        int idDoc = createDocumentAccunting(referee.Col_NoColegiado, cahier,
        //                            referee.Col_PrimerNombre + " " + referee.Col_SegundoNombre + " " + referee.Col_PrimerApellido + " " + referee.Col_SegundoApellido,
        //                            "Guatemala",
        //                            referee.Col_Nit, "", tempDocCon, idEcomomicsCTemp, idEcomomicsTTemp, idEcomomicsOTemp,
        //                            nomina.document, tempDocCol, nomina.idType, nomina.NoDocument);
        //                        nomina.document = "" + (Convert.ToInt32(nomina.document) + 1);
        //                        listDocument.Add(new JsonDataResponse()
        //                        {
        //                            url = "/Payment/Print/" + idDoc,
        //                            text = "Recibo " + "A@" + (Convert.ToInt32(nomina.document) - 1)
        //                        });
        //                    }
        //                    db.SaveChanges();

        //                }

        //                idEcomomics.AddRange(idEcomomicsC);
        //                idEcomomics.AddRange(idEcomomicsT);
        //                idEcomomics.AddRange(idEcomomicsO);

        //                if (idEcomomics.Count > 0)
        //                {

        //                    var list = (from val in idEcomomics
        //                               where val.idTypeEconomic == idPaytimbre || val.idTypeEconomic == idPayDemand
        //                                select val).ToList();
        //                    var listG = (from val in idEcomomics
        //                                 where val.idTypeEconomic != idPaytimbre && val.idTypeEconomic != idPayDemand
        //                                 select val).ToList();

        //                    decimal totalGeneral = listG.Sum(p => p.amount);
        //                    decimal total = list.Sum(p => p.amount);
        //                    decimal totalAprox = GeneralFunction.aproxDecimal(total, dCent);
        //                    decimal residuo = totalAprox - total;
        //                    if (residuo > 0)
        //                    {
        //                        ccee_CargoEconomico_timbre tempEcoTimbre = new ccee_CargoEconomico_timbre();
        //                        tempEcoTimbre.CEcT_Monto = residuo;
        //                        tempEcoTimbre.CEcT_FechaGeneracion = DateTime.Now;
        //                        tempEcoTimbre.fk_NoColegiado = nomina.NoColegiado.id;
        //                        tempEcoTimbre.fk_TipoCargoEconomico = idTypeEcoCuadre;
        //                        db.ccee_CargoEconomico_timbre.Add(tempEcoTimbre);
        //                        db.SaveChanges();
        //                        ResponseEconomicJson respO = new ResponseEconomicJson()
        //                        {
        //                            id = tempEcoTimbre.CEcT_NoCargoEconomico,
        //                            amount = tempEcoTimbre.CEcT_Monto.Value,
        //                            idTypeEconomic = 9997,
        //                            enable = 1,
        //                            description = "Cuota mora no pagada"
        //                        };
        //                        idEcomomics.Add(respO);
        //                        idEcomomicsO.Add(respO);

        //                        tempEcoTimbre = new ccee_CargoEconomico_timbre();
        //                        tempEcoTimbre.CEcT_Monto = residuo * -1;
        //                        tempEcoTimbre.CEcT_FechaGeneracion = DateTime.Now;
        //                        tempEcoTimbre.fk_NoColegiado = nomina.NoColegiado.id;
        //                        tempEcoTimbre.fk_TipoCargoEconomico = idTypeEcoCuadre;
        //                        db.ccee_CargoEconomico_timbre.Add(tempEcoTimbre);
        //                        db.SaveChanges();
        //                        respO = new ResponseEconomicJson()
        //                        {
        //                            id = tempEcoTimbre.CEcT_NoCargoEconomico,
        //                            amount = tempEcoTimbre.CEcT_Monto.Value,
        //                            idTypeEconomic = 9998,
        //                            enable = 1,
        //                            description = "Cuota mora no pagada"
        //                        };
        //                        idEcomomics.Add(respO);
        //                        idEcomomicsO.Add(respO);
        //                    }


        //                    int idDoc = createDocumentAccunting(nomina.NoColegiado.id, cahier,
        //                        nomina.NoColegiado.name + " " + nomina.NoColegiado.lastname, nomina.NoColegiado.address,
        //                        nomina.NoColegiado.nit, "", tempDocCon, idEcomomicsC, idEcomomicsT, idEcomomicsO, nomina.document, tempDoc,
        //                        nomina.idType, nomina.NoDocument);


        //                    var listTimbres = (from tim in db.ccee_Timbre
        //                                                     join hoj in db.ccee_HojaTimbre on tim.fk_HojaTimbre equals hoj.HoT_NoHojaTimbre
        //                                                     join val in db.ccee_ValorTimbre on tim.fk_ValorTimbre equals val.VaT_NoValorTimbre
        //                                                     where tim.Tim_Estatus == Constant.timbre_status_enable &&
        //                                                     hoj.fk_Cajero == nomina.idCashier
        //                                                     select new {
        //                                                         valor = val.VaT_Denominacion,
        //                                                         hoja = hoj.HoT_NoHojaTimbre,
        //                                                         timbre = tim.Tim_NumeroTimbre,
        //                                                         id = tim.Tim_NoTimbre
        //                                                     }).ToList();

        //                    //listDocument.Add("/Payment/Print/" + idDoc);

        //                    listTimbres = listTimbres.OrderByDescending(p => p.valor)
        //                                                .ThenBy(o=>o.id).ToList();
        //                    string timbresToPay = "";
        //                    decimal calculationTimbre = totalAprox;
        //                    for (int i = 0; i < listTimbres.Count; i++)
        //                    {
        //                        if (calculationTimbre == 0) break;
        //                        if(calculationTimbre >= listTimbres[i].valor)
        //                        {
        //                            timbresToPay += "H) " + listTimbres[i].hoja + " - T) " + listTimbres[i].timbre + " , ";
        //                            calculationTimbre = calculationTimbre - listTimbres[i].valor.Value;
        //                            ccee_Timbre timbre = db.ccee_Timbre.Find(listTimbres[i].id);
        //                            timbre.Tim_Estatus = Constant.timbre_status_sold;
        //                            db.Entry(timbre).State = EntityState.Modified;
        //                        }
        //                    }

        //                    if(calculationTimbre > 0)
        //                    {
        //                        timbresToPay = "Timbres insuficientes";
        //                        transaction.Rollback();
        //                        return new ResponseUploadNominaPayJson() {
        //                            status = 400,
        //                            textError = "Timbres insuficientes"
        //                        };
        //                    }

        //                    ResponseEconomicJson info = new ResponseEconomicJson()
        //                    {
        //                        id = 0,
        //                        amount = 0,
        //                        idTypeEconomic = 0,
        //                        idNoReferee = 0,
        //                        enable = 1,
        //                        description = infoData,
        //                        dateCreated = null
        //                    };
        //                    idEcomomics.Add(info);

        //                    ccee_Batch tempBatch = new ccee_Batch();
        //                    tempBatch.fk_DocContable = idDoc;
        //                    tempBatch.fk_Cajero = nomina.idCashier;
        //                    tempBatch.Bat_ContenidoEntrada = getTextInit(nomina.data);
        //                    tempBatch.Bar_ContenidoSalida = getTextFinal(errorNomina, idEcomomics, timbresToPay);
        //                    db.ccee_Batch.Add(tempBatch);

        //                    ResponseUploadNominaPayJson request = new ResponseUploadNominaPayJson();
        //                    request.status = 200;
        //                    request.timbres = timbresToPay;
        //                    request.total = totalAprox + totalGeneral;
        //                    request.extra = residuo;
        //                    request.recibo = "/Payment/Print/" + idDoc;
        //                    request.listDoc = listDocument;
        //                    request.textError = infoData;


        //                    db.SaveChanges();
        //                    Autorization.saveOperation(db, autorization, "Agregando pago nomina");
        //                    //transaction.Commit();
        //                    transaction.Rollback();
        //                    return request;
        //                }else
        //                {
        //                    transaction.Rollback();
        //                    Debug.WriteLine("ningun campo ingresado o :: " + infoData + "  ::");
        //                    return new ResponseUploadNominaPayJson()
        //                    {
        //                        status = 400,
        //                        textError = "ningun campo ingresado o :: " + infoData + "  ::"
        //                    };
        //                }

        //            }
        //            catch (Exception e)
        //            {
        //                Debug.WriteLine(e);
        //                transaction.Rollback();
        //                Debug.WriteLine("Problemas generales, posiblemente tiene un error grabe en el listado por favor revisar");
        //                return new ResponseUploadNominaPayJson()
        //                {
        //                    status = 400,
        //                    textError = "Problemas generales, posiblemente tiene un error grabe en el listado por favor revisar"
        //                };
        //            }
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        Debug.WriteLine(e);
        //        return new ResponseUploadNominaPayJson()
        //        {
        //            status = 400,
        //            textError = "Problemas generasles"
        //        };
        //    }

        //}

        /*public ResponseUploadNominaPayJson Post(RequestUploadNominaJson nomina)
        {
            
            try
            {
                int idPaytimbre = 130;
                int idPayDemand = 131;
                int idTypeEcoCuadre = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "VarCuadrePago"));
                string cent = GeneralFunction.getKeyTableVariable(db, Constant.table_var_aprox_decimal);
                decimal dCent = Convert.ToDecimal(cent);
                string infoData = "";
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        string autorization = Request.Headers.Authorization.Parameter;
                        if (!Autorization.enableAutorization(db, autorization)) return null;
                        ccee_TipoCargoEconomico typeMora = db.ccee_TipoCargoEconomico
                            .Where(o => o.TCE_CondicionCalculo.Equals("esMora")).FirstOrDefault();

                        List<ResponseEconomicJson> idEcomomics = new List<ResponseEconomicJson>();
                        List<ResponseEconomicJson> idEcomomicsC = new List<ResponseEconomicJson>();
                        List<ResponseEconomicJson> idEcomomicsT = new List<ResponseEconomicJson>();
                        List<ResponseEconomicJson> idEcomomicsO = new List<ResponseEconomicJson>();
                        List<ResponseEconomicJson> idEcomomicsCTemp;
                        List<ResponseEconomicJson> idEcomomicsTTemp;
                        List<ResponseEconomicJson> idEcomomicsOTemp;
                        List<JsonDataResponse> listDocument = new List<JsonDataResponse>();
                        List<ResponseUploadNominaJson> errorNomina = new List<ResponseUploadNominaJson>();

                        ccee_Cajero cahier = db.ccee_Cajero.Find(nomina.idCashier);


                        string typeDocAccouting = db.ccee_Variables.Where(o => o.Var_Clave.Equals("VarReciboCol")).Select(p => p.Var_Valor).FirstOrDefault();
                        int typeDocAccoutingInt = Convert.ToInt32(typeDocAccouting);
                        ccee_TipoDocContable tempDocCon =
                            db.ccee_TipoDocContable.Find(typeDocAccoutingInt);

                        ccee_DocContable tempDoc = db.ccee_DocContable.Find(nomina.idDocAccount);
                        tempDoc.DCo_ANombre = nomina.NoColegiado.name;
                        tempDoc.fk_TipoDocContable = tempDocCon.TDC_NoTipoDocContable;
                        db.Entry(tempDoc).State = EntityState.Modified;
                        
                        db.SaveChanges();

                        DateTime tempDateOfPay = ((nomina.dateOfPay == null) ? 
                            DateTime.Today : nomina.dateOfPay.Value.Date);


                        for (int i = 0; i < nomina.data.Count; i++)
                        {
                            Debug.WriteLine("Numero de fila: " + i);
                            ResponseUploadNominaJson tempUpload = nomina.data[i];
                            bool statusPay = false;
                            decimal aomuntTotal = 0;

                            idEcomomicsCTemp = new List<ResponseEconomicJson>();
                            idEcomomicsTTemp = new List<ResponseEconomicJson>();
                            idEcomomicsOTemp = new List<ResponseEconomicJson>();

                            ccee_DocContable tempDocCol = new ccee_DocContable();
                            if (nomina.idTypeDocument > 0)
                            {
                                tempDocCol.fk_TipoDocContable = tempDocCon.TDC_NoTipoDocContable;

                                if (nomina.idTypeDocument == 1)
                                {
                                    tempDocCol.DCo_ANombre = nomina.NoColegiado.name;
                                }
                                else if (nomina.idTypeDocument == 2)
                                {
                                    tempDocCol.DCo_ANombre = tempUpload.nombre;
                                }
                                db.ccee_DocContable.Add(tempDocCol);
                                db.SaveChanges();
                            }

                            ccee_Pago tempPay = new ccee_Pago()
                            {
                                Pag_Monto = 0,
                                Pag_Observacion = "Pago en nomina",
                                Pag_Fecha = tempDateOfPay,
                                fk_Caja = nomina.idCash,
                                fk_Cajero = nomina.idCashier
                            };
                            db.ccee_Pago.Add(tempPay);
                            db.SaveChanges();

                            ccee_Colegiado referee = db.ccee_Colegiado.Find(tempUpload.colegiado);

                            Debug.WriteLine("Colegiado: " + tempUpload.colegiado);
                            if (referee == null)
                            {
                                aomuntTotal = tempUpload.cuotaTimbre + tempUpload.cuotaColegiado + tempUpload.seguroPostumo;
                                ccee_DocContable tempDocCol2 = new ccee_DocContable();
                                tempDocCol2.fk_TipoDocContable = tempDocCon.TDC_NoTipoDocContable;
                                tempDocCol2.DCo_ANombre = tempUpload.nombre;
                                db.ccee_DocContable.Add(tempDocCol2);
                                ccee_NoColegiado tempNotReferee = new ccee_NoColegiado()
                                {
                                    NoC_Nombre = tempUpload.nombre,
                                    NoC_Apellido = "No colegiado por Nomina",
                                    NoC_Direccion = "Guatemala",
                                    NoC_Nit = "",
                                    NoC_NumTelefono = 55555555,
                                    NoC_Observacion = "No colegiado por Nomina",
                                    NoC_Afiliado = 0
                                };
                                db.ccee_NoColegiado.Add(tempNotReferee);
                                db.SaveChanges();

                                ccee_CargoEconomico_otros tempEcoOther = new ccee_CargoEconomico_otros();
                                tempEcoOther.CEcO_Monto = tempUpload.cuotaColegiado + tempUpload.cuotaTimbre + tempUpload.seguroPostumo;
                                tempEcoOther.CEcO_FechaGeneracion = Convert.ToDateTime(tempUpload.mes + "/" + 1 + "/" + tempUpload.anio);
                                tempEcoOther.fk_DocContable = tempDocCol2.DCo_NoDocContable;
                                tempEcoOther.fk_NoColegiado = tempNotReferee.NoC_NoNoColegiado;
                                tempEcoOther.fk_TipoCargoEconomico = 157;
                                tempEcoOther.fk_Pago = tempPay.Pag_NoPago;
                                tempEcoOther.fk_Batch = nomina.idBatch;
                                db.ccee_CargoEconomico_otros.Add(tempEcoOther);
                                db.SaveChanges();
                                ResponseEconomicJson noRefereeOther = new ResponseEconomicJson()
                                {
                                    id = tempEcoOther.CEcO_NoCargoEconomico,
                                    amount = tempEcoOther.CEcO_Monto.Value,
                                    idTypeEconomic = tempEcoOther.fk_TipoCargoEconomico,
                                    idNoReferee = tempEcoOther.fk_NoColegiado.Value,
                                    enable = 1,
                                    description = "Cuota por planilla NO COLEGIADO",
                                    dateCreated = tempEcoOther.CEcO_FechaGeneracion
                                };

                                idEcomomicsOTemp.Add(noRefereeOther);

                                int idDoc = createDocumentAccunting(nomina.NoColegiado.id, cahier,
                                    nomina.NoColegiado.name + " " + nomina.NoColegiado.lastname, nomina.NoColegiado.address,
                                    nomina.NoColegiado.nit, "", tempDocCon, idEcomomicsCTemp, idEcomomicsTTemp, idEcomomicsOTemp,
                                    nomina.document, tempDocCol2, nomina.idType, nomina.NoDocument, nomina.textOfEconomic, tempDateOfPay, nomina.jsonContentMethod);
                                infoData += " (Se agrego un no colegiado Doc: " + nomina.document + ")";
                                nomina.document = "" + (Convert.ToInt32(nomina.document) + 1);
                                listDocument.Add(new JsonDataResponse()
                                {
                                    url = "/Payment/Print/" + idDoc,
                                    text = "Recibo " + "A@" + (Convert.ToInt32(nomina.document) - 1)
                                });

                                decimal tempPaymentMethodAmount = aomuntTotal;
                                ccee_MedioPago_Pago_C tempPaymentMethodPay;
                                foreach (RequestPaymentMethod_PayJson paymentMethod in nomina.jsonContentMethod)
                                {
                                    tempPaymentMethodPay = new ccee_MedioPago_Pago_C()
                                    {
                                        MPP_Monto = tempPaymentMethodAmount,
                                        MPP_NoTransaccion = paymentMethod.noTransaction,
                                        MPP_NoDocumento = paymentMethod.noDocument,
                                        MPP_Observacion = paymentMethod.remarke,
                                        MPP_Reserva = paymentMethod.reservation,
                                        MPP_Rechazado = paymentMethod.rejection,
                                        MPP_FechaHora = DateTime.Now,
                                        MPP_EsNomina = 1,
                                        fk_MedioPago = paymentMethod.idPaymentMethod,
                                        fk_Pago = tempPay.Pag_NoPago,
                                        fk_Banco = paymentMethod.idBank
                                    };
                                    db.ccee_MedioPago_Pago_C.Add(tempPaymentMethodPay);
                                    tempPaymentMethodPay = null;
                                    tempPaymentMethodAmount = 0m;
                                }

                                //ccee_MedioPago_Pago_C tempPaymentMethodPay = new ccee_MedioPago_Pago_C()
                                //{
                                //    MPP_Monto = aomuntTotal,
                                //    MPP_NoTransaccion = "",
                                //    MPP_NoDocumento = "",
                                //    MPP_Observacion = "Pago de nomina",
                                //    MPP_Reserva = 0,
                                //    MPP_Rechazado = 0,
                                //    MPP_FechaHora = DateTime.Now,
                                //    fk_MedioPago = 44,
                                //    fk_Pago = tempPay.Pag_NoPago,
                                //    fk_Banco = 40,
                                //    MPP_Nomina = 1
                                //};
                                //if (nomina.idType == "1")
                                //{
                                //    tempPaymentMethodPay.fk_Banco = nomina.idBank;
                                //    tempPaymentMethodPay.MPP_NoDocumento = nomina.NoDocument;
                                //    tempPaymentMethodPay.fk_MedioPago = 45;
                                //}

                                //db.ccee_MedioPago_Pago_C.Add(tempPaymentMethodPay);

                                statusPay = false;
                            }

                            aomuntTotal = 0;

                            if (tempUpload.cuotaColegiado > 0 && referee != null)
                            {

                                List<ccee_CargoEconomico_colegiado> colegiado = (db.ccee_CargoEconomico_colegiado
                                                                            .Where(q => q.fk_Colegiado == tempUpload.colegiado))
                                                                            .ToList();

                                List<ccee_CargoEconomico_colegiado> colegiadoOnly = (colegiado.Where(p => p.CEcC_FechaGeneracion.Value.Month == tempUpload.mes
                                                               && p.CEcC_FechaGeneracion.Value.Year == tempUpload.anio)).ToList();

                                if (colegiadoOnly.Count == 0) // cargo econimico colegiado si este no existe
                                {

                                    List<ccee_TipoCargoEconomico> tEconomic = generarMontoColegiado();
                                    for (int j = 0; j < tEconomic.Count; j++)
                                    {
                                        int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                        ResponseEconomicJson temp = economicReferee(tempUpload.mes, tempUpload.anio, tempUpload.colegiado, tempPay.Pag_NoPago,
                                            doc, tEconomic[j], nomina.idBatch);
                                        aomuntTotal += temp.amount;
                                        idEcomomicsC.Add(temp);
                                        idEcomomicsCTemp.Add(temp);
                                        statusPay = true;
                                    }

                                }
                                else
                                {
                                    foreach (ccee_CargoEconomico_colegiado cEco in colegiadoOnly)
                                    {
                                        if (cEco.fk_Pago == null) // Si el pago si existe pero el no se ha pagado
                                        {
                                            int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                            ResponseEconomicJson temp = economicRefereeUpdate(doc, tempPay.Pag_NoPago, cEco, false, 0, false, nomina.idBatch);
                                            aomuntTotal += temp.amount;
                                            idEcomomicsC.Add(temp);
                                            idEcomomicsCTemp.Add(temp);
                                            statusPay = true;
                                        }
                                        else
                                        { // Si se pago pero se necesita cargarlo en algun cargo
                                            DateTime datePay;
                                            List<ccee_CargoEconomico_colegiado> colegiadoTemp = null;
                                            colegiadoTemp = colegiado.Where(p => p.fk_Pago == null && p.fk_TipoCargoEconomico == cEco.fk_TipoCargoEconomico)
                                                .OrderBy(o => o.CEcC_FechaGeneracion).ToList();

                                            if (colegiadoTemp.Count == 0) // si no existe ningun pago al que se le puede cargar
                                            {
                                                ccee_TipoCargoEconomico cTCET = db.ccee_TipoCargoEconomico.Find(cEco.fk_TipoCargoEconomico);
                                                ccee_CargoEconomico_colegiado cECOT = colegiado.OrderByDescending(p => p.CEcC_FechaGeneracion).FirstOrDefault();
                                                datePay = cECOT.CEcC_FechaGeneracion.Value.AddMonths(1);
                                                int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                                ResponseEconomicJson temp = economicReferee(datePay.Month, datePay.Year, tempUpload.colegiado, tempPay.Pag_NoPago,
                                                    doc, cTCET, nomina.idBatch);
                                                aomuntTotal += temp.amount;
                                                idEcomomicsC.Add(temp);
                                                idEcomomicsCTemp.Add(temp);
                                                statusPay = true;
                                            }
                                            else // Si hay algun pago al que hay que cargrlo
                                            {
                                                ccee_CargoEconomico_colegiado cECOT = colegiadoTemp.FirstOrDefault();
                                                int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                                ResponseEconomicJson temp = economicRefereeUpdate(doc, tempPay.Pag_NoPago, cECOT, false, 0, false, nomina.idBatch);
                                                aomuntTotal += temp.amount;
                                                idEcomomicsC.Add(temp);
                                                idEcomomicsCTemp.Add(temp);
                                                statusPay = true;
                                            }
                                        }
                                    }
                                }
                            }
                            else if (tempUpload.cuotaColegiado <= 0 && nomina.negatives && referee != null) // Si el pago viene negativo colegiado
                            {
                                List<ccee_CargoEconomico_colegiado> colegiado = (db.ccee_CargoEconomico_colegiado
                                                                           .Where(q => q.fk_Colegiado == tempUpload.colegiado))
                                                                           .ToList();

                                List<ccee_CargoEconomico_colegiado> colegiadoOnly = (colegiado.Where(p => p.CEcC_FechaGeneracion.Value.Month == tempUpload.mes
                                                               && p.CEcC_FechaGeneracion.Value.Year == tempUpload.anio)).ToList();

                                if (colegiadoOnly.Count != 0) // Si si existe el pago
                                {
                                    List<ccee_TipoCargoEconomico> tEconomic = generarMontoColegiado();
                                    decimal? tempSum = tEconomic.Sum(p => p.TCE_Valor);
                                    if (tempSum == (tempUpload.cuotaColegiado * -1)) // Si el monto es igual se anula el pago
                                    {
                                        foreach (ccee_CargoEconomico_colegiado cEco in colegiadoOnly)
                                        {
                                            ResponseEconomicJson temp = economicRefereeUpdate(null, tempPay.Pag_NoPago, cEco, false, 0, true, null);
                                            aomuntTotal += temp.amount;
                                        }
                                        infoData += " (Se elimino pago Colegiado de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio + ")";
                                    }
                                    else // Si no es igual no se anula el pago
                                    {
                                        foreach (var economic in tEconomic)
                                        {
                                            if (economic.TCE_CondicionCalculo == null) continue;
                                            if (economic.TCE_CondicionCalculo.Equals("varAdministracion"))
                                            {
                                                foreach (ccee_CargoEconomico_colegiado cEco in colegiadoOnly)
                                                {
                                                    if (cEco.fk_TipoCargoEconomico == economic.TCE_NoTipoCargoEconomico)
                                                    {
                                                        ResponseEconomicJson temp = economicRefereeUpdate(cEco.fk_DocContable, cEco.fk_Pago, cEco, true, tempUpload.cuotaColegiado, false, nomina.idBatch);
                                                        aomuntTotal += temp.amount;
                                                        infoData += " (Se sustituyo pago Colegiado de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio + ")";
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    infoData += " (No existe pago Colegiado de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio + ")";
                                }
                            }

                            //Listado Global para manejo de una solo llamada a base de datos
                            List<ccee_CargoEconomico_timbre> listTimbre = (db.ccee_CargoEconomico_timbre
                                                                        .Where(q => q.fk_Colegiado == tempUpload.colegiado)).ToList();

                            if (tempUpload.cuotaTimbre > 0 && referee != null) // Si hay algun pago de timbre que procesar
                            {


                                ccee_CargoEconomico_timbre timbre = (listTimbre
                                                                        .Where(p => p.CEcT_FechaGeneracion.Value.Month == tempUpload.mes
                                                                                && p.CEcT_FechaGeneracion.Value.Year == tempUpload.anio
                                                                                && p.fk_TipoCargoEconomico == idPaytimbre))
                                                                        .FirstOrDefault();

                                if (timbre == null) // si no existe el pago
                                {

                                    if (tempUpload.cuotaTimbre < 80) //si no existe y es menor a 80
                                    {
                                        ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(idPayDemand);
                                        int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                        ResponseEconomicJson temp = economicOther(tempUpload.cuotaTimbre, tempUpload.mes, tempUpload.anio,
                                            doc, tempUpload.colegiado, tempPay.Pag_NoPago, temType, nomina.idBatch);
                                        aomuntTotal += temp.amount;
                                        idEcomomicsO.Add(temp);
                                        idEcomomicsOTemp.Add(temp);
                                        statusPay = true;
                                    }
                                    else
                                    {
                                        ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(idPaytimbre);
                                        int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                        ResponseEconomicJson temp = economicTimbre(tempUpload.cuotaTimbre, tempUpload.mes, tempUpload.anio, doc,
                                            tempUpload.colegiado, tempPay.Pag_NoPago, nomina.enableUpdate, temType, nomina.idBatch);
                                        aomuntTotal += temp.amount;
                                        idEcomomicsT.Add(temp);
                                        idEcomomicsTTemp.Add(temp);
                                        statusPay = true;
                                    }
                                }
                                else // Si el cargo si existe
                                {
                                    if (timbre.fk_Pago == null) // No se ha pagado el cargo
                                    {

                                        if (tempUpload.cuotaTimbre < 80) // El cargo es menor a 80 creamos uno
                                        {
                                            ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(idPayDemand);
                                            int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                            var lastPayT = listTimbre.Where(o => o.fk_Pago != null).OrderByDescending(p => p.CEcT_FechaGeneracion).FirstOrDefault();

                                            ResponseEconomicJson temp = economicOther(tempUpload.cuotaTimbre, lastPayT.CEcT_FechaGeneracion.Value.Month, lastPayT.CEcT_FechaGeneracion.Value.Year,
                                                doc, tempUpload.colegiado, tempPay.Pag_NoPago, temType, nomina.idBatch);
                                            aomuntTotal += temp.amount;
                                            idEcomomicsO.Add(temp);
                                            idEcomomicsOTemp.Add(temp);
                                            statusPay = true;
                                        }
                                        else // se actualiza el pago si es mayor a 80
                                        {
                                            ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(timbre.fk_TipoCargoEconomico);
                                            int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                            ResponseEconomicJson temp = economicTimbreUpdate(tempUpload.cuotaTimbre, tempPay.Pag_NoPago, doc, nomina.enableUpdate, 
                                                tempUpload.colegiado, temType, timbre, false, nomina.idBatch);
                                            aomuntTotal += temp.amount;
                                            idEcomomicsT.Add(temp);
                                            idEcomomicsTTemp.Add(temp);
                                            statusPay = true;
                                        }
                                    }
                                    else // Si no es nulo se crea otro pago
                                    {
                                        ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(idPayDemand);
                                        int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                        ResponseEconomicJson temp = economicOther(tempUpload.cuotaTimbre, tempUpload.mes, tempUpload.anio,
                                            doc, tempUpload.colegiado, tempPay.Pag_NoPago, temType, nomina.idBatch);
                                        aomuntTotal += temp.amount;
                                        idEcomomicsO.Add(temp);
                                        idEcomomicsOTemp.Add(temp);
                                        statusPay = true;
                                    }
                                }
                            }
                            else if (tempUpload.cuotaTimbre <= 0 && nomina.negatives && referee != null) // Si el pago viene negativo timbre
                            {
                                ccee_CargoEconomico_timbre timbre = (listTimbre
                                                                        .Where(p => p.CEcT_FechaGeneracion.Value.Month == tempUpload.mes
                                                                                && p.CEcT_FechaGeneracion.Value.Year == tempUpload.anio
                                                                                && p.fk_TipoCargoEconomico == idPaytimbre))
                                                                        .FirstOrDefault();
                                if (timbre != null)
                                {

                                    if (timbre.CEcT_Monto == (tempUpload.cuotaTimbre * -1)) // Si el pago es igual se anula
                                    {
                                        timbre.fk_Pago = null;
                                        timbre.fk_DocContable = null;
                                        timbre.fk_Batch = null;
                                        db.Entry(timbre).State = EntityState.Modified;
                                        infoData += " (Se elimino pago Timbre de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio + ")";
                                    }
                                    else // si el pago es menor o mayor este se modifica
                                    {
                                        timbre.CEcT_Monto += tempUpload.cuotaTimbre;
                                        timbre.fk_Batch = nomina.idBatch;
                                        db.Entry(timbre).State = EntityState.Modified;
                                        aomuntTotal += timbre.CEcT_Monto.Value;
                                        infoData += " (Se sustituyo Timbre pago de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio + ")";
                                    }
                                }
                                else
                                {
                                    infoData += " (No existe pago Timbre de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio + ")";
                                }
                            }

                            if (tempUpload.seguroPostumo > 0 && referee != null)
                            {
                                ccee_CargoEconomico_timbre postumo = (listTimbre
                                                                        .Where(p => p.CEcT_FechaGeneracion.Value.Month == tempUpload.mes
                                                                        && p.CEcT_FechaGeneracion.Value.Year == tempUpload.anio
                                                                        && p.fk_TipoCargoEconomico == 5))
                                                                        .FirstOrDefault();
                                if (postumo == null)
                                {
                                    ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(5);
                                    int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                    ResponseEconomicJson temp = economicTimbre(temType.TCE_Valor.Value, tempUpload.mes, tempUpload.anio, doc,
                                        tempUpload.colegiado, tempPay.Pag_NoPago, false, temType, nomina.idBatch);
                                    aomuntTotal += temp.amount;
                                    idEcomomicsT.Add(temp);
                                    idEcomomicsTTemp.Add(temp);
                                    statusPay = true;
                                }
                                else
                                {
                                    if (postumo.fk_Pago == null) // Si si existe el cargo economico pero no se ha pagado
                                    {
                                        ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(postumo.fk_TipoCargoEconomico);
                                        int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                        ResponseEconomicJson temp = economicTimbreUpdate(tempUpload.seguroPostumo, tempPay.Pag_NoPago, doc, nomina.enableUpdate, 
                                            tempUpload.colegiado, temType, postumo, false, nomina.idBatch);
                                        aomuntTotal += temp.amount;
                                        idEcomomicsT.Add(temp);
                                        idEcomomicsTTemp.Add(temp);
                                        statusPay = true;

                                    }
                                    else
                                    {


                                        DateTime datePay;
                                        List<ccee_CargoEconomico_timbre> postumoTemp = null;
                                        postumoTemp = listTimbre.Where(p => p.fk_Pago == null && p.fk_TipoCargoEconomico == postumo.fk_TipoCargoEconomico)
                                            .OrderBy(o => o.CEcT_FechaGeneracion).ToList();

                                        if (postumoTemp.Count == 0) // si no existe ningun pago al que se le puede cargar
                                        {
                                            ccee_TipoCargoEconomico cTCET = db.ccee_TipoCargoEconomico.Find(postumo.fk_TipoCargoEconomico);
                                            ccee_CargoEconomico_timbre cECOT = listTimbre.OrderByDescending(o => o.CEcT_FechaGeneracion).ToList().FirstOrDefault();
                                            datePay = cECOT.CEcT_FechaGeneracion.Value.AddMonths(1);
                                            int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                            ResponseEconomicJson temp = economicTimbre(cTCET.TCE_Valor.Value, datePay.Month, datePay.Year, doc,
                                                        tempUpload.colegiado, tempPay.Pag_NoPago, false, cTCET, nomina.idBatch);
                                            aomuntTotal += temp.amount;
                                            idEcomomicsT.Add(temp);
                                            idEcomomicsTTemp.Add(temp);
                                            statusPay = true;
                                        }
                                        else // Si hay algun pago al que hay que cargrlo
                                        {
                                            ccee_TipoCargoEconomico cTCET = db.ccee_TipoCargoEconomico.Find(postumo.fk_TipoCargoEconomico);
                                            ccee_CargoEconomico_timbre cECOT = postumoTemp.FirstOrDefault();
                                            int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                            ResponseEconomicJson temp = economicTimbreUpdate(tempUpload.seguroPostumo, tempPay.Pag_NoPago, doc, nomina.enableUpdate, 
                                                tempUpload.colegiado, cTCET, cECOT, false, nomina.idBatch); //El cECOT es para que sea el ultimo del listado disponible
                                            aomuntTotal += temp.amount;
                                            idEcomomicsT.Add(temp);
                                            idEcomomicsTTemp.Add(temp);
                                            statusPay = true;
                                        }


                                    }

                                }
                            }
                            else if (tempUpload.seguroPostumo <= 0 && nomina.negatives && referee != null) // si el pago de postumo no esta pagado
                            {
                                ccee_CargoEconomico_timbre postumo = (listTimbre
                                                                        .Where(p => p.CEcT_FechaGeneracion.Value.Month == tempUpload.mes
                                                                        && p.CEcT_FechaGeneracion.Value.Year == tempUpload.anio
                                                                        && p.fk_TipoCargoEconomico == 5))
                                                                        .FirstOrDefault();

                                if (postumo != null)
                                {

                                    if (postumo.CEcT_Monto == (tempUpload.seguroPostumo * -1)) // Si el pago es igual se anula
                                    {
                                        postumo.fk_Pago = null;
                                        postumo.fk_DocContable = null;
                                        postumo.fk_Batch = nomina.idBatch;
                                        db.Entry(postumo).State = EntityState.Modified;
                                        infoData += " (Se elimino pago Postumo de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio + ")";
                                    }
                                    else // si el pago es menor o mayor este se modifica
                                    {
                                        postumo.CEcT_Monto += tempUpload.seguroPostumo;
                                        postumo.fk_Batch = nomina.idBatch;
                                        db.Entry(postumo).State = EntityState.Modified;
                                        aomuntTotal += postumo.CEcT_Monto.Value;
                                        infoData += " (Se sustituyo pago Postumo de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio + ")";
                                    }
                                }
                                else
                                {
                                    infoData += " (No existe pago Postumo de:  " + tempUpload.colegiado + " | " + tempUpload.mes + " / " + tempUpload.anio + ")";
                                }
                            }

                            if (statusPay)
                            {
                                decimal tempPaymentMethodAmount = aomuntTotal;
                                ccee_MedioPago_Pago_C tempPaymentMethodPay;
                                foreach (RequestPaymentMethod_PayJson paymentMethod in nomina.jsonContentMethod)
                                {
                                    tempPaymentMethodPay = new ccee_MedioPago_Pago_C()
                                    {
                                        MPP_Monto = tempPaymentMethodAmount,
                                        MPP_NoTransaccion = paymentMethod.noTransaction,
                                        MPP_NoDocumento = paymentMethod.noDocument,
                                        MPP_Observacion = paymentMethod.remarke,
                                        MPP_Reserva = paymentMethod.reservation,
                                        MPP_Rechazado = paymentMethod.rejection,
                                        MPP_FechaHora = DateTime.Now,
                                        MPP_EsNomina = 1,
                                        fk_MedioPago = paymentMethod.idPaymentMethod,
                                        fk_Pago = tempPay.Pag_NoPago,
                                        fk_Banco = paymentMethod.idBank
                                    };
                                    db.ccee_MedioPago_Pago_C.Add(tempPaymentMethodPay);
                                    tempPaymentMethodPay = null;
                                    tempPaymentMethodAmount = 0m;
                                }

                                //ccee_MedioPago_Pago_C tempPaymentMethodPay = new ccee_MedioPago_Pago_C()
                                //{
                                //    MPP_Monto = aomuntTotal,
                                //    MPP_NoTransaccion = "",
                                //    MPP_NoDocumento = "",
                                //    MPP_Observacion = "Pago de nomina",
                                //    MPP_Reserva = 0,
                                //    MPP_Rechazado = 0,
                                //    MPP_FechaHora = DateTime.Now,
                                //    fk_MedioPago = 44,
                                //    fk_Pago = tempPay.Pag_NoPago,
                                //    fk_Banco = 40,
                                //    MPP_Nomina = 1
                                //};
                                //if (nomina.idType == "1")
                                //{
                                //    tempPaymentMethodPay.fk_Banco = nomina.idBank;
                                //    tempPaymentMethodPay.MPP_NoDocumento = nomina.NoDocument;
                                //    tempPaymentMethodPay.fk_MedioPago = 45;
                                //}
                                tempPay.Pag_Monto = aomuntTotal;
                                //db.ccee_MedioPago_Pago_C.Add(tempPaymentMethodPay);
                                Debug.WriteLine("Monto Total: " + aomuntTotal);
                                Debug.WriteLine("Numero de pago: " + tempPay.Pag_NoPago);
                                db.SaveChanges();
                            }
                            else
                            {
                                errorNomina.Add(tempUpload);
                                db.ccee_Pago.Remove(tempPay);
                                db.SaveChanges();
                            }


                            //Generador de recibo por colegiado o no colegiado
                            if (nomina.idTypeDocument == 1 && referee != null && statusPay)
                            {
                                int idDoc = createDocumentAccunting(nomina.NoColegiado.id, cahier,
                                    nomina.NoColegiado.name + " " + nomina.NoColegiado.lastname, nomina.NoColegiado.address,
                                    nomina.NoColegiado.nit, "", tempDocCon, idEcomomicsCTemp, idEcomomicsTTemp, idEcomomicsOTemp,
                                    nomina.document, tempDocCol, nomina.idType, nomina.NoDocument, nomina.textOfEconomic, tempDateOfPay, nomina.jsonContentMethod);
                                nomina.document = "" + (Convert.ToInt32(nomina.document) + 1);
                                listDocument.Add(new JsonDataResponse()
                                {
                                    url = "/Payment/Print/" + idDoc,
                                    text = "Recibo " + "A@" + (Convert.ToInt32(nomina.document) - 1)
                                });

                            }
                            else if (nomina.idTypeDocument == 2 && referee != null && statusPay)
                            {
                                //ccee_Colegiado temCol = db.ccee_Colegiado.Find(tempUpload.colegiado);

                                int idDoc = createDocumentAccunting(referee.Col_NoColegiado, cahier,
                                    referee.Col_PrimerNombre + " " + referee.Col_SegundoNombre + " " + referee.Col_PrimerApellido + " " + referee.Col_SegundoApellido,
                                    "Guatemala",
                                    referee.Col_Nit, "", tempDocCon, idEcomomicsCTemp, idEcomomicsTTemp, idEcomomicsOTemp,
                                    nomina.document, tempDocCol, nomina.idType, nomina.NoDocument, nomina.textOfEconomic, tempDateOfPay, nomina.jsonContentMethod);
                                nomina.document = "" + (Convert.ToInt32(nomina.document) + 1);
                                listDocument.Add(new JsonDataResponse()
                                {
                                    url = "/Payment/Print/" + idDoc,
                                    text = "Recibo " + "A@" + (Convert.ToInt32(nomina.document) - 1)
                                });
                            }
                            db.SaveChanges();

                        }

                        idEcomomics.AddRange(idEcomomicsC);
                        idEcomomics.AddRange(idEcomomicsT);
                        idEcomomics.AddRange(idEcomomicsO);

                        if (idEcomomics.Count > 0)
                        {

                            
                            ResponseUploadNominaPayJson request = new ResponseUploadNominaPayJson();
                            request.status = 200;
                            request.listDoc = listDocument;
                            request.textError = infoData;

                            Api_StatusController apiStatus = new Api_StatusController();
                            
                            db.SaveChanges();
                            Autorization.saveOperation(db, autorization, "Agregando pago nomina");
                            bool stautusReferee = apiStatus.statusWithoutAuth(nomina.NoColegiado.id);
                            transaction.Commit();
                            //transaction.Rollback();
                            return request;
                        }else if (nomina.negatives)
                        {
                            ResponseUploadNominaPayJson request = new ResponseUploadNominaPayJson();
                            request.status = 200;
                            request.listDoc = listDocument;
                            request.textError = infoData;
                            Api_StatusController apiStatus = new Api_StatusController();
                            db.SaveChanges();
                            Autorization.saveOperation(db, autorization, "Agregando pago nomina");
                            bool stautusReferee = apiStatus.statusWithoutAuth(nomina.NoColegiado.id);
                            transaction.Commit();
                            //transaction.Rollback();
                            return request;
                        }
                        else
                        {
                            transaction.Rollback();
                            Debug.WriteLine("ningun campo ingresado o :: " + infoData + "  ::");
                            return new ResponseUploadNominaPayJson()
                            {
                                status = 400,
                                textError = "ningun campo ingresado o :: " + infoData + "  ::"
                            };
                        }

                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e);
                        transaction.Rollback();
                        Debug.WriteLine("Problemas generales, posiblemente tiene un error grabe en el listado por favor revisar");
                        return new ResponseUploadNominaPayJson()
                        {
                            status = 400,
                            textError = "Problemas generales, posiblemente tiene un error grabe en el listado por favor revisar"
                        };
                    }
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return new ResponseUploadNominaPayJson()
                {
                    status = 400,
                    textError = "Problemas generasles"
                };
            }

        }*/

        public ResponseUploadNominaPayJson Post(RequestUploadNominaJson nomina)
        {

            try
            {
                int idPaytimbre = 130;
                int idPayDemand = 131;
                int idTypeEcoCuadre = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "VarCuadrePago"));
                string cent = GeneralFunction.getKeyTableVariable(db, Constant.table_var_aprox_decimal);
                decimal dCent = Convert.ToDecimal(cent);
                string infoData = "";
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        string autorization = Request.Headers.Authorization.Parameter;
                        if (!Autorization.enableAutorization(db, autorization)) return null;
                        ccee_TipoCargoEconomico typeMora = db.ccee_TipoCargoEconomico
                            .Where(o => o.TCE_CondicionCalculo.Equals("esMora")).FirstOrDefault();

                        List<ResponseEconomicJson> idEcomomics = new List<ResponseEconomicJson>();
                        List<ResponseEconomicJson> idEcomomicsC = new List<ResponseEconomicJson>();
                        List<ResponseEconomicJson> idEcomomicsT = new List<ResponseEconomicJson>();
                        List<ResponseEconomicJson> idEcomomicsO = new List<ResponseEconomicJson>();
                        List<ResponseEconomicJson> idEcomomicsCTemp;
                        List<ResponseEconomicJson> idEcomomicsTTemp;
                        List<ResponseEconomicJson> idEcomomicsOTemp;
                        List<JsonDataResponse> listDocument = new List<JsonDataResponse>();
                        List<ResponseUploadNominaJson> errorNomina = new List<ResponseUploadNominaJson>();

                        ccee_Cajero cahier = db.ccee_Cajero.Find(nomina.idCashier);


                        string typeDocAccouting = db.ccee_Variables.Where(o => o.Var_Clave.Equals("VarReciboCol")).Select(p => p.Var_Valor).FirstOrDefault();
                        int typeDocAccoutingInt = Convert.ToInt32(typeDocAccouting);
                        ccee_TipoDocContable tempDocCon =
                            db.ccee_TipoDocContable.Find(typeDocAccoutingInt);

                        ccee_DocContable tempDoc = db.ccee_DocContable.Find(nomina.idDocAccount);
                        tempDoc.DCo_ANombre = nomina.NoColegiado.name;
                        tempDoc.fk_TipoDocContable = tempDocCon.TDC_NoTipoDocContable;
                        db.Entry(tempDoc).State = EntityState.Modified;

                        db.SaveChanges();

                        DateTime tempDateOfPay = ((nomina.dateOfPay == null) ?
                            DateTime.Today : nomina.dateOfPay.Value.Date);


                        for (int i = 0; i < nomina.data.Count; i++)
                        {
                            Debug.WriteLine("Numero de fila: " + i);
                            ResponseUploadNominaJson tempUpload = nomina.data[i];
                            bool statusPay = false;
                            decimal aomuntTotal = 0;

                            idEcomomicsCTemp = new List<ResponseEconomicJson>();
                            idEcomomicsTTemp = new List<ResponseEconomicJson>();
                            idEcomomicsOTemp = new List<ResponseEconomicJson>();

                            ccee_DocContable tempDocCol = new ccee_DocContable();
                            if (nomina.idTypeDocument > 0)
                            {
                                tempDocCol.fk_TipoDocContable = tempDocCon.TDC_NoTipoDocContable;

                                if (nomina.idTypeDocument == 1)
                                {
                                    tempDocCol.DCo_ANombre = nomina.NoColegiado.name;
                                }
                                else if (nomina.idTypeDocument == 2)
                                {
                                    tempDocCol.DCo_ANombre = tempUpload.nombre;
                                }
                                db.ccee_DocContable.Add(tempDocCol);
                                db.SaveChanges();
                            }

                            ccee_Pago tempPay = new ccee_Pago()
                            {
                                Pag_Monto = 0,
                                Pag_Observacion = "Pago en nomina",
                                Pag_Fecha = tempDateOfPay,
                                fk_Caja = nomina.idCash,
                                fk_Cajero = nomina.idCashier
                            };
                            db.ccee_Pago.Add(tempPay);
                            db.SaveChanges();

                            ccee_Colegiado referee = db.ccee_Colegiado.Find(tempUpload.colegiado);

                            Debug.WriteLine("Colegiado: " + tempUpload.colegiado);
                            if (referee == null)
                            {
                                aomuntTotal = tempUpload.cuotaTimbre + tempUpload.cuotaColegiado + tempUpload.seguroPostumo;
                                ccee_DocContable tempDocCol2 = new ccee_DocContable();
                                tempDocCol2.fk_TipoDocContable = tempDocCon.TDC_NoTipoDocContable;
                                tempDocCol2.DCo_ANombre = tempUpload.nombre;
                                db.ccee_DocContable.Add(tempDocCol2);
                                ccee_NoColegiado tempNotReferee = new ccee_NoColegiado()
                                {
                                    NoC_Nombre = tempUpload.nombre,
                                    NoC_Apellido = "No colegiado por Nomina",
                                    NoC_Direccion = "Guatemala",
                                    NoC_Nit = "",
                                    NoC_NumTelefono = 55555555,
                                    NoC_Observacion = "No colegiado por Nomina",
                                    NoC_Afiliado = 0
                                };
                                db.ccee_NoColegiado.Add(tempNotReferee);
                                db.SaveChanges();

                                ccee_CargoEconomico_otros tempEcoOther = new ccee_CargoEconomico_otros();
                                tempEcoOther.CEcO_Monto = tempUpload.cuotaColegiado + tempUpload.cuotaTimbre + tempUpload.seguroPostumo;
                                tempEcoOther.CEcO_FechaGeneracion = Convert.ToDateTime(tempUpload.mes + "/" + 1 + "/" + tempUpload.anio);
                                tempEcoOther.fk_DocContable = tempDocCol2.DCo_NoDocContable;
                                tempEcoOther.fk_NoColegiado = tempNotReferee.NoC_NoNoColegiado;
                                tempEcoOther.fk_TipoCargoEconomico = 157;
                                tempEcoOther.fk_Pago = tempPay.Pag_NoPago;
                                tempEcoOther.fk_Batch = nomina.idBatch;
                                db.ccee_CargoEconomico_otros.Add(tempEcoOther);
                                db.SaveChanges();
                                ResponseEconomicJson noRefereeOther = new ResponseEconomicJson()
                                {
                                    id = tempEcoOther.CEcO_NoCargoEconomico,
                                    amount = tempEcoOther.CEcO_Monto.Value,
                                    idTypeEconomic = tempEcoOther.fk_TipoCargoEconomico,
                                    idNoReferee = tempEcoOther.fk_NoColegiado.Value,
                                    enable = 1,
                                    description = "Cuota por planilla NO COLEGIADO",
                                    dateCreated = tempEcoOther.CEcO_FechaGeneracion
                                };

                                idEcomomicsOTemp.Add(noRefereeOther);

                                int idDoc = createDocumentAccunting(nomina.NoColegiado.id, cahier,
                                    nomina.NoColegiado.name + " " + nomina.NoColegiado.lastname, nomina.NoColegiado.address,
                                    nomina.NoColegiado.nit, "", tempDocCon, idEcomomicsCTemp, idEcomomicsTTemp, idEcomomicsOTemp,
                                    nomina.document, tempDocCol2, nomina.idType, nomina.NoDocument, nomina.textOfEconomic, tempDateOfPay, nomina.jsonContentMethod);
                                infoData += " (Se agrego un no colegiado Doc: " + nomina.document + ")";
                                nomina.document = "" + (Convert.ToInt32(nomina.document) + 1);
                                listDocument.Add(new JsonDataResponse()
                                {
                                    url = "/Payment/Print/" + idDoc,
                                    text = "Recibo " + "A@" + (Convert.ToInt32(nomina.document) - 1)
                                });

                                decimal tempPaymentMethodAmount = aomuntTotal;
                                ccee_MedioPago_Pago_C tempPaymentMethodPay;
                                foreach (RequestPaymentMethod_PayJson paymentMethod in nomina.jsonContentMethod)
                                {
                                    tempPaymentMethodPay = new ccee_MedioPago_Pago_C()
                                    {
                                        MPP_Monto = tempPaymentMethodAmount,
                                        MPP_NoTransaccion = paymentMethod.noTransaction,
                                        MPP_NoDocumento = paymentMethod.noDocument,
                                        MPP_Observacion = paymentMethod.remarke,
                                        MPP_Reserva = paymentMethod.reservation,
                                        MPP_Rechazado = paymentMethod.rejection,
                                        MPP_FechaHora = DateTime.Now,
                                        MPP_EsNomina = 1,
                                        fk_MedioPago = paymentMethod.idPaymentMethod,
                                        fk_Pago = tempPay.Pag_NoPago,
                                        fk_Banco = paymentMethod.idBank
                                    };
                                    db.ccee_MedioPago_Pago_C.Add(tempPaymentMethodPay);
                                    tempPaymentMethodPay = null;
                                    tempPaymentMethodAmount = 0m;
                                }

                                statusPay = false;
                            }

                            aomuntTotal = 0;

                            if (tempUpload.cuotaColegiado > 0 && referee != null)
                            {

                                List<ccee_CargoEconomico_colegiado> colegiado = (db.ccee_CargoEconomico_colegiado
                                                                            .Where(q => q.fk_Colegiado == tempUpload.colegiado))
                                                                            .ToList();

                                List<ccee_CargoEconomico_colegiado> colegiadoOnly = (colegiado.Where(p => p.CEcC_FechaGeneracion.Value.Month == tempUpload.mes
                                                               && p.CEcC_FechaGeneracion.Value.Year == tempUpload.anio)).ToList();

                                if (colegiadoOnly.Count == 0) // cargo econimico colegiado si este no existe
                                {
                                    decimal amountEcomonicReferee = 0;
                                    List<ccee_TipoCargoEconomico> tEconomic = generarMontoColegiado();
                                    for (int j = 0; j < tEconomic.Count; j++)
                                    {
                                        int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                        ResponseEconomicJson temp = economicReferee(tempUpload.mes, tempUpload.anio, tempUpload.colegiado, tempPay.Pag_NoPago,
                                            doc, tEconomic[j], nomina.idBatch);
                                        aomuntTotal += temp.amount;
                                        amountEcomonicReferee += temp.amount;
                                        idEcomomicsC.Add(temp);
                                        idEcomomicsCTemp.Add(temp);
                                        statusPay = true;
                                    }

                                    decimal diffAmountReferee = tempUpload.cuotaColegiado - amountEcomonicReferee;
                                    if (diffAmountReferee != 0)
                                    {
                                        //economic.TCE_CondicionCalculo.Equals("varAdministracion")
                                        foreach (var economic in tEconomic)
                                        {
                                            if (economic.TCE_CondicionCalculo == null) continue;
                                            if (economic.TCE_CondicionCalculo.Equals("varAdministracion"))
                                            {
                                                int month = tempUpload.mes;
                                                int year = tempUpload.anio;

                                                //Se crea un cargo economico de diferencia para cuadrar
                                                int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                                ResponseEconomicJson temp = economicRefereeValue(month, year, tempUpload.colegiado, tempPay.Pag_NoPago,
                                                    doc, economic, nomina.idBatch, diffAmountReferee);
                                                aomuntTotal += temp.amount;
                                                idEcomomicsC.Add(temp);
                                                idEcomomicsCTemp.Add(temp);
                                                statusPay = true;

                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    decimal amountEcomonicReferee = 0;
                                    DateTime? datePayEconomicReferee = null;
                                    foreach (ccee_CargoEconomico_colegiado cEco in colegiadoOnly)
                                    {
                                        if (cEco.fk_Pago == null) // Si el pago si existe pero el no se ha pagado
                                        {
                                            int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                            ResponseEconomicJson temp = economicRefereeUpdate(doc, tempPay.Pag_NoPago, cEco, false, 0, false, nomina.idBatch);
                                            aomuntTotal += temp.amount;
                                            idEcomomicsC.Add(temp);
                                            idEcomomicsCTemp.Add(temp);
                                            amountEcomonicReferee += temp.amount; //sumando en la variable
                                            statusPay = true;
                                        }
                                        else
                                        { // Si se pago pero se necesita cargarlo en algun cargo
                                            DateTime datePay;
                                            List<ccee_CargoEconomico_colegiado> colegiadoTemp = null;
                                            colegiadoTemp = colegiado.Where(p => p.fk_Pago == null && p.fk_TipoCargoEconomico == cEco.fk_TipoCargoEconomico)
                                                .OrderBy(o => o.CEcC_FechaGeneracion).ToList();

                                            if (colegiadoTemp.Count == 0) // si no existe ningun pago al que se le puede cargar
                                            {
                                                ccee_TipoCargoEconomico cTCET = db.ccee_TipoCargoEconomico.Find(cEco.fk_TipoCargoEconomico);
                                                ccee_CargoEconomico_colegiado cECOT = colegiado.OrderByDescending(p => p.CEcC_FechaGeneracion).FirstOrDefault();
                                                datePay = cECOT.CEcC_FechaGeneracion.Value.AddMonths(1);
                                                datePayEconomicReferee = datePay;
                                                int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                                ResponseEconomicJson temp = economicReferee(datePay.Month, datePay.Year, tempUpload.colegiado, tempPay.Pag_NoPago,
                                                    doc, cTCET, nomina.idBatch);
                                                aomuntTotal += temp.amount;
                                                idEcomomicsC.Add(temp);
                                                idEcomomicsCTemp.Add(temp);
                                                amountEcomonicReferee += temp.amount; //sumando en la variable
                                                statusPay = true;
                                            }
                                            else // Si hay algun pago al que hay que cargrlo
                                            {
                                                ccee_CargoEconomico_colegiado cECOT = colegiadoTemp.FirstOrDefault();
                                                int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                                ResponseEconomicJson temp = economicRefereeUpdate(doc, tempPay.Pag_NoPago, cECOT, false, 0, false, nomina.idBatch);
                                                aomuntTotal += temp.amount;
                                                idEcomomicsC.Add(temp);
                                                idEcomomicsCTemp.Add(temp);
                                                amountEcomonicReferee += temp.amount; //sumando en la variable
                                                statusPay = true;
                                            }
                                        }
                                    }
                                    //Diferencia para cuadrar la fecha
                                    decimal diffAmountReferee = tempUpload.cuotaColegiado - amountEcomonicReferee;
                                    if(diffAmountReferee != 0)
                                    {
                                        List<ccee_TipoCargoEconomico> tEconomic = generarMontoColegiado();
                                        //economic.TCE_CondicionCalculo.Equals("varAdministracion")
                                        foreach (var economic in tEconomic)
                                        {
                                            if (economic.TCE_CondicionCalculo == null) continue;
                                            if (economic.TCE_CondicionCalculo.Equals("varAdministracion"))
                                            {
                                                int month = tempUpload.mes;
                                                int year = tempUpload.anio;
                                                if (datePayEconomicReferee != null)
                                                {
                                                    month = datePayEconomicReferee.Value.Month;
                                                    year = datePayEconomicReferee.Value.Year;
                                                }

                                                //Se crea un cargo economico de diferencia para cuadrar
                                                int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                                ResponseEconomicJson temp = economicRefereeValue(month, year, tempUpload.colegiado, tempPay.Pag_NoPago,
                                                    doc, economic, nomina.idBatch, diffAmountReferee);
                                                aomuntTotal += temp.amount;
                                                idEcomomicsC.Add(temp);
                                                idEcomomicsCTemp.Add(temp);
                                                statusPay = true;
                                                
                                            }
                                        }
                                    }



                                }
                            }
                            else if (tempUpload.cuotaColegiado < 0 && referee != null) // Si el pago viene negativo colegiado
                            {
                                
                                List<ccee_TipoCargoEconomico> tEconomic = generarMontoColegiado();
                                //economic.TCE_CondicionCalculo.Equals("varAdministracion")
                                foreach (var economic in tEconomic)
                                {
                                    if (economic.TCE_CondicionCalculo == null) continue;
                                    if (economic.TCE_CondicionCalculo.Equals("varAdministracion"))
                                    {
                                        int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                        ResponseEconomicJson temp = economicRefereeNegative(tempUpload.mes, tempUpload.anio, tempUpload.colegiado, tempPay.Pag_NoPago,
                                            doc, economic, nomina.idBatch, tempUpload.cuotaColegiado);
                                        aomuntTotal += temp.amount;
                                        idEcomomicsC.Add(temp);
                                        idEcomomicsCTemp.Add(temp);
                                        statusPay = true;
                                        break;
                                    }
                                }
                                
                            }

                            //Listado Global para manejo de una solo llamada a base de datos
                            List<ccee_CargoEconomico_timbre> listTimbre = (db.ccee_CargoEconomico_timbre
                                                                        .Where(q => q.fk_Colegiado == tempUpload.colegiado)).ToList();

                            if (tempUpload.cuotaTimbre != 0 && referee != null) // Si hay algun pago de timbre que procesar
                            {


                                ccee_CargoEconomico_timbre timbre = (listTimbre
                                                                        .Where(p => p.CEcT_FechaGeneracion.Value.Month == tempUpload.mes
                                                                                && p.CEcT_FechaGeneracion.Value.Year == tempUpload.anio
                                                                                && p.fk_TipoCargoEconomico == idPaytimbre))
                                                                        .FirstOrDefault();

                                if(timbre == null) // v2 si no existe el pago
                                {
                                    ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(idPaytimbre);
                                    int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                    ResponseEconomicJson temp = economicTimbre(tempUpload.cuotaTimbre, tempUpload.mes, tempUpload.anio, doc,
                                        tempUpload.colegiado, tempPay.Pag_NoPago, nomina.enableUpdate, temType, nomina.idBatch);
                                    aomuntTotal += temp.amount;
                                    idEcomomicsT.Add(temp);
                                    idEcomomicsTTemp.Add(temp);
                                    statusPay = true;
                                }else // v2 si existe el pago
                                {
                                    if(timbre.fk_Pago == null) // v2  si todavia no ha sido pagado
                                    {
                                        if(timbre.CEcT_Monto <= tempUpload.cuotaTimbre) // v2 si el pago el igual o mayor de lo que debe
                                        {
                                            ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(timbre.fk_TipoCargoEconomico);
                                            int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                            ResponseEconomicJson temp = economicTimbreUpdate(tempUpload.cuotaTimbre, tempPay.Pag_NoPago, doc, nomina.enableUpdate,
                                                tempUpload.colegiado, temType, timbre, false, nomina.idBatch);
                                            aomuntTotal += temp.amount;
                                            idEcomomicsT.Add(temp);
                                            idEcomomicsTTemp.Add(temp);
                                            statusPay = true;
                                        }
                                        else // v2 si el pago es menor al pago que debe
                                        {
                                            //Primero hacemos la diferencia
                                            decimal diffAmount = timbre.CEcT_Monto.Value - tempUpload.cuotaTimbre;
                                            // Actualizamos la existente con el valor del archivo
                                            ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(timbre.fk_TipoCargoEconomico);
                                            int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                            ResponseEconomicJson temp = economicTimbreUpdate(tempUpload.cuotaTimbre, tempPay.Pag_NoPago, doc, nomina.enableUpdate,
                                                tempUpload.colegiado, temType, timbre, false, nomina.idBatch);
                                            aomuntTotal += temp.amount;
                                            idEcomomicsT.Add(temp);
                                            idEcomomicsTTemp.Add(temp);
                                            statusPay = true;

                                            //creamos una nueva cuota
                                            
                                            ResponseEconomicJson temp2 = economicTimbre(diffAmount, tempUpload.mes, tempUpload.anio, null,
                                                tempUpload.colegiado, null, nomina.enableUpdate, temType, null);
                                            idEcomomicsT.Add(temp2);
                                            idEcomomicsTTemp.Add(temp2);
                                            statusPay = true;

                                        }
                                    }else // v2 si ya fue pagada
                                    {
                                        ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(idPaytimbre);
                                        int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                        ResponseEconomicJson temp = economicTimbre(tempUpload.cuotaTimbre, tempUpload.mes, tempUpload.anio, doc,
                                            tempUpload.colegiado, tempPay.Pag_NoPago, nomina.enableUpdate, temType, nomina.idBatch);
                                        aomuntTotal += temp.amount;
                                        idEcomomicsT.Add(temp);
                                        idEcomomicsTTemp.Add(temp);
                                        statusPay = true;
                                    }
                                }
                                
                            }

                            if (tempUpload.seguroPostumo > 0 && referee != null)
                            {
                                ccee_CargoEconomico_timbre postumo = (listTimbre
                                                                        .Where(p => p.CEcT_FechaGeneracion.Value.Month == tempUpload.mes
                                                                        && p.CEcT_FechaGeneracion.Value.Year == tempUpload.anio
                                                                        && p.fk_TipoCargoEconomico == 5))
                                                                        .FirstOrDefault();
                                
                                if (postumo == null)
                                {
                                    ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(5);
                                    int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                    ResponseEconomicJson temp = economicTimbre(temType.TCE_Valor.Value, tempUpload.mes, tempUpload.anio, doc,
                                        tempUpload.colegiado, tempPay.Pag_NoPago, false, temType, nomina.idBatch);
                                    aomuntTotal += temp.amount;
                                    idEcomomicsT.Add(temp);
                                    idEcomomicsTTemp.Add(temp);
                                    statusPay = true;
                                }
                                else
                                {
                                    if (postumo.fk_Pago == null) // Si si existe el cargo economico pero no se ha pagado
                                    {
                                        if(tempUpload.seguroPostumo >= postumo.CEcT_Monto)
                                        {
                                            ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(postumo.fk_TipoCargoEconomico);
                                            int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                            ResponseEconomicJson temp = economicTimbreUpdate(tempUpload.seguroPostumo, tempPay.Pag_NoPago, doc, nomina.enableUpdate,
                                                tempUpload.colegiado, temType, postumo, false, nomina.idBatch);
                                            aomuntTotal += temp.amount;
                                            idEcomomicsT.Add(temp);
                                            idEcomomicsTTemp.Add(temp);
                                            statusPay = true;
                                        }
                                        else
                                        {
                                            decimal diffAmount = postumo.CEcT_Monto.Value - tempUpload.seguroPostumo;

                                            ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(postumo.fk_TipoCargoEconomico);
                                            int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                            ResponseEconomicJson temp = economicTimbreUpdate(tempUpload.seguroPostumo, tempPay.Pag_NoPago, doc, nomina.enableUpdate,
                                                tempUpload.colegiado, temType, postumo, false, nomina.idBatch);
                                            aomuntTotal += temp.amount;
                                            idEcomomicsT.Add(temp);
                                            idEcomomicsTTemp.Add(temp);
                                            statusPay = true;

                                            //creamos un pago con la diferencia
                                            ResponseEconomicJson temp2 = economicTimbre(diffAmount, tempUpload.mes, tempUpload.anio, doc,
                                                tempUpload.colegiado, null, nomina.enableUpdate, temType, null);
                                            idEcomomicsT.Add(temp2);
                                            idEcomomicsTTemp.Add(temp2);
                                            statusPay = true;

                                        }
                                        
                                    }
                                    else
                                    {


                                        DateTime datePay;
                                        List<ccee_CargoEconomico_timbre> postumoTemp = null;
                                        postumoTemp = listTimbre.Where(p => p.fk_Pago == null && p.fk_TipoCargoEconomico == postumo.fk_TipoCargoEconomico)
                                            .OrderBy(o => o.CEcT_FechaGeneracion).ToList();

                                        if (postumoTemp.Count == 0) // si no existe ningun pago al que se le puede cargar
                                        {
                                            ccee_TipoCargoEconomico cTCET = db.ccee_TipoCargoEconomico.Find(postumo.fk_TipoCargoEconomico);
                                            ccee_CargoEconomico_timbre cECOT = listTimbre.OrderByDescending(o => o.CEcT_FechaGeneracion).ToList().FirstOrDefault();
                                            datePay = cECOT.CEcT_FechaGeneracion.Value.AddMonths(1);
                                            int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                            ResponseEconomicJson temp = economicTimbre(cTCET.TCE_Valor.Value, datePay.Month, datePay.Year, doc,
                                                        tempUpload.colegiado, tempPay.Pag_NoPago, false, cTCET, nomina.idBatch);
                                            aomuntTotal += temp.amount;
                                            idEcomomicsT.Add(temp);
                                            idEcomomicsTTemp.Add(temp);
                                            statusPay = true;
                                        }
                                        else // Si hay algun pago al que hay que cargrlo
                                        {
                                            ccee_TipoCargoEconomico cTCET = db.ccee_TipoCargoEconomico.Find(postumo.fk_TipoCargoEconomico);
                                            ccee_CargoEconomico_timbre cECOT = postumoTemp.FirstOrDefault();
                                            int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                            ResponseEconomicJson temp = economicTimbreUpdate(tempUpload.seguroPostumo, tempPay.Pag_NoPago, doc, nomina.enableUpdate,
                                                tempUpload.colegiado, cTCET, cECOT, false, nomina.idBatch); //El cECOT es para que sea el ultimo del listado disponible
                                            aomuntTotal += temp.amount;
                                            idEcomomicsT.Add(temp);
                                            idEcomomicsTTemp.Add(temp);
                                            statusPay = true;
                                        }


                                    }

                                }
                            }
                            else if (tempUpload.seguroPostumo < 0 && referee != null) // si el pago de postumo no esta pagado
                            {
                                ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(5);
                                int doc = ((nomina.idTypeDocument > 0) ? tempDocCol.DCo_NoDocContable : tempDoc.DCo_NoDocContable);
                                ResponseEconomicJson temp = economicTimbre(tempUpload.seguroPostumo, tempUpload.mes, tempUpload.anio, doc,
                                    tempUpload.colegiado, tempPay.Pag_NoPago, false, temType, nomina.idBatch);
                                aomuntTotal += temp.amount;
                                idEcomomicsT.Add(temp);
                                idEcomomicsTTemp.Add(temp);
                                statusPay = true;
                            }

                            if (statusPay)
                            {
                                decimal tempPaymentMethodAmount = aomuntTotal;
                                ccee_MedioPago_Pago_C tempPaymentMethodPay;
                                foreach (RequestPaymentMethod_PayJson paymentMethod in nomina.jsonContentMethod)
                                {
                                    tempPaymentMethodPay = new ccee_MedioPago_Pago_C()
                                    {
                                        MPP_Monto = tempPaymentMethodAmount,
                                        MPP_NoTransaccion = paymentMethod.noTransaction,
                                        MPP_NoDocumento = paymentMethod.noDocument,
                                        MPP_Observacion = paymentMethod.remarke,
                                        MPP_Reserva = paymentMethod.reservation,
                                        MPP_Rechazado = paymentMethod.rejection,
                                        MPP_FechaHora = DateTime.Now,
                                        MPP_EsNomina = 1,
                                        fk_MedioPago = paymentMethod.idPaymentMethod,
                                        fk_Pago = tempPay.Pag_NoPago,
                                        fk_Banco = paymentMethod.idBank
                                    };
                                    db.ccee_MedioPago_Pago_C.Add(tempPaymentMethodPay);
                                    tempPaymentMethodPay = null;
                                    tempPaymentMethodAmount = 0m;
                                }

                                //ccee_MedioPago_Pago_C tempPaymentMethodPay = new ccee_MedioPago_Pago_C()
                                //{
                                //    MPP_Monto = aomuntTotal,
                                //    MPP_NoTransaccion = "",
                                //    MPP_NoDocumento = "",
                                //    MPP_Observacion = "Pago de nomina",
                                //    MPP_Reserva = 0,
                                //    MPP_Rechazado = 0,
                                //    MPP_FechaHora = DateTime.Now,
                                //    fk_MedioPago = 44,
                                //    fk_Pago = tempPay.Pag_NoPago,
                                //    fk_Banco = 40,
                                //    MPP_Nomina = 1
                                //};
                                //if (nomina.idType == "1")
                                //{
                                //    tempPaymentMethodPay.fk_Banco = nomina.idBank;
                                //    tempPaymentMethodPay.MPP_NoDocumento = nomina.NoDocument;
                                //    tempPaymentMethodPay.fk_MedioPago = 45;
                                //}
                                tempPay.Pag_Monto = aomuntTotal;
                                //db.ccee_MedioPago_Pago_C.Add(tempPaymentMethodPay);
                                Debug.WriteLine("Monto Total: " + aomuntTotal);
                                Debug.WriteLine("Numero de pago: " + tempPay.Pag_NoPago);
                                db.SaveChanges();
                            }
                            else
                            {
                                errorNomina.Add(tempUpload);
                                db.ccee_Pago.Remove(tempPay);
                                db.SaveChanges();
                            }


                            //Generador de recibo por colegiado o no colegiado
                            if (nomina.idTypeDocument == 1 && referee != null && statusPay)
                            {
                                int idDoc = createDocumentAccunting(nomina.NoColegiado.id, cahier,
                                    nomina.NoColegiado.name + " " + nomina.NoColegiado.lastname, nomina.NoColegiado.address,
                                    nomina.NoColegiado.nit, "", tempDocCon, idEcomomicsCTemp, idEcomomicsTTemp, idEcomomicsOTemp,
                                    nomina.document, tempDocCol, nomina.idType, nomina.NoDocument, nomina.textOfEconomic, tempDateOfPay, nomina.jsonContentMethod);
                                nomina.document = "" + (Convert.ToInt32(nomina.document) + 1);
                                listDocument.Add(new JsonDataResponse()
                                {
                                    url = "/Payment/Print/" + idDoc,
                                    text = "Recibo " + "A@" + (Convert.ToInt32(nomina.document) - 1)
                                });

                            }
                            else if (nomina.idTypeDocument == 2 && referee != null && statusPay)
                            {
                                //ccee_Colegiado temCol = db.ccee_Colegiado.Find(tempUpload.colegiado);

                                int idDoc = createDocumentAccunting(referee.Col_NoColegiado, cahier,
                                    referee.Col_PrimerNombre + " " + referee.Col_SegundoNombre + " " + referee.Col_PrimerApellido + " " + referee.Col_SegundoApellido,
                                    "Guatemala",
                                    referee.Col_Nit, "", tempDocCon, idEcomomicsCTemp, idEcomomicsTTemp, idEcomomicsOTemp,
                                    nomina.document, tempDocCol, nomina.idType, nomina.NoDocument, nomina.textOfEconomic, tempDateOfPay, nomina.jsonContentMethod);
                                nomina.document = "" + (Convert.ToInt32(nomina.document) + 1);
                                listDocument.Add(new JsonDataResponse()
                                {
                                    url = "/Payment/Print/" + idDoc,
                                    text = "Recibo " + "A@" + (Convert.ToInt32(nomina.document) - 1)
                                });
                            }
                            db.SaveChanges();

                        }

                        idEcomomics.AddRange(idEcomomicsC);
                        idEcomomics.AddRange(idEcomomicsT);
                        idEcomomics.AddRange(idEcomomicsO);

                        if (idEcomomics.Count > 0)
                        {


                            ResponseUploadNominaPayJson request = new ResponseUploadNominaPayJson();
                            request.status = 200;
                            request.listDoc = listDocument;
                            request.textError = infoData;

                            Api_StatusController apiStatus = new Api_StatusController();

                            db.SaveChanges();
                            Autorization.saveOperation(db, autorization, "Agregando pago nomina");
                            bool stautusReferee = apiStatus.statusWithoutAuth(nomina.NoColegiado.id);
                            transaction.Commit();
                            //transaction.Rollback();
                            return request;
                        }
                        else if (nomina.negatives)
                        {
                            ResponseUploadNominaPayJson request = new ResponseUploadNominaPayJson();
                            request.status = 200;
                            request.listDoc = listDocument;
                            request.textError = infoData;
                            Api_StatusController apiStatus = new Api_StatusController();
                            db.SaveChanges();
                            Autorization.saveOperation(db, autorization, "Agregando pago nomina");
                            bool stautusReferee = apiStatus.statusWithoutAuth(nomina.NoColegiado.id);
                            transaction.Commit();
                            //transaction.Rollback();
                            return request;
                        }
                        else
                        {
                            transaction.Rollback();
                            Debug.WriteLine("ningun campo ingresado o :: " + infoData + "  ::");
                            return new ResponseUploadNominaPayJson()
                            {
                                status = 400,
                                textError = "ningun campo ingresado o :: " + infoData + "  ::"
                            };
                        }

                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e);
                        transaction.Rollback();
                        Debug.WriteLine("Problemas generales, posiblemente tiene un error grabe en el listado por favor revisar");
                        return new ResponseUploadNominaPayJson()
                        {
                            status = 400,
                            textError = "Problemas generales, posiblemente tiene un error grabe en el listado por favor revisar"
                        };
                    }
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return new ResponseUploadNominaPayJson()
                {
                    status = 400,
                    textError = "Problemas generasles"
                };
            }

        }

        // PUT: api/Api_PayNomina/5
        public ResponseUploadNominaPayJson Put(int id, RequestUploadNominaJson nomina)
        {

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    int idPaytimbre = 130;
                    int idPayDemand = 131;
                    int idTypeEcoCuadre = Convert.ToInt32(GeneralFunction.getKeyTableVariable(db, "VarCuadrePago"));
                    string cent = GeneralFunction.getKeyTableVariable(db, Constant.table_var_aprox_decimal);
                    decimal dCent = Convert.ToDecimal(cent);
                    string infoData = "";

                    DateTime tempDateOfPay = ((nomina.dateOfPay == null) ?
                            DateTime.Today : nomina.dateOfPay.Value.Date);

                    string typeDocAccouting = db.ccee_Variables.Where(o => o.Var_Clave.Equals("VarReciboCol")).Select(p => p.Var_Valor).FirstOrDefault();
                    int typeDocAccoutingInt = Convert.ToInt32(typeDocAccouting);
                    ccee_TipoDocContable tempDocCon =
                        db.ccee_TipoDocContable.Find(typeDocAccoutingInt);
                    //ccee_DocContable tempDoc = new ccee_DocContable()
                    //{
                    //    DCo_ANombre = nomina.NoColegiado.name,
                    //    fk_TipoDocContable = tempDocCon.TDC_NoTipoDocContable 
                    //};
                    //db.ccee_DocContable.Add(tempDoc);

                    ccee_DocContable tempDoc = db.ccee_DocContable.Find(nomina.idDocAccount);
                    tempDoc.DCo_ANombre = nomina.NoColegiado.name;
                    tempDoc.fk_TipoDocContable = tempDocCon.TDC_NoTipoDocContable;
                    db.Entry(tempDoc).State = EntityState.Modified;

                    db.SaveChanges();

                    string autorization = Request.Headers.Authorization.Parameter;
                    if (!Autorization.enableAutorization(db, autorization)) return null;

                    List<ResponseEconomicJson> idEcomomics = new List<ResponseEconomicJson>();
                    List<ResponseEconomicJson> idEcomomicsC = createListEconomicRefereeBatch(id);
                    List<ResponseEconomicJson> idEcomomicsT = createListEconomicTimbreBatch(id);
                    List<ResponseEconomicJson> idEcomomicsO = createListEconomicOtherBatch(id);
                    
                    idEcomomics.AddRange(idEcomomicsC);
                    idEcomomics.AddRange(idEcomomicsT);
                    idEcomomics.AddRange(idEcomomicsO);

                    ccee_Cajero cahier = db.ccee_Cajero.Find(nomina.idCashier);

                    if (idEcomomics.Count > 0)
                    {

                        var list = (from val in idEcomomics
                                    where val.idTypeEconomic == idPaytimbre || val.idTypeEconomic == idPayDemand
                                    select val).ToList();
                        var listG = (from val in idEcomomics
                                     where val.idTypeEconomic != idPaytimbre && val.idTypeEconomic != idPayDemand
                                     select val).ToList();

                        decimal totalGeneral = listG.Sum(p => p.amount);
                        decimal total = list.Sum(p => p.amount);
                        decimal totalAprox = GeneralFunction.aproxDecimal(total, dCent);
                        decimal residuo = totalAprox - total;
                        if (residuo > 0)
                        {

                            ccee_Pago tempPay = new ccee_Pago()
                            {
                                Pag_Monto = 0,
                                Pag_Observacion = "Pago en nomina",
                                Pag_Fecha = tempDateOfPay,
                                fk_Caja = nomina.idCash,
                                fk_Cajero = nomina.idCashier
                            };
                            db.ccee_Pago.Add(tempPay);
                            db.SaveChanges();

                            ccee_CargoEconomico_timbre tempEcoTimbre = new ccee_CargoEconomico_timbre();
                            tempEcoTimbre.CEcT_Monto = residuo;
                            tempEcoTimbre.CEcT_FechaGeneracion = DateTime.Now;
                            tempEcoTimbre.fk_NoColegiado = nomina.NoColegiado.id;
                            tempEcoTimbre.fk_TipoCargoEconomico = idTypeEcoCuadre;
                            tempEcoTimbre.fk_Pago = tempPay.Pag_NoPago;
                            tempEcoTimbre.fk_DocContable = tempDoc.DCo_NoDocContable;
                            db.ccee_CargoEconomico_timbre.Add(tempEcoTimbre);
                            db.SaveChanges();
                            ResponseEconomicJson respO = new ResponseEconomicJson()
                            {
                                id = tempEcoTimbre.CEcT_NoCargoEconomico,
                                amount = tempEcoTimbre.CEcT_Monto.Value,
                                idTypeEconomic = 9997,
                                enable = 1,
                                description = "Por compensacion de timbres"
                            };
                            idEcomomics.Add(respO);
                            //idEcomomicsO.Add(respO);

                            tempEcoTimbre = new ccee_CargoEconomico_timbre();
                            tempEcoTimbre.CEcT_Monto = residuo * -1;
                            tempEcoTimbre.CEcT_FechaGeneracion = DateTime.Now;
                            tempEcoTimbre.fk_NoColegiado = nomina.NoColegiado.id;
                            tempEcoTimbre.fk_TipoCargoEconomico = idTypeEcoCuadre;
                            tempEcoTimbre.fk_Pago = tempPay.Pag_NoPago;
                            tempEcoTimbre.fk_DocContable = tempDoc.DCo_NoDocContable;
                            db.ccee_CargoEconomico_timbre.Add(tempEcoTimbre);
                            db.SaveChanges();
                            respO = new ResponseEconomicJson()
                            {
                                id = tempEcoTimbre.CEcT_NoCargoEconomico,
                                amount = tempEcoTimbre.CEcT_Monto.Value,
                                idTypeEconomic = 9998,
                                enable = 1,
                                description = "Por compensacion de timbres"
                            };
                            idEcomomics.Add(respO);
                            //idEcomomicsO.Add(respO);

                            ccee_MedioPago_Pago_C tempPaymentMethodPay;
                            foreach (RequestPaymentMethod_PayJson paymentMethod in nomina.jsonContentMethod)
                            {
                                tempPaymentMethodPay = new ccee_MedioPago_Pago_C()
                                {
                                    MPP_Monto = 0,
                                    MPP_NoTransaccion = paymentMethod.noTransaction,
                                    MPP_NoDocumento = paymentMethod.noDocument,
                                    MPP_Observacion = paymentMethod.remarke,
                                    MPP_Reserva = paymentMethod.reservation,
                                    MPP_Rechazado = paymentMethod.rejection,
                                    MPP_FechaHora = DateTime.Now,
                                    MPP_EsNomina = 1,
                                    fk_MedioPago = paymentMethod.idPaymentMethod,
                                    fk_Pago = tempPay.Pag_NoPago,
                                    fk_Banco = paymentMethod.idBank
                                };
                                db.ccee_MedioPago_Pago_C.Add(tempPaymentMethodPay);
                                tempPaymentMethodPay = null;
                            }

                        }


                        int idDoc = createDocumentAccunting(nomina.NoColegiado.id, cahier,
                            nomina.NoColegiado.name + " " + nomina.NoColegiado.lastname, nomina.NoColegiado.address,
                            nomina.NoColegiado.nit, "", tempDocCon, idEcomomicsC, idEcomomicsT, idEcomomicsO, nomina.document, tempDoc,
                            nomina.idType, nomina.NoDocument, nomina.textOfEconomic, tempDateOfPay, nomina.jsonContentMethod);


                        var listTimbres = (from tim in db.ccee_Timbre
                                           join hoj in db.ccee_HojaTimbre on tim.fk_HojaTimbre equals hoj.HoT_NoHojaTimbre
                                           join val in db.ccee_ValorTimbre on tim.fk_ValorTimbre equals val.VaT_NoValorTimbre
                                           where tim.Tim_Estatus == Constant.timbre_status_enable &&
                                           hoj.fk_Cajero == nomina.idCashier
                                           select new
                                           {
                                               valor = val.VaT_Denominacion,
                                               hoja = hoj.HoT_NoHojaTimbre,
                                               numeroHoja = hoj.HoT_NumeroHoja,
                                               timbre = tim.Tim_NumeroTimbre,
                                               id = tim.Tim_NoTimbre
                                           }).ToList();

                        //listDocument.Add("/Payment/Print/" + idDoc);

                        listTimbres = listTimbres.OrderByDescending(p => p.valor)
                                                    .ThenBy(o => o.id).ToList();
                        string timbresToPay = "";
                        decimal calculationTimbre = totalAprox;
                        for (int i = 0; i < listTimbres.Count; i++)
                        {
                            if (calculationTimbre == 0) break;
                            if (calculationTimbre >= listTimbres[i].valor)
                            {
                                timbresToPay += "H) " + listTimbres[i].numeroHoja + " - T) " + listTimbres[i].timbre + " , ";
                                calculationTimbre = calculationTimbre - listTimbres[i].valor.Value;
                                ccee_Timbre timbre = db.ccee_Timbre.Find(listTimbres[i].id);
                                timbre.Tim_Estatus = Constant.timbre_status_sold;
                                timbre.Tim_Fecha = DateTime.Today;
                                timbre.fk_Nomina = nomina.idDocAccount;
                                db.Entry(timbre).State = EntityState.Modified;
                            }
                        }

                        if (calculationTimbre > 0)
                        {
                            timbresToPay = "Timbres insuficientes";
                            transaction.Rollback();
                            return new ResponseUploadNominaPayJson()
                            {
                                status = 400,
                                textError = "Timbres insuficientes"
                            };
                        }

                        ResponseEconomicJson info = new ResponseEconomicJson()
                        {
                            id = 0,
                            amount = 0,
                            idTypeEconomic = 0,
                            idNoReferee = 0,
                            enable = 1,
                            description = infoData,
                            dateCreated = null
                        };
                        idEcomomics.Add(info);

                        //ccee_Batch tempBatch = new ccee_Batch();
                        ccee_Batch tempBatch = db.ccee_Batch.Find(id);

                        tempBatch.fk_DocContable = idDoc;
                        tempBatch.fk_Cajero = nomina.idCashier;
                        //tempBatch.Bat_ContenidoEntrada = getTextInit(nomina.data);
                        tempBatch.Bar_ContenidoSalida = getTextFinal(null, idEcomomics, timbresToPay);
                        db.Entry(tempBatch).State = EntityState.Modified;
                        //db.ccee_Batch.Add(tempBatch);

                        ResponseUploadNominaPayJson request = new ResponseUploadNominaPayJson();
                        request.status = 200;
                        request.timbres = timbresToPay;
                        request.total = totalAprox + totalGeneral;
                        request.extra = residuo;
                        request.recibo = "/Payment/Print/" + idDoc;
                        //request.listDoc = listDocument;
                        request.textError = infoData;


                        db.SaveChanges();
                        Autorization.saveOperation(db, autorization, "Agregando pago nomina");
                        transaction.Commit();
                        //transaction.Rollback();
                        return request;
                    }
                    else
                    {
                        transaction.Rollback();
                        Debug.WriteLine("ningun campo ingresado o :: " + infoData + "  ::");
                        return new ResponseUploadNominaPayJson()
                        {
                            status = 400,
                            textError = "ningun campo ingresado o :: " + infoData + "  ::"
                        };
                    }


                }
                catch (Exception)
                {
                    transaction.Rollback();
                    Debug.WriteLine("ningun campo ingresado o :: " + "" + "  ::");
                    return new ResponseUploadNominaPayJson()
                    {
                        status = 400,
                        textError = "ningun campo ingresado o :: " + "" + "  ::"
                    };
                }
            }
            
        }

        // DELETE: api/Api_PayNomina/5
        public void Delete(int id)
        {
        }


        private List<ccee_TipoCargoEconomico> generarMontoColegiado()
        {
            string cole = GeneralFunction.getKeyTableVariable(db, Constant.table_var_colegiado);
            int idEconomicReferee = Convert.ToInt32(cole);
            ccee_TipoCargoEconomico tempTypeEco = (from tca in db.ccee_TipoCargoEconomico
                                                   where tca.TCE_NoTipoCargoEconomico == idEconomicReferee
                                                   select tca).Single();

            string calculation = tempTypeEco.TCE_CondicionCalculo;
            string[] arrayCalculation = calculation.Split(',');
            List<int> intCalculation = new List<int>();
            foreach (string item in arrayCalculation)
            {
                intCalculation.Add(Convert.ToInt32(item));
            }

            var listE = from tca in db.ccee_TipoCargoEconomico
                        where intCalculation.Contains(tca.TCE_NoTipoCargoEconomico)
                        select tca;
            return listE.ToList();

        }

        private void updateSaldo(int idReferee,  decimal amountTimbre)
        {
            ccee_Colegiado tempCol = db.ccee_Colegiado.Find(idReferee);
            tempCol.Col_Salario = ((amountTimbre < 80) ? 8000 : amountTimbre * 100);
            db.Entry(tempCol).State = EntityState.Modified;
            db.SaveChanges();
        }

        private int createDocumentAccunting(int idNoReferee, ccee_Cajero tempCaj, string name, string address, string nit, string remark,
                                            ccee_TipoDocContable tempDocCon
                                            , List<ResponseEconomicJson> listEconomicC, List<ResponseEconomicJson> listEconomicT
                                            , List<ResponseEconomicJson> listEconomicO, string noDocument,
                                            ccee_DocContable tempDoc, string idType, string noDocu, string textGemeral,
                                            DateTime dateOfReceipt, IEnumerable<RequestPaymentMethod_PayJson> jsonContentMethod)
        {
            noDocument = "A@" + noDocument;
            string template = createReceipt(idNoReferee, name, tempCaj, remark,
                                                        tempDocCon, listEconomicC, listEconomicT,
                                                        listEconomicO, 
                                                        address, noDocument, idType, noDocu, textGemeral,
                                                        jsonContentMethod);
            
            tempDoc.DCo_Direccion = address;
            tempDoc.DCo_FechaHora = dateOfReceipt;
            tempDoc.DCo_Nit = nit;
            tempDoc.DCo_Observacion = remark;
            tempDoc.DCo_Xml = template;
            tempDoc.DCo_HashDoc = "";
            tempDoc.DCo_NoRecibo = noDocument.ToString();
            db.Entry(tempDoc).State = EntityState.Modified;
            db.SaveChanges();
            return tempDoc.DCo_NoDocContable;

        }


        private string createReceipt(int idNoReferee, string name, ccee_Cajero cashier, string remarke,
                                        ccee_TipoDocContable type, List<ResponseEconomicJson> listEconomicC, 
                                        List<ResponseEconomicJson> listEconomicT, List<ResponseEconomicJson> listEconomicO, 
                                        string address, string noDocumento,
                                        string idType, string noDocu, string textGeneral,
                                        IEnumerable<RequestPaymentMethod_PayJson> jsonContentMethod)
        {
            bool isCash = false;
            bool isOther = false;
            string cheque = "";
            string contentTypeEconomic = "";
            decimal totalEconomic = 0;
            string tupla = "";
            int idVerificaciorPay = 0;
            CultureInfo culture;
            culture = CultureInfo.CreateSpecificCulture("es-GT");
            string numberOther = "O";
            DateTime dateNowTime = DateTime.Today;

            int idTypeCash = Convert.ToInt32(db.ccee_Variables
                                            .Where(o => o.Var_Clave.Equals("esTipoEfectivo"))
                                            .Select(p => p.Var_Valor).FirstOrDefault());

            int idTypeCheque = Convert.ToInt32(db.ccee_Variables
                                            .Where(o => o.Var_Clave.Equals("esTipoCheque"))
                                            .Select(p => p.Var_Valor).FirstOrDefault());

            ccee_Usuario tempUser = db.ccee_Usuario.Find(cashier.fk_Usuario);
            
            List<ResponseEconomicJson> listTempEconomic = new List<ResponseEconomicJson>();

            listEconomicC = listEconomicC.Where(p => p.enable == 1).ToList();
            listEconomicT = listEconomicT.Where(p => p.enable == 1).ToList();
            listEconomicO = listEconomicO.Where(p => p.enable == 1).ToList();
            listTempEconomic.AddRange(unionList(listEconomicC, -2, "Cuota Colegiado (Q.22.00)"));
            //listTempEconomic.AddRange(unionList(listEconomicT, -1, "Cuota Timbre"));
            ResponseEconomicJson tempEco = listEconomicO.Where(p => p.idTypeEconomic == 9997).FirstOrDefault();
            if(tempEco != null)
            {
                tempEco.idReferee = 130;
                listEconomicT.Add(tempEco);
            }
            listEconomicO = listEconomicO.Where(p => p.idTypeEconomic != 9997).ToList();
            listTempEconomic.AddRange(listEconomicT);
            listTempEconomic.AddRange(listEconomicO);

            listTempEconomic = listTempEconomic.OrderBy(o => o.idTypeEconomic).ToList();

            //var select = (from r in listTempEconomic
            //             where r.enable == 1
            //             group r by new
            //             {
            //                 Category = r.idTypeEconomic
            //             } into g
            //             select new
            //             {
            //                 category = g.Key.Category,
            //                 Count = g.Count(),
            //                 Descripton = g.Select(y => y.description),
            //                 Sumatory = g.Sum(x => x.amount),
            //                 dateMin = g.Min(z => z.dateCreated),
            //                 dateMax = g.Max(z => z.dateCreated)
            //             }).ToList();

            var select = listTempEconomic
                .Where(p=>p.enable == 1)
                .GroupBy(o => new
                {
                    Category = o.idTypeEconomic
                })
                .Select(g => new 
                {
                    category = g.Key.Category,
                    Count = g.Count(),
                    Descripton = g.Select(y => y.description),
                    Sumatory = g.Sum(x => x.amount),
                    dateMin = g.Min(z => z.dateCreated),
                    dateMax = g.Max(z => z.dateCreated)
                })
                .ToList();


            foreach (var item in select)
            {
                string nameTypeEconomic = item.Descripton.FirstOrDefault();
                string datePrint = "";
                if (item.category == -2 || item.category == (Constant.type_economic_base)
                    || item.category == (Constant.type_economic_postumo))
                {
                    
                    if (item.dateMin.Equals(item.dateMax))
                    {
                        if (item.dateMin == null) continue;
                        datePrint = " de " + item.dateMin.Value.ToString("MMMM", culture) + " " + item.dateMin.Value.Year;
                    }
                    else
                    {
                        if (item.dateMin == null && item.dateMax == null) continue;
                        datePrint = " de " + item.dateMin.Value.ToString("MMMM", culture)
                            + " " + item.dateMin.Value.Year + " a " + item.dateMax.Value.ToString("MMMM", culture)
                            + " " + item.dateMax.Value.Year;
                    }

                    ;
                    //nameTypeEconomic += " " + dateNowTime.ToString("MMMM", culture) + " del " + dateNowTime.Year;

                }
                else if (item.category == Constant.type_economic_timbre_of_demand)
                {
                    DateTime dateNow = DateTime.Today;
                    datePrint = "Cuota timbre por la cantidad de Q." + Math.Round(item.Sumatory, 2) + " en " +
                                    dateNow.ToString("MMMM", culture) + "/" + dateNow.Year;
                    datePrint = "Cuota timbre por la cantidad de Q." + Math.Round(item.Sumatory, 2) + " en " + dateNowTime.ToString("MMMM", culture) + " del " + dateNowTime.Year;
                }
                nameTypeEconomic += ((textGeneral == null || textGeneral == "") ? datePrint : " " + textGeneral);

                tupla = Constant.html_pdf_remplace_tupla_type_economic.Replace(
                        Constant.html_pdf_remplace_type_economic, /*item.Count +*/ "   " + nameTypeEconomic);
                tupla = tupla.Replace(Constant.html_pdf_remplace_amount, item.Sumatory.ToString("F"));
                contentTypeEconomic += " " + tupla;
                totalEconomic += item.Sumatory;
            }

            var tempTypePay = from med in db.ccee_MedioPago
                              join pag in db.ccee_MedioPago_Pago on med.MedP_NoMedioPago equals pag.pkfk_MedioPago
                              join pgo in db.ccee_Pago on pag.pkfk_Pago equals pgo.Pag_NoPago
                              join eco in db.ccee_CargoEconomico on pgo.Pag_NoPago equals eco.fk_Pago
                              where eco.CEc_NoCargoEconomico == idVerificaciorPay
                              select new ModelAccountingDocument()
                              {
                                  medPayPay = pag,
                                  medPay = med
                              };

            foreach (RequestPaymentMethod_PayJson paymentMethod in jsonContentMethod)
            {

                ccee_MedioPago med = db.ccee_MedioPago.Find(paymentMethod.idPaymentMethod);
                ccee_BancoOEmisor banc = db.ccee_BancoOEmisor.Find(paymentMethod.idBank);

                switch (med.MdP_TipoMedio)
                {
                    case 0:
                        isCash = true;
                        break;
                    case 1:
                        cheque += " Cheque: " + paymentMethod.noDocument + " ";
                        numberOther = banc.BcE_NombreBancoEmisor;
                        break;

                    default:
                        cheque += " Bol: " + paymentMethod.noTransaction + " ";
                        numberOther += banc.BcE_NombreBancoEmisor + " ";
                        isOther = true;
                        break;

                }
            }
            

            string templateHTML = type.TDC_XmlPlantilla;
            //templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_date, GeneralFunction.dateNowFormatDateHour());
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_No_referree, idNoReferee.ToString());
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_name_referee, name);
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_remake, remarke);
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_type_economic_reciber, contentTypeEconomic);
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_total_letter, GeneralFunction.enletras(totalEconomic.ToString()));
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_total, totalEconomic.ToString("F"));
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_cash, ((idType == "0") ? "X" : "O"));
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_others, numberOther);
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_cheque, cheque);
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_address, address);
            templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_cashier, tempUser.Usu_Nombre + " " + tempUser.Usu_Apellido);
            //templateHTML = templateHTML.Replace(Constant.html_pdf_remplace_No_reciber, "" + noDocumento);
            return templateHTML;
        }


        private ResponseEconomicJson economicReferee(int month, int year, int referee, int? pay, 
            int? doc, ccee_TipoCargoEconomico typeEconomic, int? idBatch)
        {
            ccee_CargoEconomico_colegiado temCol = new ccee_CargoEconomico_colegiado();
            temCol.CEcC_Monto = typeEconomic.TCE_Valor;
            temCol.CEcC_FechaGeneracion = Convert.ToDateTime(month + "/" + 1 + "/" + year);
            temCol.fk_Colegiado = referee;
            temCol.fk_TipoCargoEconomico = typeEconomic.TCE_NoTipoCargoEconomico;
            temCol.fk_Pago = pay;
            temCol.fk_DocContable = doc;
            temCol.fk_Batch = idBatch;
            db.ccee_CargoEconomico_colegiado.Add(temCol);
            db.SaveChanges();
            ResponseEconomicJson temp = new ResponseEconomicJson()
            {
                id = temCol.CEcC_NoCargoEconomico,
                amount = temCol.CEcC_Monto.Value,
                idReferee = temCol.fk_Colegiado.Value,
                idTypeEconomic = temCol.fk_TipoCargoEconomico,
                enable = 1,
                description = typeEconomic.TCE_Descripcion,
                dateCreated = temCol.CEcC_FechaGeneracion
            };
            return temp;
        }

        private ResponseEconomicJson economicRefereeValue(int month, int year, int referee, int? pay,
            int? doc, ccee_TipoCargoEconomico typeEconomic, int? idBatch, decimal amount)
        {
            ccee_CargoEconomico_colegiado temCol = new ccee_CargoEconomico_colegiado();
            temCol.CEcC_Monto = amount;
            temCol.CEcC_FechaGeneracion = Convert.ToDateTime(month + "/" + 1 + "/" + year);
            temCol.fk_Colegiado = referee;
            temCol.fk_TipoCargoEconomico = typeEconomic.TCE_NoTipoCargoEconomico;
            temCol.fk_Pago = pay;
            temCol.fk_DocContable = doc;
            temCol.fk_Batch = idBatch;
            db.ccee_CargoEconomico_colegiado.Add(temCol);
            db.SaveChanges();
            ResponseEconomicJson temp = new ResponseEconomicJson()
            {
                id = temCol.CEcC_NoCargoEconomico,
                amount = temCol.CEcC_Monto.Value,
                idReferee = temCol.fk_Colegiado.Value,
                idTypeEconomic = temCol.fk_TipoCargoEconomico,
                enable = 1,
                description = typeEconomic.TCE_Descripcion,
                dateCreated = temCol.CEcC_FechaGeneracion
            };
            return temp;
        }

        private ResponseEconomicJson economicRefereeNegative(int month, int year, int referee, int pay,
            int doc, ccee_TipoCargoEconomico typeEconomic, int idBatch, decimal? amount)
        {
            ccee_CargoEconomico_colegiado temCol = new ccee_CargoEconomico_colegiado();
            temCol.CEcC_Monto = amount;
            temCol.CEcC_FechaGeneracion = Convert.ToDateTime(month + "/" + 1 + "/" + year);
            temCol.fk_Colegiado = referee;
            temCol.fk_TipoCargoEconomico = typeEconomic.TCE_NoTipoCargoEconomico;
            temCol.fk_Pago = pay;
            temCol.fk_DocContable = doc;
            temCol.fk_Batch = idBatch;
            db.ccee_CargoEconomico_colegiado.Add(temCol);
            db.SaveChanges();
            ResponseEconomicJson temp = new ResponseEconomicJson()
            {
                id = temCol.CEcC_NoCargoEconomico,
                amount = temCol.CEcC_Monto.Value,
                idReferee = temCol.fk_Colegiado.Value,
                idTypeEconomic = temCol.fk_TipoCargoEconomico,
                enable = 1,
                description = typeEconomic.TCE_Descripcion,
                dateCreated = temCol.CEcC_FechaGeneracion
            };
            return temp;
        }

        private ResponseEconomicJson economicRefereeUpdate(int? doc, int? pay, ccee_CargoEconomico_colegiado cEco,
                                        bool status, decimal? amount, bool statusPay, int? idBatch)
        {
            ccee_TipoCargoEconomico temType = db.ccee_TipoCargoEconomico.Find(cEco.fk_TipoCargoEconomico);
            cEco.fk_DocContable = doc;
            cEco.fk_Batch = idBatch;
            //cEco.fk_Pago = null;//((status) ? null : pay)
            if (statusPay)
            {
                cEco.fk_Pago = null;
            }else
            {
                cEco.fk_Pago = pay;
            }
            if (status)
            {
                cEco.CEcC_Monto = cEco.CEcC_Monto + amount;
            }
            db.Entry(cEco).State = EntityState.Modified;
            ResponseEconomicJson temp = new ResponseEconomicJson()
            {
                id = cEco.CEcC_NoCargoEconomico,
                amount = cEco.CEcC_Monto.Value,
                idTypeEconomic = cEco.fk_TipoCargoEconomico,
                idReferee = cEco.fk_Colegiado.Value,
                enable = 1,
                description = temType.TCE_Descripcion,
                dateCreated = cEco.CEcC_FechaGeneracion
            };
            return temp;
        }

        private ResponseEconomicJson economicTimbre(decimal amount, int month, int year, int? doc,
            int referee, int? pay, bool enable, ccee_TipoCargoEconomico typeEconomic, int? idBatch)
        {
            ccee_CargoEconomico_timbre tempEcoTimbre = new ccee_CargoEconomico_timbre();
            tempEcoTimbre.CEcT_Monto = amount;
            tempEcoTimbre.CEcT_FechaGeneracion = Convert.ToDateTime(month + "/" + 1 + "/" + year);
            tempEcoTimbre.fk_DocContable = doc;
            tempEcoTimbre.fk_Colegiado = referee;
            tempEcoTimbre.fk_TipoCargoEconomico = typeEconomic.TCE_NoTipoCargoEconomico;
            tempEcoTimbre.fk_Pago = pay;
            tempEcoTimbre.fk_Batch = idBatch;
            db.ccee_CargoEconomico_timbre.Add(tempEcoTimbre);
            db.SaveChanges();
            ResponseEconomicJson temp = new ResponseEconomicJson()
            {
                id = tempEcoTimbre.CEcT_NoCargoEconomico,
                amount = tempEcoTimbre.CEcT_Monto.Value,
                idTypeEconomic = tempEcoTimbre.fk_TipoCargoEconomico,
                idReferee = tempEcoTimbre.fk_Colegiado.Value,
                enable = 1,
                description = typeEconomic.TCE_Descripcion,
                dateCreated = tempEcoTimbre.CEcT_FechaGeneracion
            };
            if (enable)
                updateSaldo(referee, tempEcoTimbre.CEcT_Monto.Value);
            return temp;
        }

        private ResponseEconomicJson economicTimbreUpdate(decimal amount, int pay, int doc, bool enable, int referee, 
            ccee_TipoCargoEconomico typeEconomic, ccee_CargoEconomico_timbre timbre, bool isPsotumo, int idBatch)
        {
            //if (enable)
            //    timbre.CEcT_Monto = ((timbre.CEcT_Monto.Value < 80) ? 80 : timbre.CEcT_Monto.Value);
            timbre.CEcT_Monto = amount;
            timbre.fk_Pago = pay;
            timbre.fk_DocContable = doc;
            timbre.fk_Batch = idBatch;
            db.Entry(timbre).State = EntityState.Modified;
            ResponseEconomicJson temp = new ResponseEconomicJson()
            {
                id = timbre.CEcT_NoCargoEconomico,
                amount = timbre.CEcT_Monto.Value,
                idTypeEconomic = timbre.fk_TipoCargoEconomico,
                idReferee = timbre.fk_Colegiado.Value,
                enable = 1,
                description = typeEconomic.TCE_Descripcion,
                dateCreated = timbre.CEcT_FechaGeneracion
            };

            if (enable && !isPsotumo)
                updateSaldo(referee, timbre.CEcT_Monto.Value);

            return temp;
        }

        private ResponseEconomicJson economicOther(decimal amount, int month, int year, int doc, 
            int referee, int pay, ccee_TipoCargoEconomico typeEconomic, int idBatch)
        {
            ccee_CargoEconomico_otros tempEcoOther = new ccee_CargoEconomico_otros();
            tempEcoOther.CEcO_Monto = amount;
            tempEcoOther.CEcO_FechaGeneracion = Convert.ToDateTime(month + "/" + 1 + "/" + year);
            tempEcoOther.fk_DocContable = doc;
            tempEcoOther.fk_Colegiado = referee;
            tempEcoOther.fk_TipoCargoEconomico = typeEconomic.TCE_NoTipoCargoEconomico;
            tempEcoOther.fk_Pago = pay;
            tempEcoOther.fk_Batch = idBatch;
            db.ccee_CargoEconomico_otros.Add(tempEcoOther);
            db.SaveChanges();
            ResponseEconomicJson temp = new ResponseEconomicJson()
            {
                id = tempEcoOther.CEcO_NoCargoEconomico,
                amount = tempEcoOther.CEcO_Monto.Value,
                idTypeEconomic = tempEcoOther.fk_TipoCargoEconomico,
                idReferee = tempEcoOther.fk_Colegiado.Value,
                enable = 1,
                description = typeEconomic.TCE_Descripcion,
                dateCreated = tempEcoOther.CEcO_FechaGeneracion
            };
            return temp;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public List<ResponseEconomicJson> unionList(List<ResponseEconomicJson> listEco, int typeEco, string title)
        {
            var model = listEco
                .GroupBy(o => new
                {
                    Referee = o.idReferee,
                    Month = o.dateCreated.Value.Month,
                    Year = o.dateCreated.Value.Year
                })
                .Select(g => new ResponseEconomicJson
                {
                    idTypeEconomic = typeEco,
                    description = title,
                    amount = g.Sum(p => p.amount),
                    enable = g.Select(o=>o.enable).First(),
                    dateCreated = g.Select(p=>p.dateCreated).FirstOrDefault()
                })
                .ToList();

            return model;
        }

        private List<ResponseEconomicJson> createListEconomicRefereeBatch(int idBatch)
        {
            var listEcoReferee = (from lis in db.ccee_CargoEconomico_colegiado
                              join tip in db.ccee_TipoCargoEconomico on lis.fk_TipoCargoEconomico equals tip.TCE_NoTipoCargoEconomico
                              where lis.fk_Batch == idBatch
                              select new ResponseEconomicJson()
                              {
                                  id = lis.CEcC_NoCargoEconomico,
                                  amount = lis.CEcC_Monto.Value,
                                  idTypeEconomic = lis.fk_TipoCargoEconomico,
                                  idReferee = lis.fk_Colegiado.Value,
                                  enable = 1,
                                  description = tip.TCE_Descripcion,
                                  dateCreated = lis.CEcC_FechaGeneracion

                              }).ToList();
            return listEcoReferee;
        }

        private List<ResponseEconomicJson> createListEconomicTimbreBatch(int idBach)
        {
            var listEcoTimbre = (from lis in db.ccee_CargoEconomico_timbre
                             join tip in db.ccee_TipoCargoEconomico on lis.fk_TipoCargoEconomico equals tip.TCE_NoTipoCargoEconomico
                             where lis.fk_Batch == idBach
                             select new ResponseEconomicJson()
                             {
                                 id = lis.CEcT_NoCargoEconomico,
                                 amount = lis.CEcT_Monto.Value,
                                 idTypeEconomic = lis.fk_TipoCargoEconomico,
                                 idReferee = lis.fk_Colegiado.Value,
                                 enable = 1,
                                 description = tip.TCE_Descripcion,
                                 dateCreated = lis.CEcT_FechaGeneracion

                             }).ToList();
            return listEcoTimbre;
        }

        private List<ResponseEconomicJson> createListEconomicOtherBatch(int idBach)
        {
            var listEcoOther = (from lis in db.ccee_CargoEconomico_otros
                            join tip in db.ccee_TipoCargoEconomico on lis.fk_TipoCargoEconomico equals tip.TCE_NoTipoCargoEconomico
                            where lis.fk_Batch == idBach
                            select new ResponseEconomicJson()
                            {
                                id = lis.CEcO_NoCargoEconomico,
                                amount = lis.CEcO_Monto.Value,
                                idTypeEconomic = lis.fk_TipoCargoEconomico,
                                idReferee = lis.fk_Colegiado.Value,
                                enable = 1,
                                description = tip.TCE_Descripcion,
                                dateCreated = lis.CEcO_FechaGeneracion

                            }).ToList();
            return listEcoOther;
        }


        public string getTextFinal(List<ResponseUploadNominaJson> errorNomina, 
            List<ResponseEconomicJson> idEcomomics, string timbresToPay)
        {
            string textTotal = "";


            if (errorNomina != null)
            {
                foreach (ResponseUploadNominaJson item in errorNomina)
                {
                    textTotal += item.colegiado + "," + item.cuotaColegiado + "," + item.cuotaTimbre + "," + item.seguroPostumo + ",no cargado\n\r";
                }
            }

            foreach (ResponseEconomicJson item in idEcomomics)
            {
                textTotal += item.id + "," + item.idReferee + "," + item.description + "," + item.amount + "\n\r";
            }

            textTotal += timbresToPay;

            return textTotal;
        }

        public string getTextInit(List<ResponseUploadNominaJson> data)
        {
            string textTotal = "";
            foreach (ResponseUploadNominaJson item in data)
            {
                textTotal += item.colegiado + "," + item.cuotaColegiado + "," + item.cuotaTimbre + "," + item.seguroPostumo + "," + item.anio + "\n\r";
            }
            return textTotal;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonFichaBene
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string parentesco { get; set; }
        public string telefono { get; set; }
    }
}
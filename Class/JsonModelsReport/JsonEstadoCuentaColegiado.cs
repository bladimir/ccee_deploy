﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModelsReport
{
    public class JsonEstadoCuentaColegiado
    {
        public int Month { get; set; }
        public string mes {
            get
            {
                CultureInfo culture;
                culture = CultureInfo.CreateSpecificCulture("es-GT");
                return culture.DateTimeFormat.GetMonthName(this.Month);
            }
        }
        public int anio { get; set; }
        private decimal? tempMonto { get; set; }
        public decimal? monto {
            get
            {
                
                return ((tempMonto == null) ? 0 : Math.Round(tempMonto.Value, 2));
            }
            set { tempMonto = value; }
        }
        public string tipoPago { get; set; }
        public DateTime? tempfechaPago { get; set; }
        public string fechaPago {
            get
            {
                return (tempfechaPago == null) ? "" : tempfechaPago.Value.ToString("dd/MM/yyyy");
            }
        }
        public string recibo { get; set; }
        public int? idRecibo { get; set; }
        public string url { get; set; }
        public int? count { get; set; }
        public decimal? multiplicacion { get; set; }

        public DateTime? tempfechaReceipt { get; set; }
        public string fechaReceip
        {
            get
            {
                return (tempfechaReceipt == null) ? "" : tempfechaReceipt.Value.ToString("dd/MM/yyyy");
            }
        }
        public int rechazado { get; set; }
        public int? congelado { get; set; }
    }
}
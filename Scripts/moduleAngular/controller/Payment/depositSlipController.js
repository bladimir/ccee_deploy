﻿(function () {
    angular.module('payment')
        .controller('depositSlip', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'paymentFactory', function ($scope, $http, CONSTANT, $anchorScroll, paymentFactory) {
            $scope.listData;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idUpdate = 0;
            $scope.objectUpdate;
            $scope.listDataBank;
            $scope.listDataCashier = [];
            $scope.listDataNoNull = [];
            $scope.listDataNulls = [];
            $scope.listDataForCashier = [];
            $scope.statusDeposit = "";
            $scope.idDeposit = 0;
            $scope.tempCashier = 0;
            $scope.REGEX_PHONE = CONSTANT.REGEX_PHONE;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                paymentFactory.actionGetFunction(CONSTANT.URL_API_BANKACCOUNT)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listDataBank = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });

                paymentFactory.actionGetFunction(CONSTANT.URL_API_CASHIER)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);

                        if ($scope.tempCashier != 0) {
                            var len = response.data.length;
                            for (var i = 0; i < len; i++) {
                                if (response.data[i].id == $scope.tempCashier) {
                                    $scope.listDataCashier.push(response.data[i]);
                                }
                            }
                        } else {
                            $scope.listDataCashier = response.data;
                        }

                        
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }


            $scope.setAuth = function (cashier) {
                paymentFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                    .then(function (response) {
                        paymentFactory.setAuthorization(response.data);
                        $scope.init();
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    });
            }

            $scope.setCashier = function (cashier) {
                $scope.tempCashier = cashier;
            }

            $scope.getListAccounts = function () {
                $scope.idDeposit = 0;
                $scope.listDataNoNull = [];
                $scope.listDataNulls = [];
                $scope.getListNulls();
                $scope.geIdNoNull();
            }

            $scope.getListNulls = function () {
                $scope.showLoadingFunction();
                var date = $scope.dateDeposit.getFullYear() + "-" +
                            ($scope.dateDeposit.getMonth() + 1) + "-" +
                            $scope.dateDeposit.getDate();

                var dateR;
                if ($scope.dateDepositR == null || $scope.dateDepositR == "") {
                    dateR = date;
                } else {
                    dateR = $scope.dateDepositR.getFullYear() + "-" +
                            ($scope.dateDepositR.getMonth() + 1) + "-" +
                            $scope.dateDepositR.getDate();
                }

                console.log(date);
                var idC = $scope.cashierDeposit.id;
                paymentFactory.actionGetGetGetGetGetIntFunction(CONSTANT.URL_API_DEPOSITSLIP, date, idC, 0, dateR, 0)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        $scope.listDataNulls = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.geIdNoNull = function () { 
                var date = $scope.dateDeposit.getFullYear() + "-" +
                            ($scope.dateDeposit.getMonth() + 1) + "-" +
                            $scope.dateDeposit.getDate();
                

                console.log(date);
                var idC = $scope.cashierDeposit.id;
                var idB = $scope.bankDeposit.id;
                var boleta = $scope.boletaDeposit; 
                paymentFactory.actionGetGetGetGetIntFunction(CONSTANT.URL_API_DEPOSITSLIP, idC, idB, boleta, date)
                    .then(function (response) {
                        if (response.data > 0) {
                            $scope.getListNoNull(response.data);
                            $scope.idDeposit = response.data;
                            $scope.noDepositSlip = response.data;
                            console.log($scope.idDeposit);
                        }
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getListNoNull = function (idDeposit) {
                paymentFactory.actionGetGetIntFunction(CONSTANT.URL_API_DEPOSITSLIP, idDeposit, 1)
                    .then(function (response) {
                        console.log(response);
                        $scope.listDataNoNull = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getForCashier = function () {
                var date = $scope.dateDeposit.getFullYear() + "-" +
                            ($scope.dateDeposit.getMonth() + 1) + "-" +
                            $scope.dateDeposit.getDate();

                console.log(date);
                var idC = $scope.cashierDeposit.id;
                paymentFactory.actionGetGetGetIntFunction(CONSTANT.URL_API_DEPOSITSLIP, date, idC, 0)
                    .then(function (response) {
                        $scope.listDataForCashier = response.data;
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.getNumerDeposit = function () {
                
                var idDeposit = $scope.searchNoDeposit;
                paymentFactory.actionGetIntFunction(CONSTANT.URL_API_DEPOSITSLIP, idDeposit)
                    .then(function (response) {
                        console.log(response)
                        if (response.data != null) {
                            var dataResponse = response.data;
                            $scope.boletaDeposit = dataResponse.boleta;
                            for (var i = 0; i < $scope.listDataBank.length; i++) {
                                if ($scope.listDataBank[i].id == dataResponse.idBank) {
                                    $scope.bankDeposit = $scope.listDataBank[i];
                                    break;
                                }
                            }
                            for (var i = 0; i < $scope.listDataCashier.length; i++) {
                                if ($scope.listDataCashier[i].id == dataResponse.idCahier) {
                                    $scope.cashierDeposit = $scope.listDataCashier[i];
                                    break;
                                }
                            }

                            $scope.dateDeposit = new Date(dataResponse.date);
                            $scope.getListAccounts();
                        }
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.selectBoleta = function () {
                $scope.boletaDeposit = $scope.cashierAllDeposit.boleta;
                for (var i = 0; i < $scope.listDataBank.length; i++) {
                    if ($scope.listDataBank[i].id == $scope.cashierAllDeposit.idBank) {
                        $scope.bankDeposit = $scope.listDataBank[i];
                        break;
                    }
                }
                $scope.getListAccounts();
            }

            $scope.addListNoNull = function (objectNull) {
                $scope.listDataNoNull.push(objectNull);
                var pos = $scope.listDataNulls.indexOf(objectNull);
                $scope.listDataNulls.splice(pos, 1);
            }

            $scope.removeListNoNull = function (objectNoNull) {
                $scope.listDataNulls.push(objectNoNull);
                var pos = $scope.listDataNoNull.indexOf(objectNoNull);
                $scope.listDataNoNull.splice(pos, 1);
            }

            $scope.addAllListNoNull = function () {
                var len = $scope.listDataNulls.length;
                for (var i = 0; i < len; i++) {
                    $scope.listDataNoNull.push($scope.listDataNulls[i]);
                }
                $scope.listDataNulls = [];
            }

            $scope.removeAllListNoNull = function () {
                var len = $scope.listDataNoNull.length;
                for (var i = 0; i < len; i++) {
                    $scope.listDataNulls.push($scope.listDataNoNull[i]);
                }
                $scope.listDataNoNull = [];
            }

            
            $scope.sabeChange = function () {
                $scope.statusDeposit = "";
                var date = $scope.dateDeposit.getFullYear() + "-" +
                            ($scope.dateDeposit.getMonth() + 1) + "-" +
                            $scope.dateDeposit.getDate();
                $scope.showLoadingFunction();
                var information = {
                    idCahier: $scope.cashierDeposit.id,
                    idBank: $scope.bankDeposit.id,
                    date: date,
                    boleta: $scope.boletaDeposit
                }

                console.log(information);
                paymentFactory.actionPostFunction(CONSTANT.URL_API_DEPOSITSLIP, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data > 0) {
                                console.log(response.data);
                                $scope.changeList(response.data);
                            } else {
                                $scope.statusDeposit = "Error de creacion";
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                            $scope.statusDeposit = "Error de conexion";
                        });
            }


            $scope.changeList = function (idDeposit) {
                $scope.showLoadingFunction();
                var information = {
                    listNulls: $scope.listDataNulls,
                    listNoNulls: $scope.listDataNoNull
                }
                paymentFactory.actionPutFunction(CONSTANT.URL_API_DEPOSITSLIP, idDeposit, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.statusDeposit = "Completado";
                        } else {
                            $scope.statusDeposit = "Error en la actualizacion";
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                        $scope.statusDeposit = "Error de conexion";
                    });
            }

            $scope.deteleDeposit = function () {
                $scope.statusDeposit = "";
                $scope.showLoadingFunction();
                paymentFactory.actionDeleteFunction(CONSTANT.URL_API_DEPOSITSLIP, $scope.idDeposit)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data == 200) {
                                $scope.statusDeposit = "Eliminado";
                            } else {
                                $scope.statusDeposit = "Error en la eliminacion";
                            }
                        }, function (error) {
                            console.log(error);
                            $scope.hideLoadingFunction();
                            $scope.statusDeposit = "Error de conexion";
                        });
            }


            



        }]);

})()
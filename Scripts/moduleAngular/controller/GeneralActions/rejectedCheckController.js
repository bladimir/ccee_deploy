﻿(function () {
    angular.module('generals')
        .controller('rejected', ['$scope', '$http', 'CONSTANT', '$anchorScroll', 'generalactionsFactory', function ($scope, $http, CONSTANT, $anchorScroll, generalactionsFactory) {
            $scope.listBank;
            $scope.listCashier;
            $scope.showDelete = false;
            $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            $scope.idUpdate = 0;
            $scope.regexNumber = CONSTANT.REGEX_DIGIT;
            $scope.regexAmount = CONSTANT.REGEX_AMOUNT;
            $scope.numberPageText;
            $scope.objectCheck;
            $scope.visible = 1;
            $scope.aumontoPaymentMethod = 0;
            $scope.listPaymentMethod;
            $scope.listPaymentMethodPay = [];
            $scope.payFrozen = null;
            $scope.idPayMethodCash;

            $scope.showLoadingFunction = function () {
                $anchorScroll();
                $scope.showNotify = CONSTANT.NOTIFY_SHOW_CSS;
            }

            $scope.hideLoadingFunction = function () {
                $scope.showNotify = CONSTANT.NOTIFY_HIDE_CSS;
            }

            $scope.init = function () {
                $scope.getIssuingbank();
                $scope.getPaymentMethod();
                $scope.getIdPayMethodCash();
            }


            $scope.setAuth = function () {
                generalactionsFactory.actionGetFunction(CONSTANT.URL_GET_TOKEN)
                .then(function (response) {
                    generalactionsFactory.setAuthorization(response.data);
                    $scope.init();
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }

            $scope.getIssuingbank = function(){
                generalactionsFactory.actionGetFunction(CONSTANT.URL_API_ISSUINGBANK)
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    console.log(response);
                    $scope.listBank = response.data;
                }, function (error) {
                    $scope.hideLoadingFunction();
                    console.log(error);
                });
            }
            

            $scope.getPaymentMethod = function () {
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_PAYMENTMETHOD, 40)
                .then(function (response) {
                    $scope.listPaymentMethod = response.data;
                    $scope.hideLoadingFunction();
                }, function (error) {
                    console.log(error);
                    $scope.hideLoadingFunction();
                });
            }

            $scope.searchOnlyCheck = function (check) {
                $scope.cleanData();
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_REJECTEDCHECK, check.MPP_NoMedioPP)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        if (response.data != null) {
                            $scope.objectCheck = response.data;
                            $scope.rejAmount = response.data.MPP_Monto;
                            $scope.rejDate = response.data.MPP_FechaHora;
                            $scope.rejRejected = response.data.MPP_Rechazado;
                            $scope.rejReser = response.data.MPP_Reserva;
                            $scope.listData = response.data.ecomonics;
                            $scope.visible = 2;
                            $scope.aumontoPaymentMethod = response.data.MPP_Monto;
                            $scope.payFrozen = response.data.payFrozen;
                            $scope.showCheck = true;
                            $scope.totalEconomic($scope.listData);
                        }
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }


            $scope.searchCheck = function () {
                $scope.cleanData();
                $scope.showLoadingFunction();
                generalactionsFactory.actionGetGetIntFunction(CONSTANT.URL_API_REJECTEDCHECK, $scope.numberDoc, $scope.selectBanck.id)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        console.log(response);
                        if (response.data != null) {
                            $scope.listDataCheks = response.data;
                        }
                    }, function (error) {
                        $scope.hideLoadingFunction();
                        console.log(error);
                    })
            }

            $scope.totalEconomic = function (list) {
                var sum = 0;
                for (var i = 0; i < list.length; i++) {
                    sum += list[i].amount;
                }
                $scope.totalEconomics = sum;
            }

            $scope.update = function () {
                $scope.showLoadingFunction();
                var information = {
                    type: $scope.selectType,
                    objectCheck: $scope.objectCheck
                }
                generalactionsFactory.actionPutFunction(CONSTANT.URL_API_REJECTEDCHECK, 0, information)
                    .then(function (response) {
                        $scope.hideLoadingFunction();
                        if (response.data == 200) {
                            $scope.searchBank();
                        }
                    }, function (error) {
                        console.log(error);
                        $scope.hideLoadingFunction();
                    });

            }


            $scope.addMethodPay = function () {
                $scope.showLoadingFunction();
                var information = {
                    objectCheck: $scope.objectCheck,
                    jsonContentMethod: $scope.listPaymentMethodPay
                }
                console.log(information);
                generalactionsFactory.actionPostFunction(CONSTANT.URL_API_REFUNDPAYMENT, information)
                        .then(function (response) {
                            $scope.hideLoadingFunction();
                            if (response.data != null) {
                                console.log(response.data);
                                $scope.searchBank();
                            }

                        }, function (error) {
                            console.log(error);
                            $scope.idPay = 0;
                            $scope.hideLoadingFunction();
                        });
            }


            $scope.changeReservation = function (state) {
                //label label-default
                if (state == 0) {
                    $scope.rejReserColor = "label label-success";
                    return "Aceptado";
                } else if (state == 1) {
                    $scope.rejReserColor = "label label-warning";
                    return "En reserva";
                } else if (state == 2) {
                    $scope.rejReserColor = "label label-danger";
                    return "Rechazado";
                } else {
                    $scope.rejReserColor = "label label-primary";
                    return "No definido"
                }
            }

            $scope.changeRejection = function (state) {
                if (state == 0) {
                    return "No";
                } else if (state == 1) {
                    return "Si";
                } else {
                    return "No definido"
                }
            }


            $scope.cleanData = function () {
                $scope.idUpdate = 0;
                $scope.numberPageText = "";
            }

            $scope.getIdPayMethodCash = function () {
                generalactionsFactory.actionGetIntFunction(CONSTANT.URL_API_VARS, "CCEEMedioPago")
                .then(function (response) {
                    $scope.hideLoadingFunction();
                    $scope.idPayMethodCash = response.data;
                    console.log(response.data);
                }, function (error) {
                    console.log(error);
                    $scope.hideLoadingFunction();
                });
            }

            $scope.addListMethodCash = function () {
                if ($scope.amountpayMethod <= $scope.aumontoPaymentMethod && $scope.aumontoPaymentMethod > 0) {
                    var paymentMethod = {
                        amount: parseFloat($scope.amountpayMethod),
                        noTransaction: 0,
                        noDocument: 0,
                        reservation: 0,
                        rejection: 0,
                        remarke: "",
                        idBank: 40,
                        idPaymentMethod: $scope.idPayMethodCash
                    };
                    $scope.listPaymentMethodPay.push(paymentMethod);
                    $scope.aumontoPaymentMethod = parseFloat($scope.aumontoPaymentMethod) - parseFloat($scope.amountpayMethod);
                    $scope.amountpayMethod = "";
                    $scope.numberpayTrans = "";
                    $scope.numberpayDoc = "";
                    $scope.alertamountPayment = "";
                } else {
                    $scope.alertamountPayment = ": El monto no es permitido";
                }
            }

            $scope.addListMethod = function () {
                var idMethodPay = 0;
                var len = $scope.listPaymentMethod.length;
                for (var i = 0; i < len; i++) {
                    var type = $scope.listPaymentMethod[i].typeMethod;
                    if (type == $scope.radioPaymentMethod) {
                        idMethodPay = $scope.listPaymentMethod[i].id;
                        break;
                    }
                }

                console.log(idMethodPay);

                if ($scope.amountpayMethod <= $scope.aumontoPaymentMethod && $scope.aumontoPaymentMethod > 0) {
                    var paymentMethod = {
                        amount: parseFloat($scope.amountpayMethod),
                        noTransaction: $scope.numberpayTrans,
                        noDocument: $scope.numberpayDoc,
                        reservation: $scope.selectReservation,
                        rejection: 0,
                        remarke: $scope.remakepayMothod,
                        idBank: $scope.selectIssuingBank.id,
                        idPaymentMethod: idMethodPay
                    };
                    $scope.listPaymentMethodPay.push(paymentMethod);
                    $scope.aumontoPaymentMethod = parseFloat($scope.aumontoPaymentMethod) - parseFloat($scope.amountpayMethod);
                    $scope.amountpayMethod = "";
                    $scope.numberpayTrans = "";
                    $scope.numberpayDoc = "";
                    $scope.alertamountPayment = "";
                    $scope.viewBank = false;
                } else {
                    $scope.alertamountPayment = ": El monto no es permitido";
                }
            }

            $scope.deleteListMethod = function (data) {
                var pos = $scope.listPaymentMethodPay.indexOf(data);
                console.log(data);
                $scope.aumontoPaymentMethod = parseFloat($scope.aumontoPaymentMethod) + data.amount;
                $scope.listPaymentMethodPay.splice(pos, 1);
            }

            $scope.cleanData = function () {
                $scope.listPaymentMethodPay = [];
                $scope.payFrozen = null;
                $scope.visible = 1;
                $scope.showMedi = false
            }

            $scope.cancelMethodPay = function () {
                $scope.listPaymentMethodPay = [];
            }

        }]);

})()
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.Employment
{
    public class RequestEmplymentJson
    {
        public Nullable<DateTime> datestart { get; set; }
        public Nullable<DateTime> datefinish { get; set; }
        public string empleador { get; set; }
        public string puesto { get; set; }
        public int phone { get; set; }
        public string address { get; set; }
        public int idReferee { get; set; }
        public bool? actual { get; set; }
        public decimal? salario { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using webcee.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels._Tools;
using webcee.Class.JsonModels.AccountingDocument;
using webcee.Class.JsonModels.Economic;
using webcee.Class.JsonModels.PaymentMethod_Pay;
using webcee.Models;
using webcee.ViewModels;

namespace webcee.Controllers
{
    public class WS2_BancoOtrosController : ApiController
    {
        private CCEEEntities db = new CCEEEntities();
        public class ResponsePago
        {
            public int status { get; set; }
            public int idPago { get; set; }
            public string msg { get; set; }
        }
        public class Metodo
        {
            public decimal monto { get; set; }
            public string noTransaccion { get; set; }
            public string noDocumento { get; set; }
            public string observaciones { get; set; }
            public int idBanco { get; set; }
            public int idMetodoPago { get; set; }
        }

        public class RequestPay
        {
            public int colegiado { get; set; }
            public int idCajero { get; set; }
            public int idCaja { get; set; }
            public Metodo metodoPago { get; set; }
            public IEnumerable<int> typeEconomic { get; set; }
            public int noDoc { get; set; }

        }


        // GET: api/WS2_BancoOtros
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/WS2_BancoOtros/5
        public string Get(int id)
        {
            return "value";
        }

        private Tuple<List<ResponseEconomicJson>, ResponsePago, decimal> addCargosEconomicos(DbContextTransaction transaction, RequestPay requestPago)
        {
            decimal totalPayCargo = 0;
            List<ResponseEconomicJson> listaCargosGuardados = new List<ResponseEconomicJson>();
            // Crear los cargos economicos
            foreach (int idTipo in requestPago.typeEconomic)
            {
                // Ir a traer el valor del tipo economico a la BD
                ccee_TipoCargoEconomico tipoCargo = db.ccee_TipoCargoEconomico.Find(idTipo);
                // Regresar error si no hay un tipo asociado en la BD
                if (tipoCargo == null)
                    return Tuple.Create<List<ResponseEconomicJson>, ResponsePago, decimal>
                        (null, 
                        new ResponsePago
                            {
                            status = 400,
                            idPago = 0,
                            msg = "No se encuentra el tipo economico no. " + tipoCargo.TCE_NoTipoCargoEconomico
                        }, 
                        0);

                DateTime? time = DateTime.Today;
                ccee_CargoEconomico_otros tempEco = new ccee_CargoEconomico_otros();
                tempEco.CEcO_Monto = tipoCargo.TCE_Valor;
                tempEco.fk_Colegiado = requestPago.colegiado;
                tempEco.fk_TipoCargoEconomico = idTipo;
                tempEco.CEcO_FechaGeneracion = time;
                db.ccee_CargoEconomico_otros.Add(tempEco);
                db.SaveChanges();

                ResponseEconomicJson resp = new ResponseEconomicJson()
                {
                    id = tempEco.CEcO_NoCargoEconomico,
                    amount = tempEco.CEcO_Monto.Value,
                    idReferee = tempEco.fk_Colegiado.Value,
                    idTypeEconomic = tempEco.fk_TipoCargoEconomico,
                    description = tipoCargo.TCE_Descripcion,
                    conditionVAlue = tipoCargo.TCE_CondicionCalculo,
                    dateCreated = tempEco.CEcO_FechaGeneracion,
                    typeEconomic = 3,
                    idPay = 1,
                    enable = 1
                };
                // Pago Total
                totalPayCargo += tempEco.CEcO_Monto.Value;
                listaCargosGuardados.Add(resp);
            }
            return Tuple.Create<List<ResponseEconomicJson>, ResponsePago, decimal>
                        (listaCargosGuardados, null, totalPayCargo);
        }

        private int addPago(List<ResponseEconomicJson> listaCargos, RequestPay requestPago, decimal totalPayCargo) {
            try {
                ccee_Pago tempPay = new ccee_Pago()
                {
                    Pag_Monto = totalPayCargo,
                    Pag_Observacion = "",
                    Pag_Fecha = DateTime.Today,
                    fk_Caja = requestPago.idCaja,
                    fk_Cajero = requestPago.idCajero
                };
                db.ccee_Pago.Add(tempPay);
                db.SaveChanges();

                // Asociar el pago a los cargos economicos
                foreach (ResponseEconomicJson resp in listaCargos) {
                    ccee_CargoEconomico_otros tempEcoOther = db.ccee_CargoEconomico_otros.Find(resp.id);
                    tempEcoOther.fk_Pago = tempPay.Pag_NoPago;
                    db.Entry(tempEcoOther).State = EntityState.Modified;
                    db.SaveChanges();
                }

                ccee_MedioPago_Pago_C tempPaymentMethodPay = new ccee_MedioPago_Pago_C()
                {
                    MPP_Monto = totalPayCargo,
                    MPP_NoTransaccion = requestPago.metodoPago.noTransaccion,
                    MPP_NoDocumento = requestPago.metodoPago.noDocumento,
                    MPP_Observacion = requestPago.metodoPago.observaciones,
                    MPP_Reserva = 1,
                    MPP_Rechazado = 0,
                    MPP_FechaHora = DateTime.Now,
                    fk_MedioPago = requestPago.metodoPago.idMetodoPago,
                    fk_Pago = tempPay.Pag_NoPago,
                    fk_Banco = requestPago.metodoPago.idBanco
                };
                db.ccee_MedioPago_Pago_C.Add(tempPaymentMethodPay);
                db.SaveChanges();
                return tempPay.Pag_NoPago;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        // POST: api/WS2_BancoOtros
        public ResponsePago Post(RequestPay requestPago)
        {
            try
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        
                        // Verificar No de Transaccion
                        var tempExist = (from med in db.ccee_MedioPago_Pago_C
                                         where med.MPP_NoTransaccion.Equals(requestPago.metodoPago.noTransaccion)
                                         && med.fk_Banco == requestPago.metodoPago.idBanco
                                         select med.MPP_NoMedioPP).ToList();
                        if (tempExist.Count() > 0)
                            return new ResponsePago
                            {
                                status = 400,
                                idPago = 0,
                                msg = "El número de transacción ya existe en el sistema"
                            };

                        // Agregar los cargos economicos
                        Tuple<List<ResponseEconomicJson>, ResponsePago, decimal> listaCargos = addCargosEconomicos(transaction, requestPago);
                        if (listaCargos.Item1 == null)
                        {
                            transaction.Rollback();
                            return listaCargos.Item2;
                        }

                        if (Convert.ToDecimal(listaCargos.Item3) != Convert.ToDecimal(requestPago.metodoPago.monto))
                        {
                            transaction.Rollback();
                            return new ResponsePago
                            {
                                status = 400,
                                idPago = 0,
                                msg = "El monto no es suficiente para los cargos económicos"
                            };
                            //return Request.CreateResponse(HttpStatusCode.BadRequest, "El monto no es suficiente para los cargos económicos");
                        }

                        // Agregar el pago
                        int noPago = addPago(listaCargos.Item1, requestPago, listaCargos.Item3);
                        if (noPago < 0)
                        {
                            transaction.Rollback();
                            return new ResponsePago
                            {
                                status = 400,
                                idPago = 0,
                                msg = "No se puede agregar el pago"
                            };
                            //return Request.CreateResponse(HttpStatusCode.BadRequest, "No se puede agregar el pago");
                        }
                        
                        db.SaveChanges();
                        transaction.Commit();

                        var fk_pago = noPago;
                        var noRecibo = db.ccee_Documento.Count();

                        RequestAccountingDocumentJson request_Accounting = new RequestAccountingDocumentJson()
                        {
                            titleName = "",
                            address = "Guatemala",
                            nit = "",
                            remark = "Pago banca virtual",
                            idCashier = requestPago.idCajero,
                            idReferee = requestPago.colegiado,
                            idPay = fk_pago,
                            idTypeDocumentAccounting = 1,
                            NoDocumentCol = "B-"+requestPago.metodoPago.idBanco+ "-"+requestPago.metodoPago.noTransaccion,
                            //NoDocumentOther = null,   
                            listIdDocs = null,
                            dateTempCol = null,
                            dateTempTim = null,
                            isReferee = true
                        };

                        // AQUI HAY QUE HACER LO DEL OPENCLOSECASH
                        ReferreController refereeC = new ReferreController();
                        string texto = refereeC.PrintFileReferee(request_Accounting);
                        if (texto == "1")
                        {
                            return new ResponsePago
                            {
                                status = 400,
                                idPago = 0,
                                msg = "El Numero de recibo ya esta siendo usado!"
                            };
                        }

                        return new ResponsePago
                        {
                            status = 200,
                            idPago = 0,
                            msg = "Pago procesado correctamente"
                        };
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        return new ResponsePago
                        {
                            status = 400,
                            idPago = 0,
                            msg = e.Message
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                return new ResponsePago
                {
                    status = 400,
                    idPago = 0,
                    msg = ex.Message
                };
            }
        }

        private void borrarElCargoEconomico(int noCargoEco) {
            try {
                ccee_CargoEconomico_otros temp = db.ccee_CargoEconomico_otros.Find(noCargoEco);
                db.ccee_CargoEconomico_otros.Remove(temp);
                db.SaveChanges();
            } catch (Exception e) {
            }
        }

        // DELETE: api/WS2_BancoOtros/5
        public void Delete(int id)
        {
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace webcee.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ccee_Timbre
    {
        public int Tim_NoTimbre { get; set; }
        public Nullable<int> Tim_NumeroTimbre { get; set; }
        public Nullable<int> Tim_Estatus { get; set; }
        public Nullable<decimal> Tim_Valor { get; set; }
        public int fk_HojaTimbre { get; set; }
        public Nullable<int> fk_ValorTimbre { get; set; }
        public Nullable<int> fk_Pago { get; set; }
        public Nullable<System.DateTime> Tim_Fecha { get; set; }
        public Nullable<int> fk_Nomina { get; set; }
        public Nullable<int> Tim_Colegiado { get; set; }
    
        public virtual ccee_HojaTimbre ccee_HojaTimbre { get; set; }
        public virtual ccee_Pago ccee_Pago { get; set; }
        public virtual ccee_ValorTimbre ccee_ValorTimbre { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels.FileRequirement
{
    public class ResponseFileRequirementJson
    {
        public int id { get; set; }
        public string name { get; set; }
        public int required { get; set; }
        public int? initFile { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using webcee.Class;
using webcee.Class.Class;
using webcee.Class.Constant;
using webcee.Class.JsonModels.AddDues;
using webcee.Models;

namespace webcee.Controllers
{
    public class Api_AddDuesController : ApiController
    {

        private CCEEEntities db = new CCEEEntities();
        private const string regexReferee = @"^(3|8)?$";

        // GET: api/Api_AddDues
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [Route("api/Api_AddDues/{countC}/{countT}/{referee}")]
        // GET: api/Api_AddDues/5
        public ResponseAddDuesJson Get(int countC, int countT, int referee)
        {
            return getDues(countC, countT, referee);
        }

        // POST: api/Api_AddDues
        public IHttpActionResult Post(RequestAddDuesJson dues)
        {
            string autorization = Request.Headers.Authorization.Parameter;
            if (!Autorization.enableAutorization(db, autorization)) return null;

            using (var transaction = db.Database.BeginTransaction())
            {


                try
                {

                    var infoDues = addDuesPost(dues);
                    Autorization.saveOperation(db, autorization, "Agregando cuotas");
                    transaction.Commit();
                    return infoDues;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return Ok(HttpStatusCode.BadRequest);
                }
            }
        }

        // PUT: api/Api_AddDues/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api_AddDues/5
        public void Delete(int id)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        private List<ccee_TipoCargoEconomico> generarMontoColegiado()
        {
            string cole = GeneralFunction.getKeyTableVariable(db, Constant.table_var_colegiado);
            int idEconomicReferee = Convert.ToInt32(cole);
            ccee_TipoCargoEconomico tempTypeEco = (from tca in db.ccee_TipoCargoEconomico
                                                   where tca.TCE_NoTipoCargoEconomico == idEconomicReferee
                                                   select tca).Single();

            string calculation = tempTypeEco.TCE_CondicionCalculo;
            string[] arrayCalculation = calculation.Split(',');
            List<int> intCalculation = new List<int>();
            foreach (string item in arrayCalculation)
            {
                intCalculation.Add(Convert.ToInt32(item));
            }

            var listE = from tca in db.ccee_TipoCargoEconomico
                        where intCalculation.Contains(tca.TCE_NoTipoCargoEconomico)
                        select tca;
            return listE.ToList();

        }

        private bool getRegex(int typeReferee)
        {
            Regex r = new Regex(regexReferee, RegexOptions.IgnoreCase);
            Match mat = r.Match(typeReferee.ToString());
            return (mat.Success) ;
        }


        public ResponseAddDuesJson getDues(int countC, int countT, int referee)
        {
            decimal totalTimbre = 0;
            decimal totalCol = 0;
            try
            {
                //Calculo de timbres
                decimal valorP = db.ccee_TipoCargoEconomico.Find(5).TCE_Valor.Value;
                decimal montoTimbreGen = GeneralFunction.generarMontoTimbre(referee, db);
                for (int i = 0; i < countT; i++)
                {
                    totalTimbre += valorP + montoTimbreGen;
                }

                List<ccee_TipoCargoEconomico> listTipoCargos = generarMontoColegiado();
                for (int i = 0; i < countC; i++)
                {
                    for (int j = 0; j < listTipoCargos.Count; j++)
                    {
                        totalCol += listTipoCargos[j].TCE_Valor.Value;
                    }
                }

                return new ResponseAddDuesJson()
                {
                    totalC = totalCol,
                    totalT = totalTimbre
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IHttpActionResult addDuesPost(RequestAddDuesJson dues)
        {
            int idBase = Constant.type_economic_base;
            int idPostumo = Constant.type_economic_postumo;
            ccee_Colegiado referee = db.ccee_Colegiado.Find(dues.referee);

            if (dues.countC > 0)
            {
                Nullable<DateTime> lastRefereee = null;
                lastRefereee = (db.ccee_CargoEconomico_colegiado
                                    .Where(o => o.fk_Colegiado == dues.referee)
                                    .OrderByDescending(p => p.CEcC_FechaGeneracion)
                                    .Select(q => q.CEcC_FechaGeneracion))
                                    .FirstOrDefault();

                lastRefereee = ((lastRefereee == null) ? DateTime.Now : lastRefereee);
                List<ccee_TipoCargoEconomico> listTipoCargos = generarMontoColegiado();
                ccee_CargoEconomico_colegiado tempEconomicoCol;
                for (int i = 0; i < dues.countC; i++)
                {
                    DateTime nextMount = lastRefereee.Value.AddMonths(i + 1);
                    for (int j = 0; j < listTipoCargos.Count; j++)
                    {

                        tempEconomicoCol = new ccee_CargoEconomico_colegiado()
                        {
                            CEcC_Monto = listTipoCargos[j].TCE_Valor,
                            CEcC_FechaGeneracion = nextMount,
                            fk_TipoCargoEconomico = listTipoCargos[j].TCE_NoTipoCargoEconomico,
                            fk_Colegiado = dues.referee,
                            fk_Pago = null
                        };
                        if (dues.dateChange != null)
                        {
                            tempEconomicoCol.CEcC_FechaGeneracion = dues.dateChange;
                        }

                        db.ccee_CargoEconomico_colegiado.Add(tempEconomicoCol);

                    }
                }
            }

            if (dues.countT > 0)
            {
                Nullable<DateTime> lastTimbre = null;
                lastTimbre = (db.ccee_CargoEconomico_timbre
                                    .Where(o => o.fk_Colegiado == dues.referee)
                                    .OrderByDescending(p => p.CEcT_FechaGeneracion)
                                    .Select(q => q.CEcT_FechaGeneracion))
                                    .FirstOrDefault();
                lastTimbre = ((lastTimbre == null) ? DateTime.Now : lastTimbre);
                decimal valorP = db.ccee_TipoCargoEconomico.Find(idPostumo).TCE_Valor.Value;
                decimal montoTimbreGen = GeneralFunction.generarMontoTimbre(dues.referee, db);

                ccee_CargoEconomico_timbre tempTim = new ccee_CargoEconomico_timbre();
                ccee_CargoEconomico_timbre tempPostumo;
                for (int i = 0; i < dues.countT; i++)
                {
                    DateTime nextMount = lastTimbre.Value.AddMonths(i + 1);
                    if (!getRegex(referee.fk_TipoColegiado))
                    {
                        tempTim = new ccee_CargoEconomico_timbre()
                        {
                            CEcT_Monto = montoTimbreGen,
                            CEcT_FechaGeneracion = nextMount,
                            fk_TipoCargoEconomico = idBase,
                            fk_Colegiado = dues.referee,
                            fk_Pago = null
                        };
                    }

                    tempPostumo = new ccee_CargoEconomico_timbre()
                    {
                        CEcT_Monto = valorP,
                        CEcT_FechaGeneracion = nextMount,
                        fk_TipoCargoEconomico = idPostumo,
                        fk_Colegiado = dues.referee,
                        fk_Pago = null
                    };

                    if (dues.dateChange != null)
                    {
                        if (!getRegex(referee.fk_TipoColegiado))
                        {
                            tempTim.CEcT_FechaGeneracion = dues.dateChange;
                        }
                        tempPostumo.CEcT_FechaGeneracion = dues.dateChange;
                    }

                    if (!getRegex(referee.fk_TipoColegiado))
                    {
                        db.ccee_CargoEconomico_timbre.Add(tempTim);
                    }
                    db.ccee_CargoEconomico_timbre.Add(tempPostumo);

                }

            }

            db.SaveChanges();
            return Ok(HttpStatusCode.OK);
        }

    }
}

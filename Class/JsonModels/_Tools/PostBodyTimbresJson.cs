﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webcee.Class.JsonModels._Tools
{
    public class PostBodyTimbresJson
    {
        public string name { get; set; }
        public int idPay { get; set; }
        public int referee { get; set; }
        public List<PostBodyTimbreJson> timbres { get; set; }
    }
}